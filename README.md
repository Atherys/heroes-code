Atherys Heroes Skills Code
===

## Creating a Skill
Based on what the skill's name is, it will be in any of the 26 Skills_* Modules.  
(Ex: `SkillBlink` would be under `Skills/Skills_B`)  

*__All skill dependencies are inherited from the parent pom.__*
  
Steps:
1. Determine which skill submodule the skill belongs under. The Example above should explain how.
2. Open the Skills_* parent pom and find the ```<modules>``` block.
3. Add an entry just like the others (```<module>Skill[Skill Name]</module>```) into the Modules block.
4. If you are using IntelliJ IDEA, it will say that the module doesn't exist. Click on the module name, hit <kbd>ALT</kbd> + <kbd>ENTER</kbd>, and select ```Create Module with Parent```.
5. Once the skill module pom opens, accept all of the defaults.
6. Add ```<relativePath>../pom.xml</relativePath>``` above the parent version and remove the redundent ```<groupId>com.atherys</groupId>``` and ```<version>dev</version>```. (it whould look like the template below)
7. Right-click on the ```Skill[Skill Name]``` and create a new directory named ```src/main/java``` and ```src/main/resources```
8. Right-Click on the skill pom (pom.xml) and click ```Maven -> Reimport```, this will mark the above directories as ```Sources Root``` and ```Resources Root```.
9. Create ```skills.info``` in ```src/main/resources```, it will automatically copied into the root of the skill jar.
10. Right-click on ```src/main/java``` and create a new package, recommended to be either ```com.atherys.skills.[Lower-case Skill Name]``` or your own domain.
11. (Normal Java Development From Here)

#### Template Skill POM:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atherys</groupId>
        <artifactId>Skills_[A-Z]</artifactId>
        <relativePath>../pom.xml</relativePath>
        <version>dev</version>
    </parent>

    <artifactId>Skill[Skill Name]</artifactId>

</project>
```

## Compiling
#### Build Requirements for Linux Server [Jenkins]
* Maven 3
* At Least Java 1.8.0 update 101

#### Building the Entire Project (Must Be Done At Least Once)
1. Make sure all Build Requirements are Installed
2. Push a new commit or merge request to the master branch. The build will auto-build and deploy.

If the build fails with either a missing command error or something else, just run ```setup.sh``` then ```mvn``` in the project root.

#### Building a Single Skill
1. Go to Jenkins and select the AtherysSkills Project
2. Select modules and then the skill you wish to compile.
3. The jar will be placed on the server afterwards.
