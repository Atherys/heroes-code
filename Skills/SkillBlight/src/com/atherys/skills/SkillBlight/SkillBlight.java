package com.atherys.skills.SkillBlight;

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.lib.Lib;
import com.atherys.heroesaddon.lib.ParticleEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillBlight extends ActiveSkill {

    public SkillBlight(Heroes plugin) {
        super(plugin, "Blight");
        setDescription("Throw Blight, which deals $1 damage periodically over $3 seconds. Explosion radius: $2.");
        setUsage("/skill Blight");
        setArgumentRange(0, 0);
        setIdentifiers("skill Blight");
        setTypes(SkillType.DAMAGING, SkillType.DEBUFFING, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new SkillBlightListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 5.0);
        node.set(SkillSetting.DURATION.node(), 4000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set("explosion-radius", 1.5);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double radius = SkillConfigManager.getUseSetting(hero, this, "explosion-radius", 1.5D, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5.0D, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000L, false);
        return getDescription().replace("$1", damage + "").replace("$2", (double) (duration / 1000) + "").replace("$3", radius + "");
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        EnderPearl Blight = player.launchProjectile(EnderPearl.class);
        Blight.setMetadata("BlightEnderPearl", new FixedMetadataValue(plugin, hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class BlightDamageEffect extends PeriodicDamageEffect {
        public BlightDamageEffect(Skill skill, long period, long duration, double damage, Player player, boolean stuff) {
            super(skill, "BlightDamageEffect",player, period, duration, damage, stuff);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISEASE);
            this.types.add(EffectType.DISPELLABLE);
        }
    }

    public class SkillBlightListener implements Listener {

        private final Skill skill;

        public SkillBlightListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onBlightLand(ProjectileHitEvent event) {
            Entity x = event.getEntity();
            Location z = x.getLocation();
            if (!(x instanceof EnderPearl) || !x.hasMetadata("BlightEnderPearl")) {
                return;
            }
            Hero shooter = (Hero) x.getMetadata("BlightEnderPearl").get(0).value();
            if (shooter.getPlayer() == null) {
                return;
            }
            double radius = SkillConfigManager.getUseSetting(shooter, skill, "explosion-radius", 1.5D, false);
            double damage = SkillConfigManager.getUseSetting(shooter, skill, SkillSetting.DAMAGE, 5.0D, false);
            long period = (long) SkillConfigManager.getUseSetting(shooter, skill, SkillSetting.PERIOD, 1000L, false);
            long duration = (long) SkillConfigManager.getUseSetting(shooter, skill, SkillSetting.DURATION, 4000L, false);
            ParticleEffect.EXPLOSION_LARGE.display(0, 0, 0, 0, 1, z, HeroesAddon.PARTICLE_RANGE);
            z.getWorld().playSound(z, Sound.ENTITY_GENERIC_EXPLODE, 1F, 1F);
            boolean hasHit = false;
            for (Entity y : x.getWorld().getNearbyEntities(z, radius, radius, radius)) {
                if (y instanceof LivingEntity) {
                    if (damageCheck(shooter.getPlayer(), (LivingEntity) y)) {
                        CharacterTemplate character = plugin.getCharacterManager().getCharacter((LivingEntity) y);
                        addSpellTarget(y, shooter);
                        character.addEffect(new BlightDamageEffect(skill, period, duration, damage, shooter.getPlayer(), false));
                        hasHit = true;
                    }
                }
            }
            if (hasHit) {
                Lib.projectileHit(shooter.getPlayer());
            }
            x.remove();
        }
    }
}
