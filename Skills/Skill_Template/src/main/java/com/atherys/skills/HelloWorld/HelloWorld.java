package com.atherys.skills.HelloWorld;

/**
 * Created by JCastro on 5/12/2015.
 */

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class HelloWorld extends ActiveSkill {

    public HelloWorld(Heroes plugin) {
        super(plugin, "HelloWorld");
        setUsage("/skill HelloWorld");
        setArgumentRange(0, 0);
        setIdentifiers("skill HelloWorld");
        setDescription("Example HelloWorld skill for Atherys JAR creation with radius $1");
    }

    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, "radius", 25, false);
        return getDescription().replace("$1", radius+"");
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("range", Integer.valueOf(10));
        node.set(SkillSetting.COOLDOWN.node(), Integer.valueOf(300000));
        return node;
    }

    public SkillResult use(Hero hero, String[] arg1) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        broadcast(hero.getPlayer().getLocation(), hero.getName() + ChatColor.GRAY + " says Hello World!");

        for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r))
            if (e instanceof Player) {
                Player target = (Player) e;
                target.sendMessage(ChatColor.GREEN + hero.getName() + " says love you long time");
                target.spigot().playEffect(target.getLocation(), Effect.HEART, 1, 1, 0, 0, 0, 0, 20, r);
            }
        return SkillResult.NORMAL;
    }

}