package com.atherys.skills.SkillAbsolution;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainManaEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillAbsolution extends ActiveSkill {
    private String ApplyText;
    private String ExpireText;

    public SkillAbsolution(Heroes plugin) {
        super(plugin, "Absolution");
        setDescription("The three members of party within $1 blocks with lowest mana gain $2 mana every $3s for $4s.");
        setUsage("/skill absolution");
        setArgumentRange(0, 0);
        setIdentifiers("skill absolution");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING, SkillType.MANA_INCREASING);

    }

    @Override
    public String getDescription(Hero hero) {
        int amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 15, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false) / 1000;
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 10000, false) / 1000;
        String description = getDescription().replace("$1", radius + "");
        description = description.replace("$2", amount + "");
        description = description.replace("$3", period + "");
        description = description.replace("$4", duration + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), 15); //should now be the amount of mana regened per tick
        node.set(SkillSetting.RADIUS.node(), 20);
        node.set(SkillSetting.DURATION.node(), 20000L);
        node.set(SkillSetting.PERIOD.node(), 10000L); //should tick twice with default
        node.set(SkillSetting.APPLY_TEXT.node(), "Your mana is regenerating faster");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "Absolution effects expired");
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.ApplyText = (ChatColor.GRAY + SkillConfigManager.getUseSetting(null, this, SkillSetting.APPLY_TEXT.node(), "Your mana is regenerating faster"));
        this.ExpireText = (ChatColor.GRAY + SkillConfigManager.getUseSetting(null, this, SkillSetting.EXPIRE_TEXT.node(), "Absolution effects expired"));
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 15, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 10000, false);
        if (!hero.hasParty()) {
            hero.addEffect(new AbsolutionEffect(this, period, duration, amount,hero));
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        radius = radius > 0 ? radius : 0;
        Hero a = null, b = null, c = null;
        for (Hero pHero : hero.getParty().getMembers()) {
            if (player.getWorld().equals(pHero.getPlayer().getWorld()) && ((((pHero.getPlayer()).getLocation()).distanceSquared(player.getLocation())) <= radius * radius)) {
                if (a == null)
                    a = pHero;
                else if (b == null)
                    b = pHero;
                else if (c == null)
                    c = pHero;
                else if (a.getMana() > pHero.getMana())
                    a = pHero;
                else if (b.getMana() > pHero.getMana())
                    b = pHero;
                else if (c.getMana() > pHero.getMana())
                    c = pHero;
            }
        }
        if (a != null) {
            a.addEffect(new AbsolutionEffect(this, period, duration, amount,hero));
        }
        if (b != null) {
            b.addEffect(new AbsolutionEffect(this, period, duration, amount,hero));
        }
        if (c != null) {
            c.addEffect(new AbsolutionEffect(this, period, duration, amount,hero));
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class AbsolutionEffect extends PeriodicExpirableEffect {
        private final double amount;

        public AbsolutionEffect(Skill skill, long period, long duration, double amount,Hero caster) {
            super(skill, "Absolution",caster.getPlayer(), period, duration);
            this.amount = amount;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            hero.getPlayer().sendMessage(SkillAbsolution.this.ApplyText);
        }

        @Override
        public void tickHero(Hero hero) {
            try {
                HeroRegainManaEvent absEvent = new HeroRegainManaEvent(hero, (int) this.amount, this.skill);
                this.plugin.getServer().getPluginManager().callEvent(absEvent);
                if (absEvent.isCancelled()) {
                    return;
                }
                if (hero.getMana() == hero.getMaxMana()) return; //check and return
                else if (hero.getMana() + this.amount < hero.getMaxMana())
                    hero.setMana(hero.getMana() + (int) this.amount);
                else hero.setMana(hero.getMaxMana());
            } catch (Exception x) {
                System.err.println("Caught Exception : " + x.getMessage());
            }
        }

        @Override
        public void tickMonster(Monster monster) {}

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(SkillAbsolution.this.ExpireText);
        }

    }
}