package com.atherys.skills.SkillAbsorb;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillAbsorb extends ActiveSkill {
    public SkillAbsorb(Heroes plugin) {
        super(plugin, "Absorb");
        setDescription("Active\nYou gain a temporary shield that absolves some magic damage you take and turns it into mana.");
        setUsage("/skill Absorb");
        setArgumentRange(0, 0);
        setIdentifiers("skill Absorb");
        setTypes(SkillType.SILENCEABLE, SkillType.MANA_INCREASING, SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new AbsorbListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 8000);
        node.set("magic-damage-reduction", 0.25);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 8000, false);
        double reduction = SkillConfigManager.getUseSetting(hero, this, "magic-damage-reduction", 0.25, false);
        hero.addEffect(new AbsorbEffect(this, duration, reduction,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }



    public class AbsorbEffect extends PeriodicExpirableEffect {
        private double reduction;

        public AbsorbEffect(Skill skill, long duration, double reduction,Hero caster) {
            super(skill, "Absorb",caster.getPlayer(), 1000, duration);
            this.reduction = reduction;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1's $2 ran out!", player.getDisplayName(), "Absorb");
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (double[] coords : Lib.playerParticleCoords) {
                Location location = player.getLocation();
                location.add(coords[0], coords[1], coords[2]);
                location.getWorld().spawnParticle(Particle.DRIP_WATER, location, 1);
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }

        public double getReduction() { return reduction; }
    }

    public class AbsorbListener implements Listener {
        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.getDamage() == 0 || event.getCause() != EntityDamageEvent.DamageCause.MAGIC || !(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player)event.getEntity());
            if (hero.hasEffect("Absorb")) {
                double reduction = ((AbsorbEffect)hero.getEffect("Absorb")).getReduction();
                double damage = event.getDamage()*(1-reduction);
                int mana = (int)damage;
                hero.setMana(hero.getMana() + mana > hero.getMaxMana() ? hero.getMaxMana() : hero.getMana() + mana);
                event.setDamage(damage);
            }
        }
    }
}
