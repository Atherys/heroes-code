package com.atherys.skills.SkillAdrenaline;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillAdrenaline extends ActiveSkill {
	private String expireText;

	public SkillAdrenaline(Heroes plugin) {
		super(plugin, "Adrenaline");
		setDescription("Active\nYou are unable to die for a few seconds.");
		setUsage("/skill adrenaline");
		setArgumentRange(0, 0);
		setIdentifiers("skill adrenaline");
		setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
		Bukkit.getPluginManager().registerEvents(new DamageListener(), plugin);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), 10000);
		node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired!");
		return node;
	}

	@Override
	public SkillResult use(Hero hero, String[] args) {
		long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
		hero.addEffect(new AdrenalinEffect(this, duration,hero));
		broadcastExecuteText(hero);
		return SkillResult.NORMAL;
	}

	@Override
	public void init() {
		super.init();
		expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired!").replace("%hero%", "$1").replace("%skill%", "$2");
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	public class AdrenalinEffect extends ExpirableEffect {

		public AdrenalinEffect(Skill skill, long duration,Hero caster) {
			super(skill, "Adrenaline",caster.getPlayer(),duration);
			this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.UNBREAKABLE);
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			broadcast(hero.getPlayer().getLocation(), expireText, hero.getName(), "Adrenaline");
		}
	}

	public class DamageListener implements Listener {
		@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
		public void onHeroDamage(EntityDamageEvent event) {
			if ((!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
				return;
			}
			Player player = (Player) event.getEntity();
			Hero hero = plugin.getCharacterManager().getHero(player);
			if (hero.hasEffect("Adrenaline")) {
				double currentHealth = player.getHealth();
				if (currentHealth <= (event.getDamage() + player.getMaxHealth()*0.2)) {
					event.setDamage(0);
					event.setCancelled(true);
				}
			}
		}
	}


}
