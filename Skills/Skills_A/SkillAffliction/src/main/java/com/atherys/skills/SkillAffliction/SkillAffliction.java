package com.atherys.skills.SkillAffliction;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class SkillAffliction extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillAffliction(Heroes plugin) {
        super(plugin, "Affliction");
        setDescription("Targeted\nYour target is affected by Affliction, which deals damage over time and gives them Slowness and Mining Fatigue. The Affliction will spread to nearby players of the target.");
        setUsage("/skill Affliction");
        setArgumentRange(0, 0);
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.DEBUFFING);
        setIdentifiers("skill Affliction");
	    Bukkit.getServer().getPluginManager().registerEvents(new AfflictionBombListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(20000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(4000));
        node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(3));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(10));
        node.set("affliction-range", 7);
        node.set("affliction-heal", 5);
        node.set("debuffs-duration", 6000);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% is affected by %hero%'s %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% is no longer affected by %hero%'s %skill%!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT.node(), "%target% is affected by %hero%'s %skill%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT.node(), "%target% is no longer affected by %hero%'s %skill%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
    }

    public SkillResult use(Hero hero, String[] args) {
	    Player player = hero.getPlayer();
	    Potion potionItem = new Potion(PotionType.WATER, 1);
	    potionItem.splash();
	    ItemStack item = potionItem.toItemStack(1);
	    ThrownPotion potion = player.launchProjectile(ThrownPotion.class);
	    potion.setItem(item);
	    long time = System.currentTimeMillis();
	    potion.setMetadata("AfflictionBomb", new FixedMetadataValue(plugin, true));
	    if (hero.getCooldown("Outbreak") == null || (hero.getCooldown("Outbreak") - time) < 10000) {
		    hero.setCooldown("Outbreak", time + 10000);
	    }
	    broadcastExecuteText(hero);
	    return SkillResult.NORMAL;

        /*Player player = hero.getPlayer();
        if ((target.equals(player)) || (hero.getSummons().contains(target))) {
            return SkillResult.INVALID_TARGET;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 4000, true);
        double tickDamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 3, false);
        int range = SkillConfigManager.getUseSetting(hero, this, "affliction-range", 7, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, "affliction-heal", 5, false);
        long debuffsDuration = SkillConfigManager.getUseSetting(hero, this, "debuffs-duration", 6000, false);
        AfflictionEffect afflictionEffect = new AfflictionEffect(this, duration, period, tickDamage, player, range, true, heal, hero, debuffsDuration);
        plugin.getCharacterManager().getCharacter(target).addEffect(afflictionEffect);
        long time = System.currentTimeMillis();
        if (hero.getCooldown("Outbreak") == null || (hero.getCooldown("Outbreak") - time) < 10000) {
            hero.setCooldown("Outbreak", time + 10000);
        }
        return SkillResult.NORMAL;
        */
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class AfflictionEffect extends PeriodicDamageEffect {
        private int range;
        private boolean first;
        private double heal;
        private Hero applierHero;
        private long debuffsDuration;
        private int tick = 1;

        public AfflictionEffect(Skill skill, long duration, long period, double tickDamage, Player applier, int range, boolean first, double heal, Hero applierHero, long debuffsDuration) {
            super(skill, "Affliction",applier, period, duration, tickDamage);
            this.range = range;
            this.first = first;
            this.heal = heal;
            this.applierHero = applierHero;
            this.debuffsDuration = debuffsDuration;
            addMobEffect(2, (int) debuffsDuration/50, 1, false);
            addMobEffect(4, (int) debuffsDuration/50, 0, false);
            this.types.add(EffectType.DISEASE);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.SLOW);
            this.types.add(EffectType.DISPELLABLE);
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
            Lib.healHero(applierHero, heal, skill, applierHero);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, applier.getName(), player.getDisplayName(), skill.getName());
            Lib.healHero(applierHero, heal, skill, applierHero);
        }

        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, applier.getName(), player.getDisplayName(), skill.getName());
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            if(Skill.damageCheck(applier, (LivingEntity) player)) {
                addSpellTarget(player, applierHero);
                damageEntity(player, applier, tickDamage, skill.isType(SkillType.ABILITY_PROPERTY_PHYSICAL)? EntityDamageEvent.DamageCause.ENTITY_ATTACK: EntityDamageEvent.DamageCause.MAGIC, false);
                if (first && tick == 2) {
                    for (Entity entity : player.getNearbyEntities(range, range, range)) {
                        if (entity instanceof LivingEntity) {
                            if (damageCheck(applier, (LivingEntity)entity)) {
                                CharacterTemplate character = plugin.getCharacterManager().getCharacter((LivingEntity) entity);
                                if (!character.hasEffect("Affliction")) {
                                    AfflictionEffect afflictionEffect = new AfflictionEffect(skill, getDuration(), getPeriod(), tickDamage, applier, range, false, heal, applierHero, debuffsDuration);
                                    character.addEffect(afflictionEffect);
                                }
                            }
                        }
                    }
                }
                tick++;
            }
        }
    }
	public class AfflictionBombListener implements Listener {
		private Skill skill;

		public AfflictionBombListener(Skill skill) {
			this.skill = skill;
		}

		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void onPotionSplash(ProjectileHitEvent event) {
			Entity entity = event.getEntity();
			if (!(entity instanceof ThrownPotion)) {
				return;
			}
			if (event.getEntity().getShooter() instanceof Player) {
				if (event.getEntity().hasMetadata("AfflictionBomb")) {
					Player player = (Player) event.getEntity().getShooter();
					Hero hero = plugin.getCharacterManager().getHero(player);
					ThrownPotion potion = (ThrownPotion)event.getEntity();
					double radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 5D, false);
					long duration = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 20000, false);
					long period = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.PERIOD, 4000, true);
					double tickDamage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE_TICK, 3, false);
					int range = SkillConfigManager.getUseSetting(hero, skill, "affliction-range", 7, false);
					double heal = SkillConfigManager.getUseSetting(hero, skill, "affliction-heal", 5, false);
					long debuffsDuration = SkillConfigManager.getUseSetting(hero, skill, "debuffs-duration", 6000, false);
					AfflictionEffect afflictionEffect = new AfflictionEffect(skill, duration, period, tickDamage, player, range, true, heal, hero, debuffsDuration);

					for (Entity e : potion.getNearbyEntities(radius, radius, radius)) {
						if ((e instanceof Player)) {
							if (damageCheck((LivingEntity) player, (Player) e)) {
								Hero tHero = plugin.getCharacterManager().getHero((Player) e);
								addSpellTarget(tHero.getPlayer(), plugin.getCharacterManager().getHero(player));
								tHero.addEffect(afflictionEffect);
								Lib.cancelDelayedSkill(tHero);
							}
						} else if (e instanceof LivingEntity) {
							if (damageCheck(player, (LivingEntity) e)) {
								addSpellTarget((LivingEntity)e, plugin.getCharacterManager().getHero(player));
							}
						}
					}
				}
			}
		}

		@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
		public void onSplashEffect(PotionSplashEvent event) {
			if (event.getPotion().getShooter() instanceof Player) {
				if (event.getEntity().hasMetadata("AfflictionBomb")) {
					event.setCancelled(true);
				}
			}
		}
	}
}
