package com.atherys.skills.SkillAntidote;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;


public class SkillAntidote extends TargettedSkill {
    public SkillAntidote(Heroes plugin) {
        super(plugin, "Antidote");
        setDescription("Targeted\nCures your target of poisons");
        setUsage("/skill antidote <target>");
        setArgumentRange(0, 1);
        setIdentifiers("skill antidote");
        setTypes(SkillType.SILENCEABLE, SkillType.HEALING);
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if ((target instanceof Player)) {
            Hero targetHero = this.plugin.getCharacterManager().getHero((Player) target);
            boolean cured = false;
            for (Effect effect : targetHero.getEffects()) {
                if ((effect.isType(EffectType.POISON)) && (!effect.isType(EffectType.BENEFICIAL))) {
                    cured = true;
                    targetHero.removeEffect(effect);
                }
            }
            if (player.hasPotionEffect(PotionEffectType.POISON)) {
                player.removePotionEffect(PotionEffectType.POISON);
                cured = true;
            }
            if (!cured) {
                Messaging.send(player, "Your target is not poisoned!", 0);
                return SkillResult.INVALID_TARGET_NO_MSG;
            }
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
        return SkillResult.INVALID_TARGET;
    }

    public String getDescription(Hero hero) {
        String description = getDescription();
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }
}
