package com.atherys.skills.SkillAntivenom;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillType;


public class SkillAntivenom extends ActiveSkill {

    public SkillAntivenom(Heroes plugin) {
        super(plugin, "Antivenom");
        setDescription("Removes all damaging effects placed on you.");
        setUsage("/skill Antivenom");
        setArgumentRange(0, 0);
        setIdentifiers("skill Antivenom");
        setTypes(SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        for (Effect effect : hero.getEffects()) {
            if ((effect.isType(EffectType.BLEED) || effect.isType(EffectType.POISON) || effect.isType(EffectType.DISEASE))  && (!effect.isType(EffectType.UNBREAKABLE) && !effect.isType(EffectType.BENEFICIAL))) {
                hero.removeEffect(effect);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}