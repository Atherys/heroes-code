package com.atherys.skills.SkillArcaneOrb;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillArcaneOrb extends ActiveSkill {

    public SkillArcaneOrb(Heroes plugin) {
        super(plugin, "ArcaneOrb");
        setDescription("Throw ArcaneOrb, which deals $1 + explosion damage. Explosion radius: $2.");
        setUsage("/skill ArcaneOrb");
        setArgumentRange(0, 0);
        setIdentifiers("skill ArcaneOrb");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.NO_SELF_TARGETTING);
        Bukkit.getPluginManager().registerEvents(new SkillArcaneOrbListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 30.0);
        node.set("explosion-radius", 3.0);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        EnderPearl ArcaneOrb = player.launchProjectile(EnderPearl.class);
        ArcaneOrb.setMetadata("ArcaneOrbEnderPearl", new FixedMetadataValue(plugin, hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SkillArcaneOrbListener implements Listener {

        private final Skill skill;

        public SkillArcaneOrbListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onArcaneOrbLand(ProjectileHitEvent event) {
            Entity x = event.getEntity();
            Location z = x.getLocation();
            if (!(x instanceof EnderPearl) || !x.hasMetadata("ArcaneOrbEnderPearl")) {
                return;
            }
            Hero shooter = (Hero) x.getMetadata("ArcaneOrbEnderPearl").get(0).value();
            if (shooter.getPlayer() == null) {
                return;
            }
            double radius = SkillConfigManager.getUseSetting(shooter, skill, "explosion-radius", 3.0D, false);
            double damage = SkillConfigManager.getUseSetting(shooter, skill, SkillSetting.DAMAGE, 30.0D, false);
            z.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, z, 1);
            z.getWorld().playSound(z, Sound.ENTITY_GENERIC_EXPLODE, 1F, 1F);
            boolean hasHit = false;
            for (Entity y : x.getWorld().getNearbyEntities(z, radius, radius, radius)) {
                if (y instanceof LivingEntity) {
                    if (damageCheck(shooter.getPlayer(), (LivingEntity) y)) {
                        addSpellTarget(y, shooter);
                        skill.damageEntity((LivingEntity) y, shooter.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                        hasHit = true;
                    }
                }
            }
            if (hasHit) {
                Lib.projectileHit(shooter.getPlayer());
            }
            x.remove();
        }
    }
}