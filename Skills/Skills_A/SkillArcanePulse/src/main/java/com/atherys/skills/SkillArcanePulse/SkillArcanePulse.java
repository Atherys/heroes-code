package com.atherys.skills.SkillArcanePulse;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillArcanePulse extends ActiveSkill {
    public SkillArcanePulse(Heroes plugin) {
        super(plugin, "ArcanePulse");
        setDescription("All enemies in a cone infront of you are pushed back.");
        setUsage("/skill ArcanePulse");
        setArgumentRange(0, 0);
        setIdentifiers("skill ArcanePulse");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        List<Entity> nearbyPlayers = new ArrayList<Entity>();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 7, false);
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if ((entity instanceof Player)) {
                if (damageCheck(player, (LivingEntity) entity)) {
                    nearbyPlayers.add(entity);
                }
            }
        }
        if (!nearbyPlayers.isEmpty()) {
            Vector playerVector = player.getLocation().toVector();
            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000, false);
            int amplifier = SkillConfigManager.getUseSetting(hero, this, "slow-amplifier", 1, false);
            float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
            double coeficient = SkillConfigManager.getUseSetting(hero, this, "coeficient", 0.2, false);
            int maxTargets = SkillConfigManager.getUseSetting(hero, this, "max-targets", 3, false);
            int count = 0;
            for (Entity e : Lib.getEntitiesInCone(nearbyPlayers, playerVector, (float) radius, degrees, player.getLocation().getDirection())) {
                if (count >= maxTargets) {
                    break;
                }
                Player target = (Player) e;
                addSpellTarget(target, hero);
                double cc = (radius - target.getLocation().distance(player.getLocation())) * coeficient;
                Vector v = target.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
                v = v.multiply(cc);
                v.setY(0.5);
                target.setVelocity(v);
                plugin.getCharacterManager().getHero(target).addEffect(new SlowEffect(this, duration, amplifier, false, "", "", hero));
                count++;
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 7);
        node.set("coeficient", 0.2);
        node.set("degrees", 45);
        node.set(SkillSetting.DURATION.node(), 4000);
        node.set("slow-amplifier", 1);
        node.set("max-targets", 3);
        return node;
    }
}