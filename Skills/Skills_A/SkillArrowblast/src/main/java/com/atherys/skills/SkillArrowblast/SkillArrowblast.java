package com.atherys.skills.SkillArrowblast;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillArrowblast extends ActiveSkill {

    public SkillArrowblast(Heroes plugin) {
        super(plugin, "Arrowblast");
        setDescription("Active\nShoots $1-arrows at a rate of $2 per second.");
        setUsage("/skill Arrowblast");
        setArgumentRange(0, 0);
        setIdentifiers("skill Arrowblast");
        setTypes(SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.DAMAGING);
        Bukkit.getPluginManager().registerEvents(new ArrowblastListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        int arrows = SkillConfigManager.getUseSetting(hero, this, "arrows", 15, false) + (SkillConfigManager.getUseSetting(hero, this, "arrows-per-level", 0, false) * hero.getSkillLevel(this));
        int rate = SkillConfigManager.getUseSetting(hero, this, "rate", 6, false);
        String description = getDescription().replace("$1", arrows + "").replace("$2", rate + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("arrows", Double.valueOf(15));
        node.set("rate", Double.valueOf(6));
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(12.5));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        double arrows = SkillConfigManager.getUseSetting(hero, this, "arrows", Double.valueOf(15), false);
        double rate = SkillConfigManager.getUseSetting(hero, this, "rate", Double.valueOf(6), false);
        long period = (long)(1000/rate);
        long duration = (long)((arrows/rate) * 1000);
        hero.addEffect(new ArrowblastEffect(this, period, duration,hero));
        return SkillResult.NORMAL;
    }

    public class ArrowblastEffect extends PeriodicExpirableEffect {

        public ArrowblastEffect(Skill skill, long period, long duration,Hero caster) {
            super(skill, "ArrowblastEffect",caster.getPlayer(), period, duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You started shooting arrows!");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You stopped shooting arrows!");

        }

        @Override
        public void tickMonster(Monster mnstr) {
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            Arrow arrow = player.launchProjectile(Arrow.class);
            arrow.setMetadata("ArrowblastArrow", new FixedMetadataValue(plugin, true));
        }
    }

    public class ArrowblastListener implements Listener {
        private final Skill skill;

        public ArrowblastListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onArrowBlastDamage(EntityDamageByEntityEvent event) {
            if (!(event.getEntity() instanceof LivingEntity) || (!(event.getDamager() instanceof Arrow))) {
                return;
            }
            Arrow arrow = (Arrow) event.getDamager();
            if (!(arrow.getShooter() instanceof Player) || !arrow.hasMetadata("ArrowblastArrow")) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) arrow.getShooter());
            double damage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE, 12.5D, false);
            if (((LivingEntity) event.getEntity()).getHealth() <= 0) {
                arrow.remove();
                event.setCancelled(true);
            } else {
	            damageEntity((LivingEntity) event.getEntity(), hero.getPlayer(), damage, EntityDamageEvent.DamageCause.PROJECTILE, true);
                event.setDamage(0);
	            arrow.remove();
            }
        }

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onProjectileHit(ProjectileHitEvent event) {
            if (!(event.getEntity() instanceof Arrow) || !event.getEntity().hasMetadata("ArrowblastArrow")) {
                return;
            }
            event.getEntity().remove();
        }
    }
}