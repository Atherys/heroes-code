package com.atherys.skills.SkillAssassinsBlade;


import com.atherys.effects.BleedPeriodicDamageEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class SkillAssassinsBlade extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillAssassinsBlade(Heroes plugin) {
        super(plugin, "AssassinsBlade");
        setDescription("Active\nYou poison your blade which will deal an extra $1 magic damage every $2 seconds.");
        setUsage("/skill assassinsblade");
        setArgumentRange(0, 0);
        setIdentifiers("skill assassinsblade");
        setTypes(SkillType.BUFFING,SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillDamageListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("weapons", Util.swords);
        node.set("buff-duration", 60000);
        node.set("poison-duration", 10000);
        node.set(SkillSetting.PERIOD.node(), 2000);
        node.set(SkillSetting.DAMAGE_TICK.node(), 2);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 0);
        node.set("attacks", 1);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% is poisoned!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the poison!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% is poisoned!").replace("%target%", "$1");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% has recovered from the poison!").replace("%target%", "$1");
    }

    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 60000, false);
        int numAttacks = SkillConfigManager.getUseSetting(hero, this, "attacks", 1, false);
        if (!hero.hasEffect("PoisonBlade"))
            hero.addEffect(new AssassinBladeBuff(this, duration, numAttacks,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 2, false);
        damage += SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0, false) * hero.getSkillLevel(this);
        double seconds = SkillConfigManager.getUseSetting(hero, this, "poison-duration", 10000, false) / 1000.0D;
        String s = getDescription().replace("$1", damage + "").replace("$2", seconds + "");
        s = s + Lib.getSkillCostStats(hero, this);
        return s;
    }

    public class AssassinBladeBuff extends ExpirableEffect {
        private int numAttacks;

        public AssassinBladeBuff(Skill skill, long duration, int numAttacks,Hero caster) {
            super(skill, "PoisonBlade",caster.getPlayer(), duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
            this.numAttacks = numAttacks;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "Your blade is no longer poisoned!");
        }
        
        public int getAttacksLeft() {
            return numAttacks;
        }
        
        public void setAttacksLeft(int value) {
            this.numAttacks = value;
        }
    }

    public class AssassinBladePoison extends BleedPeriodicDamageEffect {
        public AssassinBladePoison(Skill skill, long period, long duration, double damage, Player player) {
            super(skill, "AssassinBladePoison",player, period, duration, damage);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.POISON);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.DISPELLABLE);
        }
    }

    public class SkillDamageListener implements Listener {
        private final Skill skill;

        public SkillDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero) ||
                    !(event.getEntity() instanceof Player) ||
                    (event.getDamage() == 0) ||
                    (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero hero = (Hero) event.getDamager();
            Player player = hero.getPlayer();
            ItemStack item = player.getItemInHand();
            if (!SkillConfigManager.getUseSetting(hero, this.skill, "weapons", Util.swords).contains(item.getType().name()))
                return;
            if (!(hero).hasEffect("PoisonBlade"))
                return;

            long duration = SkillConfigManager.getUseSetting(hero, this.skill, "poison-duration", 10000, false);
            long period = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.PERIOD, 2000, false);
            double tickDamage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE_TICK, 2, false);
            AssassinBladeBuff he = ((AssassinBladeBuff) hero.getEffect("PoisonBlade"));
            Entity target = event.getEntity();
            if ((event.getEntity() instanceof LivingEntity)) {
                checkBuff(hero);
                CharacterTemplate character = plugin.getCharacterManager().getCharacter((LivingEntity) target);
                if (!character.hasEffect("AssassinBladePoison")) {
                    character.addEffect(new AssassinBladePoison(skill, period, duration, tickDamage, hero.getPlayer()));
                }
            }
        }

        private void checkBuff(Hero hero) {
            AssassinBladeBuff abb = (AssassinBladeBuff)hero.getEffect("PoisonBlade");
            if (abb.getAttacksLeft() == 0) {
                hero.removeEffect(hero.getEffect("PoisonBlade"));
            } else {
                abb.setAttacksLeft(abb.getAttacksLeft() - 1);
            }
        }
    }
}