package com.atherys.skills.SkillBackstab;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class SkillBackstab extends PassiveSkill {
    private String useText;

    public SkillBackstab(Heroes plugin) {
        super(plugin, "Backstab");
        setDescription("Passive\nYou have a $1% chance to deal $2% damage when attacking from behind!");
        setArgumentRange(0, 0);
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.BUFFING,SkillType.SILENCEABLE);
        setEffectTypes(EffectType.BENEFICIAL, EffectType.PHYSICAL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroesListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("weapons", Util.swords);
        node.set("attack-bonus", 1.5D);
        node.set("attack-chance", 0.5D);
        node.set(SkillSetting.USE_TEXT.node(), "%hero% backstabbed %target%!");
        return node;
    }

    public void init() {
        super.init();
        useText = SkillConfigManager.getRaw(this, SkillSetting.USE_TEXT, "%hero% backstabbed %target%!").replace("%hero%", "$1").replace("%target%", "$2");
    }

    public String getDescription(Hero hero) {
        double chance = SkillConfigManager.getUseSetting(hero, this, "attack-chance", 0.5D, false);
        double percent = SkillConfigManager.getUseSetting(hero, this, "attack-bonus", 1.5D, false);
        return getDescription().replace("$1", Util.stringDouble(chance * 100.0D)).replace("$2", Util.stringDouble(percent * 100.0D));
    }

    public class SkillHeroesListener implements Listener {
        private final Skill skill;

        public SkillHeroesListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero)) {
                return;
            }
            Hero hero = (Hero) event.getDamager();
            Player player = hero.getPlayer();
            Entity target = event.getEntity();
            if (hero.hasEffect("Backstab") && ((hero.getCooldown("Backstab") == null) || (hero.getCooldown("Backstab") <= System.currentTimeMillis())) && (target != player)) {
                ItemStack item = player.getItemInHand();
                if (!SkillConfigManager.getUseSetting(hero, skill, "weapons", Util.swords).contains(item.getType().name())) {
                    return;
                }
                if (event.getEntity().getLocation().getDirection().dot(player.getLocation().getDirection()) <= 0.0D) {
                    return;
                } else if (Util.nextRand() < SkillConfigManager.getUseSetting(hero, skill, "attack-chance", 0.5D, false)) {
                    event.setDamage(event.getDamage() * SkillConfigManager.getUseSetting(hero, skill, "attack-bonus", 1.5D, false));
                }
                hero.setCooldown("Backstab", SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN, 3000, false) + System.currentTimeMillis());
            }
        }
    }
}