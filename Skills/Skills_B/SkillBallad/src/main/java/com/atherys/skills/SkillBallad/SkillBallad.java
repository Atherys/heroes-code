package com.atherys.skills.SkillBallad;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SkillBallad extends ActiveSkill {
    public SkillBallad(Heroes plugin) {
        super(plugin, "Ballad");
        setDescription("Active\nA song that makes enemies within $1 blocks store their weapons.");
        setUsage("/skill Ballad");
        setArgumentRange(0, 0);
        setIdentifiers("skill Ballad");
        setTypes(SkillType.SILENCEABLE, SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.DEBUFFING);
    }
    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        double r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        for (Entity x : player.getNearbyEntities(r, r, r)) {
            if ((x instanceof Player) && (damageCheck(player, (LivingEntity) x))) {
                disarmPlayer((Player)x);
                Util.syncInventory((Player)x, plugin);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }
    private void disarmPlayer(Player target) {
        ItemStack handStack = target.getItemInHand();
        if (handStack == null || handStack.getType() == Material.AIR) return;
        Inventory targetInv = target.getInventory();
        int lastEmptySlot = -1;
        for (int i = 35; i >= 0; i--) {
            ItemStack curStack = targetInv.getItem(i);
            if (curStack == null || curStack.getType() == Material.AIR) {
                lastEmptySlot = i;
                break;
            }
        }
        int swapSlot;
        if (lastEmptySlot >= 0) {
            swapSlot = lastEmptySlot;
        } else { // no empty slots
            // swap with random slot
            swapSlot = Util.nextInt(36);
        }
        target.setItemInHand(targetInv.getItem(swapSlot));
        targetInv.setItem(swapSlot, handStack);
    }
    @Override
    public String getDescription(Hero hero) {
        double r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        return getDescription().replace("$1", ((int)r) + "") + " " + Lib.getSkillCostStats(hero, this);
    }
}
