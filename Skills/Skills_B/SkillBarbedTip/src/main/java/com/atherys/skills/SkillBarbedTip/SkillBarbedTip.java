package com.atherys.skills.SkillBarbedTip;

import com.atherys.effects.KneebreakEffect;
import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;

public class SkillBarbedTip extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillBarbedTip(Heroes plugin) {
        super(plugin, "BarbedTip");
        setDescription("Your next loosed arrow within $1s will slow your target for $2s.");
        setUsage("/skill barbedtip");
        setArgumentRange(0, 0);
        setIdentifiers("skill barbedtip");
        setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillDamageListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(6000));
        node.set(SkillSetting.USE_TEXT.node(), "%hero% attaches a barbed tip to an arrow!");
        node.set("slow-duration", Integer.valueOf(4000));
        node.set("speed-multiplier", Integer.valueOf(2));
	    node.set("Kneebreak-duration",Long.valueOf(2000));
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% has been slowed by %hero%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% is no longer slowed!");
        return node;
    }

    public void init() {
        super.init();
        setUseText("%hero% attaches a barbed tip to an arrow!".replace("%hero%", "$1"));
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% has been slowed by %hero%!").replace("%target%", "$1").replace("%hero%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% is no longer slowed!").replace("%target%", "$1");
    }

    public SkillResult use(Hero hero, String[] args) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, false);
        hero.addEffect(new BarbedTipEffect(this, duration, 1,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int debuffDuration = SkillConfigManager.getUseSetting(hero, this, "slow-duration", 4000, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", debuffDuration / 1000 + "");
    }

    public class SkillDamageListener implements Listener {
        private final Skill skill;

        public SkillDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((!(event.getEntity() instanceof LivingEntity)) || (!(event instanceof EntityDamageByEntityEvent))) {
                return;
            }
            LivingEntity target = (LivingEntity) event.getEntity();
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            if (!(subEvent.getDamager() instanceof Arrow)) {
                return;
            }
            Arrow arrow = (Arrow) subEvent.getDamager();
            if (!(arrow.getShooter() instanceof Player)) {
                return;
            }
            Player player = (Player) arrow.getShooter();
            if (player.equals(target)) {
                return;
            }
            Hero hero = SkillBarbedTip.this.plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("BarbedTipEffect")) {
                if (damageCheck(player, target)) {
                    long duration = SkillConfigManager.getUseSetting(hero, this.skill, "slow-duration", 4000, false);
                    int amplifier = SkillConfigManager.getUseSetting(hero, this.skill, "speed-multiplier", 2, false);
	                long kneeDuration = SkillConfigManager.getUseSetting(hero, this.skill,"Kneebreak-duration",2000,false);
                    plugin.getCharacterManager().getCharacter(target).addEffect(new SlowEffect(this.skill, duration, amplifier, false, SkillBarbedTip.this.applyText, SkillBarbedTip.this.expireText, hero));
	                plugin.getCharacterManager().getCharacter(target).addEffect(new KneebreakEffect(this.skill,kneeDuration,hero));
                    hero.removeEffect((hero.getEffect("BarbedTipEffect")));
                }
            }
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onEntityShootBow(EntityShootBowEvent event) {
            if ((event.isCancelled()) || (!(event.getEntity() instanceof Player)) || (!(event.getProjectile() instanceof Arrow))) {
                return;
            }
            Hero hero = SkillBarbedTip.this.plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("BarbedTipEffect")) {
                int attacksLeft = ((BarbedTipEffect) hero.getEffect("BarbedTipEffect")).getAttacksLeft();
                if (attacksLeft!=0)
                    ((BarbedTipEffect) hero.getEffect("BarbedTipEffect")).setAttacksLeft(attacksLeft-1);
                else
                    hero.removeEffect(hero.getEffect("BarbedTipEffect"));
            }
        }
    }

    private static class BarbedTipEffect extends ExpirableEffect {
        private int attacks;
        public BarbedTipEffect(Skill skill, long duration, int attacks,Hero caster) {
            super(skill, "BarbedTipEffect",caster.getPlayer(), duration);
            this.attacks = attacks;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "BarbedTip expired");
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }
    }
}