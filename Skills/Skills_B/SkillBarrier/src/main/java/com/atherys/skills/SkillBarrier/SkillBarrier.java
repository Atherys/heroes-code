package com.atherys.skills.SkillBarrier;

import com.atherys.effects.KneebreakEffect;
import com.atherys.heroesaddon.lib.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillBarrier extends ActiveSkill {

    public SkillBarrier(Heroes plugin) {
        super(plugin, "Barrier");
        setArgumentRange(0, 0);
        setUsage("/skill Barrier");
        setIdentifiers("skill Barrier");
        setTypes(SkillType.SILENCEABLE);
        setDescription("Makes all party members within range of the rift take reduced damage and gives them immunity to fire damage!");
        Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(0.35D));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        if (!hero.hasEffect("Rift")) {
            player.sendMessage(ChatColor.GRAY + "You do not have an active obelisk!");
            return SkillResult.CANCELLED;
        }

        Object obeliskLoc = player.getMetadata("Rift-Obelisk-Location").get(0).value();
        if ( !( obeliskLoc instanceof Location ) ) {
            player.sendMessage("An internal error occured. Could not execute.");
            return SkillResult.FAIL;
        }

        long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 5000, false);
        double percentReduction = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.35D, false);
        double radius = SkillConfigManager.getUseSetting(hero, this, "radius", 10, false);
        BarrierBuffEffect applyEffect = new BarrierBuffEffect(this, duration, percentReduction,hero);


        hero.addEffect(applyEffect);
        if (hero.hasParty()) {
            for (Hero pHero : hero.getParty().getMembers()) {
                if (!(pHero.equals(hero)) && player.getWorld().equals(pHero.getPlayer().getWorld()) && (pHero.getPlayer().getLocation().distanceSquared((Location) obeliskLoc) <= radius * radius)) {
                    pHero.addEffect(applyEffect);
                }
            }
        }

        for (Entity entity : player.getWorld().getNearbyEntities((Location) obeliskLoc, radius, radius, radius)) {
            if (entity instanceof Player) {
                Player tPlayer = (Player) entity;
                if (damageCheck(player, (LivingEntity) tPlayer)) {
                    //KneebreakEffect applyEffectTwo = new KneebreakEffect(this, duration,hero);

                    Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
                    KneebreakEffect applyEffectTwo2 = new KneebreakEffect(this, duration,hero);
                    tHero.addEffect(applyEffectTwo2);
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class HeroDamageListener implements Listener {
        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("BarrierBuff")) {
                BarrierBuffEffect b = (BarrierBuffEffect) hero.getEffect("BarrierBuff");
                event.setDamage(event.getDamage() * b.getAmount());
            }
        }
    }

    public class BarrierBuffEffect extends ExpirableEffect {
        private final double amount;

        public BarrierBuffEffect(Skill skill, long duration, double amount,Hero caster) {
            super(skill, "BarrierBuff",caster.getPlayer(), duration);
            this.amount = amount;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(12, tickDuration, 1, false);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }

        public double getAmount() {
            return amount;
        }
    }
}
