package com.atherys.skills.SkillBasher;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillBasher extends PassiveSkill {
	private Skill basher;

	public SkillBasher(Heroes plugin) {
		super(plugin, "Basher");
		setDescription("Passive\nPassive $1% chance to stun for $2s on attack.");
		setTypes(SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.BUFFING);
		setEffectTypes(EffectType.BENEFICIAL, EffectType.PHYSICAL);
		Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
	}

	@Override
	public String getDescription(Hero hero) {
		int level = hero.getSkillLevel(this);
		double chance = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE.node(), 0.2D, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE_LEVEL.node(), 0.2D, false) * level) * 100.0D;
		chance = chance > 0.0D ? chance : 0.0D;
		double duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 2000, false) + level * SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false)) / 1000;
		duration = duration > 0.0D ? duration : 0.0D;
		return getDescription().replace("$1", chance + "").replace("$2", duration + "");
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.CHANCE.node(), 0.2D);
		node.set(SkillSetting.CHANCE_LEVEL.node(), 0.0D);
		node.set(SkillSetting.COOLDOWN.node(), 500);
		node.set(SkillSetting.DURATION.node(), 2000);
		node.set("duration-increase", 0);
		node.set("exp-per-stun", 0);
		return node;
	}

	@Override
	public void init() {
		super.init();
		basher = this;
	}

	public class SkillHeroListener implements Listener {
		private final Skill skill;

		public SkillHeroListener(Skill skill) {
			this.skill = skill;
		}

		@EventHandler(ignoreCancelled = true)
		public void onWeaponDamage(WeaponDamageEvent event) {
			if (!(event.getEntity() instanceof Player))
				return;
			Player tPlayer = (Player) event.getEntity();
			if ((event.getDamager() instanceof Hero)) {
				Player player = ((Hero) event.getDamager()).getPlayer();

				Hero hero = (Hero) event.getDamager();
				Hero tHero = plugin.getCharacterManager().getHero(tPlayer);

                if ( event.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE) ) {
                    return;
                }

				if (damageCheck(hero.getPlayer(), (LivingEntity) tPlayer)) {

					if ((hero.hasEffect("Basher")) && !tHero.hasEffect("Stun") && ((hero.getCooldown("Basher") == null) || (hero.getCooldown("Basher") <= System.currentTimeMillis())) && damageCheck(player, (LivingEntity) tPlayer)) {
						double chance = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.CHANCE.node(), 0.2D, false) + hero.getSkillLevel(skill) * SkillConfigManager.getUseSetting(hero, skill, SkillSetting.CHANCE_LEVEL.node(), 0.0D, false);
						chance = chance > 0.0D ? chance : 0.0D;
						long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(skill);
						cooldown = cooldown > 0L ? cooldown : 0L;
						hero.setCooldown("Basher", cooldown + System.currentTimeMillis());
						if (Util.nextRand() <= chance) {
							long duration = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION.node(), 2000, false) + hero.getSkillLevel(skill) * SkillConfigManager.getUseSetting(hero, skill, "duration-increase", 0, false);
							duration = duration > 0L ? duration : 0L;
							tHero.addEffect(new StunEffect(basher, duration,hero));
							Lib.cancelDelayedSkill(tHero);
							return;
						}
					}
				}
			}
		}
	}
}
