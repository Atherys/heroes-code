package com.atherys.skills.SkillBattleCommander;

import com.atherys.effects.CommanderDeployedEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroLeavePartyEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillBattleCommander extends TargettedSkill {
    private String applyText;
    private String expireText;
    public SkillBattleCommander(Heroes plugin) {
        super(plugin, "BattleCommander");
        setDescription("Targeted\nDesignate a target ally or yourself as the BattleCommander.");
        setArgumentRange(0, 1);
        setIdentifiers("skill battlecommander");
        setUsage("/skill battlecommander");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new PartyLeaveListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return  getDescription() + " " + Lib.getSkillCostStats(hero, this) ;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 is now the BattleCommander!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 is no longer the BattleCommander!");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) { //Target is not a player
            return SkillResult.INVALID_TARGET;
        }
        if (hero.hasParty()) {
            if (!(hero.getParty().isPartyMember((Player) target))) { //Target is not a party member
                return SkillResult.INVALID_TARGET;
            }
        } else if (!(target.equals(player))) { //Target != yourself IF you don't have a party
            return SkillResult.INVALID_TARGET;
        }
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        if(targetHero.hasEffect("CommanderEffect") || targetHero.hasEffect("OfficerEffect")) { //If the target already has an effect
            String message = (target.equals(player)) ? "A tactician has already assigned you a role!" : "A tactician has already assigned your target a role!";
            Messaging.send(player, message);
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
	    if (targetHero.hasEffect("MoraleOfficer")){
		    targetHero.removeEffect(targetHero.getEffect("MoraleOfficer"));
	    }
	    if (hero.hasEffect("MoraleOfficer")){
		    hero.removeEffect(hero.getEffect("MoraleOfficer"));
	    }
        if(hero.hasEffect("CommanderDeployedEffect")) { //hero had previously assigned a BattleCommander, remove previous effects
            CommanderDeployedEffect oldDeployEffect = (CommanderDeployedEffect) hero.getEffect("CommanderDeployedEffect");
            Hero oldCommander = oldDeployEffect.getCommander();
            CommanderEffect oldCommanderEffect = (CommanderEffect) oldCommander.getEffect("CommanderEffect");
            hero.removeEffect(oldDeployEffect);
            oldCommander.removeEffect(oldCommanderEffect);
        }
        //adding new effects
        hero.addEffect(new CommanderDeployedEffect(this, targetHero));
        targetHero.addEffect(new CommanderEffect(this, hero, applyText, expireText));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class CommanderEffect extends Effect {
        private Hero tactician;
        private final String applyText;
        private final String expireText;

        public CommanderEffect(Skill skill, Hero tactician, String applyText, String expireText) {
            super(skill, "CommanderEffect");
            this.applyText = applyText;
            this.expireText = expireText;
            this.tactician = tactician;
            this.types.add(EffectType.BENEFICIAL);
        }
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
        public Hero getTactician(){
            return tactician;
        }
    }

    public class PartyLeaveListener implements Listener {

        public PartyLeaveListener(){}

        @EventHandler(ignoreCancelled = true)
        public void onPartyLeave(HeroLeavePartyEvent event) {
            Hero leftHero = event.getHero();
            Player leftPlayer = leftHero.getPlayer();

            if(!leftPlayer.isOnline()||event.getReason().equals(HeroLeavePartyEvent.LeavePartyReason.DISCONNECT)) return;
            else if(leftHero.hasEffect("CommanderDeployedEffect")){
                CommanderDeployedEffect oldDeployEffect = (CommanderDeployedEffect)leftHero.getEffect("CommanderDeployedEffect");
                Hero oldCommander = oldDeployEffect.getCommander();
                if(oldCommander.equals(leftHero)) return; //If you were your own commander, leave yourself as the commander
                CommanderEffect oldCommanderEffect = (CommanderEffect)oldCommander.getEffect("CommanderEffect");
                leftHero.removeEffect(oldDeployEffect);
                oldCommander.removeEffect(oldCommanderEffect);
            } else if (leftHero.hasEffect("CommanderEffect")){
                CommanderEffect oldCommanderEffect = (CommanderEffect)leftHero.getEffect("CommanderEffect");
                Hero oldTactician = oldCommanderEffect.getTactician();
                CommanderDeployedEffect oldDeployEffect = (CommanderDeployedEffect)oldTactician.getEffect("CommanderDeployedEffect");
                leftHero.removeEffect(oldCommanderEffect);
                oldTactician.removeEffect(oldDeployEffect);
            }
        }
    }
}