package com.atherys.skills.SkillBattleCry;

import com.atherys.effects.MightEffect;
import com.atherys.effects.SilenceEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillBattleCry extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillBattleCry(Heroes plugin) {
        super(plugin, "BattleCry");
        setDescription("You call your allies to arms, dealing damage and silencing enemies nearby. You also grant nearby allies a damage boost.");
        setUsage("/skill battlecry");
        setArgumentRange(0, 0);
        setIdentifiers("skill battlecry");
        setTypes(SkillType.FORCE, SkillType.DAMAGING, SkillType.AREA_OF_EFFECT, SkillType.BUFFING);
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "Your muscles bulge with power!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You feel strength leave your body!");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage-radius", 5);
        node.set("buff-radius", 10);
        node.set(SkillSetting.DAMAGE.node(), 5);
        node.set("silence-duration", 2000);
        node.set("buff-duration", 6000);
        node.set("bonus-damage", 2);
        node.set("party-cooldown", 5000);
        node.set(SkillSetting.APPLY_TEXT.node(), "Your muscles bulge with power!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You feel strength leave your body!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int damageRadius = SkillConfigManager.getUseSetting(hero, this, "damage-radius", 5, false);
        int buffRadius = SkillConfigManager.getUseSetting(hero, this, "buff-radius", 10, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
        long silenceDuration = SkillConfigManager.getUseSetting(hero, this, "silence-duration", 2000, false);
        long buffDuration = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 6000, false);
        double bonusDamage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 2, false);
        Player player = hero.getPlayer();
        boolean hit = false;
        for (Entity e : player.getNearbyEntities(damageRadius, damageRadius, damageRadius)) {
            if ((e instanceof LivingEntity)) {
                if (damageCheck(player, (LivingEntity) e)) {
                    if (e instanceof Player) {
                        Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                        if (tHero.hasEffect("Invisible")) {
                            tHero.removeEffect(tHero.getEffect("Invisible"));
                        }
                        Lib.cancelDelayedSkill(tHero);
                        tHero.addEffect(new SilenceEffect(this, silenceDuration, true,hero));
                    }
                    damageEntity((LivingEntity)e, player, damage, DamageCause.MAGIC);
                    hit = true;
                }
            }
        }
        if (!hit) {
            Messaging.send(player, "No valid nearby enemies!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        MightEffect mightEffect = new MightEffect(this, buffDuration, bonusDamage, applyText, expireText,hero);
        if (hero.hasParty()) {
            for (Hero pHero : hero.getParty().getMembers()) {
                if (pHero.getPlayer().getWorld().equals(player.getWorld()) && pHero.getPlayer().getLocation().distanceSquared(player.getLocation()) <= buffRadius*buffRadius) {
                    pHero.addEffect(mightEffect);
                }
            }
            long partyCooldown = SkillConfigManager.getUseSetting(hero, this, "party-cooldown", 5000, false);
            Lib.partyCooldown(this, "BattleCry", hero, partyCooldown, buffRadius);
        } else {
            hero.addEffect(mightEffect);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}