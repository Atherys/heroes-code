package com.atherys.skills.SkillBearTrap;

import com.atherys.effects.StunEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillBearTrap extends ActiveSkill {
	private static final Set<Material> transMaterials = new HashSet<Material>();
	private String applyText;
	private String expireText;
	private String tiggerText;

	public SkillBearTrap(Heroes plugin) {
		super(plugin, "BearTrap");
		setDescription("3x3 trap");
		setUsage("/skill beartrap");
		setArgumentRange(0, 0);
		setIdentifiers("skill beartrap");
		setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
	}

	static {
		transMaterials.add(Material.WATER);
		transMaterials.add(Material.AIR);
		transMaterials.add(Material.LAVA);
		transMaterials.add(Material.SOUL_SAND);

	}
	@Override
	public SkillResult use(final Hero hero, String[] args) {
		applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% places a %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
		tiggerText = SkillConfigManager.getRaw(this, "tigger_text", "%enemy% triggered %caster%'s %skill%!").replace("%enemy%", "$1").replace("%caster%","$2").replace("%skill%", "$3");
		expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% has expired!").replace("%hero%", "$1").replace("%skill%", "$2");


		final int maxDistance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
		final int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
		final long delay = SkillConfigManager.getUseSetting(hero, this, "trap-delay", 3000, false);
		final int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
		final int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 35, false);
		final int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 15000, false);
		final Skill skill = this;
		//get player
		Player player = hero.getPlayer();

		Material mat = player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType();
		if (((transMaterials.contains(mat))) || (player.isInsideVehicle())) {
			Messaging.send(player, "You can't Place BearTrap while mid-Air");
			return SkillResult.FAIL;
		}
		// x,y,z
		//get center block of 3x3 area and the area
		final Set<Block> blocks = new HashSet<>();
		Location loc = player.getLocation();
		loc.add(0,0,0);
		final Block block = loc.getWorld().getBlockAt(loc);
		blocks.add(block);
		blocks.add(block.getRelative(BlockFace.NORTH));
		blocks.add(block.getRelative(BlockFace.NORTH_WEST));
		blocks.add(block.getRelative(BlockFace.WEST));
		blocks.add(block.getRelative(BlockFace.SOUTH_WEST));
		blocks.add(block.getRelative(BlockFace.SOUTH));
		blocks.add(block.getRelative(BlockFace.SOUTH_EAST));
		blocks.add(block.getRelative(BlockFace.EAST));
		blocks.add(block.getRelative(BlockFace.NORTH_EAST));

		final Set<Block> trapPlates = new HashSet<>();

		for (Block trapblocks :blocks){
			if (trapblocks.getType().equals(Material.AIR)){
				//check block below
				if (!((trapblocks.getRelative(BlockFace.DOWN).getType().equals(Material.AIR)) &&
						(trapblocks.getRelative(BlockFace.DOWN).getType().equals(Material.WATER)) &&
							(trapblocks.getRelative(BlockFace.DOWN).getType().equals(Material.GLASS))&&
								trapblocks.getRelative(BlockFace.DOWN).getType().equals(Material.STAINED_GLASS))){

					trapblocks.setType(Material.STONE_PLATE);
					trapblocks.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
					trapPlates.add(trapblocks);

				}
			}
		}


		/*
		for (Block b1 : blocks){
			if (block.getType() == Material.AIR){
				if (!(b1.getRelative(BlockFace.DOWN).getType()==Material.AIR)||(!(b1.getRelative(BlockFace.DOWN).getType()==Material.WATER))){
					Location b1loc = b1.getLocation().clone();
					if ((!(b1.getLocation().add(0,-1,0).getBlock().getType() == Material.GLASS))||(!(b1.getLocation().add(0,-1,0).getBlock().getType() == Material.STAINED_GLASS))||(!(b1.getLocation().add(0,-1,0).getBlock().getType() == Material.WATER))) {
						b1=b1loc.getBlock();
						b1.setType(Material.STONE_PLATE);
						b1.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
						trapPlates.add(b1);
					}
				}
			}
		}
		*/


	/*for (Block b1 : blocks) {
			if (!(b1.getType() == Material.AIR || b1.getType()==Material.WATER)) {

				if (!(b1.getRelative(BlockFace.DOWN).getType() == Material.AIR)) {
					if ((b1.getLocation().getBlock().getType() == Material.AIR)
							&&(!(b1.getLocation().getBlock().getType() == Material.GLASS))
							&&(!(b1.getLocation().getBlock().getType() == Material.STAINED_GLASS))
							&&(!(b1.getLocation().getBlock().getType() == Material.WATER))) {

						b1.setType(Material.STONE_PLATE);
						b1.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
						trapPlates.add(b1);
					}
				}
			}
		}
		*/


		//wait delay timer and make stone plates to iron plates
		plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
			@Override
			public void run() {
				for (Block b2 : trapPlates) {
					b2.setType(Material.IRON_PLATE);
					b2.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
				}
				hero.addEffect(new BearTrapEffect(skill, damage, radius, block, trapPlates, maxDistance, period, duration,hero));
			}
		}, ((delay/1000)*20));



		return SkillResult.NORMAL;
	}

	@Override
	public String getDescription(Hero p0) {
		return "3x3 trap";
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), 15000);
		node.set(SkillSetting.DAMAGE.node(), 35);
		node.set(SkillSetting.RADIUS.node(), 5);
		node.set("trap-delay", 2000);
		node.set(SkillSetting.PERIOD.node(), 2000);
		node.set(SkillSetting.MAX_DISTANCE.node(), 7);
		return node;
	}


	public class BearTrapEffect extends PeriodicExpirableEffect {
		final long damage;
		final long radius;
		final Set<Block> blockSet;
		final long maxDistance;
		final long period;
		final long duration;
		final Block centerBlock;
		Player result;

		public BearTrapEffect(Skill skill, long damage, long radius, Block centerBlock, Set<Block> blockSet, long maxDistance, long period, long duration,Hero caster) {
			super(skill, "BearTrapEffect",caster.getPlayer(),400L, duration);
			this.damage = damage;
			this.radius = radius;
			this.duration = duration;
			this.period = period;
			this.maxDistance = maxDistance;
			this.blockSet = blockSet;
			this.centerBlock = centerBlock;
			this.types.add(EffectType.BENEFICIAL);
		}

		@Override
		public void tickMonster(Monster p0) {
		//... N/A
		}

		@Override
		public void tickHero(Hero hero) {

			int radius = SkillConfigManager.getUseSetting(hero, this.skill, "trap-radius", 1, false);
			int damage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE, 35, false);
			int stunrad = SkillConfigManager.getUseSetting(hero, this.skill, "stun-radius", 5, false);
			long stunperiod = SkillConfigManager.getUseSetting(hero, this.skill, "stun-period", 2500, false);

			Block block = centerBlock;
			Player player=hero.getPlayer();

			if (result == null) {
				double lastDistance = Double.MAX_VALUE;
				for (Entity e : block.getWorld().getNearbyEntities(block.getLocation(), radius, radius, radius)) {
					if (e instanceof Player) {
						if (player.equals(e))
							continue;

						double distance = player.getLocation().distance(e.getLocation());
						if (distance < lastDistance) {
							lastDistance = distance;
							result = (Player) e;
						}
					}
				}
			}else{
				Hero tHero = plugin.getCharacterManager().getHero(result);
				if (!((hero.hasParty() && (hero.getParty().isPartyMember(result))))) {
					if (damageCheck(hero.getPlayer(), (LivingEntity) result)) {
						broadcast(hero.getPlayer().getLocation(), tiggerText, tHero.getName(), hero.getName(), "BearTrap");
						if (!(tHero.hasEffect("BearTrapHitEffect"))) {
							tHero.addEffect(new BearTrapHitEffect(skill, duration, hero, stunperiod, stunrad, damage));
							removeFromHero(hero);
							this.expire();
						}else if (tHero.hasEffect("BearTrapHitEffect")){
							this.expire();
						}
					}
				}
				broadcast(hero.getPlayer().getLocation(), expireText, hero.getName(), "BearTrap");
			}
		}


		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			broadcast(hero.getPlayer().getLocation(), applyText, hero.getName(), "BearTrap");
		}


		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			Lib.removeBlocks(blockSet);

		}
	}

	private class BearTrapHitEffect extends ExpirableEffect {

		final long stunrad;
		final int damage;
		final long stunperiod;
		final Hero caster;

		public BearTrapHitEffect(Skill skill, long period, Hero caster, long stunperiod, long stunrad, int damage) {
			super(skill, "BearTrapHitEffect",caster.getPlayer(), period);

			this.stunrad = stunrad;
			this.damage = damage;
			this.stunperiod = stunperiod;
			this.caster = caster;
		}

		@Override
		public void applyToHero(Hero hero) {
			for (Entity entity : hero.getPlayer().getLocation().getWorld().getNearbyEntities(hero.getPlayer().getLocation(), stunrad, stunrad, stunrad)) {
				if ((entity instanceof Player)) {
					Hero tHero = plugin.getCharacterManager().getHero((Player) entity);
					if (damageCheck(caster.getPlayer(), (LivingEntity) tHero.getPlayer())) {
						if (!(tHero.getPlayer().equals(caster.getPlayer()))) {
							damageEntity((LivingEntity) entity, caster.getPlayer(), (double) damage, EntityDamageEvent.DamageCause.CUSTOM);
							addSpellTarget(entity, caster);
							tHero.addEffect(new StunEffect(skill, stunperiod,hero));
						}
					}
				}
			}
		}
		@Override
		public void removeFromHero(Hero hero){
			super.removeFromHero(hero);
		}
	}
}