package com.atherys.skills.SkillBlazingSpeed;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillBlazingSpeed extends ActiveSkill {
    private String expiretext;

    public SkillBlazingSpeed(Heroes plugin) {
        super(plugin, "BlazingSpeed");
        setDescription("Active\nCaster gains speed buff for $1 seconds and players they touch catch fire.");
        setArgumentRange(0, 0);
        setUsage("/skill BlazingSpeed");
        setIdentifiers("skill BlazingSpeed");
        setTypes(SkillType.ABILITY_PROPERTY_FIRE,SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-multiplier", 2);
        node.set("fire-ticks", 50);
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired.");
        return node;
    }

    @Override
    public void init() {
        super.init();
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%' %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        String description = getDescription().replace("$1", d / 1000L + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int m = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 10000, false);
        int f = SkillConfigManager.getUseSetting(hero, this, "fire-ticks", 10000, false);
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        hero.addEffect(new BlazingSpeedEffect(this, d, f, m,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class BlazingSpeedEffect extends PeriodicExpirableEffect {
        private final int fireticks;

        public BlazingSpeedEffect(Skill skill, long duration, int fireticks, int multiplier,Hero caster) {
            super(skill, "BlazingSpeed",caster.getPlayer(), 250L, duration);
            this.fireticks = fireticks;
            addMobEffect(1, (int) (duration / 1000L) * 20, multiplier, false);
            this.types.add(EffectType.FIRE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.SPEED);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), getName());
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            Player p = hero.getPlayer();
            for (Entity e : p.getNearbyEntities(3, 3, 3)) {
                if ((e instanceof LivingEntity) && (damageCheck(p, ((LivingEntity) e)))) {
                    e.setFireTicks(fireticks);
                }
            }
        }
    }
}