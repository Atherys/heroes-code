package com.atherys.skills.SkillBleed;


import com.atherys.effects.BleedPeriodicDamageEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillBleed extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillBleed(Heroes plugin) {
        super(plugin, "Bleed");
        setDescription("Targeted\nYou cause your target to bleed, dealing $1 physical damage over $1 seconds.");
        setUsage("/skill bleed");
        setArgumentRange(0, 0);
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.ABILITY_PROPERTY_BLEED, SkillType.ABILITY_PROPERTY_PHYSICAL);
        setIdentifiers("skill bleed");
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(2000));
        node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(1));
        node.set(SkillSetting.DAMAGE_INCREASE.node(), Integer.valueOf(0));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(7));
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% is bleeding!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has stopped bleeding!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT.node(), "%target% is bleeding!").replace("%target%", "$1");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT.node(), "%target% has stopped bleeding!").replace("%target%", "$1");
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if ((target.equals(player)) || (hero.getSummons().contains(target))) {
            return SkillResult.INVALID_TARGET;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, true);
        double tickDamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 1, false);
        tickDamage += SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0, false) * hero.getSkillLevel(this);
        BleedSkillEffect bEffect = new BleedSkillEffect(this, duration, period, tickDamage, player);
        this.plugin.getCharacterManager().getCharacter(target).addEffect(bEffect);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1, false);
        damage += SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0, false) * hero.getSkillLevel(this);
        damage = damage * duration / period;
        String description = getDescription().replace("$1", damage + "").replace("$2", duration / 1000 + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    public class BleedSkillEffect extends BleedPeriodicDamageEffect {
        public BleedSkillEffect(Skill skill, long duration, long period, double tickDamage, Player applier) {
            super(skill, "Bleed",applier, period, duration, tickDamage);
            this.types.add(EffectType.BLEED);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.HARMFUL);
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillBleed.this.applyText, player.getDisplayName());
        }

        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
            broadcast(monster.getEntity().getLocation(), SkillBleed.this.expireText, monster.getName().toLowerCase() );
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillBleed.this.expireText, player.getDisplayName());
        }
    }
}
