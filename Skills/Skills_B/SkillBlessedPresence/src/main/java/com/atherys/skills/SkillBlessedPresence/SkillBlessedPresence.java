package com.atherys.skills.SkillBlessedPresence;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillBlessedPresence extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillBlessedPresence(Heroes plugin) {
        super(plugin, "BlessedPresence");
        setDescription("Toggle\nPeriodic heal, heals $1 hp every $2s in $3 radius around caster.");
        setUsage("/skill BlessedPresence");
        setArgumentRange(0, 0);
        setIdentifiers("skill blessedpresence");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING, SkillType.HEALING);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("BlessedPresence")) {
            hero.removeEffect(hero.getEffect("BlessedPresence"));
            return SkillResult.NORMAL;
        }
        int radius = (int) Math.pow(SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false), 2);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000, false);
        int mana = SkillConfigManager.getUseSetting(hero, this, "mana-per-tick", 10, false);
        double tickheal = SkillConfigManager.getUseSetting(hero, this, "tick-heal", 10, false);
        hero.addEffect(new BlessedPresenceEffect(this, period, radius, mana, tickheal));
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int h = SkillConfigManager.getUseSetting(hero, this, "tick-heal", 10, false);
        long p = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        String description = getDescription().replace("$1", h + "").replace("$2", p / 1000 + "").replace("$3", r + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("on-text", "%hero% starts %skill%!");
        node.set("off-text", "%hero% stops %skill%!");
        node.set("tick-heal", Integer.valueOf(1));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set("mana-per-tick", Integer.valueOf(1));
        return node;
    }

    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, "on-text", "%hero% starts %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, "off-text", "%hero% stops %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    public class BlessedPresenceEffect extends PeriodicEffect {
        private int radius;
        private int mana;
        private double tickheal;

        public BlessedPresenceEffect(SkillBlessedPresence skill, long period, int radius, int mana, double tickheal) {
            super(skill, "BlessedPresence", period);
            this.radius = radius;
            this.mana = mana;
            this.tickheal = tickheal;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.HEALING);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "BlessedPresence");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "BlessedPresence");
        }

        @Override
        public void tickHero(Hero hero) {
            if (hero.getMana() - mana <= 0) {
                hero.setMana(0);
                hero.removeEffect(this);
            } else {
                hero.setMana(hero.getMana() - mana);
            }
            if (!hero.hasParty()) {
                HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(hero, tickheal, skill, hero);
                plugin.getServer().getPluginManager().callEvent(hrhEvent);
                if (hrhEvent.isCancelled()) {
                    return;
                }
                hero.heal(hrhEvent.getDelta());
                return;
            }
            for (Hero partyHero : hero.getParty().getMembers()) {
                if (hero.getPlayer().getWorld().equals(partyHero.getPlayer().getWorld()) && (partyHero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= radius * radius)) {
                    HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(partyHero, tickheal, skill, hero);
                    plugin.getServer().getPluginManager().callEvent(hrhEvent);
                    if (hrhEvent.isCancelled()) {
                        return;
                    }
                    partyHero.heal(hrhEvent.getDelta());
                }
            }
        }
    }
}
