package com.atherys.skills.SkillBlessing;

import com.atherys.effects.InvulnEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillBlessing extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillBlessing(Heroes plugin) {
        super(plugin, "Blessing");
        setDescription("Place a glowstone block that will give party members within 8 blocks 30% magic resistance for 3 seconds, every 3 seconds for 9 seconds total.");
        setUsage("/skill Blessing");
        setArgumentRange(0, 0);
        setIdentifiers("skill Blessing");
        setTypes(SkillType.ABILITY_PROPERTY_EARTH, SkillType.ABILITY_PROPERTY_LIGHT, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 9000);
        node.set("buff-duration", 2000);
        node.set(SkillSetting.PERIOD.node(), 3000);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        node.set(SkillSetting.RADIUS.node(), 4);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%' %skill% expired.");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription();
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%' %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player p = hero.getPlayer();
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block b = p.getTargetBlock(Util.transparentBlocks, max);
        if (b.getType() == Material.AIR) {
            p.sendMessage(ChatColor.GRAY + "You must target a block.");
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        Block b1 = b.getRelative(BlockFace.UP);
        if (!(b1.getType() == Material.AIR || b1.getType() == Material.SNOW)) {
            p.sendMessage(ChatColor.GRAY + "Cannot be placed here");
            return SkillResult.CANCELLED;
        }
        values.add(b1);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 9000, false);
        long buffduration = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 2000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 3000, false);
        double r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 4.0, false);
        b1.setType(Material.GLOWSTONE);
        b1.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
        hero.addEffect(new BlessingEffect(this, period, duration, buffduration, r, b1, values,hero));
        return SkillResult.NORMAL;
    }

    public class BlessingEffect extends PeriodicExpirableEffect {
        private final double radius;
        private final Block block;
        private final long buffDuration;
        private final Set<Block> values;
        private final Skill skill;

        public BlessingEffect(Skill skill, long period, long duration, long buffDuration, double radius, Block b, Set<Block> values,Hero caster) {
            super(skill, "BlessingEffect",caster.getPlayer(), period, duration);
            this.radius = radius;
            this.block = b;
            this.buffDuration = buffDuration;
            this.values = values;
            this.skill = skill;
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.FORM);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Blessing");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Blessing");
            }
            Lib.removeBlocks(values);
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            if (hero.hasParty()) {
                Hero a = null;
                for (Hero pHero : hero.getParty().getMembers()) {
                    if (pHero.getPlayer().getWorld().equals(hero.getPlayer().getWorld()) && ((((pHero.getPlayer()).getLocation()).distanceSquared(block.getLocation())) <= radius * radius)) {
                        if (a == null) {
                            a = pHero;
                        } else if (a.getPlayer().getHealth() > pHero.getPlayer().getHealth()) {
                            a = pHero;
                        }
                    }
                }

                if (a != null) a.addEffect(new InvulnEffect(skill, "BlessingInvuln", buffDuration,hero));
            } else {
                if (hero.getPlayer().getLocation().distanceSquared(block.getLocation()) <= radius * radius) {
                    hero.addEffect(new InvulnEffect(skill, "BlessingInvuln", buffDuration,hero));
                }
            }
        }
    }
}

