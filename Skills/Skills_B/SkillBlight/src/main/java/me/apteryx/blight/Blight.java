package me.apteryx.blight;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import me.apteryx.blight.events.BlightPoisonDamageEvent;
import me.apteryx.blight.events.BlightSplashEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

/**
 * @author apteryx
 * @time 1:30 PM
 * @since 12/27/2016
 */
public class Blight extends ActiveSkill {

    private static Blight instance;
    private long period, duration;
    private double radius, damage;

    public Blight(Heroes plugin) {
        super(plugin, "Blight");
        setTypes(SkillType.ABILITY_PROPERTY_POISON, SkillType.DAMAGING);
        setUsage("/skill blight");
        setIdentifiers("skill blight");
        setDescription("Throw a poisoned potion, which deals %damage% magic damage every %period% seconds for %duration% seconds total to enemies within %radius% blocks of the impact.");
        Bukkit.getPluginManager().registerEvents(new BlightSplashEvent(), this.plugin);
        Bukkit.getPluginManager().registerEvents(new BlightPoisonDamageEvent(), this.plugin);
        instance = this;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        period = SkillConfigManager.getUseSetting(hero, this, "period", 2000, false);
        duration = SkillConfigManager.getUseSetting(hero, this, "duration", 8000, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 3, false);
        damage = SkillConfigManager.getUseSetting(hero, this, "damage", 15.0, false);
        Potion potion = new Potion(PotionType.POISON);
        potion.setSplash(true);
        ItemStack item = potion.toItemStack(1);
        ThrownPotion thrownPotion = hero.getPlayer().launchProjectile(ThrownPotion.class, hero.getPlayer().getLocation().getDirection());
        thrownPotion.setItem(item);
        thrownPotion.setMetadata("BlightPotion", new FixedMetadataValue(plugin, "The Black Keys - My Next Girl"));
        thrownPotion.setCustomName(hero.getName());
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("radius", 3);
        node.set("period", 2000);
        node.set("duration", 8000);
        node.set("damage", 15.0);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        period = SkillConfigManager.getUseSetting(hero, this, "period", 2000, false);
        duration = SkillConfigManager.getUseSetting(hero, this, "duration", 8000, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 3, false);
        damage = SkillConfigManager.getUseSetting(hero, this, "damage", 15.0, false);
        return getDescription().replace("%damage%", damage+"").replace("%duration%", (duration / 1000)+"").replace("%period%", (period / 1000)+"").replace("%radius%", radius+"");
    }

    public static Blight getInstance() {
        return instance;
    }

    public double getRadius() {
        return radius;
    }

    public long getDuration() {
        return duration;
    }

    public long getPeriod() {
        return period;
    }

    public double getDamage() {
        return damage;
    }
}
