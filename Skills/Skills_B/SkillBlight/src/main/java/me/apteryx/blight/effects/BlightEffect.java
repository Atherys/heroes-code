package me.apteryx.blight.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import me.apteryx.blight.Blight;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffectType;

/**
 * @author apteryx
 * @time 2:04 PM
 * @since 12/27/2016
 */
public class BlightEffect extends PeriodicExpirableEffect {


    public BlightEffect(Skill skill, String name, Player applier, long period, long duration) {
        super(skill, name, applier, period, duration);
        this.types.add(EffectType.POISON);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().sendMessage("\2477You have been plagued by a blight.");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        hero.getPlayer().sendMessage("\2477Your blight has been resolved.");
        if (hero.getPlayer().hasPotionEffect(PotionEffectType.POISON)) {
            hero.getPlayer().removePotionEffect(PotionEffectType.POISON);
        }
    }


    @Override
    public void tickMonster(Monster monster) {

    }

    @Override
    public void tickHero(Hero hero) {
        Blight.getInstance().damageEntity(hero.getPlayer(), getApplier(), Blight.getInstance().getDamage(), EntityDamageEvent.DamageCause.MAGIC, false);
    }
}
