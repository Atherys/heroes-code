package me.apteryx.blight.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author apteryx
 * @time 3:13 PM
 * @since 12/27/2016
 */
public class BlightPoisonDamageEvent implements Listener {

    @EventHandler
    public void onPoisonDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (!event.getEntity().hasMetadata("Poisoned")) {
                if (hero.hasEffect("Blight")) {
                    if (event.getCause() == EntityDamageEvent.DamageCause.POISON) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }
}
