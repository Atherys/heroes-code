package me.apteryx.blight.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import me.apteryx.blight.Blight;
import me.apteryx.blight.effects.BlightEffect;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author apteryx
 * @time 1:39 PM
 * @since 12/27/2016
 */
public class BlightSplashEvent implements Listener {

    @EventHandler
    public void onBlightSplash(PotionSplashEvent event) {
        if (event.getPotion().hasMetadata("BlightPotion")) {
            event.setCancelled(true);
            event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_SHULKER_BULLET_HIT, 1.0F, 0.1F);
            Hero hero = Heroes.getInstance().getCharacterManager().getHero(Bukkit.getPlayer(event.getPotion().getName()));
            if (hero != null) {
                for (Entity e : event.getEntity().getNearbyEntities(Blight.getInstance().getRadius(), Blight.getInstance().getRadius(), Blight.getInstance().getRadius())) {
                    if (e instanceof LivingEntity) {
                        if (e != hero.getPlayer()) {
                            if (hero.getParty() != null) {
                                if (e instanceof Player) {
                                    if (hero.getParty().isPartyMember((Player) e)) {
                                        continue;
                                    }
                                }
                            }

                            if (e instanceof Player) {
                                Player player = (Player) e;
                                player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, (int) (Blight.getInstance().getDuration() / 50), 1));
                                Hero target = Heroes.getInstance().getCharacterManager().getHero(player);
                                if (target.hasEffect("Blight")) {
                                    target.removeEffect(target.getEffect("Blight"));
                                    target.addEffect(new BlightEffect(Blight.getInstance(), "Blight", hero.getPlayer(), Blight.getInstance().getPeriod(), Blight.getInstance().getDuration()));
                                    continue;
                                }
                                target.addEffect(new BlightEffect(Blight.getInstance(), "Blight", hero.getPlayer(), Blight.getInstance().getPeriod(), Blight.getInstance().getDuration()));
                            }
                        }
                    }
                }
            }
        }
    }
}
