package com.atherys.skills.SkillBlink;

import com.atherys.effects.FeatherFallingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

public class SkillBlink extends ActiveSkill {
    public SkillBlink(Heroes plugin) {
        super(plugin, "Blink");
        setDescription("Active\nTeleports you forward in the direction you're looking.");
        setUsage("/skill blink");
        setArgumentRange(0, 0);
        setIdentifiers("skill blink");
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), 6);
        node.set("safefall-duration", 3000);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return SkillResult.FAIL;
        }
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        Block block = Lib.getTargetBlock(player, distance);
        if (block != null) {
            Location teleport;
            if (Util.transparentBlocks.contains(block.getRelative(BlockFace.UP).getType())) {
                teleport = block.getLocation().clone();
            } else {
                teleport = block.getRelative(BlockFace.DOWN).getLocation().clone();
            }
            if (!player.getLocation().getBlock().equals(teleport.getBlock()) && !player.getLocation().getBlock().getRelative(BlockFace.UP).equals(teleport.getBlock())) {
                teleport.add(0.5, 0, 0.5);
                teleport.setPitch(player.getLocation().getPitch());
                teleport.setYaw(player.getLocation().getYaw());
	            player.getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1.0F, 1.0F);
	            player.getPlayer().getWorld().playEffect(hero.getPlayer().getLocation(), Effect.ENDER_SIGNAL, 55);
                player.teleport(teleport, PlayerTeleportEvent.TeleportCause.UNKNOWN);
                long duration = SkillConfigManager.getUseSetting(hero, this, "safefall-duration", 3000, false);
                Messaging.send(player, "You blinked");
                hero.addEffect(new FeatherFallingEffect(this, duration,hero));
                return SkillResult.NORMAL;
            }
        }
        Messaging.send(player, "No location to blink to.");
        return SkillResult.INVALID_TARGET_NO_MSG;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
