package com.atherys.skills.SkillBlizzard;


import com.atherys.effects.KneebreakEffect;
import com.atherys.effects.SkillCastingEffect;
import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillBlizzard extends ActiveSkill {
    public SkillBlizzard(Heroes plugin) {
        super(plugin, "Blizzard");
        setUsage("/skill Blizzard");
        setArgumentRange(0, 0);
        setIdentifiers("skill Blizzard");
        setDescription("Active\nYou start channeling an Blizzard, slowing nearby enemies and raining icebolts from above periodically.");
        setTypes(SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_ICE, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new BlizzardListener(this), plugin);
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 25);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.PERIOD.node(), 5000);
        node.set("warm-up", 5000);
        node.set("blizzard-duration", 15000);
        node.set("period-slow-duration", 3000);
        node.set("kneebreak-duration", 1000);
        node.set("period-slow-amplifier", 2);
        node.set("circles", 7);
        node.set("icebolt-slow-duration", 6000);
        node.set("icebolt-slow-amplifier", 3);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        broadcastExecuteText(hero);
        long warmupDuration = SkillConfigManager.getUseSetting(hero, this, "warm-up", 5000, false);
        long blizzardDuration = SkillConfigManager.getUseSetting(hero, this, "blizzard-duration", 15000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 5000, false);
        double range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long periodSlowDuration = SkillConfigManager.getUseSetting(hero, this, "period-slow-duration", 3000, false);
        long kneebreakDuration = SkillConfigManager.getUseSetting(hero, this, "kneebreak-duration", 1000, false);
        int periodSlowAmplifier = SkillConfigManager.getUseSetting(hero, this, "period-slow-amplifier", 2, false);
        int circles = SkillConfigManager.getUseSetting(hero, this, "circles", 7, false);
        hero.addEffect(new BlizzardCastingEffect(this, warmupDuration, period, blizzardDuration, range, periodSlowDuration, kneebreakDuration, periodSlowAmplifier, circles,hero));

        return SkillResult.NORMAL;
    }

    public class BlizzardCastingEffect extends SkillCastingEffect {
        private long period;
        private long blizzardDuration;
        private double range;
        private long periodSlowDuration;
        private long kneebreakDuration;
        private int periodSlowAmplifier;
        private int circles;
        private Hero caster;

        public BlizzardCastingEffect(Skill skill, long duration, long period, long blizzardDuration, double range, long periodSlowDuration, long kneebreakDuration, int periodSlowAmplifier, int circles,Hero caster) {
            super(skill, duration, false, true,caster);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.ICE);
            this.period = period;
            this.blizzardDuration = blizzardDuration;
            this.range = range;
            this.periodSlowDuration = periodSlowDuration;
            this.kneebreakDuration = kneebreakDuration;
            this.periodSlowAmplifier = periodSlowAmplifier;
            this.circles = circles;
            this.caster=caster;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                hero.addEffect(new BlizzardEffect(skill, period, blizzardDuration, range, periodSlowDuration, kneebreakDuration, periodSlowAmplifier, circles,caster));
            }
        }
    }

    public class BlizzardEffect extends PeriodicExpirableEffect {
        private double range;
        private long periodSlowDuration;
        private long kneebreakDuration;
        private int periodSlowAmplifier;
        private int circles;

        public BlizzardEffect(Skill skill, long period, long duration, double range, long periodSlowDuration, long kneebreakDuration, int periodSlowAmplifier, int circles,Hero caster) {
            super(skill, "Blizzard",caster.getPlayer(), period, duration);
            this.range = range;
            this.periodSlowDuration = periodSlowDuration;
            this.kneebreakDuration = kneebreakDuration;
            this.periodSlowAmplifier = periodSlowAmplifier;
            this.circles = circles;
            this.types.add(EffectType.UNBREAKABLE);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            hero.addEffect(new BlizzardWaveEffect(skill, player.getLocation(), circles,hero));

            for (Entity aNear : player.getNearbyEntities(range, range, range)) {
                if (aNear instanceof LivingEntity) {
                    LivingEntity e = (LivingEntity) aNear;
                    if (!(damageCheck(hero.getPlayer(), e))) {
                        continue;
                    }
                    addSpellTarget(aNear, hero);
                    CharacterTemplate characterTemplate = plugin.getCharacterManager().getCharacter(e);
                    if (characterTemplate.hasEffectType(EffectType.SLOW)) {
                        characterTemplate.addEffect(new KneebreakEffect(skill, "BlizzardKneebreak", kneebreakDuration, "", "",hero));
                    }
                    characterTemplate.addEffect(new SlowEffect(skill, "BlizzardSlow", periodSlowDuration, periodSlowAmplifier, false, "", "", hero));
                }
            }

        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }

    public class BlizzardWaveEffect extends PeriodicExpirableEffect {
        private double counter = 2;
        private int snowballs = 8;
        private Location center;
        private int circles;
        private int periodNumber = 1;

        public BlizzardWaveEffect(Skill skill, Location center, int circles,Hero caster) {
            super(skill, "BlizzardWave",caster.getPlayer(), 100L, 100 * circles);
            this.center = center;
            this.circles = circles;
            this.types.add(EffectType.UNBREAKABLE);
        }

        @Override
        public void tickHero(Hero hero) {
            if (periodNumber >= circles) {
                this.expire();
                return;
            }
            Player player = hero.getPlayer();
            double increment = (2 * Math.PI) / snowballs;
            for (int a = 0; a < snowballs; a++) {
                double angle = a * increment;
                double xvalue = center.getX() + (counter * Math.cos(angle));
                double zvalue = center.getZ() + (counter * Math.sin(angle));
                Location spawnLoc = new Location(player.getWorld(), xvalue, /*height*/ player.getLocation().getY() + 4, zvalue);
                if (spawnLoc.getBlock().getType() != Material.AIR) {
                    continue;
                }
                spawnLoc.setPitch(90);
                Snowball icebolt = (Snowball) player.getWorld().spawnEntity(spawnLoc, EntityType.SNOWBALL);
                icebolt.setMetadata("BlizzardSnowball", new FixedMetadataValue(plugin, player));
                icebolt.getLocation().setDirection(new Vector(0, icebolt.getLocation().getY() - 10, 0));
            }
            snowballs += 2;
            counter += 1.5;
            periodNumber++;
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }

    public class BlizzardListener implements Listener {
        private final Skill skill;

        public BlizzardListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageByEntityEvent event) {
            if (!(event.getEntity() instanceof LivingEntity) || !(event.getDamager() instanceof Snowball) || !event.getDamager().hasMetadata("BlizzardSnowball")) {
                return;
            }
            Player player = (Player) ((Snowball) event.getDamager()).getMetadata("BlizzardSnowball").get(0).value();
            LivingEntity livingEntity = (LivingEntity) event.getEntity();
            if (!damageCheck(player, livingEntity)) {
                event.setCancelled(true);
                return;
            }
            CharacterTemplate characterTemplate = plugin.getCharacterManager().getCharacter(livingEntity);
            if (characterTemplate.hasEffect("BlizzardCheck")) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero(player);
            characterTemplate.addEffect(new DamageCheckEffect(skill,hero));

            double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 25.0, false);
            long iceboltSlowDuration = SkillConfigManager.getUseSetting(hero, skill, "icebolt-slow-duration", 6000, false);
            int iceboltSlowAmplifier = SkillConfigManager.getUseSetting(hero, skill, "icebolt-slow-amplifier", 3, false);
            characterTemplate.addEffect(new SlowEffect(skill, "BlizzardIceboltSlow", iceboltSlowDuration, iceboltSlowAmplifier, false, "", "", hero));
            addSpellTarget(livingEntity, hero);
            damageEntity(livingEntity, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
            event.setCancelled(true);
        }
    }

    public class DamageCheckEffect extends ExpirableEffect {
        public DamageCheckEffect(Skill skill,Hero caster) {
            super(skill, "BlizzardCheck",caster.getPlayer(), 500L);
        }
    }
}