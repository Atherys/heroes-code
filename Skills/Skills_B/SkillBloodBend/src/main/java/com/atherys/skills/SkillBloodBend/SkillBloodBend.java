package com.atherys.skills.SkillBloodBend;


import com.atherys.effects.RootEffect;
import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.util.IDAPI;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashSet;

public class SkillBloodBend extends TargettedSkill {
    public SkillBloodBend(Heroes plugin) {
        super(plugin, "BloodBend");
        setDescription("Targeted\n Takes control of a target.");
        setUsage("/skill BloodBend");
        setArgumentRange(0, 0);
        setIdentifiers("skill BloodBend");
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 20000);
        node.set("max-distance", 20);
        node.set("root-duration", 3000L);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!hero.getPlayer().hasLineOfSight(target)) {
            return SkillResult.INVALID_TARGET;
        }
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }

        Player targetp = (Player) target;
        if (targetp.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }

        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000L, false);
        BloodBendEffect b = new BloodBendEffect(this, duration, targetp, hero);
        hero.addEffect(b);
        damageEntity(targetp, hero.getPlayer(), 1D, EntityDamageEvent.DamageCause.MAGIC);
        playParticles((Player) target);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    public HashSet<Byte> TransparentBlock() {
        HashSet<Byte> set = new HashSet<>();
        set.add((byte) IDAPI.getIdOfMaterial(Material.AIR));
        set.add((byte) IDAPI.getIdOfMaterial(Material.WATER));
        set.add((byte) IDAPI.getIdOfMaterial(Material.REDSTONE_TORCH_ON));
        set.add((byte) IDAPI.getIdOfMaterial(Material.REDSTONE_TORCH_OFF));
        set.add((byte) IDAPI.getIdOfMaterial(Material.REDSTONE_WIRE));
        set.add((byte) IDAPI.getIdOfMaterial(Material.TORCH));
        set.add((byte) IDAPI.getIdOfMaterial(Material.SNOW));
        set.add((byte) IDAPI.getIdOfMaterial(Material.STRING));
        set.add((byte) IDAPI.getIdOfMaterial(Material.GRASS));
        return set;
    }

    public boolean isReplaceable(Material mat) {
        return (mat == Material.AIR) || (mat == Material.SNOW) || (mat == Material.STRING) || (mat == Material.GRASS);
    }

    public class BloodBendEffect extends ExpirableEffect {
        private Player target = null;

        public BloodBendEffect(Skill skill, long duration, Player target,Hero caster) {
            super(skill, "BloodBendEffect",caster.getPlayer(), duration);
            types.add(EffectType.CONFUSION);
            this.target = target;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                Messaging.send(hero.getPlayer(), "You lose your grasp on the target.");
            }
        }

        public Player getTarget() {
            return target;
        }

        public void setTarget(Player target) {
            this.target = target;
        }
    }

    public class PlayerInteractListener implements Listener {
        private final Skill skill;

        public PlayerInteractListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onPlayerInteract(PlayerInteractEvent event) {
            Hero hero = plugin.getCharacterManager().getHero(event.getPlayer());
            BloodBendEffect b = (BloodBendEffect) hero.getEffect("BloodBendEffect");
            if (!hero.hasEffect("BloodBendEffect"))
                return;
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Location loc = event.getClickedBlock().getRelative(BlockFace.UP, 1).getLocation();
                if (isReplaceable(loc.getBlock().getType()) && (event.getClickedBlock().getRelative(BlockFace.UP, 2).getLocation().getBlock().getType() == Material.AIR)) {
                    Player p = b.getTarget();
                    if (b.getTarget() == null) {
                        hero.removeEffect(b);
                        return;
                    }
                    if (!(event.getPlayer().hasLineOfSight(b.getTarget())))
                        return;
                    long duration = (long) SkillConfigManager.getUseSetting(hero, skill, "root-duration", 3000L, false);
                    playParticles(p);
                    p.teleport(loc);
                    playParticles(p);
                    Hero tHero = plugin.getCharacterManager().getHero(p);
                    tHero.addEffect(new RootEffect(skill, duration,hero));
                    Lib.cancelDelayedSkill(tHero);
                    hero.removeEffect(b);
                }
            }
        }
    }

    private void playParticles(Player player) {
        for (double[] coords : Lib.playerParticleCoords) {
            Location location = player.getLocation();
            location.add(coords[0], coords[1], coords[2]);
            player.getWorld().spigot().playEffect(location, Effect.TILE_BREAK, Material.NETHER_WARTS.getId(), 0, 0, 0.0F, 0.0F, 0.0F, 100, HeroesAddon.PARTICLE_RANGE);
        }
    }
}
