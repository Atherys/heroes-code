package com.atherys.skills.SkillBloodBoil;


import com.atherys.effects.StunEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillBloodBoil extends ActiveSkill {
    public SkillBloodBoil(Heroes plugin) {
        super(plugin, "BloodBoil");
        setDescription("Active\nAoE stun costing 20 health and returning 10 health per person hit, capping at 100 health returned.");
        setUsage("/skill BloodBoil");
        setArgumentRange(0, 0);
        setIdentifiers("skill bloodboil");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("heal", 10);
        node.set("targeted-players", 10);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set(SkillSetting.DURATION.node(), 1000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1000, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, "heal", 10, false);
        int targetplayers = SkillConfigManager.getUseSetting(hero, this, "targeted-players", 10, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 15, false);
        int healthReturn = 0;
        for (Entity e : player.getNearbyEntities(r, r, r)) {
            if ((e instanceof LivingEntity) && (damageCheck(hero.getPlayer(), (LivingEntity) e))) {
                CharacterTemplate characterTemplate = plugin.getCharacterManager().getCharacter((LivingEntity) e);
                characterTemplate.addEffect(new StunEffect(this, duration,hero));
                damageEntity(characterTemplate.getEntity(), player, damage, EntityDamageEvent.DamageCause.MAGIC, false);
                if (e instanceof Player) {
                    Lib.cancelDelayedSkill((Hero) characterTemplate);
                }
                HeroRegainHealthEvent healthGet = new HeroRegainHealthEvent(hero, heal, this, hero);
                plugin.getServer().getPluginManager().callEvent(healthGet);
                if (!(healthGet.isCancelled())) {
                    if (healthReturn <= (targetplayers - 1)) {
                        hero.heal(heal);
                        healthReturn++;
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}