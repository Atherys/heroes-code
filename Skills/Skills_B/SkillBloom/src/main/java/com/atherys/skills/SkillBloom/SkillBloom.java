package com.atherys.skills.SkillBloom;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;

public class SkillBloom extends ActiveSkill {
    public SkillBloom(Heroes plugin) {
        super(plugin, "Bloom");
        setDescription("Toggle\nChanges a skill to perform its bloom effects");
        setUsage("/skill Bloom");
        setArgumentRange(0, 0);
        setIdentifiers("skill bloom");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (!hero.hasEffect("BloomEffect")) {
            hero.addEffect(new BloomEffect(this));
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Bloom effects activated");
        } else {
            hero.removeEffect(hero.getEffect("BloomEffect"));
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Bloom effects deactivated");
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class BloomEffect extends Effect {
        public BloomEffect(Skill skill) {
            super(skill, "BloomEffect");
            types.add(EffectType.UNBREAKABLE);
        }
    }
}
