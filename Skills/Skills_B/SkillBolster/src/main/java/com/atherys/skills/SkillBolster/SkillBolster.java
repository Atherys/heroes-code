package com.atherys.skills.SkillBolster;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillBolster extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillBolster(Heroes plugin) {
        super(plugin, "Bolster");
        setDescription("Increases your party's defense by $1% for $2 seconds.");
        setArgumentRange(0, 0);
        setUsage("/skill Bolster");
        setIdentifiers("skill bolster");
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL,SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(0.5D));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double a = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.2, false);
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        String description = getDescription().replace("$1", a * 100 + "").replace("$2", d / 1000 + "");
        // COOLDOWN
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description += " CD:" + cooldown + "s";
        }
        // MANA
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
        if (mana > 0) {
            description += " M:" + mana;
        }
        // HEALTH_COST
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this));
        if (healthCost > 0) {
            description += " HP:" + healthCost;
        }
        // STAMINA
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
        if (staminaCost > 0) {
            description += " FP:" + staminaCost;
        }
        // DELAY
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description += " W:" + delay + "s";
        }
        // EXP
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description += " XP:" + exp;
        }
        return description;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, "radius", 10, true);
        long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.2D, false);
        if (!hero.hasParty()) {
            broadcastExecuteText(hero);
            hero.addEffect(new BolsterEffect(this, duration, player, amount));
            return SkillResult.NORMAL;
        }
        BolsterEffect b = new BolsterEffect(this, duration, player, amount);
        broadcastExecuteText(hero);
        for (Hero phero : hero.getParty().getMembers()) {
            if (player.getWorld().equals(phero.getPlayer().getWorld()) && phero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= radius * radius) {
                phero.addEffect(b);
            }
        }
        return SkillResult.NORMAL;
    }

    public class HeroDamageListener implements Listener {
        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("Bolster")) {
                BolsterEffect b = (BolsterEffect) hero.getEffect("Bolster");
                event.setDamage(event.getDamage() * b.getAmount());
            }
        }
    }

    public class BolsterEffect extends ExpirableEffect {
        private final Player caster;
        private final double amount;

        public BolsterEffect(Skill skill, long duration, Player caster, double amount) {
            super(skill, "Bolster",caster, duration);
            this.caster = caster;
            this.amount = amount;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Messaging.send(hero.getPlayer(), "You are now Bolstered!");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "You are no longer Bolstered!");
        }

        public double getAmount() {
            return amount;
        }
    }
}
