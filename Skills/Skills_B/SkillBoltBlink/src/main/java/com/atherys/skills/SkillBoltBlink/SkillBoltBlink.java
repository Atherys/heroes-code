package com.atherys.skills.SkillBoltBlink;


import com.atherys.effects.FeatherFallingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillBoltBlink extends ActiveSkill {
    public SkillBoltBlink(Heroes plugin) {
        super(plugin, "BoltBlink");
        setDescription("Active\nTeleports you forward in the direction you're looking, striking closest player within a few blocks with lightning dealing magic damage.");
        setUsage("/skill boltblink");
        setArgumentRange(0, 0);
        setIdentifiers("skill boltblink");
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING, SkillType.ABILITY_PROPERTY_LIGHTNING);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(10));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(25));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(5));
        node.set("safefall-duration", 3000);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return SkillResult.FAIL;
        }
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        Block block = Lib.getTargetBlock(player, distance);
        if (block != null) {
            Location teleport;
            if (Util.transparentBlocks.contains(block.getRelative(BlockFace.UP).getType())) {
                teleport = block.getLocation().clone();
            } else {
                teleport = block.getRelative(BlockFace.DOWN).getLocation().clone();
            }
            if (!player.getLocation().getBlock().equals(teleport.getBlock()) && !player.getLocation().getBlock().getRelative(BlockFace.UP).equals(teleport.getBlock())) {
                double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 25, false);
                double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
                LivingEntity target = null;
                for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                    if (e instanceof LivingEntity) {
                        if (damageCheck(player, (LivingEntity) e)) {
                            if (target == null || target.getLocation().distanceSquared(player.getLocation()) > e.getLocation().distanceSquared(player.getLocation())) {
                                target = (LivingEntity) e;
                            }
                        }
                    }
                }
	            try {
		            if ((target != null)) {
			            target.getWorld().strikeLightning(target.getLocation());
			            damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
			            addSpellTarget(target, hero);
		            } else {
			            player.getWorld().strikeLightningEffect(loc);
		            }
	            }catch (Exception e){
		            //placeholder for error handling!
	            }
                teleport.add(0.5, 0, 0.5);
                teleport.setPitch(player.getLocation().getPitch());
                teleport.setYaw(player.getLocation().getYaw());
                player.teleport(teleport);
                broadcastExecuteText(hero);
                long duration = SkillConfigManager.getUseSetting(hero, this, "safefall-duration", 3000, false);
                hero.addEffect(new FeatherFallingEffect(this, duration,hero));
                return SkillResult.NORMAL;
            }
        }
        Messaging.send(player, "No location to blink to.");
        return SkillResult.INVALID_TARGET_NO_MSG;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}