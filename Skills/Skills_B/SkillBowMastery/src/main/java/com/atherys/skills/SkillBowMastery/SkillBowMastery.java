package com.atherys.skills.SkillBowMastery;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillBowMastery extends PassiveSkill {

    public SkillBowMastery(Heroes plugin) {
        super(plugin, "BowMastery");
        setDescription("$1 damage with bow.");
        Bukkit.getPluginManager().registerEvents(new ShootArrow(this), plugin);
        setEffectTypes(EffectType.BENEFICIAL, EffectType.UNBREAKABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage", 30D);
        return node;
    }

    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, "damage", 30, false);
        return getDescription().replace("$1", damage + "");
    }

    public class ShootArrow implements Listener {
        private final Skill skill;

        public ShootArrow(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void bowshoot(EntityShootBowEvent event) {
            if (event.getProjectile().getType() != EntityType.ARROW) {
                return;
            }
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Arrow arrow = (Arrow) event.getProjectile();
            Hero hero = SkillBowMastery.this.plugin.getCharacterManager().getHero((Player) arrow.getShooter());
            if (!hero.hasEffect(this.skill.getName())) {
                return;
            }
            arrow.setMetadata("BowMasteryArrow", new FixedMetadataValue(plugin, event.getForce()));
        }


        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void osa(EntityDamageByEntityEvent event) {
            if (!(event.getEntity() instanceof LivingEntity) || (!(event.getDamager() instanceof Arrow))) {
                return;
            }
            Arrow arrow = (Arrow) event.getDamager();
            if (!(arrow.getShooter() instanceof Player) || !arrow.hasMetadata("BowMasteryArrow")) {
                return;
            }
            if (arrow.hasMetadata("ExplosiveShotFullDraw")) {
                event.setDamage(0);
                return;
            }
            Hero hero = SkillBowMastery.this.plugin.getCharacterManager().getHero((Player) arrow.getShooter());
            if (!hero.hasEffect(this.skill.getName())) {
                return;
            }
            double damage = SkillConfigManager.getUseSetting(hero, this.skill, "damage", 30, false);
            damage = (damage * arrow.getMetadata("BowMasteryArrow").get(0).asFloat());
			LivingEntity target= (LivingEntity) event.getEntity();
	        damageEntity(target, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.PROJECTILE, true);
            //Skill.damageEntity((LivingEntity) event.getEntity(), hero.getPlayer(), damage, EntityDamageEvent.DamageCause.PROJECTILE, true);
            if (((LivingEntity) event.getEntity()).getHealth() <= 0) {
                arrow.remove();
                event.setCancelled(true);
            } else {
                event.setDamage(0);
            }
        }
    }
}
