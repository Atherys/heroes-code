package com.atherys.skills.SkillBuckshot;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillBuckshot extends ActiveSkill {

    public SkillBuckshot(Heroes plugin) {
        super(plugin, "Buckshot");
        setDescription("Shoot a Arrow and jump back at he same time!");
        setUsage("/skill Buckshot");
        setArgumentRange(0, 0);
        setIdentifiers("skill Buckshot");
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.DAMAGING);
        Bukkit.getPluginManager().registerEvents(new BuckshotListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(-3));
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(25));
        node.set("charges", 2);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int shots = SkillConfigManager.getUseSetting(hero, this, "charges", 2, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        if (!hero.hasEffect("Buckshot")) {
            hero.addEffect(new BuckshotEffect(this, duration, shots,hero));
        }
        BuckshotEffect buckshotEffect = (BuckshotEffect) hero.getEffect("Buckshot");
        buckshotEffect.shoot(hero);
        Player player = hero.getPlayer();
        Location location = player.getEyeLocation();
        Vector vector = location.getDirection();
        double angle = Math.toRadians(30);
        double x, y, z;
        double r = 1;
        for (double theta = 0; theta <= 180; theta += 10) {
            double dphi = 10 / Math.sin(Math.toRadians(theta));
            for (double phi = 0; phi < 360; phi += dphi) {
                double rphi = Math.toRadians(phi);
                double rtheta = Math.toRadians(theta);
                x = r * Math.cos(rphi) * Math.sin(rtheta);
                y = r * Math.sin(rphi) * Math.sin(rtheta);
                z = r * Math.cos(rtheta);
                Vector direction = new Vector(x, z, y);
                if (direction.angle(vector) <= angle) {
                    Arrow arrow = hero.getPlayer().launchProjectile(Arrow.class);
                    arrow.setVelocity(direction.multiply(2));
                    arrow.setMetadata("BuckshotArrow", new FixedMetadataValue(plugin, true));
                }
            }
        }
        Location playerLoc = player.getLocation();
        Vector direction = playerLoc.getDirection().normalize();
        direction.multiply(SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, -3D, false));
        playerLoc.subtract(direction);
        direction.setY(0.5);
        player.setVelocity(direction);
        return SkillResult.SKIP_POST_USAGE;
    }

    @Override
    public String getDescription(Hero hero) {
        return "Shoot a Arrow and jump back at he same time!";
    }

    public class BuckshotEffect extends ExpirableEffect {
        int shots;

        public BuckshotEffect(Skill skill, long duration, int shots,Hero caster) {
            super(skill, "Buckshot",caster.getPlayer(), duration);
            this.shots = shots;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
        }

        public void shoot(Hero hero) {
            shots--;
            if (shots == 0) {
                this.expire();

            } else {
                long time = System.currentTimeMillis();
                long miniCooldown = 1000;
                if (hero.getCooldown("Buckshot") == null || hero.getCooldown("Buckshot") < time + miniCooldown) {
                    hero.setCooldown("Buckshot", time + miniCooldown);
                }
                Messaging.send(hero.getPlayer(), "You have $1 shot left in your $2!", shots+"", "Buckshot");
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "$1 expired!", "Buckshot");
            long time = System.currentTimeMillis();
            long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN, 25000, false);
            if (hero.getCooldown("Buckshot") == null || hero.getCooldown("Buckshot") < time + cooldown) {
                hero.setCooldown("Buckshot", time + cooldown);
            }
        }
    }

    public class BuckshotListener implements Listener {
        private final Skill skill;

        public BuckshotListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onBuckShotDamage(EntityDamageByEntityEvent event) {
            if (!(event.getEntity() instanceof LivingEntity) || (!(event.getDamager() instanceof Arrow))) {
                return;
            }
            Arrow arrow = (Arrow) event.getDamager();
            if (!(arrow.getShooter() instanceof Player) || !arrow.hasMetadata("BuckshotArrow")) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) arrow.getShooter());
            double damage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE, 25D, false);
            skill.damageEntity((LivingEntity) event.getEntity(), hero.getPlayer(), damage, EntityDamageEvent.DamageCause.PROJECTILE, true);
            if (((LivingEntity) event.getEntity()).getHealth() <= 0) {
                arrow.remove();
                event.setCancelled(true);
            } else {
                event.setDamage(0);
            }
        }

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onProjectileHit(ProjectileHitEvent event) {
            if (!(event.getEntity() instanceof Arrow) || !event.getEntity().hasMetadata("BuckshotArrow")) {
                return;
            }
            event.getEntity().remove();
        }
    }
}