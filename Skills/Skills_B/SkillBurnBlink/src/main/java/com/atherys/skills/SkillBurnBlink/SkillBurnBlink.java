package com.atherys.skills.SkillBurnBlink;


import com.atherys.effects.BleedPeriodicDamageEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillBurnBlink extends ActiveSkill {
    public SkillBurnBlink(Heroes plugin) {
        super(plugin, "BurnBlink");
        setDescription("Active\nTeleports you to targeted location and deals magic damage over time to nearby enemies.");
        setUsage("/skill BurnBlink");
        setArgumentRange(0, 0);
        setIdentifiers("skill burnblink");
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(6));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(20));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(20));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(1000));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(5));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 20, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        damageEffect dEffect = new damageEffect(this, period, duration, damage, player);
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if ((e instanceof Player)) {
                Player tplayer = (Player) e;
                if (damageCheck(tplayer, (LivingEntity) player)) {
                    plugin.getCharacterManager().getHero(tplayer).addEffect(dEffect);
                }
            }
        }
        Block block = Lib.getTargetBlock(player, distance);
        if (block != null) {
            Location teleport;
            if (Util.transparentBlocks.contains(block.getRelative(BlockFace.UP).getType())) {
                teleport = block.getLocation().clone();
            } else {
                teleport = block.getRelative(BlockFace.DOWN).getLocation().clone();
            }
            if (!player.getLocation().getBlock().equals(teleport.getBlock()) && !player.getLocation().getBlock().getRelative(BlockFace.UP).equals(teleport.getBlock())) {
                teleport.add(0.5, 0, 0.5);
                teleport.setPitch(player.getLocation().getPitch());
                teleport.setYaw(player.getLocation().getYaw());
                player.teleport(teleport);
                broadcastExecuteText(hero);
                return SkillResult.NORMAL;
            }
        }
        Messaging.send(player, "No location to blink to.");
        return SkillResult.INVALID_TARGET_NO_MSG;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class damageEffect extends BleedPeriodicDamageEffect {
        public damageEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "burnblink",applier, period, duration, tickDamage);
            types.add(EffectType.MAGIC);
            types.add(EffectType.BLEED);
            types.add(EffectType.FIRE);
            types.add(EffectType.HARMFUL);
            types.add(EffectType.DISPELLABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }
    }
}
