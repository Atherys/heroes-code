package com.atherys.skills.SkillBurningPresence;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillBurningPresence extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillBurningPresence(Heroes plugin) {
        super(plugin, "BurningPresence");
        setDescription("Toggle\n deal $1 magic damage to all targets $2 blocks around self.");
        setUsage("/skill burningpresence");
        setArgumentRange(0, 0);
        setIdentifiers("skill burningpresence", "skill burnpres");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE, SkillType.AREA_OF_EFFECT, SkillType.BUFFING);
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1.0, false);
        damage = damage > 0 ? damage : 0;
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        radius = radius > 1 ? radius : 1;
        String description = getDescription().replace("$1", damage + "").replace("$2", radius + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("on-text", "%hero% heats the air with their %skill%!");
        node.set("off-text", "%hero% stops their %skill%!");
        node.set("tick-damage", 1);
        node.set(SkillSetting.PERIOD.node(), 5000);
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, "on-text", "%hero% heats the air with their %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, "off-text", "%hero% stops their %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String args[]) {
        if (hero.hasEffect("BurningPresence")) {
            hero.removeEffect(hero.getEffect("BurningPresence"));
        } else {
            long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 5000, false);
            double tickDamage = (int) SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1.0, false);
            tickDamage = tickDamage > 0 ? tickDamage : 0;
            double range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 1.0, false);
            range = range > 1 ? range : 1;
            hero.addEffect(new BurningPresenceEffect(this, period, tickDamage, range));
        }
        return SkillResult.NORMAL;
    }

    public class BurningPresenceEffect extends PeriodicEffect {
        private double tickDamage;
        private double range;

        public BurningPresenceEffect(SkillBurningPresence skill, long period, double tickDamage, double range) {
            super(skill, "BurningPresence", period);
            this.tickDamage = tickDamage;
            this.range = range;
            this.types.add(EffectType.UNBREAKABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
            addMobEffect(12, Integer.MAX_VALUE, 0, false);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "BurningPresence");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "BurningPresence");
        }

        @Override
        public void tickHero(Hero hero) {
            super.tickHero(hero);

            Player player = hero.getPlayer();
            for (Entity entity : player.getNearbyEntities(range, range, range)) {
                if (entity instanceof LivingEntity) {
                    LivingEntity lEntity = (LivingEntity) entity;
                    damageEntity(lEntity, player, tickDamage, DamageCause.MAGIC);
                    addSpellTarget(lEntity, hero);
                }
            }
            player.getLocation().getWorld().spawnParticle(Particle.SMOKE_NORMAL, 10, 1, 0.5f, 1);
        }
    }
}
