package com.atherys.skills.SkillCauterize;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Particle;

public class SkillCauterize extends ActiveSkill {
    public SkillCauterize(Heroes plugin) {
        super(plugin, "Cauterize");
        setDescription("Active\n Forces your own blood to clot, stopping bleed damage on yourself.");
        setUsage("/skill Cauterize");
        setArgumentRange(0, 0);
        setIdentifiers("skill Cauterize");
        setTypes( SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        for (Effect e : hero.getEffects()) {
	        if ((e.isType(EffectType.BLEED) || e.isType(EffectType.POISON)) || ( e.isType(EffectType.HARMFUL) && (!e.isType(EffectType.UNBREAKABLE) && !e.isType(EffectType.BENEFICIAL)))) {
	        hero.removeEffect(e);
            }
        }
        hero.getPlayer().getLocation().getWorld().spawnParticle(Particle.FLAME, hero.getPlayer().getLocation(), 5, 0.5f, 0.5f, 0.5f, 1 );
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}

