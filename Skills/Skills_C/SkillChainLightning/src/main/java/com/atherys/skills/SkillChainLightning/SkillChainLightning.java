package com.atherys.skills.SkillChainLightning;

import com.atherys.effects.SparkStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.HashSet;
import java.util.Set;

public class SkillChainLightning extends TargettedSkill {
    public SkillChainLightning(Heroes plugin) {
        super(plugin, "ChainLightning");
        setDescription("Targeted\nCalls a bolt of lightning down on the target dealing $1 magic damage,"
                + "increasing by $2 per stack of Spark");
        setUsage("/skill chainlightning");
        setArgumentRange(0, 0);
        setIdentifiers("skill chainlightning");
        setTypes(SkillType.ABILITY_PROPERTY_LIGHTNING, SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.AGGRESSIVE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(15));
        node.set("stack-damage-increase", Double.valueOf(5));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000));
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(10));
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 15, false);
        double stackIncrement = SkillConfigManager.getUseSetting(hero, this, "stack-damage-increase", 5, false);
        int stacks = 0;
        if (hero.hasEffect("SparkStackEffect")) {
            stacks = ((SparkStackEffect) hero.getEffect("SparkStackEffect")).getStacks();
            damage += stacks * stackIncrement;
            Messaging.send(player, "Your ChainLightning was enhanced by your " + stacks + (stacks == 1 ? " spark stack." : " spark stacks."));
        }
        if (stacks > 0) {
            long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
            double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
            hero.addEffect(new ChainLightningEffect(this, (long) (stacks * 1000), period, damage, radius, stackIncrement, target.getLocation(), target,hero));
        }
        else {
            target.getWorld().strikeLightningEffect(target.getLocation());
            damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
            addSpellTarget(target, hero);
        }
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class ChainLightningEffect extends PeriodicExpirableEffect {
        private double damage;
        private final double radius;
        private Set<LivingEntity> alreadyHit;
        private final double stackDecrement;
        private Location location;
        private LivingEntity initialTarget;
        private boolean first;

        public ChainLightningEffect(Skill skill, long duration, long period, double damage, double radius, double stackDecrement, Location location, LivingEntity initialTarget,Hero caster) {
            super(skill, "ChainLightningEffect",caster.getPlayer(), period, duration);
            this.damage = damage;
            this.radius = radius;
            this.alreadyHit = new HashSet<>();
            this.stackDecrement = stackDecrement;
            this.location = location;
            this.initialTarget = initialTarget;
            this.first = true;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.LIGHTNING);
        }

        @Override
        public void tickMonster(Monster monster) {
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            LivingEntity target = null;
            if (first) {
                target = initialTarget;
            } else {
                for (Entity e : location.getWorld().getNearbyEntities(location, radius, radius, radius)) {
                    if (e instanceof LivingEntity) {
                        if (damageCheck(player, (LivingEntity) e) && !alreadyHit.contains(e)) {
                            if (target == null || target.getLocation().distanceSquared(location) > e.getLocation().distanceSquared(location)) {
                                target = (LivingEntity) e;
                            }
                        }
                    }
                }
            }
            if (target != null) {
                if (!first) {
                    damage -= stackDecrement;
                }
                first = false;
                double tempDamage = damage;
                if (target instanceof Player) {
                    Hero tHero = plugin.getCharacterManager().getHero((Player) target);
                    if (tHero.hasEffect("Grounded") && target.isOnGround()) {
                        tempDamage *= (1 - SkillConfigManager.getUseSetting(tHero, plugin.getSkillManager().getSkill("Grounded"), "damage-reduction", 0.5, false));
                    }
                }
                target.getWorld().strikeLightningEffect(target.getLocation());
                damageEntity(target, player, tempDamage, EntityDamageEvent.DamageCause.MAGIC);
                addSpellTarget(target, hero);
                alreadyHit.add(target);
                location = target.getLocation();
            }
        }
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}