package com.atherys.skills.SkillChargeLeader;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillChargeLeader extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillChargeLeader(Heroes plugin) {
        super(plugin, "ChargeLeader");
        setDescription("Targeted\n For $1 seconds, allies in a radius of $2 target gain movement speed for $3 seconds, every $4 seconds");
        setArgumentRange(0, 1);
        setIdentifiers("skill chargeleader");
        setUsage("/skill chargeleader");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING, SkillType.MOVEMENT_INCREASING);
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false) / 1000;
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000, false) / 1000;
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        int SpeedDur = SkillConfigManager.getUseSetting(hero, this, "speed-duration", 5000, false) / 1000;
        String description = getDescription().replace("$1", duration + "");
        description = description.replace("$2", radius + "");
        description = description.replace("$3", SpeedDur + "");
        description = description.replace("$4", period + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(30000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(10000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(5));
        node.set("speed-multiplier", Integer.valueOf(2));
        node.set("speed-duration", Integer.valueOf(5000));
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 is leading the charge!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 is no longer leading the charge!");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player))
            return SkillResult.INVALID_TARGET;
        if (hero.hasParty()) {
            if (!(hero.getParty().isPartyMember((Player) target)))
                return SkillResult.INVALID_TARGET;
        } else if (!target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        long SpeedDur = SkillConfigManager.getUseSetting(hero, this, "speed-duration", 5000, false);
        int multiplier = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 2, false);
        if (multiplier > 20) {
            multiplier = 20;
        }
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        QuickenEffect effect = new QuickenEffect(this, "Rally", SpeedDur, multiplier,hero);
        targetHero.addEffect(new ChargeEffect(this, period, duration, radius, effect, applyText, expireText,hero));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class ChargeEffect extends PeriodicExpirableEffect {
        private final String applyText;
        private final String expireText;
        private final int radius;
        private final QuickenEffect effect;

        public ChargeEffect(Skill skill, long period, long duration, int radius, QuickenEffect effect, String applyText, String expireText,Hero caster) {
            super(skill, "ChargeEffect",caster.getPlayer(), period, duration);
            this.applyText = applyText;
            this.expireText = expireText;
            this.radius = radius;
            this.effect = effect;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.SPEED);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }

        @Override
        public void tickHero(Hero hero) {
            hero.addEffect(effect);
            for (Entity e : hero.getPlayer().getNearbyEntities(this.radius, this.radius, this.radius)) {
                if (hero.hasParty()) {
                    if (((e instanceof Player) && hero.getParty().isPartyMember((Player) e))) {
                        CharacterTemplate target = this.plugin.getCharacterManager().getCharacter((LivingEntity) e);
                        target.addEffect(this.effect);
                    }
                }
            }
        }

        @Override
        public void tickMonster(Monster mob) {
        }
    }

    public class QuickenEffect extends ExpirableEffect {

        public QuickenEffect(Skill skill, String name, long duration, int amplifier,Hero caster) {
            super(skill, name,caster.getPlayer(), duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
            this.addMobEffect(1, (int) (duration / 1000L) * 20, amplifier, false);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You gained a burst of speed!");
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You returned to normal speed!");
        }
    }
}
