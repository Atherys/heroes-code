package com.atherys.skills.SkillCommand;

import com.atherys.effects.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillCommand extends ActiveSkill {

    public SkillCommand(Heroes plugin) {
        super(plugin, "Command");
        setDescription("Active\nCommand your BattleCommander to grant Speed $1 and $2% bonus melee damage for $3s," +
                "and your MoraleOfficer to grant $4 health and $5 mana every $6s for $7s" +
                "to allies within $8 blocks of each leader.");
        setUsage("/skill command");
        setArgumentRange(0, 0);
        setIdentifiers("skill command");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new HeroesDamageListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        int speedMult = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 2, false);
        double bonusDamage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 0.3, false);
        long commanderDuration = SkillConfigManager.getUseSetting(hero, this, "commander-duration", 5000, false);

        double healthTick = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_TICK.node(), 5, false);
        int manaTick = SkillConfigManager.getUseSetting(hero, this, "mana-tick", 4, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 5, false);
        long officerDuration = SkillConfigManager.getUseSetting(hero, this, "officer-duration", 5000, false);

        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 7, false);


        String description = getDescription().replace("$1", "" + speedMult)
                .replace("$2", "" + bonusDamage * 100).replace("$3", "" + commanderDuration/1000L).replace("$4", "" + healthTick)
                .replace("$5", "" + manaTick).replace("$6", "" + period/1000L).replace("$7", "" + officerDuration/1000L).replace("$8", "" + radius);
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-multiplier", Integer.valueOf(2));
        node.set("bonus-damage", 0.3);
        node.set("commander-duration", 5000);
        node.set(SkillSetting.HEALTH_TICK.node(), Integer.valueOf(2));
        node.set("mana-tick", 2);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set("officer-duration", 5000);
        node.set(SkillSetting.RADIUS.node(), 7);
        return node;
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if(hero.hasEffect("CommanderDeployedEffect")) {
	        Hero commander =((CommanderDeployedEffect)hero.getEffect("CommanderDeployedEffect")).getCommander();
	        int range = SkillConfigManager.getUseSetting(hero, this, "Range", 20, false);
	        if (hero.getPlayer().getLocation().distanceSquared(commander.getPlayer().getLocation()) <= range * range){
		        int speedMult = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 2, false);
		        double bonusDamage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 0.3, false);
		        long commanderDuration = SkillConfigManager.getUseSetting(hero, this, "commander-duration", 5000, false);

		        if(!hero.hasParty()&&commander.equals(hero)) {
			        hero.addEffect(new QuickenEffect(this, "CommandQuicken", commanderDuration, speedMult,hero));
			        hero.addEffect(new ZealEffect(this, commanderDuration, bonusDamage, "You feel a surge of power!", "Your new strength fades.",hero));
		        } else {
			        Player pCommander = commander.getPlayer();
			        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 7, false);
			        for (Hero pHero : hero.getParty().getMembers()) {
				        Player pPlayer = pHero.getPlayer();
				        if (pPlayer.getWorld().equals(pCommander.getWorld()) && pPlayer.getLocation().distanceSquared(pCommander.getLocation()) <= radius * radius ) {
					        pHero.addEffect(new QuickenEffect(this, "CommandQuicken", commanderDuration, speedMult,hero));
					        pHero.addEffect(new ZealEffect(this, commanderDuration, bonusDamage, "You feel a surge of power!", "Your new strength fades.",hero));
				        }
			        }
			        broadcast(pCommander.getLocation(), "$1's troops are charging forward!", commander.getName());
		        }
	        }else {
		        Messaging.send(hero.getPlayer(), "Target too far away!");
		        return SkillResult.INVALID_TARGET;
	        }


        }
        if(hero.hasEffect("OfficerDeployedEffect")) {
            Hero officer =( (OfficerDeployedEffect)hero.getEffect("OfficerDeployedEffect")).getOfficer();
	        int range = SkillConfigManager.getUseSetting(hero, this, "Range", 20, false);
	        if (hero.getPlayer().getLocation().distanceSquared(officer.getPlayer().getLocation()) <= range * range){
		        double healthTick = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_TICK.node(), 5, false);
		        int manaTick = SkillConfigManager.getUseSetting(hero, this, "mana-tick", 4, false);
		        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 1000, false);
		        long officerDuration = SkillConfigManager.getUseSetting(hero, this, "officer-duration", 5000, false);

		        if(!hero.hasParty()&&officer.equals(hero)) {
			        hero.addEffect(new VitalizeEffect(this, period, officerDuration, healthTick, hero.getPlayer(), manaTick,hero));
		        } else {
			        Player pOfficer = officer.getPlayer();
			        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 7, false);
			        for (Hero pHero : hero.getParty().getMembers()) {
				        Player pPlayer = pHero.getPlayer();
				        if (pPlayer.getWorld().equals(pOfficer.getWorld()) && pPlayer.getLocation().distanceSquared(pOfficer.getLocation()) <= radius * radius ) {
					        pHero.addEffect(new VitalizeEffect(this, period, officerDuration, healthTick, pOfficer, manaTick,hero));
				        }
			        }
			        broadcast(pOfficer.getLocation(), "$1's troops are hunkering down!", officer.getName());
		        }
	        }else {
		        Messaging.send(hero.getPlayer(), "Target too far away!");
		        return SkillResult.INVALID_TARGET;
	        }

        }
        if(!hero.hasEffect("OfficerDeployedEffect")&&!hero.hasEffect("CommanderDeployedEffect")) {
            Messaging.send(hero.getPlayer(), "You have deployed neither a MoraleOfficer nor a BattleCommander");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class QuickenEffect extends ExpirableEffect {

        public QuickenEffect(Skill skill, String name, long duration, int amplifier,Hero caster) {
            super(skill, name,caster.getPlayer(), duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
            this.addMobEffect(1, (int) (duration / 1000L) * 20, amplifier, false);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }
    }

    public class HeroesDamageListener implements Listener {
        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.isCancelled()) {
                return;
            }
            if ((event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) || (event.getDamage() == 0)) {
                return;
            }
            CharacterTemplate player = event.getDamager();
            if (!player.hasEffect("Zeal")||player.hasEffect("Inflame")) {
                return;
            }
            double mult = ((ZealEffect) player.getEffect("Zeal")).getMultiplier();
            event.setDamage(event.getDamage() * (1 + mult));
        }
    }
}