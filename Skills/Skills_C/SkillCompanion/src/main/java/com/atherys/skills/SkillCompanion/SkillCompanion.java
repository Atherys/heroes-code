package com.atherys.skills.SkillCompanion;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SkillCompanion extends ActiveSkill {

    public SkillCompanion(Heroes plugin) {
        super(plugin, "Companion");
        setDescription("Toggle-able passive that spawns a companion wolf that will follow you in your battles. Use the skill again with an argument to set your companion's name.");
        setUsage("/skill companion <name>");
        setArgumentRange(0, 1);
        setIdentifiers("skill companion");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.BUFFING, SkillType.SUMMONING);
        Bukkit.getServer().getPluginManager().registerEvents(new CompanionListener(this), plugin);
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("respawn-duration", 20000);
        node.set("wolf-damage", 7.0);
        node.set("wolf-health", 100.0);
        node.set("wolf-bite-damage", 10.0);
        node.set("wolf-bite-slow-duration", 2000);
        node.set("wolf-bite-cooldown", 20000);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        if (hero.hasEffect("Companion")) {
            if (args.length == 1) {
                CompanionEffect ce = (CompanionEffect) hero.getEffect("Companion");
                ce.setCompanionName(args[0]);
                Messaging.send(player, "Your wolf is now named " + args[0] + ".");
                return SkillResult.SKIP_POST_USAGE;
            }
            if (hero.isInCombat()) {
                Messaging.send(player, "You can't dismiss your wolf in combat.");
                return SkillResult.CANCELLED;
            }
            hero.removeEffect(hero.getEffect("Companion"));
            return SkillResult.SKIP_POST_USAGE;
        } else {
            if (args.length == 1) {
                Messaging.send(player, "You don't have an active companion wolf.");
                return SkillResult.CANCELLED;
            }
            if (hero.hasEffect("CompanionRespawn")) {
                Messaging.send(player, "$1 seconds before your wolf can respawn.", ((CompanionRespawnEffect) hero.getEffect("CompanionRespawn")).getRemainingTime() / 1000);
                return SkillResult.CANCELLED;
            }
            hero.addEffect(new CompanionEffect(this));
        }
        return SkillResult.NORMAL;
    }

    public class CompanionEffect extends Effect {
        private Skill skill;
        private Wolf wolf;
        private String companionName;
        private boolean alive;

        public CompanionEffect(Skill skill) {
            super(skill, "Companion");
            this.skill = skill;
            this.types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), ChatColor.WHITE + player.getName() + ChatColor.GRAY + " summons their wolf!");
            spawnCompanion(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if (isWolfAlive() && wolf != null) {
                wolf.remove();
                broadcast(player.getLocation(), ChatColor.WHITE + player.getName() + ChatColor.GRAY + " dismisses their wolf!");
            } else {
                Messaging.send(player, "Your wolf will no longer respawn.");
            }
        }

        public Wolf getWolf() {
            return wolf;
        }

        public void setWolfAlive(Hero hero, boolean alive) {
            this.alive = alive;
            if (!alive) {
                if (wolf != null) {
                    wolf.remove();
                }
                hero.addEffect(new CompanionRespawnEffect(skill, SkillConfigManager.getUseSetting(hero, skill, "respawn-duration", 20000, false),hero));
            }
        }

        public boolean isWolfAlive() {
            return alive;
        }

        public void setCompanionName(String name) {
            companionName = name;
            if (isWolfAlive()) {
                wolf.setCustomName(ChatColor.translateAlternateColorCodes('&', companionName));
            }
        }

        public String getCompanionName() {
            return companionName;
        }

        public void spawnCompanion(Hero hero) {
            Player player = hero.getPlayer();
            wolf = (Wolf) player.getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
            plugin.getCharacterManager().getMonster(wolf).setExperience(0);
            setWolfAlive(hero, true);
            wolf.setOwner(player);
            wolf.setBreed(false);
            double health = SkillConfigManager.getUseSetting(hero, skill, "wolf-health", 100.0, false);
            wolf.setMaxHealth(health);
            wolf.setHealth(health);
            wolf.setCollarColor(DyeColor.WHITE);
            if (companionName == null || companionName.isEmpty()) {
                wolf.setCustomName(ChatColor.AQUA + player.getName() + "'s Companion");
            } else {
                wolf.setCustomName(ChatColor.translateAlternateColorCodes('&', companionName));
            }
            wolf.setCustomNameVisible(true);

            wolf.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 864000, 1, false, false));
        }
    }

    public class CompanionRespawnEffect extends ExpirableEffect {

        public CompanionRespawnEffect(Skill skill, long duration,Hero caster) {
            super(skill, "CompanionRespawn",caster.getPlayer(), duration);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, ChatColor.WHITE + "" + (SkillConfigManager.getUseSetting(hero, skill, "respawn-duration", 20000, false)/1000)  + ChatColor.GRAY + " seconds until your wolf respawns.");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            if (hero.hasEffect("Companion")) {
                CompanionEffect ce = (CompanionEffect) hero.getEffect("Companion");
                if (!ce.isWolfAlive()) {
                    ce.spawnCompanion(hero);
                    broadcast(player.getLocation(), ChatColor.WHITE + player.getName() + ChatColor.GRAY + "'s wolf respawned!");
                }
            }
        }
    }

    public class CompanionListener implements Listener {
        private Skill skill;

        public CompanionListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onWolfDeath(EntityDeathEvent event) {
            if (!(event.getEntity() instanceof Wolf)) {
                return;
            }
            Wolf wolf = (Wolf) event.getEntity();
            if (wolf.isTamed()) {
                if (wolf.getOwner() instanceof Player) {
                    Player player = (Player) wolf.getOwner();
                    Hero hero = plugin.getCharacterManager().getHero(player);
                    if (!hero.hasEffect("CompanionRespawn") && hero.hasEffect("Companion")) {
                        CompanionEffect ce = (CompanionEffect) hero.getEffect("Companion");
                        if (wolf.equals(ce.getWolf())) {
                            if (ce.isWolfAlive()) {
                                ce.setWolfAlive(hero, false);
                            }
                        }
                    }
                }
            }
        }

        @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onOwnerMeleeDamage(WeaponDamageEvent event) {
            if (event.getDamage() == 0 || !(event.getEntity() instanceof LivingEntity)) {
                return;
            }
            if (event.getDamager() instanceof Hero) {
                Hero hero = (Hero)event.getDamager();
                if (!hero.hasEffect("CompanionRespawn") && hero.hasEffect("Companion")) {
                    Wolf wolf = ((CompanionEffect)hero.getEffect("Companion")).getWolf();
                    if (!wolf.equals(event.getEntity()) && (wolf.getTarget() == null || !wolf.getTarget().equals(event.getEntity()))) {
                        wolf.setTarget((LivingEntity)event.getEntity());
                    }
                }
            }
            /*if (event.getEntity() instanceof Player) {
                Hero hero = plugin.getCharacterManager().getHero((Player)event.getEntity());
                if (!hero.hasEffect("CompanionRespawn") && hero.hasEffect("Companion")) {
                    Wolf wolf = ((CompanionEffect)hero.getEffect("Companion")).getWolf();
                    if (wolf.getTarget() != null && !wolf.getTarget().equals(event.getDamager().getEntity())) {
                        wolf.setTarget(event.getDamager().getEntity());
                    }
                }
            }*/
        }

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onOwnerRangedDamage(EntityDamageByEntityEvent event) {
            if (!(event.getEntity() instanceof LivingEntity) || !(event.getDamager() instanceof Arrow)) {
                return;
            }
            LivingEntity target = (LivingEntity) event.getEntity();
            Arrow arrow = (Arrow) event.getDamager();
            if (!(arrow.getShooter() instanceof Player)) {
                return;
            }
            Player player = (Player) arrow.getShooter();
            if (player.equals(target)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!hero.hasEffect("CompanionRespawn") && hero.hasEffect("Companion")) {
                Wolf wolf = ((CompanionEffect)hero.getEffect("Companion")).getWolf();
                if (!wolf.equals(target) && (wolf.getTarget() == null || !wolf.getTarget().equals(target))) {
                    wolf.setTarget(target);
                }
            }
        }

        @EventHandler
        public void onWolfDamage(EntityDamageByEntityEvent event) {
            if (event.isCancelled() || event.getDamage() == 0 || !(event.getDamager() instanceof Wolf) || !(event.getEntity() instanceof LivingEntity)) {
                return;
            }
            Wolf wolf = (Wolf)event.getDamager();
            if (wolf.isTamed()) {
                if (wolf.getOwner() instanceof Player) {
                    Hero hero = plugin.getCharacterManager().getHero((Player)wolf.getOwner());
                    if (!hero.hasEffect("CompanionRespawn") && hero.hasEffect("Companion")) {
                        if (wolf.equals(((CompanionEffect)hero.getEffect("Companion")).getWolf())) {
                            if (damageCheck(hero.getPlayer(), (LivingEntity)event.getEntity())) {
                                event.setCancelled(true);
                                double damage = SkillConfigManager.getUseSetting(hero, skill, "wolf-damage", 7.0, false);
                                if (hero.getCooldown("CompanionBite") == null || hero.getCooldown("CompanionBite") <= System.currentTimeMillis()) {
                                    CharacterTemplate target = plugin.getCharacterManager().getCharacter((LivingEntity) event.getEntity());
                                    damage += SkillConfigManager.getUseSetting(hero, skill, "wolf-bite-damage", 10.0, false);
                                    long duration = SkillConfigManager.getUseSetting(hero, skill, "wolf-bite-slow-duration", 2000, false);
                                    int cooldown = SkillConfigManager.getUseSetting(hero, skill, "wolf-bite-cooldown", 20000, false);
                                    target.addEffect(new SlowEffect(skill, "CompanionBite", duration, 1, false, "", "", hero));
                                    addSpellTarget(event.getEntity(), hero);
                                    broadcast(wolf.getLocation(), ChatColor.WHITE + hero.getPlayer().getName() + "'s Companion" + ChatColor.GRAY +" used " + ChatColor.WHITE + "Bite" + ChatColor.GRAY +" on " + ChatColor.WHITE + target.getName() + ChatColor.GRAY + "!");
                                    hero.setCooldown("CompanionBite", cooldown + System.currentTimeMillis());
                                }
                                if (event.getEntity() instanceof Player){
                                    damageEntity((LivingEntity) event.getEntity(), hero.getPlayer(), damage, EntityDamageEvent.DamageCause.ENTITY_ATTACK, false);
                                }
                            } else {
                                event.setDamage(0);
                                event.setCancelled(true);
                                wolf.setTarget(null);
                            }
                        }
                    }
                }
            }
        }

        @EventHandler (priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onWolfTarget(EntityTargetLivingEntityEvent event) {
            if (!event.getEntityType().equals(EntityType.WOLF) || (!(event.getTarget() instanceof Player))) {
                return;
            }
            Hero target = plugin.getCharacterManager().getHero((Player)event.getTarget());
            if (target.hasEffect("VanishEff") || target.hasEffect("Invisible")) {
                event.setCancelled(true);
            }
        }
    }
}
