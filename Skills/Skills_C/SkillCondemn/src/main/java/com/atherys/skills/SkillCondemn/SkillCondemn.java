package com.atherys.skills.SkillCondemn;

import com.atherys.effects.TenacityEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;

public class SkillCondemn extends TargettedSkill {
    private String applytext;
    private String expiretext;

    public SkillCondemn(Heroes plugin) {
        super(plugin, "Condemn");
        setDescription("Target\n Target takes $1 physical damage every $2s for $3s and cannot use skills, Potions, nor Weapons.");
        setUsage("/skill Condemn");
        setArgumentRange(0, 0);
        setIdentifiers("skill Condemn");
        setTypes(SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new DamageCancelListener(), plugin);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        Player target = (Player) le;
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        if (hero.hasParty()){
            if (hero.getParty().getMembers().contains(target)){
                return SkillResult.INVALID_TARGET_NO_MSG;
            }
        }else {
            Hero thero = plugin.getCharacterManager().getHero(target);
            long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
            long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000L, false);
            double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
            thero.addEffect(new CondemnEffect(this, period, duration, damage, hero.getPlayer()));
            hero.addEffect(new TenacityEffect(this, duration, true, hero));
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
        return SkillResult.FAIL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% is %skill%ed!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% expired from %target%!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000L, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "").replace("$2", period / 1000 + "").replace("$3", duration / 1000 + "") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target%  %skill%").replace("%target%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%skill% expired from %target%").replace("%target%", "$1").replace("%skill%", "$2");
    }

    public class CondemnEffect extends PeriodicDamageEffect {

        public CondemnEffect(Skill skill, long period, long duration, double damage, Player caster) {
            super(skill, "CondemnEffect",caster, period, duration, damage );
            types.add(EffectType.HARMFUL);
            types.add(EffectType.PHYSICAL);
            types.add(EffectType.DISPELLABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Condemn");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Condemn");
        }
    }

    public class DamageCancelListener implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onEntityDamage(WeaponDamageEvent event) {
            if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Hero) || (event.getDamage() == 0)) {
                return;
            }
            if (event.getDamager().hasEffect("CondemnEffect")) {
                event.setCancelled(true);
                Messaging.send(((Hero)event.getDamager()).getPlayer(), "You cannot damage that target while Condemned!");
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onShootBow(EntityShootBowEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            if (plugin.getCharacterManager().getHero((Player)event.getEntity()).hasEffect("CondemnEffect")) {
                event.setCancelled(true);
                Messaging.send(((Player) event.getEntity()).getPlayer(), "You cannot shoot your bow while Condemned!");
            }
        }
    }
}
