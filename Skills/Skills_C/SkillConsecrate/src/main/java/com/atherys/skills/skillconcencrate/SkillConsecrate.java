package com.atherys.skills.skillconcencrate;

import com.atherys.heroesaddon.util.GeometryUtil;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.*;

import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

public class SkillConsecrate extends ActiveSkill implements Listener
{
    public SkillConsecrate(Heroes plugin)
    {
        super(plugin, "Consecrate");
        setDescription("You consecrate the earth at your target location (within $1 blocks), infusing it with holy power. Allies within $2 blocks of this location "
                + "take $3% less magic damage and are infused with sacred strength, bolstering their melee damage by $4%. Consecration lasts for $5 seconds.");
        setUsage("/skill consecrate");
        setArgumentRange(0, 0);
        setIdentifiers("skill consecrate");
        setTypes(SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public String getDescription(Hero hero)
    {
        int range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 15, true);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 6, true);
        double magicReduce = SkillConfigManager.getUseSetting(hero, this, "magic-damage-reduction", 0.8, true);
        String magicReducePerc = String.valueOf(magicReduce * 100);
        double meleeBoost = SkillConfigManager.getUseSetting(hero, this, "melee-damage-increase", 0.5, true);
        String meleeBoostPerc = String.valueOf(meleeBoost * 100);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, true);
        String formattedDuration = String.valueOf(new BigDecimal(duration / 1000).doubleValue());
        return getDescription().replace("$1", range + "")
                .replace("$2", radius + "")
                .replace("$3", magicReducePerc)
                .replace("$4", meleeBoostPerc)
                .replace("$5", formattedDuration);
    }

    public ConfigurationSection getDefaultConfig()
    {
        ConfigurationSection node = super.getDefaultConfig();

        node.set(SkillSetting.MAX_DISTANCE.node(), 15);
        node.set(SkillSetting.RADIUS.node(), 6);
        node.set("magic-damage-reduction", 0.8);
        node.set("melee-damage-increase", 0.5);
        node.set(SkillSetting.DURATION.node(), 6000);

        return node;
    }

    public SkillResult use(Hero hero, String[] args)
    {
        final Player player = hero.getPlayer();
        final Skill skill = this;

        int range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 15, true);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 6, true);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, true);

        Block targetBlock = player.getTargetBlock((Set<Material>) null, (int) range);

        if (targetBlock.getType() == Material.LONG_GRASS) targetBlock = targetBlock.getLocation().subtract(0, 1, 0).getBlock();
        else if (!targetBlock.getType().isSolid()) {
            player.sendMessage("You must target the ground!");
            return SkillResult.FAIL;
        }
        Location targetLoc = targetBlock.getLocation().clone().add(0.5, 1, 0.5);

        Vector dir = targetLoc.toVector().subtract(player.getEyeLocation().toVector()).divide(new Vector(15, 15, 15));
        double dist = player.getEyeLocation().distance(targetLoc);
        Location to = player.getEyeLocation().clone();
        double traveled = 0.0D;
        while (traveled < Math.abs(dist))
        {
            to.add(dir);
            to.getWorld().spigot().playEffect(to, Effect.INSTANT_SPELL, 0, 0, 0.05F, 0.05F, 0.05F, 0.0F, 3, 128);
            traveled = Math.abs(to.distance(player.getEyeLocation()));
        }
        targetLoc.getWorld().spigot().playEffect(targetLoc.clone().add(0, 0.1, 0), Effect.INSTANT_SPELL, 0, 0, 2.0F, 0.3F, 2.0F, 0.1F, 25, 128);

        targetLoc.getWorld().playSound(targetLoc, Sound.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 1.0F, 1.0F);
        targetLoc.getWorld().playSound(targetLoc, Sound.ENTITY_LIGHTNING_THUNDER, 1.0F, 2.0F);
        boolean chicken = (new Random().nextInt(100) == 0);
        if (chicken) targetLoc.getWorld().playSound(targetLoc, Sound.ENTITY_CHICKEN_HURT, 1.0F, 1.0F); // an easter egg I'm sure you guys will immediately pick up, it's a 1% chance

        final int fxTickDuration = (int) (duration / 1000) * 20;
        ArrayList<Location> circle = GeometryUtil.circle(targetLoc.clone().add(0, 0.2, 0), 30, radius); // hooray for only having to call this once since it's an immobile GTAoE

        // circle
        for (int startIndex = 0; startIndex < 31; startIndex += (startIndex == 0 ? 7 : 8)) // this is pointlessly complicated thanks to indices, I am testing this
        {
            final int si = startIndex;
            new BukkitRunnable() {
                int index = si;
                int ticks = 0;

                public void run() {
                    Location l = circle.get(index);
                    l.getWorld().spigot().playEffect(l, Effect.COLOURED_DUST, 0, 0, 1.0F, 1.0F, 0.0F, 1.0F, 0, 128); // let's see if this actually works, it works 100% for redstone dust to change its colour
                    index++;
                    if (index == circle.size() - 1) index = 0;
                    ticks++;
                    if (ticks == fxTickDuration) cancel();
                }
            }.runTaskTimer(plugin, 0, 1); // from what I have seen this is not incredibly memory intensive, it simply consists of pulling a location from a pregenerated list and playing an effect there.
        }

        ArrayList<Location> filledCircle = GeometryUtil.filledCircle(targetLoc.clone().add(0, 0.2, 0), 15, radius - 0.5D); // normally wouldn't use something as mathematical and would instead use offsets, but as I can only call a single effect at a time and can't use offsets, I'm using this. Plus it guarantees nothing gets played outside the radius.

        // This is both a visual and a method of calculating the actual players nearby and applying the appropriate effect.
        new BukkitRunnable() {
            int ticks = 0;
            Random rand = new Random();

            public void run() {
                Location testLoc = targetLoc.subtract(0, 0.5, 0);
                Snowball test = (Snowball) testLoc.getWorld().spawnEntity(testLoc, EntityType.SNOWBALL);
                // I tried to hide the snowball inside the block while not detracting from the radius, but if for whatever reason it's still visible from time to time, uncomment the below code.
                /*for (Player p : Bukkit.getServer().getOnlinePlayers())
                {
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(((CraftSnowball) test).getHandle().getId()));
                }*/
                for (Entity e : test.getNearbyEntities(radius, radius, radius)) {
                    if (!(e instanceof Player)) continue;
                    Player p = (Player) e;
                    Hero h = plugin.getCharacterManager().getHero(p);
                    if (h.equals(hero)) {
                            hero.addEffect(new ConsecratedEffect(skill, player, 300)); // this way someone running through it at the end won't receive 6s of resistances
                    }
                    else if (hero.hasParty() && hero.getParty().getMembers().contains(h)) {
                         h.addEffect(new ConsecratedEffect(skill, player, 300));
                    }
                }

                // visuals below here
                for (int i = 0; i < 4; i++) {
                    Location l = filledCircle.get(rand.nextInt(filledCircle.size())); // rand will never call the int you specify, but will include 0, making it perfect for picking an element at random from a list
                    l.getWorld().spigot().playEffect(l, Effect.INSTANT_SPELL, 0, 0, 1.0F, 0.2F, 1.0F, 0.0F, 4, 128); // way too memory hoggy but idc rn
                }
                ticks++;
                if (ticks == fxTickDuration / 5) {
                    targetLoc.getWorld().playSound(targetLoc, Sound.ENTITY_ZOMBIE_INFECT, 1.0F, 1.0F);
                    cancel(); // as this will only run 5 times a second instead of constantly updating, remember to cut down the max ticks - I fail to do this sometimes and the runnable goes on for 10 minutes or until I reload.
                }
            }
        }.runTaskTimer(plugin, 0, 4);

        broadcast(player.getLocation(), ChatColor.WHITE + "$1" + ChatColor.GRAY + " consecrates the earth!", hero.getName());
        return SkillResult.NORMAL;
    }

    @EventHandler(priority = EventPriority.HIGHEST) // this way the resistance will factor in before any other effects trigger
    public void reduceMagicDamage(EntityDamageEvent e)
    {
        if (e.getCause() != EntityDamageEvent.DamageCause.MAGIC || !(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        Hero hero = plugin.getCharacterManager().getHero(player);
        if (!hero.hasEffect("Consecrated")) return;
        Hero applierHero = plugin.getCharacterManager().getHero(((ConsecratedEffect) hero.getEffect("Consecrated")).getApplier());
        double magicReduce = SkillConfigManager.getUseSetting(applierHero, this, "magic-damage-reduction", 0.8, true);
        e.setDamage(e.getFinalDamage() - (e.getFinalDamage() * magicReduce));
    }

    @EventHandler(priority = EventPriority.HIGH) // change if necessary
    public void boostMeleeDamage(WeaponDamageEvent e)
    {
        if (!(e.getDamager() instanceof Hero)) return;
        Hero attacker = (Hero) e.getDamager();
        if (!attacker.hasEffect("Consecrated")) return;
        Hero applierHero = plugin.getCharacterManager().getHero(((ConsecratedEffect) attacker.getEffect("Consecrated")).getApplier());
        double meleeBoost = SkillConfigManager.getUseSetting(applierHero, this, "melee-damage-increase", 0.5, true);
        e.setDamage(e.getDamage() + (e.getDamage() * meleeBoost));
    }

    public class ConsecratedEffect extends ExpirableEffect
    {
        public ConsecratedEffect(Skill skill, Player applier, long duration)
        {
            super(skill, "Consecrated", applier, duration);
        }

        public void applyToHero(Hero hero)
        {
            final Player player = hero.getPlayer();
            ArrayList<Location> baseCirc = GeometryUtil.circle(player.getLocation().add(0, 0.1, 0), 7, 0.3);
            ArrayList<Location> midCirc = GeometryUtil.circle(player.getLocation().add(0, 1.6, 0), 7, 0.45);
            ArrayList<Location> topCirc = GeometryUtil.circle(player.getLocation().add(0, 2.1, 0), 7, 0.3);
            for (Location l : baseCirc)
            {
                l.getWorld().spigot().playEffect(l, Effect.WATERDRIP, 0, 0, 0.0F, 0.0F, 0.0F, 0.0F, 1, 128);
            }
            for (Location l : midCirc)
            {
                l.getWorld().spigot().playEffect(l, Effect.WATERDRIP, 0, 0, 0.0F, 0.0F, 0.0F, 0.0F, 1, 128);
            }
            for (Location l : topCirc)
            {
                l.getWorld().spigot().playEffect(l, Effect.WATERDRIP, 0, 0, 0.0F, 0.0F, 0.0F, 0.0F, 1, 128);
            }
        }
    }
}