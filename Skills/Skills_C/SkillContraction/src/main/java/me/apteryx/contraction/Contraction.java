package me.apteryx.contraction;

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * @author apteryx
 * @time 2:02 PM
 * @since 11/14/2016
 */
public class Contraction extends TargettedSkill {

    private float healthCost, lifeGain;

    public Contraction(Heroes plugin) {
        super(plugin, "Contraction");
        this.setTypes(SkillType.TELEPORTING, SkillType.SILENCEABLE);
        this.setUsage("/skill contraction");
        this.setIdentifiers("skill contraction");
        this.setDescription("Pay %health-cost% health to teleport to target ally within %max-distance% blocks and restore %life-gain% health to them.");
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription().replace("%max-distance%", SkillConfigManager.getUseSetting(hero, this, "max-distance", 25.0, false)+"").replace("%health-cost%", SkillConfigManager.getUseSetting(hero, this, "health-cost", 10.0, false)+"").replace("%life-gain%", SkillConfigManager.getUseSetting(hero, this, "life-gain", 20.0, false)+"");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("health-cost", 10.0);
        node.set("max-distance", 25.0);
        node.set("life-gain", 20.0);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity livingEntity, String[] strings) {
        healthCost = (float) SkillConfigManager.getUseSetting(hero, this, "health-cost", 10.0, false);
        lifeGain = (float) SkillConfigManager.getUseSetting(hero, this, "life-gain", 20.0, false);

        if (hero.getParty() != null) {
            if (livingEntity instanceof Player && livingEntity != hero.getEntity()) {
                Player player = (Player) livingEntity;
                if (hero.getParty().isPartyMember(player)) {
                    broadcastExecuteText(hero, player);
                    if (!(hero.getPlayer().getHealth() <= healthCost)) {
                        for (double[] coords : Lib.playerParticleCoords) {
                            Location location = hero.getPlayer().getLocation();
                            location.add(coords[0], coords[1], coords[2]);
                            hero.getPlayer().getWorld().spigot().playEffect(location, Effect.TILE_BREAK, Material.NETHER_WARTS.getId(), 0, 0, 0.0F, 0.0F, 0.0F, 100, HeroesAddon.PARTICLE_RANGE);
                        }
                        hero.getPlayer().teleport(player.getLocation().setDirection(hero.getPlayer().getLocation().getDirection()), PlayerTeleportEvent.TeleportCause.COMMAND);
                        for (double[] coords : Lib.playerParticleCoords) {
                            Location location = hero.getPlayer().getLocation();
                            location.add(coords[0], coords[1], coords[2]);
                            hero.getPlayer().getWorld().spigot().playEffect(location, Effect.TILE_BREAK, Material.NETHER_WARTS.getId(), 0, 0, 0.0F, 0.0F, 0.0F, 100, HeroesAddon.PARTICLE_RANGE);
                        }
                        //hero.getPlayer().sendMessage(player.getHealth()+"\n"+player.getMaxHealth());
                        if (!((player.getHealth() + lifeGain) > player.getMaxHealth())) {
                            player.setHealth(player.getHealth() + lifeGain);
                        }else{
                            player.setHealth(player.getMaxHealth());
                        }
                        hero.getPlayer().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_SLIME_ATTACK, 1.0F, 1.0F);
                        return SkillResult.NORMAL;
                    }
                }
            }
        }
        return SkillResult.INVALID_TARGET_NO_MSG;
    }
}
