package com.atherys.skills.SkillConvergence;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class SkillConvergence extends ActiveSkill {
    public SkillConvergence(Heroes plugin) {
        super(plugin, "Convergence");
        setDescription("Active\n Pulls nearby enemies towards you and gives them a Slowness debuff.");
        setUsage("/skill Convergence");
        setArgumentRange(0, 0);
        setIdentifiers("skill Convergence");
        setTypes(SkillType.SILENCEABLE, SkillType.DEBUFFING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.DURATION.node(), 4000);
        node.set("vertical-vector", Double.valueOf(0.5));
        node.set("horizontal-vector", Double.valueOf(1.0));
        node.set("slow-amplifier", 3);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000, false);
        int amplifier = SkillConfigManager.getUseSetting(hero, this, "slow-amplifier", 2, false);
        double v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 1.0, false);
        double v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 0.5, false);

        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if (e instanceof Player) {
                Player le = (Player)e;
                if (damageCheck(player, (LivingEntity) le)) {
                    addSpellTarget(le, hero);
                    Location to = player.getLocation();
                    Location from = le.getLocation();
                    Vector vTo = new Vector(to.getX(), to.getY(), to.getZ());
                    Vector vFrom = new Vector(from.getX(), from.getY(), from.getZ());
                    Vector v = vTo.subtract(vFrom).normalize();
                    v.setY(v2);
                    v.multiply(v1);
                    le.setVelocity(v);
                    CharacterTemplate target = plugin.getCharacterManager().getCharacter(le);
                    target.addEffect(new SlowEffect(this, "Convergence", duration, amplifier, false, "", "", hero));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}

