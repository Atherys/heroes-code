package com.atherys.skills.SkillCorruption;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

public class SkillCorruption extends ActiveSkill {
    private String applyText;
    private String expireText;
    private String missText;
    private String negateText;

    public SkillCorruption(Heroes plugin) {
        super(plugin, "Corruption");
        setDescription("Active\nLaunch a projectile, dealing $1 damage and turning healing into damage for $2s. $3 damage cap, blocking heals beyond it.");
        setUsage("/skill Corruption");
        setArgumentRange(0, 0);
        setIdentifiers("skill Corruption");
        setTypes(SkillType.ABILITY_PROPERTY_DARK, SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.DEBUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(), plugin);
        Bukkit.getServer().getPluginManager().registerEvents(new CorruptionListener(this), plugin);
    }

    public void init() {
        super.init();
        applyText = SkillConfigManager.getUseSetting(null, this, SkillSetting.APPLY_TEXT.node(), "%target% has been cursed!").replace("%target%", "$1");
        missText = SkillConfigManager.getUseSetting(null, this, "miss-text", "%target%s heal was inverted!").replace("%target%", "$1");
        negateText = SkillConfigManager.getUseSetting(null, this, "negate-text", "%target%s heal was blocked!").replace("%target%", "$1");
        expireText = SkillConfigManager.getUseSetting(null, this, SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the curse!").replace("%target%", "$1");

    }

    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5D, false);
        double duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        double cap = SkillConfigManager.getUseSetting(hero, this, "damage-cap", 50D, false);
        return getDescription().replace("$1", damage + "").replace("$2", (duration/1000) + "").replace("$3", cap + "") + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(2));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(5000));
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(5));
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% has been cursed!");
        node.set("damage-cap", Double.valueOf(50));
        node.set("miss-text", "%target%s heal was inverted!");
        node.set("negate-text", "%target%s heal was blocked!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the curse!");
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Potion potionItem = new Potion(PotionType.WATER, 1);
        potionItem.splash();
        ItemStack item = potionItem.toItemStack(1);
        ThrownPotion potion = player.launchProjectile(ThrownPotion.class);
        potion.setItem(item);
        //potion.getEffects().clear();
        potion.setMetadata("Corruption", new FixedMetadataValue(plugin, true));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class CorruptionEffect extends ExpirableEffect {
        private Player caster;
        private double cap;
        private double damageInverted;

        public CorruptionEffect(Skill skill, long duration, Player caster) {
            super(skill, "Corruption",caster, duration);
            this.caster = caster;
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.WITHER);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(20, tickDuration, 1, false);
            Hero cHero = plugin.getCharacterManager().getHero(caster);
            this.cap = SkillConfigManager.getUseSetting(cHero, skill, "damage-cap", 50D, false);
            this.damageInverted = 0;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName());
            hero.getPlayer().removePotionEffect(PotionEffectType.WITHER);
        }

        public Player getCaster() {
            return caster;
        }

        public double getDamage() {
            return damageInverted;
        }

        public void setDamage(double value) {
            damageInverted = value;
        }

        public double getCap() {
            return cap;
        }
    }

    public class SkillEntityListener implements Listener {
        @EventHandler (ignoreCancelled = true)
        public void onEntityRegainHeroHealth(HeroRegainHealthEvent event) {
            Hero hero = event.getHero();
            if (hero.hasEffect("Corruption")) {
                if (hero.getPlayer().hasPotionEffect(PotionEffectType.REGENERATION)) {
                    hero.getPlayer().removePotionEffect(PotionEffectType.REGENERATION);
                }
                double damage = event.getDelta();
                event.setDelta(0d);
                CorruptionEffect corruptionEffect = (CorruptionEffect) hero.getEffect("Corruption");
                double damageInverted = corruptionEffect.getDamage();
                double cap = corruptionEffect.getCap();
                if(damageInverted < cap) {
                    damageInverted += damage;
                    corruptionEffect.setDamage(damageInverted);
                    if(damageInverted > cap) {
                        double overflow =   damageInverted - cap;
                        double damageInflicted = damage - overflow;
                        addSpellTarget(hero.getPlayer(), hero);
                        damageEntity(hero.getPlayer(), corruptionEffect.getCaster(), damageInflicted, DamageCause.MAGIC);
                        broadcast(hero.getPlayer().getLocation(), missText, hero.getPlayer().getDisplayName());
                        //broadcast(hero.getPlayer().getLocation(), "Healing was converted into $1 damage!", (int) (damageInflicted));
                        //broadcast(hero.getPlayer().getLocation(), "Total for $1: $2 damage!", hero.getName(), (int) (damageInverted - overflow));
                        //broadcast(hero.getPlayer().getLocation(), "Healing of $1 health was blocked!", (int) (overflow));
                    } else {
                        addSpellTarget(hero.getPlayer(), hero);
                        damageEntity(hero.getPlayer(), corruptionEffect.getCaster(), damage, DamageCause.MAGIC);
                        broadcast(hero.getPlayer().getLocation(), missText, hero.getPlayer().getDisplayName());
                        //broadcast(hero.getPlayer().getLocation(), "Healing was converted into $1 damage!", (int) (damage));
                        //broadcast(hero.getPlayer().getLocation(), "Total for $1: $2 damage!", hero.getName(), (int) (damageInverted));
                    }
                } else {
                    broadcast(hero.getPlayer().getLocation(), negateText, hero.getPlayer().getDisplayName());
                    //broadcast(hero.getPlayer().getLocation(), "Healing of $1 health was blocked!", (int) (damage));
                }
            }
        }

        @EventHandler (ignoreCancelled = true)
        public void onEntityRegainRegularHealth(EntityRegainHealthEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("Corruption")) {
                if (hero.getPlayer().hasPotionEffect(PotionEffectType.REGENERATION)) {
                    hero.getPlayer().removePotionEffect(PotionEffectType.REGENERATION);
                }
                double damage = event.getAmount();
                event.setAmount(0D);
                CorruptionEffect corruptionEffect = (CorruptionEffect) hero.getEffect("Corruption");
                double damageInverted = corruptionEffect.getDamage();
                double cap = corruptionEffect.getCap();
                if(damageInverted < cap) {
                    damageInverted += damage;
                    corruptionEffect.setDamage(damageInverted);
                    if(damageInverted > cap) {
                        double overflow =   damageInverted - cap;
                        double damageInflicted = damage - overflow;
                        addSpellTarget(hero.getPlayer(), hero);
                        damageEntity(hero.getPlayer(), corruptionEffect.getCaster(), damageInflicted, DamageCause.MAGIC);
                        broadcast(hero.getPlayer().getLocation(), missText, hero.getPlayer().getDisplayName());
                        //broadcast(hero.getPlayer().getLocation(), "Healing was converted into $1 damage!", (int) (damageInflicted));
                        //broadcast(hero.getPlayer().getLocation(), "Total for $1: $2 damage!", hero.getName(), (int) (damageInverted - overflow));
                        //broadcast(hero.getPlayer().getLocation(), "Healing of $1 health was blocked!", (int) (overflow));
                    } else {
                        addSpellTarget(hero.getPlayer(), hero);
                        damageEntity(hero.getPlayer(), corruptionEffect.getCaster(), damage, DamageCause.MAGIC);
                        broadcast(hero.getPlayer().getLocation(), missText, hero.getPlayer().getDisplayName());
                        //broadcast(hero.getPlayer().getLocation(), "Healing was converted into $1 damage!", (int) (damage));
                        //broadcast(hero.getPlayer().getLocation(), "Total for $1: $2 damage!", hero.getName(), (int) (damageInverted));
                    }
                } else {
                    broadcast(hero.getPlayer().getLocation(), negateText, hero.getPlayer().getDisplayName());
                    //broadcast(hero.getPlayer().getLocation(), "Healing of $1 health was blocked!", (int) (damage));
                }
            }
        }
    }

    public class CorruptionListener implements Listener {
        private Skill skill;

        public CorruptionListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onPotionSplash(ProjectileHitEvent event) {
            Entity entity = event.getEntity();
            if (!(entity instanceof ThrownPotion)) {
                return;
            }
            if (event.getEntity().getShooter() instanceof Player) {
                if (event.getEntity().hasMetadata("Corruption")) {
                    Player player = (Player) event.getEntity().getShooter();
                    Hero hero = plugin.getCharacterManager().getHero(player);
                    ThrownPotion potion = (ThrownPotion)event.getEntity();
                    double radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 2D, false);
                    long duration = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 5000, false);
                    double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 5D, false);
                    for (Entity e : potion.getNearbyEntities(radius, radius, radius)) {
                        if ((e instanceof Player)) {
                            if (damageCheck(player, (LivingEntity) e)) {
                                Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                                addSpellTarget(tHero.getPlayer(), plugin.getCharacterManager().getHero(player));
                                damageEntity(tHero.getPlayer(), player, damage, DamageCause.MAGIC);
                                CorruptionEffect corruptionEffect = new CorruptionEffect(skill, duration, player);
                                tHero.addEffect(corruptionEffect);
                                Lib.cancelDelayedSkill(tHero);
                            }
                        } else if (e instanceof LivingEntity) {
                            if (damageCheck(player, (LivingEntity) e)) {
                                addSpellTarget((LivingEntity)e, plugin.getCharacterManager().getHero(player));
                                damageEntity((LivingEntity) e, player, damage, DamageCause.MAGIC);
                            }
                        }
                    }
                    Location loc = potion.getLocation();
                    loc.getWorld().spawnParticle(Particle.CLOUD, loc, 1);
                    loc.getWorld().spawnParticle(Particle.SMOKE_LARGE, loc, 1);
                }
            }
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onSplashEffect(PotionSplashEvent event) {
            if (event.getPotion().getShooter() instanceof Player) {
                if (event.getEntity().hasMetadata("Corruption")) {
                    event.setCancelled(true);
                }
            }
        }
    }
}