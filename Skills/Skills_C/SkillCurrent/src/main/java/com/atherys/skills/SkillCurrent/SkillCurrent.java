package com.atherys.skills.SkillCurrent;


import com.atherys.effects.WaveStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Set;

public class SkillCurrent extends ActiveSkill {
    public SkillCurrent(Heroes plugin) {
        super(plugin, "Current");
        setDescription("Heal $1 nearest allies within $2 blocks for $3 health every $4 second for $5 seconds total and give a swim buff for $6 seconds (swim buff breaks on combat). If you have 3 Wave stacks, remove all Wave stacks and push enemies away from you every second for $5 seconds.");
        setUsage("/skill Current");
        setArgumentRange(0, 0);
        setIdentifiers("skill Current");
        setTypes(SkillType.SILENCEABLE, SkillType.HEALING, SkillType.FORCE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int allies = SkillConfigManager.getUseSetting(hero, this, "number-of-allies", 10, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 5D, false);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10D, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000L, false);
        long healDur = (long) SkillConfigManager.getUseSetting(hero, this, "heal-duration", 8000L, false);
        long speedDur = (long) SkillConfigManager.getUseSetting(hero, this, "speed-duration", 20000L, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE.node(), 5, false);
        double v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 0.5D, false);
        double v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 0.5D, false);
        hero.addEffect(new CurrentHealEffect(this, allies, heal, radius, period, healDur, hero, v1, v2));
        hero.addEffect(new CurrentSpeedEffect(this, speedDur, 500L, distance, Material.STATIONARY_WATER,hero));
        if (hero.hasParty()) {
            int counter = 0;
            for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                if (counter >= allies) {
                    break;
                }
                if ((e instanceof Player) && (hero.getParty().isPartyMember((Player) e))) {
                    plugin.getCharacterManager().getHero((Player) e).addEffect(new CurrentSpeedEffect(this, speedDur, 500L, distance, Material.STATIONARY_WATER,hero));
                    counter++;
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class CurrentHealEffect extends PeriodicExpirableEffect {
        private final int allies;
        private final double radius;
        private final double heal;
        private final boolean hasThreeWaveStacks;
        private final double v1;
        private final double v2;

        public CurrentHealEffect(Skill skill, int allies, double heal, double radius, long period, long duration, Hero caster, double v1, double v2) {
            super(skill, "CurrentHeal",caster.getPlayer(), period, duration);
            this.allies = allies;
            this.radius = radius;
            this.heal = heal;
            this.v1 = v1;
            this.v2 = v2;
            this.hasThreeWaveStacks = WaveStackEffect.chargeWave(caster);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.HEALING);
            this.types.add(EffectType.WATER);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            if (hasThreeWaveStacks) {
                broadcast(player.getLocation(), "$1 expended their Wave stacks on $2!", player.getDisplayName(), "Current");
            }
        }

        @Override
        public void tickHero(Hero hero) {
            int counter = 0;
            for (Entity e : hero.getPlayer().getNearbyEntities(radius, radius, radius)) {
                if (hero.hasParty() && (e instanceof Player) && hero.getParty().isPartyMember((Player) e) && (counter <= allies)) {
                    Hero h = plugin.getCharacterManager().getHero((Player) e);
                    Lib.healHero(h, heal, skill, hero);
                    counter++;
                }
                else if (hasThreeWaveStacks && (e instanceof LivingEntity) && damageCheck(hero.getPlayer(), (LivingEntity)e)) {
                    Vector v = e.getLocation().toVector().subtract(hero.getPlayer().getLocation().toVector()).normalize();
                    v = v.multiply(v1);
                    v.setY(v2);
                    e.setVelocity(v);
                }
            }
            Lib.healHero(hero, heal, skill, hero);
        }

        @Override
        public void tickMonster(Monster monster) {
            //nothing here
        }
    }

    public class CurrentSpeedEffect extends PeriodicExpirableEffect {
        private final int distance;
        private final Material mat;

        public CurrentSpeedEffect(Skill skill, long duration, long period, int distance, Material mat,Hero caster) {
            super(skill, "CurrentSpeed",caster.getPlayer(), period, duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.WATER);
            this.distance = distance;
            this.mat = mat;
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            if (hero.isInCombat()) {
                this.expire();
            }
            Block b = player.getLocation().getBlock();
            if (b.getType() != mat) {
                return;
            }
            Location l = player.getTargetBlock((Set<Material>) null, distance).getLocation();
            Location pL = player.getLocation().getBlock().getRelative(BlockFace.DOWN).getLocation();
            Vector vc = new Vector(l.getX() - pL.getX(), 0.5D, l.getZ() - pL.getZ());
            vc.multiply(0.2);
            player.setVelocity(vc);
        }

        @Override
        public void tickMonster(Monster arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "Current Effect applied!");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "Current Effect Expired!");
        }
    }

    @Override
    public String getDescription(Hero hero) {
        int allies = SkillConfigManager.getUseSetting(hero, this, "number-of-allies", 10, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 5D, false);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10D, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000L, false);
        long healDur = (long) SkillConfigManager.getUseSetting(hero, this, "heal-duration", 8000L, false);
        long speedDur = (long) SkillConfigManager.getUseSetting(hero, this, "speed-duration", 20000L, false);
        return getDescription().replace("$1", allies + "").replace("$2", (int) radius + "").replace("$3", (int) heal + "").replace("$4", (double) (period / 1000) + "").replace("$5", (double) (healDur / 1000) + "").replace("$6", (double) (speedDur / 1000) + " ") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("number-of-allies", Integer.valueOf(10));
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(5));
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(10));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000));
        node.set("heal-duration", Long.valueOf(8000));
        node.set("speed-duration", Long.valueOf(20000));
        node.set(SkillSetting.MAX_DISTANCE.node(), Double.valueOf(5));
        node.set("horizontal-vector", Double.valueOf(0.5));
        node.set("vertical-vector", Double.valueOf(0.5));
        return node;
    }
}
