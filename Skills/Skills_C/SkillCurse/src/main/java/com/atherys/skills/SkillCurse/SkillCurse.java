package com.atherys.skills.SkillCurse;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.*;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

import java.math.BigDecimal;
import java.util.HashMap;

public class SkillCurse extends ActiveSkill implements Listener
{
    private HashMap<ThrownPotion, Player> potions;
    public SkillCurse(Heroes plugin)
    {
        super(plugin, "Curse");
        setDescription("You throw a potion. Enemies within $1 blocks of the impact take $2 damage and cannot wield weapons for $3 seconds.");
        setUsage("/skill curse");
        setArgumentRange(0, 0);
        setIdentifiers("skill curse");
        potions = new HashMap<ThrownPotion, Player>();
        Bukkit.getPluginManager().registerEvents(this, plugin);
        Bukkit.getPluginManager().registerEvents(new DamageCancelListener(), plugin);
    }

    public String getDescription(Hero hero)
    {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 30, true);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 4, true);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000, true);
        String formattedDuration = String.valueOf(new BigDecimal(duration / 1000).doubleValue());
        return getDescription().replace("$1", radius + "")
                .replace("$2", damage + "")
                .replace("$3", formattedDuration);
    }

    public ConfigurationSection getDefaultConfig()
    {
        ConfigurationSection node = super.getDefaultConfig();

        node.set(SkillSetting.DAMAGE.node(), 30);
        node.set(SkillSetting.RADIUS.node(), 4);
        node.set(SkillSetting.DURATION.node(), 4000);

        return node;
    }

    public SkillResult use(Hero hero, String[] args)
    {
        final Player player = hero.getPlayer();

        ThrownPotion pot = player.launchProjectile(ThrownPotion.class, player.getLocation().getDirection());
        potions.put(pot, player);

        broadcast(player.getLocation(), hero.getName() + " throws a cursed concoction!");
        return SkillResult.NORMAL;
    }

    @EventHandler
    public void onPotionLand(ProjectileHitEvent e)
    {
        if (!(e.getEntity() instanceof ThrownPotion)) return;
        ThrownPotion pot = (ThrownPotion) e.getEntity();
        if (!potions.containsKey(pot)) return;
        Player p = potions.get(pot);
        Hero h = plugin.getCharacterManager().getHero(p);

        pot.getWorld().spigot().playEffect(pot.getLocation().add(0, 0.3, 0), Effect.WITCH_MAGIC, 0, 0, 2.0F, 0.2F, 2.0F, 0.2F, 100, 128);

        double damage = SkillConfigManager.getUseSetting(h, this, SkillSetting.DAMAGE, 30, true);
        double radius = SkillConfigManager.getUseSetting(h, this, SkillSetting.RADIUS, 4, true);
        long duration = SkillConfigManager.getUseSetting(h, this, SkillSetting.DURATION, 4000, true);

        for (Entity entity : pot.getNearbyEntities(radius, radius, radius))
        {
            if (!(entity instanceof LivingEntity)) continue;
            LivingEntity le = (LivingEntity) entity;
            if (!damageCheck(le, p)) continue;
            addSpellTarget(le, h);
            damageEntity(le, p, damage, EntityDamageEvent.DamageCause.MAGIC);
            CurseEffect ce = new CurseEffect(this, duration, p);
            plugin.getCharacterManager().getCharacter(le).addEffect(ce);
        }
    }

    public class CurseEffect extends ExpirableEffect {

        public CurseEffect(Skill skill, long duration, Player caster) {
            super(skill, "Cursed", caster, duration);
            types.add(EffectType.HARMFUL);
            types.add(EffectType.PHYSICAL);
            types.add(EffectType.DISPELLABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), ChatColor.WHITE + hero.getName() + ChatColor.GRAY +  " has been Cursed!");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), ChatColor.WHITE + hero.getName() + ChatColor.GRAY +  " is no longer Cursed!");
        }
    }

    public class DamageCancelListener implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onEntityDamage(WeaponDamageEvent event) {
            if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Hero) || (event.getDamage() == 0)) {
                return;
            }
            if (event.getDamager().hasEffect("Cursed")) {
                event.setCancelled(true);
                Messaging.send(((Hero)event.getDamager()).getPlayer(), "You cannot damage that target while Cursed!");
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onShootBow(EntityShootBowEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            if (plugin.getCharacterManager().getHero((Player)event.getEntity()).hasEffect("Cursed")) {
                event.setCancelled(true);
                Messaging.send(((Player) event.getEntity()).getPlayer(), "You cannot shoot your bow while Cursed!");
            }
        }
    }
}