package com.atherys.skills.SkillDash;

import com.atherys.effects.DashSprintingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSprintEvent;

/*
 * Made by pumpapa
 */
public class SkillDash extends PassiveSkill {
    public SkillDash(Heroes plugin) {
        super(plugin, "Dash");
        setDescription("A Passive that gradually increases your movement speed while sprinting over time.");
        setTypes(SkillType.SILENCEABLE, SkillType.MOVEMENT_INCREASING);
        Bukkit.getServer().getPluginManager().registerEvents(new DashListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        return node;
    }

    public class DashListener implements Listener {
        private Skill skill;

        public DashListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onPlayerToggleSprint(PlayerToggleSprintEvent event) {
            Player player = event.getPlayer();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("Dash") && !hero.hasEffect("Sprint")) {
                if (event.isSprinting()) {
                    hero.addEffect(new DashSprintingEffect(skill, "DashSprinting0", 3000, 0,hero));
                } else {
                    for (int i = 0; i <= 2; i++) {
                        if (hero.hasEffect("DashSprinting" + i)) {
                            hero.removeEffect(hero.getEffect("DashSprinting" + i));
                            break;
                        }
                    }
                }
            }
            if (hero.hasEffect("DashSprintingFinal")) {
                hero.removeEffect(hero.getEffect("DashSprintingFinal"));
            }
        }
    }
}