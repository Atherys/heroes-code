package com.atherys.skills.SkillDaze;

import com.atherys.effects.SilenceEffect;
import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillDaze extends ActiveSkill {
    public SkillDaze(Heroes plugin) {
        super(plugin, "Daze");
        setDescription("Active\n Your next melee attack will silence your target for $1s and slow it for $2s");
        setUsage("/skill daze");
        setArgumentRange(0, 0);
        setIdentifiers("skill daze");
        setTypes(SkillType.BUFFING);
        Bukkit.getPluginManager().registerEvents(new HeroDamageListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("number-of-attacks", 1);
        node.set("slow-multiplier", 3);
        node.set("slow-duration", 3000);
        node.set("add-slowminning-potion", true);
        node.set("silence-duration", 3000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long slowDuration = (long) SkillConfigManager.getUseSetting(hero, this, "slow-duration", 5000L, false);
        long silenceDuration = (long) SkillConfigManager.getUseSetting(hero, this, "silence-duration", 3000L, false);
        return getDescription().replace("$1", slowDuration/1000 + "").replace("$2", silenceDuration/1000 + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int attacks = SkillConfigManager.getUseSetting(hero, this, "number-of-attacks", 1, false);
        int slowMultiplier = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 3, false);
        long slowDuration = (long) SkillConfigManager.getUseSetting(hero, this, "slow-duration", 3000L, false);
        long silenceDuration = (long) SkillConfigManager.getUseSetting(hero, this, "silence-duration", 3000L, false);
        boolean slowMining = SkillConfigManager.getUseSetting(hero, this, "add-slowminning-potion", true);
        hero.addEffect(new DazeEffect(this, duration, attacks, slowMultiplier, silenceDuration, slowDuration, slowMining,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    private static class DazeEffect extends ExpirableEffect {
        private int attacks;
        private long silenceDuration;
        private int slowMultiplier;
        private boolean slowMining;
        private long slowDuration;

        public DazeEffect(Skill skill, long duration, int attacks, int slowMultiplier, long silenceDuration, long slowDuration, boolean slowMining,Hero caster) {
            super(skill, "DazeEffect",caster.getPlayer(), duration);
            this.attacks = attacks;
            this.silenceDuration = silenceDuration;
            this.slowMultiplier = slowMultiplier;
            this.slowDuration = slowDuration;
            this.slowMining = slowMining;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Daze expired");
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }

        public long getSilenceDuration() {
            return silenceDuration;
        }

        public int getSlowMultiplier() {
            return slowMultiplier;
        }

        public long getSlowDuration() {
            return slowDuration;
        }

        public boolean addSlowMining() {
            return slowMining;
        }
    }

    public class HeroDamageListener implements Listener {
        private final Skill skill;

        public HeroDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero) ||
                    (event.isCancelled()) ||
                    !(event.getEntity() instanceof Player) ||
                    (event.getDamage() == 0) ||
                    (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero target = plugin.getCharacterManager().getHero((Player) event.getEntity());
            Hero hero = (Hero) event.getDamager();
            if (!event.getDamager().hasEffect("DazeEffect"))
                return;
            DazeEffect he = (DazeEffect) hero.getEffect("DazeEffect");
            target.addEffect(new SlowEffect(skill, he.getSlowDuration(), he.getSlowMultiplier(), he.addSlowMining(), "", "", hero));
            target.addEffect(new SilenceEffect(skill, he.getSilenceDuration(), true,hero));
            Lib.cancelDelayedSkill(target);
            getAttacksLeft(hero, he);
        }

        private void getAttacksLeft(Hero hero, DazeEffect he) {
            if (he.getAttacksLeft() <= 1) {
                hero.removeEffect(he);
            } else {
                he.setAttacksLeft(he.getAttacksLeft() - 1);
            }
        }
    }
}
