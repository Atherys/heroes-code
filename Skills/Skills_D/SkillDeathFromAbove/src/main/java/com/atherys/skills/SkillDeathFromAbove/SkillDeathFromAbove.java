package com.atherys.skills.SkillDeathFromAbove;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillDeathFromAbove extends ActiveSkill {
    private String applyText;
    private String removeText;

    public SkillDeathFromAbove(Heroes plugin) {
        super(plugin, "DeathFromAbove");
        setDescription("Active\nFor $1s, deals your fall damage times $2 as physical damage to players within $3 blocks of you.");
        setUsage("/skill deathfromabove");
        setArgumentRange(0, 0);
        setIdentifiers("skill deathfromabove", "skill dfa");
        setTypes(SkillType.DAMAGING, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.MOVEMENT_SLOWING_COUNTERING,SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new DeathFromAboveListener(this), plugin);
        // registerEvent(Type.ENTITY_DAMAGE, new DeathFromAboveListener(this),
        // Priority.Normal);
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + (SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0, false) * hero.getSkillLevel(this))) / 1000;
        duration = duration > 0 ? duration : 0;
        double damageMulti = (SkillConfigManager.getUseSetting(hero, this, "damage-multiplier", 1.0, false) + (SkillConfigManager.getUseSetting(hero, this, "damage-multi-increase", 0.0, false) * hero.getSkillLevel(this)));
        damageMulti = damageMulti > 0 ? damageMulti : 0;
        int radius = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 5, false) + (SkillConfigManager.getUseSetting(hero, this, "radius-increase", 0.0, false) * hero.getSkillLevel(this)));
        radius = radius > 0 ? radius : 0;
        String description = getDescription().replace("$1", duration + "").replace("$2", damageMulti + "").replace("$3", radius + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("duration-increase", 0);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set("radius-increase", 0);
        node.set("damage-multiplier", 1.0);
        node.set("damage-multi-increase", 0);
        node.set("safefall", "true");
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% is ready to pounce!");
        node.set("remove-text", "%hero% is not ready to pounce anymore!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getUseSetting(null, this, SkillSetting.APPLY_TEXT.node(), "%hero% is ready to pounce!").replace("%hero%", "$1");
        removeText = SkillConfigManager.getUseSetting(null, this, "remove-text", "%hero% is not ready to pounce anymore!").replace("%hero%", "$1");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        broadcastExecuteText(hero);
        long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + (SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0, false) * hero.getSkillLevel(this)));
        duration = duration > 0 ? duration : 0;
        hero.addEffect(new DeathFromAboveEffect(this, duration,hero));
        return SkillResult.NORMAL;
    }

    public class DeathFromAboveEffect extends ExpirableEffect {
        public DeathFromAboveEffect(Skill skill, long duration,Hero caster) {
            super(skill, "DeathFromAbove",caster.getPlayer(), duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applyText, hero.getPlayer().getDisplayName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), removeText, hero.getPlayer().getDisplayName());
        }
    }

    public class DeathFromAboveListener implements Listener {
        private Skill skill;

        public DeathFromAboveListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.isCancelled() || event.getDamage() == 0 || !(event.getEntity() instanceof Player) || event.getCause() != DamageCause.FALL)
                return;
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!hero.hasEffect("DeathFromAbove")) {
                return;
            }
            int radius = (int) (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS.node(), 5, false) + (SkillConfigManager.getUseSetting(hero, skill, "radius-increase", 0.0, false) * hero.getSkillLevel(skill)));
            radius = radius > 0 ? radius : 0;
            double damage = event.getDamage();
            double damageMulti = (SkillConfigManager.getUseSetting(hero, skill, "damage-multiplier", 1.0, false) + (SkillConfigManager.getUseSetting(hero, skill, "damage-multi-increase", 0.0, false) * hero.getSkillLevel(skill)));
            damageMulti = damageMulti > 0 ? damageMulti : 0;
            damage = damage * damageMulti;
            for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                if (e instanceof Player && !(e.equals(player))) {
                    Player p = (Player) e;
                    addSpellTarget(e, hero);
                    damageEntity(p, player, damage, DamageCause.ENTITY_ATTACK);
                    // p.damage(damage, player);
                } else if (e instanceof Creature) {
                    Creature c = (Creature) e;
                    addSpellTarget(e, hero);
                    damageEntity(c, player, damage, DamageCause.ENTITY_ATTACK);
                    // c.damage(damage, player);
                }
            }
            if (SkillConfigManager.getUseSetting(hero, skill, "safefall", "true").equals("true")) {
                event.setDamage(0);
                event.setCancelled(true);
            }
        }
    }
}