package com.atherys.skills.SkillDefences;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillDefences extends PassiveSkill {
    private Skill Defences;

    public SkillDefences(Heroes plugin) {
        super(plugin, "Defences");
        setDescription("Adjusts the Hero's resistances appropriately.");
        setTypes(SkillType.MOVEMENT_PREVENTION_COUNTERING, SkillType.BUFFING);
        setEffectTypes(EffectType.BENEFICIAL, EffectType.PHYSICAL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("magic-damage-reduction", 0.0);
        node.set("physical-damage-reduction", 0.0);
        node.set("projectile-damage-reduction", 0.0);
        node.set("wither-damage-reduction", 0.0);
        node.set("anvil-damage-reduction", 0.0);
        node.set("falling-damage-reduction", 0.0);
        node.set("lightning-damage-reduction", 0.0);
        return node;
    }

    @Override
    public void init() {
        super.init();
        Defences = this;
    }

    public class SkillHeroListener implements Listener {
        @EventHandler(ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (!(event.getEntity() instanceof Player) || event.getDamage() < 1) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!hero.hasEffect("Defences")) {
                return;
            }
            if (event.getCause() == DamageCause.MAGIC) {
                double amount = SkillConfigManager.getUseSetting(hero, Defences, "magic-damage-reduction", 0.0, false);
                event.setDamage(event.getDamage() * (1 - amount));
            }
            if (event.getCause() == DamageCause.ENTITY_ATTACK) {
                double amount = SkillConfigManager.getUseSetting(hero, Defences, "physical-damage-reduction", 0.0, false);
                event.setDamage(event.getDamage() * (1 - amount));
            }
            if (event.getCause() == DamageCause.PROJECTILE) {
                double amount = SkillConfigManager.getUseSetting(hero, Defences, "projectile-damage-reduction", 0.0, false);
                event.setDamage(event.getDamage() * (1 - amount));
            }
            if (event.getCause() == DamageCause.WITHER) {
                double amount = SkillConfigManager.getUseSetting(hero, Defences, "wither-damage-reduction", 0.0, false);
                event.setDamage(event.getDamage() * (1 - amount));
            }
            if (event.getCause() == DamageCause.FALL) {
                double amount = SkillConfigManager.getUseSetting(hero, Defences, "falling-damage-reduction", 0.0, false);
                event.setDamage(event.getDamage() * (1 - amount));
            }
            if (event.getCause() == DamageCause.LIGHTNING) {
                double amount = SkillConfigManager.getUseSetting(hero, Defences, "lightning-damage-reduction", 0.0, false);
                event.setDamage(event.getDamage() * (1 - amount));
            }
            if (event.getCause() == DamageCause.FALLING_BLOCK) {
                double amount = SkillConfigManager.getUseSetting(hero, Defences, "anvil-damage-reduction", 0.0, false);
                event.setDamage(event.getDamage() * (1 - amount));
            }
        }
    }
}