package com.atherys.skills.SkillDiscipline;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.ManaChangeEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillDiscipline extends PassiveSkill {
    private Skill Discipline;

    public SkillDiscipline(Heroes plugin) {
        super(plugin, "Discipline");
        setDescription("Skills of party members within a certain number of blocks cost less mana to use.");
        setTypes(SkillType.KNOWLEDGE, SkillType.BUFFING);
        setEffectTypes(EffectType.BENEFICIAL, EffectType.MAGIC);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillDisciplineListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(2.5));
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(5));
        return node;
    }

    @Override
    public void init() {
        super.init();
        Discipline = this;
    }

    public class SkillDisciplineListener implements Listener {
        @EventHandler(ignoreCancelled = true)
        public void onSkillMana(ManaChangeEvent event) {
            if (event.getFinalMana() > event.getInitialMana()) {
                return;
            }
            Hero hero = event.getHero();
            if (hero.hasParty()) {
                for (Hero x : hero.getParty().getMembers()) {
                    if (x.hasAccessToSkill(Discipline) && (!x.equals(hero))) {
                        double radius = SkillConfigManager.getUseSetting(x, Discipline, SkillSetting.RADIUS, 5D, false);
                        if (x.getPlayer().getWorld().equals(hero.getPlayer().getWorld()) && x.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) < (radius*radius)) {
                            double amount = SkillConfigManager.getUseSetting(x, Discipline, SkillSetting.AMOUNT, 2.5D, false);
                            event.setFinalMana((event.getFinalMana() + amount) > hero.getMaxMana() ? hero.getMaxMana() : (int)(event.getFinalMana() + amount));
                            break;
                        }
                    }
                }
            }
        }
    }
}