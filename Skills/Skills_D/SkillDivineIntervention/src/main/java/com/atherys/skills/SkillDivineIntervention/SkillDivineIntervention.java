package com.atherys.skills.SkillDivineIntervention;


import com.atherys.effects.InvulnEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;


public class SkillDivineIntervention extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillDivineIntervention(Heroes plugin) {
        super(plugin, "DivineIntervention");
        setDescription("Active\nYou make the two lowest-hp party members invulnerable for $2s, while still allowing them to attack enemies");
        setUsage("/skill DivineIntervention");
        setArgumentRange(0, 0);
        setIdentifiers("skill DivineIntervention");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE, SkillType.MOVEMENT_PREVENTION_COUNTERING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.RADIUS_INCREASE.node(), 0.0D);
        node.set(SkillSetting.DURATION.node(), 12000);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 0.0D);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% is invulnerable!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% is now vulnerable!");
        node.set("party-cooldown", 40000);
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% is invulnerable!").replace("%target%", "$1");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% is now vulnerable!").replace("%target%", "$1");
    }

    @Override
    public String getDescription(Hero hero) {
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS_INCREASE, 0.0D, false) * hero.getSkillLevel(this);
        double duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 12000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false) * hero.getSkillLevel(this);
        String description = getDescription().replace("$1", radius + "").replace("$2", (int) (duration / 1000.0D) + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 12000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false) * hero.getLevel();
        if (!hero.hasParty()) {
            if (!hero.hasEffect("DivineIntervention")) {
                hero.addEffect(new InvulnEffect(this, "DivineIntervention", duration,hero));
            }
        } else {
            double radius = (double) SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "radius-increase", 0.0D, false) * hero.getLevel();
            long pcd = (long) SkillConfigManager.getUseSetting(hero, this, "party-cooldown", 40000L, false);
            Hero a = null, b = null;
            for (Hero pHero : hero.getParty().getMembers()) {
                if (pHero.hasAccessToSkill(this) && (!hero.equals(pHero)) && (hero.getCooldown("DivineIntervention") == null || hero.getCooldown("DivineIntervention") < pcd + System.currentTimeMillis())) {
                    pHero.setCooldown("DivineIntervention", pcd + System.currentTimeMillis());
                }
                if (pHero.getPlayer().getWorld().equals(player.getWorld()) && ((((pHero.getPlayer()).getLocation()).distanceSquared(player.getLocation())) <= radius * radius)) {
                    if (a == null)
                        a = pHero;
                    else if (b == null)
                        b = pHero;
                    else if (a.getPlayer().getHealth() > pHero.getPlayer().getHealth())
                        a = pHero;
                    else if (b.getPlayer().getHealth() > pHero.getPlayer().getHealth())
                        b = pHero;
                }
            }

            if (a != null) {
                a.addEffect(new InvulnEffect(this, "DivineIntervention", duration,hero));
            }
            if (b != null) {
                b.addEffect(new InvulnEffect(this, "DivineIntervention", duration,hero));
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
