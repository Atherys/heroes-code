package com.atherys.skills.SkillDivineShield;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillDivineShield extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillDivineShield(Heroes plugin) {
        super(plugin, "DivineShield");
        setDescription("Place a glowstone block that will give party members within 8 blocks 30% magic resistance for 3 seconds, every 3 seconds for 9 seconds total.");
        setUsage("/skill DivineShield");
        setArgumentRange(0, 0);
        setIdentifiers("skill DivineShield");
        setTypes(SkillType.ABILITY_PROPERTY_EARTH, SkillType.ABILITY_PROPERTY_LIGHT, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new DSListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 9000);
        node.set("buff-duration", 3000);
        node.set(SkillSetting.PERIOD.node(), 3000);
        node.set(SkillSetting.AMOUNT.node(), 0.3D);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        node.set(SkillSetting.RADIUS.node(), 8);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%' %skill% expired.");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription();
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%' %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player p = hero.getPlayer();
        HashSet<Byte> transparent = new HashSet<>();
        transparent.add((byte) Material.AIR.getId());
        transparent.add((byte) Material.SNOW.getId());
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block b = p.getTargetBlock(transparent, max);
        if (b.getType() == Material.AIR) {
            p.sendMessage(ChatColor.GRAY + "You must target a block.");
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        Block b1 = b.getRelative(BlockFace.UP);
        if (!(b1.getType() == Material.AIR || b1.getType() == Material.SNOW)) {
            p.sendMessage(ChatColor.GRAY + "Cannot be placed here");
            return SkillResult.CANCELLED;
        }
        values.add(b1);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 9000, false);
        long buffduration = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 3000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 3000, false);
        double mr = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.3D, false);
        double r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 8, false);
        b1.setType(Material.GLOWSTONE);
        b1.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
        Long timer = Long.valueOf(System.currentTimeMillis());
        hero.addEffect(new DSEffect(this, period, duration, buffduration, mr, r, b1, values,hero));
        if (hero.getCooldown("RayOfLight") == null || (hero.getCooldown("RayOfLight") - (long) timer) < 20000) {
            hero.setCooldown("RayOfLight", (long) timer + 20000);
        }
        if (hero.getCooldown("Blessing") == null || (hero.getCooldown("Blessing") - (long) timer) < 20000) {
            hero.setCooldown("Blessing", (long) timer + 20000);
        }
        return SkillResult.NORMAL;
    }

    public class DSEffect extends PeriodicExpirableEffect {
        private final double mr;
        private final double radius;
        private final Block b;
        private final long buffDuration;
        private final Set<Block> values;
        private final Skill skill;

        public DSEffect(Skill skill, long period, long duration, long buffDuration, double mr, double radius, Block b, Set<Block> values,Hero caster) {
            super(skill, "DSEffect",caster.getPlayer(), period, duration);
            this.mr = mr;
            this.radius = radius;
            this.b = b;
            this.buffDuration = buffDuration;
            this.values = values;
            this.skill = skill;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.FORM);
            types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Divine Shield");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Divine Shield");
            }
            Lib.removeBlocks(values);
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            for (Entity e : b.getLocation().getWorld().getNearbyEntities(b.getLocation(), radius, radius, radius)) {
                if (!(e instanceof Player)) continue;
                Hero h = plugin.getCharacterManager().getHero((Player) e);
                if (h.equals(hero) || (h.hasParty() && (h.getParty().getMembers().contains(hero)))) {
                    h.addEffect(new DSMREffect(skill, buffDuration, mr,h));
                }
            }
        }
    }

    public class DSMREffect extends ExpirableEffect {
        private final double mr;

        public DSMREffect(Skill skill, long duration, double mr,Hero caster) {
            super(skill, "DSMREffect",caster.getPlayer(), duration);
            this.mr = mr;
        }

        public double getMagicResist() {
            return mr;
        }
    }

    public class DSListener implements Listener {
        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onMagicDamage(EntityDamageByEntityEvent event) {
            if (!(event.getEntity() instanceof Player) || (event.getCause() != EntityDamageEvent.DamageCause.MAGIC)) {
                return;
            }
            CharacterTemplate hero = plugin.getCharacterManager().getCharacter((Player) event.getEntity());
            if (hero.hasEffect("DSMREffect")) {
                DSMREffect dsmr = (DSMREffect) hero.getEffect("DSMREffect");
                event.setDamage(event.getDamage() - event.getDamage() * dsmr.getMagicResist());
            }

        }
    }
}

