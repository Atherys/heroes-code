package com.atherys.skills.SkillDodge;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class SkillDodge extends ActiveSkill {
    public SkillDodge(Heroes plugin) {
        super(plugin, "Dodge");
        setDescription("Active\nLaunches you backwards.");
        setUsage("/skill Dodge");
        setArgumentRange(0, 0);
        setIdentifiers("skill Dodge");
        setTypes(SkillType.MOVEMENT_INCREASING, SkillType.BUFFING,SkillType.SILENCEABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        double chance = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE.node(), 0.2, false) +
                (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE_LEVEL.node(), 0.0, false) * hero.getSkillLevel(this))) * 100;
        chance = chance > 0 ? chance : 0;
        String description = getDescription().replace("$1", chance + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        final Player player = hero.getPlayer();
        Location playerLoc = player.getLocation();
        Vector direction = playerLoc.getDirection().normalize();
        direction.multiply(SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, -2, false));
        playerLoc.subtract(direction);
        direction.setY(1);
        player.setVelocity(direction);
        player.setFallDistance(-8.0F);
        Messaging.send(player, "You dodged away!");
        return SkillResult.NORMAL;
    }
}