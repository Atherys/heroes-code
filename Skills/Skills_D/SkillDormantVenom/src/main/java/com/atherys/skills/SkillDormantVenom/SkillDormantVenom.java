package com.atherys.skills.SkillDormantVenom;

import com.atherys.effects.InvulnEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class SkillDormantVenom extends TargettedSkill {
    public SkillDormantVenom(Heroes plugin) {
        super(plugin, "DormantVenom");
        setDescription("Mark a target with venom. After a few seconds, cast the skill again to Snare the target.");
        setUsage("/skill dormantvenom");
        setArgumentRange(0, 0);
        setIdentifiers("skill dormantvenom");
        setTypes(SkillType.DEBUFFING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(20000));
        node.set("delay-time", Integer.valueOf(5000));
        node.set("stun-duration", 3000);
        node.set("max-range", 100);
        node.set("mana-cost", 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!hero.hasEffect("DormantVenomEffect")) {
            if ((hero.getCooldown("DormantVenomCooldown") != null && hero.getCooldown("DormantVenomCooldown") > System.currentTimeMillis())) {
                DecimalFormat decimalFormat = new DecimalFormat("#0.0");
                Messaging.send(hero.getPlayer(), "[" + ChatColor.DARK_GREEN + "Skill" + ChatColor.GRAY + "] $1 has $2s until recovery!", "DormantVenom", decimalFormat.format(((double) (hero.getCooldown("DormantVenomCooldown") - System.currentTimeMillis())) / 1000D));
                return SkillResult.CANCELLED;
            } else {
                if(!(target instanceof Player)){
                    return SkillResult.INVALID_TARGET;
                }
                if (!damageCheck(hero.getPlayer(), target)) {
                    return SkillResult.INVALID_TARGET_NO_MSG;
                }
                int manaCost = SkillConfigManager.getUseSetting(hero, this, "mana-cost", 10, false);
                if (hero.getMana() < manaCost) {
                    return SkillResult.LOW_MANA;
                }
                long delay = SkillConfigManager.getUseSetting(hero, this, "delay-time", 5000, false);
                long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
                long cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 45000, false);

                Hero targetHero = this.plugin.getCharacterManager().getHero((Player) target);
                hero.addEffect(new DormantVenomEffect(this, duration, targetHero,hero));
                hero.setCooldown("DormantVenomCooldown", System.currentTimeMillis() + cooldown);
                hero.setCooldown("DormantVenomDelay", System.currentTimeMillis() + delay);
                broadcastExecuteText(hero, target);
                hero.setMana(hero.getMana() + manaCost);
                return SkillResult.SKIP_POST_USAGE;
            }
        } else if (hero.getCooldown("DormantVenomDelay") == null || hero.getCooldown("DormantVenomDelay") <= System.currentTimeMillis()) {
            DormantVenomEffect dormantVenomEffect = (DormantVenomEffect) hero.getEffect("DormantVenomEffect");
            Hero targetHero = dormantVenomEffect.getTarget();
            int maxRange = SkillConfigManager.getUseSetting(hero, this, "max-range", 100, false);
            if (targetHero.getPlayer().getWorld().equals(hero.getPlayer().getWorld()) && targetHero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) > maxRange*maxRange) {
                Messaging.send(hero.getPlayer(), "Your target got out of range!");
                hero.removeEffect(hero.getEffect("DormantVenomEffect"));
                return SkillResult.CANCELLED;
            }
            long duration = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 3000, false);
            targetHero.addEffect(new InvulnEffect(this, "DormantVenomInvuln", duration,hero));
            targetHero.addEffect(new StunEffect(this, duration,hero));
            broadcast(hero.getPlayer().getLocation(), "$1 activated their $2!", hero.getName(), "DormantVenom");
            hero.removeEffect(dormantVenomEffect);
            return SkillResult.SKIP_POST_USAGE;
        } else {
            DecimalFormat decimalFormat = new DecimalFormat("#0.0");
            Messaging.send(hero.getPlayer(), "Wait $1 more seconds before reactivating this skill.", decimalFormat.format(((double)(hero.getCooldown("DormantVenomDelay")-System.currentTimeMillis()))/1000D));
            return SkillResult.CANCELLED;
        }
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class DormantVenomEffect extends ExpirableEffect {
        private Hero target;

        public DormantVenomEffect(Skill skill, long duration, Hero target,Hero caster) {
            super(skill, "DormantVenomEffect",caster.getPlayer(), duration);
            types.add(EffectType.BENEFICIAL);
            this.target = target;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if(this.isExpired()) {
                Messaging.send(hero.getPlayer(), "Your target is no longer marked!");
            }
        }

        public Hero getTarget() {
            return target;
        }
    }
}