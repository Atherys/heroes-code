package com.atherys.skills.SkillEagleEye;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.List;

/*
 * Made by pumpapa
 */
public class SkillEagleEye extends ActiveSkill {
    public SkillEagleEye(Heroes plugin) {
        super(plugin, "EagleEye");
        setArgumentRange(0, 0);
        setDescription("Shows some info about all players within a $1 blocks area around you.");
        setIdentifiers("skill eagleeye");
        setUsage("/skill eagleeye");
        setTypes(SkillType.SILENCEABLE, SkillType.KNOWLEDGE);
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 150, false);
        return description.replace("$1", "" + r);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(300));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 150, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 3000, false);
        hero.addEffect(new EagleEyeEffect(this, duration, r, hero.getPlayer().getWorld().getPlayers(), hero.getPlayer().getWorld(),hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class EagleEyeEffect extends ExpirableEffect {
        private final int r;
        private final List<Player> players;
        private final World world;

        public EagleEyeEffect(Skill skill, long duration, int r, List<Player> players, World world,Hero caster) {
            super(skill, "EagleEyeEffect",caster.getPlayer(), duration);
            this.r = r;
            this.players = players;
            this.world = world;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            if (hero.hasParty()) {
                for (Player e : players) {
                    if ((e!= null) && (e.getWorld().equals(world)) && (e != player) && (e.getLocation().distanceSquared(player.getLocation()) <= (r * r)) && !e.hasMetadata("NPC") && !e.isOp() && !hero.getParty().isPartyMember(e)) {
                        Messaging.send(e, "You feel you are being watched...ʕ•ᴥ•ʔ");
                    }
                }
            } else {
                for (Player e : players) {
                    if ((e!= null) && (e.getWorld().equals(world)) && (e != player) && (e.getLocation().distanceSquared(player.getLocation()) <= (r * r)) && !e.hasMetadata("NPC") && !e.isOp()) {
                        Messaging.send(e, "You feel you are being watched...ʕ•ᴥ•ʔ");
                    }
                }
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            int hit = 0;
            Messaging.send(player, "Nearby players:");
            if (hero.hasParty()) {
                for (Player e : players) {
                    if ((e!= null) && (e.getWorld().equals(world)) && (e != player) && (e.getLocation().distanceSquared(player.getLocation()) <= (r * r)) && !e.hasMetadata("NPC") && !e.isOp() && !hero.getParty().isPartyMember(e)) {
                        Hero thero = plugin.getCharacterManager().getHero(e);
                        if (!thero.hasEffect("Invisible")) {
                            Messaging.send(player, "Player: $1. Class: $2. Level: $3", e.getDisplayName(), thero.getHeroClass().toString(), thero.getLevel());
                            hit++;
                        }
                    }
                }
            } else {
                for (Player e : players) {
                    if ((e!= null) && (e.getWorld().equals(world)) && (e != player) && (e.getLocation().distanceSquared(player.getLocation()) <= (r * r)) && !e.hasMetadata("NPC") && !e.isOp()) {
                        Hero thero = plugin.getCharacterManager().getHero(e);
                        if (!thero.hasEffect("Invisible")) {
                            Messaging.send(player, "Player: $1. Class: $2. Level: $3", e.getDisplayName(), thero.getHeroClass().toString(), thero.getLevel());
                            hit++;
                        }
                    }
                }
            }
            Messaging.send(player, "There are $1 players within $2 blocks from you.", hit, r);
        }
    }
}