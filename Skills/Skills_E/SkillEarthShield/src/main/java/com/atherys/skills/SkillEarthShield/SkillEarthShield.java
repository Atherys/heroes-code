package com.atherys.skills.SkillEarthShield;

import com.atherys.heroesaddon.util.Lib;
import com.atherys.skills.SkillEarthShield.event.EventDamageHero;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillEarthShield extends ActiveSkill {
	private static SkillEarthShield instance;
	private double resistance;
	private String applyText;
	private String expireText;

	public SkillEarthShield(Heroes plugin) {
		super(plugin, "EarthShield");
		setDescription("Active\nThe stationary dome of stone that surrounds the caster for $1s.");
		setUsage("/skill EarthShield");
		setArgumentRange(0, 0);
		setIdentifiers("skill earthshield");
		setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
		Bukkit.getPluginManager().registerEvents(new EventDamageHero(), this.plugin);
		if (instance == null) {
			instance = this;
		}
	}

	@Override
	public String getDescription(Hero hero) {
		int duration = (SkillConfigManager.getUseSetting(hero, this, "shield-duration", 10000, false) / 1000);
		String description = getDescription().replace("$1", duration + "");
		return description + Lib.getSkillCostStats(hero, this);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set("on-text", "%hero% creates %skill%");
		node.set("off-text", "%hero%'s %skill% expired!");
		node.set("BlockType", "STONE");
		node.set("shield-duration", 10000);
		node.set("resistance", 0.2);
		return node;
	}

	@Override
	public void init() {
		super.init();
		applyText = SkillConfigManager.getRaw(this, "on-text", "%hero% creates %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
		expireText = SkillConfigManager.getRaw(this, "off-text", "%hero%'s %skill% expired!").replace("%hero%", "$1").replace("%skill%", "$2");
	}

	@Override
	public SkillResult use(Hero hero, String[] args) {
		resistance = SkillConfigManager.getUseSetting(hero, this, "resistance", 0.2, false);
		if (!hero.hasEffect("EarthShield")) {
			earthShield(hero);
			hero.getPlayer().playSound(hero.getPlayer().getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1.0F, 0.5F);
			return SkillResult.CANCELLED;
		}else{
			EarthEffect earthEffect = (EarthEffect) hero.getEffect("EarthShield");
			if (earthEffect != null) {
				Lib.removeBlocks(earthEffect.getBlocks());
				hero.removeEffect(earthEffect);
				return SkillResult.NORMAL;
			}

		}
		return SkillResult.FAIL;
	}


	public static SkillEarthShield getInstance() {
		return instance;
	}

	public double getResistance() {
		return resistance;
	}



	private void earthShield(Hero hero) {
		final Player player = hero.getPlayer();
		long shieldDuration = SkillConfigManager.getUseSetting(hero, this, "shield-duration", 10000, false);
		final Material setter = Material.valueOf(SkillConfigManager.getUseSetting(hero, this, "BlockType", "STONE"));

		final Set<Block> blocks = new HashSet<>();
		final Location loc = player.getLocation();
		Block mainBlock = loc.getBlock();


		for (int x = -3; x <= 3; x++) {
			for (int z = -3; z <= 3; z++) {
				for (int y = -1; y <= 4; y++) {
					if ((x == -3 || x == 3) ||
							(z == -3 || z == 3) ||
							(y == 4) || (y == -1)) {
						Block block = mainBlock.getRelative(x, y, z);
						if (block.getType() == Material.AIR) {
							block.setType(setter);
							block.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
							blocks.add(block);
						}
					}
				}
			}
		}
		mainBlock.getWorld().playEffect(mainBlock.getLocation(), Effect.STEP_SOUND, mainBlock.getType().getId());
		hero.addEffect(new EarthEffect(this, shieldDuration, blocks, hero));
	}


	public class EarthEffect extends PeriodicExpirableEffect {
		private Set<Block> blocks;

		public EarthEffect(Skill skill, long shieldDuration, Set<Block> blocks,Hero caster) {
			super(skill, "EarthShield",caster.getPlayer(), 20L, shieldDuration);
			this.blocks = blocks;
			types.add(EffectType.BENEFICIAL);
			types.add(EffectType.FORM);
			types.add(EffectType.DISPELLABLE);
		}

		public Set<Block> getBlocks() {
			return blocks;
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
		}

		@Override
		public void removeFromHero(Hero hero) {
			Player player = hero.getPlayer();
			super.removeFromHero(hero);

			if (this.isExpired()) {
				hero.setCooldown("EarthShield", SkillConfigManager.getUseSetting(hero, this.skill, "cooldown", 12000, false) + System.currentTimeMillis());
				broadcast(player.getLocation(), expireText, player.getDisplayName(), "EarthShield");
			}
			Lib.removeBlocks(blocks);
		}

		@Override
		public void tickMonster(Monster monster) {

		}

		@Override
		public void tickHero(Hero hero) {
			for(double[] coordinates : Lib.playerParticleCoords) {
				Location location = hero.getPlayer().getLocation();
				location.add(coordinates[0], coordinates[1], coordinates[2]);
				hero.getPlayer().getWorld().spigot().playEffect(location, Effect.WATERDRIP);
				hero.getPlayer().getWorld().spigot().playEffect(location, Effect.LAVADRIP);
			}
		}
	}
}