package com.atherys.skills.SkillEarthShield.event;

import com.atherys.skills.SkillEarthShield.SkillEarthShield;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author apteryx
 * @time 1:35 PM
 * @since 11/22/2016
 */
public class EventDamageHero implements Listener {

    @EventHandler
    public void onHeroDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("EarthShield")) {
                if (event.getCause() == EntityDamageEvent.DamageCause.MAGIC) {
                    event.setDamage(event.getDamage() * SkillEarthShield.getInstance().getResistance());
                    Bukkit.getLogger().info("Damage after: " + event.getDamage() * SkillEarthShield.getInstance().getResistance());
                    Bukkit.getLogger().info("Damage before: " + event.getDamage() * SkillEarthShield.getInstance().getResistance());
                }
            }
        }
    }

    @EventHandler
    public void onWeaponDamage(WeaponDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("EarthShield")) {
                event.setDamage(event.getDamage() * SkillEarthShield.getInstance().getResistance());
                Bukkit.getLogger().info("Damage after: " + event.getDamage() * SkillEarthShield.getInstance().getResistance());
                Bukkit.getLogger().info("Damage before: " + event.getDamage() * SkillEarthShield.getInstance().getResistance());
            }
        }
    }

}