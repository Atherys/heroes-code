package com.atherys.skills.SkillEarthWall;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class SkillEarthWall extends ActiveSkill {

    public SkillEarthWall(Heroes plugin) {
        super(plugin, "EarthWall");
        setUsage("/skill EarthWall");
        setArgumentRange(0, 0);
        setIdentifiers("skill EarthWall");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("height", Integer.valueOf(3));
        node.set("width", Integer.valueOf(2));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(20));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set("charges", 3);
        node.set("block-type", "STONE");
        return node;
    }

    public String getDescription(Hero hero) {
        return getDescription();
    }

    public SkillResult use(Hero hero, String[] args) {
        if (hero.getCooldown("EarthWallMiniCooldown") == null || hero.getCooldown("EarthWallMiniCooldown") <= System.currentTimeMillis()) {
            hero.setCooldown("EarthWallMiniCooldown", System.currentTimeMillis() + 1000);
        } else {
            Messaging.send(hero.getPlayer(), "Wait 1 more second before recasting the skill.");
            return SkillResult.CANCELLED;
        }
        int charges = SkillConfigManager.getUseSetting(hero, this, "charges", 3, false);
        int height = SkillConfigManager.getUseSetting(hero, this, "height", 3, false);
        int width = SkillConfigManager.getUseSetting(hero, this, "width", 2, false);
        int maxDist = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 5, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        Material setter = Material.valueOf(SkillConfigManager.getUseSetting(hero, this, "block-type", "STONE"));
        if (!earthWall(hero, height, width, maxDist, duration, setter, charges)) {
            return SkillResult.CANCELLED;
        }
        ChargesEffect effect = (ChargesEffect) hero.getEffect("Charges");
        if (effect.getCharges() >= 1) {
            Messaging.send(hero.getPlayer(), "$1 " + (effect.getCharges() == 1 ? "wall" : "walls") + " remaining.", effect.getCharges());
            return SkillResult.SKIP_POST_USAGE;
        } else {
            Messaging.send(hero.getPlayer(), "You have no more walls left.");
            hero.removeEffect(effect);
            return SkillResult.NORMAL;
        }
    }

    private boolean earthWall(Hero hero, int height, int width, int maxDist, long duration, Material setter, int charges) {
        Player player = hero.getPlayer();
        Block tBlock = player.getTargetBlock((Set<Material>) null, maxDist);
        if (tBlock.getType() == Material.AIR) {
            return false;
        }
        Set<Block> blcksToAdd = new HashSet<>();
        if (SkillEarthWall.this.is_X_Direction(player)) {
            for (int yDir = 0; yDir < height; yDir++) {
                for (int xDir = -width; xDir < width + 1; xDir++) {
                    Block chBlock = tBlock.getRelative(xDir, yDir, 0);
                    if ((chBlock.getType() == Material.AIR) || (chBlock.getType() == Material.SNOW)) {
                        chBlock.setType(setter);
                        blcksToAdd.add(chBlock);
                        chBlock.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
                    }
                }
            }
        } else {
            for (int yDir = 0; yDir < height; yDir++) {
                for (int zDir = -width; zDir < width + 1; zDir++) {
                    Block chBlock = tBlock.getRelative(0, yDir, zDir);
                    if ((chBlock.getType() == Material.AIR) || (chBlock.getType() == Material.SNOW)) {
                        chBlock.setType(setter);
                        blcksToAdd.add(chBlock);
                        chBlock.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
                    }
                }
            }
        }
        ChargesEffect effect;
        if (!hero.hasEffect("Charges")) {
            effect = new ChargesEffect(this, charges);
            hero.addEffect(effect);
        } else {
            effect = (ChargesEffect) hero.getEffect("Charges");
        }
        effect.decCharges();
        EarthWallEffect swe = new EarthWallEffect(this, effect.getCharges(), duration, blcksToAdd,hero);
        hero.addEffect(swe);
        return true;
    }

    private boolean is_X_Direction(Player player) {
        Vector u = player.getLocation().getDirection();
        u = new Vector(u.getX(), 0.0D, u.getZ()).normalize();
        Vector v = new Vector(0, 0, -1);
        double magU = Math.sqrt(Math.pow(u.getX(), 2.0D) + Math.pow(u.getZ(), 2.0D));
        double magV = Math.sqrt(Math.pow(v.getX(), 2.0D) + Math.pow(v.getZ(), 2.0D));
        double angle = Math.acos(u.dot(v) / (magU * magV));
        angle = angle * 180.0D / 3.141592653589793D;
        angle = Math.abs(angle - 180.0D);
        return (angle <= 45.0D) || (angle > 135.0D);
    }

    public class ChargesEffect extends Effect {
        private int charges;

        public ChargesEffect(Skill skill, int charges) {
            super(skill, "Charges");
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.UNBREAKABLE);
            this.charges = charges;
        }

        public int getCharges() {
            return charges;
        }

        public void decCharges() { --charges; }
    }

    public class EarthWallEffect extends ExpirableEffect {
        private final Set<Block> blocks;

        public EarthWallEffect(Skill skill, int number, long duration, Set<Block> blcksToAdd,Hero caster) {
            super(skill, "EarthWall" + number,caster.getPlayer(), duration);
            this.blocks = blcksToAdd;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.FORM);
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 made a wall", player.getDisplayName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Lib.removeBlocks(blocks);
            if (this.isExpired()) {
                broadcast(player.getLocation(), "$1's wall has been removed", player.getDisplayName());
            }
        }
    }
}
