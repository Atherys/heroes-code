package com.atherys.skills.SkillEarthquake;

import com.atherys.effects.SkillCastingEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillEarthquake extends ActiveSkill {
    public SkillEarthquake(Heroes plugin) {
        super(plugin, "Earthquake");
        setUsage("/skill earthquake");
        setArgumentRange(0, 0);
        setIdentifiers("skill earthquake");
        setDescription("Active\nYou start channeling an Earthquake, knocking up and dealing damage to nearby enemies periodically.");
        setTypes(SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        return getDescription();
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(3));
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(10));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(30000));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        broadcastExecuteText(hero);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1500, false);
        double range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        double tickDamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10D, false);
        hero.addEffect(new EarthquakeCastingEffect(this, duration,hero));
        hero.addEffect(new EarthquakeEffect(this, period, duration, range, tickDamage,hero));
        return SkillResult.NORMAL;
    }

    public class EarthquakeCastingEffect extends SkillCastingEffect {

        public EarthquakeCastingEffect(Skill skill, long duration,Hero caster) {
            super(skill, duration, false, "$1 is channeling their $2!", "$1 finished channeling their $2.", "$1 got interrupted from channeling their $2!",caster);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (hero.hasEffect("Earthquake")) {
                hero.removeEffect(hero.getEffect("Earthquake"));
            }
        }
    }

    public class EarthquakeEffect extends PeriodicExpirableEffect {
        private final double range;
        private final double tickDamage;

        public EarthquakeEffect(Skill skill, long period, long duration, double range, double tickDamage,Hero caster) {
            super(skill, "Earthquake",caster.getPlayer(), period, duration);
            this.range = range;
            this.tickDamage = tickDamage;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            boolean hit = false;
            for (Entity aNear : player.getNearbyEntities(range, range, range)) {
                if (!(aNear instanceof LivingEntity)) {
                    continue;
                }
                LivingEntity e = (LivingEntity) aNear;
                if (!(damageCheck(hero.getPlayer(), e))) {
                    continue;
                }
                if (aNear.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.AIR)) {
                    continue;
                }
                addSpellTarget(aNear, hero);
                damageEntity(e, player, tickDamage, DamageCause.CUSTOM);
                e.setVelocity(e.getVelocity().setY(1.0));
                hit = true;
            }
            if (hit) {
                hero.getPlayer().getWorld().createExplosion(hero.getPlayer().getLocation(), 0.0F, false);
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }
}