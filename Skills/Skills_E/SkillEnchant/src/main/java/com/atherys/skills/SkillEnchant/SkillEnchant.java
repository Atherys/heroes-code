package com.atherys.skills.SkillEnchant;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SkillEnchant extends PassiveSkill {
    public SkillEnchant(Heroes plugin) {
        super(plugin, "Enchant");
        setDescription("Active\nYou are able to enchant items.");
        setArgumentRange(0, 0);
        setUsage("/skill Enchant");
        setIdentifiers("skill Enchant");
        setTypes(SkillType.KNOWLEDGE, SkillType.ITEM_MODIFYING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEnchantListener(this), plugin);
    }

    public static Inventory myMainInventory = Bukkit.createInventory(null, 27, "Main Menu");
    public static Inventory myArmorInventory = Bukkit.createInventory(null, 27, "Armor Enchantments");
    public static Inventory myWeaponsInventory = Bukkit.createInventory(null, 27, "Weapon Enchantments");
    public static Inventory myToolsInventory = Bukkit.createInventory(null, 27, "Tool Enchantments");

    static {

        createDisplay(Material.LEATHER_HELMET, myMainInventory, 11, "Armor");
        createDisplay(Material.IRON_SWORD, myMainInventory, 13, "Weapons");
        createDisplay(Material.DIAMOND_SPADE, myMainInventory, 15, "Tools");
        createDisplay(Material.REDSTONE_BLOCK, myMainInventory, 8, "Exit");

        createDisplay(Material.ENCHANTED_BOOK, myArmorInventory, 17, "Feather Falling");
        createDisplay(Material.ENCHANTED_BOOK, myArmorInventory, 11, "Respiration");
        createDisplay(Material.ENCHANTED_BOOK, myArmorInventory, 13, "Aqua Affinity");
        createDisplay(Material.ENCHANTED_BOOK, myArmorInventory, 15, "Thorns");
        createDisplay(Material.ENCHANTED_BOOK, myArmorInventory, 9, "Unbreaking");
        createDisplay(Material.REDSTONE_BLOCK, myArmorInventory, 8, "Back");

        createDisplay(Material.ENCHANTED_BOOK, myWeaponsInventory, 13, "Knockback");
        createDisplay(Material.ENCHANTED_BOOK, myWeaponsInventory, 11, "Fire Aspect");
        createDisplay(Material.ENCHANTED_BOOK, myWeaponsInventory, 9, "Unbreaking");
        createDisplay(Material.ENCHANTED_BOOK, myWeaponsInventory, 15, "Flame");
        createDisplay(Material.ENCHANTED_BOOK, myWeaponsInventory, 17, "Infinity");
        createDisplay(Material.REDSTONE_BLOCK, myWeaponsInventory, 8, "Back");

        createDisplay(Material.ENCHANTED_BOOK, myToolsInventory, 10, "Unbreaking");
        createDisplay(Material.ENCHANTED_BOOK, myToolsInventory, 12, "Efficiency");
        createDisplay(Material.ENCHANTED_BOOK, myToolsInventory, 14, "Luck");
        createDisplay(Material.ENCHANTED_BOOK, myToolsInventory, 16, "Lure");
        createDisplay(Material.REDSTONE_BLOCK, myToolsInventory, 8, "Back");
//The first parameter, is the slot that is assigned to. Starts counting at 0
    }

    public static void createDisplay(Material material, Inventory inv, int Slot, String name) {
        ItemStack item = new ItemStack(material, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        inv.setItem(Slot, item);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection section = super.getDefaultConfig();
        section.set("featfallfour", Double.valueOf(0.10));
        section.set("featfallthree", Double.valueOf(0.25));
        section.set("featfalltwo", Double.valueOf(0.50));
        section.set("featfallone", Double.valueOf(1.0));
        section.set("respthree", Double.valueOf(0.25));
        section.set("resptwo", Double.valueOf(0.50));
        section.set("respone", Double.valueOf(1.0));
        section.set("thorthree", Double.valueOf(0.25));
        section.set("thortwo", Double.valueOf(0.50));
        section.set("thorone", Double.valueOf(1.0));
        section.set("knoctwo", Double.valueOf(0.50));
        section.set("knocone", Double.valueOf(1.0));
        section.set("firetwo", Double.valueOf(0.50));
        section.set("fireone", Double.valueOf(1.0));
        section.set("effifive", Double.valueOf(0.05));
        section.set("effifour", Double.valueOf(0.15));
        section.set("effithree", Double.valueOf(0.30));
        section.set("effitwo", Double.valueOf(0.60));
        section.set("effione", Double.valueOf(1.0));
        section.set("unbrthree", Double.valueOf(0.25));
        section.set("unbrtwo", Double.valueOf(0.50));
        section.set("unbrone", Double.valueOf(1.0));
        section.set("luckthree", Double.valueOf(0.25));
        section.set("lucktwo", Double.valueOf(0.50));
        section.set("luckone", Double.valueOf(1.0));
        section.set("lurethree", Double.valueOf(0.25));
        section.set("luretwo", Double.valueOf(0.50));
        section.set("lureone", Double.valueOf(1.0));
        section.set(SkillSetting.APPLY_TEXT.node(), "");
        section.set(SkillSetting.UNAPPLY_TEXT.node(), "");
        return section;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class SkillEnchantListener implements Listener {
        private final Skill skill;

        public SkillEnchantListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.LOW)
        public void onPlayerInteract(PlayerInteractEvent event) {
            if ((event.getClickedBlock() == null) || (event.getClickedBlock().getType() != Material.ENCHANTMENT_TABLE) || (event.getAction() != Action.RIGHT_CLICK_BLOCK)) {
                return;
            }
            event.setCancelled(true);
            Player player = event.getPlayer();
            if (!plugin.getCharacterManager().getHero(player).hasAccessToSkill("Enchant")) {
                Messaging.send(player, "You are not an enchanter!");
                return;
            }
            int count = 0;
            for (int z = -2; z <= 2; z++) {
                for (int x = -2; x <= 2; x++) {
                    if ((z == -2) || (z == 2) || (x == -2) || (x == 2)) {
                        if (event.getClickedBlock().getRelative(x, 0, z).getType() == Material.BOOKSHELF) {
                            count++;
                        }
                        if (event.getClickedBlock().getRelative(x, 1, z).getType() == Material.BOOKSHELF) {
                            count++;
                        }
                    }
                }
            }
            player.setMetadata("EnchantBookcases", new FixedMetadataValue(plugin, count >= 15 ? true : false));
            player.openInventory(myMainInventory);
        }

        @EventHandler(priority = EventPriority.NORMAL)
        public void onInventoryEnchantClick(InventoryClickEvent event) {
            Player player = (Player) event.getWhoClicked();
            ItemStack clicked = event.getCurrentItem();
            Inventory inventory = event.getInventory();
            if (inventory.getName().equals(myMainInventory.getName())) {
                event.setCancelled(true);
                InventoryView iv = event.getView();
                if ((event.getRawSlot() >= iv.getTopInventory().getSize()) || (clicked == null) || (!clicked.hasItemMeta()) || (!clicked.getItemMeta().hasDisplayName())){
                    return;
                }
                String name = clicked.getItemMeta().getDisplayName();
                if (name.equals("Armor")) {
                    player.closeInventory();
                    player.openInventory(myArmorInventory);
                } else if (name.equals("Weapons")) {
                    player.closeInventory();
                    player.openInventory(myWeaponsInventory);
                } else if (name.equals("Tools")) {
                    player.closeInventory();
                    player.openInventory(myToolsInventory);
                } else if (name.equals("Exit")) {
                    player.closeInventory();
                }
            } else if ((inventory.getName().equals(myWeaponsInventory.getName())) || (inventory.getName().equals(myToolsInventory.getName())) || (inventory.getName().equals(myArmorInventory.getName()))) {
                event.setCancelled(true);
                InventoryView iv = event.getView();
                if (event.getRawSlot() >= iv.getTopInventory().getSize()) {
                    return;
                }
                if ((clicked == null) || (((!clicked.getType().equals(Material.ENCHANTED_BOOK) || !clicked.getType().equals(Material.REDSTONE_BLOCK)) && (!clicked.hasItemMeta())))) {
                    return;
                }
                if (!clicked.getItemMeta().hasDisplayName()) {
                    return;
                }
                if (clicked.getItemMeta().getDisplayName().equals("Back")) {
                    player.closeInventory();
                    player.openInventory(myMainInventory);
                    return;
                }
                Hero caster = plugin.getCharacterManager().getHero(player);
                Enchantment name = Lib.enchants.get(clicked.getItemMeta().getDisplayName());
                if (player.getItemInHand() != null) {
                    ItemStack hand = player.getItemInHand();
                    if (!(Lib.enchantableItems.contains(hand.getType()))) {
                        player.closeInventory();
                        Messaging.send(player, "This item cannot be enchanted!");
                        return;
                    }
                    if (name.equals(Enchantment.KNOCKBACK)) {
                        Material x = hand.getType();
                        if (x.equals(Material.DIAMOND_HOE) || x.equals(Material.IRON_HOE) || x.equals(Material.GOLD_HOE) || x.equals(Material.STONE_HOE) || x.equals(Material.WOOD_AXE) || x.equals(Material.BOW)) {
                            Messaging.send(player, "You cannot enchant this item with this enchantment!");
                            return;
                        }
                    }
                    int cost = hand.getItemMeta().getEnchants().size();
                    if (cost >= 3 && !(hand.getItemMeta().getEnchants().containsKey(name))) {
                        Messaging.send(player, "This item has the maximum number of enchantments!");
                        return;
                    } else if (cost == 0) {
                        cost++;
                    } else {
                        cost += hand.getItemMeta().getEnchants().containsKey(name) ? 1 : 0;
                    }

                    ItemStack reagent = new ItemStack(Material.BLAZE_POWDER);
                    ItemMeta im = reagent.getItemMeta();
                    im.setDisplayName(ChatColor.YELLOW + "Enchanter's Dust");
                    List<String> loreList = new ArrayList<String>();
                    loreList.add(ChatColor.RED + "Used for enchanting");
                    im.setLore(loreList);
                    reagent.setItemMeta(im);

                    if (player.getInventory().containsAtLeast(reagent, (cost))) {
                        if (player.hasMetadata("EnchantBookcases")) {
                            reagent.setAmount(cost);
                            player.getInventory().removeItem(reagent);
                            int level = getEnchantmentLevel(caster, name);
                            if (!player.getMetadata("EnchantBookcases").get(0).asBoolean()) {
                                level = 1;
                                Messaging.send(player, "Your enchant was reduced to enchant-level 1 because you have less than 15 bookcases around your Enchanting Table.");
                            }
                            hand.addUnsafeEnchantment(name, level);
                            player.closeInventory();
                            Messaging.send(player, "You enchanted your item with $1 $2", clicked.getItemMeta().getDisplayName(), level);
                            player.removeMetadata("EnchantBookcases", plugin);
                        } else {
                            Messaging.send(player, "Enchant failed.");
                        }
                    } else {
                        Messaging.send(player, "You do not have enough $1 to enchant this item!", "Enchanter's Dust");
                        player.closeInventory();
                    }
                }
            }
        }
    }

    public int getEnchantmentLevel(Hero caster, Enchantment type) {
        Random generator = new Random();
        String name = type.getName();
        double value = generator.nextDouble();
        switch (name) {
            case "PROTECTION_FALL":
                if (SkillConfigManager.getUseSetting(caster, this, "featfallfour", 0.10, true) > value) {
                    return 4;
                } else if (SkillConfigManager.getUseSetting(caster, this, "featfallthree", 0.25, true) > value) {
                    return 3;
                } else if (SkillConfigManager.getUseSetting(caster, this, "featfalltwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "featfallone", 1.0, true) > value) {
                    return 1;
                }
            case "OXYGEN":
                if (SkillConfigManager.getUseSetting(caster, this, "respthree", 0.25, true) > value) {
                    return 3;
                } else if (SkillConfigManager.getUseSetting(caster, this, "resptwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "respone", 1.0, true) > value) {
                    return 1;
                }
            case "WATER_WORKER":
                return 1;
            case "THORNS":
                if (SkillConfigManager.getUseSetting(caster, this, "thorthree", 0.25, true) > value) {
                    return 3;
                } else if (SkillConfigManager.getUseSetting(caster, this, "thortwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "thorone", 1.0, true) > value) {
                    return 1;
                }
            case "KNOCKBACK":
                if (SkillConfigManager.getUseSetting(caster, this, "knoctwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "knocone", 1.0, true) > value) {
                    return 1;
                }
            case "FIRE_ASPECT":
                if (SkillConfigManager.getUseSetting(caster, this, "firetwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "fireone", 1.0, true) > value) {
                    return 1;
                }
            case "DIG_SPEED":
                if (SkillConfigManager.getUseSetting(caster, this, "effifive", 0.05, true) > value) {
                    return 5;
                } else if (SkillConfigManager.getUseSetting(caster, this, "effifour", 0.15, true) > value) {
                    return 4;
                } else if (SkillConfigManager.getUseSetting(caster, this, "effithree", 0.30, true) > value) {
                    return 3;
                } else if (SkillConfigManager.getUseSetting(caster, this, "effitwo", 0.60, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "effione", 1.0, true) > value) {
                    return 1;
                }
            case "DURABILITY":
                if (SkillConfigManager.getUseSetting(caster, this, "unbrthree", 0.25, true) > value) {
                    return 3;
                } else if (SkillConfigManager.getUseSetting(caster, this, "unbrtwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "unbrone", 1.0, true) > value) {
                    return 1;
                }
            case "ARROW_FIRE":
                return 1;
            case "ARROW_INFINITE":
                return 1;
            case "LUCK":
                if (SkillConfigManager.getUseSetting(caster, this, "luckthree", 0.25, true) > value) {
                    return 3;
                } else if (SkillConfigManager.getUseSetting(caster, this, "lucktwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "luckone", 1.0, true) > value) {
                    return 1;
                }
            case "LURE":
                if (SkillConfigManager.getUseSetting(caster, this, "lurethree", 0.25, true) > value) {
                    return 3;
                } else if (SkillConfigManager.getUseSetting(caster, this, "luretwo", 0.50, true) > value) {
                    return 2;
                } else if (SkillConfigManager.getUseSetting(caster, this, "lureone", 1.0, true) > value) {
                    return 1;
                }
            default:
                return 1;
        }
    }
}