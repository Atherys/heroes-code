package com.atherys.skills.SkillEnergyShield;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

public class SkillEnergyShield extends ActiveSkill {

    public SkillEnergyShield(Heroes plugin) {
        super(plugin, "EnergyShield");
        setDescription("Active\nYour EnergyShield absorbs part of the physical damage you take for a duration and explodes at the end, dealing damage an knocking back nearby enemies.");
        setUsage("/skill EnergyShield");
        setArgumentRange(0, 0);
        setIdentifiers("skill EnergyShield");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new SkillDamageListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 15000);
        node.set("damage-reduction", 0.4);
        node.set("shield-hp", 100);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set("explosion-damage", 15);
        node.set("coeficient", 1.0);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 15000, false);
        double damageReduction = SkillConfigManager.getUseSetting(hero, this, "damage-reduction", 0.4, false);
        double shieldHP = SkillConfigManager.getUseSetting(hero, this, "shield-hp", 100, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        double explosionDamage = SkillConfigManager.getUseSetting(hero, this, "explosion-damage", 15, false);
        hero.addEffect(new EnergyShieldEffect(this, duration, damageReduction, shieldHP, radius, explosionDamage,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class EnergyShieldEffect extends PeriodicExpirableEffect {
        private double damageReduction;
        private double shieldHP;
        private int radius;
        private double explosionDamage;

        public EnergyShieldEffect(Skill skill, long duration, double damageReduction, double shieldHP, int radius, double explosionDamage,Hero caster) {
            super(skill, "EnergyShield",caster.getPlayer(), 1000, duration);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.MAGIC);
            this.damageReduction = damageReduction;
            this.shieldHP = shieldHP;
            this.radius = radius;
            this.explosionDamage = explosionDamage;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), "$1's $2 exploded!", hero.getPlayer().getDisplayName(), "EnergyShield");
                Player player = hero.getPlayer();
                double coeficient = SkillConfigManager.getUseSetting(hero, skill, "coeficient", 1.0, false);
                for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                    if (e instanceof Player || e instanceof org.bukkit.entity.Monster) {
                        LivingEntity le = (LivingEntity) e;
                        if (damageCheck(player, le)) {
                            damageEntity(le, player, explosionDamage, DamageCause.MAGIC);
                            if (e instanceof Player) {
                                Player target = (Player) e;
                                addSpellTarget(target, hero);
                                double cc = (radius - target.getLocation().distance(player.getLocation())) * coeficient;
                                Vector v = target.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
                                v = v.multiply(cc);
                                v.setY(0.5);
                                target.setVelocity(v);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (double[] coords : Lib.playerParticleCoords) {
                Location location = player.getLocation();
                location.add(coords[0], coords[1], coords[2]);
                location.getWorld().spawnParticle(Particle.DRIP_LAVA, location, 1);
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }

        public double getShieldHP() {
            return shieldHP;
        }

        public void setShieldHP(double shieldHP) { this.shieldHP = shieldHP; }

        public double getDamageReduction() { return damageReduction; }
    }

    public class SkillDamageListener implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.getDamage() == 0 || event.getCause() != DamageCause.ENTITY_ATTACK || !(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (!hero.hasEffect("EnergyShield")) {
                return;
            }
            EnergyShieldEffect es = (EnergyShieldEffect) hero.getEffect("EnergyShield");
            double damage = event.getDamage();
            double damageReduced = damage * es.getDamageReduction();
            double shieldHP = es.getShieldHP();
            if (shieldHP - damageReduced <= 0) {
                event.setDamage(damage - (damageReduced - shieldHP));
                hero.removeEffect(es);
            } else {
                event.setDamage(damage - damageReduced);
                es.setShieldHP(shieldHP - damageReduced);
            }
        }
    }
}
