package com.atherys.skills.SkillEnvenom;

import com.atherys.effects.BleedPeriodicDamageEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SkillEnvenom extends ActiveSkill {
    public SkillEnvenom(Heroes plugin) {
        super(plugin, "Envenom");
        setDescription("Active\nImbibes nearby party members' weapons with poison, causing their next attack to apply attacks DoT dealing $1 damage every $2s for $3");
        setUsage("/skill Envenom");
        setArgumentRange(0, 0);
        setIdentifiers("skill Envenom");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.AREA_OF_EFFECT);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("attacks", 5);
        node.set("buff-duration", 20000);
        node.set("damage-period", 1000);
        node.set("poison-duration", 10000);
        node.set("poison-damage", 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 10000, true);
        int a = SkillConfigManager.getUseSetting(hero, this, "attacks", 5, true);
	    long period = SkillConfigManager.getUseSetting(hero, this, "damage-period", 5000,false);
	    long PoisonDuration = SkillConfigManager.getUseSetting(hero, this, "poison-duration", 25000, false);
	    double damage = SkillConfigManager.getUseSetting(hero, this, "poison-damage", 7.5, false);
        if (hero.hasParty()) {
            for (Hero partyhero : hero.getParty().getMembers()) {
                if (partyhero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= r * r) {
                    partyhero.addEffect(new EnvenomBuffEffect(this, duration, a , period, PoisonDuration, damage,hero));
                }
            }
        } else {
            hero.addEffect(new EnvenomBuffEffect(this, duration, a, period, PoisonDuration, damage,hero));
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class EnvenomBuffEffect extends ExpirableEffect {
        private int attacks;
		private long period;
	    private long poisondur;
	    private double damage;

        public EnvenomBuffEffect(Skill skill, long duration, int attacks, long period, long poisonDuration, double damage,Hero caster) {
            super(skill, "EnvenomBuff",caster.getPlayer(), duration);
            this.attacks = attacks;
	        this.period = period;
	        this.poisondur=poisonDuration;
	        this.damage=damage;
        }

        public int getAttacksLeft() {
            return attacks;
        }
	    public long getPeriod(){ return period; }
	    public long getPoisondur(){return poisondur;}
	    public double getDamage(){return damage;}


        public void setAttacksLeft(int attacks) {
            this.attacks = attacks;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "Your blade is no longer poisoned!");
        }
    }

    public class EnvenomPoisonEffect extends BleedPeriodicDamageEffect {
        public EnvenomPoisonEffect(Skill skill, long period, long duration, double damage, Player player) {
            super(skill, "EnvenomPoison",player, period, duration, damage);
            types.add(EffectType.POISON);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "Your body recovered from poison.");
        }
    }

    @Override
    public String getDescription(Hero hero) {
        long period = SkillConfigManager.getUseSetting(hero, this, "damage-period", 1000, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, "poison-duration", 10, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, "poison-damage", 10, false);
        return getDescription().replace("$1", damage + "").replace("$2", period / 1000 + "").replace("$3", duration / 1000 + "") + Lib.getSkillCostStats(hero, this);
    }

    public class SkillEntityListener implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(WeaponDamageEvent event) {
            if (event.getDamage() == 0 || !(event.getEntity() instanceof LivingEntity)) {
                return;
            }
            if (!(event.getDamager() instanceof Hero)) {
                return;
            }
            Hero hero = (Hero) event.getDamager();
            if (!(hero.hasEffect("EnvenomBuff"))) {
                return;
            }
            EnvenomBuffEffect envenomBuffEffect = (EnvenomBuffEffect) hero.getEffect("EnvenomBuff");

            plugin.getCharacterManager().getCharacter((LivingEntity) event.getEntity()).addEffect(new EnvenomPoisonEffect(skill, envenomBuffEffect.getPeriod(),envenomBuffEffect.getPoisondur(), envenomBuffEffect.damage, hero.getPlayer()));
            envenomBuffEffect.setAttacksLeft(envenomBuffEffect.getAttacksLeft() - 1);
            if (envenomBuffEffect.getAttacksLeft() < 1) {
                hero.removeEffect(hero.getEffect("EnvenomBuff"));
            }
        }
    }
}