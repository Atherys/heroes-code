package com.atherys.skills.SkillEscapeArtist;


import com.atherys.effects.BetterInvis;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

public class SkillEscapeArtist extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillEscapeArtist(Heroes plugin) {
        super(plugin, "EscapeArtist");
        setDescription("Active\n You break free of any effects that impede your movement, briefly turning invisible.");
        setUsage("/skill escapeartist");
        setArgumentRange(0, 0);
        setIdentifiers("skill escapeartist", "skill eartist", "skill escape");
        this.setTypes(new SkillType[] { SkillType.MOVEMENT_INCREASING, SkillType.DISABLE_COUNTERING, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.STEALTHY });
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("chance", Double.valueOf(0.5D));
        node.set(SkillSetting.APPLY_TEXT.node(), "You slip away!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You come back into view!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "You slip away!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You come back into view!");
    }


    public SkillResult use(final Hero hero, final String[] args) {
        boolean removed = false;
        for (final Effect effect : hero.getEffects()) {
            if ((effect.isType(EffectType.DISABLE) || effect.isType(EffectType.SLOW) || effect.isType(EffectType.STUN) || effect.isType(EffectType.ROOT)) && (!(effect.isType(EffectType.UNBREAKABLE)))) {

                double chance = SkillConfigManager.getUseSetting(hero, this, "chance", 0.5D, false);
                double random = Math.random();
                if (random < chance) {
                    hero.removeEffect(effect);
                    removed = true;
                }
            }
        }
        if (removed) {
            this.broadcastExecuteText(hero);
            hero.addEffect(new BetterInvis(this, 3000, this.applyText, this.expireText, hero));
            return SkillResult.NORMAL;
        }
        hero.getPlayer().sendMessage(ChatColor.GRAY + "There is no effect impeding your movement!");
        return SkillResult.INVALID_TARGET_NO_MSG;
    }
/*
    public SkillResult use(Hero hero, String[] args) {
        boolean removed = false;
        for (Effect effect : hero.getEffects()) {
            if (((effect.isType(EffectType.STUN)) || (effect.isType(EffectType.DISABLE)) ||
                    (effect.isType(EffectType.SLOW)) || (effect.isType(EffectType.ROOT)))
                    && (effect.isType(EffectType.UNBREAKABLE))){
                removed = true;
                double chance = SkillConfigManager.getUseSetting(hero, this, "chance", 0.5D, false);
                double random = Math.random();
                if (random < chance) {
                    hero.removeEffect(effect);
                }
            }
        }
        if (removed) {
            broadcastExecuteText(hero);
            hero.addEffect(new BetterInvis(this, 3000, this.applyText, this.expireText, hero));
            return SkillResult.NORMAL;
        }
        Messaging.send(hero.getPlayer(), "There is no effects impeding your movement!");
        return SkillResult.INVALID_TARGET_NO_MSG;
    }
    */

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}