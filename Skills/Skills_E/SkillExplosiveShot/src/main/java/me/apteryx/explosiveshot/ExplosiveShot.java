package me.apteryx.explosiveshot;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import me.apteryx.explosiveshot.effects.ExplosiveShotEffect;
import me.apteryx.explosiveshot.events.ExplosiveBowShotEvent;
import me.apteryx.explosiveshot.events.ExplosiveProjectileHitEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author apteryx
 * @time 6:12 PM
 * @since 12/17/2016
 */
public class ExplosiveShot extends ActiveSkill {

    private double explosiveDamage, radius;
    private int attacks;
    private long duration;
    private static ExplosiveShot explosiveShot;

    public ExplosiveShot(Heroes plugin) {
        super(plugin, "ExplosiveShot");
        setDescription("Your next %attacks% fully drawn arrows within %duration% seconds will explode on impact dealing %damage% true damage to enemies within %radius% blocks.");
        setIdentifiers("skill explosiveshot");
        setUsage("/skill explosiveshot");
        explosiveShot = this;
        Bukkit.getPluginManager().registerEvents(new ExplosiveBowShotEvent(), this.plugin);
        Bukkit.getPluginManager().registerEvents(new ExplosiveProjectileHitEvent(), this.plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        explosiveDamage = SkillConfigManager.getUseSetting(hero, this, "explosive-damage", 30.0, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 5.0, false);
        attacks = SkillConfigManager.getUseSetting(hero, this, "attacks", 3, false);
        duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        hero.addEffect(new ExplosiveShotEffect(this, "ExplosiveShotImbue", hero.getPlayer(), duration, attacks));
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        explosiveDamage = SkillConfigManager.getUseSetting(hero, this, "explosive-damage", 30.0, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 5.0, false);
        attacks = SkillConfigManager.getUseSetting(hero, this, "attacks", 3, false);
        duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        return getDescription().replace("%attacks%", attacks+"").replace("%duration%", duration / 1000+"").replace("%damage%", explosiveDamage+"").replace("%radius%", radius+"");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("explosive-damage", 30.0);
        node.set("radius", 5.0);
        node.set("attacks", 3);
        return node;
    }



    public static ExplosiveShot getInstance() {
        return explosiveShot;
    }

    public double getRadius() {
        return radius;
    }

    public double getExplosiveDamage() {
        return explosiveDamage;
    }
}