package me.apteryx.explosiveshot.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Player;

/**
 * @author apteryx
 * @time 6:21 PM
 * @since 12/17/2016
 */
public class ExplosiveShotEffect extends ExpirableEffect {

    private int shotsLeft;

    public ExplosiveShotEffect(Skill skill, String name, Player applier, long duration, int maxShots) {
        super(skill, name, applier, duration);
        this.shotsLeft = maxShots;
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        broadcast(hero.getPlayer().getLocation(), "$1 sets up $2 explosive shots.", hero.getName(), shotsLeft);
    }

    public void setShotsLeft(int shotsLeft) {
        this.shotsLeft = shotsLeft;
    }

    public int getShotsLeft() {
        return shotsLeft;
    }


}