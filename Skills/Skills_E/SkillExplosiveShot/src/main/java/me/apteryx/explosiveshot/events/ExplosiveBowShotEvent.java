package me.apteryx.explosiveshot.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import me.apteryx.explosiveshot.ExplosiveShot;
import me.apteryx.explosiveshot.effects.ExplosiveShotEffect;
import org.bukkit.Effect;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author apteryx
 * @time 6:22 PM
 * @since 12/17/2016
 */
public class ExplosiveBowShotEvent implements Listener {

    @EventHandler
    public void onExplosiveBowShot(EntityShootBowEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (hero != null) {
                if (hero.hasEffect("ExplosiveShotImbue")) {
                    ExplosiveShotEffect explosiveShot = (ExplosiveShotEffect) hero.getEffect("ExplosiveShotImbue");
                    if (explosiveShot != null) {
                        if (explosiveShot.getShotsLeft() != 0) {
                            if (event.getForce() == 1.0) {
                                explosiveShot.setShotsLeft(explosiveShot.getShotsLeft() - 1);
                                event.getEntity().sendMessage("\2477You have \247F" + (explosiveShot.getShotsLeft() > 0 ? explosiveShot.getShotsLeft() : "no") + "\2477 explosive shots left.");
                                event.getProjectile().setMetadata("ExplosiveShotForce", new FixedMetadataValue(ExplosiveShot.getInstance().plugin, "Fuck you for looking at my code."));
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        if (event.getProjectile().isDead()) cancel();
                                        event.getProjectile().getWorld().playEffect(event.getProjectile().getLocation(), Effect.LAVA_POP, 10);
                                        event.getProjectile().getWorld().spigot().playEffect(event.getProjectile().getLocation(), Effect.FLAME);
                                        event.getProjectile().getWorld().spigot().playEffect(event.getProjectile().getLocation(), Effect.LARGE_SMOKE);
                                    }
                                }.runTaskTimer(ExplosiveShot.getInstance().plugin, 0, 1);
                            }
                        }
                    }
                }
            }
        }
    }

}