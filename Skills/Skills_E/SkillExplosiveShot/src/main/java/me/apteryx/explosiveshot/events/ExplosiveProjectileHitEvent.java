package me.apteryx.explosiveshot.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import me.apteryx.explosiveshot.ExplosiveShot;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

/**
 * @author apteryx
 * @time 6:22 PM
 * @since 12/17/2016
 */
public class ExplosiveProjectileHitEvent implements Listener {

    @EventHandler
    public void onExplosiveProjectileHit(ProjectileHitEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity().getShooter());
            if (event.getEntity() instanceof Arrow) {
                if (event.getEntity().hasMetadata("ExplosiveShotForce")) {
                    double radius = ExplosiveShot.getInstance().getRadius();
                    for (Entity e : event.getEntity().getNearbyEntities(radius, radius, radius)) {
                        if (e instanceof LivingEntity) {
                            if (e instanceof Player) {
                                if (hero.getParty() != null) {
                                    if (hero.getParty().isPartyMember((Player) event.getHitEntity())) {
                                        continue;
                                    }
                                }
                            }

                            if (e != hero.getPlayer()) {
                                ExplosiveShot.getInstance().damageEntity((LivingEntity) e, hero.getPlayer(), ExplosiveShot.getInstance().getExplosiveDamage(), EntityDamageEvent.DamageCause.CUSTOM, false);
                            }
                        }
                    }

                    event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 1.0F);
                    event.getEntity().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, event.getEntity().getLocation(), 2);
                    event.getEntity().remove();
                }

            }
        }
    }
}