/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.expose;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ExposeEffect extends ExpirableEffect implements Listener {

    public ExposeEffect(Skill skill, Hero target, long dur) {
        super(skill, "ExposeEff", target.getPlayer(), dur);
        Bukkit.getPluginManager().registerEvents(this, Heroes.getInstance());
        this.types.add(EffectType.HARMFUL);
        this.types.add(EffectType.DISPELLABLE);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, (int) getDuration(), 1, true, false));
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (hero.getPlayer().hasPotionEffect(PotionEffectType.GLOWING)) {
            hero.getPlayer().removePotionEffect(PotionEffectType.GLOWING);
        }
    }

}