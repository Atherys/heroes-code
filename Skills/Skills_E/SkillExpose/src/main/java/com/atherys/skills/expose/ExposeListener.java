/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.expose;

import com.herocraftonline.heroes.api.events.SkillUseEvent;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import static com.atherys.skills.expose.SkillExpose.getHero;
import static com.herocraftonline.heroes.characters.skill.Skill.damageCheck;

public class ExposeListener implements Listener {

    private Skill skill = null;
    private Player lastHit = null;

    public ExposeListener(Skill skill) {
        this.skill = skill;
    }

    @EventHandler
    public void onEntityDamage(WeaponDamageEvent event) {
        boolean nonPlayerTarget = !(event.getEntity() instanceof Player);
        boolean nonPlayerSource = !(event.getAttackerEntity() instanceof Player);
        boolean noDamage = (event.getDamage() == 0);
        if (nonPlayerTarget || nonPlayerSource || noDamage) return;
        Hero hero = getHero((Player) event.getAttackerEntity());
        if (!hero.getHeroClass().hasSkill("Expose")) return;
        if (!damageCheck(event.getDamager().getEntity(), (Player) event.getEntity())) return;
        Hero target = getHero((Player) event.getEntity());
        if (target.getPlayer() != lastHit) {
            if (lastHit != null && getHero(lastHit).hasEffect("ExposeEff")) {
                Hero last = getHero(lastHit);
                last.removeEffect(last.getEffect("ExposeEff"));
            }
            lastHit = target.getPlayer();
        }
        long dur = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION.node(), 3000, false);
        target.addEffect(new ExposeEffect(skill, target, dur));
    }

    @EventHandler
    public void onSkillUse(SkillUseEvent event) {
        String skillName = event.getSkill().getName();
        if (!getHero(event.getPlayer()).hasEffect("ExposeEff")) return;
        if (skillName.equalsIgnoreCase("smoke") || skillName.equalsIgnoreCase("massinvis")) {
            event.setCancelled(true);
            event.getPlayer().sendMessage("\u00A77You cannot disappear while \u00A7fExposed\u00A77!");
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerDmg(WeaponDamageEvent event) {
        Entity d1 = event.getAttackerEntity();
        Entity d2 = event.getEntity();
        if (!(d1 instanceof Player) || !(d2 instanceof Player)) return;
        if (!getHero((Player)d2).hasEffect("ExposeEff")) return;
        if (!Skill.damageCheck((Player)d1, (LivingEntity) d2)) return;
        Hero hero = getHero((Player)d1);
        double bonus = SkillConfigManager.getUseSetting(hero, skill, "bonus-damage", 0.2, false) + 1;
        if ( hero.getPlayer().getInventory().getItemInMainHand() == null ) return;
        double dmg = hero.getHeroClass().getItemDamage(hero.getPlayer().getInventory().getItemInMainHand().getType()).getScaled(hero) ;
        event.setDamage(dmg * bonus);
    }

}