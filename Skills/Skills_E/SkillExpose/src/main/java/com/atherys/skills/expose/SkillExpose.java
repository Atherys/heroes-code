/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.expose;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class SkillExpose extends PassiveSkill implements Listener {

    public SkillExpose(Heroes plugin) {
        super(plugin, "Expose");
        setDescription("Passive\nThe most recent enemy you melee becomes Exposed, glowing and receiving $1% more damage from all sources. Effect expires if a new enemy is meleed or after $2 seconds.");
        setUsage("/skill expose");
        setArgumentRange(0, 0);
        setIdentifiers("skill expose");
        setTypes(SkillType.DEBUFFING);
        Bukkit.getPluginManager().registerEvents(new ExposeListener(this), Heroes.getInstance());
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 30000);
        node.set("bonus-damage", 0.2);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double bonusDmg = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 0.2, false);
        long dur = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 3000, false);
        return getDescription().replace("$1", String.valueOf(bonusDmg * 100)).replace("$2", String.valueOf(dur/ 1000));
    }

    public static Hero getHero(Player player) {
        return Heroes.getInstance().getCharacterManager().getHero(player);
    }

}