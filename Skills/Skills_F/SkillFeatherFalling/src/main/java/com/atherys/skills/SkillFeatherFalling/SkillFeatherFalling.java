package com.atherys.skills.SkillFeatherFalling;


import com.atherys.effects.FeatherFallingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillFeatherFalling extends ActiveSkill {
    public SkillFeatherFalling(Heroes plugin) {
        super(plugin, "FeatherFalling");
        setDescription("Active\n You and your party within $1 float safely to the ground for $2 seconds.");
        setUsage("/skill featherfalling");
        setArgumentRange(0, 0);
        setIdentifiers("skill featherfalling");
        setTypes(SkillType.MOVEMENT_INCREASING, SkillType.BUFFING, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(60000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(25));
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        if (!hero.hasParty()) {
            hero.addEffect(new FeatherFallingEffect(this, duration,hero));
        } else {
            for (Hero sfHero : hero.getParty().getMembers()) {
                Player sfPlayer = sfHero.getPlayer();
                if ((sfPlayer.getWorld().equals(player.getWorld())) && (sfPlayer.getLocation().distanceSquared(player.getLocation()) <= radius * radius) && (!sfHero.hasEffect("Feather Falling"))) {
                    sfHero.addEffect(new FeatherFallingEffect(this, duration,hero));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 25, false);
        String description = getDescription().replace("$1", radius + "").replace("$2", duration + "");
        return description + Lib.getSkillCostStats(hero, this);
    }
}
