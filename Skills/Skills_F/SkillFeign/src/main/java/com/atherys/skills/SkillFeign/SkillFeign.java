package com.atherys.skills.SkillFeign;

/**
 * Created by Arthur on 5/17/2015.
 */

import com.atherys.effects.VanishEffect;
import com.atherys.heroesaddon.lib.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class SkillFeign extends ActiveSkill {

    public SkillFeign(Heroes plugin) {
        super(plugin, "Feign");
        setDescription("Active\n You display a death message, but instead go auto-smoke for $2s.");
        setUsage("/skill Feign");
        setArgumentRange(0, 0);
        setIdentifiers("skill feign");
        setTypes(SkillType.ABILITY_PROPERTY_ILLUSION, SkillType.BUFFING, SkillType.STEALTHY, SkillType.UNINTERRUPTIBLE, SkillType.DISABLE_COUNTERING);
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, "duration", 6000, false) / 1000;
        String description = getDescription().replace("$2", duration + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 1000);
        node.set(SkillSetting.APPLY_TEXT.node(), "You vanished in a cloud of smoke!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You reappeared");
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        String pn = player.getDisplayName();
        if ((hero.isInCombat()) && (hero.getCombatants().keySet().size() > 0)) {
            if (hero.hasEffect("Smoke")){
                hero.removeEffect(hero.getEffect("Smoke"));

            }
            LivingEntity attacker = hero.getCombatEffect().getLastCombatant();
            if (attacker instanceof Player) {
                broadcast(player.getLocation(), ChatColor.WHITE + pn + " was slain by " + ((Player) attacker).getDisplayName());
            } else if (attacker instanceof Monster) {
                broadcast(player.getLocation(), ChatColor.WHITE + pn + " was slain by " + attacker.getName());
            } else {
                broadcast(player.getLocation(), ChatColor.WHITE + pn + " died");
            }
        } else {
            broadcast(player.getLocation(), ChatColor.WHITE + pn + " died");
        }

        for(Effect effect : hero.getEffects()) {
            //Bukkit.getLogger().info("Effect: " + effect.getName());
            effect.types.stream().filter(type -> type.name().equalsIgnoreCase("STUN") || type.name().equalsIgnoreCase("ROOT") || type.name().equalsIgnoreCase("DISABLE") || type.name().equalsIgnoreCase("MOVEMENT_PREVENTING")).forEach(type -> {
                hero.removeEffect(effect);
            });
        }

        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, false);
        Set<String> x = new HashSet<>();
        x.add("Feign");
        hero.addEffect(new VanishEffect(this, duration, x,hero));
        return SkillResult.NORMAL;
    }
}
