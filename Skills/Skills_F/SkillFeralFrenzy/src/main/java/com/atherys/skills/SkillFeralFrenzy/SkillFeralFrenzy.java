package com.atherys.skills.SkillFeralFrenzy;


import com.atherys.effects.MightEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.ArrayList;
import java.util.List;

public class SkillFeralFrenzy extends ActiveSkill {
	private String applyText;

	public SkillFeralFrenzy(Heroes plugin) {
		super(plugin, "FeralFrenzy");
		setDescription("Active\n The Warden gains increased strength, speed and spawns wolfs that will attack the enemy");
		setUsage("/skill FeralFrenzy");
		setArgumentRange(0, 0);
		setIdentifiers("skill FeralFrenzy");
		setTypes(SkillType.BUFFING, SkillType.SUMMONING, SkillType.SILENCEABLE);
		Bukkit.getServer().getPluginManager().registerEvents(new wolfListener(this), plugin);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), 8000);
		node.set("haste-multiplier", 2);
		node.set("bonus-damage", 4D);
		node.set("wolves-spawned", 5);
		node.set("wolf-time", 5000);
		return node;
	}

	@Override
	public void init() {
		super.init();
		applyText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero% used %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
	}
	@Override
	public SkillResult use(Hero hero, String[] p1) {
		Player player = hero.getPlayer();

		long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 8000, false);
		int hasteMultiplier = SkillConfigManager.getUseSetting(hero, this, "haste-multiplier", 2, false);
		double damage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 4D, false);
		double wolfs = SkillConfigManager.getUseSetting(hero, this, "wolves-spawned", 5, false);
		long wolfLife = SkillConfigManager.getUseSetting(hero, this, "wolf-time", 5000, false);
		long wolfhp = SkillConfigManager.getUseSetting(hero, this, "wolf-hp", 150, false);
		double wolfdamage = SkillConfigManager.getUseSetting(hero, this, "wolf-damage", 1, false);

		hero.addEffect(new MightEffect(this, duration, damage,hero));
		hero.addEffect(new FeralFrenzyEffect(this, duration, hasteMultiplier,hero));

		List<Wolf> wolfList = new ArrayList<>();

		for (int i = 0; i < wolfs; i++) {
			Wolf swolf = (Wolf) player.getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
			swolf.setCollarColor(DyeColor.GREEN);
			swolf.setCustomName(player.getName() + "'s Wolf");
			swolf.setCustomNameVisible(true);
			plugin.getCharacterManager().getMonster(swolf).setExperience(0);
			swolf.setOwner(player);
			swolf.setMaxHealth(wolfhp);
			swolf.setHealth(wolfhp);
			wolfList.add(swolf);
		}
		hero.addEffect(new WolfEffect(this, wolfLife, wolfList,hero));
		broadcast(hero.getPlayer().getLocation(), "$1 used $2!", hero.getName(), "FeralFrenzy");
		return SkillResult.NORMAL;
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}


	public class FeralFrenzyEffect extends ExpirableEffect {

		public FeralFrenzyEffect(Skill skill, long duration, int multiplier,Hero caster) {
			super(skill, "FeralFrenzyEffect",caster.getPlayer(), duration);
			this.types.add(EffectType.BENEFICIAL);
			this.types.add(EffectType.SPEED);
			this.types.add(EffectType.PHYSICAL);
			addMobEffect(3, (int) ((duration / 1000) * 20), multiplier, false);
			addMobEffect(1, (int) ((duration / 1000) * 20), multiplier, false);
		}

		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
		}

		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
		}
	}

	public class WolfEffect extends ExpirableEffect {
		private List<Wolf> wolfs;

		public WolfEffect(Skill skill, long duration, List<Wolf> wolfs,Hero caster) {
			super(skill, "WolfEffect",caster.getPlayer(), duration);
			this.wolfs = wolfs;
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			for (Wolf wolf : wolfs) {
				wolf.remove();
				wolf.setHealth(0);
			}
		}

		public List<Wolf> getwolfs() {
			return wolfs;
		}
	}

	public class wolfListener implements Listener {
		private Skill skill;

		public wolfListener(Skill skill) {
			this.skill = skill;
		}


		@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
		public void onOwnerMeleeDamage(WeaponDamageEvent event) {
			if (event.getDamage() == 0 || !(event.getEntity() instanceof LivingEntity)) {
				return;
			}
			if (event.getDamager() instanceof Hero) {
				Hero hero = (Hero) event.getDamager();
				if (hero.hasEffect("WolfEffect")) {
					List<Wolf> wolf = ((WolfEffect) hero.getEffect("WolfEffect")).getwolfs();
					for (Wolf temp : wolf) {
						if (!temp.equals(event.getEntity()) && (temp.getTarget() == null || !temp.getTarget().equals(event.getEntity()))) {
							temp.setTarget((LivingEntity) event.getEntity());
						}
					}
				}
			}
		}

		@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
		public void onOwnerRangedDamage(EntityDamageByEntityEvent event) {
			if (!(event.getEntity() instanceof LivingEntity) || !(event.getDamager() instanceof Arrow)) {
				return;
			}
			LivingEntity target = (LivingEntity) event.getEntity();
			Arrow arrow = (Arrow) event.getDamager();
			if (!(arrow.getShooter() instanceof Player)) {
				return;
			}
			Player player = (Player) arrow.getShooter();
			if (player.equals(target)) {
				return;
			}
			Hero hero = plugin.getCharacterManager().getHero(player);
			if (hero.hasEffect("WolfEffect")) {
				List<Wolf> wolf = ((WolfEffect) hero.getEffect("WolfEffect")).getwolfs();
				for (Wolf temp : wolf) {
					if (!temp.equals(target) && (temp.getTarget() == null || !temp.getTarget().equals(target))) {
						temp.setTarget(target);
					}
				}
			}
		}

		@EventHandler
		public void onWolfDamage(EntityDamageByEntityEvent event) {
			if (event.isCancelled() || event.getDamage() == 0 || !(event.getDamager() instanceof Wolf) || !(event.getEntity() instanceof LivingEntity)) {
				return;
			}
			Wolf wolf = (Wolf) event.getDamager();
			if (wolf.isTamed()) {
				if (wolf.getOwner() instanceof Player) {
					Hero hero = plugin.getCharacterManager().getHero((Player) wolf.getOwner());
					if (hero.hasEffect("WolfEffect")) {
						List<Wolf> wolfs = ((WolfEffect)hero.getEffect("WolfEffect")).getwolfs();
						if (wolfs.contains(wolf)) {
							if (damageCheck(hero.getPlayer(), (LivingEntity) event.getEntity())) {
								event.setCancelled(true);
							}
							double wolfdamage = SkillConfigManager.getUseSetting(hero,skill, "wolf-damage", 1, false);
							damageEntity((LivingEntity) event.getEntity(), hero.getPlayer(), wolfdamage, EntityDamageEvent.DamageCause.ENTITY_ATTACK, false);
						} else {
							event.setDamage(0);
							event.setCancelled(true);
						}
					}
				}
			}
		}
	}
}




