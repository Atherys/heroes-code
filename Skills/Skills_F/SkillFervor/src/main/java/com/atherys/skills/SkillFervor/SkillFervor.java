package com.atherys.skills.SkillFervor;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SkillFervor extends TargettedSkill {
    public SkillFervor(Heroes plugin) {
        super(plugin, "Fervor");
        setDescription("Targeted\n Jumps between $1 enemies stunning them and dealing $2 physical damage every $3s for $4s");
        setUsage("/skill Fervor");
        setArgumentRange(0, 0);
        setIdentifiers("skill Fervor");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(30));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(1000));
        node.set("jumps", Integer.valueOf(3));
        node.set("distance-between-jumps", Integer.valueOf(5));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 30, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 1000, false);
        int jumps = SkillConfigManager.getUseSetting(hero, this, "jumps", 3, false);
        return getDescription().replace("$1", jumps + "").replace("$2", damage + "").replace("$3", period / 1000 + "").replace("$4", duration / 1000 + "") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if ((player.equals(target))) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 30, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 1000, false);
        int jumps = SkillConfigManager.getUseSetting(hero, this, "jumps", 3, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, "distance-between-jumps", 3, false);
        plugin.getCharacterManager().getHero((Player) target).addEffect(new FervorEffect(this, period, duration, damage, player, jumps, distance));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class FervorEffect extends PeriodicDamageEffect {
        private int jumps;
        private int distance;
        private Skill skill;

        // private Player caster;
        public FervorEffect(Skill skill, long period, long duration, double damage, Player caster, int jumps, int distance) {
            super(skill, "FervorEffect",caster, period, duration, damage );
            types.add(EffectType.HARMFUL);
            types.add(EffectType.MAGIC);
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.JUMP_BOOST);
            this.skill = skill;
            this.jumps = jumps;
            // this.caster = caster;
            this.distance = distance;
        }

        @Override
        public void removeFromHero(Hero hero) {
            if (jumps - 1 <= 0) {
                super.removeFromHero(hero);
                return;
            }
            FervorEffect feffect = new FervorEffect(skill, getPeriod(), getDuration(), getTickDamage(), getApplier(), jumps - 1, distance);
            getNextTarget(feffect, hero, getApplier());
            super.removeFromHero(hero);
        }

        private void getNextTarget(FervorEffect effect, Hero hero, Player caster) {
            Player target = hero.getPlayer();
            List<Player> l = new ArrayList<>();
            for (Entity e : target.getNearbyEntities(distance, distance, distance)) {
                if (!(e instanceof Player)) {
                    continue;
                }
                Player pl = (Player) e;
                if (damageCheck(caster, (LivingEntity) pl)) {
                    l.add(pl);
                }
            }
            if (l.isEmpty()) {
                return;
            }
            // Get the closest player
            Player p = l.get(0);
            for (Player player : l) {
                if ((player.getLocation().distanceSquared(target.getLocation())) <= p.getLocation().distanceSquared(target.getLocation())) {
                    p = player;
                }
            }
            plugin.getCharacterManager().getHero(p).addEffect(effect);
            l.clear();
        }
    }
}