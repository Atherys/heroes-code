package com.atherys.skills.SkillFester;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainManaEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillFester extends ActiveSkill {
    public String applyText;
    public String expireText;

    public SkillFester(Heroes plugin) {
        super(plugin, "Fester");
        setDescription("Festers enemies around you, dealing damage over time and stuns them if they are affected by either Outbreak or Affliction.");
        setUsage("/skill Fester");
        setArgumentRange(0, 0);
        setIdentifiers("skill Fester");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE, SkillType.DEBUFFING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE_TICK.node(), 3);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.DURATION.node(), 12000);
        node.set(SkillSetting.PERIOD.node(), 3000);
        node.set("stun-duration", 750);
        node.set("mana-gain", 5);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% has caught %hero%s %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from %hero%s %skill%!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT.node(), "%target% has caught %hero%s %skill%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from %hero%s %skill%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 3, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 12000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 3000, false);
        long stunDuration = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 750, false);
        int manaGain = SkillConfigManager.getUseSetting(hero, this, "mana-gain", 5, false);
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if (e instanceof LivingEntity) {
                LivingEntity l = (LivingEntity) e;
                if (damageCheck(player, l)) {
                    CharacterTemplate character = plugin.getCharacterManager().getCharacter(l);
                    boolean powered = character.hasEffect("Outbreak") || character.hasEffect("Affliction");
                    plugin.getCharacterManager().getCharacter(l).addEffect(new FesterEffect(this, duration, period, damage, hero, stunDuration, manaGain, powered));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class FesterEffect extends PeriodicExpirableEffect {
        private double damage;
        private Player caster;
        private Hero casterHero;
        private long stunDuration;
        private int manaGain;
        private boolean powered;

        public FesterEffect(Skill skill, long duration, long period, double damage, Hero casterHero, long stunDuration, int manaGain, boolean powered) {
            super(skill, "Fester",casterHero.getPlayer(), period, duration);
            this.damage = damage;
            this.caster = casterHero.getPlayer();
            this.casterHero = casterHero;
            this.stunDuration = stunDuration;
            this.manaGain = manaGain;
            this.powered = powered;
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISEASE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), applyText, caster.getDisplayName(), p.getDisplayName(), "Fester");
            if (powered) {
                HeroRegainManaEvent heroRegainManaEvent = new HeroRegainManaEvent(casterHero, manaGain, skill);
                plugin.getServer().getPluginManager().callEvent(heroRegainManaEvent);
                if (!heroRegainManaEvent.isCancelled()) {
                    boolean mana = casterHero.getMana() + heroRegainManaEvent.getDelta() >= hero.getMaxMana();
                    casterHero.setMana(mana ? casterHero.getMaxMana() : casterHero.getMana() + heroRegainManaEvent.getDelta());
                }
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), expireText, caster.getDisplayName(), p.getDisplayName(), "Fester");
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            if (damageCheck(player, (LivingEntity) caster)) {
                damageEntity(player, caster, damage, DamageCause.MAGIC);
                if (powered) {
                    hero.addEffect(new StunEffect(skill, stunDuration,hero));
                    Lib.cancelDelayedSkill(hero);
                }
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            if (damageCheck(caster, monster.getEntity())) {
                damageEntity(monster.getEntity(), caster, damage, DamageCause.MAGIC);
                if (powered) {
                    monster.addEffect(new StunEffect(skill, stunDuration,casterHero));
                }
            }
        }
    }
}