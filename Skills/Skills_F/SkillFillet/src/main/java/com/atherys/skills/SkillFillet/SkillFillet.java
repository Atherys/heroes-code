package com.atherys.skills.SkillFillet;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillFillet extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillFillet(Heroes plugin) {
        super(plugin, "Fillet");
        setDescription("Targeted\n Deal increasing true damage with each consecutive hit on the same target.");
        setUsage("/skill Fillet");
        setArgumentRange(0, 0);
        setIdentifiers("skill Fillet");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 15);
        node.set(SkillSetting.DURATION.node(), 5000);
        node.set("damage-increment", 5D);
        return node;
    }

    @Override
    public void init() {
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% applied %stack% stacks of Fillet on %target%!").replace("%stack%", "$1").replace("%target%", "$2");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s Fillet stacks expired from %target%!").replace("%target%", "$1").replace("%hero%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] strings) {
        Player player = hero.getPlayer();
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false)
                + SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 75000, false);
        double incrementer = SkillConfigManager.getUseSetting(hero, this, "damage-increment", 5D, false);
        if (!target.equals(hero.getPlayer()) && damageCheck(hero.getPlayer(), target)) {
            CharacterTemplate targetCharacter = plugin.getCharacterManager().getCharacter(target);
            int stacks = 1;
	            if ((targetCharacter.hasEffect("FilletStackEffect" + hero.getName()))) {
		            stacks = ((FilletStackEffect) targetCharacter.getEffect("FilletStackEffect" + hero.getName())).getStackIdentifier();
		            stacks = (stacks <= 3) ? (stacks + 1) : (stacks);
		            if (stacks >= 3) {
			            damage = 50;
		            }
		            else if (stacks == 2) {
			            damage = 35;

		            }
		            else if (stacks == 1) {
			            damage = 20;

		            }


	            }
            addSpellTarget(target, hero);
            damageEntity(target, player, damage, DamageCause.CUSTOM);
            targetCharacter.addEffect(new FilletStackEffect(this, duration, stacks, hero));
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
        return SkillResult.INVALID_TARGET;
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "") + Lib.getSkillCostStats(hero, this);
    }

    public class FilletStackEffect extends ExpirableEffect {
        private int stackIdentifier;
        private Hero caster;

        public FilletStackEffect(Skill skill, long duration, int stackIdentifier, Hero caster) {
            super(skill, "FilletStackEffect" + caster.getName(),caster.getPlayer(), duration);
            this.stackIdentifier = stackIdentifier;
            this.caster = caster;
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISPELLABLE);

        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
	        if (getStackIdentifier()<3){
		        Messaging.send(hero.getPlayer(), applyText,3, hero.getPlayer().getDisplayName(), caster.getName());
		        Messaging.send(caster.getPlayer(), applyText, 3, hero.getPlayer().getDisplayName(), caster.getName());

	        }else {
		        Messaging.send(hero.getPlayer(), applyText, getStackIdentifier(), hero.getPlayer().getDisplayName(), caster.getName());
		        Messaging.send(caster.getPlayer(), applyText, getStackIdentifier(), hero.getPlayer().getDisplayName(), caster.getName());
	        }

        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
	            Messaging.send(hero.getPlayer(), expireText,  hero.getPlayer().getDisplayName(), caster.getName());
	            Messaging.send(caster.getPlayer(), expireText, hero.getPlayer().getDisplayName(), caster.getName());
            }
        }

        public int getStackIdentifier() {
            return stackIdentifier;
        }
    }
}