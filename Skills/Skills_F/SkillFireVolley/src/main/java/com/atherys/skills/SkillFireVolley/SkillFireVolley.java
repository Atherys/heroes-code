package com.atherys.skills.SkillFireVolley;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillFireVolley extends ActiveSkill {

    public SkillFireVolley(Heroes plugin) {
        super(plugin, "FireVolley");
        setDescription("Active\n Creates ring of fireballs around you dealing $1 magic damage to enemies that are hit");
        setUsage("/skill FireVolley");
        setArgumentRange(0, 0);
        setIdentifiers("skill firevolley");
        setTypes(SkillType.ABILITY_PROPERTY_FIRE, SkillType.SILENCEABLE, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        return getDescription().replace("$1", damage + "") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("fireball-damage", 15);
        node.set("max-damage", 40);
        node.set("velocity-multiplier", 1.0D);
        node.set("fire-ticks", 100);
        node.set(SkillSetting.DURATION.node(), 3000);
        node.set(SkillSetting.PERIOD.node(), 900);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 3000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 900, false);
        hero.addEffect(new FireVolleyEffect(this, period, duration,hero));
        hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 1.0F, 1.0F);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class FireVolleyEffect extends PeriodicExpirableEffect {

        public FireVolleyEffect(Skill skill, long period, long duration,Hero caster) {
            super(skill, "FireVolley",caster.getPlayer(),period, duration);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            double mult = SkillConfigManager.getUseSetting(hero, skill, "velocity-multiplier", 1.0D, false);
            Location location = player.getEyeLocation();
            for (float a = -10; a <= 10; a += 5) {
                Location loc = location.clone();
                loc.setYaw(location.getYaw() + a);
                Vector vel = loc.getDirection();
                Snowball fireball = player.launchProjectile(Snowball.class);
                fireball.setFireTicks(100);
                long time = System.currentTimeMillis();
                fireball.setMetadata("FireVolleyFireball", new FixedMetadataValue(plugin, time));
                fireball.setVelocity(vel.multiply(mult));
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }

    public class SkillEntityListener implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof Snowball)) || (!projectile.hasMetadata("FireVolleyFireball"))) {
                return;
            }
            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            Entity dmger = (Entity) ((Snowball) projectile).getShooter();
            if (!(dmger instanceof Player)) {
                return;
            }
            if (entity instanceof Player) {
                Hero hero = SkillFireVolley.this.plugin.getCharacterManager().getHero((Player) dmger);
                Hero thero = plugin.getCharacterManager().getHero((Player) entity);
                if (!thero.hasEffect("dce")) {
                    thero.addEffect(new DamageCheckEffect(skill,hero));
                } else
                    return;
            }

            Player player = (Player) dmger;
            if (damageCheck(player, entity)) {
                Hero hero = SkillFireVolley.this.plugin.getCharacterManager().getHero(player);
                int ft = SkillConfigManager.getUseSetting(hero, skill, "fire-ticks", 100, false);
                entity.setFireTicks(ft);
                SkillFireVolley.this.addSpellTarget(entity, hero);
                long time = projectile.getMetadata("FireVolleyFireball").get(0).asLong();
                time = System.currentTimeMillis() - time;
                double amount = (double) time / 1000;
                double fireballdamage = SkillConfigManager.getUseSetting(hero, skill, "fireball-damage", 15, false);
                fireballdamage *= amount;
                double maxDamage = SkillConfigManager.getUseSetting(hero, skill, "max-damage", 40, false);
                fireballdamage = fireballdamage < 0 ? 0 : fireballdamage > maxDamage ? maxDamage : fireballdamage;
                skill.damageEntity(entity, hero.getPlayer(), fireballdamage, EntityDamageEvent.DamageCause.MAGIC);
                Lib.projectileHit(player);
                event.setCancelled(true);
            }
        }
    }

    public class DamageCheckEffect extends ExpirableEffect {
        public DamageCheckEffect(Skill skill,Hero caster) {
            super(skill, "dce",caster.getPlayer(), 20L);
        }
    }
}
