package com.atherys.skills.SkillFlicker;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillFlicker extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillFlicker(Heroes plugin) {
        super(plugin, "Flicker");
        setDescription("Active\n Caster enters stealth every $1 seconds for $3 second for $2 seconds");
        setUsage("/skill Flicker");
        setArgumentRange(0, 0);
        setIdentifiers("skill flicker");
        setTypes(SkillType.ABILITY_PROPERTY_ILLUSION, SkillType.SILENCEABLE, SkillType.BUFFING);
    }

    @Override
    public String getDescription(Hero hero) {
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 2000, false) / 1000;
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 6000, false) / 1000;
        int sduration = SkillConfigManager.getUseSetting(hero, this, "smoke-duration", 1000, false) / 1000;
        String description = getDescription().replace("$1", period + "").replace("$2", duration + "").replace("$3", sduration + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired!");
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(6000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(2000));
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(final Hero hero, String[] args) {
        broadcastExecuteText(hero);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 20, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 10, false);
        hero.addEffect(new FlickerEffect(this, period, duration,hero));
        return SkillResult.NORMAL;
    }

    public class FlickerEffect extends PeriodicExpirableEffect {
        // private final String expireText = "unsmoked";
        // private final String applyText = "smoked";
        int a = 1;

        public FlickerEffect(SkillFlicker skill, long period, long duration,Hero caster) {
            super(skill, "Flicker",caster.getPlayer(), period, duration);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.INVISIBILITY);
            types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            final Player player = hero.getPlayer();
            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                @Override
                public void run() {
                    for (Player online : plugin.getServer().getOnlinePlayers()) {
                        if ((!online.equals(player))) {
                            online.showPlayer(player);
                        }
                    }
                }
            }, 2L);
            broadcast(hero.getPlayer().getLocation(), expireText, hero.getName(), "Flicker");
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            if (a == 0) {
                for (Player online : plugin.getServer().getOnlinePlayers()) {
                    if ((!online.equals(player))) {
                        online.hidePlayer(player);
                    }
                }
                a = 1;
            } else {
                for (Player online : plugin.getServer().getOnlinePlayers()) {
                    if ((!online.equals(player))) {
                        online.showPlayer(player);
                    }
                }
                a = 0;
            }
        }

        @Override
        public void tickMonster(Monster mnstr) {
        }
    }
}
