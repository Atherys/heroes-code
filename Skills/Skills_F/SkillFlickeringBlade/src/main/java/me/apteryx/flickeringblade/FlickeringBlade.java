package me.apteryx.flickeringblade;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import me.apteryx.flickeringblade.events.HeroWeaponDamageEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author apteryx
 * @time 2:33 PM
 * @since 11/24/2016
 *
 * It's a blade that flickers.
 */
public class FlickeringBlade extends PassiveSkill {

    private static FlickeringBlade flickeringBlade;

    public FlickeringBlade(Heroes plugin) {
        super(plugin, "FlickeringBlade");
        this.setDescription("It's a blade that flickers eh.");
        Bukkit.getPluginManager().registerEvents(new HeroWeaponDamageEvent(), this.plugin);
        if (flickeringBlade == null) {
            flickeringBlade = this;
        }
    }

    @Override
    public String getDescription(Hero hero) {
        return this.getDescription();
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("duration", 1000L);
        node.set("speed-multiplier", 2);
        node.set("bonus-melee-damage", 5.0);
        node.set("cooldown", 8000);
        return node;
    }

    public static FlickeringBlade getInstance() {
        return flickeringBlade;
    }
}