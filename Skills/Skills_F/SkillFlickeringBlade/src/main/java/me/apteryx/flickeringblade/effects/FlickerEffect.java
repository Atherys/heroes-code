package me.apteryx.flickeringblade.effects;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

/**
 * @author apteryx
 * @time 2:13 PM
 * @since 11/28/2016
 */
public class FlickerEffect extends PeriodicExpirableEffect {
    // private final String expireText = "unsmoked";
    // private final String applyText = "smoked";
    int a = 1;

    public FlickerEffect(Skill skill, long period, long duration, Hero caster) {
        super(skill, "Flicker",caster.getPlayer(), period, duration);
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.INVISIBILITY);
        this.types.add(EffectType.MAGIC);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().sendMessage("\2477You have disappeared in a puff of smoke!");
        for (double[] coordinates : Lib.playerParticleCoords) {
            Location location = hero.getPlayer().getLocation().add(coordinates[0], coordinates[1], coordinates[2]);
            hero.getPlayer().getWorld().spawnParticle( Particle.BLOCK_DUST, location, 1, new MaterialData(Material.COAL_BLOCK) );
            for (int i = 0; i < 8; i++) {
                hero.getPlayer().getWorld().spawnParticle(Particle.SPELL_MOB, hero.getPlayer().getLocation(), 1);
            }
        }

    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        final Player player = hero.getPlayer();
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            for (Player online : plugin.getServer().getOnlinePlayers()) {
                if ((!online.equals(player))) {
                    online.showPlayer(player);
                }
            }
        }, 2L);

        hero.getPlayer().sendMessage("\2477The smoke has dissipated, you have reappeared.");
        for (double[] coordinates : Lib.playerParticleCoords) {
            Location location = hero.getPlayer().getLocation().add(coordinates[0], coordinates[1], coordinates[2]);
            hero.getPlayer().getWorld().spawnParticle( Particle.BLOCK_DUST, location, 1, 0.5d, 0.5d, 0.5d, new MaterialData(Material.COAL_BLOCK) );
            hero.getPlayer().getWorld().spawnParticle( Particle.SPELL_MOB, hero.getPlayer().getLocation(), 1);
        }
    }

    @Override
    public void tickHero(Hero hero) {
        Player player = hero.getPlayer();
        for (Player online : plugin.getServer().getOnlinePlayers()) {
            if ((!online.equals(player))) {
                online.hidePlayer(player);
            }
        }
    }

    @Override
    public void tickMonster(Monster mnstr) {
    }
}