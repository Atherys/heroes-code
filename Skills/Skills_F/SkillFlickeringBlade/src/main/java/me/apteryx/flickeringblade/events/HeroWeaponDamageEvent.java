package me.apteryx.flickeringblade.events;

import com.atherys.effects.BetterInvis;
import com.atherys.effects.MightEffect;
import com.atherys.effects.VanishEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import me.apteryx.flickeringblade.FlickeringBlade;
import me.apteryx.flickeringblade.effects.FlickerEffect;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashSet;
import java.util.Set;

/**
 * @author apteryx
 * @time 2:48 PM
 * @since 11/24/2016
 */
public class HeroWeaponDamageEvent implements Listener {

    @EventHandler
    public void onHitDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
//          Bukkit.getLogger().info(event.getDamager().getName());
            Player player = (Player) event.getDamager();
            Hero hero = Heroes.getInstance().getCharacterManager().getHero(player);
            if (event.getEntity() instanceof LivingEntity) {
//                Bukkit.getLogger().info(event.getDamager().getName());
                if (hero.hasEffect("FlickeringBlade")) {
                    if (event.getEntity() instanceof Player) {
                        if (hero.getParty() != null) {
                            if (hero.getParty().isPartyMember((Player) event.getEntity())) {
                                return;
                            }
                        }
                    }
                    if (hero.getCooldown("FlickeringBlade") == null) {
                        hero.setCooldown("FlickeringBlade", 0L);
                    }
//                    Bukkit.getLogger().info("Has effect.");
//                    Bukkit.getLogger().info("FlickeringBlade CD: " + hero.getCooldown("FlickeringBlade"));
                    if (hero.getCooldown("FlickeringBlade") <= System.currentTimeMillis()) {
                        hero.addEffect(new MightEffect(FlickeringBlade.getInstance(), SkillConfigManager.getUseSetting(hero, FlickeringBlade.getInstance(), "duration", 1000, false), SkillConfigManager.getUseSetting(hero, FlickeringBlade.getInstance(), "bonus-melee-damage", 5.0, false), "Your muscles bulge with power!", "You feel strength leave your body!", hero));
                        hero.addEffect(new FlickerEffect(FlickeringBlade.getInstance(), 20L, SkillConfigManager.getUseSetting(hero, FlickeringBlade.getInstance(), "duration", 1000, false), hero));
                        hero.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, SkillConfigManager.getUseSetting(hero, FlickeringBlade.getInstance(), "duration", 1000, false) / 50, SkillConfigManager.getUseSetting(hero, FlickeringBlade.getInstance(), "speed-multiplier", 2, false)));
                        hero.setCooldown("FlickeringBlade", SkillConfigManager.getUseSetting(hero, FlickeringBlade.getInstance(), "cooldown", 8000, false) + System.currentTimeMillis());
                    }
                }
            }
        }
    }
}