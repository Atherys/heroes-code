package com.atherys.skills.SkillFlood;

import com.atherys.effects.BulwarkEffect;
import com.atherys.effects.WaveStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class SkillFlood extends ActiveSkill {
    public SkillFlood(Heroes plugin) {
        super(plugin, "Flood");
        setDescription("Push enemies in front of your away and give allies a shield. Grants you 3 Wave stacks.");
        setUsage("/skill Flood");
        setArgumentRange(0, 0);
        setIdentifiers("skill Flood");
        setTypes(SkillType.FORCE, SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Vector playerVector = player.getLocation().toVector();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 7, false);
        float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
        double c = SkillConfigManager.getUseSetting(hero, this, "coeficient", 0.4, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, "shield-duration", 8000, false);
        double health = SkillConfigManager.getUseSetting(hero, this, "shield-health", 200, false);
        for (Entity e : Lib.getEntitiesInCone(player.getNearbyEntities(radius, radius, radius), playerVector, (float) radius, degrees, player.getLocation().getDirection())) {
            if (e instanceof Player) {
                Player tPlayer = (Player)e;
                Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
                if (hero.hasParty()) {
                    if (hero.getParty().isPartyMember(tPlayer)) {
                        tHero.addEffect(new BulwarkEffect(this, duration, health,hero));
                        continue;
                    }
                }
                if (damageCheck(player, (LivingEntity) tPlayer)) {
                    addSpellTarget(tPlayer, hero);
                    double i = tPlayer.getLocation().distance(player.getLocation());
                    double cc = (radius - i) * c;
                    Vector v = tPlayer.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
                    v = v.multiply(cc);
                    v.setY(0.5);
                    tPlayer.setVelocity(v);
                }
            }
        }
        int stacks = SkillConfigManager.getUseSetting(hero, this, "wave-stacks", 3, false);
        broadcastExecuteText(hero);
        WaveStackEffect.addWaveStacks(this, hero, stacks);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("shield-duration", 8000);
        node.set("shield-health", 200);
        node.set(SkillSetting.RADIUS.node(), 7);
        node.set("coeficient", 0.4);
        node.set("degrees", 45);
        node.set("wave-stacks", 3);
        return node;
    }
}