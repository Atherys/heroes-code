package com.atherys.skills.SkillForge;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.material.PistonBaseMaterial;

public class SkillForge extends PassiveSkill {
    public SkillForge(Heroes plugin) {
        super(plugin, "Forge");
        setDescription("Passive\n Allows you to copy books at a Print machine");
        setArgumentRange(0, 0);
        setUsage("/skill Forge");
        setIdentifiers("skill Forge");
        setTypes(SkillType.KNOWLEDGE, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), plugin);
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.COOLDOWN.node(), Integer.valueOf(10000));
        return node;
    }

    public class PlayerInteractListener implements Listener {
        private final Skill skill;

        public PlayerInteractListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onPlayerInteract(PlayerInteractEvent event) {
            if (event.getAction() != Action.LEFT_CLICK_BLOCK)
                return;
            if (event.getPlayer().getItemInHand().getType() != Material.WRITTEN_BOOK)
                return;
            if (event.getClickedBlock().getType() != Material.PISTON_BASE)
                return;
            Hero hero = SkillForge.this.plugin.getCharacterManager().getHero(event.getPlayer());
            if (!hero.hasEffect("Forge"))
                return;
            if ((hero.getCooldown("Forge") == null) || (hero.getCooldown("Forge").longValue() <= System.currentTimeMillis())) {
                Block block = event.getClickedBlock();
                PistonBaseMaterial p = (PistonBaseMaterial) Material.PISTON_BASE.getNewData(block.getData());
                BlockFace[] bfarray = {BlockFace.DOWN, BlockFace.WEST, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH};
                for (BlockFace bf : bfarray)
                    if ((p.getFacing() == bf) && ((block.getLocation().getBlock().getRelative(bf).getType() == Material.AIR) || (block.getLocation().getBlock().getRelative(bf, 2).getType() == Material.IRON_BLOCK))) {
                        Block tempblock = block.getRelative(bf);
                        p.setPowered(true);
                        block.setData(p.getData(), true);
                        tempblock.setType(Material.PISTON_EXTENSION);
                        tempblock.setData(tempblock.getState().getData().getData());
                        BookMeta book = (BookMeta) event.getPlayer().getInventory().getItemInHand().getItemMeta();
                        block.getState().update();
                        tempblock.getState().update();
                        ItemStack newbook = new ItemStack(Material.WRITTEN_BOOK, 1);
                        BookMeta newbookdata = (BookMeta) newbook.getItemMeta();
                        newbookdata.setTitle(book.getTitle());
                        newbookdata.setAuthor(book.getAuthor());
                        newbookdata.setPages(book.getPages());
                        newbook.setItemMeta(newbookdata);
                        event.getPlayer().getWorld().dropItem(event.getClickedBlock().getLocation().getBlock().getRelative(bf).getLocation(), newbook);
                        long cooldown = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN, 5000, false);
                        hero.setCooldown("Forge", cooldown + System.currentTimeMillis());
                        return;
                    }
            }
        }
    }
}
