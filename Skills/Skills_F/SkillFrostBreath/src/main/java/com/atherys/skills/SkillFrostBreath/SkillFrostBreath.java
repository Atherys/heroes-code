package com.atherys.skills.SkillFrostBreath;

import com.atherys.effects.KneebreakEffect;
import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillFrostBreath extends ActiveSkill {
    public SkillFrostBreath(Heroes plugin) {
        super(plugin, "FrostBreath");
        setDescription("Active\nDamage and Slow all enemies infront of you and also prevent them from jumping.");
        setUsage("/skill FrostBreath");
        setArgumentRange(0, 0);
        setIdentifiers("skill FrostBreath");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_ICE);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        List<Entity> nearbyPlayers = new ArrayList<Entity>();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if ((entity instanceof LivingEntity)) {
                if (damageCheck(player, (LivingEntity) entity)) {
                    nearbyPlayers.add(entity);
                }
            }
        }
        if (!nearbyPlayers.isEmpty()) {
            Vector playerVector = player.getLocation().toVector();
            float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
            double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
            long slowDuration = SkillConfigManager.getUseSetting(hero, this, "slow-duration", 3000, false);
            long kneebreakDuration = SkillConfigManager.getUseSetting(hero, this, "kneebreak-duration", 1000, false);
            int amplifier = SkillConfigManager.getUseSetting(hero, this, "slow-amplifier", 2, false);
            for (Entity e : Lib.getEntitiesInCone(nearbyPlayers, playerVector, (float) radius, degrees, player.getLocation().getDirection())) {
                LivingEntity target = (LivingEntity) e;
                CharacterTemplate characterTemplate = plugin.getCharacterManager().getCharacter(target);
                addSpellTarget(target, hero);
                damageEntity(target, player, damage, DamageCause.MAGIC);
                characterTemplate.addEffect(new SlowEffect(this, "FrostBreathSlow", slowDuration, amplifier, false, "", "", hero));
                characterTemplate.addEffect(new KneebreakEffect(this, "FrostBreathKneebreak", kneebreakDuration, "", "",hero));
                player.getLocation().getWorld().spawnParticle(Particle.SNOW_SHOVEL, player.getLocation().add(0,1,0), 1 );
            }
        }
        player.getLocation().getWorld().spawnParticle(Particle.SNOW_SHOVEL, player.getLocation().add(0,1,0), 1 );
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 5);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set("slow-duration", 3000);
        node.set("slow-amplifier", 2);
        node.set("kneebreak-duration", 1000);
        node.set("degrees", 45);
        return node;
    }
}