package com.atherys.skills.SkillFrostVolley;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings("deprecation")
public class FrostVolleyEffect extends PeriodicExpirableEffect  {

    private byte iceboltAmount;
    private double velocityMult;
    private int particleCount;

    public FrostVolleyEffect(Skill skill, Hero caster, byte iceboltsAmount, double velocity, long period, long duration ) {
        super(skill, "FireVolley", caster.getPlayer(), period, duration);
        velocityMult = SkillConfigManager.getUseSetting( caster, skill, "velocity-multiplier", velocity, false );
        this.iceboltAmount = iceboltsAmount;
        this.particleCount = SkillConfigManager.getUseSetting ( caster, skill, "particle-count", 3, false);
    }

    @Override
    public void tickHero(Hero hero) {
        Player caster = hero.getPlayer();
        Location location = caster.getEyeLocation();

        for (float i = -10; i <= 10; i += 20/(float) iceboltAmount + 1 ) {
            Location loc = location.clone();
            loc.setYaw(location.getYaw() + i);
            Vector vel = loc.getDirection();
            Snowball icebolt = caster.launchProjectile(Snowball.class);
            long time = System.currentTimeMillis();

            if ( particleCount != 0 ) {
                applyEffectsTo( caster, icebolt, particleCount );
            }

            icebolt.setShooter(caster);
            icebolt.setMetadata("FrostVolleyIcebolt", new FixedMetadataValue(plugin, time));
            icebolt.setVelocity(vel.multiply(velocityMult));
            hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.BLOCK_SNOW_PLACE, 1.5F, 1.0F);
        }
    }

    public void applyEffectsTo ( Player player, Snowball snowball, int count ) {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (snowball.isDead()) {
                    timer.cancel();
                    this.cancel();
                    return;
                }
                Location loc = snowball.getLocation();
                player.getWorld().spawnParticle(Particle.SNOWBALL, loc, count);//, 0, 0, 0, 1, new MaterialData( Material.PACKED_ICE ) );
            }
        };
        timer.schedule(task, 200, 100);
    }

    @Override
    public void tickMonster(Monster monster) {
        // TODO: Maybe allow monsters to use the skill too?
    }
}
