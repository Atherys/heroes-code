package com.atherys.skills.SkillFrostVolley;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillFrostVolley extends ActiveSkill {

    private double defIceboltDamage = 20;
    private double defMaxDamage = 40;
    private double defSlowDuration = 1250;
    private int defSlowAmplifier = 2;
    private double defSkillDuration = 3000;
    private double defSkillPeriod = 750;
    private double defVelocityMult = 1.0D;
    private byte defIceboltAmount = 4;
    private int defParticle = 3;

    public SkillFrostVolley(Heroes plugin) {
        super(plugin, "FrostVolley");
        setDescription("Active\nLaunch a volley of low velocity Icebolts every $1 seconds for $2 seconds, dealing $3 magic damage per hit and Slowing for $4 seconds.");
        setUsage("/skill FrostVolley");
        setArgumentRange(0, 0);
        setIdentifiers("skill frostvolley");
        setTypes(SkillType.ABILITY_PROPERTY_ICE, SkillType.SILENCEABLE, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, "icebolt-damage", defIceboltDamage, false);
        double period = SkillConfigManager.getUseSetting( hero, this, SkillSetting.PERIOD, defSkillPeriod, false );
        double time = SkillConfigManager.getUseSetting( hero, this, SkillSetting.DURATION, defSkillDuration, false );
        double slowTime = SkillConfigManager.getUseSetting( hero, this, "slow-duration", defSlowDuration, false);

        String description = getDescription();

        description = description.replace("$3", damage + "");
        description = description.replace("$1", period/1000 + "");
        description = description.replace("$2", time/1000 + "");
        description = description.replace("$4", slowTime/1000 + "");

        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        int defManaCost = 20;
        int defCooldown = 6000;

        ConfigurationSection node = super.getDefaultConfig();
        node.set("icebolt-damage", defIceboltDamage);
        node.set("max-damage", defMaxDamage);
        node.set("icebolt-amount", defIceboltAmount);
        node.set("velocity-multiplier", defVelocityMult);
        node.set("slow-duration", defSlowDuration);
        node.set("speed-multiplier", defSlowAmplifier );
        node.set("particle-count", defParticle);
        node.set(SkillSetting.DURATION.node(), defSkillDuration);
        node.set(SkillSetting.PERIOD.node(), defSkillPeriod);
        node.set(SkillSetting.MANA.node(), defManaCost );
        node.set(SkillSetting.COOLDOWN.node(), defCooldown );
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {

        double velocityMultiplier = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", defVelocityMult, false );
        byte iceboltsAmount = (byte) SkillConfigManager.getUseSetting ( hero, this, "icebolt-amount", defIceboltAmount, false);
        double period = SkillConfigManager.getUseSetting( hero, this, SkillSetting.PERIOD, defSkillPeriod, false );
        double time = SkillConfigManager.getUseSetting( hero, this, SkillSetting.DURATION, defSkillDuration, false );

        FrostVolleyEffect fvEffect = new FrostVolleyEffect( this, hero, iceboltsAmount, velocityMultiplier, (long) period, (long) time );
        hero.addEffect(fvEffect);

        broadcastExecuteText(hero);

        return SkillResult.NORMAL;

    }

    public class SkillEntityListener implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill ) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {

            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }

            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();

            if ((!(projectile instanceof Snowball)) || (!projectile.hasMetadata("FrostVolleyIcebolt"))) {
                return;
            }

            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            Entity dmger = (Entity) ((Snowball) projectile).getShooter();

            if (!(dmger instanceof Player)) {
                return;
            }

            Player player = (Player) dmger;
            if (damageCheck(player, entity)) {

                Hero hero = SkillFrostVolley.this.plugin.getCharacterManager().getHero((Player) dmger);

                double slowTime = SkillConfigManager.getUseSetting(hero, skill, "slow-duration", defSlowDuration / 1000, false);
                int amplifier = SkillConfigManager.getUseSetting(hero, skill, "speed-multiplier", defSlowAmplifier, false);
                double iceboltdamage = SkillConfigManager.getUseSetting(hero, skill, "icebolt-damage", defIceboltDamage, false);
                double maxDamage = SkillConfigManager.getUseSetting(hero, skill, "max-damage", defMaxDamage, false);


                SlowEffect slow = new SlowEffect(this.skill, (long) slowTime, amplifier, false, null, null, hero);
                if ( entity instanceof Player ) Heroes.getInstance().getCharacterManager().getHero( (Player) entity ).addEffect(slow);

                SkillFrostVolley.this.addSpellTarget(entity, hero);
                long time = projectile.getMetadata("FrostVolleyIcebolt").get(0).asLong();
                time = System.currentTimeMillis() - time;
                double amount = (double) time / 1000;
                iceboltdamage *= amount;
                iceboltdamage = iceboltdamage < 0 ? 0 : iceboltdamage > maxDamage ? maxDamage : iceboltdamage;

                skill.damageEntity(entity, hero.getPlayer(), iceboltdamage, EntityDamageEvent.DamageCause.MAGIC);
                if ( entity instanceof Player ) Lib.projectileHit((Player) entity);

                event.setCancelled(true);
            }
        }
    }


}
