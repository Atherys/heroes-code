package com.atherys.skills.SkillFrostbite;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillFrostbite extends TargettedSkill {
    private String removeText;

    public SkillFrostbite(Heroes plugin) {
        super(plugin, "Frostbite");
        setDescription("Targeted\n Deals $1 magic damage to target, every $2s if they have moved, they take an additional $3 magic damage, lasts $4s");
        setUsage("/skill frostbite");
        setArgumentRange(0, 0);
        setIdentifiers("skill frostbite");
        setTypes(SkillType.DEBUFFING, SkillType.DAMAGING, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        int damage = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE.node(), 5, false) + SkillConfigManager.getUseSetting(hero, this, "damage-increase", 0.0D, false) * hero.getSkillLevel(this));
        damage = damage > 0 ? damage : 0;
        int tickDamage = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK.node(), 2, false) + SkillConfigManager.getUseSetting(hero, this, "tick-damage-increase", 0.0D, false) * hero.getSkillLevel(this));
        tickDamage = tickDamage > 0 ? tickDamage : 0;
        int maxDistance = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE.node(), 2, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE_INCREASE.node(), 0.0D, false) * hero.getSkillLevel(this));
        maxDistance = maxDistance > 0 ? maxDistance : 0;
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 1000, false);
        int duration = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0D, false) * hero.getSkillLevel(this));
        String description = getDescription().replace("$1", damage + "").replace("$2", period / 1000 + "").replace("$3", tickDamage + "").replace("$4", duration / 1000 + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set("duration-increase", Integer.valueOf(0));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(2000));
        node.set("tick-damage-increase", Integer.valueOf(0));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(5));
        node.set("damage-incrase", Integer.valueOf(0));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(15));
        node.set(SkillSetting.MAX_DISTANCE_INCREASE.node(), Integer.valueOf(0));
        node.set("mindamage", 0);
        node.set("remove-text", "%target% has recovered from %hero%'s %skill%!");
        return node;
    }

    public void init() {
        super.init();
        this.removeText = SkillConfigManager.getUseSetting(null, this, "remove-text", "%target% has recovered from %hero%s %skill%!").replace("%target%", "$1").replace("%hero%", "$2").replace("%skill%", "$3");
    }

    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        Player player = hero.getPlayer();
        if ((le instanceof Player)) {
            Player target = (Player) le;
            if (target.equals(hero.getPlayer())) {
                return SkillResult.INVALID_TARGET_NO_MSG;
            }else {
	            Hero tHero = this.plugin.getCharacterManager().getHero((Player) le);
	            double damage = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false));
	            damage = damage > 0 ? damage : 0;
	            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
	            duration = duration > 0L ? duration : 0L;
	            long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
	            double minDamage = SkillConfigManager.getUseSetting(hero, this, "mindamage", 0, false);
	            FrostbiteEffect cEffect = new FrostbiteEffect(this, period, duration, damage, player, minDamage);
	            tHero.addEffect(cEffect);
	            broadcastExecuteText(hero, le);
	            return SkillResult.NORMAL;
            }
        }
        return SkillResult.INVALID_TARGET;
    }

    public class FrostbiteEffect extends PeriodicExpirableEffect {
        private final double minDamage;
        private double originalDamage;
        private double damageTick;
        private Player caster;
        private Location prevLocation;

        public FrostbiteEffect(Skill skill, long period, long duration, double damageTick, Player caster, double minDamage) {
            super(skill, "Frostbite",caster, period, duration);
            this.caster = caster;
            this.damageTick = damageTick;
            this.minDamage = minDamage;
            this.originalDamage += this.damageTick;
            this.types.add(EffectType.BLEED);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.ICE);
        }

        public void tickHero(Hero hero) {
            Player pHero = hero.getPlayer();
            Location pLoc = pHero.getLocation();
            if (this.prevLocation != null) {
                double x = Math.hypot((pLoc.getX() - prevLocation.getX()), (pLoc.getZ() - prevLocation.getZ()));
                this.damageTick = (this.damageTick - x > minDamage) ? (this.damageTick -= x) : minDamage;
            }
            skill.damageEntity(hero.getPlayer(), this.caster, this.damageTick, EntityDamageEvent.DamageCause.MAGIC);
            this.prevLocation = pHero.getLocation();
            this.damageTick = originalDamage;
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            this.prevLocation = hero.getPlayer().getLocation();
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), SkillFrostbite.this.removeText, hero.getPlayer().getDisplayName(), this.caster.getDisplayName(), "Frostbite");
        }

        public void tickMonster(Monster mnstr) {
            super.tick(mnstr);
            skill.damageEntity(mnstr.getEntity(), this.caster, this.damageTick, EntityDamageEvent.DamageCause.MAGIC);
        }
    }
}
