/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.skills.SkillGarrison;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Set;

import static com.herocraftonline.heroes.characters.skill.Skill.damageCheck;

public class GarrisonEffect extends PeriodicExpirableEffect {
    private final Player caster;
    private final Location loc;
    private final int r;
    private final double mEffectReduction;
    private final double v1;
    private final double v2;
    private final Set<Block> values;

    private String applyText;
    private String expireText;

    public GarrisonEffect(Skill skill, long duration, Player caster, Location loc, int r, double amp, double v1, double v2, Set<Block> values) {
        super(skill, "Garrison",caster, 1000L, duration);
        this.caster = caster;
        this.loc = loc;
        this.r = r;
        this.mEffectReduction = amp;
        this.v1 = v1;
        this.v2 = v2;
        this.values = values;
        types.add(EffectType.BENEFICIAL);
        types.add(EffectType.FORM);
        types.add(EffectType.UNBREAKABLE);
        setApplyText("$1 used $2.");
        setExpireText("$1's $2 expired!");
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player p = hero.getPlayer();
        broadcast(p.getLocation(), getApplyText(), hero.getName(), "Garrison");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player p = hero.getPlayer();
        broadcast(p.getLocation(), getExpireText(), hero.getName(), "Garrison");
        Lib.removeBlocks(values);
    }

    public Player getCaster() {
        return this.caster;
    }

    public Location getObeliskLocation() {
        return loc;
    }

    @Override
    public void tickMonster(Monster mnstr) {
    }

    public boolean garison(Player p) {
        for (Block x : values) {
            if (x.getRelative(BlockFace.NORTH, 1).getType().equals(Material.AIR)) {
                if (x.getRelative(BlockFace.NORTH, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                    p.teleport(x.getRelative(BlockFace.NORTH, 1).getLocation());
                    return true;
                }
            } else if (x.getRelative(BlockFace.SOUTH, 1).getType().equals(Material.AIR)) {
                if (x.getRelative(BlockFace.SOUTH, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                    p.teleport(x.getRelative(BlockFace.SOUTH, 1).getLocation());
                    return true;
                }
            } else if (x.getRelative(BlockFace.EAST, 1).getType().equals(Material.AIR)) {
                if (x.getRelative(BlockFace.EAST, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                    p.teleport(x.getRelative(BlockFace.EAST, 1).getLocation());
                    return true;
                }
            } else if (x.getRelative(BlockFace.WEST, 1).getType().equals(Material.AIR)) {
                if (x.getRelative(BlockFace.WEST, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                    p.teleport(x.getRelative(BlockFace.WEST, 1).getLocation());
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void tickHero(Hero hero) {
        for (Entity e : loc.getWorld().getNearbyEntities(loc, r, r, r)) {
            if (e instanceof Player) {
                Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                if (tHero.equals(hero)) {
                    hero.addEffect(new MEffect(skill, getPeriod(), mEffectReduction,hero));
                } else if (hero.hasParty() && hero.getParty().isPartyMember(tHero)) {
                    tHero.addEffect(new MEffect(skill, getPeriod(), mEffectReduction,hero));
                } else if (damageCheck(hero.getPlayer(), (LivingEntity) tHero.getPlayer())) {
                    tHero.addEffect(new SlowEffect(skill, getPeriod() * 3, 2, true, "", "", hero));
                    if (hero.hasEffect("Pulse")) {
                        Vector v = tHero.getPlayer().getLocation().toVector().subtract(loc.toVector()).normalize();
                        v.multiply(v1);
                        v.setY(v2);
                        tHero.getPlayer().setVelocity(v);
                    }
                }
            }
        }
    }

    public class MEffect extends ExpirableEffect {
        private final double amp;

        public MEffect(Skill skill, long duration, double amp,Hero hero) {
            super(skill, "MEffect",hero.getPlayer(), duration);
            this.amp = amp;
            types.add(EffectType.BENEFICIAL);
        }

        public double getAmplifier() {
            return this.amp;
        }
    }

}
