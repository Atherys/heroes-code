package com.atherys.skills.SkillGarrison;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillGarrison extends TargettedSkill {
    public SkillGarrison(Heroes plugin) {
        super(plugin, "Garrison");
        setArgumentRange(0, 0);
        setUsage("/skill Garrison");
        setIdentifiers("skill Garrison");
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
        setDescription("Target player is moved to the obelisk.");
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        Player caster = hero.getPlayer();
        if (!(hero.hasEffect("Rift"))) {
            Messaging.send(caster, "You don't have active obelisk");
            return SkillResult.CANCELLED;
        }

        if (le.hasMetadata("NPC")) {
            return SkillResult.CANCELLED;
        }

        if (!(le instanceof Player)) {
            return SkillResult.CANCELLED;
        }
        GarrisonEffect r = (GarrisonEffect) hero.getEffect("Rift");
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 100, false);
        Hero targetHero = this.plugin.getCharacterManager().getHero((Player) le);
        if (caster.equals(targetHero.getPlayer())) {
            return SkillResult.CANCELLED;
        } else if (targetHero.hasParty() && (targetHero.getParty().getMembers().contains(hero)) && (canBeTeleported(targetHero.getPlayer().getLocation(), r.getObeliskLocation(), max))) {
            for (Effect effect : targetHero.getEffects()) {
                if (((effect.isType(EffectType.DISABLE)) || (effect.isType(EffectType.SLOW)) || (effect.isType(EffectType.STUN)) || (effect.isType(EffectType.ROOT))) && (!effect.isType(EffectType.UNBREAKABLE))) {
                    targetHero.removeEffect(effect);
                }
            }
            if (r.garison(targetHero.getPlayer())) {
                broadcastExecuteText(hero, le);
                return SkillResult.NORMAL;
            }
            Messaging.send(caster, "Target cannot be garrisoned to the Rift without being suffocated!");
        }
        return SkillResult.CANCELLED;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), 100);
        return node;
    }

    public boolean canBeTeleported(Location loc1, Location loc2, int distance) {
        return loc1.distanceSquared(loc2) <= distance * distance;
    }
}
