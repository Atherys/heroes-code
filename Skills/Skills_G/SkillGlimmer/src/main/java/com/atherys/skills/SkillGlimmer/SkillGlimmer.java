package com.atherys.skills.SkillGlimmer;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillGlimmer extends ActiveSkill {
    private String applytext;
    private String expiretext;
    private String bufftext;
    private String buffetext;

    public SkillGlimmer(Heroes plugin) {
        super(plugin, "Glimmer");
        setDescription("Places 2 block high yellow stained glass, and after $1 seconds will shatter (glass breaks with noise) and gives $2% damage reduction buff for $3 seconds to party members within a $4 block radius.");
        setUsage("/skill Glimmer");
        setArgumentRange(0, 0);
        setIdentifiers("skill Glimmer");
        setTypes(SkillType.HEALING, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new GlimmerListener(), plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block block = hero.getPlayer().getTargetBlock((Set<Material>) null, d);
        if (block.getType() == Material.AIR) {
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        for (int y = 1; y<3; y++) {
            Block b = block.getRelative(BlockFace.UP, y);
            if (b.getType().equals(Material.AIR)) {
                b.setType(Material.STAINED_GLASS);
                b.setData((byte) 4);
                values.add(b);
                b.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
            }
        }
        long glassDuration = (long)SkillConfigManager.getUseSetting(hero, this, "glass-duration", 5000L, false);
        double armorBonus = SkillConfigManager.getUseSetting(hero, this, "armor-bonus", 0.2D, false);
        long buffDuration = (long)SkillConfigManager.getUseSetting(hero, this, "buff-duration", 30000L, false);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3, false);
        hero.addEffect(new GlimmerEffect(this, glassDuration, buffDuration, armorBonus, radius, values, block.getLocation(),hero));
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("glass-duration", Long.valueOf(5000));
        node.set("buff-duration", Long.valueOf(30000));
        node.set("armor-bonus", Double.valueOf(0.2));
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(3));
        node.set(SkillSetting.MAX_DISTANCE.node(), Double.valueOf(15));
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired!");
        node.set("buff-text", "Your armor was buffed by %caster%'s %skill%!");
        node.set("buff-expire", "Your armor buff expired!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired!").replace("%hero%", "$1").replace("%skill%", "$2");
        bufftext = SkillConfigManager.getRaw(this, "buff-text", "Your armor was buffed by %caster%'s %skill%!").replace("%caster%", "$1").replace("%skill%", "$2");
        buffetext = SkillConfigManager.getRaw(this, "buff-expire", "Your armor buff expired!");
    }

    @Override
    public String getDescription(Hero hero) {
        long glassDuration = (long)SkillConfigManager.getUseSetting(hero, this, "glass-duration", 5000L, false);
        double armorBonus = SkillConfigManager.getUseSetting(hero, this, "armor-bonus", 0.2D, false);
        long buffDuration = (long)SkillConfigManager.getUseSetting(hero, this, "buff-duration", 30000L, false);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3, false);
        return getDescription().replace("$1", (double)(glassDuration/1000) + "").replace("$2", (int)(armorBonus*100) + "").replace("$3", (double)(buffDuration/1000) + "").replace("$4", (int)radius + " ") + Lib.getSkillCostStats(hero, this);
    }

    public class GlimmerEffect extends ExpirableEffect {
        private final double bonus;
        private final long buffDuration;
        private Set<Block> values;
        private final double radius;
        private final Location loc;

        public GlimmerEffect(Skill skill, long glassDuration, long buffDuration, double armorBonus, double radius, Set<Block> values, Location loc,Hero caster) {
            super(skill, "GlimmerGlassEffect",caster.getPlayer(), glassDuration);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.MAGIC);
            types.add(EffectType.FORM);
            this.values = values;
            this.bonus = armorBonus;
            this.buffDuration = buffDuration;
            this.radius = radius;
            this.loc = loc;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Glimmer");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Glimmer");
                loc.getWorld().playSound(loc, Sound.ENTITY_FIREWORK_TWINKLE, 1F, 1F);
                if (hero.hasParty()) {
                    for (Entity e : loc.getWorld().getNearbyEntities(loc, radius, radius, radius)) {
                        if (e instanceof Player) {
                            Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                            if (hero.getParty().getMembers().contains(tHero)) {
                                tHero.addEffect(new GlimmerBEffect(skill, buffDuration, bonus, hero.getPlayer()));
                            }
                        }
                    }
                }
                else {
                    hero.addEffect(new GlimmerBEffect(skill, buffDuration, bonus, hero.getPlayer()));
                }
                Lib.removeBlocks(values);
            }
            else {
                broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Glimmer");
                Lib.removeBlocks(values);
            }
        }
    }

    public class GlimmerBEffect extends ExpirableEffect {
        private final double bonus;
        private final Player caster;

        public GlimmerBEffect(Skill skill, long duration, double bonus, Player caster) {
            super(skill, "GlimmerBEffect",caster, duration);
            this.bonus = bonus;
            this.caster = caster;
        }

        public double getBonus() {
            return bonus;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Messaging.send(hero.getPlayer(), bufftext, caster.getName(), "Glimmer");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), buffetext, caster.getName(), "Glimmer");
        }
    }

    public class GlimmerListener implements Listener {
        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onPlayerDamage(WeaponDamageEvent event) {
            if ((!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("GlimmerBEffect")) {
                GlimmerBEffect g = (GlimmerBEffect) hero.getEffect("GlimmerBEffect");
                event.setDamage(event.getDamage() * (1.0D-g.getBonus()));
            }
        }
    }
}    