package com.atherys.skills.SkillGrace;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillGrace extends PassiveSkill {

    public SkillGrace(Heroes plugin) {
        super(plugin, "Grace");
        setDescription("Passive\nYou and your nearby allies are invulnerable while stunned.");
        setTypes(SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillGraceListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 5);
        return node;
    }

    public class SkillGraceListener implements Listener {
        private final Skill skill;

        public SkillGraceListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Player player = (Player)event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffectType(EffectType.STUN)) {
                if (hero.hasEffect("Grace")) {
                    event.setCancelled(true);
                } else if (hero.hasParty()) {
                    for (Hero partyHero : hero.getParty().getMembers()) {
                        if (partyHero.hasEffect("Grace")) {
                            int distance = SkillConfigManager.getUseSetting(partyHero, skill, SkillSetting.RADIUS, 5, false);
                            if (player.getWorld().equals(partyHero.getPlayer().getWorld()) && partyHero.getPlayer().getLocation().distanceSquared(player.getLocation()) <= distance) {
                                event.setCancelled(true);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}