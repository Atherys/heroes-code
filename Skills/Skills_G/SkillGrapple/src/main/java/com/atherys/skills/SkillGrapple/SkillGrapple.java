package com.atherys.skills.SkillGrapple;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;

public class SkillGrapple extends ActiveSkill {
    public SkillGrapple(Heroes plugin) {
        super(plugin, "Grapple");
        setDescription("Active\n The next arrow you shoot will rappel you to it's location once it lands.");
        setUsage("/skill Grapple");
        setArgumentRange(0, 0);
        setIdentifiers("skill grapple");
        setTypes(SkillType.MOVEMENT_PREVENTING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        return super.getDefaultConfig();
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 5000, true);
        hero.addEffect(new GrappleEff(this, duration,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class GrappleEff extends ExpirableEffect {
        private int arrowLeft = 1;

        public GrappleEff(Skill skill, long duration,Hero caster) {
            super(skill, "GrappleEff",caster.getPlayer(), duration);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.IMBUE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if (this.isExpired()) {
                Messaging.send(player, "Grapple expired.");
            }
        }

        public int useArrow() {
            arrowLeft--;
            return arrowLeft;
        }
    }

    public class GrapplePullEff extends PeriodicExpirableEffect {
        private double va, vm;
        private Arrow loc;
        private double distance;
        private Location prevLoc;

        public GrapplePullEff(Skill skill, long period, long duration, Arrow loc, double va, double vm, double distance,Hero caster) {
            super(skill, "GrapplePullEff",caster.getPlayer(), period, duration);
            this.loc = loc;
            this.va = va;
            this.vm = vm;
            this.distance = distance;
            this.prevLoc = null;
            this.types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            if (loc == null) {
                this.expire();
            }
            double asdf = loc.getLocation().distanceSquared(player.getLocation());
            if ((asdf < 15*15) || (asdf > 100*100)) {
                this.expire();
            }
            if (this.prevLoc != null) {
                double x = Math.hypot((player.getLocation().getX() - prevLoc.getX()), (player.getLocation().getZ() - prevLoc.getZ()));
                if (x < 0.75) {
                    this.expire();
                }
            }
            prevLoc = player.getLocation();
            double newdistance = loc.getLocation().distanceSquared(player.getLocation());
            double multiplier = (((newdistance / distance) > 0.5) && (newdistance / distance < 1.0)) ? (newdistance / distance) : 0.5;
            Vector v = (loc.getLocation().toVector().subtract(player.getLocation().toVector())).normalize();
            v.multiply(vm * multiplier);
            player.setVelocity(v);
        }

        @Override
        public void tickMonster(Monster monster) {
        }
    }

    public class SkillHeroListener implements Listener {
        final Skill skill;

        public SkillHeroListener(Skill skill) {
            this.skill = skill;
        }

        //called if it hits a block
        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void PlayerBowShoot(ProjectileHitEvent event) {
            Entity entity = event.getEntity();
            if (!(entity instanceof Arrow)) {
                return;
            }
            Arrow arrow = (Arrow) entity;
            Entity shooter = (Entity) arrow.getShooter();
            if (!(shooter instanceof Player)) {
                return;
            }
            Player player = (Player) shooter;
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!(hero.hasEffect("GrappleEff"))) {
                return;
            }

            Location loc = arrow.getLocation();
            if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
                Messaging.send(player, "The void prevents you from grappling!");
                return;
            }
            int distance = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.MAX_DISTANCE, 100, false);
            double vm = SkillConfigManager.getUseSetting(hero, skill, "overall-multiplier", 5, false);
            double va = SkillConfigManager.getUseSetting(hero, skill, "vertical-decrement", 0.45, false);

            if (!(arrow.getLocation().distance(player.getLocation()) > distance)) { //check the distance, if it is greater than specified grapple will return as failed
                double distancebetween = arrow.getLocation().distanceSquared(player.getLocation());
                hero.addEffect(new GrapplePullEff(skill, 200L, 7000L, arrow, va, vm, distancebetween,hero));
                player.setFallDistance(-20.0F);

                GrappleEff rp = (GrappleEff) hero.getEffect("GrappleEff");
                if (rp.useArrow() < 1) {
                    hero.removeEffect(rp);
                }
                return;
            }
            Messaging.send(player, "No location to grapple to.");

            GrappleEff rp = (GrappleEff) hero.getEffect("GrappleEff"); //if it misses remove the arrow anyways
            if (rp.useArrow() < 1) {
                hero.removeEffect(rp);
            }
        }
    }
}