package com.atherys.skills.SkillGroundPound;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import java.util.List;

public class SkillGroundPound extends ActiveSkill {
    public SkillGroundPound(Heroes plugin) {
        super(plugin, "Groundpound");
        setUsage("/skill groundpound");
        setDescription("Acitve\n Hit the ground dealing $1 physical damage and sending $2 nearby enemies into the air");
        setArgumentRange(0, 0);
        setIdentifiers("skill groundpound");
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.DAMAGING, SkillType.FORCE, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 5);
        node.set("max-targets", 4);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set("velocity", 0.6D);
        return node;
    }

    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
        int targets = SkillConfigManager.getUseSetting(hero, this, "max-targets", 10, false);
        return getDescription().replace("$1", damage + "").replace("$2", targets + "") + Lib.getSkillCostStats(hero, this);
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
        int hitsLeft = SkillConfigManager.getUseSetting(hero, this, "max-targets", 10, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        float velocity = (float) SkillConfigManager.getUseSetting(hero, this, "velocity", 0.6D, false);
        List<Entity> nearby = player.getNearbyEntities(radius, radius, radius);
        Vector vector = new Vector(0.0F, velocity, 0.0F);
        for (Entity entity : nearby) {
            if (hitsLeft <= 0)
                break;
            if ((entity instanceof LivingEntity)) {
                if (damageCheck(player, (LivingEntity) entity)) {
                    addSpellTarget(entity, hero);
                    damageEntity((LivingEntity) entity, player, damage, EntityDamageEvent.DamageCause.ENTITY_ATTACK);
                    entity.setVelocity(vector);
                    hitsLeft--;
                }
            }
        }
        long time = System.currentTimeMillis();
        if (hero.getCooldown("Smash") == null || (hero.getCooldown("Smash") - time) < 10000) {
            hero.setCooldown("Smash", time + 3000);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
