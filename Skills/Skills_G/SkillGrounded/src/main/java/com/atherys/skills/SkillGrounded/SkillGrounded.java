package com.atherys.skills.SkillGrounded;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;

public class SkillGrounded extends PassiveSkill {

    public SkillGrounded(Heroes plugin) {
        super(plugin, "Grounded");
        setDescription("Passive\nYou take less damage from Lightning based skills while you're connected to the ground.");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_LIGHTNING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage-reduction", 0.5);
        return node;
    }
}