package com.atherys.skills.SkillGroupTether;

import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillGroupTether extends ActiveSkill {

    public SkillGroupTether(Heroes plugin) {
        super(plugin, "GroupTether");
        setDescription("Tethers players around you for $1s.");
        setUsage("/skill grouptether");
        setArgumentRange(0, 0);
        setIdentifiers("skill grouptether");
        setTypes(SkillType.DEBUFFING, SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.MOVEMENT_PREVENTING, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 5000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0D, false) * hero.getSkillLevel(this)) / 1000L;
        duration = duration > 0L ? duration : 0L;
        String description = getDescription().replace("$1", duration + "");
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this);
        if (healthCost > 0) {
            description = description + " HP:" + healthCost;
        }
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (staminaCost > 0) {
            description = description + " FP:" + staminaCost;
        }
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description = description + " W:" + delay + "s";
        }
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description = description + " XP:" + exp;
        }
        return description;
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.APPLY_TEXT.node(), ""); //set to empty string from %target% was tethered!
        node.set(SkillSetting.EXPIRE_TEXT.node(), ""); //set to empty string %target% got away!
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(3));
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3, false);
        long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 5000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0D, false) * hero.getSkillLevel(this));
        duration = duration > 0L ? duration : 0L;
        int h = 0;
        Player player = hero.getPlayer();
        for (org.bukkit.entity.Entity entity : player.getNearbyEntities(r, r, r)) {
            if (h <= 2) {
                if (entity instanceof Player) {
                    if (player.hasLineOfSight(entity)) {
                        Player tplayer = (Player) entity;
                        if (damageCheck(tplayer, (LivingEntity) player)) {
                            Hero thero = plugin.getCharacterManager().getHero(tplayer);
                            thero.addEffect(new CurseEffect(this, duration, hero.getPlayer()));
                            h++;
                        }
                    }
                }
            }
        }
        if (h == 0) {
            player.sendMessage(ChatColor.GRAY + "No valid targets within range!");
            return SkillResult.CANCELLED;
        }
        for (Effect e : hero.getEffects()) {
            if ((e.isType(EffectType.DISABLE) || e.isType(EffectType.STUN) || e.isType(EffectType.ROOT) || e.isType(EffectType.SLOW)) && (!e.isType(EffectType.UNBREAKABLE))) {
                hero.removeEffect(e);
            }
        }
        hero.addEffect(new SlowEffect(this, duration, 1, false, "", "", hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class CurseEffect extends PeriodicExpirableEffect {
        private Player caster;

        public CurseEffect(Skill skill, long duration, Player caster) {
            super(skill, "Tether",caster, 20L, duration);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.DISABLE);
            this.caster = caster;
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            hero.getPlayer().sendMessage(net.md_5.bungee.api.ChatColor.GRAY + "You are silenced!");
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(net.md_5.bungee.api.ChatColor.GRAY + "You are no longer silenced!");
        }

        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            try {
                if (!player.hasLineOfSight(caster.getPlayer()) || !player.getWorld().equals(caster.getPlayer().getWorld())) {
                    this.expire();
                } else if (player.getLocation().distanceSquared(this.caster.getLocation()) > 3 * 3)
                    player.teleport(this.caster);
            } catch (IllegalArgumentException iae) {
            }
        }

        public void tickMonster(com.herocraftonline.heroes.characters.Monster mnstr) {
        }
    }
}