package com.atherys.skills.SkillGuard;

/**
 * Created by Arthur on 5/17/2015.
 */

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillGuard extends TargettedSkill {
	private String applyText;
	private String expireText;

	public SkillGuard(Heroes plugin) {
		super(plugin, "Guard");
		setDescription("Takes all damage for target party member for a few seconds.");
		setArgumentRange(0, 0);
		setUsage("/skill Guard");
		setIdentifiers("skill Guard");
		setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
		Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
		node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill% on %target%!");
		node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% on %target% expired!");
		return node;
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	@Override
	public void init() {
		super.init();
		applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill% on %target%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
		expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% on %target% expired!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
	}

	@Override
	public SkillResult use(Hero hero, LivingEntity target, String[] strings) {
		long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
		double magicResist = SkillConfigManager.getUseSetting(hero, this, "magic-resist", 0.75D, false);
		double physicalResist = SkillConfigManager.getUseSetting(hero, this, "physical-resist", 0.75D, false);

		Player player = hero.getPlayer();
		if (!hero.hasParty()) {
			Messaging.send(player, "You need to be in a party.");
			return SkillResult.CANCELLED;
		}
		if (!(target instanceof Player)) {
			return SkillResult.INVALID_TARGET;
		}
		Hero targetHero = this.plugin.getCharacterManager().getHero((Player) target);
		if (hero.equals(targetHero) || !(hero.getParty().isPartyMember(targetHero))) {
			return SkillResult.INVALID_TARGET_NO_MSG;
		}
		if (targetHero.hasEffect("GuardEffect")) {
			Messaging.send(player, "That target is already being guarded.");
			return SkillResult.INVALID_TARGET_NO_MSG;
		}
		targetHero.addEffect(new GuardEffect(this, duration, hero.getPlayer(), magicResist, physicalResist));
		return SkillResult.NORMAL;
	}

	public class GuardEffect extends ExpirableEffect {
		private final Player caster;
		private final double magicResist;
		private final double physicalResist;

		public GuardEffect(Skill skill, long duration, Player caster, double magicResist, double physicalResist) {
			super(skill, "Guard",caster, duration);
			this.caster = caster;
			this.magicResist = magicResist;
			this.physicalResist = physicalResist;
			this.types.add(EffectType.BENEFICIAL);
			this.types.add(EffectType.PHYSICAL);
			this.types.add(EffectType.UNBREAKABLE);
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			Player p = hero.getPlayer();
			broadcast(p.getLocation(), applyText, caster.getDisplayName(), p.getDisplayName(), "Guard");
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			Player p = hero.getPlayer();
			broadcast(p.getLocation(), expireText, caster.getDisplayName(), p.getDisplayName(), "Guard");
		}

		public double getMagicResist() {
			return magicResist;
		}

		public double getPhysicalResist() {
			return physicalResist;
		}
	}

	public class HeroDamageListener implements Listener {
		@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
		public void onEntityDamage(EntityDamageEvent event) {
			if (!(event.getEntity() instanceof Player)) {
				return;
			}
			if (((event.getCause() == EntityDamageEvent.DamageCause.MAGIC) ||
					(event.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK))) {
				Player player = (Player) event.getEntity();
				Hero hero = plugin.getCharacterManager().getHero(player);
				if (hero.hasEffect("Guard")) {
					SkillGuard.GuardEffect GuardEffect = (GuardEffect) hero.getEffect("Guard");
					if (event.getCause() == EntityDamageEvent.DamageCause.MAGIC) {
						event.setDamage((event.getDamage() * (1 - GuardEffect.getMagicResist())));
					} else {
						event.setDamage((event.getDamage() * (1 - GuardEffect.getPhysicalResist())));
					}
				}
			}
		}
	}
}

