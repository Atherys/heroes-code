package com.atherys.skills.SkillGuardianAngel;


import com.atherys.effects.InvulnEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillGuardianAngel extends ActiveSkill {

    public SkillGuardianAngel(Heroes plugin) {
        super(plugin, "GuardianAngel");
        setDescription("Active\n Gives you and your party within $1 blocks invulnerability for $2s");
        setUsage("/skill guardianangel");
        setArgumentRange(0, 0);
        setIdentifiers("skill guardianangel");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE, SkillType.DISABLE_COUNTERING);
    }

    @Override
    public String getDescription(Hero hero) {
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS_INCREASE, 0.0D, false) * hero.getSkillLevel(this);
        double duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 12000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false) * hero.getSkillLevel(this);
        String description = getDescription().replace("$1", radius + "").replace("$2", (int) (duration / 1000.0D) + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.RADIUS_INCREASE.node(), 0.0D);
        node.set(SkillSetting.DURATION.node(), 12000);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 0.0D);
        node.set("party-cooldown", 15000);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 12000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false) * hero.getLevel();
        InvulnEffect iEffect = new InvulnEffect(this, duration,hero);
        long pcd = (long) SkillConfigManager.getUseSetting(hero, this, "party-cooldown", 15000L, false);
        if (!hero.hasParty()) {
            hero.addEffect(iEffect);
        } else {
            long time = System.currentTimeMillis();
            double r = (double) SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "radius-increase", 0.0D, false) * hero.getLevel();
            for (Hero pHero : hero.getParty().getMembers()) {
                Player pPlayer = pHero.getPlayer();
                if (pHero.hasAccessToSkill(this) && (!hero.equals(pHero)) && (pHero.getCooldown("GuardianAngel") == null || ((pHero.getCooldown("GuardianAngel") - time) < pcd))) {
                    pHero.setCooldown("GuardianAngel", pcd + time);
                }
                if ((pPlayer.getWorld().equals(player.getWorld())) && (pPlayer.getLocation().distanceSquared(player.getLocation()) <= r * r)) {
                    pHero.addEffect(iEffect);
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
