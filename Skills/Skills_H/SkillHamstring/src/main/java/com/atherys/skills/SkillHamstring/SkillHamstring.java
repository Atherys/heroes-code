package com.atherys.skills.SkillHamstring;

import com.atherys.effects.*; import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillHamstring extends ActiveSkill {
    public SkillHamstring(Heroes plugin) {
        super(plugin, "Hamstring");
        setDescription("Active\n Your next melee attack will deal $1 more damage and slow your target for $2s");
        setUsage("/skill Hamstring");
        setArgumentRange(0, 0);
        setIdentifiers("skill Hamstring");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new HeroDamageListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("number-of-attacks", 1);
        node.set("slow-multiplier", 2);
        node.set("slow-duration", 1000);
        node.set("add-slowminning-potion", true);
        node.set("bonus-damage", 10);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 10, false);
        return getDescription().replace("$1", damage + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int attacks = SkillConfigManager.getUseSetting(hero, this, "number-of-attacks", 1, false);
        int slowMultiplier = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 2, false);
        long slowDuration = (long) SkillConfigManager.getUseSetting(hero, this, "slow-duration", 1000L, false);
        boolean slowMining = SkillConfigManager.getUseSetting(hero, this, "add-slowminning-potion", true);
        double damage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 10D, false);
        hero.addEffect(new HamstringEffect(this, duration, attacks, slowMultiplier, damage, slowDuration, slowMining,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    private static class HamstringEffect extends ExpirableEffect {
        private int attacks;
        private double damage;
        private int slowMultiplier;
        private boolean slowMining;
        private long slowDuration;

        public HamstringEffect(Skill skill, long duration, int attacks, int slowMultiplier, double damage, long slowDuration, boolean slowMining,Hero caster) {
            super(skill, "HamstringEffect",caster.getPlayer(), duration);
            this.attacks = attacks;
            this.damage = damage;
            this.slowMultiplier = slowMultiplier;
            this.slowDuration = slowDuration;
            this.slowMining = slowMining;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Hamstring expired");
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }

        public double getDamage() {
            return damage;
        }

        public int getSlowMultiplier() {
            return slowMultiplier;
        }

        public long getSlowDuration() {
            return slowDuration;
        }

        public boolean addSlowMining() {
            return slowMining;
        }
    }

    public class HeroDamageListener implements Listener {
        private final Skill skill;

        public HeroDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero) ||
                    (event.isCancelled()) ||
                    !(event.getEntity() instanceof Player) ||
                    (event.getDamage() == 0) ||
                    (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero target = plugin.getCharacterManager().getHero((Player) event.getEntity());
            Hero hero = (Hero) event.getDamager();
            if (!event.getDamager().hasEffect("HamstringEffect"))
                return;
            HamstringEffect he = (HamstringEffect) hero.getEffect("HamstringEffect");
            target.addEffect(new SlowEffect(skill, he.getSlowDuration(), he.getSlowMultiplier(), he.addSlowMining(), "", "", hero));
            event.setDamage(event.getDamage() + he.getDamage());
            getAttacksLeft(hero, he);
        }

        private void getAttacksLeft(Hero hero, HamstringEffect he) {
            if (he.getAttacksLeft() <= 1) {
                hero.removeEffect(he);
            } else {
                he.setAttacksLeft(he.getAttacksLeft() - 1);
            }
        }
    }
}
