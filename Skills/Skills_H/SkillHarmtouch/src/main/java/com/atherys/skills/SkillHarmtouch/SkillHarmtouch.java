package com.atherys.skills.SkillHarmtouch;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillHarmtouch extends TargettedSkill {
    public SkillHarmtouch(Heroes plugin) {
        super(plugin, "Harmtouch");
        setDescription("You deal magic damage based on the target's max health.");
        setUsage("/skill harmtouch <target>");
        setArgumentRange(0, 0);
        setIdentifiers("skill harmtouch");
        setTypes(SkillType.ABILITY_PROPERTY_DARK, SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.ABILITY_PROPERTY_MAGICAL);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("min-damage", 15);
        node.set("max-damage", 50);
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        double damage = (target.getMaxHealth()-150)/4;
        double minDamage = SkillConfigManager.getUseSetting(hero, this, "min-damage", 15, false);
        double maxDamage = SkillConfigManager.getUseSetting(hero, this, "max-damage", 50, false);
        addSpellTarget(target, hero);
        damage = damage >= minDamage ? damage <= maxDamage ? damage : maxDamage : minDamage;
        damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
