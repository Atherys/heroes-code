package com.atherys.skills.SkillHealBomb;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillHealBomb extends TargettedSkill {
	public SkillHealBomb(Heroes plugin) {
		super(plugin, "HealBomb");
		setDescription("Heal the target for $1 and deal $2 damage players $3 blocks around them. R:$4");
		setUsage("/skill healbomb [target]");
		setArgumentRange(0, 1);
		setIdentifiers("skill healbomb");
		setTypes(SkillType.HEALING, SkillType.DAMAGING, SkillType.SILENCEABLE);
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.HEALTH.node(), 10);
		node.set(SkillSetting.DAMAGE.node(), 5);
		node.set(SkillSetting.RADIUS.node(), 6);
		node.set(SkillSetting.MAX_DISTANCE.node(), 15);
		return node;
	}

	@Override
	public SkillResult use(Hero hero, LivingEntity target, String[] strings) {
		Player player = hero.getPlayer();

		//if (le.equals(player)) {
		//    broadcastExecuteText(hero, player);
		//     healBomb(hero, hero);
		//     le.getLocation().getWorld().createExplosion(le.getLocation(), 0.0F, false);
		if (!(target instanceof Player)) {
			return SkillResult.INVALID_TARGET;
		}
		if (target.equals(player)) {
			return SkillResult.INVALID_TARGET;
		}else {
			Hero target1 = plugin.getCharacterManager().getHero((Player) target);
			if (hero.getParty() != null && hero.getParty().getMembers().contains(target1)) {
				broadcastExecuteText(hero, target);
				healBomb(hero, target1);
			}
			target.getLocation().getWorld().createExplosion(target.getLocation(), 0.0F, false);
			return SkillResult.NORMAL;
		}
	}

    private void healBomb(Hero hero, Hero target) {
        Player player = hero.getPlayer();
        Player tPlayer = target.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 6, false);
        radius = radius > 0 ? radius : 0;
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE.node(), 5, false);
        damage = damage > 0 ? damage : 0;
        double health = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH.node(), 10, false));
        health = health > 0 ? health : 0;
        HeroRegainHealthEvent hr = new HeroRegainHealthEvent(target, health, this, hero);
        this.plugin.getServer().getPluginManager().callEvent(hr);
        if (!hr.isCancelled()) {
            target.heal(hr.getDelta());
        }
        for (Entity e : tPlayer.getNearbyEntities(radius, radius, radius)) {
            if (e instanceof LivingEntity) {
                if (e instanceof Creature) {
                    Creature c = (Creature) e;
                    damageEntity(c, player, damage, DamageCause.MAGIC);
                    //c.damage(damage, player);
                } else if (e instanceof Player) {
                    Player p = (Player) e;
                    if (hero.getParty() == null || !hero.getParty().getMembers().contains(plugin.getCharacterManager().getHero(p))) {
                        damageEntity(p, player, damage, DamageCause.MAGIC);
                        //p.damage(damage, player);
                    }
                }
            }
        }
    }
}