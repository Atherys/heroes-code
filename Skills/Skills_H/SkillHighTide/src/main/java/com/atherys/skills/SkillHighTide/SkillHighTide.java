package com.atherys.skills.SkillHighTide;

import com.atherys.effects.SkillCastingEffect;
import com.atherys.effects.WaveStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.Set;

public class SkillHighTide extends ActiveSkill {
    public SkillHighTide(Heroes plugin) {
        super(plugin, "HighTide");
        setUsage("/skill hightide");
        setArgumentRange(0, 0);
        setIdentifiers("skill hightide");
        setDescription("Active\nYou start channeling a high tide for $1 seconds, healing the 4 lowest health allies within $2 blocks for $3 health and gain a Wave stack every $4 seconds.");
        setTypes(SkillType.SILENCEABLE, SkillType.HEALING);
    }

    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 15000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 3000, false);
        double range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10D, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 40D, false);
        return getDescription().replace("$1", (int) (duration / 1000) + "").replace("$2", (int) range + "").replace("$3", (int) heal + "").replace("$4", (int) (period / 1000) + " ") + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(10));
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(40));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(3000));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(15000));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        broadcastExecuteText(hero);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 15000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 3000, false);
        double range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10D, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 40D, false);
        hero.addEffect(new HighTideCastingEffect(this, duration,hero));
        hero.addEffect(new HighTideEffect(this, period, duration, range, heal,hero));
        return SkillResult.NORMAL;
    }

    public class HighTideCastingEffect extends SkillCastingEffect {

        public HighTideCastingEffect(Skill skill, long duration,Hero caster) {
            super(skill, duration, false, "$1 is channeling their $2!", "$1 finished channeling their $2.", "$1 got interrupted from channeling their $2!",caster);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (hero.hasEffect("HighTide")) {
                hero.removeEffect(hero.getEffect("HighTide"));
            }
        }
    }

    public class HighTideEffect extends PeriodicExpirableEffect {
        private final double range;
        private final double heal;

        public HighTideEffect(Skill skill, long period, long duration, double range, double heal,Hero caster) {
            super(skill, "HighTide",caster.getPlayer(), period, duration);
            this.range = range;
            this.heal = heal;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.HEALING);
            this.types.add(EffectType.WATER);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            int stacks = SkillConfigManager.getUseSetting(hero, skill, "wave-stacks", 1, false);
            if (hero.hasParty()) {
                Set<Hero> party = hero.getParty().getMembers();
                Player a = null, b = null, c = null, d = null;
                for (Hero pHero : party) {
                    Player pPlayer = pHero.getPlayer();
                    if (player.getWorld().equals(pPlayer.getWorld()) && (((pPlayer.getLocation()).distanceSquared(player.getLocation())) <= range * range)) {
                        if (a == null)
                            a = pPlayer;
                        else if (b == null)
                            b = pPlayer;
                        else if (c == null)
                            c = pPlayer;
                        else if (d == null)
                            d = pPlayer;
                        else if (a.getHealth() > pPlayer.getHealth())
                            a = pPlayer;
                        else if (b.getHealth() > pPlayer.getHealth())
                            b = pPlayer;
                        else if (c.getHealth() > pPlayer.getHealth())
                            c = pPlayer;
                        else if (d.getHealth() > pPlayer.getHealth())
                            d = pPlayer;
                    }
                }
                WaveStackEffect.addWaveStacks(skill, hero, stacks);
                if (a != null) {
                    Hero aHero = plugin.getCharacterManager().getHero(a);
                    Lib.healHero(aHero, heal, skill, hero);
                }
                if (b != null) {
                    Hero bHero = plugin.getCharacterManager().getHero(a);
                    Lib.healHero(bHero, heal, skill, hero);
                }
                if (c != null) {
                    Hero cHero = plugin.getCharacterManager().getHero(a);
                    Lib.healHero(cHero, heal, skill, hero);
                }
                if (d != null) {
                    Hero dHero = plugin.getCharacterManager().getHero(a);
                    Lib.healHero(dHero, heal, skill, hero);
                }
            } else {
                WaveStackEffect.addWaveStacks(skill, hero, stacks);
                Lib.healHero(hero, heal, skill, hero);
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }
}