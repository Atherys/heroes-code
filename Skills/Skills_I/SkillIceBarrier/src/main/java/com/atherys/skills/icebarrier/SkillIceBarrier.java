package com.atherys.skills.icebarrier;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillIceBarrier extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillIceBarrier(Heroes plugin) {
        super(plugin, "IceBarrier");
        setDescription("Toggle-able Passive that slows people when they hit you.");
        setArgumentRange(0, 0);
        setTypes(SkillType.ABILITY_PROPERTY_ICE, SkillType.SILENCEABLE, SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillIceBarrierListener(this), plugin);
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 2000);
        node.set("amplifier", 2);
        node.set("slow-cooldown", 5000);
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, "on-text", "%hero% activates their %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, "off-text", "%hero% stops their %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("IceBarrier")) {
            hero.removeEffect(hero.getEffect("IceBarrier"));
        } else {
            hero.addEffect(new IceBarrierEffect(this));
        }
        return SkillResult.NORMAL;
    }

    public class IceBarrierEffect extends Effect {

        public IceBarrierEffect(SkillIceBarrier skill) {
            super(skill, "IceBarrier");
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.ICE);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.UNBREAKABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "IceBarrier");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "IceBarrier");
        }
    }

    public class SkillIceBarrierListener implements Listener {
        private final Skill skill;

        public SkillIceBarrierListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK || !(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Hero)) {
                return;
            }
            Player player = (Player)event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("IceBarrier")) {
                long time = System.currentTimeMillis();
                if (hero.getCooldown("IceBarrierCooldown") == null || hero.getCooldown("IceBarrierCooldown") <= time) {
                    Hero tHero = (Hero)event.getDamager();
                    if (damageCheck(player, (LivingEntity) tHero.getPlayer())) {
                        long duration = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 2000, false);
                        int amplifier = SkillConfigManager.getUseSetting(hero, skill, "amplifier", 2, false);
                        long cooldown = SkillConfigManager.getUseSetting(hero, skill, "slow-cooldown", 5000, false);
                        hero.setCooldown("IceBarrierCooldown", cooldown + time);
                        tHero.addEffect(new SlowEffect(skill, "IceBarrierSlow", duration, amplifier, false, "", "", hero));
                    }
                }
            }
        }
    }
}
