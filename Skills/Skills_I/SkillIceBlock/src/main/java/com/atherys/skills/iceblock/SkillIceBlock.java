package com.atherys.skills.iceblock;


import com.atherys.skills.iceblock.effects.IceBlockEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillIceBlock extends ActiveSkill {

    private long defDuration = 5000;
    private int defBlockRadius = 4;
    private double defStunTime = 500;
    private int defSpeedMulti = 2;
    private String defBlockMat = "ICE";
    private int defParticleCount = 4;

    public SkillIceBlock(Heroes plugin) {
        super(plugin, "IceBlock");
        setDescription("Active\nEncase yourself in $0 and become Invulnerable for $1 seconds. Enemies are slowed while within $2 blocks of you. When you break out of the icicle, stun enemies within $2 blocks for $3 seconds. You may second cast to break out early.");
        setUsage("/skill iceblock");
        setArgumentRange(0, 0);
        setIdentifiers("skill iceblock");
        setTypes(SkillType.ABILITY_PROPERTY_ICE, SkillType.SILENCEABLE, SkillType.AREA_OF_EFFECT, SkillType.DEBUFFING);
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();

        double invulnTime = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, defDuration, false);
        int blockRadius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, defBlockRadius, false);
        double stunTime = SkillConfigManager.getUseSetting(hero, this, "stun-time", defStunTime, false);
        String material = SkillConfigManager.getUseSetting(hero, this, "block-material", defBlockMat );

        return description.replace("$0", material.toLowerCase() ).replace("$1", invulnTime/1000 + "" ).replace("$2", blockRadius + "").replace("$3", stunTime/1000 + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), defBlockRadius);
        node.set(SkillSetting.DURATION.node(), defDuration);
        node.set("stun-time", defStunTime);
        node.set("speed-multiplier", defSpeedMulti);
        node.set("block-material", defBlockMat);
        node.set("particle-count", defParticleCount);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {

        if ( hero.hasEffect("IceBlockEffect") ) {
            Effect invulnEffect = hero.getEffect("IceBlockEffect");
            hero.removeEffect(invulnEffect);
            hero.removeEffect(hero.getEffect("Invuln"));
            return SkillResult.NORMAL;
        }

        double invulnTime = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, defDuration, false);
        int blockRadius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, defBlockRadius, false);
        double stunTime = SkillConfigManager.getUseSetting(hero, this, "stun-time", defStunTime, false);
        int speedMulti = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", defSpeedMulti, false);
        int particleCount = SkillConfigManager.getUseSetting(hero, this, "particle-count", defParticleCount, false);
        long cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 0, false );

        Set<Block> blocks = selfIcicle ( hero );
        hero.addEffect(new IceBlockEffect(this, "IceBlockEffect", hero.getPlayer(), (long) invulnTime, 20L, blocks, blockRadius, (long) stunTime, speedMulti, particleCount, cooldown));
        return SkillResult.CANCELLED;
    }

    public Set<Block> selfIcicle(Hero hero) {
        final Player targetPlayer = hero.getPlayer();
        final Material setter = Material.valueOf(SkillConfigManager.getUseSetting(hero, this, "block-material", defBlockMat));
        Location tploc = targetPlayer.getLocation();
        Location[] locs = new Location[]{
                //Layer one
                targetPlayer.getLocation().add(0, -1, 0), targetPlayer.getLocation().add(0, -1, -1),
                targetPlayer.getLocation().add(-1, -1, -1), targetPlayer.getLocation().add(-1, -1, 0), targetPlayer.getLocation().add(-1, -1, +1),
                targetPlayer.getLocation().add(0, -1, +1), targetPlayer.getLocation().add(+1, -1, +1), targetPlayer.getLocation().add(+1, -1, 0),
                targetPlayer.getLocation().add(+1, -1, -1),
                //Layer two
                targetPlayer.getLocation().add(0, 0, -1),
                targetPlayer.getLocation().add(-1, 0, -1), targetPlayer.getLocation().add(-1, 0, 0), targetPlayer.getLocation().add(-1, 0, +1),
                targetPlayer.getLocation().add(0, 0, +1), targetPlayer.getLocation().add(+1, 0, +1), targetPlayer.getLocation().add(+1, 0, 0),
                targetPlayer.getLocation().add(+1, 0, -1),
                //Layer three
                targetPlayer.getLocation().add(0, 1, -1), targetPlayer.getLocation().add(-1, 1, 0), targetPlayer.getLocation().add(0, 1, +1),
                targetPlayer.getLocation().add(+1, 1, 0),
                //layer four
                targetPlayer.getLocation().add(0, 2, -1), targetPlayer.getLocation().add(-1, 2, 0), targetPlayer.getLocation().add(0, 2, +1),
                targetPlayer.getLocation().add(+1, 2, 0),
                //layer five
                targetPlayer.getLocation().add(0, 3, -1), targetPlayer.getLocation().add(-1, 3, 0), targetPlayer.getLocation().add(0, 3, +1),
                targetPlayer.getLocation().add(+1, 3, 0),
                //layer six and seven
                targetPlayer.getLocation().add(0, 4, 0), targetPlayer.getLocation().add(0, 5, 0)
        };
        final Set<Block> blocks = new HashSet<>();
        for (Location loc : locs) {
            if (loc.getBlock().getType() == Material.AIR) {
                loc.getBlock().setType(setter);
                loc.getBlock().setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
                blocks.add(loc.getBlock());
            }
        }


        double x = Math.floor(tploc.getX());
        double z = Math.floor(tploc.getZ());

        x += .5;
        z += .5;
        targetPlayer.getPlayer().teleport(new Location(targetPlayer.getWorld(), x, targetPlayer.getLocation().getY(), z, targetPlayer.getLocation().getYaw()
                , targetPlayer.getLocation().getPitch()));

        targetPlayer.getWorld().playSound(targetPlayer.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 1.0F, 1.0F);

        return blocks;
    }
}

