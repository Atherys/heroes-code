package com.atherys.skills.iceblock.effects;

import com.atherys.core.Core;
import com.atherys.effects.SlowEffect;
import com.atherys.effects.StunEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.*;
import com.herocraftonline.heroes.characters.effects.common.InvulnerabilityEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.*;
import org.bukkit.Effect;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

public class IceBlockEffect extends PeriodicExpirableEffect {

    private final Set<Block> blocks;
    private int slowTaskID;
    private int radius;
    private long stunDuration;
    private int speedMulti;
    private int particleCount;
    private long cooldown;

    public IceBlockEffect(Skill skill, String name, Player applier, long duration, long period , Set<Block> blocks, int radius, long stunDuration, int speedMulti, int particleCount, long cooldown ) {
        super(skill, name, applier, period ,duration);
        this.blocks = blocks;
        this.radius = radius;
        this.stunDuration = stunDuration;
        this.speedMulti = speedMulti;
        this.particleCount = particleCount;
        this.cooldown = cooldown;
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        broadcast(player.getLocation(), "$1 has encased themselves in ice.", player.getDisplayName() );
        slowTaskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Heroes.getInstance(), new Runnable() {
            @Override
            public void run() {
                List<Entity> nearbyEntities = player.getNearbyEntities( radius , radius, radius );
                for ( Entity entity : nearbyEntities ) {
                    if ( entity instanceof Player ) {
                        SlowEffect slow = new SlowEffect(skill, 1000, speedMulti, false, null, null, hero);
                        player.getLocation().getWorld().spawnParticle(Particle.VILLAGER_HAPPY, player.getLocation(), 1);
                        Heroes.getInstance().getCharacterManager().getHero( (Player) entity ).addEffect(slow);
                    }
                }
            }
        },0 ,10 );
        hero.addEffect( new InvulnerabilityEffect(skill, player, this.getDuration()));
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        removeBlocks(blocks);

        List<Entity> nearbyEntities = player.getNearbyEntities( radius , radius, radius );

        for ( Entity entity : nearbyEntities ) {
            if ( entity instanceof Player && entity != hero.getEntity() ) {
                StunEffect stunEffect = new StunEffect( super.getSkill(), stunDuration, hero );
                Hero target = Heroes.getInstance().getCharacterManager().getHero((Player) entity);
                target.addEffect(stunEffect);
            }
        }
        hero.removeEffect(hero.getEffect("Invuln"));
        Bukkit.getServer().getScheduler().cancelTask(slowTaskID);
        broadcast(player.getLocation(), "$1 has broken out of their icy shell!", player.getDisplayName());
        hero.setCooldown("IceBlock", cooldown + System.currentTimeMillis());
    }

    // this was ripped out of the Lib class from HeroesAddon. That class uses old NMS library packages which have not been updated.
    private void removeBlocks(Set<Block> blocks) {
        Set<Chunk> chunks = new HashSet<>();
        for (Block block : blocks) {
            Chunk chunk = block.getChunk();
            if (!chunks.contains(chunk) && !chunk.isLoaded()) {
                chunk.load();
                chunks.add(chunk);
            }
        }

        for (Block block : blocks) {
            if (block.hasMetadata("HeroesUnbreakableBlock")) {
                block.removeMetadata("HeroesUnbreakableBlock", Core.getHeroes());
            }
            if (block.getType() != Material.AIR) {
                this.getApplier().getWorld().playEffect( block.getLocation(), Effect.STEP_SOUND, block.getType() );
                this.getApplier().getWorld().spawnParticle( Particle.BLOCK_CRACK, block.getLocation(), particleCount, 0, 0, 0, 10, new MaterialData( block.getType() ) );
                block.setType(Material.AIR);
            }
        }

        //Error checking
        for (Block block : blocks) {
            if (block.getType() != Material.AIR) {
                Bukkit.getServer().getLogger().log(Level.SEVERE, "Error removing block " + block.getType().name() + " in chunk " + block.getChunk().getX() + ", " + block.getChunk().getZ());
            }
        }

        for (Chunk chunk : chunks) {
            if (chunk.isLoaded()) {
                chunk.unload();
            } else {
                Bukkit.getServer().getLogger().log(Level.SEVERE, "Error: Chunk was never loaded, could not remove blocks!");
            }
        }
    }

    @Override
    public void tickMonster(Monster monster) {

    }

    @Override
    public void tickHero(Hero hero) {
        for(double[] locations : Lib.playerParticleCoords) {
            Location location = hero.getPlayer().getLocation();
            location.add(locations[0], locations[1], locations[2]);
            hero.getPlayer().getWorld().spigot().playEffect(location, Effect.HAPPY_VILLAGER);
        }
    }

}
