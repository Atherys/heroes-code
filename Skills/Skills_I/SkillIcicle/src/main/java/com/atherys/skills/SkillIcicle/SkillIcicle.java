package com.atherys.skills.SkillIcicle;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillIcicle extends TargettedSkill {

	public SkillIcicle(Heroes plugin) {
		super(plugin, "Icicle");
		setDescription("Encases your target in ice");
		setUsage("/skill Icicle");
		setArgumentRange(0, 0);
		setIdentifiers("skill Icicle");
		setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
	}

	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
		node.set("block-type", "ICE");
		return node;
	}



	public String getDescription(Hero hero) {
		return getDescription();
	}
	public SkillResult use(Hero hero,LivingEntity target,String[] args) {
		Player player = hero.getPlayer();
		if (!(target instanceof Player)) {
			return SkillResult.INVALID_TARGET;
		}
		else if(target instanceof Player){
			Player tplayer = (Player)target;
			Hero targetHero = this.plugin.getCharacterManager().getHero(tplayer);

			if (!player.hasLineOfSight(tplayer)) {
				return SkillResult.INVALID_TARGET_NO_MSG;
			}
			else if (player.equals(target)){
				return SkillResult.INVALID_TARGET_NO_MSG;
			}else if (hero.hasParty()){
				if(hero.getParty().getMembers().contains(targetHero)) {
					return SkillResult.INVALID_TARGET;
				}
			} else {

				Icicle(hero,targetHero);
				return SkillResult.NORMAL;
			}
		}
		return SkillResult.FAIL;
	}

	public void Icicle(Hero hero,Hero target) {
		final Player targetPlayer = target.getPlayer();
		long shieldduration = SkillConfigManager.getUseSetting(hero, this, "duration", 5000, false);
		final Material setter = Material.valueOf(SkillConfigManager.getUseSetting(hero, this, "BlockType", "ICE"));
		Location tploc = targetPlayer.getLocation();
		Location[] locs = new Location[]{
				//Layer one
				targetPlayer.getLocation().add(0, -1, 0), targetPlayer.getLocation().add(0, -1, -1),
				targetPlayer.getLocation().add(-1, -1, -1), targetPlayer.getLocation().add(-1, -1, 0), targetPlayer.getLocation().add(-1, -1, +1),
				targetPlayer.getLocation().add(0, -1, +1), targetPlayer.getLocation().add(+1, -1, +1), targetPlayer.getLocation().add(+1, -1, 0),
				targetPlayer.getLocation().add(+1, -1, -1),
				//Layer two
				targetPlayer.getLocation().add(0, 0, -1),
				targetPlayer.getLocation().add(-1, 0, -1), targetPlayer.getLocation().add(-1, 0, 0), targetPlayer.getLocation().add(-1, 0, +1),
				targetPlayer.getLocation().add(0, 0, +1), targetPlayer.getLocation().add(+1, 0, +1), targetPlayer.getLocation().add(+1, 0, 0),
				targetPlayer.getLocation().add(+1, 0, -1),
				//Layer three
				targetPlayer.getLocation().add(0, 1, -1), targetPlayer.getLocation().add(-1, 1, 0), targetPlayer.getLocation().add(0, 1, +1),
				targetPlayer.getLocation().add(+1, 1, 0),
				//layer four
				targetPlayer.getLocation().add(0, 2, -1), targetPlayer.getLocation().add(-1, 2, 0), targetPlayer.getLocation().add(0, 2, +1),
				targetPlayer.getLocation().add(+1, 2, 0),
				//layer five
				targetPlayer.getLocation().add(0, 3, -1), targetPlayer.getLocation().add(-1, 3, 0), targetPlayer.getLocation().add(0, 3, +1),
				targetPlayer.getLocation().add(+1, 3, 0),
				//layer six and seven
				targetPlayer.getLocation().add(0, 4, 0), targetPlayer.getLocation().add(0, 5, 0)
		};

		final Set<Block> blocks = new HashSet<>();
		for (Location loc : locs) {
			if (loc.getBlock().getType() == Material.AIR) {
				loc.getBlock().setType(setter);
				loc.getBlock().setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
				blocks.add(loc.getBlock());
			}
		}
		//targetPlayer.teleport(tploc);


		double x = Math.floor(tploc.getX());
		double z = Math.floor(tploc.getZ());

		x += .5;
		z += .5;
		targetPlayer.getPlayer().teleport(new Location(targetPlayer.getWorld(), x, targetPlayer.getLocation().getY(), z, targetPlayer.getLocation().getYaw()
				, targetPlayer.getLocation().getPitch()));

		hero.addEffect(new IcicleEffect(this, "IcicleEffect", shieldduration, blocks,hero));
	}


	public class IcicleEffect extends ExpirableEffect {
		private final Set<Block> blocks;

		public IcicleEffect(Skill skill, String name, long duration,Set<Block> blocks,Hero caster ) {
			super(skill, name,caster.getPlayer(), duration);
			this.blocks = blocks;
			types.add(EffectType.DISPELLABLE);
			types.add(EffectType.FORM);
			types.add(EffectType.ICE);
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			Player player = hero.getPlayer();
			broadcast(player.getLocation(), "$1 has frozen the area", player.getDisplayName());
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			Player player = hero.getPlayer();
			Lib.removeBlocks(blocks);
			if (this.isExpired()) {
				broadcast(player.getLocation(), "$1's Icicle has thawed ", player.getDisplayName());
			}
		}
	}
}
