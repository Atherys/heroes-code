package me.apteryx.immunity;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillType;
import me.apteryx.immunity.events.ImmunityDamageEvent;
import org.bukkit.Bukkit;

/**
 * @author apteryx
 * @time 4:01 PM
 * @since 12/26/2016
 */
public class Immunity extends PassiveSkill {

    public Immunity(Heroes plugin) {
        super(plugin, "Immunity");
        setDescription("Grants immunity from all poison damage effects.");
        setTypes(SkillType.UNINTERRUPTIBLE, SkillType.DISPELLING);
        Bukkit.getPluginManager().registerEvents(new ImmunityDamageEvent(), this.plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription();
    }
}
