package me.apteryx.immunity.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author apteryx
 * @time 4:09 PM
 * @since 12/26/2016
 */
public class ImmunityDamageEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPoisonDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("Immunity")) {
               //Bukkit.getLogger().info(hero.getName() + " has immunity.");
                for (Effect effect : hero.getEffects()) {
                    //Bukkit.getLogger().info("Effect: " + effect.getName());
                    for (EffectType type : effect.types) {
                        if (type.name().equalsIgnoreCase("poison")) {
                            hero.removeEffect(effect);
                            event.setCancelled(true);
                        }
                    }
                }

                if (event.getCause() == EntityDamageEvent.DamageCause.POISON) {
                    event.setCancelled(true);
                }
            }
        }
    }

}
