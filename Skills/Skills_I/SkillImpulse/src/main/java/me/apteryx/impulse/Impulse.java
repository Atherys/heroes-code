package me.apteryx.impulse;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import me.apteryx.impulse.events.ImpulseDamageEvent;
import me.apteryx.impulse.events.ImpulseManaEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author apteryx
 * @time 8:47 PM
 * @since 12/12/2016
 */
public class Impulse extends PassiveSkill {

    private static Impulse instance;


    public Impulse(Heroes plugin) {
        super(plugin, "Impulse");
        setDescription("When your mana is lowered to 0, your next melee within %melee-duration% seconds will deal %damage% extra magic damage and knockback the enemy significantly.");
        instance = this;
        Bukkit.getPluginManager().registerEvents(new ImpulseDamageEvent(), this.plugin);
        Bukkit.getPluginManager().registerEvents(new ImpulseManaEvent(), this.plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription().replace("%melee-duration%", SkillConfigManager.getUseSetting(hero, this, "melee-duration", 5000, false) / 1000+"").replace("%damage%", SkillConfigManager.getUseSetting(hero, this, "damage", 50, false)+"");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage", 50);
        node.set("melee-duration", 5000);
        node.set("horizontal-knockback", 1.5);
        node.set("verticle-knockback", 1.1);
        return node;
    }

    public static Impulse getInstance() {
        return instance;
    }
}