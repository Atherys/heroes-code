package me.apteryx.impulse.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import me.apteryx.impulse.Impulse;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author apteryx
 * @time 10:58 PM
 * @since 12/12/2016
 */
public class ImpulseImbueEffect extends ExpirableEffect {

    public ImpulseImbueEffect(Skill skill, Player applier, long duration) {
        super(skill, "ImpulseImbue", applier, duration);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().setMetadata("ImpulseHit", new FixedMetadataValue(Impulse.getInstance().plugin, "We don't allow gay kids on this server."));
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (hero.getPlayer().hasMetadata("ImpulseHit")) {
            hero.getPlayer().removeMetadata("ImpulseHit", Impulse.getInstance().plugin);
        }
    }
}