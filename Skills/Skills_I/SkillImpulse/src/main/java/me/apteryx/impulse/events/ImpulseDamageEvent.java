package me.apteryx.impulse.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import me.apteryx.impulse.Impulse;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

/**
 * @author apteryx
 * @time 9:06 PM
 * @since 12/12/2016
 */
public class ImpulseDamageEvent implements Listener {

    @EventHandler
    public void onImpulseAttack(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            if (event.getEntity() instanceof LivingEntity) {
                if (event.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
                    Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getDamager());
                    if (hero.hasEffect("Impulse")) {
                        if (hero.getPlayer().hasMetadata("ImpulseHit")) {
                            if (hero.getCooldown("Impulse") == null) {
                                hero.setCooldown("Impulse", 0);
                            }

                            if (hero.getCooldown("Impulse") <= System.currentTimeMillis()) {
                                event.setCancelled(true);
                                Impulse.getInstance().damageEntity((LivingEntity) event.getEntity(), hero.getPlayer(), (double)SkillConfigManager.getUseSetting(hero, Impulse.getInstance(), "damage", 50, false), EntityDamageEvent.DamageCause.MAGIC, false);
                                hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_ENDERDRAGON_HURT, 1.0F, 1.5F);
                                hero.getPlayer().getWorld().spigot().playEffect(hero.getPlayer().getEyeLocation().add(hero.getPlayer().getLocation().getDirection().normalize()), Effect.FIREWORKS_SPARK, 0, 0, 0.4f, 0.4f, 0.4f, 0.3f, 55, 128);
                                hero.getPlayer().getWorld().spigot().playEffect(hero.getPlayer().getEyeLocation().add(hero.getPlayer().getLocation().getDirection().normalize()), Effect.MAGIC_CRIT, 0, 0, 0.4f, 0.4f, 0.4f, 0.3f, 55, 128);
                                Vector velocity = event.getEntity().getLocation().add(0, 1, 0).toVector().subtract(hero.getPlayer().getLocation().toVector());
                                velocity = velocity.multiply(SkillConfigManager.getUseSetting(hero, Impulse.getInstance(), "horizontal-velocity", 1.5, false));
                                velocity.setY(SkillConfigManager.getUseSetting(hero, Impulse.getInstance(), "vertical-velocity", 1.1, false));
                                event.getEntity().setVelocity(velocity);
                                hero.setCooldown("Impulse", SkillConfigManager.getUseSetting(hero, Impulse.getInstance(), "cooldown", 1000, false) + System.currentTimeMillis());
                            }
                        }
                    }
                }
            }
        }
    }
}