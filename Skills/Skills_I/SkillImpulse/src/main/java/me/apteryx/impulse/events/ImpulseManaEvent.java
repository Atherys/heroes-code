package me.apteryx.impulse.events;

import com.herocraftonline.heroes.api.events.ManaChangeEvent;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import me.apteryx.impulse.Impulse;
import me.apteryx.impulse.effects.ImpulseImbueEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author apteryx
 * @time 9:40 PM
 * @since 12/12/2016
 */
public class ImpulseManaEvent implements Listener {

    @EventHandler
    public void onMana(ManaChangeEvent event) {
        if (event.getHero().hasEffect("Impulse")) {
            if (event.getHero().getPlayer().getTicksLived() > 120 && !event.getHero().getPlayer().isDead()) {
                if (event.getFinalMana() == 0) {
                    Bukkit.getLogger().info("Mana is 0.");
                    if (!event.getHero().hasEffect("ImpulseImbue")) {
                        event.getHero().addEffect(new ImpulseImbueEffect(Impulse.getInstance(), event.getHero().getPlayer(), SkillConfigManager.getUseSetting(event.getHero(), Impulse.getInstance(), "melee-duration", 5000, false)));
                    }
                }
            }
        }
    }
}