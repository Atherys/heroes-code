package com.atherys.skills.SkillInfernalBlade;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillInfernalBlade extends ActiveSkill {
    public SkillInfernalBlade(Heroes plugin) {
        super(plugin, "InfernalBlade");
        setDescription("Active\n when you melee opponents, deal your melee damage to all other enemies in a cone in front of you and within 7 blocks");
        setUsage("/skill infernalblade");
        setArgumentRange(0, 0);
        setIdentifiers("skill infernalblade");
        setTypes(SkillType.BUFFING);
        Bukkit.getPluginManager().registerEvents(new InfernalBladeListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("weapons", Util.swords);
	    node.set("min-damage", 5D);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double minDamage = SkillConfigManager.getUseSetting(hero, this, "min-damage", 5D, false);
        return getDescription().replace("$1", (int) minDamage + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
	    long radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 7, false);
	    long angle = SkillConfigManager.getUseSetting(hero, this, "angle", 60, false);
        hero.addEffect(new InfernalBladeEffect(this, duration,radius,angle,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    private class InfernalBladeEffect extends ExpirableEffect {

        public InfernalBladeEffect(Skill skill, long duration, long radius, long angle,Hero caster) {
            super(skill, "InfernalBladeEffect",caster.getPlayer(), duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.FIRE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "InfernalBlade expired!");
        }
    }


    public class InfernalBladeListener implements Listener {
        private final Skill skill;


        public InfernalBladeListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
        public void onWeaponDamage(final WeaponDamageEvent event) {
	        if (!(event.getDamager() instanceof Hero) ||
			        !(event.getEntity() instanceof Player) ||
			        (event.getDamage() == 0) ||
			        (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
		        return;
	        }
	        final Hero hero = (Hero) event.getDamager();

	        if (!event.getDamager().hasEffect("InfernalBladeEffect"))
		        return;
	        final Player player = hero.getPlayer();
	        ItemStack item = player.getItemInHand();
	        if (!SkillConfigManager.getUseSetting(hero, this.skill, "weapons", Util.swords).contains(item.getType().name()))
		        return;

	        Bukkit.getScheduler().scheduleSyncDelayedTask(this.skill.plugin, new Runnable() {
		        public void run() {
			        if (!event.getDamager().hasEffect("InfernalBladeEffect"))
				        return;
			        double radius = SkillConfigManager.getUseSetting(hero, skill, "radius", 7, false);
			        double angle = SkillConfigManager.getUseSetting(hero, skill, "angle", 60, false);
			        List<Entity> nearbyPlayers = new ArrayList<Entity>();
			        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
				        if ((entity instanceof Player)) {
					        if (damageCheck(player, (LivingEntity) entity)) {
						        nearbyPlayers.add(entity);
					        }
				        }
			        }

			        Vector playerVector = player.getLocation().toVector();


			        for (Entity target : Lib.getEntitiesInCone(nearbyPlayers, playerVector, (float) radius, (float) angle, player.getLocation().getDirection())) {
				        if (target instanceof Player) {
					        if (!(((Player) target).getPlayer() ==  event.getEntity())){
						        Hero tHero = plugin.getCharacterManager().getHero((Player) target);
						        addSpellTarget(event.getEntity(), hero);
						        damageEntity(tHero.getPlayer(), player, getInfernalDamage(hero), EntityDamageEvent.DamageCause.MAGIC, true);
						        tHero.getPlayer().getWorld().playSound(tHero.getPlayer().getLocation(), Sound.ENTITY_BLAZE_BURN, 1F, 1F);
					        }

				        }

			        }
			        //Hero tHero = plugin.getCharacterManager().getHero((Player) event.getEntity());
			        //damageEntity(tHero.getPlayer(), player, getInfernalDamage(hero), EntityDamageEvent.DamageCause.MAGIC, true);
			        //tHero.getPlayer().getWorld().playSound(tHero.getPlayer().getLocation(), Sound.BLAZE_BREATH, 1F, 1F);
			        event.setCancelled(true);
		        }
	        }, 10L);


	        //hero.removeEffect(hero.getEffect("InfernalBladeEffect"));

        }

        private Double getInfernalDamage(Hero caster) {
	        ItemStack item = caster.getPlayer().getItemInHand();
	        if (SkillConfigManager.getUseSetting(caster, this.skill, "weapons", Util.swords).contains(item.getType().name())){
		        return caster.getHeroClass().getItemDamage(item.getType()).getScaled(caster);
	        }
		    return SkillConfigManager.getUseSetting(caster, skill, "min-damage", 5D, false);

        }
    }
}
