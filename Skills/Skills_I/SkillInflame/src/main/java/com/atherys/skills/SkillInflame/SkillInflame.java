package com.atherys.skills.SkillInflame;

import com.atherys.effects.MightEffect;
import com.atherys.effects.ZealEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillInflame extends PassiveSkill {

    public SkillInflame(Heroes plugin) {
        super(plugin, "Inflame");
        setDescription("Your melee strikes deal magic damage, ignore armor, and grants health. Double damage to mobs.");
        setIdentifiers("skill Inflame");
        setTypes(SkillType.ABILITY_PROPERTY_FIRE, SkillType.DAMAGING);
        Bukkit.getPluginManager().registerEvents(new SkillDamageListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("health-per-attack", 1);
        node.set(SkillSetting.COOLDOWN.node(), 500);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class SkillDamageListener implements Listener {
        private Skill skill;

        public SkillDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.getDamage() == 0 || event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK || !(event.getDamager() instanceof Hero) || !(event.getEntity() instanceof LivingEntity)) {
                return;
            }
            Hero hero = (Hero)event.getDamager();
            if (!hero.hasEffect("Inflame")) {
                return;
            }
            if (damageCheck(hero.getPlayer(), (LivingEntity)event.getEntity())) {
                Player player = hero.getPlayer();
                double damage = event.getDamage();
                if((event.getEntity()) instanceof Monster) {
                    damage = 2*damage;
                }
                if (hero.hasEffect("Zeal")) {
	                double mult = ((ZealEffect) hero.getEffect("Zeal")).getMultiplier();
                    damage = damage * (1 + mult);
                }
                if(hero.hasEffect("Might")){
                    MightEffect mightEffect = (MightEffect)hero.getEffect("Might");
                    damage = damage + mightEffect.getDamage();
                }
                damageEntity((LivingEntity) event.getEntity(), player, damage, EntityDamageEvent.DamageCause.MAGIC, true);
                event.setDamage(1D);
                player.playSound(event.getEntity().getLocation(), Sound.ENTITY_BLAZE_HURT, 1, 1);

                if(event.getEntity() instanceof Player) {
                    if (hero.getCooldown("Lifesteal") == null || hero.getCooldown("Lifesteal") <= System.currentTimeMillis()) {
                        double health = SkillConfigManager.getUseSetting(hero, skill, "health-per-attack", 1, false);
                        health = health > 0 ? health : 0;
                        long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN.node(), 500, false);
                        cooldown = cooldown > 0 ? cooldown : 0;
                        hero.setCooldown("Lifesteal", cooldown + System.currentTimeMillis());
                        if (player.getHealth() + health >= player.getMaxHealth()) {
                            player.setHealth(player.getMaxHealth());
                        } else {
                            player.setHealth(health + player.getHealth());
                        }
                    }
                }

            }
        }
    }
}