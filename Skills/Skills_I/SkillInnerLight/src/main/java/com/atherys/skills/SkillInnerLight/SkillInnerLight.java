package com.atherys.skills.SkillInnerLight;

import com.atherys.effects.BulwarkEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;

public class SkillInnerLight extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillInnerLight(Heroes plugin) {
        super(plugin, "InnerLight");
        setDescription("You form a shield that absorbs $1 damage before dissipating.");
        setUsage("/skill InnerLight");
        setArgumentRange(0, 0);
        setIdentifiers("skill InnerLight");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        int size = SkillConfigManager.getUseSetting(hero, this, "block-max", 100, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", size + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("block-max", 100);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired.");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        double size = SkillConfigManager.getUseSetting(hero, this, "block-max", 100, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        hero.addEffect(new BulwarkEffect(this, duration, size,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}