package com.atherys.skills.SkillInspire;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainManaEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillInspire extends ActiveSkill {
    public SkillInspire(Heroes plugin) {
        super(plugin, "Inspire");
        setDescription("Increases mana regeneration for you and your party for $2s.");
        setArgumentRange(0, 0);
        setUsage("/skill Inspire");
        setIdentifiers("skill Inspire");
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new ManaRegainListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.AMOUNT.name(), 1.2D);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill% on %target%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% on %target% expired!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        return getDescription().replace("$2", d / 1000L + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 1.2, false);
        InspireEffect i = new InspireEffect(this, duration, amount,hero);
        if (!hero.hasParty()) {
            hero.addEffect(i);
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        int radius = SkillConfigManager.getUseSetting(hero, this, "radius", 10, true);
        for (Hero phero : hero.getParty().getMembers()) {
            if (hero.getPlayer().getWorld().equals(phero.getPlayer().getWorld()) && hero.getPlayer().getLocation().distanceSquared(phero.getPlayer().getLocation()) < radius * radius) {
                phero.addEffect(i);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class InspireEffect extends ExpirableEffect {
        private final double amount;

        public InspireEffect(Skill skill, long duration, double amount,Hero caster) {
            super(skill, "InspireEffect",caster.getPlayer(), duration);
            this.amount = amount;
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.MAGIC);
        }

        public double getAmount() {
            return amount;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Your mana is now regenerating faster.");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Increased mana regen expired.");
        }
    }

    public class ManaRegainListener implements Listener {
        @EventHandler
        public void onManaRegen(HeroRegainManaEvent event) {
            if (event.isCancelled()) {
                return;
            }
            Hero h = event.getHero();
            if (!h.hasEffect("InspireEffect")) {
                return;
            }
            InspireEffect i = (InspireEffect) h.getEffect("InspireEffect");
            event.setDelta((int) (event.getDelta() * i.getAmount()));
        }
    }
}
