package me.apteryx.skillintervene;

import com.atherys.effects.InvulnEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import me.apteryx.skillintervene.utils.TimerUtil;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

;

/**
 * @author apteryx
 * @time 8:46 PM
 * @since 10/18/2016
 */
public class Intervene extends TargettedSkill {

    private TimerUtil timer, particleTimer;

    public Intervene(Heroes plugin) {
        super(plugin, "Intervene");
        this.setUsage("/skill intervene");
        this.setArgumentRange(0, 0);
        this.setIdentifiers("skill intervene");
        this.setDescription("Teleports to a nearby ally within %range% blocks and gives them invulnerability for %invuln-time% seconds.");
        this.setTypes(SkillType.TELEPORTING, SkillType.BUFFING);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity livingEntity, String[] strings) {
        if (livingEntity instanceof Player && livingEntity != hero.getPlayer()) {
            //hero.getPlayer().sendMessage(livingEntity.getName());
            if (hero.getParty() != null) {
                //hero.getPlayer().sendMessage("Party: "+(hero.getParty() != null));
                if (hero.getParty().isPartyMember((Player) livingEntity)) {
                    //hero.getPlayer().sendMessage("Hero is in party.");
                    this.broadcastExecuteText(hero, livingEntity);
                    hero.getPlayer().teleport(livingEntity.getLocation());
                    hero.getPlayer().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_PLAYER_ATTACK_SWEEP, 4.0F, 1.0F);
                    //hero.getPlayer().sendMessage("\2474\247oWoosh.");
                    handleEffects(this.plugin.getCharacterManager().getHero((Player) livingEntity), this);
                    return SkillResult.NORMAL;
                }
            }


        }
        return SkillResult.FAIL;
    }

    @Override
    public String getDescription(Hero hero) {
        return super.getDescription().replace("%range%", SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 30.0, false)+"").replace("%invuln-time%", (SkillConfigManager.getUseSetting(hero, this, "invuln-time", 2500, false) / 1000)+"");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), 30.0);
        node.set("invuln-time", 2500);
        node.set(SkillSetting.COOLDOWN.node(), 10000);
        return node;
    }

    private void handleEffects(Hero hero, Intervene intervene) {
        hero.addEffect(new InvulnEffect(this, "Intervene", (long) SkillConfigManager.getUseSetting(hero, this, "invuln-time", 2500, false), hero));
        new Thread() {
            List<Location> points;
            @Override
            public void run() {
                timer = new TimerUtil();
                particleTimer = new TimerUtil();
                while(!timer.isComplete((SkillConfigManager.getUseSetting(hero, intervene, "invuln-time", 2500, false)))) {
                    if (particleTimer.isComplete(250)) {
                        points = circle(hero.getPlayer().getLocation(), 5, 0.5);
                        for (Location location : points) {
                            hero.getPlayer().getWorld().spigot().playEffect(location, Effect.HAPPY_VILLAGER);
                            hero.getPlayer().getWorld().spigot().playEffect(location.add(0.0, 0.5, 0.0), Effect.HAPPY_VILLAGER);
                            hero.getPlayer().getWorld().spigot().playEffect(location.add(0.0, 0.8, 0.0), Effect.HAPPY_VILLAGER);

                        }
                        particleTimer.reset();
                    }

                }
                timer.reset();

            }
        }.start();
    }

    private ArrayList<Location> circle(final org.bukkit.Location centerPoint, final int particleAmount, final double circleRadius) {
        final World world = centerPoint.getWorld();
        final double increment = 6.283185307179586 / particleAmount;
        final ArrayList<Location> locations = new ArrayList<>();
        for (int i = 0; i < particleAmount; ++i) {
            final double angle = i * increment;
            final double x = centerPoint.getX() + circleRadius * Math.cos(angle);
            final double z = centerPoint.getZ() + circleRadius * Math.sin(angle);
            locations.add(new Location(world, x, centerPoint.getY(), z));
        }
        return locations;
    }




}
