package me.apteryx.skillintervene.utils;

/**
 * @author apteryx
 * @time 10:12 AM
 * @since 10/19/2016
 */
public class TimerUtil {

    private final long startTime;
    private long resetMS = -1L;

    public TimerUtil() {
        resetMS = now();
        startTime = resetMS;
    }

    public long now() {
        return (long) (System.nanoTime() / 1E6);
    }

    public void addReset(long addedReset) {
        resetMS += addedReset;
    }

    public boolean isComplete(int milliseconds) {
        long d = getElapsedTime();
        return d > milliseconds;
    }

    public short convertToMS(float perSecond) {
        return (short) (int) (1000.0F / perSecond);
    }

    public void reset() {
        resetMS = now();
    }

    public long getElapsedTime() {
        return now() - resetMS;
    }

}
