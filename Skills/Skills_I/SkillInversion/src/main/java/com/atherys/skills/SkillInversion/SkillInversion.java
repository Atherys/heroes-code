package com.atherys.skills.SkillInversion;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillInversion extends TargettedSkill {
    public SkillInversion(Heroes plugin) {
        super(plugin, "Inversion");
        setDescription("You deal magic damage equal to $1% of the mana the target is missing.");
        setUsage("/skill inversion");
        setArgumentRange(0, 0);
        setIdentifiers("skill inversion");
        setTypes(SkillType.MANA_INCREASING, SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        double damageMod = SkillConfigManager.getUseSetting(hero, this, "damage-modifier", 1.0D, false);
        damageMod += SkillConfigManager.getUseSetting(hero, this, "damage-modifier-increase", 0.0D, false) * hero.getSkillLevel(this);
        String description = getDescription().replace("$1", (int) (damageMod * 100.0D) + "");
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE, 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA, 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE, 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this);
        if (healthCost > 0) {
            description = description + " HP:" + healthCost;
        }
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE, 0, false) * hero.getSkillLevel(this);
        if (staminaCost > 0) {
            description = description + " FP:" + staminaCost;
        }
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description = description + " W:" + delay + "s";
        }
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description = description + " XP:" + exp;
        }
        return description;
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("max-damage", Integer.valueOf(0));
        node.set("damage-modifier", Integer.valueOf(1));
        node.set("damage-modifier-increase", Integer.valueOf(0));
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        Player player = hero.getPlayer();
	    Player targetplayer = (Player) target;
	    if (targetplayer.equals(hero.getPlayer())) {
		    return SkillResult.INVALID_TARGET_NO_MSG;
	    }
	    Hero enemy = this.plugin.getCharacterManager().getHero((Player) target);
	    if (hero.hasParty()){
		    if (hero.getParty().getMembers().contains(enemy)){
			    return SkillResult.INVALID_TARGET_NO_MSG;
		    }
	    }

        int maxDamage = SkillConfigManager.getUseSetting(hero, this, "max-damage", 0, false);
        double damageMod = SkillConfigManager.getUseSetting(hero, this, "damage-modifier", 1.0D, false) + SkillConfigManager.getUseSetting(hero, this, "damage-modifier-increase", 0.0D, false) * hero.getSkillLevel(this);
        double damage = ((enemy.getMaxMana() - enemy.getMana()) * damageMod);
        if ((maxDamage != 0) && (damage > maxDamage)) {
            damage = maxDamage;
        }
        addSpellTarget(target, hero);
        damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
}
