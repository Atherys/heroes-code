package com.atherys.skills.SkillInvoke;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillInvoke extends ActiveSkill {
    private String applyText;

    public SkillInvoke(Heroes plugin) {
        super(plugin, "Invoke");
        setDescription("Heals yourself or a nearby ally with the lowest health.");
        setUsage("/skill invoke");
        setArgumentRange(0, 0);
        setIdentifiers("skill invoke");
        setTypes(SkillType.HEALING, SkillType.SILENCEABLE);
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill% on %target%.").replace("%hero%", "$1").replace("%skill%", "$2").replace("%target%", "$3");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        double hp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH, 10, false);
        Hero targetHero = hero;
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 15, false);
        if (hero.hasParty()) {
            for (Hero phero : hero.getParty().getMembers()) {
                if (player.getWorld().equals(phero.getPlayer().getWorld()) && (player.hasLineOfSight(phero.getPlayer())) && ((((phero.getPlayer()).getLocation()).distanceSquared(player.getLocation())) <= radius * radius) && (targetHero.getPlayer().getHealth() > phero.getPlayer().getHealth())) {
                    targetHero = phero;
                }
            }
        }
        boolean healed = false;
        if (hero.getPlayer().getHealth() < hero.getPlayer().getMaxHealth()) {
            HeroRegainHealthEvent hrheh = new HeroRegainHealthEvent(hero, hp, this, hero);
            this.plugin.getServer().getPluginManager().callEvent(hrheh);
            if (!hrheh.isCancelled()) {
                hero.heal(hrheh.getDelta());
                healed = true;
            }
        }
        if (!targetHero.equals(hero)) {
            HeroRegainHealthEvent hrhet = new HeroRegainHealthEvent(targetHero, hp, this, hero);
            this.plugin.getServer().getPluginManager().callEvent(hrhet);
            if (!hrhet.isCancelled()) {
                targetHero.heal(hrhet.getDelta());
                broadcast(hero.getPlayer().getLocation(), applyText, hero.getName(), "Invoke", targetHero.getName());
                return SkillResult.NORMAL;
            }
        }
        if (!healed) {
            Messaging.send(player, "Unable to heal you or a nearby ally at this time.");
            return SkillResult.CANCELLED;
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH, 10, false);
        return getDescription().replace("$1", amount + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.HEALTH.node(), Integer.valueOf(10));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(5));
        return node;
    }

}