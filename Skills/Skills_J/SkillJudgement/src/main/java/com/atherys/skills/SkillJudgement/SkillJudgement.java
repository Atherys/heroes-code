package com.atherys.skills.SkillJudgement;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillJudgement extends PassiveSkill {

    public SkillJudgement(Heroes plugin) {
        super(plugin, "Judgement");
        setDescription("Passive\nYou return a percentage of melee damage to the attackers.");
        setTypes(SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillJudgementListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("return-percentage", 0.1);
        node.set(SkillSetting.MANA.node(), 1);
        return node;
    }

    public class SkillJudgementListener implements Listener {
        private final Skill skill;

        public SkillJudgementListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if ((!(event.getEntity() instanceof Player)) || (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("Judgement")) {
                int mana = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.MANA, 1, false);
                if (hero.getMana() - mana >= 0) {
                    CharacterTemplate damager = event.getDamager();
                    double returnPercent = SkillConfigManager.getUseSetting(hero, skill, "return-percentage", 0.1, false);
                    damageEntity(damager.getEntity(), hero.getPlayer(), event.getDamage() * returnPercent, EntityDamageEvent.DamageCause.MAGIC, false);
                    hero.setMana(hero.getMana() - mana);
                }
            }
        }
    }
}