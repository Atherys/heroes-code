package com.atherys.skills.SkillJustify;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillJustify extends ActiveSkill {
    public SkillJustify(Heroes plugin) {
        super(plugin, "Justify");
        setDescription("Active\n Your next melee attack will deal extra damage and gives you some mana.");
        setUsage("/skill Justify");
        setArgumentRange(0, 0);
        setIdentifiers("skill Justify");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE, SkillType.DAMAGING);
        Bukkit.getPluginManager().registerEvents(new HeroDamageListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 5000);
        node.set(SkillSetting.DAMAGE.node(), 15);
        node.set("mana-gain", 20);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        hero.addEffect(new JustifyEffect(this, duration,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    private static class JustifyEffect extends ExpirableEffect {

        public JustifyEffect(Skill skill, long duration,Hero caster) {
            super(skill, "Justify",caster.getPlayer(), duration);
            types.add(EffectType.IMBUE);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.MAGIC);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                Messaging.send(hero.getPlayer(), "Justify expired");
            }
        }
    }

    public class HeroDamageListener implements Listener {
        private final Skill skill;

        public HeroDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero) || !(event.getEntity() instanceof LivingEntity) || (event.getDamage() == 0) || (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero hero = (Hero) event.getDamager();
            if (event.getDamager().hasEffect("Justify")) {
                LivingEntity livingEntity = (LivingEntity) event.getEntity();
                double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 15, false);
                int mana = SkillConfigManager.getUseSetting(hero, skill, "mana-gain", 20, false);
                damageEntity(livingEntity, hero.getPlayer(), damage, DamageCause.CUSTOM, false);
                if (hero.getMana() + mana > hero.getMaxMana()) {
                    hero.setMana(hero.getMaxMana());
                } else {
                    hero.setMana(hero.getMana() + mana);
                }
                hero.removeEffect(hero.getEffect("Justify"));
            }
        }


    }
}
