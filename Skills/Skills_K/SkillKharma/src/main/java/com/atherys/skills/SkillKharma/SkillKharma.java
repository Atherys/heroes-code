package com.atherys.skills.SkillKharma;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillKharma extends TargettedSkill {
    public SkillKharma(Heroes plugin) {
        super(plugin, "Kharma");
        setDescription("Heals target Ally for $1 or Deals $1 damage to enemy");
        setUsage("/skill Kharma");
        setArgumentRange(0, 0);
        setIdentifiers("skill kharma");
        setTypes(SkillType.SILENCEABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(2D));
        return node;
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 10, false);
        // If party member
        if (hero.hasParty() && target instanceof Player && hero.getParty().isPartyMember(((Player) target))) {
            Hero targetHero = plugin.getCharacterManager().getHero(((Player) target));
            if (target.getHealth() >= target.getMaxHealth()) {
                Messaging.send(player, "Target is already fully healed.");
                return SkillResult.INVALID_TARGET_NO_MSG;
            }
            HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(targetHero, amount, this, hero);
            plugin.getServer().getPluginManager().callEvent(hrhEvent);
            if (hrhEvent.isCancelled()) {
                Messaging.send(player, "Unable to heal the target at this time!");
                return SkillResult.CANCELLED;
            }
            targetHero.heal(hrhEvent.getDelta());
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
        // else Enemy
        else {
            if (!damageCheck(player, target)) {
                Messaging.send(player, "Sorry, You can't damage that target!");
                return SkillResult.INVALID_TARGET_NO_MSG;
            }
            addSpellTarget(target, hero);
            damageEntity(target, player, amount, EntityDamageEvent.DamageCause.MAGIC);
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
    }
}