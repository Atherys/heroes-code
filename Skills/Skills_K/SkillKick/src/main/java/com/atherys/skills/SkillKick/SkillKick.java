package com.atherys.skills.SkillKick;

import com.atherys.effects.SilenceEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

public class SkillKick extends TargettedSkill {

    public SkillKick(Heroes plugin) {
        super(plugin, "Kick");
        setDescription("Kicks your target");
        setUsage("/skill kick <target>");
        setArgumentRange(0, 0);
        setIdentifiers("skill kick");
        setTypes(SkillType.DAMAGING, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.DAMAGING);
    }

    public String getDescription(Hero hero) {
        StringBuilder descr = new StringBuilder(getDescription());
        double silenceSec = SkillConfigManager.getUseSetting(hero, this, "silence-duration", 3000, false) / 1000.0D;
        if (silenceSec > 0.0D) {
            descr.append(" and silences it for ");
            descr.append(Util.formatDouble(silenceSec));
            descr.append("s");
        }
        double cdSec = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 15000, false) / 1000.0D;
        if (cdSec > 0.0D) {
            descr.append(" CD:");
            descr.append(Util.formatDouble(cdSec));
            descr.append("s");
        }
        return descr.toString();
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection defaultConfig = super.getDefaultConfig();
        defaultConfig.set(SkillSetting.COOLDOWN.node(), Integer.valueOf(15000));
        defaultConfig.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(5));
        defaultConfig.set(SkillSetting.DAMAGE.node(), Integer.valueOf(1));
        defaultConfig.set("silence-duration", Integer.valueOf(3000));
        return defaultConfig;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {

        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 1, false);

        if ((target instanceof Player)) {
            Player targetPlayer = (Player) target;
            if (target.equals(hero.getPlayer())) {
                return SkillResult.INVALID_TARGET_NO_MSG;
            }else {
	            Hero enemy = this.plugin.getCharacterManager().getHero((Player) target);
	            if (hero.hasParty()){
		            if (hero.getParty().getMembers().contains(enemy)){
			            return SkillResult.INVALID_TARGET_NO_MSG;
		            }
	            }
                damageEntity(target, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.ENTITY_ATTACK, false);
                target.setVelocity(new Vector(Math.random() * 0.4D - 0.2D, 0.8D, Math.random() * 0.4D - 0.2D));
                Hero targetHero = this.plugin.getCharacterManager().getHero(targetPlayer);
                int silenceDuration = SkillConfigManager.getUseSetting(hero, this, "silence-duration", 3000, false);
                if (silenceDuration > 0) {
                    targetHero.addEffect(new SilenceEffect(this, silenceDuration, true, hero));
                    Lib.cancelDelayedSkill(targetHero);
                }
            }
        }
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
}