package com.atherys.skills.SkillLavaStride;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Set;

import static org.bukkit.Material.getMaterial;

public class SkillLavaStride extends ActiveSkill {
    public SkillLavaStride(Heroes plugin) {
        super(plugin, "LavaStride");
        setDescription("Lets you move faster on certain terrain");
        setUsage("/skill lavastride");
        setArgumentRange(0, 0);
        setIdentifiers("skill lavastride");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING, SkillType.MOVEMENT_INCREASING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 600000);
        node.set(SkillSetting.MAX_DISTANCE.node(), 5);
        node.set("terrain-type", "STATIONARY_LAVA");
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 600000, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE.node(), 5, false);
        String block = SkillConfigManager.getUseSetting(hero, this, "terrain-type", "STATIONARY_LAVA");
        Material mat = getMaterial(block);
        if (mat == null) {
            hero.getPlayer().sendMessage(ChatColor.RED + "SkillLavaStride has invalid terrain-type.");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        hero.addEffect(new LavaStrideEffect(this, 500, duration, distance, mat,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription();
    }

    public class LavaStrideEffect extends PeriodicExpirableEffect {
        private final int distance;
        private final Material mat;

        public LavaStrideEffect(Skill skill, long period, long duration, int distance, Material mat,Hero caster) {
            super(skill, "LavaStrideEffect",caster.getPlayer(), period, duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.FIRE);
            this.distance = distance;
            this.mat = mat;
            addMobEffect(12, (int)duration/50, 0, false);
        }

        @Override
        public void tickMonster(Monster mnstr) {
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            if (hero.isInCombat()) {
                this.expire();
            }
            Block b = player.getLocation().getBlock();
            if (b.getType() == mat) {
                Location l = player.getTargetBlock((Set<Material>) null, distance).getLocation();
                Location pL = player.getLocation().getBlock().getRelative(BlockFace.DOWN).getLocation();
                Vector vc = new Vector(l.getX() - pL.getX(), 0.5D, l.getZ() - pL.getZ());
                vc.multiply(0.2);
                player.setVelocity(vc);
            }
        }
    }
}
