package com.atherys.skills.SkillLeech;

import com.atherys.effects.BleedPeriodicDamageEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillLeech extends TargettedSkill {
    public SkillLeech(Heroes plugin) {
        super(plugin, "Leech");
        setDescription("Sucks $1 health from your target over $2 seconds. The caster heals $3 health over $4 seconds.");
        setUsage("/skill leech <target>");
        setArgumentRange(0, 0);
        setIdentifiers("skill leech");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage", 10);
        node.set("damage-period", 1000);
        node.set("damage-duration", 10000);
        node.set("heal", 10);
        node.set("heal-period", 1000);
        node.set("heal-duration", 10000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int d = SkillConfigManager.getUseSetting(hero, this, "damage", 10, false);
        long dd = SkillConfigManager.getUseSetting(hero, this, "damage-duration", 10, false);
        int h = SkillConfigManager.getUseSetting(hero, this, "heal", 10, false);
        long hd = SkillConfigManager.getUseSetting(hero, this, "heal-duration", 10, false);
        return getDescription().replace("$1", d + "").replace("$2", dd / 1000 + "").replace("$3", "" + h).replace("$4", hd / 1000 + "");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] strings) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }
        double d = SkillConfigManager.getUseSetting(hero, this, "damage", 10, false);
        long dd = SkillConfigManager.getUseSetting(hero, this, "damage-duration", 10, false);
        long dp = SkillConfigManager.getUseSetting(hero, this, "damage-period", 10, false);
        int h = SkillConfigManager.getUseSetting(hero, this, "heal", 10, false);
        long hp = SkillConfigManager.getUseSetting(hero, this, "heal-period", 1000, false);
        long hd = SkillConfigManager.getUseSetting(hero, this, "heal-duration", 10000, false);
        hero.addEffect(new healEffect(this, hp, hd, h, hero.getPlayer()));
        plugin.getCharacterManager().getHero((Player) target).addEffect(new damageEffect(this, dp, dd, d, hero.getPlayer()));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class damageEffect extends BleedPeriodicDamageEffect {
        public damageEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "DamageBloodBoil",applier, period, duration, tickDamage);
            types.add(EffectType.HARMFUL);
            //types.add(EffectType.Draining);
            types.add(EffectType.BLEED);
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.MAGIC);
        }
    }

    public class healEffect extends PeriodicHealEffect {
        public healEffect(Skill skill, long period, long duration, double amount, Player applier) {
            super(skill, "HealBloodBoil",applier, period, duration, amount );
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.HEALING);
            types.add(EffectType.MAGIC);
        }
    }
}