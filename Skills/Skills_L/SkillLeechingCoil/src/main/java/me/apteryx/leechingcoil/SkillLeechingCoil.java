package me.apteryx.leechingcoil;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import me.apteryx.leechingcoil.event.EventWitherExplosion;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.WitherSkull;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author apteryx
 * @time 3:22 PM
 * @since 10/17/2016
 */
public class SkillLeechingCoil extends ActiveSkill {

    private double radius;
    private double damage;
    private double lifeGain;
    private static SkillLeechingCoil skillLeechingCoil;


    public SkillLeechingCoil(Heroes plugin) {
        super(plugin, "LeechingCoil");
        this.setUsage("/skill leechingcoil");
        this.setArgumentRange(0, 0);
        this.setIdentifiers("skill leechingcoil");
        this.setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
        this.setDescription("Sacrifice %health-cost% health to launch a leeching coil that deals %damage% magic damage to enemies within %radius% blocks of impact and refunds %life-gain% health if an enemy is hit");
        Bukkit.getPluginManager().registerEvents(new EventWitherExplosion(), this.plugin);
        if (skillLeechingCoil == null) {
            skillLeechingCoil = this;
        }
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 2.0, false);
        radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 4.0, false);
        lifeGain = SkillConfigManager.getUseSetting(hero, this, "life-gain", 2.0, false);
        Projectile witherSkull = hero.getPlayer().launchProjectile(WitherSkull.class);
        witherSkull.setMetadata("LeechingCoil", new FixedMetadataValue(this.plugin, hero.getPlayer()));
        this.broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 3.0, false);
        return this.getDescription().replace("%health-cost%", healthCost+"").replace("%damage%", SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 2.0, false)+"").replace("%radius%", SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 4.0, false)+"").replace("%life-gain%", SkillConfigManager.getUseSetting(hero, this, "life-gain", 2.0, false)+"");
    }


    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection section = super.getDefaultConfig();
        section.set(SkillSetting.HEALTH_COST.node(), 3.0);
        section.set(SkillSetting.DAMAGE.node(), 2.0);
        section.set(SkillSetting.RADIUS.node(), 4.0);
        section.set("life-gain", 2.0);
        return section;
    }

    public double getRadius() {
        return radius;
    }

    public double getDamage() {
        return damage;
    }

    public double getLifeGain() {
        return lifeGain;
    }

    public static SkillLeechingCoil getInstance() {
        return skillLeechingCoil;
    }

}
