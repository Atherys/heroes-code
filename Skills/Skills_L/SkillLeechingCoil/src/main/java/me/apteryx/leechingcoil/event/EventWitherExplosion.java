package me.apteryx.leechingcoil.event;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import me.apteryx.leechingcoil.SkillLeechingCoil;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * @author apteryx
 * @time 1:37 PM
 * @since 11/18/2016
 */
public class EventWitherExplosion implements Listener {

    private boolean healed = false;


    @EventHandler
    public void onWitherExplosion(EntityExplodeEvent event) {
        if (event.getEntity().hasMetadata("LeechingCoil")) {
            //Bukkit.getLogger().info("Has data.");
            event.blockList().clear();
            event.setCancelled(true);
            for(Entity entity : event.getEntity().getNearbyEntities(SkillLeechingCoil.getInstance().getRadius(), SkillLeechingCoil.getInstance().getRadius(), SkillLeechingCoil.getInstance().getRadius())) {
                if (entity instanceof LivingEntity) {
                    //Bukkit.getLogger().info("Entity: " + entity.getName());
                    if (entity != event.getEntity().getMetadata("LeechingCoil").get(0).value()) {
                        //Bukkit.getLogger().info("Player:" + ((Player)event.getEntity().getMetadata("LeechingCoil").get(0).value()).getName());
                        Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity().getMetadata("LeechingCoil").get(0).value());
                        //Bukkit.getLogger().info("Hero: " + hero.getName());
                        if (entity instanceof Player) {
                            if (hero.getParty() != null) {
                                if (hero.getParty().isPartyMember((Player) entity)) {
                                    continue;
                                }
                            }
                        }
                        if (!healed) {
                            healed = true;
                            hero.getPlayer().setHealth(hero.getPlayer().getMaxHealth() < (hero.getPlayer().getHealth() + SkillLeechingCoil.getInstance().getLifeGain()) ? hero.getPlayer().getMaxHealth() : (hero.getPlayer().getHealth() + SkillLeechingCoil.getInstance().getLifeGain()));
                        }
                        SkillLeechingCoil.getInstance().damageEntity((LivingEntity) entity, hero.getPlayer(), SkillLeechingCoil.getInstance().getDamage(), EntityDamageEvent.DamageCause.MAGIC);
                        //Bukkit.getLogger().info("Damage: " + SkillLeechingCoil.getInstance().getDamage());
                    }
                }
            }
        }
        healed = false;
    }

}
