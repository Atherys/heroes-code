package com.atherys.skills.SkillLifeLink;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class SkillLifeLink extends PassiveSkill {

    public SkillLifeLink(Heroes plugin) {
        super(plugin, "LifeLink");
        setDescription("Passive\nWhenever a nearby ally takes lethal damage, prevent that damage and set their health to 20% instead and also give your ally a temporary debuff that kills them whenever you die.");
        setUsage("/skill LifeLink");
        setArgumentRange(0, 0);
        setIdentifiers("skill LifeLink");
        Bukkit.getServer().getPluginManager().registerEvents(new LifeLinkListener(this), plugin);
        setTypes(SkillType.HEALING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("health-percent", 0.2);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set("debuff-duration", 120000);
        return node;
    }

    public class LifeLinkDebuffEffect extends ExpirableEffect {
        private Hero lifeLinked;

        public LifeLinkDebuffEffect(Skill skill, long duration, Hero lifeLinked,Hero caster ) {
            super(skill, "LifeLinkDebuff",caster.getPlayer(), duration);
            this.lifeLinked = lifeLinked;
            this.types.add(EffectType.UNBREAKABLE);
        }

        public Hero getLifeLinked() {
            return lifeLinked;
        }
    }

    public class LifeLinkListener implements Listener {
        private final Skill skill;

        public LifeLinkListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
                return;
            }
            Player player = (Player) event.getEntity();
            double currentHealth = player.getHealth();
            if (currentHealth <= (event.getDamage() + player.getMaxHealth()*0.2)) {
                Hero hero = plugin.getCharacterManager().getHero(player);
                if (hero.hasParty() && hero.getParty().getMembers().size() > 1) {
                    for (Hero pHero : hero.getParty().getMembers()) {
                        if (!hero.equals(pHero) && pHero.hasEffect("LifeLink") && (pHero.getCooldown("LifeLink") == null || pHero.getCooldown("LifeLink") <= System.currentTimeMillis())) {
                            int radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 5, false);
                            Player pPlayer = pHero.getPlayer();
                            if (pPlayer.getWorld().equals(player.getWorld()) && pPlayer.getLocation().distanceSquared(player.getLocation()) <= radius * radius) {
                                double regainPercent = SkillConfigManager.getUseSetting(hero, skill, "health-percent", 0.2, false);
                                double healthRegain = (player.getMaxHealth() * regainPercent);
                                event.setDamage(0);
                                event.setCancelled(true);
                                hero.heal(healthRegain);
                                broadcast(player.getLocation(), "$1's LifeLink got activated on $2!", pPlayer.getDisplayName(), player.getDisplayName());
                                long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN, 120000, false);
                                long duration = SkillConfigManager.getUseSetting(hero, skill, "debuff-duration", 120000, false);
                                pHero.setCooldown("LifeLink", cooldown + System.currentTimeMillis());
                                LifeLinkDebuffEffect lifeLinkDebuffEffect = new LifeLinkDebuffEffect(skill, duration, pHero,hero);
                                pHero.addEffect(lifeLinkDebuffEffect);
                            }
                        }
                    }
                }
            }
        }

        @EventHandler
        public void onPlayerDeath(PlayerDeathEvent event) {
            Hero hero = plugin.getCharacterManager().getHero(event.getEntity());
            if (hero.hasEffect("LifeLinkDebuff")) {
                Hero lifeLinked = ((LifeLinkDebuffEffect) hero.getEffect("LifeLinkDebuff")).getLifeLinked();
                if (!lifeLinked.hasEffect("LifeLinkDebuff")) {
                    lifeLinked.getPlayer().setHealth(0);
                    broadcast(lifeLinked.getPlayer().getLocation(), "$1 died because their life was linked with $2!", lifeLinked.getPlayer().getDisplayName(), hero.getPlayer().getDisplayName());
                }
            }
        }
    }
}
