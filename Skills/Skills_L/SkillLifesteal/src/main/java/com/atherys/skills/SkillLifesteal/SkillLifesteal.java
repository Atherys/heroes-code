package com.atherys.skills.SkillLifesteal;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillLifesteal extends PassiveSkill {
	public SkillLifesteal(Heroes plugin) {
		super(plugin, "Lifesteal");
		setDescription("Passive health gain on non-skill damage");
		setTypes(SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.BUFFING);
		Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
		//registerEvent(Type.CUSTOM_EVENT, new SkillHeroListener(this), Priority.Normal);
	}

	@Override
	public String getDescription(Hero hero) {
		long cooldown = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 500, false) -
				(SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0.0, false) * hero.getSkillLevel(this))) / 1000;
		cooldown = cooldown > 0 ? cooldown : 0;
		int health = (int) (SkillConfigManager.getUseSetting(hero, this, "health-per-attack", 1, false) +
				(SkillConfigManager.getUseSetting(hero, this, "health-increase", 0.0, false) * hero.getSkillLevel(this)));
		health = health > 0 ? health : 0;
		String description = getDescription().replace("$1", cooldown + "").replace("$2", health + "");
		return description;
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set("health-per-attack", 1);
		node.set("health-increase", 0);
		node.set(SkillSetting.COOLDOWN.node(), 500);
		node.set(SkillSetting.COOLDOWN_REDUCE.node(), 0);
		node.set("exp-per-heal", 0);
		return node;
	}

	public class SkillHeroListener implements Listener {
		private Skill skill;

		public SkillHeroListener(Skill skill) {
			this.skill = skill;
		}

		@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
		public void onWeaponDamage(WeaponDamageEvent event) {
			if (event.getCause() != DamageCause.ENTITY_ATTACK || event.getDamage() == 0 || !(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Hero)) {
				return;
			}
			Hero hero = (Hero) event.getDamager();
			Player tPlayer = (Player) event.getEntity();
			//Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
			if (damageCheck(hero.getPlayer(), (LivingEntity) tPlayer)) {
				if (hero.hasEffect("Lifesteal"))
					if (hero.getCooldown("Lifesteal") == null || hero.getCooldown("Lifesteal") <= System.currentTimeMillis()) {
						Player player = hero.getPlayer();
						double health = SkillConfigManager.getUseSetting(hero, skill, "health-per-attack", 1, false);
						health = health > 0 ? health : 0;
						long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN.node(), 500, false);
						cooldown = cooldown > 0 ? cooldown : 0;
						hero.setCooldown("Lifesteal", cooldown + System.currentTimeMillis());
						if (player.getHealth() + health >= player.getMaxHealth()) {
							player.setHealth(player.getMaxHealth());
						} else {
							player.setHealth(health + player.getHealth());
						}
					}
			}
		}
	}
}
