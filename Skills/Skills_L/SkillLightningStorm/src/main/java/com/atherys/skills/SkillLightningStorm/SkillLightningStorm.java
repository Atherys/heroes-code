package com.atherys.skills.SkillLightningStorm;

import com.atherys.effects.SkillCastingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.ArrayList;
import java.util.List;

public class SkillLightningStorm extends ActiveSkill {

    public SkillLightningStorm(Heroes plugin) {
        super(plugin, "LightningStorm");
        setDescription("After channeling for a few seconds, you will stun and strike lightning on nearby enemies every second.");
        setUsage("/skill LightningStorm");
        setArgumentRange(0, 0);
        setIdentifiers("skill LightningStorm");
        setTypes(SkillType.ABILITY_PROPERTY_LIGHTNING, SkillType.SILENCEABLE, SkillType.DAMAGING);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(30));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(15));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(10000));
        node.set("warm-up", Long.valueOf(6000));
        node.set("stun-duration", 1000);
        return node;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        long warmup = SkillConfigManager.getUseSetting(hero, this, "warm-up", 6000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 15, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 30, false);
        long stunDuration = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 1000, false);
        hero.addEffect(new CastingLightningStormEffect(this, warmup, false, duration, period, radius, damage, stunDuration,hero));
        return SkillResult.NORMAL;
    }

    public class CastingLightningStormEffect extends SkillCastingEffect {
        private long duration;
        private long period;
        private int radius;
        private double damage;
        private long stunDuration;

        public CastingLightningStormEffect(Skill skill, long warmup, boolean slow, long duration, long period, int radius, double damage, long stunDuration, Hero caster) {
            super(skill, warmup, slow, true,caster);
            this.duration = duration;
            this.period = period;
            this.radius = radius;
            this.damage = damage;
            this.stunDuration = stunDuration;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                hero.addEffect(new LightningStormEffect(skill, duration, period, radius, damage, stunDuration,hero));
            }
        }
    }

    public class LightningStormEffect extends PeriodicExpirableEffect {
        private List<LivingEntity> affectedEnemies;
        private int radius;
        private double damage;
        private long stunDuration;

        public LightningStormEffect(Skill skill, long duration, long period, int radius, double damage, long stunDuration,Hero caster) {
            super(skill, "LightningStormEffect",caster.getPlayer(), period, duration);
            affectedEnemies = new ArrayList<>();
            this.radius = radius;
            this.damage = damage;
            this.stunDuration = stunDuration;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.LIGHTNING);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            LivingEntity target = null;
            for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                if (e instanceof Player || e instanceof org.bukkit.entity.Monster) {
                    LivingEntity le = (LivingEntity) e;
                    if (!affectedEnemies.contains(le)) {
                        if (damageCheck(player, le)) {
                            if (target == null || le.getLocation().distanceSquared(player.getLocation()) < target.getLocation().distanceSquared(player.getLocation())) {
                                target = le;
                            }
                        }
                    }
                }
            }
            if (target != null) {
                CharacterTemplate characterTemplate = plugin.getCharacterManager().getCharacter(target);
                double actualDamage = damage;
                if (target instanceof Player) {
                    if (characterTemplate.hasEffect("Grounded") && ((LivingEntity) target).isOnGround()) {
                        actualDamage *= (1 - SkillConfigManager.getUseSetting((Hero) characterTemplate, plugin.getSkillManager().getSkill("Grounded"), "damage-reduction", 0.5, false));
                    }
                    Lib.cancelDelayedSkill((Hero) characterTemplate);
                }
                target.getWorld().strikeLightningEffect(target.getLocation());
                addSpellTarget(target, hero);
                damageEntity(target, player, actualDamage, EntityDamageEvent.DamageCause.MAGIC);
                affectedEnemies.add(target);
                characterTemplate.addEffect(new StunEffect(skill, stunDuration,hero));
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            // N/A
        }
    }
}
