package com.atherys.skills.SkillLightningrod;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainManaEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.common.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class SkillLightningrod extends ActiveSkill {

    private HashMap<Player, Lightningrod> lightningrods = new HashMap<Player, Lightningrod>();

    public SkillLightningrod(Heroes plugin) {
        super(plugin, "Lightningrod");
        setDescription("You place a Lightningrod at your target location (within $1 meters). The lightningrod lasts $2 seconds and restores $3 mana per $4 second(s) to you as long as you stay within $5 meters of it. If an enemy comes within this radius, the lightningrod collapses, dealing $6 damage to them and stunning them for $7 seconds.");
        setUsage("/skill lightningrod");
        setArgumentRange(0, 0);
        setIdentifiers("skill lightningrod", "skill cannon");
        setTypes(SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        double range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 15, true);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, true);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, true);
        int manaRestore = SkillConfigManager.getUseSetting(hero, this, "mana-restore", 100, true);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 25, true) +
                (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 1, true) * hero.getSkillLevel(this));
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3.5, true);
        long stunTime = SkillConfigManager.getUseSetting(hero, this, "stun-time", 750, true);

        return getDescription().replace("$1", range + "")
                .replace("$2", String.valueOf(duration / 1000))
                .replace("$3", manaRestore + "")
                .replace("$4", String.valueOf(period / 1000))
                .replace("(s)", (period != 1000 ? "s" : ""))
                .replace("$5", radius + "")
                .replace("$6", damage + "")
                .replace("$7", String.valueOf(stunTime / 1000));
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();

        node.set("mana-restore", 100);
        node.set(SkillSetting.MAX_DISTANCE.node(), 15);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set(SkillSetting.DAMAGE.node(), 25);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 1);
        node.set(SkillSetting.RADIUS.node(), 3.5);
        node.set("stun-duration", 750);

        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        final Player player = hero.getPlayer();

        double range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 15, true);

        Block targetBlock = player.getTargetBlock((HashSet<Byte>) null, (int) range);
        if (targetBlock.getType() == Material.LONG_GRASS) targetBlock = targetBlock.getLocation().subtract(0, 1, 0).getBlock(); // fuck ur grass
        else if (!targetBlock.getType().isSolid()) {
            player.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Skill" + ChatColor.GRAY + "] You can't place a lightningrod there!");
            return SkillResult.FAIL;
        }
        Location base = targetBlock.getLocation().add(new Vector(0.5, 1, 0.5));
        // check to see if you can build at that location
        boolean isObstructed = false;
        ArrayList<Block> blocks = new ArrayList<Block>();
        for (double y = 0.0D; y <= 3.0D; y++) {
            for (double x = -1.0D; x <= 1.0D; x++) {
                for (double z = -1.0D; z <= 1.0D; z++) {
                    Location l = base.clone().add(new Vector(x, y, z));
                    blocks.add(l.getBlock());
                    if (l.getBlock().getType().isSolid() || l.getBlock().isLiquid()) isObstructed = true;
                }
            }
        }

        if (isObstructed) {
            player.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Skill" + ChatColor.GRAY + "] You can't place a lightningrod there!");
            return SkillResult.FAIL;
        }

        lightningrods.put(player, new Lightningrod(hero, this, base, blocks));
        base.getWorld().strikeLightningEffect(base);
        base.getWorld().playSound(base, Sound.ENTITY_LIGHTNING_THUNDER, 1.0F, 1.0F);
        base.getWorld().playSound(base, Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 1.0F);
        broadcast(player.getLocation(), ChatColor.GRAY + "[" + ChatColor.BLUE + "Skill" + ChatColor.GRAY + "] " + ChatColor.WHITE + hero.getName() + ChatColor.GRAY + " places a lightningrod!");

        return SkillResult.NORMAL;
    }

    public class Lightningrod implements Listener {
        private Hero hero;
        private Skill skill;
        private Location loc;

        private boolean deconstructed = false;

        private ArrayList<Block> componentBlocks;

        public Lightningrod(Hero h, Skill s, Location l, ArrayList<Block> blocks) {
            hero = h;
            skill = s;
            loc = l;
            componentBlocks = blocks;
            Bukkit.getPluginManager().registerEvents(this, plugin);

            construct();
        }

        @EventHandler
        public void blockBuilding(BlockPlaceEvent e) {
            if (componentBlocks.contains(e.getBlock())) e.setCancelled(true);
        }

        @EventHandler
        public void blockBreaking(BlockBreakEvent e) {
            if (componentBlocks.contains(e.getBlock())) e.setCancelled(true);
        }

        @EventHandler
        public void blockInteraction(PlayerInteractEvent e) {
            if (componentBlocks.contains(e.getClickedBlock())) e.setCancelled(true);
        }

        public void construct() {
            Location buildLoc = loc.clone();
            buildLoc.getBlock().setType(Material.IRON_BLOCK);
            buildLoc.add(0, 1, 0);
            buildLoc.getBlock().setType(Material.IRON_FENCE);
            buildLoc.add(0, 1, 0);
            buildLoc.getBlock().setType(Material.IRON_FENCE);
            buildLoc.add(0, 1, 0);
            buildLoc.getBlock().setType(Material.IRON_BLOCK);
            buildLoc.add(1, 0, 0);
            buildLoc.getBlock().setType(Material.IRON_FENCE);
            buildLoc.subtract(2, 0, 0);
            buildLoc.getBlock().setType(Material.IRON_FENCE);
            buildLoc.add(1, 0, 1);
            buildLoc.getBlock().setType(Material.IRON_FENCE);
            buildLoc.subtract(0, 0, 2);
            buildLoc.getBlock().setType(Material.IRON_FENCE);
            activate();
        }

        public void activate() {
            long duration = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 10000, true);
            long period = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.PERIOD, 1000, true);
            int manaRestore = SkillConfigManager.getUseSetting(hero, skill, "mana-restore", 100, true);
            double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 25, true) +
                    (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE_INCREASE, 1, true) * hero.getSkillLevel(skill));
            double radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 3.5, true);
            long stunTime = SkillConfigManager.getUseSetting(hero, skill, "stun-time", 750, true);

            int tickPeriod = (int) ((period / 1000) * 20);

            int maxTicks = (int) ((duration / period));

            new BukkitRunnable() {
                int ticks = 0;

                public void run() {
                    Snowball test = (Snowball) loc.getWorld().spawnEntity(loc, EntityType.SNOWBALL);
                    for (Player p : Bukkit.getServer().getOnlinePlayers()) test.remove();
                    for (Entity e : test.getNearbyEntities(radius, radius, radius)) {
                        if (!(e instanceof LivingEntity)) continue;
                        LivingEntity le = (LivingEntity) e;
                        if (le instanceof Player && plugin.getCharacterManager().getHero((Player) le).equals(hero)) {
                            if (hero.getMana() < hero.getMaxMana()) {
                                HeroRegainManaEvent heal = new HeroRegainManaEvent(hero, manaRestore, skill);
                                plugin.getServer().getPluginManager().callEvent(heal);
                                if (!heal.isCancelled()) {
                                    hero.setMana(heal.getDelta() + hero.getMana());
                                    le.getWorld().spigot().playEffect(le.getLocation().add(0, 0.5, 0), Effect.MAGIC_CRIT, 0, 0, 0.5F, 0.5F, 0.5F, 0, 20, 16);
                                }
                            }
                            continue;
                        }
                        if (!damageCheck(hero.getPlayer(), le)) continue;
                        if (!(le instanceof Player)) continue;
                        Player p = (Player) le;
                        Hero h = plugin.getCharacterManager().getHero(p);
                        addSpellTarget(p, hero);
                        damageEntity(p, hero.getPlayer(), damage, DamageCause.MAGIC, false);
                        h.addEffect(new StunEffect(skill, hero.getPlayer(), stunTime));
                        p.getWorld().spigot().playEffect(p.getLocation().add(0, 1.5, 0), Effect.CRIT, 0, 0, 1.0F, 1.0F, 1.0F, 0.0F, 45, 128);
                        p.getWorld().spigot().playEffect(p.getLocation().add(0, 1.5, 0), Effect.FIREWORKS_SPARK, 0, 0, 1.0F, 1.0F, 1.0F, 0.0F, 45, 128);
                        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1.0F, 2.0F);
                        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_FIREWORK_BLAST, 1.0F, 2.0F);
                        Location loc2 = p.getLocation().clone().add(0, 1, 0);
                        Location loc1 = loc.clone().add(0, 4, 0);
                        Vector dir = loc2.toVector().subtract(loc1.toVector()).divide(new Vector(10, 10, 10));
                        double dist = loc1.distance(loc2);
                        Location to = loc1.clone();
                        double traveled = 0.0D;
                        while (traveled < Math.abs(dist)) {
                            to.getWorld().spigot().playEffect(to, Effect.INSTANT_SPELL, 0, 0, 0.05F, 0.05F, 0.05F, 0.0F, 3, 128);
                            to.add(dir);
                            traveled = Math.abs(to.distance(loc1));
                        }
                        deconstruct();
                        cancel();
                    }
                    ticks++;
                    if (ticks == maxTicks) {
                        deconstruct();
                        cancel();
                    }
                }
            }.runTaskTimer(plugin, 20, tickPeriod);

            ArrayList<Location> circle = circle(loc.clone().add(0, 0.2, 0), 20, radius);
            new BukkitRunnable() {
                int index = 0;
                public void run() {
                    if (deconstructed) cancel();
                    Location l = circle.get(index);
                    l.getWorld().spigot().playEffect(l, Effect.CRIT, 0, 0, 0.0F, 0.0F, 0.0F, 0.0F, 1, 128);
                    index++;
                    if (index == circle.size() - 1) index = 0;
                }
            }.runTaskTimer(plugin, 0, 1);
        }

        public ArrayList<Location> circle(Location centerPoint, int particleAmount, double circleRadius) {
            World world = centerPoint.getWorld();
            double increment = (2 * Math.PI) / particleAmount;
            ArrayList<Location> locations = new ArrayList<Location>();
            for (int i = 0; i < particleAmount; i++) {
                double angle = i * increment;
                double x = centerPoint.getX() + (circleRadius * Math.cos(angle));
                double z = centerPoint.getZ() + (circleRadius * Math.sin(angle));
                locations.add(new Location(world, x, centerPoint.getY(), z));
            }
            return locations;
        }

        public void deconstruct() {
            for (Block b : componentBlocks) {
                if (b.getType() != Material.AIR) b.getWorld().spigot().playEffect(b.getLocation().add(0.5, 0, 0.5), Effect.TILE_BREAK, b.getType().getId(), b.getData(), 0.6F, 0.6F, 0.6F, 0.0F, 25, 128);
                b.getWorld().spigot().playEffect(b.getLocation().add(0.5, 0, 0.5), Effect.PARTICLE_SMOKE, 0, 0, 0.6F, 0.6F, 0.6F, 0.0F, 10, 128);
                b.getWorld().playSound(b.getLocation(), Sound.BLOCK_STONE_BREAK, 0.3F, 1.0F);
                b.getWorld().playSound(b.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 0.3F, 0.5F);
                b.setType(Material.AIR);
            }
            deconstructed = true;
            broadcast(hero.getPlayer().getLocation(), ChatColor.GRAY + "[" + ChatColor.BLUE + "Skill" + ChatColor.GRAY + "] " + ChatColor.WHITE + hero.getName() + ChatColor.GRAY + "'s lightningrod collapses.");
            lightningrods.remove(hero.getPlayer());
        }
    }

}
