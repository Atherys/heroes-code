package com.atherys.skills.SkillLivingBomb;

import com.atherys.effects.TenacityEffect;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.util.Vector;

public class SkillLivingBomb extends ActiveSkill {

    public SkillLivingBomb(Heroes plugin) {
        super(plugin, "LivingBomb");
        setDescription("You become immune to knockback effects for $1 seconds. At the end of this duration, you explode violently, dealing $2 damage to everything within $3 meters and knocking it back.");
        setUsage("/skill livingbomb");
        setArgumentRange(0, 0);
        setIdentifiers("skill livingbomb");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000, true);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 100, true);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5.0D, true);
        return getDescription().replace("$1", (duration / 1000) + "").replace("$2", damage + "").replace("$3", radius + "");
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 4000);
        node.set(SkillSetting.DAMAGE.node(), 100);
        node.set(SkillSetting.RADIUS.node(), 5.0);
        node.set(SkillSetting.RADIUS_INCREASE.node(), 0.1);
        node.set("blast-force-multiplier", 0.3);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        final Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000, true);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 100, true);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5.0D, true);
        double blastForce = SkillConfigManager.getUseSetting(hero, this, "blast-force-multiplier", 0.3D, true);
        broadcastExecuteText(hero);
        LivingBombEffect lb = new LivingBombEffect(this, duration, player, radius, damage, blastForce);
        hero.addEffect(lb);
        TenacityEffect tenacity = new TenacityEffect(this, duration, false, hero);
        hero.addEffect(tenacity);
        player.getWorld().spigot().playEffect(player.getLocation().add(0, 0.5, 0), Effect.LAVA_POP, 0, 0, 0.4f, 0.5F, 0.4F, 0.0F, 25, 16);
        player.getWorld().spigot().playEffect(player.getLocation().add(0, 0.5, 0), Effect.LARGE_SMOKE, 0, 0, 0.4f, 0.5F, 0.4F, 0.0F, 25, 16);
        player.getWorld().playSound(player.getLocation(), Sound.BLOCK_LAVA_AMBIENT, 1.0F, 1.0F);
        return SkillResult.NORMAL;
    }

    public class LivingBombEffect extends PeriodicExpirableEffect {

        private double radius;
        private double damage;
        private double blastForce;
        private int tickDelay = 0;
        private int maxTickDelay = 5;

        public LivingBombEffect(Skill skill, long duration, Player applier, double radius, double damage, double blastForce) {
            super(skill, "LivingBomb", applier, 100, duration);
            this.radius = radius;
            this.damage = damage;
            this.blastForce = blastForce;
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            final Player player = hero.getPlayer();
            broadcast(player.getLocation(), "" + hero.getName() + " begins ticking ominously...");
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            final Player player = hero.getPlayer();
            broadcast(player.getLocation(), "" + hero.getName() + " explodes!");
            hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 1.0F);
            hero.getPlayer().getWorld().spigot().playEffect(hero.getPlayer().getLocation().add(0, 0.5, 0), Effect.LAVA_POP, 0, 0, 0.4f, 0.5F, 0.4F, 0.0F, 35, 128);
            hero.getPlayer().getWorld().spigot().playEffect(hero.getPlayer().getLocation().add(0, 0.5, 0), Effect.FLAME, 0, 0, 0.4f, 0.5F, 0.4F, 0.2F, 35, 128);
            hero.getPlayer().getWorld().spigot().playEffect(hero.getPlayer().getLocation().add(0, 0.5, 0), Effect.EXPLOSION_LARGE, 0, 0, 2.4f, 2.5F, 2.4F, 0.0F, 15, 128);

            for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                if (!(e instanceof LivingEntity)) continue;
                LivingEntity le = (LivingEntity) e;
                if (!damageCheck(player, le)) continue;
                addSpellTarget(le, hero);
                damageEntity(le, player, damage, DamageCause.MAGIC, false);
                Location tLoc = le.getLocation();
                Location pLoc = player.getLocation();
                Vector velocity = tLoc.toVector().subtract(pLoc.toVector()).multiply(blastForce);
                le.setVelocity(velocity);
            }
        }

        public void tickHero(Hero hero) {
            if (tickDelay == 0) {
                hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.BLOCK_TRIPWIRE_CLICK_ON, 1.0F, 2.0F);
                tickDelay = maxTickDelay;
            }
            else tickDelay--;
            hero.getPlayer().getWorld().spigot().playEffect(hero.getPlayer().getLocation().add(0, 0.5, 0), Effect.LAVA_POP, 0, 0, 0.4f, 0.5F, 0.4F, 0.0F, 15, 16);
            hero.getPlayer().getWorld().spigot().playEffect(hero.getPlayer().getLocation().add(0, 0.5, 0), Effect.LAVADRIP, 0, 0, 0.4f, 0.5F, 0.4F, 0.0F, 15, 16);
        }

        public void tickMonster(Monster arg0) {}

    }

}
