package com.atherys.skills.SkillLivingBomb.event;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerVelocityEvent;

/**
 * @author apteryx
 * @time 7:31 PM
 * @since 11/6/2016
 */
public class KnockbackEvent implements Listener {

    @EventHandler
    public void onKnockback(PlayerVelocityEvent event) {
        Hero hero = Heroes.getInstance().getCharacterManager().getHero(event.getPlayer());
        if (hero != null) {
            if (hero.hasEffect("LivingBomb")) {
                event.setCancelled(true);
            }
        }

    }
}
