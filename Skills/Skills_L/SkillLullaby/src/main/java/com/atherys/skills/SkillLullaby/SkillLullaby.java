package com.atherys.skills.SkillLullaby;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Arthur on 10/27/2015.
 */
public class SkillLullaby extends TargettedSkill {
    public SkillLullaby(Heroes plugin) {
        super(plugin, "Lullaby");
        setDescription("Give a target Slow $1 for $2 seconds, if the target does not take melee damage, then the target falls asleep, Blinding and Rooting the target for $3 seconds.");
        setUsage("/skill Lullaby");
        setArgumentRange(0, 0);
        setIdentifiers("skill Lullaby");
        setTypes(SkillType.SILENCEABLE, SkillType.DESUMMONING, SkillType.ABILITY_PROPERTY_DARK, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillLullabyListener(), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("slow-duration", Long.valueOf(3000));
        node.set("slow-amplifier", Integer.valueOf(2));
        node.set("blind-root-duration", Long.valueOf(5000));
        node.set(SkillSetting.MAX_DISTANCE.node(), Double.valueOf(10));
        return node;
    }

    public String getDescription(Hero hero) {
        long slowDuration = (long) SkillConfigManager.getUseSetting(hero, this, "slow-duration", 3000L, false);
        int slowAmp = SkillConfigManager.getUseSetting(hero, this, "slow-amplifier", 2, false);
        long blindRootDuration = (long) SkillConfigManager.getUseSetting(hero, this, "blind-root-duration", 5000L, false);
        return getDescription().replace("$1", slowAmp + "").replace("$2", (int) (slowDuration / 1000) + "").replace("$3", (int) (blindRootDuration / 1000) + " ") + Lib.getSkillCostStats(hero, this);
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.CANCELLED;
        }
        Hero tHero = plugin.getCharacterManager().getHero((Player) target);
        if (!target.equals(player)) {
            if (damageCheck(player, target)) {
                long slowDuration = (long) SkillConfigManager.getUseSetting(hero, this, "slow-duration", 3000L, false);
                int slowAmp = SkillConfigManager.getUseSetting(hero, this, "slow-amplifier", 2, false);
                long blindRootDuration = (long) SkillConfigManager.getUseSetting(hero, this, "blind-root-duration", 5000L, false);
                tHero.addEffect(new LullabySlowEffect(this, slowDuration, slowAmp, blindRootDuration,hero) );
                broadcastExecuteText(hero, target);
                return SkillResult.NORMAL;
            }
            return SkillResult.CANCELLED;
        }
        return SkillResult.CANCELLED;
    }

    public class LullabySlowEffect extends ExpirableEffect {
        private final long blindRootDuration;

        public LullabySlowEffect(Skill skill, long slowDuration, int slowAmp, long blindRootDuration,Hero caster) {
            super(skill, "LullabySlowEffect",caster.getPlayer(), slowDuration);
            this.blindRootDuration = blindRootDuration;
            int tickDuration = (int) (slowDuration / 1000L) * 20;
            addMobEffect(2, tickDuration, slowAmp, false);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.SLOW);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                hero.addEffect(new LullabyBlindRootEffect(skill, blindRootDuration,hero));
            }
        }
    }

    public class LullabyBlindRootEffect extends PeriodicExpirableEffect {
        private double x;
        private double y;
        private double z;
        private double playerhp;

        public LullabyBlindRootEffect(Skill skill, long duration,Hero caster) {
            super(skill, "LullabyBlindRootEffect",caster.getPlayer(), 100L, duration);
            addMobEffect(15, (int) (duration / 1000L * 20L), 3, false);
            this.types.add(EffectType.ROOT);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.BLIND);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Location location = hero.getPlayer().getLocation();
            this.x = location.getX();
            this.y = location.getY();
            this.z = location.getZ();
            Player player = hero.getPlayer();
            this.playerhp = player.getHealth();
            broadcast(location, "$1 has fallen asleep!", player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 has awoken!", player.getDisplayName());
        }

        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            Location location = player.getLocation();
            if (player.getHealth() >= playerhp) {
                if ((location.getX() != this.x) || (location.getY() != this.y) || (location.getZ() != this.z)) {
                    location.setX(this.x);
                    location.setY(this.y);
                    location.setZ(this.z);
                    location.setYaw(player.getLocation().getYaw());
                    location.setPitch(player.getLocation().getPitch());
                    player.teleport(location);
                }
            } else {
                this.expire();
                player.removePotionEffect(PotionEffectType.BLINDNESS);
            }
        }

        public void tickMonster(Monster monster) {
        }
    }

    public class SkillLullabyListener implements Listener {
        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onMeleeDamage(WeaponDamageEvent event) {
            if ((!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("LullabySlowEffect")) {
                hero.removeEffect(hero.getEffect("LullabySlowEffect"));
            }
        }
    }
}
