/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.skills.lungingstrike;

import com.atherys.core.Core;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class LungingStrikeEffect extends PeriodicExpirableEffect {

    public LungingStrikeEffect(Skill skill, Player applier, long period, long duration) {
        super(skill, "LungingStrike", applier, period, duration);
        this.types.add(EffectType.PHYSICAL);
        this.types.add(EffectType.HARMFUL);
    }

    @Override
    public void tickMonster(Monster monster) {

    }

    @Override
    public void tickHero(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 2, false);
        hero.getPlayer().getNearbyEntities(radius, radius, radius).forEach(e -> {
            if (e instanceof Player && damageCheck(hero.getPlayer(), (LivingEntity) e)) {
                Hero target = Core.getHeroes().getCharacterManager().getHero((Player)e);

                if (!target.hasEffect("Stun")) target.addEffect(new StunEffect(skill, getDuration(), target));
            }
        });
    }

    public static boolean damageCheck(Player player, LivingEntity target) {
        if(player.equals(target)) {
            return false;
        } else {
            EntityDamageByEntityEvent event = new EntityDamageByEntityEvent(player, target, EntityDamageEvent.DamageCause.CUSTOM, 0.0D);
            Bukkit.getServer().getPluginManager().callEvent(event);
            return !event.isCancelled();
        }
    }
}
