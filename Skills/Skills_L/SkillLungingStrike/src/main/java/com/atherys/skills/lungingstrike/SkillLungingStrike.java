/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.skills.lungingstrike;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class SkillLungingStrike extends ActiveSkill {

    public SkillLungingStrike(Heroes plugin) {
        super(plugin, "LungingStrike");
        setDescription("Lunge forward and stun enemies you pass through for $1 seconds");
        setUsage("/skill LungingStrike");
        setArgumentRange(0, 0);
        setIdentifiers("skill LungingStrike");
        setTypes(SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.DAMAGING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("max-drift", 5);
        node.set(SkillSetting.RADIUS.node(), 2);
        node.set(SkillSetting.DURATION.node(), 3000);
        node.set("stun-duration", 2000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, "stun-duration", 2000, false);
        return getDescription().replace("$1", String.valueOf(duration / 1000));
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 2000, false);
        Player player = hero.getPlayer();
        Material mat = player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType();
        if (mat.equals(Material.AIR) || player.isInsideVehicle()) {
            Messaging.send(player, "You can't lunge while mid-air or from inside a vehicle!");
            return SkillResult.FAIL;
        }
        lunge(hero);
        broadcast(player.getLocation(), "    $1 used $2!", hero.getPlayer().getName(), "LungingStrike");
        hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_PLAYER_ATTACK_STRONG, 1F, 0.5F);
        hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_PLAYER_ATTACK_CRIT, 1F, 0.5F);
        hero.addEffect(new LungingStrikeEffect(this, hero.getPlayer(), 10, duration));
        return SkillResult.NORMAL;
    }

    private void lunge(Hero hero) {
        Player player = hero.getPlayer();
        float pitch = player.getEyeLocation().getPitch();
        float maxDrift = (float) SkillConfigManager.getUseSetting(hero, this, "max-drift", 1, false);
        if (pitch > 45.0F) maxDrift = 1;
        if (pitch > 0.0F) pitch = -pitch;
        float multiplier = (90.0F + pitch) / 50.0F;
        Vector v = player.getVelocity().setY(0.5).add(
                player.getLocation()
                        .getDirection().setY(0).normalize()
                        .multiply(multiplier * maxDrift)
        );
        player.setVelocity(v);
    }
}
