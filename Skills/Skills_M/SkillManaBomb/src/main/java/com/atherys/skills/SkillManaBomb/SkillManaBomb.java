package com.atherys.skills.SkillManaBomb;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillManaBomb extends ActiveSkill {
    private String expiretext;

    public SkillManaBomb(Heroes plugin) {
        super(plugin, "ManaBomb");
        setDescription("You place a ManaBomb which will periodically pull nearby enemies and after a short time explode, dealing magic damage to nearby enemies and stealing mana from them.");
        setUsage("/skill ManaBomb");
        setArgumentRange(0, 0);
        setIdentifiers("skill ManaBomb");
        setTypes(SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new ManaBombListener(), plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block b = player.getTargetBlock(Util.transparentBlocks, max);
        if (b.getType() == Material.AIR || !Util.transparentBlocks.contains(b.getRelative(BlockFace.UP).getType())) {
            return SkillResult.CANCELLED;
        }
        Location loc = b.getLocation().add(0.5, 1, 0.5);
        EnderCrystal enderCrystal = (EnderCrystal)loc.getWorld().spawnEntity(loc, EntityType.ENDER_CRYSTAL);
        enderCrystal.setMetadata("ManaBombEnderCrystal", new FixedMetadataValue(plugin, true));
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 40, false);
        int mana = SkillConfigManager.getUseSetting(hero, this, "mana-drain", 25, false);
        int pullRange = SkillConfigManager.getUseSetting(hero, this, "pull-range", 8, false);
        int explodeRange = SkillConfigManager.getUseSetting(hero, this, "explode-range", 5, false);
        hero.addEffect(new ManaBombEffect(this, period, duration, damage, mana, pullRange, explodeRange, enderCrystal,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), 15);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set(SkillSetting.DURATION.node(), 4000);
        node.set(SkillSetting.DAMAGE.node(), 40);
        node.set("mana-drain", 25);
        node.set("pull-range", 8);
        node.set("explode-range", 5);
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% detonated!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% detonated!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class ManaBombEffect extends PeriodicExpirableEffect {
        private EnderCrystal manaBomb;
        private double damage;
        private int mana;
        private int pullRange;
        private int explodeRange;

        public ManaBombEffect(Skill skill, long period, long duration, double damage, int mana, int pullRange, int explodeRange, EnderCrystal manaBomb,Hero caster) {
            super(skill, "ManaBomb",caster.getPlayer(), period, duration);
            this.manaBomb = manaBomb;
            this.damage = damage;
            this.mana = mana;
            this.pullRange = pullRange;
            this.explodeRange = explodeRange;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.FORM);
            types.add(EffectType.MAGIC);
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), expiretext, player.getDisplayName(), "ManaBomb");
                manaBomb.getWorld().playSound(manaBomb.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
                manaBomb.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_HUGE, manaBomb.getLocation(), 1);
                for (Entity e : manaBomb.getNearbyEntities(explodeRange, explodeRange, explodeRange)) {
                    if (e instanceof Player) {
                        Player target = (Player)e;
                        if (damageCheck(player, (LivingEntity) target)) {
                            damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
                            Hero tHero = plugin.getCharacterManager().getHero(target);
                            tHero.setMana(tHero.getMana() - mana < 0 ? 0 : tHero.getMana() - mana);
                        }
                    }
                }
            }
            manaBomb.remove();
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (Entity e : manaBomb.getNearbyEntities(pullRange, pullRange, pullRange)) {
                if (e instanceof Player) {
                    Player target = (Player)e;
                    if (damageCheck(player, (LivingEntity) target)) {
                        Vector v = manaBomb.getLocation().toVector().subtract(target.getLocation().toVector()).normalize();
                        double cc = target.getLocation().distance(manaBomb.getLocation()) * 0.2;
                        v = v.multiply(cc);
                        if (cc >= 0.4) {
                            v = v.setY(0.5);
                        }
                        target.setVelocity(v);
                    }
                }
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }

    public class ManaBombListener implements Listener {
        @EventHandler (priority = EventPriority.HIGH)
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.getEntity() instanceof EnderCrystal) {
                if (event.getEntity().hasMetadata("ManaBombEnderCrystal")) {
                    event.setDamage(0);
                    event.setCancelled(true);
                }
            }
        }

        @EventHandler (priority = EventPriority.HIGH)
        public void onEntityExplode(EntityExplodeEvent event) {
            if (event.getEntity() instanceof EnderCrystal) {
                if (event.getEntity().hasMetadata("ManaBombEnderCrystal")) {
                    event.setYield(0);
                    event.blockList().clear();
                    event.setCancelled(true);
                }
            }
        }
    }
}