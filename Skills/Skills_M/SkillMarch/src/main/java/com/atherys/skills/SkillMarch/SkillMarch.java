package com.atherys.skills.SkillMarch;

import com.atherys.effects.CommanderDeployedEffect;
import com.atherys.effects.InvulnEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillMarch extends ActiveSkill {

    public SkillMarch(Heroes plugin) {
        super(plugin, "March");
        setDescription("Allies within $1 blocks of your BattleCommander are granted invulnerability for $2s while still being allowed to attack.");
        setUsage("/skill march");
        setArgumentRange(0, 0);
        setIdentifiers("skill march");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);

    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 7, false);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        String description = getDescription().replace("$1", radius + "").replace("$2", duration/1000 + "");
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 5000);
        node.set(SkillSetting.RADIUS.node(), 7);
        node.set("party-cooldown", 20000L);
        return node;
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if(hero.hasEffect("CommanderDeployedEffect")) {
            Hero commander =( (CommanderDeployedEffect)hero.getEffect("CommanderDeployedEffect")).getCommander();
	        int range = SkillConfigManager.getUseSetting(hero, this, "Range", 20, false);
	        if (hero.getPlayer().getLocation().distanceSquared(commander.getPlayer().getLocation()) <= range * range){
		        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
		        long pcd = (long) SkillConfigManager.getUseSetting(hero, this, "party-cooldown", 20000L, false);
		        long time = System.currentTimeMillis();
		        if(!hero.hasParty()&&commander.equals(hero)) {
			        hero.addEffect(new InvulnEffect(this, "March", duration,hero));
		        } else {
			        Player pCommander = commander.getPlayer();
			        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 7, false);
			        for (Hero pHero : hero.getParty().getMembers()) {
				        Player pPlayer = pHero.getPlayer();
				        if (pPlayer.getWorld().equals(pCommander.getWorld()) && pPlayer.getLocation().distanceSquared(pCommander.getLocation()) <= radius * radius ) {
					        pHero.addEffect(new InvulnEffect(this, "March", duration,hero));
				        }
				        if (pHero.hasAccessToSkill(this) && (!hero.equals(pHero)) && (pHero.getCooldown("March") == null || ((pHero.getCooldown("March") - time) < pcd))) {
					        pHero.setCooldown("March", pcd + time);
				        }
			        }
			        broadcast(pCommander.getLocation(), "$1's troops are on the march!", commander.getName());
		        }
		        broadcastExecuteText(hero);
		        return SkillResult.NORMAL;
	        }else {
		        Messaging.send(hero.getPlayer(), "Target too far away!");
		        return SkillResult.INVALID_TARGET;
	        }

        } else {
            Messaging.send(hero.getPlayer(), "You have not deployed a BattleCommander");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
    }

}