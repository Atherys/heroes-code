package com.atherys.skills.SkillMassDispell;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;

public class SkillMassDispell extends ActiveSkill {
    public SkillMassDispell(Heroes plugin) {
        super(plugin, "MassDispell");
        setDescription("Removes 1 debuff from each member of your party.");
        setArgumentRange(0, 0);
        setUsage("/skill MassDispell");
        setIdentifiers("skill MassDispell");
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
	    if (!hero.hasParty()) {
		    //hero.getPlayer().sendMessage(ChatColor.GRAY + "You aren't in party.");
		    for (Effect effect : hero.getEffects()) {
			    if (effect.isType(EffectType.HARMFUL) && effect.isType(EffectType.DISPELLABLE) && !effect.isType(EffectType.UNBREAKABLE) && !effect.isType(EffectType.BENEFICIAL)) {
				    hero.removeEffect(effect);
				    break; //BREAKS AFTER 1 HARMFUL EFFECT
			    }
		    }
		    broadcastExecuteText(hero);
		    return SkillResult.NORMAL;
	    } else {
		    for (Hero phero : hero.getParty().getMembers()) {
			    for (Effect effect : phero.getEffects()) {
				    if (effect.isType(EffectType.HARMFUL) && effect.isType(EffectType.DISPELLABLE) && !effect.isType(EffectType.UNBREAKABLE) && !effect.isType(EffectType.BENEFICIAL)) {
					    phero.removeEffect(effect);
					    break; //BREAKS AFTER 1 HARMFUL EFFECT
				    }
			    }
		    }
		    broadcastExecuteText(hero);
		    return SkillResult.NORMAL;
	    }
    }
}