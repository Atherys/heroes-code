package com.atherys.skills.SkillMassInvis;

import com.atherys.effects.BetterInvis;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillMassInvis extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillMassInvis(Heroes plugin) {
        super(plugin, "MassInvis");
        setDescription("Gives you and your party within $1 blocks invisibility for $2s");
        setUsage("/skill massinvis");
        setArgumentRange(0, 0);
        setIdentifiers("skill massinvis");
        setTypes(SkillType.ABILITY_PROPERTY_ILLUSION, SkillType.BUFFING, SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.STEALTHY, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 30, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS_INCREASE, 0, false) * hero.getSkillLevel(this);
        int duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false) * hero.getSkillLevel(this)) / 1000;
        String description = getDescription().replace("$1", radius + "").replace("$2", duration + "");
        int cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false);
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        return description;
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set("radius-increase", Integer.valueOf(0));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(12000));
        node.set("duration-increase", Integer.valueOf(0));
        node.set(SkillSetting.APPLY_TEXT.node(), "You and your party member are now invisible!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You reappeared!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% is now invisible!!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "Someone reappeared");
    }

    public SkillResult use(Hero hero, String[] args) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 15000, false);
        Location heroLoc;
        Player player = hero.getPlayer();
        if (!hero.hasParty()) {
            hero.addEffect(new BetterInvis(this, duration, this.applyText, this.expireText, hero));
            return SkillResult.NORMAL;
        } else {
            int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
            heroLoc = player.getLocation();
            for (Hero partyHero : hero.getParty().getMembers()) {
                if (player.getWorld().equals(partyHero.getPlayer().getWorld())) {
                    if (partyHero.getPlayer().getLocation().distanceSquared(heroLoc) <= r * r) {
                        partyHero.getPlayer().spawnParticle(Particle.SMOKE_NORMAL, partyHero.getPlayer().getLocation(), 1);
                        partyHero.addEffect(new BetterInvis(this, duration, this.applyText, this.expireText, partyHero));
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
