package com.atherys.skills.SkillMassPiggify;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillMassPiggify extends ActiveSkill {

    public SkillMassPiggify(Heroes plugin) {
        super(plugin, "MassPiggify");
        setDescription("You force your all enemies to ride a pig for $1 seconds.");
        setUsage("/skill MassPiggify");
        setArgumentRange(0, 0);
        setIdentifiers("skill MassPiggify");
        setTypes(SkillType.SILENCEABLE, SkillType.SUMMONING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        for (Entity e : hero.getPlayer().getNearbyEntities(radius, radius, radius)) {
            if (e instanceof LivingEntity) {
                if (!damageCheck(hero.getPlayer(), (LivingEntity) e)) {
                    continue;
                }
                if (e instanceof Player) {
                    Player tplayer = (Player)e;
                    if (tplayer.equals(hero.getPlayer())) {
                        return SkillResult.INVALID_TARGET_NO_MSG;
                    }else {
	                    Entity creature = e.getWorld().spawnEntity(e.getLocation(), EntityType.PIG);
	                    CharacterTemplate characterTemplate = this.plugin.getCharacterManager().getCharacter((LivingEntity) e);
	                    characterTemplate.addEffect(new PigEffect(this, duration, creature,hero));
                        Lib.cancelDelayedSkill((Hero) characterTemplate);
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class PigEffect extends ExpirableEffect {
        private final Pig creature;

        public PigEffect(Skill skill, long duration, Entity creature,Hero caster) {
            super(skill, "Piggify",caster.getPlayer(), duration);
            this.creature = (Pig) creature;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.HARMFUL);
            types.add(EffectType.DISABLE);
            types.add(EffectType.MAGIC);
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
	        creature.setSaddle(true);
            creature.setPassenger(monster.getEntity());
            creature.setMetadata("PiggifyCreature", new FixedMetadataValue(plugin, monster));
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
	        creature.setSaddle(true);
            creature.setPassenger(player);
            creature.setMetadata("PiggifyCreature", new FixedMetadataValue(plugin, hero));
        }

        @Override
        public void removeFromMonster(Monster rider) {
            super.removeFromMonster(rider);
            creature.remove();
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            creature.remove();
        }
    }
}