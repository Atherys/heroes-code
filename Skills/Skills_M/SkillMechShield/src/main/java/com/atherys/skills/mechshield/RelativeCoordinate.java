/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.skills.mechshield;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.bukkit.Location;
import org.bukkit.World;

public class RelativeCoordinate {

    private World w;
    private int x;
    private int y;
    private int z;

    public RelativeCoordinate(Location loc, int dx, int dz) {
        this(loc, dx, 0, dz);
    }

    public RelativeCoordinate(Location loc, int dx, int dy, int dz) {
        this.w = loc.getWorld();
        this.x = loc.getBlockX() + dx;
        this.y = loc.getBlockY() + dy;
        this.z = loc.getBlockZ() + dz;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("x=" + x)
                .append("y=" + y)
                .append("z=" + z)
                .build();
    }

    public Location toLocation() {
        return new Location(w, x, y, z);
    }

}
