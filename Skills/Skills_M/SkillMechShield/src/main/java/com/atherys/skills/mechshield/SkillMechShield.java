package com.atherys.skills.mechshield;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

public class SkillMechShield extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillMechShield(Heroes plugin) {
        super(plugin, "MechShield");
        setDescription("Saboteur places MechShield for $1 seconds.");
        setUsage("/skill MechShield");
        setArgumentRange(0, 0);
        setIdentifiers("skill MechShield");
        setTypes(SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("MechShield")) hero.removeEffect(hero.getEffect("MechShield"));
        Player p = hero.getPlayer();
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block b = p.getTargetBlock(Util.transparentBlocks, max);
        if (b.getType() == Material.AIR) {
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        List<RelativeCoordinate> points = buildPointList(b.getLocation());
        points.forEach(point -> {
            for (int dy = 0; dy < 3; dy++) {
                Location loc = point.toLocation().add(0, dy, 0);
                Block block = loc.getWorld().getBlockAt(loc);
                if (block.getType() != Material.AIR) continue;
                block.setType(Material.IRON_FENCE);
                block.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(Heroes.getInstance(), true));
                values.add(block);
            }
        });
        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1F, 0.5F);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        hero.addEffect(new MechShieldEffect(this, duration, values, hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.MAX_DISTANCE.node(), 15);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% placed %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% has been removed.");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% placed %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% has been removed.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class MechShieldEffect extends ExpirableEffect {
        private final Set<Block> values;

        public MechShieldEffect(Skill skill, long duration, Set<Block> values,Hero caster) {
            super(skill, "MechShield",caster.getPlayer(), duration);
            this.values = values;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.FORM);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.PHYSICAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "MechShield");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Lib.removeBlocks(values);
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "MechShield");
            }
        }
    }

    public List<RelativeCoordinate> buildPointList(Location loc) {
        List<RelativeCoordinate> points = new ArrayList<>();
        loc.add(0, 1, 0);
        
        points.add(new RelativeCoordinate(loc, 0, 0));
        points.add(new RelativeCoordinate(loc, 1, 0));
        points.add(new RelativeCoordinate(loc, -1, 0));
        points.add(new RelativeCoordinate(loc, 1, 1));
        points.add(new RelativeCoordinate(loc, 2, 1));
        points.add(new RelativeCoordinate(loc, 2, 2));
        points.add(new RelativeCoordinate(loc, 3, 2));
        points.add(new RelativeCoordinate(loc, 3, 3));
        points.add(new RelativeCoordinate(loc, 1, -1));
        points.add(new RelativeCoordinate(loc, 2, -1));
        points.add(new RelativeCoordinate(loc, 2, -2));
        points.add(new RelativeCoordinate(loc, 3, -2));
        points.add(new RelativeCoordinate(loc, 3, -3));
        points.add(new RelativeCoordinate(loc, -1, -1));
        points.add(new RelativeCoordinate(loc, -2, -1));
        points.add(new RelativeCoordinate(loc, -2, -2));
        points.add(new RelativeCoordinate(loc, -3, -2));
        points.add(new RelativeCoordinate(loc, -3, -3));
        points.add(new RelativeCoordinate(loc, -1, 1));
        points.add(new RelativeCoordinate(loc, -2, 1));
        points.add(new RelativeCoordinate(loc, -2, 2));
        points.add(new RelativeCoordinate(loc, -3, 2));
        points.add(new RelativeCoordinate(loc, -3, 3));

        return points;
    }

}