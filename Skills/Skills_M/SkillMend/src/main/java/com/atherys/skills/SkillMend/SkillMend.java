package com.atherys.skills.SkillMend;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillMend extends ActiveSkill {
    private String ApplyText;
    private String ExpireText;

    public SkillMend(Heroes plugin) {
        super(plugin, "Mend");
        setDescription("Targets your weakest member and slowly mends their wounds. Removes bleed.");
        setUsage("/skill mend");
        setArgumentRange(0, 0);
        setIdentifiers("skill Mend");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(2D));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(20));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(20000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(15));
        node.set(SkillSetting.APPLY_TEXT.node(), "You were picked as the weakest member of the party.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "Mend has expired");
        return node;
    }

    @Override
    public void init() {
        super.init();
        ApplyText = ChatColor.GRAY + SkillConfigManager.getUseSetting(null, this, SkillSetting.APPLY_TEXT.node(), "You were picked as the weakest member of the party.");
        ExpireText = ChatColor.GRAY + SkillConfigManager.getUseSetting(null, this, SkillSetting.EXPIRE_TEXT.node(), "Mend has expired");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 10, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000L, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        if (!hero.hasParty()) {
            hero.addEffect(new MendEffect(this, duration, period, amount, player));
            if (hero.hasEffect("Bleed")) {
                hero.removeEffect(hero.getEffect("Bleed"));
            }
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 15, false);
        Hero h = hero;
        for (Hero phero : hero.getParty().getMembers()) {
            if (player.getWorld().equals(phero.getPlayer().getWorld()) && (player.hasLineOfSight(phero.getPlayer())) && ((((phero.getPlayer()).getLocation()).distanceSquared(player.getLocation())) <= radius * radius) && (h.getPlayer().getHealth() > phero.getPlayer().getHealth())) {
                h = phero;
            }
        }
        h.addEffect(new MendEffect(this, duration, period, amount, player));
        if (h.hasEffect("Bleed")) {
            h.removeEffect(h.getEffect("Bleed"));
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class MendEffect extends PeriodicHealEffect {
        public MendEffect(Skill skill, long duration, long period, double amount, Player caster) {
            super(skill, "Mend",caster, period, duration, amount);
            types.add(EffectType.HEALING);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            hero.getPlayer().sendMessage(ApplyText);
        }

        /*
                @Override
                public void tickHero(Hero hero)
                {
                    super.tickHero(hero);
                    HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(hero, amount, skill, plugin.getCharacterManager().getHero(caster));
                    plugin.getServer().getPluginManager().callEvent(hrhEvent);
                }
               */
        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ExpireText);
        }
    }
}