package com.atherys.skills.SkillMeteor;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillMeteor extends ActiveSkill {

    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "");
    }

    public SkillMeteor(Heroes plugin) {
        super(plugin, "Meteor");
        setDescription("You shoot a ball of fire that deals $1 damage and lights your target on fire");
        setUsage("/skill Meteor");
        setArgumentRange(0, 0);
        setIdentifiers("skill meteor");
        setTypes(SkillType.ABILITY_PROPERTY_FIRE, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 4);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 0.0D);
        node.set("velocity-multiplier", 1.0D);
        node.set("fire-ticks", 100);
        node.set(SkillSetting.RADIUS.node(), 3);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        LargeFireball fireball = player.launchProjectile(LargeFireball.class);
        fireball.setIsIncendiary(false);
        fireball.setYield(1.0F);
        fireball.setMetadata("MeteorMeteor", new FixedMetadataValue(plugin, true));
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 1, false);
        fireball.setVelocity(fireball.getVelocity().multiply(mult));
        fireball.setShooter(player);
        player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 1.0F, 1.0F);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SkillEntityListener implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onGhastProjectileHit(EntityExplodeEvent event) {
            if ((!(event.getEntity() instanceof LargeFireball)) || (!event.getEntity().hasMetadata("MeteorMeteor"))) {
                return;
            }

            event.blockList().clear();
            LargeFireball fireball = (LargeFireball) event.getEntity();
            Entity dmger = (Player) fireball.getShooter();

            Hero hero = SkillMeteor.this.plugin.getCharacterManager().getHero((Player) dmger);
            int radius = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.RADIUS.node(), 30, false);
            for (Entity e : fireball.getNearbyEntities(radius, radius, radius)) {
                if (((e instanceof LivingEntity)) && (SkillMeteor.damageCheck((Player) dmger, (LivingEntity) e))) {
                    int ft = SkillConfigManager.getUseSetting(hero, this.skill, "fire-tics", 100, false);
                    e.setFireTicks(ft);
                    SkillMeteor.this.addSpellTarget(e, hero);

                    double damage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE, 4, false);
                    damage += (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE_INCREASE, 0.0D, false) * hero.getSkillLevel(this.skill));

                    skill.damageEntity((LivingEntity) e, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                }
                event.setCancelled(true);
            }
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onProjectileHit(ProjectileHitEvent event) {
            if (!(event.getEntity() instanceof LargeFireball)) {
                return;
            }
            LargeFireball fireball = (LargeFireball) event.getEntity();
            if (!fireball.hasMetadata("MeteorMeteor")) {
                return;
            }
            fireball.setIsIncendiary(false);
            fireball.setFireTicks(0);
        }
    }
}
