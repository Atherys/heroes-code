package com.atherys.skills.SkillMinuet;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillMinuet extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillMinuet(Heroes plugin) {
        super(plugin, "Minuet");
        setDescription("Tethers the nearest enemy to their nearest ally for $1 seconds. ");
        setUsage("/skill Minuet");
        setArgumentRange(0, 0);
        setIdentifiers("skill Minuet");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        Player p = getClosestPlayer(hero.getPlayer(), r);
        if (p == null) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "No nearby targets.");
            return SkillResult.CANCELLED;
        }
        Hero p2 = getClosestFriendlyPlayer(plugin.getCharacterManager().getHero(p), r);
        if (p2 == null) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "This target is here alone.");
            return SkillResult.CANCELLED;
        }
        plugin.getCharacterManager().getHero(p).addEffect(new MinuetEffect(this, duration, r, p2.getPlayer(),hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("barrier-size", Integer.valueOf(50));
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% was tethered to %target2%");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% %target2%");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% was tethered to %target2%").replace("%target%", "$1").replace("%target2%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% - %target2%").replace("%target%", "$1").replace("%target2%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class MinuetEffect extends PeriodicExpirableEffect {
        //private final int r;
        private final Player p2;

        public MinuetEffect(Skill skill, long duration, int r, Player p2, Hero caster) {
            super(skill, "MinuetEffect",caster.getPlayer(), 500L, duration);
            types.add(EffectType.DISABLE);
            types.add(EffectType.HARMFUL);
            types.add(EffectType.DISPELLABLE);
            //this.r = r;
            this.p2 = p2;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), p2.getDisplayName());
            hero.getPlayer().sendMessage(net.md_5.bungee.api.ChatColor.GRAY + "You are silenced!");
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            try {
                hero.getPlayer().teleport(p2);
            } catch (NullPointerException e) {
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), p2.getDisplayName());
            hero.getPlayer().sendMessage(net.md_5.bungee.api.ChatColor.GRAY + "You are no longer silenced!");
        }
    }

    public Player getClosestPlayer(Player player, double radius) {
        double rSquared = radius * radius;
        double curDist;
        Player closest = null;
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if (!(e instanceof Player)) {
                continue;
            }
            if (damageCheck(player, (LivingEntity) e)) {
                curDist = player.getLocation().distanceSquared(e.getLocation());
                if (curDist < rSquared) {
                    rSquared = curDist;
                    closest = ((Player) e);
                }
            }
        }
        return closest;
    }

    public Hero getClosestFriendlyPlayer(Hero h, double radius) {
        double rSquared = radius * radius;
        double curDist;
        Hero closest = null;
        if (h.hasParty()) {
            for (Hero phero : h.getParty().getMembers()) {
                if (h.getPlayer().getWorld().equals(phero.getPlayer().getWorld())) {
                    curDist = h.getPlayer().getLocation().distanceSquared(phero.getPlayer().getLocation());
                    if (!(phero.equals(h)) && curDist < rSquared) {
                        rSquared = curDist;
                        closest = phero;
                    }
                }
            }
        }
        return closest;
    }
}
