package me.apteryx.mirrorforce;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import me.apteryx.mirrorforce.effects.MirrorForceShield;
import me.apteryx.mirrorforce.events.ForcerDamageEntityEvent;
import me.apteryx.mirrorforce.events.ForcerDamageEvent;
import me.apteryx.mirrorforce.events.ForcerPearlEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Projectile;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author apteryx
 * @time 1:07 PM
 * @since 11/19/2016
 */
public class MirrorForce extends ActiveSkill {

    private long shieldDuration, orbCastDuration, cooldown;
    private double shieldValue;
    private double reflectionMultiplier, radius;
    private static MirrorForce mirrorForce;

    public MirrorForce(Heroes plugin) {
        super(plugin, "MirrorForce");
        this.setIdentifiers("skill mirrorforce");
        this.setTypes(SkillType.SILENCEABLE);
        this.setUsage("/skill mirrorforce");
        this.setDescription("Gain a %shield-value% shield for the next %shield-duration% seconds. You may cast again within %orb-cast-duration% seconds to launch an orb dealing %reflect-multiplier% percent of the damage absorbed by your shield to enemies within %radius% blocks of the impact as magic damage.");
        Bukkit.getPluginManager().registerEvents(new ForcerDamageEvent(), this.plugin);
        Bukkit.getPluginManager().registerEvents(new ForcerPearlEvent(), this.plugin);
        Bukkit.getPluginManager().registerEvents(new ForcerDamageEntityEvent(), this.plugin);
        if (mirrorForce == null) {
            mirrorForce = this;
        }
    }

    @Override
    public String getDescription(Hero hero) {
        shieldValue = SkillConfigManager.getUseSetting(hero, this, "shield-value", 300, false);
        shieldDuration = (long) SkillConfigManager.getUseSetting(hero, this, "shield-duration", 10000L, false);
        orbCastDuration = SkillConfigManager.getUseSetting(hero, this, "orb-cast-duration", 30000, false);
        reflectionMultiplier = SkillConfigManager.getUseSetting(hero, this, "reflect-multiplier", 0.6, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 3.0, false);

        return getDescription().replace("%shield-value%", shieldValue+"").replace("%shield-duration%", (shieldDuration / 1000)+"").replace("%orb-cast-duration%", (orbCastDuration / 1000)+"").replace("%reflect-multiplier%", reflectionMultiplier * 100+"").replace("%radius%", radius+"");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("shield-value", 300);
        node.set("shield-duration", 10000L);
        node.set("orb-cast-duration", 30000L);
        node.set("reflect-multiplier", 0.6);
        node.set("radius", 3.0);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        shieldValue = SkillConfigManager.getUseSetting(hero, this, "shield-value", 300, false);
        shieldDuration = (long) SkillConfigManager.getUseSetting(hero, this, "shield-duration", 10000L, false);
        orbCastDuration = SkillConfigManager.getUseSetting(hero, this, "orb-cast-duration", 30000, false);
        reflectionMultiplier = SkillConfigManager.getUseSetting(hero, this, "reflect-multiplier", 0.6, false);
        cooldown = SkillConfigManager.getUseSetting(hero, this, "cooldown", 10000, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 3.0, false);
        if (!hero.hasEffect("MirrorForceShield")) {
            if (hero.hasEffect("MirrorForceOrb")) {
                Projectile pearl = hero.getPlayer().launchProjectile(EnderPearl.class);
                pearl.setMetadata("MirrorForceOrb", new FixedMetadataValue(this.plugin, hero));
                hero.removeEffect(hero.getEffect("MirrorForceOrb"));
                return SkillResult.NORMAL;
            }
            hero.addEffect(new MirrorForceShield(this, "MirrorForceShield", hero.getPlayer(), 20L, shieldDuration, shieldValue));
            broadcastExecuteText(hero);
            return SkillResult.CANCELLED;

        }
        return SkillResult.FAIL;
    }

    public static MirrorForce getInstance() {
        return mirrorForce;
    }

    public double getRadius() {
        return radius;
    }

    public double getShieldValue() {
        return shieldValue;
    }

    public double getReflectionMult() {
        return reflectionMultiplier;
    }

    public long getShieldDuration() {
        return shieldDuration;
    }

    public long getOrbCastDuration() {
        return orbCastDuration;
    }

    public long getCooldown() { return cooldown; }

}