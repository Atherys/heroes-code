package me.apteryx.mirrorforce.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import me.apteryx.mirrorforce.MirrorForce;
import org.bukkit.entity.Player;

/**
 * @author apteryx
 * @time 9:24 PM
 * @since 12/11/2016
 */
public class MirrorForceOrb extends ExpirableEffect {


    public MirrorForceOrb(Skill skill, String name, Player applier, long duration) {
        super(skill, name, applier, duration);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (this.isExpired()) {
            hero.getPlayer().sendMessage("\2477Your orb damage has dissipated.");
            hero.setCooldown("MirrorForce", System.currentTimeMillis() + MirrorForce.getInstance().getCooldown());
            if (MirrorForceShield.getDamageMap().containsKey(hero.getPlayer().getUniqueId())) {
                MirrorForceShield.getDamageMap().remove(hero.getPlayer().getUniqueId());
            }
            return;
        }
        hero.setCooldown("MirrorForce", System.currentTimeMillis() + MirrorForce.getInstance().getCooldown());
        broadcast(hero.getPlayer().getLocation(), "$1 has launched a charged orb!", hero.getName());
    }
}