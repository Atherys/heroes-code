package me.apteryx.mirrorforce.effects;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import me.apteryx.mirrorforce.MirrorForce;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author apteryx
 * @time 2:52 PM
 * @since 11/23/2016
 */
public class MirrorForceShield extends PeriodicExpirableEffect {

    private static Map<UUID, Double> damageMap = new HashMap<>();

    private double health;
    private double damageBlocked = 0.0;
    private double maxHealth;

    public MirrorForceShield(Skill skill, String name, Player applier, long period, long duration, double health) {
        super(skill, name, applier, period, duration);
        this.health = health;
        this.maxHealth = health;
        Bukkit.getLogger().info("Health: " + health);
    }

    public double getHealth() {
        return health;
    }

    public void setDamageBlocked(double damage) {
        this.damageBlocked = damage;
    }

    public double getDamageBlocked() {
        return damageBlocked;
    }

    public double getMaxHealth() {
        return maxHealth;
    }

    public void applyDamage(double damage, Player player) {
        health -= damage;
        damageBlocked +=damage;
        player.sendMessage(String.format("\2477Your shield sustained \247F%s\2477 damage.", (int)damageBlocked));
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().sendMessage("\2477You have gained a temporary shield.");
        hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_GUARDIAN_DEATH, 1.5F, 1.5F);
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (this.isExpired()) {
            hero.getPlayer().sendMessage("\2477Your shield has expired.");
            hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.BLOCK_GLASS_BREAK, 1.0F, 1.0F);
            hero.addEffect(new MirrorForceOrb(MirrorForce.getInstance(), "MirrorForceOrb", hero.getPlayer(), MirrorForce.getInstance().getOrbCastDuration()));
            if (damageBlocked > 0) {
                Bukkit.getLogger().info("MirrorForceDamage: " + damageBlocked);
                damageMap.put(hero.getPlayer().getUniqueId(), damageBlocked);
                damageBlocked = 0.0;
            }
            return;
        }
        hero.getPlayer().sendMessage("\2477Your shield has shattered.");
        hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.BLOCK_GLASS_BREAK, 1.0F, 1.0F);
        hero.addEffect(new MirrorForceOrb(MirrorForce.getInstance(), "MirrorForceOrb", hero.getPlayer(), MirrorForce.getInstance().getOrbCastDuration()));
        damageMap.put(hero.getPlayer().getUniqueId(), damageBlocked);
        damageBlocked = 0.0;
    }

    @Override
    public void tickMonster(Monster monster) {

    }

    @Override
    public void tickHero(Hero hero) {
        for(double[] coords : Lib.playerParticleCoords) {
            Location location = hero.getPlayer().getLocation();
            location.add(coords[0], coords[1], coords[2]);
            location.getWorld().spawnParticle(Particle.REDSTONE, location, 1, 255, 196, 44);
        }
    }

    public static Map<UUID, Double> getDamageMap() {
        return damageMap;
    }
}