package me.apteryx.mirrorforce.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * @author apteryx
 * @time 3:58 PM
 * @since 12/12/2016
 */
public class ForcerDamageEntityEvent implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void forcerDamagedByEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("MirrorForce")) {
                if (event.getDamager() instanceof Player) {
                    if (hero.getParty() != null) {
                        if (hero.getParty().isPartyMember((Player) event.getDamager())) {
                            event.setCancelled(true);
                            event.setDamage(0.0D);
                            Bukkit.getLogger().info("Is party member.");
                        }
                    }
                }
            }
        }
    }
}