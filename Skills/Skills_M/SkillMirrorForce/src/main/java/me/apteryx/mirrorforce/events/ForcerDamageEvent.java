package me.apteryx.mirrorforce.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import me.apteryx.mirrorforce.effects.MirrorForceShield;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author apteryx
 * @time 5:59 PM
 * @since 12/11/2016
 */
public class ForcerDamageEvent implements Listener {

    private boolean hasShattered;

    /**
     * Note:
     * Strange behaviour when anything lower than monitor. Seems to only pass vanilla damage.
     */

    @EventHandler(priority = EventPriority.MONITOR)
    public void onForcerDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("MirrorForceShield")) {
                hasShattered = false;
                MirrorForceShield mirrorForceShield = (MirrorForceShield) hero.getEffect("MirrorForceShield");
                if (mirrorForceShield != null) {
                    event.setCancelled(true);
                    if (event.getDamage() > 0) {
                        if (mirrorForceShield.getDamageBlocked() >= mirrorForceShield.getMaxHealth() || event.getDamage() >= mirrorForceShield.getMaxHealth() || event.getDamage() + mirrorForceShield.getDamageBlocked() >= mirrorForceShield.getMaxHealth() && !hasShattered) {
                            hero.removeEffect(mirrorForceShield);
                            hasShattered = true;
                            return;
                        }
                        mirrorForceShield.applyDamage(event.getDamage(), hero.getPlayer());
                    }
                }
            }
        }
    }
}