package me.apteryx.mirrorforce.events;

import com.herocraftonline.heroes.characters.Hero;
import me.apteryx.mirrorforce.MirrorForce;
import me.apteryx.mirrorforce.effects.MirrorForceShield;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

/**
 * @author apteryx
 * @time 11:15 PM
 * @since 12/11/2016
 */
public class ForcerPearlEvent implements Listener {

    @EventHandler
    public void onLand(ProjectileHitEvent event) {
        if (event.getEntity() instanceof EnderPearl) {
            if (event.getEntity().hasMetadata("MirrorForceOrb")) {
                Hero hero = (Hero) event.getEntity().getMetadata("MirrorForceOrb").get(0).value();
                event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 1.0F);
                event.getEntity().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, event.getEntity().getLocation(), 2);
                if (MirrorForceShield.getDamageMap().containsKey(hero.getPlayer().getUniqueId())) {
                    double damage = MirrorForceShield.getDamageMap().get(hero.getPlayer().getUniqueId()) * MirrorForce.getInstance().getReflectionMult();
                    MirrorForceShield.getDamageMap().remove(hero.getPlayer().getUniqueId());
                    for (Entity entity : event.getEntity().getNearbyEntities(MirrorForce.getInstance().getRadius(), MirrorForce.getInstance().getRadius(), MirrorForce.getInstance().getRadius())) {
                        if (entity instanceof LivingEntity) {
                            if (entity != hero.getPlayer()) {
                                if (entity instanceof Player) {
                                    if (hero.getParty() != null) {
                                        if (hero.getParty().isPartyMember((Player) entity)) {
                                            continue;
                                        }
                                    }
                                }
                                MirrorForce.getInstance().damageEntity((LivingEntity) entity, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                            }
                        }
                    }
                }
            }
        }
    }

}