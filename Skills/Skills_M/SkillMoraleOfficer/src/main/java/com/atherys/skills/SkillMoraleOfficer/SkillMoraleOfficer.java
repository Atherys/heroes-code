package com.atherys.skills.SkillMoraleOfficer;

import com.atherys.effects.OfficerDeployedEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroLeavePartyEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillMoraleOfficer extends TargettedSkill {
    private String applyText;
    private String expireText;
    public SkillMoraleOfficer(Heroes plugin) {
        super(plugin, "MoraleOfficer");
        setDescription("Targeted\nDesignate a target ally or yourself as the MoraleOfficer.");
        setArgumentRange(0, 1);
        setIdentifiers("skill moraleofficer");
        setUsage("/skill moraleofficer");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new PartyLeaveListener(), plugin);
    }
    @Override
    public String getDescription(Hero hero) {
        return  getDescription() + " " + Lib.getSkillCostStats(hero, this) ;
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        return node;
    }
    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 is now the MoraleOfficer!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 is no longer the MoraleOfficer!");
    }
    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) { //Target is not a player
            return SkillResult.INVALID_TARGET;
        }
        if (hero.hasParty()) {
            if (!(hero.getParty().isPartyMember((Player) target))) { //Target is not a party member
                return SkillResult.INVALID_TARGET;
            }
        } else if (!(target.equals(player))) { //Target != yourself IF you don't have a party
            return SkillResult.INVALID_TARGET;
        }
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        if(targetHero.hasEffect("CommanderEffect") || targetHero.hasEffect("OfficerEffect")) { //If the target already has an effect
            String message = (target.equals(player)) ? "A tactician has already assigned you a role!" : "A tactician has already assigned your target a role!";
            Messaging.send(player, message);
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
	    if (targetHero.hasEffect("CommanderEffect")){
		    targetHero.removeEffect(targetHero.getEffect("MoraleOfficer"));
	    }
	    if (hero.hasEffect("CommanderEffect")){
		    hero.removeEffect(hero.getEffect("MoraleOfficer"));
	    }
        if(hero.hasEffect("OfficerDeployedEffect")) { //hero had previously assigned a morale officer, remove previous effects
            OfficerDeployedEffect oldDeployEffect = (OfficerDeployedEffect) hero.getEffect("OfficerDeployedEffect");
            Hero oldOfficer = oldDeployEffect.getOfficer();
            OfficerEffect oldOfficerEffect = (OfficerEffect) oldOfficer.getEffect("OfficerEffect");
            hero.removeEffect(oldDeployEffect);
            oldOfficer.removeEffect(oldOfficerEffect);
        }
        //adding new effects
        hero.addEffect(new OfficerDeployedEffect(this, targetHero));
        targetHero.addEffect(new OfficerEffect(this, hero, applyText, expireText));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
    public class OfficerEffect extends Effect {
        private Hero tactician;
        private final String applyText;
        private final String expireText;
        public OfficerEffect(Skill skill, Hero tactician, String applyText, String expireText) {
            super(skill, "OfficerEffect");
            this.applyText = applyText;
            this.expireText = expireText;
            this.tactician = tactician;
            this.types.add(EffectType.BENEFICIAL);
        }
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
        public Hero getTactician(){
            return tactician;
        }
    }
    public class PartyLeaveListener implements Listener {
        public PartyLeaveListener(){}
        @EventHandler(ignoreCancelled = true)
        public void onPartyLeave(HeroLeavePartyEvent event) {
            Hero leftHero = event.getHero();
            Player leftPlayer = leftHero.getPlayer();
            if(!leftPlayer.isOnline()||event.getReason().equals(HeroLeavePartyEvent.LeavePartyReason.DISCONNECT)) return;
            else if(leftHero.hasEffect("OfficerDeployedEffect")){
                OfficerDeployedEffect oldDeployEffect = (OfficerDeployedEffect)leftHero.getEffect("OfficerDeployedEffect");
                Hero oldOfficer = oldDeployEffect.getOfficer();
                if(oldOfficer.equals(leftHero)) return;
                OfficerEffect oldOfficerEffect = (OfficerEffect)oldOfficer.getEffect("OfficerEffect");
                leftHero.removeEffect(oldDeployEffect);
                oldOfficer.removeEffect(oldOfficerEffect);
            } else if (leftHero.hasEffect("OfficerEffect")){
                OfficerEffect oldOfficerEffect = (OfficerEffect)leftHero.getEffect("OfficerEffect");
                Hero oldTactician = oldOfficerEffect.getTactician();
                OfficerDeployedEffect oldDeployEffect = (OfficerDeployedEffect)oldTactician.getEffect("OfficerDeployedEffect");
                leftHero.removeEffect(oldOfficerEffect);
                oldTactician.removeEffect(oldDeployEffect);
            }
        }
    }
}