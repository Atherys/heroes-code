package com.atherys.skills.SkillMultibolt;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillMultibolt extends TargettedSkill {
	private String applyText;
	private String expireText;

	public SkillMultibolt(Heroes plugin) {
		super(plugin, "Multibolt");
		setDescription("Bolt of electricity that strikes your target for $1 damage and amplifies for $3 damage upon re-usage on same target within $2 seconds of the cooldown ending. Max-Stacks: 5");
		setUsage("/skill multibolt");
		setArgumentRange(0, 0);
		setIdentifiers("skill multibolt");
		setTypes(SkillType.ABILITY_PROPERTY_LIGHTNING, SkillType.DAMAGING, SkillType.SILENCEABLE);
	}

	@Override
	public void init() {
		applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%caster% applied %stack% stacks of Multibolt on %target%!").replace("%stack%", "$1").replace("%target%", "$2").replace("%caster%", "$3");
		expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%caster%'s Multibolt stacks expired from %victim%!").replace("%caster%", "$1").replace("%victim%", "$2");
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), 10000L);
		node.set(SkillSetting.DAMAGE.node(), 14);
		node.set("damage-increment", 10);
		node.set(SkillSetting.EXPIRE_TEXT.node(), "%caster%'s Multibolt stacks expired from %victim%!");
		return node;
	}

	@Override
	public SkillResult use(Hero hero, LivingEntity target, String[] args) {
		Player player = hero.getPlayer();
		double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE.node(), 20, false);
		double incrementer = SkillConfigManager.getUseSetting(hero, this, "damage-increment", 15, false);
		long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 30000, false);
		if (!target.equals(hero.getPlayer())) {
			if (damageCheck(hero.getPlayer(), target)) {
				CharacterTemplate targetCharacter = plugin.getCharacterManager().getCharacter(target);
				int stacks = 0;
				if (targetCharacter.hasEffect("BoltStackEffect" + hero.getName())) {
					stacks = ((BoltStackEffect) targetCharacter.getEffect("BoltStackEffect" + hero.getName())).getStackIdentifier();
					damage += incrementer * stacks;
				}
				if (target instanceof Player) {
					if (targetCharacter.hasEffect("Grounded") && ((LivingEntity) target).isOnGround()) {
						damage *= (1 - SkillConfigManager.getUseSetting((Hero) targetCharacter, plugin.getSkillManager().getSkill("Grounded"), "damage-reduction", 0.5, false));
					}
				}
				stacks = stacks < 5 ? stacks + 1 : stacks;
				addSpellTarget(target, hero);
				target.getWorld().strikeLightningEffect(target.getLocation());
				damageEntity(target, player, damage, DamageCause.MAGIC);
				targetCharacter.addEffect(new BoltStackEffect(this, duration, stacks, hero));
				//broadcast(player.getLocation(), "$1 applies $2" + (stacks == 1 ? " stack of " : " stacks of ") + "$3 to $4!", player.getDisplayName(), stacks, "Multibolt", target.getName());
				return SkillResult.NORMAL;
			}
			return SkillResult.INVALID_TARGET;
		}
		return SkillResult.CANCELLED;
	}

	public class BoltStackEffect extends ExpirableEffect {
		private final String caster;
		private final Player casterPlayer;
		private int stackIdentifier;

		public BoltStackEffect(Skill skill, long duration, int stackIdentifier, Hero caster) {
			super(skill, "BoltStackEffect" + caster.getName(),caster.getPlayer(), duration);
			this.stackIdentifier = stackIdentifier;
			this.caster = caster.getName();
			this.casterPlayer = caster.getPlayer();
			this.types.add(EffectType.HARMFUL);
			this.types.add(EffectType.DISPELLABLE);
			this.types.add(EffectType.LIGHTNING);
			this.types.add(EffectType.MAGIC);
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			Messaging.send(hero.getPlayer(), applyText, getStackIdentifier(), hero.getPlayer().getDisplayName(), caster);
			Messaging.send(casterPlayer, applyText, getStackIdentifier(), hero.getPlayer().getDisplayName(), caster);
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			if (this.isExpired()) {
				Messaging.send(hero.getPlayer(), expireText, hero.getPlayer().getDisplayName(), caster);
				Messaging.send(casterPlayer, expireText, hero.getPlayer().getDisplayName(), caster);
			}
		}

		public int getStackIdentifier() {
			return stackIdentifier;
		}
	}
}