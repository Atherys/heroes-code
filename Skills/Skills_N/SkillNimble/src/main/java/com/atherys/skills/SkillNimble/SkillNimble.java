package com.atherys.skills.SkillNimble;

/**
 * Created by arara_000 on 10/26/2015.
 */

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SkillNimble extends PassiveSkill {

    public SkillNimble(Heroes plugin) {
        super(plugin, "Nimble");
        setDescription("Passive $1% chance to dodge enemy attacks every $2 seconds.");
        setTypes(SkillType.MOVEMENT_PREVENTION_COUNTERING, SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        double chance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE.node(), 0.2, false);
        long cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 5000, false);
        return getDescription().replace("$1", chance + "").replace("$2", ((int) cooldown / 1000) + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.CHANCE.node(), 1);
        node.set(SkillSetting.COOLDOWN.node(), 5000);
        return node;
    }

    public class SkillHeroListener implements Listener {
        private Skill skill;

        public SkillHeroListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(WeaponDamageEvent event) {
            if ((!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("Nimble")) {
                if (hero.getCooldown("Nimble") == null || hero.getCooldown("Nimble") <= System.currentTimeMillis()) {
                    double chance = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.CHANCE.node(), 1, false);
                    if (Math.random() <= chance) {
                        if (event.getAttackerEntity() instanceof LivingEntity) {
                            if (damageCheck(player, (LivingEntity) event.getAttackerEntity())){
                                event.setDamage(0D);
                                event.setCancelled(true);
                                long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN.node(), 5000, false);
                                hero.setCooldown("Nimble", cooldown + System.currentTimeMillis());
                                broadcast(player.getLocation(), "$1 nimbly dodged an attack!", player.getDisplayName());
                            }
                        }
                    }
                }
            }
        }
    }
}
