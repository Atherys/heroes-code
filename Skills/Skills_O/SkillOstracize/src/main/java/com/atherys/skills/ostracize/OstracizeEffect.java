/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.ostracize;

import com.atherys.effects.StunEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.InvulnerabilityEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class OstracizeEffect extends PeriodicExpirableEffect {

    private final Player effected;

    public OstracizeEffect(Skill skill, Player effected, Player caster, long duration) {
        super(skill, "OstracizeEffect", caster, 10, duration);
        this.effected = effected;
        this.types.add(EffectType.STUN);
        this.types.add(EffectType.INVULNERABILITY);
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (this.isExpired()) {
            hero.removeEffect(hero.getEffect("Invuln"));
            hero.removeEffect(hero.getEffect("Stun"));
        }
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.addEffect(new StunEffect(skill, getDuration(), hero));
        hero.addEffect(new InvulnerabilityEffect(skill, effected, getDuration()));
    }

    @Override
    public void tickMonster(Monster monster) {

    }

    @Override
    public void tickHero(Hero hero) {
        for(double[] locations : Lib.playerParticleCoords) {
            Location location = hero.getPlayer().getLocation();
            location.add(locations[0], locations[1], locations[2]);
            hero.getPlayer().getWorld().spigot().playEffect(location, Effect.HAPPY_VILLAGER);
        }
    }
}
