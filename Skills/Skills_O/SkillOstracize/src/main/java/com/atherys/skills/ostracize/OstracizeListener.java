/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.skills.ostracize;

import com.atherys.core.Core;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import static com.herocraftonline.heroes.characters.skill.Skill.damageCheck;

public class OstracizeListener implements Listener {

    private Skill skill;

    private int hitCount = 0;
    private long firstHit = 0;
    private Player lastTarget = null;

    public OstracizeListener(Skill skill, Heroes plugin) {
        this.skill = skill;
        Bukkit.getPluginManager().registerEvents(this, plugin); // Self-Register when Constructor is Called
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityDamage(WeaponDamageEvent event) {
        boolean nonPlayerTarget = !(event.getEntity() instanceof Player);
        boolean nonPlayerSource = !(event.getAttackerEntity() instanceof Player);
        boolean noDamage = (event.getDamage() == 0);
        if (nonPlayerTarget || nonPlayerSource || noDamage) return;
        Hero hero = Core.getHeroes().getCharacterManager().getHero((Player) event.getAttackerEntity());
        if (!hero.getHeroClass().hasSkill("Ostracize")) return;
        Hero target = Core.getHeroes().getCharacterManager().getHero((Player) event.getEntity());
        handleHitCounter(hero, target.getPlayer());
        if (shouldOstricize(hero)) ostracize(hero, target);
    }

    private boolean isOnCooldown(Hero hero) {
        if (hero.getCooldown("Ostracize") == null) hero.setCooldown("Ostracize", 0);
        return (hero.getCooldown("Ostracize") > System.currentTimeMillis());
    }

    private void handleHitCounter(Hero hero, Player target) {
        int expire_dur = SkillConfigManager.getUseSetting(hero, skill, "expire-duration", 10000, false);
        if (firstHit < (System.currentTimeMillis() - expire_dur) || !target.equals(lastTarget)) {
            lastTarget = target;
            firstHit = System.currentTimeMillis();
            hitCount = 1;
        } else {
            hitCount++;
        }
    }

    private boolean shouldOstricize(Hero hero) {
        int num_hits = SkillConfigManager.getUseSetting(hero, skill, "number-of-hits", 4, false);
        boolean hasEff = hero.hasEffect("Ostracize");
        boolean numHits = (hitCount >= num_hits);
        boolean cooldown = isOnCooldown(hero);
        boolean dmgCheck = damageCheck(hero.getPlayer(), (LivingEntity) lastTarget);
        boolean ostricize = (hasEff && numHits && !cooldown && dmgCheck);
        return ostricize;
    }

    private void ostracize(Hero attacker, Hero target) {
        hitCount = 0;
        firstHit = 0;
        lastTarget = null;
        long cooldown = SkillConfigManager.getUseSetting(attacker, skill, SkillSetting.COOLDOWN, 3000, false);
        long snare_dur = SkillConfigManager.getUseSetting(attacker, skill, "snare-duration", 4000, false);
        attacker.setCooldown("Ostracize", cooldown + System.currentTimeMillis());
        target.addEffect(new OstracizeEffect(skill, target.getPlayer(), attacker.getPlayer(), snare_dur));
        skill.broadcast(
                attacker.getPlayer().getLocation(),
                "    $1 used $2 on $3",
                attacker.getName(),
                "Ostracize",
                target.getName()
        );
    }

}
