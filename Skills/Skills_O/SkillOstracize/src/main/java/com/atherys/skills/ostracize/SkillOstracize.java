/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.ostracize;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SkillOstracize extends PassiveSkill {

    public SkillOstracize(Heroes plugin) {
        super(plugin, "Ostracize");
        setArgumentRange(0, 0);
        setDescription("Hitting an enemy consecutively $1 times Snares them for $2 seconds. Hit tally resets after $3 seconds.");
        new OstracizeListener(this, plugin);
    }

    public String getDescription(Hero hero) {
        int num_hits = SkillConfigManager.getUseSetting(hero, this, "number-of-hits", 4, false);
        int snare_dur = SkillConfigManager.getUseSetting(hero, this, "snare-duration", 4000, false);
        int expire_dur = SkillConfigManager.getUseSetting(hero, this, "expire-duration", 10000, false);
        return getDescription()
                .replace("$1", String.valueOf(num_hits))
                .replace("$2", String.valueOf(snare_dur / 1000))
                .replace("$3", String.valueOf(expire_dur / 1000));
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("number-of-hits", 4);
        node.set("snare-duration", 4000);
        node.set("expire-duration", 10000);
        node.set(SkillSetting.COOLDOWN.node(), 3000);
        return node;
    }

}