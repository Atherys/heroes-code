package com.atherys.skills.SkillOvergrowth;

import com.atherys.effects.SilenceEffect;
import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillOvergrowth extends ActiveSkill {
    public SkillOvergrowth(Heroes plugin) {
        super(plugin, "Overgrowth");
        setDescription("Silence and Slow all enemies around you. Bloom: instant aoe heal + Fast acting HoT");
        setUsage("/skill Overgrowth");
        setArgumentRange(0, 0);
        setIdentifiers("skill Overgrowth");
        setNotes("Note: Using this skill also puts Shell and WildGrowth on a 10 second cooldown.");
        setTypes(SkillType.SILENCEABLE, SkillType.DEBUFFING, SkillType.HEALING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("instant-heal", 25);
        node.set("heal-tick", 5);
        node.set("heal-duration", 3000);
        node.set("heal-period", 1000);
        node.set("heal-radius", 10);
        node.set("debuff-radius", 5);
        node.set("debuff-duration", 3000);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        if (!hero.hasEffect("BloomEffect")) {
            int r = SkillConfigManager.getUseSetting(hero, this, "debuff-radius", 5, false);
            long duration = SkillConfigManager.getUseSetting(hero, this, "debuff-duration", 3000, false);
            for (Entity e : player.getNearbyEntities(r, r, r)) {
                if (!(e instanceof Player)) {
                    continue;
                }
                if (damageCheck(((Player) e), (LivingEntity) hero.getPlayer())) {
                    Hero thero = plugin.getCharacterManager().getHero((Player) e);
                    thero.addEffect(new SilenceEffect(this, duration, true,hero));
                    Lib.cancelDelayedSkill(thero);
                    thero.addEffect(new SlowEffect(this, "OvergrowthSlow", duration, 2, false, "", "", hero));
                }
            }
        } else {
            double amount = SkillConfigManager.getUseSetting(hero, this, "instant-heal", 25, false);
            int r = SkillConfigManager.getUseSetting(hero, this, "heal-radius", 10, false);
            long period = (long) SkillConfigManager.getUseSetting(hero, this, "heal-period", 1000, true);
            long duration = (long) SkillConfigManager.getUseSetting(hero, this, "heal-duration", 3000L, true);
            double heal = SkillConfigManager.getUseSetting(hero, this, "heal-tick", 5, true);
            if (hero.hasParty()) {
                for (Hero partyhero : hero.getParty().getMembers()) {
                    if (player.getWorld().equals(partyhero.getPlayer().getWorld()) && partyhero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= r * r) {
                        Lib.healHero(partyhero, amount, this, hero);
                        partyhero.addEffect(new OvergrowthHealEffect(this, period, duration, heal, hero.getPlayer()));
                    }
                }
            } else {
                Lib.healHero(hero, amount, this, hero);
                hero.addEffect(new OvergrowthHealEffect(this, period, duration, heal, hero.getPlayer()));
            }
        }
        long time = System.currentTimeMillis();
        if (hero.getCooldown("WildGrowth") == null || (hero.getCooldown("WildGrowth") - time) < 10000) {
            hero.setCooldown("WildGrowth", time + 10000);
        }
        if (hero.getCooldown("Shell") == null || (hero.getCooldown("Shell") - time) < 10000) {
            hero.setCooldown("Shell", time + 10000);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class OvergrowthHealEffect extends PeriodicHealEffect {
        public OvergrowthHealEffect(Skill skill, long period, long duration, double heal, Player player) {
            super(skill, "OvergrowthHeal",player, period, duration, heal );
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.HEALING);
            this.types.add(EffectType.MAGIC);
        }
    }
}