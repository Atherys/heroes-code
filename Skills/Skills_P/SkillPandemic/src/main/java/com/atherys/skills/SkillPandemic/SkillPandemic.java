package com.atherys.skills.SkillPandemic;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillPandemic extends ActiveSkill {
    public String applyText;
    public String expireText;

    public SkillPandemic(Heroes plugin) {
        super(plugin, "Pandemic");
        setDescription("Drains $2hp or to 1hp from everyone within $1 blocks over $3s.");
        setUsage("/skill pandemic");
        setArgumentRange(0, 0);
        setIdentifiers("skill pandemic");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE, SkillType.DEBUFFING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set("radius-increase", Integer.valueOf(0));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(16));
        node.set("damage-increase", Integer.valueOf(0));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(2000));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(20000));
        node.set("duration-increase", Integer.valueOf(0));
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% has caught %hero%s %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from %hero%s %skill%!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT.node(), "%target% has caught %hero%s %skill%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from %hero%s %skill%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "radius-increase", 0, false) * hero.getSkillLevel(this);
        radius = radius > 0 ? radius : 0;
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false) * hero.getSkillLevel(this) / 1000L;
        duration = duration > 0L ? duration : 0L;
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE.node(), 16, false) + SkillConfigManager.getUseSetting(hero, this, "damage-increase", 0, false) * hero.getSkillLevel(this);
        damage = damage > 0 ? damage : 0;
        String description = getDescription().replace("$1", radius + "").replace("$2", damage + "").replace("$3", duration + "");
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this);
        if (healthCost > 0) {
            description = description + " HP:" + healthCost;
        }
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (staminaCost > 0) {
            description = description + " FP:" + staminaCost;
        }
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description = description + " W:" + delay + "s";
        }
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description = description + " XP:" + exp;
        }
        return description;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE.node(), 16, false) + SkillConfigManager.getUseSetting(hero, this, "damage-increase", 0, false) * hero.getSkillLevel(this);
        damage = damage > 0 ? damage : 0;
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "radius-increase", 0, false) * hero.getSkillLevel(this);
        radius = radius > 0 ? radius : 0;
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false) * hero.getSkillLevel(this);
        duration = duration > 0L ? duration : 0L;
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 2000, false);
        PandemicEffect pe = new PandemicEffect(this, duration, period, damage, player);
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if (!(e instanceof LivingEntity)) {
                continue;
            }
            LivingEntity l = (LivingEntity) e;
            if (damageCheck(player, l)) {
                if (l instanceof Player) {
                    plugin.getCharacterManager().getHero((Player) l).addEffect(pe);
                } else {
                    plugin.getCharacterManager().getMonster(l).addEffect(pe);
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class PandemicEffect extends PeriodicDamageEffect {
        private final double damage;
        private final Player caster;

        public PandemicEffect(Skill skill, long duration, long period, double damage, Player caster) {
            super(skill, "Pandemic",caster, period, duration, damage );
            int numberOfTicks = (int) (duration / period);
            this.damage = (damage / numberOfTicks);
            this.caster = caster;
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISEASE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), applyText, caster.getDisplayName(), p.getDisplayName(), "Pandemic");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), expireText, caster.getDisplayName(), p.getDisplayName(), "Pandemic");
        }

        @Override
        public void tickHero(Hero hero) {
            if (damageCheck(hero.getPlayer(), (LivingEntity) caster)) {
                damageEntity(hero.getPlayer(), caster, damage, DamageCause.MAGIC);
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            if (damageCheck(caster, monster.getEntity()))
                damageEntity(monster.getEntity(), caster, damage, DamageCause.MAGIC);
        }
    }
}