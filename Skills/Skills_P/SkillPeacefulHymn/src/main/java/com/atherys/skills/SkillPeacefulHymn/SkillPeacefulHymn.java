package com.atherys.skills.SkillPeacefulHymn;

import com.atherys.effects.DamageReductionEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Jukebox;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillPeacefulHymn extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillPeacefulHymn(Heroes plugin) {
        super(plugin, "PeacefulHymn");
        setDescription("Places a Jukebox that will periodically reduce the damage of nearby enemies.");
        setArgumentRange(0, 0);
        setUsage("/skill PeacefulHymn");
        setIdentifiers("skill PeacefulHymn");
        setTypes(SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new PeacefulHymnListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 15000);
        node.set(SkillSetting.PERIOD.node(), 3000);
        node.set("damage-reduction", 4);
        node.set(SkillSetting.RADIUS.node(), 8);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired.");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block block = player.getTargetBlock(Util.transparentBlocks, distance);
        if (block.getType() == Material.AIR || block.getRelative(BlockFace.UP).getType() != Material.AIR) {
            Messaging.send(player, "You must target a block!");
            return SkillResult.CANCELLED;
        }
        block = block.getRelative(BlockFace.UP);
        block.setMetadata("PeacefulHymnJukebox", new FixedMetadataValue(plugin, true));
        block.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
        block.setType(Material.JUKEBOX);
        Jukebox jukebox = (Jukebox) block.getState();
        jukebox.setPlaying(Material.RECORD_8);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 15000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 3000, false);
        double damageReduction = SkillConfigManager.getUseSetting(hero, this, "damage-reduction", 4, false);
        int range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 8, false);
        hero.addEffect(new PeacefulHymnEffect(this, period, duration, damageReduction, range, jukebox,hero));
        return SkillResult.NORMAL;
    }

    public class PeacefulHymnEffect extends PeriodicExpirableEffect {
        private double damageReduction;
        private int range;
        private Jukebox jukebox;

        public PeacefulHymnEffect(Skill skill, long period, long duration, double damageReduction, int range, Jukebox jukebox,Hero caster) {
            super(skill, "PeacefulHymn",caster.getPlayer(), period, duration);
            this.damageReduction = damageReduction;
            this.range = range;
            this.jukebox = jukebox;
            types.add(EffectType.FORM);
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), applyText, new Object[]{hero.getName(), "PeacefulHymn"});
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Block block = jukebox.getBlock();
            Set<Block> blocks = new HashSet<>();
            blocks.add(block);
            jukebox.setPlaying(null);
            if (block.hasMetadata("PeacefulHymnJukebox")) {
                block.removeMetadata("PeacefulHymnJukebox", plugin);
            }
            Lib.removeBlocks(blocks);
            if (this.isExpired()) {
                broadcast(player.getLocation(), expireText, new Object[]{hero.getName(), "PeacefulHymn"});
            }
        }

        @Override
        public void tickHero(Hero hero) {
            for (Entity e : jukebox.getWorld().getNearbyEntities(jukebox.getLocation(), range, range, range)) {
                if (e instanceof LivingEntity) {
                    LivingEntity le = (LivingEntity)e;
                    if (damageCheck(hero.getPlayer(), le)) {
                        CharacterTemplate characterTemplate = plugin.getCharacterManager().getCharacter(le);
                        characterTemplate.addEffect(new DamageReductionEffect(skill, getPeriod(), damageReduction, "", "",hero));
                    }
                }
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }

    public class PeacefulHymnListener implements Listener {

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onPlayerInteract(PlayerInteractEvent event) {
            if ((event.getClickedBlock() == null) || (event.getClickedBlock().getType() != Material.JUKEBOX) || (event.getAction() != Action.RIGHT_CLICK_BLOCK) || (!event.getClickedBlock().hasMetadata("PeacefulHymnJukebox"))) {
                return;
            }
            event.setUseInteractedBlock(Event.Result.DENY);
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onBlockDispense(BlockDispenseEvent event) {
            if ((event.getBlock().getType() != Material.JUKEBOX)) {
                return;
            }
            if (event.getBlock().hasMetadata("PeacefulHymnJukebox")) {
                event.setCancelled(true);

            }
        }
    }
}