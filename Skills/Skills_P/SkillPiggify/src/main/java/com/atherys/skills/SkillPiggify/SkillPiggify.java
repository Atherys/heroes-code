package com.atherys.skills.SkillPiggify;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillPiggify extends TargettedSkill {

    public SkillPiggify(Heroes plugin) {
        super(plugin, "Piggify");
        setDescription("You force your target to ride a pig for $1 seconds.");
        setUsage("/skill piggify <target>");
        setArgumentRange(0, 0);
        setIdentifiers("skill piggify");
        setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {

        if (target instanceof Player) {
            Player tplayer = (Player)target;
            if (tplayer.equals(hero.getPlayer())) {
                return SkillResult.INVALID_TARGET;
            }else {
	            Hero enemy = this.plugin.getCharacterManager().getHero((Player) target);
	            if (hero.hasParty()){
		            if (hero.getParty().getMembers().contains(enemy)){
			            return SkillResult.INVALID_TARGET_NO_MSG;
		            }
	            }
	            Entity creature = target.getWorld().spawnEntity(target.getLocation(), EntityType.PIG);
	            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
	            CharacterTemplate characterTemplate = this.plugin.getCharacterManager().getCharacter(target);
	            characterTemplate.addEffect(new PigEffect(this, duration, creature,hero));
                Lib.cancelDelayedSkill((Hero) characterTemplate);
            }

        }
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class SkillEntityListener implements Listener {

        @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.getEntity() instanceof LivingEntity) {
	            LivingEntity livingEntity = (LivingEntity) event.getEntity();
	            CharacterTemplate character;
	            if (livingEntity.hasMetadata("PiggifyCreature")) {
		            character = (CharacterTemplate) event.getEntity().getMetadata("PiggifyCreature").get(0).value();
	            } else {
		            character = plugin.getCharacterManager().getCharacter(livingEntity);
	            }
	            if (character.hasEffect("Piggify")) {
		            if (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                        if (livingEntity instanceof Player && event instanceof EntityDamageByEntityEvent) {
                            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
                            if (subEvent.getDamager() instanceof LivingEntity && !damageCheck((Player)livingEntity,
                                    (LivingEntity)subEvent.getDamager())) {
                                event.setCancelled(true);
                                return;
                            }
                        }
			            character.removeEffect(character.getEffect("Piggify"));
		            } else {
			            event.setCancelled(true);
		            }
	            }
            }
        }
    }

    public class PigEffect extends ExpirableEffect {
        private final Pig creature;

        public PigEffect(Skill skill, long duration, Entity creature,Hero caster) {
            super(skill, "Piggify",caster.getPlayer(), duration);
            this.creature = (Pig) creature;
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISABLE);
            this.types.add(EffectType.MAGIC);
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
	        creature.setSaddle(true);
            this.creature.setPassenger(monster.getEntity());
            creature.setMetadata("PiggifyCreature", new FixedMetadataValue(plugin, monster));
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
	        creature.setSaddle(true);
            this.creature.setPassenger(player);
            creature.setMetadata("PiggifyCreature", new FixedMetadataValue(plugin, hero));
        }

        public void removeFromMonster(Monster rider) {
            super.removeFromMonster(rider);
            this.creature.remove();
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            this.creature.remove();
        }
    }
}
