package com.atherys.skills.SkillPlague;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillPlague extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillPlague(Heroes plugin) {
        super(plugin, "Plague");
        setDescription("Afflicts the nearest enemy with plague - dealing $1 damage over $2 seconds. Anyone nearby a person afflicted with plague will contract it as well.");
        setUsage("/skill Plague");
        setArgumentRange(0, 0);
        setIdentifiers("skill Plague");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        LivingEntity target = getClosestPlayer(hero.getPlayer(), r);
        if (target == null) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "No nearby targets.");
            return SkillResult.CANCELLED;
        }
        CharacterTemplate ct = plugin.getCharacterManager().getCharacter(target);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000L, false);
        double damagetonear = SkillConfigManager.getUseSetting(hero, this, "damage-to-nearby-targets", 10, false);
        ct.addEffect(new PlagueEffect(this, period, duration, damage, hero.getPlayer(), r, damagetonear));
        return SkillResult.NORMAL;
    }

    public LivingEntity getClosestPlayer(Player player, double radius) {
        double minimalDistance = radius * radius;
        LivingEntity closest = null;
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if ((e instanceof LivingEntity)) {
                if (damageCheck(player, (LivingEntity) e)) {
                    double curDist = player.getLocation().distanceSquared(e.getLocation());
                    if (curDist < minimalDistance) {
                        minimalDistance = curDist;
                        closest = (LivingEntity) e;
                    }
                }
            }
        }
        return closest;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set("damage-to-nearby-targets", 10);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% was infected with %hero%'s %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired from %target%.");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        return getDescription().replace("$2", "" + duration / 1000).replace("$1", "" + damage);
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% was infected with %hero%'s %skill%.").replace("%target%", "$1").replace("%skill%", "$2").replace("%hero%", "$3");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired from %target%.").replace("%target%", "$1").replace("%skill%", "$2").replace("%hero%", "$3");
    }

    public class PlagueEffect extends PeriodicExpirableEffect {
        private final Player caster;
        private final double damage;
        private final int r;
        private final double damagetonear;

        public PlagueEffect(Skill skill, long period, long duration, double damage, Player caster, int r, double damagetonear) {
            super(skill, "PlagueEffect",caster, period, duration);
            this.caster = caster;
            this.damage = damage;
            this.r = r;
            this.damagetonear = damagetonear;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.HARMFUL);
            types.add(EffectType.DISEASE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Plague", caster.getName());
        }

        @Override
        public void tickHero(Hero hero) {
            if (damage > 0) {
                if (damageCheck(hero.getPlayer(), (LivingEntity) caster) || ((caster.equals(hero.getPlayer())) && (!plugin.getCharacterManager().getHero(caster).hasEffectType(EffectType.INVULNERABILITY)))) {
                    damageEntity(hero.getPlayer(), caster, damage, EntityDamageEvent.DamageCause.CUSTOM, false);
                }
            }
            for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
                if (e instanceof LivingEntity) {
                    if (damageCheck(caster, (LivingEntity) e)) {
                        damageEntity((LivingEntity) e, caster, damagetonear, DamageCause.CONTACT);
                    }
                }
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Plague", caster.getName());
        }

        @Override
        public void removeFromMonster(Monster m) {
            super.removeFromMonster(m);
            broadcast(m.getEntity().getLocation(), expiretext, m.getName(), "Plague", caster.getName());
        }

        @Override
        public void applyToMonster(Monster m) {
            super.applyToMonster(m);
            broadcast(m.getEntity().getLocation(), applytext, m.getName(), "Plague", caster.getName());
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //damageEntity(mnstr.getEntity(), caster, damage, DamageCause.CUSTOM);
            damageEntity(mnstr.getEntity(),caster,damage);
            for (Entity e : mnstr.getEntity().getNearbyEntities(r, r, r)) {
                if (e instanceof LivingEntity) {
                    if (damageCheck(caster, (LivingEntity) e)) {
                        damageEntity((LivingEntity) e, caster, damagetonear, DamageCause.CONTACT);
                    }
                }
            }
        }
    }
}
