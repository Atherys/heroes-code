package com.atherys.skills.SkillPoison;

import com.atherys.effects.BleedPeriodicDamageEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillPoison extends TargettedSkill {
    private String expireText;

    public SkillPoison(Heroes plugin) {
        super(plugin, "Poison");
        setDescription("You poison your target dealing $1 damage over $2 seconds.");
        setUsage("/skill poison <target>");
        setArgumentRange(0, 0);
        setIdentifiers("skill poison");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(2000));
        node.set("tick-damage", Integer.valueOf(1));
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the poison!");
        return node;
    }

    public void init() {
        super.init();
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% has recovered from the poison!").replace("%target%", "$1");
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, true);
        double tickDamage = SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1, false);
        this.plugin.getCharacterManager().getCharacter(target).addEffect(new PoisonSkillEffect(this, period, duration, tickDamage, player));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1, false);
        return getDescription().replace("$1", damage * duration / period + "").replace("$2", duration / 1000 + "");
    }

    public class PoisonSkillEffect extends BleedPeriodicDamageEffect {
        public PoisonSkillEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "Poison",applier, period, duration, tickDamage);
            this.types.add(EffectType.POISON);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISPELLABLE);
            addMobEffect(19, (int) (duration / 1000L) * 20, 0, true);
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
            broadcast(monster.getEntity().getLocation(), SkillPoison.this.expireText, monster.getName().toLowerCase());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillPoison.this.expireText, player.getDisplayName());
        }
    }
}
