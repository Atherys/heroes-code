package com.atherys.skills.poisonarrow;

import com.atherys.skills.poisonarrow.effects.BleedPeriodicDamageEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class PoisonArrow extends PassiveSkill {
    private String applyText;
    private String expireText;

    public PoisonArrow(Heroes plugin) {
        super(plugin, "PoisonArrow");
        setDescription("Your arrows will poison their target dealing $1 damage over $2 seconds, each arrow will drain $3 mana.");
        setArgumentRange(0, 0);
        setTypes(SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillDamageListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.PERIOD.node(), 2000);
        node.set("mana-per-shot", 1);
        node.set("tick-damage", 2);
        node.set(SkillSetting.USE_TEXT.node(), "%hero% imbues their arrows with poison!");
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% is poisoned!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the poison!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% is poisoned!").replace("%target%", "$1");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% has recovered from the poison!").replace("%target%", "$1");
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1, false);
        int mana = SkillConfigManager.getUseSetting(hero, this, "mana-per-shot", 1, true);
        damage = damage * duration / period;
        return getDescription().replace("$1", damage + "").replace("$2", duration / 1000 + "").replace("$3", mana + "");
    }

    public class SkillDamageListener implements Listener {
        private final Skill skill;

        public SkillDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.getCause().equals(EntityDamageEvent.DamageCause.POISON) && event.getEntity() instanceof Player) {
                if (event.getEntity().getMetadata("Poisoned").get(0).asBoolean()) {
                    event.setCancelled(true);
                }
            }
            if ((event.isCancelled()) || (!(event.getEntity() instanceof LivingEntity)) || (!(event instanceof EntityDamageByEntityEvent))) {
                return;
            }
            LivingEntity target = (LivingEntity) event.getEntity();
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            if (!(subEvent.getDamager() instanceof Arrow)) {
                return;
            }
            Arrow arrow = (Arrow) subEvent.getDamager();
            if (!(arrow.getShooter() instanceof Player)) {
                return;
            }
            Player player = (Player) arrow.getShooter();
            if (player.equals(target)) {
                return;
            }
            Hero hero = PoisonArrow.this.plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("PoisonArrow")) {
                long duration = SkillConfigManager.getUseSetting(hero, this.skill, "poison-duration", 10000, false);
                long period = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.PERIOD, 2000, true);
                double tickDamage = SkillConfigManager.getUseSetting(hero, this.skill, "tick-damage", 2, false);
                if (!PoisonArrow.this.plugin.getCharacterManager().getCharacter(target).hasEffect("ArrowPoison")) {
                    PoisonArrow.this.plugin.getCharacterManager().getCharacter(target).addEffect(new PoisonArrow.ArrowPoison(PoisonArrow.this, period, duration, tickDamage, player));
                }
            }
        }

        @EventHandler
        public void onEntityShootBow(EntityShootBowEvent event) {
            if ((event.isCancelled()) || (!(event.getEntity() instanceof Player)) || (!(event.getProjectile() instanceof Arrow))) {
                return;
            }
            Hero hero = PoisonArrow.this.plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("PoisonArrow")) {
                int mana = SkillConfigManager.getUseSetting(hero, this.skill, "mana-per-shot", 1, true);

                if (hero.getMana() < mana) {
                    hero.removeEffect(hero.getEffect("PoisonArrow"));
                }else{
                    hero.setMana(hero.getMana() - mana);
                }
            }
        }
    }

    public class ArrowPoison extends BleedPeriodicDamageEffect {

        public ArrowPoison(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "ArrowPoison",applier, period, duration, tickDamage);
            this.types.add(EffectType.POISON);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.HARMFUL);
            addMobEffect(19, (int) duration / 50, 1, false);
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
            broadcast(monster.getEntity().getLocation(), PoisonArrow.this.applyText, monster.getName().toLowerCase());
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), PoisonArrow.this.applyText, player.getDisplayName());
            player.setMetadata("Poisoned", new FixedMetadataValue(Heroes.getInstance(), true));
        }

        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
            broadcast(monster.getEntity().getLocation(), PoisonArrow.this.expireText, monster.getName().toLowerCase());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, PoisonArrow.this.expireText, player.getDisplayName());
            player.setMetadata("Poisoned", new FixedMetadataValue(Heroes.getInstance(), false));
        }
    }
}
