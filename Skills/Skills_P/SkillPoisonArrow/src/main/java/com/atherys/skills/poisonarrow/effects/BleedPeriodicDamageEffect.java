package com.atherys.skills.poisonarrow.effects;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Player;

public class BleedPeriodicDamageEffect extends PeriodicDamageEffect {
	public BleedPeriodicDamageEffect(Skill skill, String name, Player applier, long period, long duration, double tickDamage) {
		super(skill, name, applier, period, duration, tickDamage);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}

	public BleedPeriodicDamageEffect(Skill skill, String name, Player applier, long period, long duration, double tickDamage, boolean knockback) {
		super(skill, name, applier, period, duration, tickDamage, knockback);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}

	public BleedPeriodicDamageEffect(Skill skill, String name, Player applier, long period, long duration, double tickDamage, String applyText, String expireText) {
		super(skill, name, applier, period, duration, tickDamage, applyText, expireText);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}

	public BleedPeriodicDamageEffect(Skill skill, String name, Player applier, long period, long duration, double tickDamage, boolean knockback, String applyText, String expireText) {
		super(skill, name, applier, period, duration, tickDamage, knockback, applyText, expireText);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}

	public BleedPeriodicDamageEffect(Skill skill, Heroes plugin, String name, Player applier, long period, long duration, double tickDamage) {
		super(skill, plugin, name, applier, period, duration, tickDamage);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}

	public BleedPeriodicDamageEffect(Skill skill, Heroes plugin, String name, Player applier, long period, long duration, double tickDamage, boolean knockback) {
		super(skill, plugin, name, applier, period, duration, tickDamage, knockback);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}

	public BleedPeriodicDamageEffect(Skill skill, Heroes plugin, String name, Player applier, long period, long duration, double tickDamage, String applyText, String expireText) {
		super(skill, plugin, name, applier, period, duration, tickDamage, applyText, expireText);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}

	public BleedPeriodicDamageEffect(Skill skill, Heroes plugin, String name, Player applier, long period, long duration, double tickDamage, boolean knockback, String applyText, String expireText) {
		super(skill, plugin, name, applier, period, duration, tickDamage, knockback, applyText, expireText);
		this.types.add(EffectType.BLEED);
		this.types.add(EffectType.POISON);
	}
}
