package com.atherys.skills.SkillPortal;

import com.atherys.heroesaddon.util.IDAPI;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.apache.commons.lang.WordUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.bukkit.Material.AIR;
import static org.bukkit.Material.OBSIDIAN;

public class SkillPortal extends ActiveSkill {

    public SkillPortal(Heroes plugin) {
        super(plugin, "Portal");
        setDescription("Creates a portal to a destination that people may enter. Type \"/skill portal list\" for a list of available locations.");
        setUsage("/skill Portal [location]|list");
        setArgumentRange(1, 1);
        setIdentifiers("skill Portal");
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), plugin);
        setTypes(SkillType.SILENCEABLE);
    }


    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("locations", Collections.<String>emptyList());
        node.set(SkillSetting.DURATION.node(), 60000L);
        node.set(SkillSetting.REAGENT.node(), "381");
        node.set(SkillSetting.REAGENT_COST.node(), 1);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (hero.isInCombat()) {
            Messaging.send(hero.getPlayer(), "You cannot use this skill in combat!");
            return SkillResult.CANCELLED;
        }

        if ( args.length == 0 ) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You must specify a location. Use '/skill portal list' in order to find out all possible locations.");
            return SkillResult.CANCELLED;
        }

        List<String> ports = SkillConfigManager.getUseSetting(hero, this, "locations", Collections.<String>emptyList());
        Player player = hero.getPlayer();
        if (args[0].equalsIgnoreCase("list")) {
            Messaging.send(player, "List of Portal locations:");
            String location = null;
            int x = 0;
            int y = 0;
            int z = 0;
            for (String s : ports) {
                String[] sp = s.split(":");
                location = WordUtils.capitalize(sp[0]);
                x = Integer.parseInt(sp[1]);
                y = Integer.parseInt(sp[2]);
                z = Integer.parseInt(sp[3]);
                Messaging.send(player, "- Name: $1, X: $2, Y: $3, Z: $4", location, x, y, z);
            }
            return SkillResult.SKIP_POST_USAGE;
        } //end if just list
        String location = null;
        int x = 0;
        int y = 0;
        int z = 0;
        String world = null;
        for (String s : ports) {
            String[] sp = s.split(":");
            if (sp[0].equalsIgnoreCase(args[0])) {
                location = sp[0];
                x = Integer.parseInt(sp[1]);
                y = Integer.parseInt(sp[2]);
                z = Integer.parseInt(sp[3]);
                world = sp[4];

                break;
            }
        }
        if (location == null) {
            player.sendMessage(ChatColor.GRAY + "You cannot create a portal to this location");
            return SkillResult.CANCELLED;
        }
        Location l = new Location(Bukkit.getWorld(world), x, y, z);
        player.sendMessage("" + location + ", X: " + x + ", Y:" + y + ", Z:" + z + ", W:" + world);
        if (!player.getWorld().getName().equals(world)) {
            player.sendMessage(ChatColor.GRAY + "You cannot create a portal to this world.");
            return SkillResult.CANCELLED;
        }
        Block block = player.getTargetBlock((Set<Material>) null, 5);
        if (block.getType() == AIR) {
            return SkillResult.CANCELLED;
        }
        for (int ry = 1; ry < 3; ry++) {
            if (block.getRelative(0, ry, 0).getType() != AIR) {
                player.sendMessage(ChatColor.GRAY + "There's not enough room for a portal.");
                return SkillResult.CANCELLED;
            }
        }

        int s = SkillConfigManager.getUseSetting(hero, this, SkillSetting.REAGENT, 381, false);
        int a = SkillConfigManager.getUseSetting(hero, this, SkillSetting.REAGENT_COST, 1, false);

        ItemStack cost = new ItemStack(com.atherys.heroesaddon.lib.IDAPI.getMaterialById(s), a);
        if (player.getInventory().containsAtLeast(cost, a)) {
            player.getInventory().removeItem(cost);
            player.updateInventory();
            Set<Block> values = new HashSet<>();
            Block b1 = block.getRelative(0, 1, 0);
            b1.setMetadata("PortalObsidian", new FixedMetadataValue(plugin, l));
            b1.setType(OBSIDIAN);
            values.add(b1);
            Block b2 = block.getRelative(0, 2, 0);
            b2.setMetadata("PortalObsidian", new FixedMetadataValue(plugin, l));
            b2.setType(OBSIDIAN);
            values.add(b2);
            long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 60000L, false);
            hero.addEffect(new PortalEffect(this, duration, values,hero));
            broadcast(player.getLocation(), ChatColor.WHITE + hero.getName() + ChatColor.GRAY + " created a portal to " + ChatColor.WHITE + location + ".");
        } else {
            player.sendMessage(ChatColor.GRAY + "You need to have at least " + a + " " + IDAPI.getMaterialById(s).name() + "s in your inventory.");
            return SkillResult.CANCELLED;
        }

        return SkillResult.NORMAL;
    }


    public class PortalEffect extends ExpirableEffect {
        private final Set<Block> values;

        public PortalEffect(Skill skill, long duration, Set<Block> values,Hero caster) {
            super(skill, "PortalEffect",caster.getPlayer(), duration);
            this.values = values;
            types.add(EffectType.MAGIC);
            types.add(EffectType.FORM);
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            for (Block block : values) {
                block.removeMetadata("PortalObsidian", plugin);
            }
            Lib.removeBlocks(values);
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), hero.getName() + ChatColor.GRAY + "' portal was closed.");
            }
        }
    }

    public class PlayerListener implements Listener {
        @EventHandler
        public void onPlayerInteract(PlayerInteractEvent event) {
            if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
                return;
            }
            Block b = event.getClickedBlock();
            if (b.getType() != OBSIDIAN) {
                return;
            }
            if (plugin.getCharacterManager().getHero(event.getPlayer()).isInCombat()) {
                event.getPlayer().sendMessage(ChatColor.GRAY + "You cannot use this while in combat!");
                return;
            }
            if (b.hasMetadata("PortalObsidian")) {
                Player player = event.getPlayer();
                player.teleport((Location) b.getMetadata("PortalObsidian").get(0).value());
                player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 1);
            }
        }
    }
}
