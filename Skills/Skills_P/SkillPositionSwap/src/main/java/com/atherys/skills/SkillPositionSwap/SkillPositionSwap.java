package com.atherys.skills.SkillPositionSwap;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillPositionSwap extends TargettedSkill {
    public SkillPositionSwap(Heroes plugin) {
        super(plugin, "PositionSwap");
        setDescription("Swap positions with your target.");
        setUsage("/skill positionswap <target>");
        setArgumentRange(0, 0);
        setIdentifiers("skill positionswap");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.DAMAGING, SkillType.TELEPORTING);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(4));
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if(target instanceof Player) {
            Player tplayer = (Player) target;
            Hero targetHero = this.plugin.getCharacterManager().getHero(tplayer);
            if (!player.hasLineOfSight(tplayer)) {
                return SkillResult.INVALID_TARGET_NO_MSG;
            }
	        if (tplayer.equals(hero.getPlayer())) {
                return SkillResult.INVALID_TARGET_NO_MSG;
            } else if (hero.hasParty()){
                if (hero.getParty().getMembers().contains(targetHero)) {
                    return SkillResult.INVALID_TARGET;
                }else {
	                Location tlocation = target.getLocation();
	                Location plocation = player.getLocation();
	                player.teleport(tlocation);
	                target.teleport(plocation);
	                broadcastExecuteText(hero, target);
	                return SkillResult.NORMAL;
                }
            }
        }
	    return SkillResult.CANCELLED;
    }

    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        return getDescription().replace("$1", damage + "");
    }
}
