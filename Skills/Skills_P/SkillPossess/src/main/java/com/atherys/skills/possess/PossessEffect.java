/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.possess;

import com.atherys.effects.SilenceEffect;
import com.atherys.effects.VanishEffect;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class PossessEffect extends PeriodicExpirableEffect {

    private final Player target;

    public PossessEffect(Hero hero, Player target, Skill skill, long dur) {
        super(skill, "Possess", hero.getPlayer(), 10, dur);
        this.target = target;
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ELDER_GUARDIAN_DEATH, 1F, 1F);
        hero.addEffect(new SilenceEffect(getSkill(), getDuration(), false, hero));
        hero.addEffect(new VanishEffect(getSkill(), getDuration(), null, hero));
        Messaging.send(target, "You have been possessed!");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (hero.hasEffect("Possess")) {
            hero.removeEffect(hero.getEffect("QuietSilence"));
            hero.removeEffect(hero.getEffect("VanishEff"));
        }
        Messaging.send(target, "You are no longer possessed!");
    }

    @Override
    public void tickHero(Hero hero) {
        target.teleport(getApplier());
    }

    @Override public void tickMonster(Monster monster) {}
}