/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.possess;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillPossess extends TargettedSkill {

    public SkillPossess(Heroes plugin) {
        super(plugin, "Possess");
        setDescription("Target an enemy within $1 blocks. You become Invulnerable, Silenced, and Vanished and the enemy is tethered to you for $2 seconds.");
        setUsage("/skill possess");
        setArgumentRange(0, 0);
        setIdentifiers("skill possess");
    }

    @Override
    public String getDescription(Hero hero) {
        int dist = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE.node(), 6, false);
        long dur = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 5000, false));
        return getDescription().replace("$1", String.valueOf(dist)).replace("$2", String.valueOf(dur / 1000));
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), 6);
        node.set(SkillSetting.DURATION.node(), 5000);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] idk) {
        if (target == hero.getPlayer()) return SkillResult.INVALID_TARGET;
        if (!(target instanceof Player)) return SkillResult.INVALID_TARGET;
        if (!hero.getPlayer().hasLineOfSight(target)) return SkillResult.INVALID_TARGET;
        int dist = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE.node(), 6, false);
        if (hero.getPlayer().getLocation().distance(target.getLocation()) > dist) return SkillResult.INVALID_TARGET;
        long dur = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 5000, false));
        hero.addEffect(new PossessEffect(hero, (Player) target, this, dur));
        return SkillResult.NORMAL;
    }

}