package com.atherys.skills.SkillPotion;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class SkillPotion extends PassiveSkill {

    public SkillPotion(Heroes plugin) {
        super(plugin, "Potion");
        setDescription("You are able to use potions!");
        setTypes(SkillType.KNOWLEDGE, SkillType.ITEM_MODIFYING);
        setEffectTypes(EffectType.BENEFICIAL);
        Bukkit.getPluginManager().registerEvents(new PotionListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection section = super.getDefaultConfig();
        section.set(SkillSetting.LEVEL.node(), 1);
        section.set(SkillSetting.NO_COMBAT_USE.node(), true);
        for (PotionType type : PotionType.values()) {
            section.set("allow." + type.name(), true);
            section.set("cooldown." + type.name(), 600000);
        }
        for (PotionType type : PotionType.values()) {
            section.set("allow." + type.name() + "-splash", false);
            section.set("cooldown." + type.name() + "-splash", 600000);
        }

        return section;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class PotionListener
            implements Listener {
        private final Skill skill;

        public PotionListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
        public void onPlayerDrinkPotion(PlayerItemConsumeEvent event) {
            if (event.getItem().getType() != Material.POTION) {
                return;
            }
            ItemStack item = event.getItem();
            Potion potion = Potion.fromItemStack(item);
            if (potion.isSplash()) {
                return;
            }

            Player player = event.getPlayer();
            Hero hero = SkillPotion.this.plugin.getCharacterManager().getHero(player);

            if ((!hero.canUseSkill(this.skill)) || ((hero.isInCombat()) && (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.NO_COMBAT_USE, true)))
                    || !SkillConfigManager.getUseSetting(hero, this.skill, "allow." + potion.getType().name(), false)) {
                Messaging.send(player, "You can't use this potion!");
                event.setCancelled(true);
                return;
            }

            String cooldownName = "Potion" + potion.getType().name();

            long time = System.currentTimeMillis();
            Long readyTime = hero.getCooldown(cooldownName);
            if ((readyTime != null) && (time < readyTime.longValue())) {
                int secRemaining = (int) Math.ceil((readyTime.longValue() - time) / 1000.0D);
                Messaging.send(player, "You can't use this potion for $1 seconds!", secRemaining);
                event.setCancelled(true);
                return;
            }

            long cooldown = SkillConfigManager.getUseSetting(hero, this.skill, "cooldown." + potion.getType().name(), 600000, true);
            hero.setCooldown(cooldownName, time + cooldown);
        }

        @EventHandler(priority = EventPriority.LOW, ignoreCancelled = false)
        public void onPlayerUsePotion(PlayerInteractEvent event) {
            if (!event.hasItem() || event.getItem().getType() != Material.POTION) {
                return;
            }
            ItemStack item = event.getItem();
            Potion potion = Potion.fromItemStack(item);
            if (!potion.isSplash()) {
                return;
            }

            Player player = event.getPlayer();
            Hero hero = SkillPotion.this.plugin.getCharacterManager().getHero(player);

            if ((!hero.canUseSkill(this.skill)) || ((hero.isInCombat()) && (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.NO_COMBAT_USE, true)))
                    || !SkillConfigManager.getUseSetting(hero, this.skill, "allow." + potion.getType().name() + "-splash", false)) {
                Messaging.send(player, "You can't use this potion!");
                event.setCancelled(true);
                return;
            }

            String cooldownName = "Potion" + potion.getType().name() + "-splash";

            long time = System.currentTimeMillis();
            Long readyTime = hero.getCooldown(cooldownName);
            if ((readyTime != null) && (time < readyTime.longValue())) {
                int secRemaining = (int) Math.ceil((readyTime.longValue() - time) / 1000.0D);
                Messaging.send(player, "You can't use this potion for $1 seconds!", secRemaining);
                event.setCancelled(true);
                return;
            }

            long cooldown = SkillConfigManager.getUseSetting(hero, this.skill, "cooldown." + potion.getType().name() + "-splash", 600000, true);
            hero.setCooldown(cooldownName, time + cooldown);
        }

    }
}
