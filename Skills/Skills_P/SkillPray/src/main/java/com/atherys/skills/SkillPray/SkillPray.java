package com.atherys.skills.SkillPray;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillPray extends TargettedSkill {

    public SkillPray(Heroes plugin) {
        super(plugin, "Pray");
        setDescription("You restore $1 health to your target and yourself.");
        setUsage("/skill pray");
        setArgumentRange(0, 0);
        setIdentifiers("skill pray");
        setTypes(SkillType.HEALING, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.HEALTH.node(), Integer.valueOf(10));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(15));
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
	    Player player = hero.getPlayer();
	    if (!(target instanceof Player)) {
		    return SkillResult.INVALID_TARGET;
	    }
		if (player.equals(target)){
			return SkillResult.INVALID_TARGET_NO_MSG;
	    }
	    Hero targetHero = this.plugin.getCharacterManager().getHero((Player) target);
	    if (hero.hasParty()) {
			if (hero.getParty().getMembers().contains(targetHero)) {

				double hpPlus = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH, 10, false);
				double targetHealth = target.getHealth();
				if (targetHealth >= target.getMaxHealth() && player.getHealth() >= player.getMaxHealth()) {
					Messaging.send(player, "Both you and your target are already at full health.");
					return SkillResult.INVALID_TARGET_NO_MSG;
				}
				HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(hero, hpPlus, this, hero);
				HeroRegainHealthEvent thrhEvent = new HeroRegainHealthEvent(targetHero, hpPlus, this, hero);
				this.plugin.getServer().getPluginManager().callEvent(hrhEvent);
				this.plugin.getServer().getPluginManager().callEvent(thrhEvent);
				if (hrhEvent.isCancelled() && thrhEvent.isCancelled()) {
					Messaging.send(player, "Unable to heal any of you at this time!");
					return SkillResult.CANCELLED;
				}
				if (targetHealth < target.getMaxHealth()) {
					targetHero.heal(thrhEvent.getDelta());
				}
				if (player.getHealth() < player.getMaxHealth()) {
					hero.heal(hrhEvent.getDelta());
				}
				broadcastExecuteText(hero, target);
				return SkillResult.NORMAL;
			}
			return SkillResult.CANCELLED;
		}
	    return SkillResult.CANCELLED;
    }

    public String getDescription(Hero hero) {
        int health = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH.node(), 10, false);
        return getDescription().replace("$1", health + "");
    }
}
