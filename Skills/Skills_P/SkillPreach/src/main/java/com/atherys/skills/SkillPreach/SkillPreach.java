package com.atherys.skills.SkillPreach;

import com.atherys.effects.SkillCastingEffect;
import com.atherys.effects.StunEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SkillPreach extends ActiveSkill {
    public SkillPreach(Heroes plugin) {
        super(plugin, "Preach");
        setDescription("After channeling for $1 seconds, puts nearby enemies within $2 blocks to sleep for $3 seconds.");
        setUsage("/skill preach");
        setArgumentRange(0, 0);
        setIdentifiers("skill Preach");
        setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(10));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(1500L));
        node.set("warm-up", Long.valueOf(3000L));
        node.set("messages", Collections.emptyList());
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        double r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1500L, false);
        long warmup = (long) SkillConfigManager.getUseSetting(hero, this, "warm-up", 3000L, false);
        hero.addEffect(new CastingPreachEffect(this, warmup, false, duration, 100L, r,hero));
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1500L, false);
        long warmup = (long) SkillConfigManager.getUseSetting(hero, this, "warm-up", 3000L, false);
        return getDescription().replace("$1", ((double) (warmup / 1000)) + "").replace("$2", ((int) r) + "").replace("$3", ((double) (duration / 1000)) + " ") + Lib.getSkillCostStats(hero, this);
    }

    public class CastingPreachEffect extends SkillCastingEffect {
        private final long duration;
        private final long period;
        private final double radius;

        public CastingPreachEffect(Skill skill, long warmup, boolean slow, long duration, long period, double radius,Hero caster) {
            super(skill, warmup, slow, true,caster);
            this.duration = duration;
            this.period = period;
            this.radius = radius;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                for (Entity e : hero.getPlayer().getNearbyEntities(radius, radius, radius)) {
                    if (!(e instanceof Player)) {
                        continue;
                    }
                    if (damageCheck((Player) e, (LivingEntity) hero.getPlayer())) {
                        Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                        tHero.addEffect(new StunEffect(skill, duration,hero));
                        Lib.cancelDelayedSkill(tHero);
                    }
                }
                List<String> l = SkillConfigManager.getUseSetting(hero, skill, "messages", Collections.<String>emptyList());
                if (l.isEmpty()) {
                    System.out.println("You have to add messages for skill preach.");
                    return;
                }
                Random random = new Random();
                String message = l.get(random.nextInt(l.size()));
                broadcast(hero.getPlayer().getLocation(), "[" + ChatColor.GOLD + "local" + ChatColor.WHITE + "] " + ChatColor.YELLOW + hero.getName() + ChatColor.WHITE + ": " + ChatColor.GOLD + message + ".");
            }
        }
    }

}