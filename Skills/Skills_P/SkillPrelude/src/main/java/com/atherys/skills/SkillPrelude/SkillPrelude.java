package com.atherys.skills.SkillPrelude;

import com.atherys.effects.SkillCastingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffectType;

public class SkillPrelude extends ActiveSkill {

    public SkillPrelude(Heroes plugin) {
        super(plugin, "Prelude");
        setDescription("While channeling, the song will make nearby enemies' heals be reduced by a large amount.");
        setArgumentRange(0, 0);
        setUsage("/skill Prelude");
        setIdentifiers("skill Prelude");
        setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_DARK);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("heal-reduction", 0.75);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double healReduction = SkillConfigManager.getUseSetting(hero, this, "heal-reduction", 0.75, false);
        hero.addEffect(new PreludeCastingEffect(this, duration,hero));
        hero.addEffect(new PreludeEffect(this, duration, radius, healReduction,hero));
        return SkillResult.NORMAL;
    }

    public class PreludeCastingEffect extends SkillCastingEffect {

        public PreludeCastingEffect(Skill skill, long duration,Hero caster) {
            super(skill, duration, false, "$1 is channeling their $2!", "$1 finished channeling their $2.", "$1 got interrupted from channeling their $2!",caster);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (hero.hasEffect("PreludeCasting")) {
                hero.removeEffect(hero.getEffect("PreludeCasting"));
            }
        }
    }

    public class PreludeEffect extends PeriodicExpirableEffect {
        private double range;
        private double healReduction;

        public PreludeEffect(Skill skill, long duration, double range, double healReduction,Hero caster) {
            super(skill, "PreludeCasting",caster.getPlayer(), 1000, duration);
            this.range = range;
            this.healReduction = healReduction;
            this.types.add(EffectType.UNBREAKABLE);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (Entity e : player.getNearbyEntities(range, range, range)) {
                if (e instanceof Player) {
                    if (damageCheck(player, (LivingEntity)e)) {
                        plugin.getCharacterManager().getHero((Player) e).addEffect(new PreludeCurseEffect(skill, healReduction,hero));
                    }
                }
            }
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }
    }

    public class PreludeCurseEffect extends ExpirableEffect {
        private double healReduction;

        public PreludeCurseEffect(Skill skill, double healReduction,Hero caster) {
            super(skill, "Prelude",caster.getPlayer(), 1000);
            this.healReduction = healReduction;
            this.types.add(EffectType.UNBREAKABLE);
            this.types.add(EffectType.WITHER);
            addMobEffect(20, 20, 1, false);
        }

        public double getHealReduction() {
            return healReduction;
        }
    }

    public class SkillEntityListener implements Listener {
        @EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onEntityRegainHealth(HeroRegainHealthEvent event) {
            Hero hero = event.getHero();
            if (hero.hasEffect("Prelude")) {
                if (hero.getPlayer().hasPotionEffect(PotionEffectType.REGENERATION)) {
                    hero.getPlayer().removePotionEffect(PotionEffectType.REGENERATION);
                }
                double healReduction = ((PreludeCurseEffect)hero.getEffect("Prelude")).getHealReduction();
                event.setDelta(event.getDelta() * (1 - healReduction));
            }
        }
    }
}