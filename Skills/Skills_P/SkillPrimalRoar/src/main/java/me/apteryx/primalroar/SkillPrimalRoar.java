package me.apteryx.primalroar;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

//Written by LordOfRoosters

public class SkillPrimalRoar extends ActiveSkill
{
    public SkillPrimalRoar(Heroes plugin)
    {
        super(plugin, "PrimalRoar");
        setDescription("Unleash a Primal Roar that deals $1 damage to all enemies within $2 blocks directly in front of you. The explosive force of the roar knocks up any targets it hits.");
        setUsage("/skill primalroar");
        setArgumentRange(0, 0);
        setIdentifiers("skill primalroar", "skill roar");
        setTypes(SkillType.FORCE, SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero)
    {
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 5, false);
        double dmg = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 40, false);
        double damageIncrease = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0.5, false) * hero.getSkillLevel(this);
        final double damage = dmg + damageIncrease;

        return getDescription().replace("$1", damage + "").replace("$2", distance + "");
    }

    public ConfigurationSection getDefaultConfig()
    {
        ConfigurationSection node = super.getDefaultConfig();

        node.set(SkillSetting.MAX_DISTANCE.node(), 5);
        node.set(SkillSetting.DAMAGE.node(), 40);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 0.5);
        node.set("roar-delay", 2);
        node.set("knockup-vector", 0.6);
        node.set(SkillSetting.RADIUS.node(), 2);

        return node;
    }

    public SkillResult use(final Hero hero, String[] args)
    {
        final Player player = hero.getPlayer();

        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 5, false);

        Block tempBlock;
        BlockIterator iter = null;
        try {
            iter = new BlockIterator(player, distance);
        }
        catch (IllegalStateException e) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }

        broadcastExecuteText(hero);

        double dmg = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 40, false);

        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 2, false);
        final int radiusSquared = radius * radius;

        int delay = SkillConfigManager.getUseSetting(hero, this, "roar-delay", 2, false);

        double knockupVec = SkillConfigManager.getUseSetting(hero, this, "knockup-vector", 0.6, true);

        final List<Entity> nearbyEntities = player.getNearbyEntities(distance * 2, distance, distance * 2);
        final List<Entity> hitEnemies = new ArrayList<Entity>();

        int numBlocks = 0;

        player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 0.5F, 1.0F);

        while (iter.hasNext())
        {
            tempBlock = iter.next();
            Material tempBlockType = tempBlock.getType();
            if (Util.transparentBlocks.contains(tempBlockType))
            {
                final Location targetLocation = tempBlock.getLocation().clone().add(new Vector(.5, 0, .5));

                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                    targetLocation.getWorld().playSound(targetLocation, Sound.ENTITY_GENERIC_EXPLODE, 0.5F, 1.3F);
                    targetLocation.getWorld().spigot().playEffect(targetLocation, Effect.EXPLOSION_LARGE, 0, 0, 0.5F, 0.5F, 0.5F, 0.0F, 2, 20);
                    targetLocation.getWorld().spigot().playEffect(targetLocation, Effect.NOTE, 0, 0, 0.5F, 0.5F, 0.5F, 0.0F, 25, 20);
                    targetLocation.getWorld().spigot().playEffect(targetLocation, Effect.MAGIC_CRIT, 0, 0, 0.5F, 0.5F, 0.5F, 0.0F, 40, 20);
                    for (Entity entity : nearbyEntities)
                    {
                        if (!(entity instanceof LivingEntity) || hitEnemies.contains(entity) || entity.getLocation().distanceSquared(targetLocation) > radiusSquared)
                            continue;

                        if (!damageCheck(player, (LivingEntity) entity))
                            continue;

                        LivingEntity target = (LivingEntity) entity;

                        Vector velocity = new Vector(0, knockupVec, 0);
                        target.setVelocity(velocity);

                        addSpellTarget(target, hero);
                        damageEntity(target, player, dmg, DamageCause.PROJECTILE, false);

                        hitEnemies.add(entity);
                    }
                }, numBlocks * delay);

                numBlocks++;
            }
            else
                break;
        }
        return SkillResult.NORMAL;
    }
}
