package com.atherys.skills.SkillPulse;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

public class SkillPulse extends ActiveSkill {
    //private long duration;
    public SkillPulse(Heroes plugin) {
        super(plugin, "Pulse");
        setArgumentRange(0, 0);
        setUsage("/skill Pulse");
        setIdentifiers("skill Pulse");
        setTypes(SkillType.SILENCEABLE);
        setDescription("The obelisk pulses with energy pushing players away from it for x seconds");
    }

    @Override
    public String getDescription(Hero hero) {
        int a = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 10, false);
        return getDescription().replace("$1", a + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (!hero.hasEffect("Rift")) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You do not have active obelisk");
            return SkillResult.CANCELLED;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        hero.addEffect(new ExpirableEffect(this, "Pulse", hero.getPlayer() ,duration));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
