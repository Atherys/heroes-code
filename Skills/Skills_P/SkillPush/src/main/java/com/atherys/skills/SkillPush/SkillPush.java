package com.atherys.skills.SkillPush;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

public class SkillPush extends TargettedSkill {
    public SkillPush(Heroes plugin) {
        super(plugin, "Push");
        setDescription("Single target knockback and deals damage deals $1 damage to target.");
        setUsage("/skill Push");
        setArgumentRange(0, 0);
        setIdentifiers("skill Push");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }
        Player tPlayer = (Player) target;
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        double v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 1, false);
        double v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 1, false);
        damageEntity(tPlayer, player, damage, DamageCause.MAGIC);
        Vector v = tPlayer.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
        v = v.multiply(v1);
        v = v.setY(v2);
        tPlayer.setVelocity(v);
        tPlayer.setFallDistance(-8.0F);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10));
        node.set("vertical-vector", Integer.valueOf(10));
        node.set("horizontal-vector", Integer.valueOf(10));
        return node;
    }
}