package com.atherys.skills.SkillPyroblast;

import com.atherys.effects.SkillCastingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillPyroblast extends ActiveSkill {

    public SkillPyroblast(Heroes plugin) {
        super(plugin, "Pyroblast");
        setDescription("After channeling for a few seconds, you rapidly fire multiple Meteors in the direction you're looking.");
        setUsage("/skill Pyroblast");
        setArgumentRange(0, 0);
        setIdentifiers("skill pyroblast");
        setTypes(SkillType.ABILITY_PROPERTY_FIRE, SkillType.SILENCEABLE );
        Bukkit.getServer().getPluginManager().registerEvents(new PyroblastEntityListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(4));
        node.set(SkillSetting.DAMAGE_INCREASE.node(), Double.valueOf(0.0D));
        node.set("velocity-multiplier", Double.valueOf(1.0D));
        node.set("fire-ticks", Integer.valueOf(100));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(3));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(500));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(2500));
        node.set("warm-up", Long.valueOf(5000));
        return node;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 2500, false);
        long warmup = SkillConfigManager.getUseSetting(hero, this, "warm-up", 5000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 500, false);
        hero.addEffect(new CastingPyroblastEffect(this, warmup, false, duration, period,hero));
        return SkillResult.NORMAL;
    }

    public class CastingPyroblastEffect extends SkillCastingEffect {
        private final long duration;
        private final long period;

        public CastingPyroblastEffect(Skill skill, long warmup, boolean slow, long duration, long period,Hero caster) {
            super(skill, warmup, slow, true,caster);
            this.duration = duration;
            this.period = period;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                hero.addEffect(new PyroblastEffect(skill, duration, period,hero));
            }
        }
    }

    public class PyroblastEffect extends PeriodicExpirableEffect {
        public PyroblastEffect(Skill skill, long duration, long period,Hero caster) {
            super(skill, "PyroblastEffect",caster.getPlayer(), period, duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.FIRE);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            LargeFireball fireball = player.launchProjectile(LargeFireball.class);
            fireball.setIsIncendiary(false);
            fireball.setYield(1.0F);
            fireball.setMetadata("PyroblastMeteor", new FixedMetadataValue(plugin, true));
            double mult = SkillConfigManager.getUseSetting(hero, skill, "velocity-multiplier", 1, false);
            fireball.setVelocity(fireball.getVelocity().multiply(mult));
            fireball.setShooter(player);
        }

        @Override
        public void tickMonster(Monster arg0) {
            //N/A
        }
    }

    public class PyroblastEntityListener implements Listener {
        private final Skill skill;

        public PyroblastEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onGhastProjectileHit(EntityExplodeEvent event) {
            if ((!(event.getEntity() instanceof LargeFireball)) || (!event.getEntity().hasMetadata("PyroblastMeteor"))) {
                return;
            }

            LargeFireball fireball = (LargeFireball) event.getEntity();
            Entity dmger = (Player) fireball.getShooter();
            Hero hero = SkillPyroblast.this.plugin.getCharacterManager().getHero((Player) dmger);
            int radius = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.RADIUS.node(), 30, false);
            for (Entity e : fireball.getNearbyEntities(radius, radius, radius)) {
                if (((e instanceof LivingEntity)) && (SkillPyroblast.damageCheck((Player) dmger, (LivingEntity) e))) {
                    int ft = SkillConfigManager.getUseSetting(hero, this.skill, "fire-tics", 100, false);
                    e.setFireTicks(ft);
                    SkillPyroblast.this.addSpellTarget(e, hero);
                    double damage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE, 4, false);
                    skill.damageEntity((LivingEntity) e, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                }
            }
            event.setCancelled(true);
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onProjectileHit(ProjectileHitEvent event) {
            if (!(event.getEntity() instanceof LargeFireball)) {
                return;
            }
            LargeFireball fireball = (LargeFireball) event.getEntity();
            if (!fireball.hasMetadata("PyroblastMeteor")) {
                return;
            }
            fireball.setIsIncendiary(false);
            fireball.setFireTicks(0);
        }
    }
}
