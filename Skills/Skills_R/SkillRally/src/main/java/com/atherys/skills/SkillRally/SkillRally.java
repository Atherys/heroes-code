package com.atherys.skills.SkillRally;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillRally extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillRally(Heroes plugin) {
        super(plugin, "Rally");
        setDescription("You and your party gain a burst of speed for $1 seconds.");
        setUsage("/skill rally");
        setArgumentRange(0, 0);
        setIdentifiers("skill rally");
        setTypes(SkillType.BUFFING, SkillType.MOVEMENT_INCREASING, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-multiplier", Integer.valueOf(2));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(20000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(30));
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        int multiplier = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 2, false);
        QuickenEffect qEffect = new QuickenEffect(this, getName(), duration, multiplier,hero);
        if (!hero.hasParty()) {
            broadcastExecuteText(hero);
            hero.addEffect(qEffect);
            return SkillResult.NORMAL;
        } else {
            int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
            broadcastExecuteText(hero);
            for (Hero pHero : hero.getParty().getMembers()) {
                Player pPlayer = pHero.getPlayer();
                if ((pPlayer.getWorld().equals(player.getWorld())) &&
                        (pPlayer.getLocation().distanceSquared(player.getLocation()) <= r * r) &&
                        (!pHero.hasEffect("rally"))) {
                    pHero.addEffect(qEffect);
                }
            }
            return SkillResult.NORMAL;
        }
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class QuickenEffect extends ExpirableEffect {

        public QuickenEffect(Skill skill, String name, long duration, int amplifier,Hero caster) {
            super(skill, name,caster.getPlayer(), duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.SPEED);
            this.addMobEffect(1, (int) (duration / 1000L) * 20, amplifier, false);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You gained a burst of speed!");
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You returned to normal speed!");
        }
    }
}
