package com.atherys.skills.SkillRam;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

public class SkillRam extends TargettedSkill {
    public SkillRam(Heroes plugin) {
        super(plugin, "Ram");
        setDescription("Caster ports to a player within five blocks dealing $1 damage and knocking them back two blocks.");
        setUsage("/skill Ram");
        setArgumentRange(0, 0);
        setIdentifiers("skill Ram");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!hero.getPlayer().hasLineOfSight(le)) {
            return SkillResult.INVALID_TARGET;
        }
        if (!(le instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        } else if (le instanceof Player) {
            Player target = (Player) le;
            if (target.equals(hero.getPlayer())) {
		        return SkillResult.INVALID_TARGET_NO_MSG;
	        }
	        Hero enemy = this.plugin.getCharacterManager().getHero(target);
	        if (hero.hasParty()){
		        if (hero.getParty().getMembers().contains(enemy)){
			        return SkillResult.INVALID_TARGET_NO_MSG;
		        }
	        }

            Location loc = le.getLocation();
            loc.setPitch(hero.getPlayer().getLocation().getPitch());
            loc.setYaw(hero.getPlayer().getLocation().getYaw());
            hero.getPlayer().teleport(loc);
            double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
            damageEntity(le, hero.getPlayer(), damage, DamageCause.MAGIC, true);
            double horizontalMult = SkillConfigManager.getUseSetting(hero, this, "horizontal-multiplier", 3, false);
            double verticalMult = SkillConfigManager.getUseSetting(hero, this, "vertical-multiplier", 0.6, false);
            Vector v = hero.getPlayer().getEyeLocation().getDirection();
            v.multiply(horizontalMult);
            v.setY(verticalMult);
            le.setVelocity(v);
            broadcastExecuteText(hero, le);
            return SkillResult.NORMAL;
        }
        return SkillResult.INVALID_TARGET;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", "" + damage);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 50);
        node.set("horizontal-multiplier", 3.0);
        node.set("vertical-multiplier", 0.6);
        return node;
    }
}
