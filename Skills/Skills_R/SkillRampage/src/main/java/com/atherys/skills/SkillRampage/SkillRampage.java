package com.atherys.skills.SkillRampage;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillRampage extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillRampage(Heroes plugin) {
        super(plugin, "Rampage");
        setDescription("Active\nCaster gains Speed $1 for $2s seconds and deals $3 physical damage to surrounding enemies within a $4 block radius every $5 seconds.");
        setUsage("/skill Rampage");
        setArgumentRange(0, 0);
        setIdentifiers("skill Rampage");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING, SkillType.ABILITY_PROPERTY_PHYSICAL);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        int range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 2, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        int amplifier = SkillConfigManager.getUseSetting(hero, this, "amplifier", 1, false);
        hero.addEffect(new RampageEffect(this, period, damage, range, duration, amplifier,hero));
        return SkillResult.NORMAL;
    }

    @Override
    public void init() {
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(4));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(1000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(2));
        node.set("amplifier", Integer.valueOf(1));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        int range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 2, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        int amplifier = SkillConfigManager.getUseSetting(hero, this, "amplifier", 1, false);
        return getDescription().replace("$1", "" + amplifier).replace("$2", (duration / 1000L) + "").replace("$3", damage + "").replace("$4", range + "").replace("$5", (period / 1000L) + "") + Lib.getSkillCostStats(hero, this);
    }

    public class RampageEffect extends PeriodicExpirableEffect {
        private double tickDamage;
        private int range;

        public RampageEffect(SkillRampage skill, long period, double tickDamage, int range, long duration, int amplifier,Hero caster) {
            super(skill, "Rampage",caster.getPlayer(), period, duration);
            this.tickDamage = tickDamage;
            this.range = range;
            this.types.add(EffectType.SPEED);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(1, tickDuration, amplifier, false);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "Rampage");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "Rampage");
        }

        @Override
        public void tickMonster(Monster arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (Entity entity : player.getNearbyEntities(range, range, range)) {
                if (entity instanceof LivingEntity) {
                    LivingEntity lEntity = (LivingEntity) entity;
                    damageEntity(lEntity, player, tickDamage, EntityDamageEvent.DamageCause.MAGIC);
                    addSpellTarget(lEntity, hero);
                }
            }
        }
    }
}
