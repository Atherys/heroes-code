package com.atherys.skills.SkillRayOfLight;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillRayOfLight extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillRayOfLight(Heroes plugin) {
        super(plugin, "RayOfLight");
        setDescription("Sunlight breaks through to target area for $1 seconds. Light would kiss the ground in a $2 block radius and all party members inside would heal at a rate of $3.");
        setUsage("/skill RayOfLight");
        setArgumentRange(0, 0);
        setIdentifiers("skill RayOfLight");
        setTypes(SkillType.ABILITY_PROPERTY_EARTH, SkillType.ABILITY_PROPERTY_LIGHT, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 20000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set("heal", 10);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%' %skill% expired.");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int heal = SkillConfigManager.getUseSetting(hero, this, "heal", 10, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", r + "").replace("$3", heal + "");
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%' %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player p = hero.getPlayer();
        HashSet<Byte> transparent = new HashSet<>();
        transparent.add((byte) Material.AIR.getId());
        transparent.add((byte) Material.SNOW.getId());
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block b = p.getTargetBlock(transparent, max);
        if (b.getType() == Material.AIR) {
            p.sendMessage(ChatColor.GRAY + "You must target a block.");
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        Block b1 = b.getRelative(BlockFace.UP);
        if (!(b1.getType() == Material.AIR || b1.getType() == Material.SNOW)) {
            p.sendMessage(ChatColor.GRAY + "Cannot be placed here");
            return SkillResult.CANCELLED;
        }
        values.add(b1);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, "heal", 10, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        b1.setType(Material.GLOWSTONE);
        b1.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
        Long identifierrol = Long.valueOf(System.currentTimeMillis());
        hero.addEffect(new RoLEffect(this, period, duration, heal, r, b1, values,hero));
        //Sentinel CD stuff
        if (hero.getCooldown("Blessing") == null || (hero.getCooldown("Blessing") - (long) identifierrol) < 20000) {
            hero.setCooldown("Blessing", (long) identifierrol + 20000);
        }
        if (hero.getCooldown("DivineShield") == null || (hero.getCooldown("DivineShield") - (long) identifierrol) < 20000) {
            hero.setCooldown("DivineShield", (long) identifierrol + 20000);
        }
        return SkillResult.NORMAL;
    }

    public class RoLEffect extends PeriodicExpirableEffect {
        private final double heal;
        private final int radius;
        private final Block b;
        private final Set<Block> values;

        public RoLEffect(Skill skill, long period, long duration, double heal, int radius, Block b, Set<Block> values,Hero caster) {
            super(skill, "RoLEffect",caster.getPlayer(), period, duration);
            this.heal = heal;
            this.radius = radius;
            this.b = b;
            this.values = values;
            types.add(EffectType.HEALING);
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.FORM);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Ray of Light");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Ray of Light");
            }
            Lib.removeBlocks(values);
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            for (Entity e : b.getLocation().getWorld().getNearbyEntities(b.getLocation(), radius, radius, radius)) {
                if (!(e instanceof Player)) continue;
                Hero h = plugin.getCharacterManager().getHero((Player) e);
                if (h.equals(hero) || h.hasParty() && (h.getParty().getMembers().contains(hero))) {
                    HeroRegainHealthEvent hr = new HeroRegainHealthEvent(h, heal, skill, hero);
                    this.plugin.getServer().getPluginManager().callEvent(hr);
                    if (!hr.isCancelled()) {
                        h.heal(hr.getDelta());
                    }
                }
            }
        }
    }
}
