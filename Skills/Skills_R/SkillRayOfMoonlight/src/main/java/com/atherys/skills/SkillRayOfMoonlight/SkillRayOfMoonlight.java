package com.atherys.skills.SkillRayOfMoonlight;


import com.atherys.effects.SilenceEffect;
import com.atherys.effects.WaveStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.BlindEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillRayOfMoonlight extends ActiveSkill {
	private String expiretext;

	public SkillRayOfMoonlight(Heroes plugin) {
		super(plugin, "RayOfMoonlight");
		setDescription("Moonlight breaks through to target area healing allies around it periodically. If you have 3 Wave stacks it will also Blind and Silence nearby enemies.");
		setUsage("/skill RayOfMoonlight");
		setArgumentRange(0, 0);
		setIdentifiers("skill RayOfMoonlight");
		setTypes(SkillType.ABILITY_PROPERTY_EARTH, SkillType.ABILITY_PROPERTY_LIGHT, SkillType.SILENCEABLE);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), 20000);
		node.set(SkillSetting.PERIOD.node(), 4000);
		node.set("heal", 10);
		node.set(SkillSetting.MAX_DISTANCE.node(), 10);
		node.set(SkillSetting.RADIUS.node(), 7);
		node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%' %skill% expired.");
		return node;
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	@Override
	public void init() {
		super.init();
		expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%' %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
	}

	@Override
	public SkillResult use(Hero hero, String[] args) {
		Player player = hero.getPlayer();
		int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
		Block b = player.getTargetBlock(Util.transparentBlocks, max);
		if (b.getType() == Material.AIR) {
			Messaging.send(player, "You must target a block.");
			return SkillResult.CANCELLED;
		}
		Block block = b.getRelative(BlockFace.UP);
		if (!(block.getType() == Material.AIR || block.getType() == Material.SNOW)) {
			Messaging.send(player, "Cannot be placed here");
			return SkillResult.CANCELLED;
		}
		long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
		long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 4000, false);
		long blindPeriod = SkillConfigManager.getUseSetting(hero, this, "blind", 4000, false);
		double heal = SkillConfigManager.getUseSetting(hero, this, "heal", 10, false);
		int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 7, false);
		block.setType(Material.SEA_LANTERN);
		block.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
		hero.addEffect(new RoMLEffect(this, period, blindPeriod, duration, heal, radius, WaveStackEffect.chargeWave(hero), block,hero));
		broadcastExecuteText(hero);
		return SkillResult.NORMAL;
	}

	public class RoMLEffect extends PeriodicExpirableEffect {
		private final double heal;
		private final int radius;
		private final boolean charged;
		private final Block block;
		private final long blindPeriod;

		public RoMLEffect(Skill skill, long blindPeriod, long period, long duration, double heal, int radius, boolean charged, Block block,Hero caster) {
			super(skill, "RoMLEffect",caster.getPlayer(), period, duration);
			this.heal = heal;
			this.radius = radius;
			this.charged = charged;
			this.block = block;
			this.blindPeriod = blindPeriod;
			types.add(EffectType.HEALING);
			types.add(EffectType.DISPELLABLE);
			types.add(EffectType.FORM);
			types.add(EffectType.BENEFICIAL);
			types.add(EffectType.MAGIC);

		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			if (charged) {
				broadcast(hero.getPlayer().getLocation(), "$1 expended their Wave stacks on $2!", hero.getName(), "Ray of Moonlight");
			}
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			if (this.isExpired()) {
				broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Ray of Moonlight");
			}
			Set<Block> blocks = new HashSet<Block>();
			blocks.add(block);
			Lib.removeBlocks(blocks);
		}

		@Override
		public void tickMonster(Monster monstr) {
			//N/A
		}

		@Override
		public void tickHero(Hero hero) {
			for (Entity e : block.getWorld().getNearbyEntities(block.getLocation(), radius, radius, radius)) {
				if (e instanceof Player) {
					Player tPlayer = (Player) e;
					Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
					if ((hero.hasParty() && (hero.getParty().isPartyMember(tPlayer)))) {
						Lib.healHero(tHero, heal, skill, hero);
					} else if (charged) {
						if (damageCheck(hero.getPlayer(), (LivingEntity) tPlayer)) {
							tHero.addEffect(new BlindEffect(skill,hero.getPlayer(),getPeriod(), "", ""));
							tHero.addEffect(new SilenceEffect(skill, getPeriod(), true,hero));
						}
					}
				}
			}
		}

		public class RayCustomEffect extends ExpirableEffect {
			private final String applyText;
			private final String expireText;
			private final long duration;
			private final long period;


			public RayCustomEffect(final Skill skill, long duration, long period, final String applyText, final String expireText,Hero caster) {
				super(skill, "RayCustom",caster.getPlayer(), duration);
				this.applyText = applyText;
				this.expireText = expireText;
				this.types.add(EffectType.DISPELLABLE);
				this.types.add(EffectType.HARMFUL);
				this.types.add(EffectType.BLIND);
				this.addMobEffect(9, (int) ((duration + 4000L) / 1000L * 20L), 1, false);
				this.addMobEffect(15, (int) (duration / 1000L * 20L), 1, false);
				this.duration = duration;
				this.period = period;
			}

			@Override
			public void applyToHero(final Hero hero) {
				super.applyToHero(hero);
				final Player player = hero.getPlayer();

				final int currentHunger = player.getFoodLevel();
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this.skill.plugin, (Runnable) new Runnable() {
					@Override
					public void run() {
						player.setFoodLevel(currentHunger);
					}
				}, 2L);
				this.broadcast(player.getLocation(), this.applyText, player.getDisplayName());
			}

			@Override
			public void removeFromHero(final Hero hero) {
				super.removeFromHero(hero);
				final Player player = hero.getPlayer();
				this.broadcast(player.getLocation(), this.expireText, player.getDisplayName());
			}
		}
	}
}

