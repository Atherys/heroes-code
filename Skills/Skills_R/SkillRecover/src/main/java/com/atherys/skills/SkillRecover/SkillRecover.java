package com.atherys.skills.SkillRecover;

import com.atherys.effects.OfficerDeployedEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;

public class SkillRecover extends ActiveSkill {

    public SkillRecover(Heroes plugin) {
        super(plugin, "Recover");
        setDescription("Cures each ally within $1 blocks of your MoraleOfficer of 1 debuff.");
        setUsage("/skill recover");
        setArgumentRange(0, 0);
        setIdentifiers("skill recover");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
	    Bukkit.getServer().getPluginManager().registerEvents(new DamageCancelListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        String description = getDescription().replace("$1", radius + "");
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
	    node.set(SkillSetting.PERIOD.node(),4000);
        return node;
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
	    if (hero.hasEffect("OfficerDeployedEffect")) {
		    Hero officer = ((OfficerDeployedEffect) hero.getEffect("OfficerDeployedEffect")).getOfficer();
		    int range = SkillConfigManager.getUseSetting(hero, this, "Range", 20, false);
		    int Period = SkillConfigManager.getUseSetting(hero, this, "Period", 3000, false);

		    if (hero.getPlayer().getLocation().distanceSquared(officer.getPlayer().getLocation()) <= range * range) {
			    if (!hero.hasParty() && officer.equals(hero)) {
				    for (Effect effect : hero.getEffects()) {
					    if (effect.isType(EffectType.HARMFUL) && effect.isType(EffectType.DISPELLABLE) && !effect.isType(EffectType.UNBREAKABLE) && !effect.isType(EffectType.BENEFICIAL)) {
						    hero.removeEffect(effect);
					    }
				    }
				    hero.addEffect(new UnstoppableEffect(this, Period));
			    } else {
				    Player pOfficer = officer.getPlayer();
				    int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
				    for (Hero pHero : hero.getParty().getMembers()) {
					    Player pPlayer = pHero.getPlayer();
					    if (pPlayer.getWorld().equals(pOfficer.getWorld()) && pPlayer.getLocation().distanceSquared(pOfficer.getLocation()) <= radius * radius) {
						    for (Effect effect : pHero.getEffects()) {
							    if (effect.isType(EffectType.HARMFUL) && effect.isType(EffectType.DISPELLABLE) && !effect.isType(EffectType.UNBREAKABLE) && !effect.isType(EffectType.BENEFICIAL)) {
								    pHero.removeEffect(effect);
							    }
						    }
						    pHero.addEffect(new UnstoppableEffect(this, Period));
					    }
				    }
				    broadcast(pOfficer.getLocation(), "$1's troops have recovered!", officer.getName());
			    }
			    broadcastExecuteText(hero);
			    return SkillResult.NORMAL;
		    } else{
			    Messaging.send(hero.getPlayer(), "Target too far away!");
			    return SkillResult.INVALID_TARGET;
		    }

	    } else {
		    Messaging.send(hero.getPlayer(), "You have not deployed a MoraleOfficer");
		    return SkillResult.INVALID_TARGET_NO_MSG;
	    }


    }
	public class UnstoppableEffect extends PeriodicEffect {

		public UnstoppableEffect(Skill skill, long period) {
			super(skill, "UnstoppableEffect",period);
			types.add(EffectType.HARMFUL);
			types.add(EffectType.PHYSICAL);
			types.add(EffectType.DISPELLABLE);
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
		}
	}

	public class DamageCancelListener implements Listener {
		@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
		public void onEntityDamage(WeaponDamageEvent event) {
			if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Hero) || (event.getDamage() == 0)) {
				return;
			}
			if (event.getDamager().hasEffect("UnstoppableEffect")) {
				event.setCancelled(true);
				Messaging.send(((Hero)event.getDamager()).getPlayer(), "You cannot damage that target while Unstoppable!");
			}
		}

		@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
		public void onShootBow(EntityShootBowEvent event) {
			if (!(event.getEntity() instanceof Player)) {
				return;
			}
			if (plugin.getCharacterManager().getHero((Player)event.getEntity()).hasEffect("UnstoppableEffect")) {
				event.setCancelled(true);
				Messaging.send(((Player) event.getEntity()).getPlayer(), "You cannot shoot your bow while Unstoppable!");
			}
		}
	}
}