package com.atherys.skills.SkillRend;

import com.atherys.effects.BleedPeriodicDamageEffect;
import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillRend extends ActiveSkill {

    public SkillRend(Heroes plugin) {
        super(plugin, "Rend");
        setDescription("Active\n Your next melee attack will deal $1 more damage, slow your target for $2s and apply a DoT for $3s");
        setUsage("/skill Rend");
        setArgumentRange(0, 0);
        setIdentifiers("skill Rend");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new HeroDamageListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 5000L);
        node.set(SkillSetting.PERIOD.node(), 2500L);
        node.set("number-of-attacks", 1);
        node.set("slow-multiplier", 2);
        node.set("slow-duration", 3000L);
        node.set("add-slowminning-potion", true);
        node.set("bonus-damage", 15);
        node.set("dot-damage", 2.5D);
        node.set("dot-duration", 8000L);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 10, false);
        long slowDuration = (long) SkillConfigManager.getUseSetting(hero, this, "slow-duration", 3000L, false);
        long dotDuration = (long) SkillConfigManager.getUseSetting(hero, this, "dot-duration", 5000L, false);
        return getDescription().replace("$1", damage + "").replace("$2", slowDuration / 1000 + "").replace("$3", dotDuration / 1000 + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2500, false);
        int attacks = SkillConfigManager.getUseSetting(hero, this, "number-of-attacks", 1, false);
        int slowMultiplier = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 2, false);
        long slowDuration = (long) SkillConfigManager.getUseSetting(hero, this, "slow-duration", 3000L, false);
        boolean slowMining = SkillConfigManager.getUseSetting(hero, this, "add-slowminning-potion", true);
        double dotDamage = SkillConfigManager.getUseSetting(hero, this, "dot-damage", 2D, false);
        long dotDuration = (long) SkillConfigManager.getUseSetting(hero, this, "dot-duration", 5000L, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 15D, false);
        hero.addEffect(new RendEffect(this, duration, period, attacks, slowMultiplier, damage, slowDuration, slowMining, dotDamage, dotDuration,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    private class RendEffect extends ExpirableEffect {
        private int attacks;
        private double damage;
        private int slowMultiplier;
        private boolean slowMining;
        private long slowDuration;
        private double dotDamage;
        private long dotDuration;
        private long period;

        public RendEffect(Skill skill, long duration, long period, int attacks, int slowMultiplier, double damage, long slowDuration, boolean slowMining, double dotDamage, long dotDuration,Hero caster) {
            super(skill, "RendEffect",caster.getPlayer(), duration);
            this.attacks = attacks;
            this.damage = damage;
            this.slowMultiplier = slowMultiplier;
            this.slowDuration = slowDuration;
            this.slowMining = slowMining;
            this.dotDamage = dotDamage;
            this.dotDuration = dotDuration;
            this.period = period;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Rend expired.");
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }

        public double getDamage() {
            return damage;
        }

        public int getSlowMultiplier() {
            return slowMultiplier;
        }

        public long getSlowDuration() {
            return slowDuration;
        }

        public boolean addSlowMining() {
            return slowMining;
        }

        public double getDotDamage() {
            return dotDamage;
        }

        public long getDotDuration() {
            return dotDuration;
        }

        public long getPeriod() {
            return period;
        }
    }

    public class RendDoTEffect extends BleedPeriodicDamageEffect {
        public RendDoTEffect(Skill skill, long period, long duration, double damage, Player player, boolean stuff) {
            super(skill, "RendDoTEffect",player, period, duration, damage , stuff);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BLEED);
            this.types.add(EffectType.PHYSICAL);
        }
    }

    public class HeroDamageListener implements Listener {
        private final Skill skill;

        public HeroDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero) ||
                    (event.isCancelled()) ||
                    !(event.getEntity() instanceof Player) ||
                    (event.getDamage() == 0) ||
                    (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero target = plugin.getCharacterManager().getHero((Player) event.getEntity());
            Hero hero = (Hero) event.getDamager();
            if (!event.getDamager().hasEffect("RendEffect"))
                return;
            RendEffect he = (RendEffect) hero.getEffect("RendEffect");
            target.addEffect(new SlowEffect(skill, he.getSlowDuration(), he.getSlowMultiplier(), he.addSlowMining(), "", "", hero));
            target.addEffect(new RendDoTEffect(skill, he.getPeriod(), he.getDotDuration(), he.getDotDamage(), hero.getPlayer(), false));
            event.setDamage(event.getDamage() + he.getDamage());
            getAttacksLeft(hero, he);
        }

        private void getAttacksLeft(Hero hero, RendEffect he) {
            if (he.getAttacksLeft() <= 1) {
                hero.removeEffect(hero.getEffect("RendEffect"));
            } else {
                he.setAttacksLeft(he.getAttacksLeft() - 1);
            }
        }
    }
}
