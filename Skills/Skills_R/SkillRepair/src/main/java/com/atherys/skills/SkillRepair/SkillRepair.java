package com.atherys.skills.SkillRepair;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.MaterialUtil;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.Set;

public class SkillRepair extends ActiveSkill {
    String useText = null;

    public SkillRepair(Heroes plugin) {
        super(plugin, "Repair");
        setDescription("You are able to repair tools and armor. There is a $1% chance the item will be disenchanted. You may also use Anvils.");
        setUsage("/skill repair");
        setArgumentRange(0, 0);
        setIdentifiers("skill repair");
        setTypes(SkillType.KNOWLEDGE, SkillType.ITEM_MODIFYING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillListener(this), plugin);
    }

    public void init() {
        super.init();
        this.useText = SkillConfigManager.getRaw(this, SkillSetting.USE_TEXT, "%hero% repaired a %item%%ench%").replace("%hero%", "$1").replace("%item%", "$2").replace("%ench%", "$3");
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.USE_TEXT.node(), "%hero% repaired a %item%%ench%");
        node.set("wood", Integer.valueOf(1));
        node.set("stone", Integer.valueOf(1));
        node.set("leather", Integer.valueOf(1));
        node.set("chain", Integer.valueOf(1));
        node.set("iron", Integer.valueOf(1));
        node.set("gold", Integer.valueOf(1));
        node.set("diamond", Integer.valueOf(1));
        node.set("unchant-chance", Double.valueOf(0.5D));
        node.set("unchant-chance-reduce", Double.valueOf(0.005D));
        return node;
    }

    public String getDescription(Hero hero) {
        double unchant = SkillConfigManager.getUseSetting(hero, this, "unchant-chance", 0.5D, true);
        unchant -= SkillConfigManager.getUseSetting(hero, this, "unchant-chance-reduce", 0.005D, false) * hero.getSkillLevel(this);
        return getDescription().replace("$1", Util.stringDouble(unchant * 100.0D));
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        ItemStack is = player.getItemInHand();
        Material isType = is.getType();
        Material reagent = getRequiredReagent(isType);
        Block anvil = player.getTargetBlock((Set<Material>) null, 5);
        if (!anvil.getType().equals(Material.ANVIL)) {
            Messaging.send(player, "You have to target an Anvil!");
            return SkillResult.CANCELLED;
        }
        if (reagent == null) {
            Messaging.send(player, "You are not holding a repairable tool.");
            Messaging.send(player, String.valueOf(is.getType().ordinal()));
            return SkillResult.FAIL;
        }
        if (is.getDurability() == 0) {
            Messaging.send(player, "That item is already at full durability!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        ItemStack reagentStack = new ItemStack(reagent, getRepairCost(hero, is));
        if (!hasReagentCost(player, reagentStack)) {
            return new SkillResult(SkillResult.ResultType.MISSING_REAGENT, true, Integer.valueOf(reagentStack.getAmount()), MaterialUtil.getFriendlyName(reagentStack.getType()));
        }
        if (is.getItemMeta().hasLore()) {
            hero.getPlayer().sendMessage(ChatColor.RED + "You cannot repair special items");
            return SkillResult.CANCELLED;
        }
        boolean lost = false;
        boolean enchanted = !is.getEnchantments().isEmpty();
        if (enchanted) {
            double unchant = SkillConfigManager.getUseSetting(hero, this, "unchant-chance", 0.5D, true);
            unchant -= SkillConfigManager.getUseSetting(hero, this, "unchant-chance-reduce", 0.005D, false) * hero.getLevel();
            if (Util.nextRand() <= unchant) {
                for (Enchantment enchant : is.getEnchantments().keySet()) {
                    is.removeEnchantment(enchant);
                }
                lost = true;
            }
        }
        is.setDurability((short) 0);
        // For Wood items
        if (reagent == Material.WOOD) {
            if (player.getInventory().contains(new ItemStack(reagent, 1, (short) 0)))
                reagentStack.setDurability((short) 0);
            else if (player.getInventory().contains(new ItemStack(reagent, 1, (short) 1)))
                reagentStack.setDurability((short) 1);
            else if (player.getInventory().contains(new ItemStack(reagent, 1, (short) 2)))
                reagentStack.setDurability((short) 2);
            else if (player.getInventory().contains(new ItemStack(reagent, 1, (short) 3)))
                reagentStack.setDurability((short) 3);
            else
                return new SkillResult(SkillResult.ResultType.MISSING_REAGENT, true, Integer.valueOf(reagentStack.getAmount()), MaterialUtil.getFriendlyName(reagentStack.getType()));
        }
        player.getInventory().removeItem(reagentStack);
        Util.syncInventory(player, this.plugin);
        broadcast(player.getLocation(), this.useText, player.getDisplayName(), is.getType().name().toLowerCase().replace("_", " "), lost ? " and stripped it of enchantments!" : !enchanted ? "." : " and successfully kept the enchantments.");
        return SkillResult.NORMAL;
    }

    private int getRepairCost(Hero hero, ItemStack is) {
        Material mat = is.getType();
        switch (mat) {
            // Wood
            case WOOD_SWORD:
            case WOOD_SPADE:
            case WOOD_PICKAXE:
            case WOOD_AXE:
            case WOOD_HOE:
            case BOW:
                return SkillConfigManager.getUseSetting(hero, this, "wood", 1, true);
            // Stone
            case STONE_SWORD:
            case STONE_SPADE:
            case STONE_PICKAXE:
            case STONE_AXE:
            case STONE_HOE:
                return SkillConfigManager.getUseSetting(hero, this, "stone", 1, true);
            // Leather
            case LEATHER_HELMET:
            case LEATHER_CHESTPLATE:
            case LEATHER_LEGGINGS:
            case LEATHER_BOOTS:
                return SkillConfigManager.getUseSetting(hero, this, "leather", 1, true);
            // Chain
            case CHAINMAIL_HELMET:
            case CHAINMAIL_CHESTPLATE:
            case CHAINMAIL_LEGGINGS:
            case CHAINMAIL_BOOTS:
                return SkillConfigManager.getUseSetting(hero, this, "chain", 1, true);
            // Gold
            case GOLD_HELMET:
            case GOLD_CHESTPLATE:
            case GOLD_LEGGINGS:
            case GOLD_BOOTS:
            case GOLD_SWORD:
            case GOLD_SPADE:
            case GOLD_PICKAXE:
            case GOLD_AXE:
            case GOLD_HOE:
                return SkillConfigManager.getUseSetting(hero, this, "gold", 1, true);
            // Iron
            case IRON_HELMET:
            case IRON_CHESTPLATE:
            case IRON_LEGGINGS:
            case IRON_BOOTS:
            case IRON_SWORD:
            case IRON_SPADE:
            case IRON_PICKAXE:
            case IRON_AXE:
            case IRON_HOE:
                return SkillConfigManager.getUseSetting(hero, this, "iron", 1, true);
            // Diamond
            case DIAMOND_HELMET:
            case DIAMOND_CHESTPLATE:
            case DIAMOND_LEGGINGS:
            case DIAMOND_BOOTS:
            case DIAMOND_SWORD:
            case DIAMOND_SPADE:
            case DIAMOND_PICKAXE:
            case DIAMOND_AXE:
            case DIAMOND_HOE:
                return SkillConfigManager.getUseSetting(hero, this, "diamond", 1, true);
            default:
                return 1;
        }
    }

    private Material getRequiredReagent(Material material) {
        switch (material) {
            // Wood
            case WOOD_SWORD:
            case WOOD_SPADE:
            case WOOD_PICKAXE:
            case WOOD_AXE:
            case WOOD_HOE:
            case BOW:
            case FISHING_ROD:
                return Material.WOOD;
            // Stone
            case STONE_SWORD:
            case STONE_SPADE:
            case STONE_PICKAXE:
            case STONE_AXE:
            case STONE_HOE:
                return Material.COBBLESTONE;
            // Leather
            case LEATHER_HELMET:
            case LEATHER_CHESTPLATE:
            case LEATHER_LEGGINGS:
            case LEATHER_BOOTS:
                return Material.LEATHER;
            // Chain
            case CHAINMAIL_HELMET:
            case CHAINMAIL_CHESTPLATE:
            case CHAINMAIL_LEGGINGS:
            case CHAINMAIL_BOOTS:
                return Material.IRON_INGOT;
            // Gold
            case GOLD_HELMET:
            case GOLD_CHESTPLATE:
            case GOLD_LEGGINGS:
            case GOLD_BOOTS:
            case GOLD_SWORD:
            case GOLD_SPADE:
            case GOLD_PICKAXE:
            case GOLD_AXE:
            case GOLD_HOE:
                return Material.GOLD_INGOT;
            // Iron
            case IRON_HELMET:
            case IRON_CHESTPLATE:
            case IRON_LEGGINGS:
            case IRON_BOOTS:
            case IRON_SWORD:
            case IRON_SPADE:
            case IRON_PICKAXE:
            case IRON_AXE:
            case IRON_HOE:
            case SHEARS:
                return Material.IRON_INGOT;
            // Diamond
            case DIAMOND_HELMET:
            case DIAMOND_CHESTPLATE:
            case DIAMOND_LEGGINGS:
            case DIAMOND_BOOTS:
            case DIAMOND_SWORD:
            case DIAMOND_SPADE:
            case DIAMOND_PICKAXE:
            case DIAMOND_AXE:
            case DIAMOND_HOE:
                return Material.DIAMOND;
            default:
                return null;
        }
    }

    public class SkillListener implements Listener {
        private final Skill skill;

        public SkillListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.LOW)
        public void onPlayerInteract(PlayerInteractEvent event) {
            if ((event.getClickedBlock() == null) || (event.getClickedBlock().getType() != Material.ANVIL) || (event.getAction() != Action.RIGHT_CLICK_BLOCK)) {
                return;
            }
            RegisteredServiceProvider<Permission> rsp = Bukkit.getServer()
                    .getServicesManager()
                    .getRegistration(Permission.class);
            if (rsp != null) {
                String group = rsp.getProvider().getPlayerGroups(event.getPlayer())[0];
                if (group.equalsIgnoreCase("Magistrate")) return;
            }
            Messaging.send(event.getPlayer(), "You can't use Anvils!");
            event.setUseInteractedBlock(Event.Result.DENY);
        }
    }
}