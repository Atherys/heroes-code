package com.atherys.skills.SkillRepentance;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

public class SkillRepentance extends ActiveSkill {
    private String applyText;

    public SkillRepentance(Heroes plugin) {
        super(plugin, "Repentance");
        setDescription("Gives either yourself or a nearby party member with lower health a buff that returns damage when being hit.");
        setUsage("/skill Repentance");
        setArgumentRange(0, 0);
        setIdentifiers("skill Repentance");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new RepentanceListener(this), plugin);
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill% on %target%.").replace("%hero%", "$1").replace("%skill%", "$2").replace("%target%","$3");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        Hero targetHero = hero;
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        double returnPercentage = SkillConfigManager.getUseSetting(hero, this, "return-percentage", 0.75, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        if (hero.hasParty()) {
            for (Hero phero : hero.getParty().getMembers()) {
                if (player.getWorld().equals(phero.getPlayer().getWorld()) && (player.hasLineOfSight(phero.getPlayer())) && ((((phero.getPlayer()).getLocation()).distanceSquared(player.getLocation())) <= radius * radius) && (targetHero.getPlayer().getHealth() > phero.getPlayer().getHealth())) {
                    targetHero = phero;
                }
            }
        }

        RepentanceEffect repentanceEffect = new RepentanceEffect(this, duration, returnPercentage, hero);
        targetHero.addEffect(repentanceEffect);
        if (!targetHero.equals(hero)) {
            broadcast(hero.getPlayer().getLocation(), applyText, hero.getName(), "Repentance", targetHero.getName());
        } else {
            broadcastExecuteText(hero);
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 5000);
        node.set("return-percentage", 0.75);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set("max-damage", Double.valueOf(30));
        return node;
    }


    public class RepentanceEffect extends PeriodicExpirableEffect {
        private double returnPercentage;
        private Hero caster;
        private double damageDone;

        public RepentanceEffect(Skill skill, long duration, double returnPercentage, Hero caster) {
            super(skill, "Repentance",caster.getPlayer(), 300L, duration);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.MAGIC);
            this.returnPercentage = returnPercentage;
            this.caster = caster;
            this.damageDone = 0.0;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "Repentance expired!");
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (double[] coords : Lib.playerParticleCoords) {
                Location location = player.getLocation();
                location.add(coords[0], coords[1], coords[2]);
                location.getWorld().spawnParticle(Particle.SPELL_WITCH, location, 1 );
            }
        }

        @Override
        public void tickMonster(Monster monster) {

        }

        public double getReturnPercentage() {
            return returnPercentage;
        }

        public Hero getCaster() {
            return caster;
        }

        public double getDamageDone() { return damageDone; }

        public void setDamageDone(double damageDone) { this.damageDone = damageDone; }
    }

    public class RepentanceListener implements Listener {
        private final Skill skill;

        public RepentanceListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if ((!(event.getEntity() instanceof Player)) || (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            CharacterTemplate damager = event.getDamager();
            if (hero.hasEffect("Repentance")) {
                RepentanceEffect repentanceEffect = (RepentanceEffect) hero.getEffect("Repentance");
                if (damageCheck(hero.getPlayer(), damager.getEntity()) && damageCheck(repentanceEffect.getCaster().getPlayer(), damager.getEntity())) {
                    double maxdamage = SkillConfigManager.getUseSetting(repentanceEffect.getCaster(), skill, "max-damage", 30.0, false);
                    if (repentanceEffect.getDamageDone() != maxdamage) {
                        double damage = (((event.getDamage() * repentanceEffect.getReturnPercentage()) + repentanceEffect.getDamageDone()) < maxdamage) ? (event.getDamage() * repentanceEffect.getReturnPercentage()) : (maxdamage - repentanceEffect.getDamageDone());
                        addSpellTarget(damager.getEntity(), repentanceEffect.getCaster());
                        damageEntity(damager.getEntity(), repentanceEffect.getCaster().getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                        repentanceEffect.setDamageDone(repentanceEffect.getDamageDone() + damage);
                    }
                    Vector v = damager.getEntity().getLocation().toVector().subtract(event.getEntity().getLocation().toVector()).normalize();
                    double v1 = SkillConfigManager.getUseSetting(repentanceEffect.getCaster(), skill, "horizontal-vector", 2.5, false);
                    double v2 = SkillConfigManager.getUseSetting(repentanceEffect.getCaster(), skill, "vertical-vector", 0.5, false);
                    v = v.multiply(v1);
                    v = v.setY(v2);
                    damager.getEntity().setVelocity(v);
                }
            }
        }
    }

}