package me.apteryx.repulsionorb;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import me.apteryx.repulsionorb.event.EventPearlHit;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author apteryx
 * @time 10:27 PM
 * @since 10/19/2016
 */
public class RepulsionOrb extends ActiveSkill {

    private static RepulsionOrb repulsionOrb;
    private double damage, radius, horizontalVel, verticalVel;

    public RepulsionOrb(Heroes heroes) {
        super(heroes, "RepulsionOrb");
        this.setUsage("/skill repulsionorb");
        this.setArgumentRange(0, 0);
        this.setIdentifiers("skill repulsionorb");
        this.setTypes(SkillType.DISABLING, SkillType.VELOCITY_INCREASING, SkillType.SILENCEABLE);
        this.setDescription("Launch an orb that deals %damage% magic damage and knocks back enemies within %radius% blocks of the impact.");
        Bukkit.getPluginManager().registerEvents(new EventPearlHit(), this.plugin);
        if (repulsionOrb == null) {
           repulsionOrb = this;
        }

    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        damage = SkillConfigManager.getUseSetting(hero, this, "damage",3.0D, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 5.0D, false);
        horizontalVel = SkillConfigManager.getUseSetting(hero, this, "horizontal-knockback", 1.5D, false);
        verticalVel = SkillConfigManager.getUseSetting(hero, this, "vertical-knockback", 0.8D, false);
        EnderPearl enderPearl = hero.getPlayer().launchProjectile(EnderPearl.class);
        enderPearl.setMetadata("RepulsionOrb", new FixedMetadataValue(this.plugin, "Apteryx"));
        this.broadcastExecuteText(hero);
        enderPearl.setCustomName(hero.getName());
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return super.getDescription().replace("%damage%", String.valueOf(SkillConfigManager.getUseSetting(hero, this, "damage",3.0D, false))).replace("%radius%", String.valueOf(SkillConfigManager.getUseSetting(hero, this, "radius", 5.0D, false)));
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("radius", 5.0D);
        node.set("damage", 3.0D);
        node.set("horizontal-knockback", 1.5D);
        node.set("vertical-knockback", 0.8D);
        return node;
    }

    public static RepulsionOrb getInstance() {
        return repulsionOrb;
    }

    public double getDamage() {
        return damage;
    }

    public double getRadius() {
        return radius;
    }

    public double getHorizontalVel() {
        return horizontalVel;
    }

    public double getVerticalVel() {
        return verticalVel;
    }
}
