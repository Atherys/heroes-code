package me.apteryx.repulsionorb.event;

import com.herocraftonline.heroes.Heroes;
import me.apteryx.repulsionorb.RepulsionOrb;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

/**
 * @author apteryx
 * @time 11:37 PM
 * @since 10/19/2016
 */
public class EventPearlHit implements Listener {

    @EventHandler
    public void onPearlHit(ProjectileHitEvent event) {
        if (event.getEntity().hasMetadata("RepulsionOrb")) {
            //Bukkit.getLogger().info("EnderPearl is Projectile");
            event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1.0F, 1.0F);
            event.getEntity().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, event.getEntity().getLocation(), 2);
            event.getEntity().getNearbyEntities(RepulsionOrb.getInstance().getRadius(), RepulsionOrb.getInstance().getRadius(), RepulsionOrb.getInstance().getRadius()).stream().filter(entity -> entity instanceof LivingEntity).forEach(entity -> {
                if (!entity.getName().equalsIgnoreCase(event.getEntity().getCustomName())) {
                    RepulsionOrb.getInstance().damageEntity((LivingEntity) entity, (LivingEntity) Bukkit.getPlayer(event.getEntity().getCustomName()), RepulsionOrb.getInstance().getDamage(), EntityDamageEvent.DamageCause.MAGIC, false);
                    entity.setVelocity(knockBack(event.getEntity(), entity));
                }

            });
        }
    }

    private Vector knockBack(Entity object, Entity player) {
        Vector velocity = player.getVelocity().add(player.getLocation().toVector().subtract(object.getLocation().toVector()).normalize().multiply(RepulsionOrb.getInstance().getHorizontalVel()));
        velocity.setY(RepulsionOrb.getInstance().getVerticalVel());
        return velocity;
    }

}
