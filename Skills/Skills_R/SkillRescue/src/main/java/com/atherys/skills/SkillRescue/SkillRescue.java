package com.atherys.skills.SkillRescue;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SkillRescue extends TargettedSkill {
    public SkillRescue(Heroes plugin) {
        super(plugin, "Rescue");
        setDescription("Pulls target player to caster within $1 block range.");
        setUsage("/skill Rescue");
        setArgumentRange(0, 0);
        setIdentifiers("skill Rescue");
        setTypes(SkillType.HEALING, SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target instanceof Player) {
            if (!target.equals(player)) {
	            if (!hero.hasParty()){
		            return SkillResult.INVALID_TARGET;
	            }//party check
	            Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
	            if (hero.getParty().getMembers().contains(targetHero)) {

		            for (Effect effect : targetHero.getEffects()) {
			            if (((effect.isType(EffectType.DISABLE)) || (effect.isType(EffectType.SLOW)) || (effect.isType(EffectType.STUN)) || (effect.isType(EffectType.ROOT))) && (!effect.isType(EffectType.UNBREAKABLE))) {
				            targetHero.removeEffect(effect);
			            }
		            }
		            player.setPassenger(target);
		            int amplifier = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 1, false);
		            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
		            if (targetHero.hasEffect("Sneak")) {
			            targetHero.removeEffect(targetHero.getEffect("Sneak"));
		            }
		            targetHero.addEffect(new RescueEffect(this, duration, amplifier, player));
		            broadcastExecuteText(hero, target);
		            return SkillResult.NORMAL;
	            }
	            return SkillResult.INVALID_TARGET;
            }
            return SkillResult.CANCELLED;
        }
        return SkillResult.CANCELLED;
    }

    @Override
    public String getDescription(Hero hero) {
        int d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        return getDescription().replace("$1", d + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-multiplier", Integer.valueOf(1));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(5000));
        return node;
    }

    public class RescueEffect extends ExpirableEffect {
        private final Player caster;

        public RescueEffect(Skill skill, long duration, int amplifier, Player caster) {
            super(skill, "RescueEffect",caster, duration);
            this.caster = caster;
            caster.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, (int) (duration / 1000L) * 20, amplifier));
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                if (caster != null) {
                    caster.eject();
                }
            }
        }
    }
}
