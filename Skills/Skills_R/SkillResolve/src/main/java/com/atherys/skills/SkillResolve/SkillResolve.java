package com.atherys.skills.SkillResolve;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class SkillResolve extends PassiveSkill {

    public SkillResolve(Heroes plugin) {
        super(plugin, "Resolve");
        setDescription("Passive\nYou are immune to the kneebreak effect and you can not take more than 25 damage at a time.");
        setTypes(SkillType.DISABLE_COUNTERING, SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillResolveListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("max-damage", 25);
        return node;
    }

    public class SkillResolveListener implements Listener {
        private final Skill skill;

        public SkillResolveListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageByEntityEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Player player = (Player)event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("Resolve")) {
                double maxDamage = SkillConfigManager.getUseSetting(hero, skill, "max-damage", 25, false);
                double damage = event.getDamage();
                if (damage > maxDamage) {
                    event.setDamage(damage - 20 < maxDamage ? maxDamage : damage - 20);
                }
            }
        }
    }
}