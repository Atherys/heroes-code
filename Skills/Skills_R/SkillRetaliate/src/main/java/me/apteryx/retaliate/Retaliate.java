package me.apteryx.retaliate;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import me.apteryx.retaliate.effects.RetaliateEffect;
import me.apteryx.retaliate.events.RetaliateDamageEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author apteryx
 * @time 6:56 PM
 * @since 12/14/2016
 */
public class Retaliate extends ActiveSkill {

    private static Retaliate instance;
    private int speedMultiplier;
    private double reflectMultiplier;
    private long duration;

    public Retaliate(Heroes plugin) {
        super(plugin, "Retaliate");
        this.setIdentifiers("skill retaliate");
        this.setUsage("/skill retaliate");
        instance = this;
        setDescription("[Insert cool description here]");
        Bukkit.getPluginManager().registerEvents(new RetaliateDamageEvent(), this.plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        reflectMultiplier = SkillConfigManager.getUseSetting(hero, this, "reflect-multiplier", 1.0, false);
        speedMultiplier = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 3, false);
        duration = SkillConfigManager.getUseSetting(hero, this, "duration", 3500, false);
        if (!hero.hasEffect("RetaliateEffect")) {
            hero.addEffect(new RetaliateEffect(this, "RetaliateEffect", hero.getPlayer(), 20L, duration));
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        return SkillResult.FAIL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-multiplier", 3);
        node.set("reflect-multiplier", 1.0);
        node.set("duration", 3500);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription();//.replace("%reflect-multiplier%", SkillConfigManager.getUseSetting(hero, this, "reflect-multiplier", 1.0, false) * 100+"").replace("%duration%", SkillConfigManager.getUseSetting(hero, this, "duration", 3500, false) / 1000+"");
    }

    public static Retaliate getInstance() {
        return instance;
    }

    public int getSpeedMultiplier() {
        return speedMultiplier;
    }

    public double getReflectMultiplier() {
        return reflectMultiplier;
    }

    public long getDuration() {
        return duration;
    }
}
