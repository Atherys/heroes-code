package me.apteryx.retaliate.effects;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import me.apteryx.retaliate.Retaliate;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author apteryx
 * @time 8:00 PM
 * @since 12/14/2016
 */
public class RetaliateEffect extends PeriodicExpirableEffect {


    public RetaliateEffect(Skill skill, String name, Player applier, long period, long duration) {
        super(skill, name, applier, period, duration);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, (int)Retaliate.getInstance().getDuration() / 50, Retaliate.getInstance().getSpeedMultiplier()));
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        hero.getPlayer().removePotionEffect(PotionEffectType.SPEED);
    }

    @Override
    public void tickMonster(Monster monster) {

    }

    @Override
    public void tickHero(Hero hero) {
        for (double[] coords : Lib.playerParticleCoords) {
            Location location = hero.getPlayer().getLocation();
            location.add(coords[0], coords[1], coords[2]);
            hero.getPlayer().getWorld().spawnParticle(Particle.SPELL_WITCH, hero.getPlayer().getLocation(), 1);
        }
    }
}