package me.apteryx.retaliate.events;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import me.apteryx.retaliate.Retaliate;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author apteryx
 * @time 8:52 PM
 * @since 12/14/2016
 */
public class RetaliateDamageEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamageEvent(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (event.getDamager() instanceof Player) {
                if (hero.getParty() != null) {
                    if (hero.getParty().isPartyMember((Player) event.getDamager())) {
                        return;
                    }
                }
            }

            if (event.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
                if (hero.hasEffect("RetaliateEffect")) {
                    event.setCancelled(true);
                    Retaliate.getInstance().damageEntity((LivingEntity) event.getDamager(), (LivingEntity) event.getEntity(), event.getDamage() * Retaliate.getInstance().getReflectMultiplier(), EntityDamageEvent.DamageCause.MAGIC, true);
                }
            }
        }
    }

}