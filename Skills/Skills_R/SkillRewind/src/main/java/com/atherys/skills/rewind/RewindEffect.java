/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.rewind;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class RewindEffect extends PeriodicExpirableEffect {

    private Location origin;

    public RewindEffect(Skill skill, Player effected, Player caster, long duration) {
        super(skill, "RewindEffect", caster, 10, duration);
        this.origin = effected.getLocation();
        this.types.add(EffectType.DISPELLABLE);
        this.types.add(EffectType.MAGIC);
        setExpireText("\u00A7f " + caster.getName() + "\u00A77's \u00A7fRewind\u00A77 activated!");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (hero.getPlayer().isDead()) return;
        if (this.isExpired()) {
            Location loc = hero.getPlayer().getLocation();
            loc.getWorld().playEffect(loc, Effect.ENDER_SIGNAL, 10);
            hero.getPlayer().teleport(origin);
            origin.getWorld().playSound(origin, Sound.ENTITY_ENDERMEN_TELEPORT, 1F, 0.5F);
            loc.getWorld().playSound(loc, Sound.ENTITY_ENDERMEN_TELEPORT, 1F, 0.5F);
        }
    }

    @Override
    public void tickMonster(Monster monster) {

    }

    @Override
    public void tickHero(Hero hero) {
        if (hero.getPlayer().isDead()) super.removeFromHero(hero);
        origin.getWorld().playEffect(origin, Effect.ENDER_SIGNAL, 10);
        Location loc = hero.getPlayer().getLocation();
        loc.getWorld().playEffect(loc, Effect.ENDER_SIGNAL, 10);
    }
}
