/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.rewind;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SkillRewind extends PassiveSkill {

    public SkillRewind(Heroes plugin) {
        super(plugin, "Rewind");
        setArgumentRange(0, 0);
        setDescription("Enemies you melee are returned to that location after $1 seconds.");
        Bukkit.getPluginManager().registerEvents(new SkillRewindListener(this), plugin);
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, "duration", 25, false);
        return getDescription().replace("$1", String.valueOf(duration / 1000));
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.COOLDOWN.node(), Integer.valueOf(10000));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(3000));
        return node;
    }

}