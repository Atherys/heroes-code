/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.skills.rewind;

import com.atherys.core.Core;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import static com.herocraftonline.heroes.characters.skill.Skill.damageCheck;

public class SkillRewindListener implements Listener {
    private Skill skill;

    public SkillRewindListener(Skill skill) {
        this.skill = skill;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityDamage(WeaponDamageEvent event) {
        if ((!(event.getEntity() instanceof Player)) || (event.getDamage() == 0) || !(event.getAttackerEntity() instanceof Player)) return;
        Hero hero = Core.getHeroes().getCharacterManager().getHero((Player) event.getAttackerEntity());
        Hero target = Core.getHeroes().getCharacterManager().getHero((Player) event.getEntity());
        if (hero.hasEffect("Rewind")) {
            if (hero.getCooldown("Rewind") == null || hero.getCooldown("Rewind") <= System.currentTimeMillis()) {
                if (damageCheck(target.getPlayer(), (LivingEntity) hero.getPlayer())){
                    long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN, 10000, false);
                    long duration = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 3000, false);
                    hero.setCooldown("Rewind", cooldown + System.currentTimeMillis());
                    target.addEffect(new RewindEffect(skill, target.getPlayer(), hero.getPlayer(), duration));
                    skill.broadcast(
                            hero.getPlayer().getLocation(),
                            "    $1 used \u00A7fRewind\u00A77 on $2",
                            hero.getName(),
                            target.getName()
                    );
                }
            }
        }
    }

}