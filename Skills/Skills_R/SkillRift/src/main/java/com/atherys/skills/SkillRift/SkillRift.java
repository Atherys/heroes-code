package com.atherys.skills.SkillRift;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroJoinPartyEvent;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.party.HeroParty;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class SkillRift extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillRift(Heroes plugin) {
        super(plugin, "Rift");
        setDescription("Places Obelisk at target location for $1 seconds.");
        setArgumentRange(0, 0);
        setUsage("/skill Rift");
        setIdentifiers("skill Rift");
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 5000);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.AMOUNT.node(), 0.5);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        node.set("vertical-vector", 10);
        node.set("horizontal-vector", 10);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        return getDescription().replace("$1", d / 1000 + "");
    }

    private boolean isReplaceable(Material m) {
        return m == Material.AIR || m == Material.SNOW;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        // Player player = hero.getPlayer();
        if (hero.hasParty()) {
            for (final Hero ph : hero.getParty().getMembers()) {
                if (ph.hasEffect("Rift")) {
                    hero.getPlayer().sendMessage(ChatColor.GRAY + "Party member " + ChatColor.WHITE + ph.getPlayer().getName() + ChatColor.GRAY + " already has an active obelisk!");
                    return SkillResult.CANCELLED;
                }
            }
        }
        int m = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        final Block b = hero.getPlayer().getTargetBlock(Util.transparentBlocks, m);
        if (b.getType() == Material.AIR || b.getRelative(BlockFace.UP).getType() != Material.AIR) {
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        for (int x = 0; x <= 1; x++) {
            for (int z = 0; z <= 1; z++) {
                for (int y = 0; y <= 5; y++) {
                    Block bl = b.getRelative(x, y, z);
                    if (isReplaceable(bl.getType())) {
                        values.add(bl);
                        bl.setType(Material.STAINED_GLASS);
                        bl.setData((byte) 7);
                        bl.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
                    }
                }
            }
        }
        int r = SkillConfigManager.getUseSetting(hero, this, "radius", 10, true);
        long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        double v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 1, false);
        double v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 1, false);
        double amp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.5D, false);
        hero.addEffect(new RiftEffect(this, duration, hero.getPlayer(), b.getLocation(), r, amp, v1, v2, values));
        return SkillResult.NORMAL;
    }

    public class RiftEffect extends PeriodicExpirableEffect {
        private final Player caster;
        private final Location loc;
        private final int r;
        private final double mEffectReduction;
        private final double v1;
        private final double v2;
        private final Set<Block> values;

        public RiftEffect(Skill skill, long duration, Player caster, Location loc, int r, double amp, double v1, double v2, Set<Block> values) {
            super(skill, "Rift",caster, 1000L, duration);
            this.caster = caster;
            this.loc = loc;
            this.r = r;
            this.mEffectReduction = amp;
            this.v1 = v1;
            this.v2 = v2;
            this.values = values;
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.FORM);
            types.add(EffectType.UNBREAKABLE);

            caster.setMetadata( "Rift-Obelisk-Location", new FixedMetadataValue(Heroes.getInstance(), loc) );
            Bukkit.getScheduler().scheduleSyncDelayedTask(Heroes.getInstance(), new Runnable() {
                @Override
                public void run() {
                    caster.removeMetadata( "Rift-Obelisk-Location", Heroes.getInstance() );
                }
            }, duration/50 );
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), applyText, hero.getName(), "Rift");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), expireText, hero.getName(), "Rift");
            Lib.removeBlocks(values);
        }

        public Player getCaster() {
            return this.caster;
        }

        public Location getObeliskLocation() {
            return loc;
        }

        @Override
        public void tickMonster(Monster mnstr) {
        }

        public boolean garison(Player p) {
            for (Block x : values) {
                if (x.getRelative(BlockFace.NORTH, 1).getType().equals(Material.AIR)) {
                    if (x.getRelative(BlockFace.NORTH, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                        p.teleport(x.getRelative(BlockFace.NORTH, 1).getLocation());
                        return true;
                    }
                } else if (x.getRelative(BlockFace.SOUTH, 1).getType().equals(Material.AIR)) {
                    if (x.getRelative(BlockFace.SOUTH, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                        p.teleport(x.getRelative(BlockFace.SOUTH, 1).getLocation());
                        return true;
                    }
                } else if (x.getRelative(BlockFace.EAST, 1).getType().equals(Material.AIR)) {
                    if (x.getRelative(BlockFace.EAST, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                        p.teleport(x.getRelative(BlockFace.EAST, 1).getLocation());
                        return true;
                    }
                } else if (x.getRelative(BlockFace.WEST, 1).getType().equals(Material.AIR)) {
                    if (x.getRelative(BlockFace.WEST, 1).getRelative(0, 1, 0).getType().equals(Material.AIR)) {
                        p.teleport(x.getRelative(BlockFace.WEST, 1).getLocation());
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public void tickHero(Hero hero) {
            for (Entity e : loc.getWorld().getNearbyEntities(loc, r, r, r)) {
                if (e instanceof Player) {
                    Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                    if (tHero.equals(hero)) {
                        hero.addEffect(new MEffect(skill, getPeriod(), mEffectReduction,hero));
                    } else if (hero.hasParty() && hero.getParty().isPartyMember(tHero)) {
                        tHero.addEffect(new MEffect(skill, getPeriod(), mEffectReduction,hero));
                    } else if (damageCheck(hero.getPlayer(), (LivingEntity) tHero.getPlayer())) {
                        tHero.addEffect(new SlowEffect(skill, getPeriod() * 3, 2, true, "", "", hero));
                        if (hero.hasEffect("Pulse")) {
                            Vector v = tHero.getPlayer().getLocation().toVector().subtract(loc.toVector()).normalize();
                            v.multiply(v1);
                            v.setY(v2);
                            tHero.getPlayer().setVelocity(v);
                        }
                    }
                }
            }
        }
    }

    public class MEffect extends ExpirableEffect {
        private final double amp;

        public MEffect(Skill skill, long duration, double amp,Hero hero) {
            super(skill, "MEffect",hero.getPlayer(), duration);
            this.amp = amp;
            types.add(EffectType.BENEFICIAL);
        }

        public double getAmplifier() {
            return this.amp;
        }
    }

    public class HeroDamageListener implements Listener {

        @EventHandler(priority = EventPriority.NORMAL)
        public void onWeaponDamage(final WeaponDamageEvent event) {
            if (event.isCancelled() || (event.getDamage() == 0) || (!(event.getDamager().hasEffect("MEffect")))) {
                return;
            }
            MEffect m = (MEffect) event.getDamager().getEffect("MEffect");
            event.setDamage(event.getDamage() * m.getAmplifier());
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onPartyJoin(final HeroJoinPartyEvent event) {
            if (event.isCancelled()) return;
            final Hero h = event.getHero();
            if (!h.hasEffect("Rift"))
                return;
            final HeroParty p = event.getParty();
            for (Hero ph : p.getMembers()) {
                if (ph.hasEffect("Rift")) {
                    h.removeEffect(h.getEffect("Rift"));
                    break;
                }
            }
        }
    }
}