package com.atherys.skills.SkillRiptide;

import com.atherys.effects.WaveStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.google.common.collect.Sets;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillRiptide extends ActiveSkill {
    public SkillRiptide(Heroes plugin) {
        super(plugin, "Riptide");
        setDescription("Heal all allies in a cone in front of you and within $1 blocks instantly for $2 health. If you have 3 Wave stacks, remove all Wave stacks and push enemies in front of you back.");
        setUsage("/skill Riptide");
        setArgumentRange(0, 0);
        setIdentifiers("skill Riptide");
        setTypes(SkillType.HEALING, SkillType.SILENCEABLE, SkillType.FORCE);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        List<Entity> nearbyPlayers = new ArrayList<Entity>();
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 7D, false);
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if ((entity instanceof Player)) {
                if (hero.getParty() != null) {
                    if (hero.getParty().isPartyMember((Player) entity)) {
                        nearbyPlayers.add(entity);
                    }
                }
                if (damageCheck(player, (LivingEntity) entity)) {
                    nearbyPlayers.add(entity);
                }
            }
        }
        Vector playerVector = player.getLocation().toVector();
        float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
        boolean hasThreeWaveStacks = WaveStackEffect.chargeWave(hero);
        Vector locV = player.getLocation().toVector();
        double v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 4D, false);
        double v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 1D, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 40D, false);
        waveEffect(hero.getPlayer());
        if (hero.hasParty()) {
            for (Entity e : Lib.getEntitiesInCone(nearbyPlayers, playerVector, (float) radius, degrees, player.getLocation().getDirection())) {
                if (damageCheck(player, (LivingEntity) e)) {
                    Vector v = e.getLocation().toVector().subtract(locV).normalize();
                    v.multiply(v1);
                    v.setY(v2);
                    e.setVelocity(v);
                }
                else if (hasThreeWaveStacks && hero.getParty().isPartyMember((Player) e)) {
                    Hero pHero = plugin.getCharacterManager().getHero((Player) e);
                    Lib.healHero(pHero, heal, this, hero);
                }
            }
            if (hasThreeWaveStacks) Lib.healHero(hero, heal, this, hero);
            broadcastExecuteText(hero);
            if (hasThreeWaveStacks) {
                broadcast(player.getLocation(), "$1 expended their Wave stacks on $2!", player.getDisplayName(), "Riptide");
            }
            return SkillResult.NORMAL;
        }
        //the bottom is separated from the top by the return statement 2 lines above
        broadcastExecuteText(hero);
        if (hasThreeWaveStacks) {
            Lib.healHero(hero, heal, this, hero);
            broadcast(player.getLocation(), "$1 expended their Wave stacks on $2!", player.getDisplayName(), "Riptide");
        }
        for (Entity e : Lib.getEntitiesInCone(nearbyPlayers, playerVector, (float) radius, degrees, player.getLocation().getDirection())) {
            if (e instanceof Player) {
                if (damageCheck(player, (LivingEntity) e)) {
                    Vector v = e.getLocation().toVector().subtract(locV).normalize();
                    v.multiply(v1);
                    v.setY(v2);
                    e.setVelocity(v);
                }
            }
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 7D, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 40D, false);
        return getDescription().replace("$1", (int) radius + "").replace("$2", (int) heal + " ") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(7));
        node.set("degrees", Float.valueOf(45));
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(40));
        node.set("horizontal-vector", Double.valueOf(4));
        node.set("vertical-vector", Double.valueOf(1));
        return node;
    }

    private void waveEffect(Player player) {
        Bukkit.getOnlinePlayers().stream().filter(player::canSee).forEach(p -> {
            p.playSound(player.getLocation(), Sound.ENTITY_PLAYER_SWIM, 2F, 0.5F);
            p.playSound(player.getLocation(), Sound.ENTITY_PLAYER_SPLASH, 2F, 0.5F);
        });
        List<Block> blocks = player.getLineOfSight(Sets.newHashSet(Material.AIR), 8);
        blocks.forEach(block -> {
            Location loc = block.getLocation();
            loc.setY(player.getLocation().getY() + 0.25);
            loc.setX(loc.getX() + 1);
            loc.setZ(loc.getZ() + 1);
            loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 20, 200, 200, 255);
            loc.getWorld().spawnParticle(Particle.WATER_SPLASH, loc, 10, 3, 0, 3);
            loc.getWorld().spawnParticle(Particle.WATER_WAKE, loc, 10, 3, 0, 3);
        });
    }

}