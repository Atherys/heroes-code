package com.atherys.skills.SkillRoar;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillRoar extends ActiveSkill {
    public SkillRoar(Heroes plugin) {
        super(plugin, "Roar");
        setDescription("All enemies in a cone infront of you are pushed back.");
        setUsage("/skill Roar");
        setArgumentRange(0, 0);
        setIdentifiers("skill Roar");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        List<Entity> nearbyPlayers = new ArrayList<Entity>();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if ((entity instanceof Player)) {
                if (damageCheck(player, (LivingEntity) entity)) {
                    nearbyPlayers.add(entity);
                }
            }
        }
        Vector playerVector = player.getLocation().toVector();
        float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        double c = SkillConfigManager.getUseSetting(hero, this, "coeficient", 0.2, false);
        for (Entity e : Lib.getEntitiesInCone(nearbyPlayers, playerVector, (float) radius, degrees, player.getLocation().getDirection())) {
            LivingEntity le = (LivingEntity) e;
            addSpellTarget(le, hero);
            damageEntity(le, player, damage, DamageCause.ENTITY_ATTACK);
            double i = le.getLocation().distance(player.getLocation());
            double cc = (radius - i) * c;
            Vector v = le.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
            v = v.multiply(cc);
            v.setY(1);
            le.setVelocity(v);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10));
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set("coeficient", 0.2);
        node.set("degrees", 45);
        return node;
    }
}