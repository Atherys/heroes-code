package com.atherys.skills.SkillRoot;

import com.atherys.effects.RootEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillRoot extends TargettedSkill {
    private String expireText;

    public SkillRoot(Heroes plugin) {
        super(plugin, "Root");
        setDescription("Root your target in place for $1 seconds. This root will break if the target recieves damage.");
        setUsage("/skill root");
        setArgumentRange(0, 0);
        setIdentifiers("skill root");
        setTypes(SkillType.MOVEMENT_INCREASING, SkillType.DEBUFFING, SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_EARTH, SkillType.DAMAGING);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.EXPIRE_TEXT.node(), "Root faded from %target%!");
        return node;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public void init() {
        super.init();
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT.node(), "Root faded from %target%!").replace("%target%", "$1");
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        CharacterTemplate characterTemplate = this.plugin.getCharacterManager().getCharacter(target);
        this.plugin.getCharacterManager().getCharacter(target).addEffect(new RootEffect(this, duration,hero));
        if (target instanceof Player) {
            Lib.cancelDelayedSkill((Hero) characterTemplate);
        }
        // hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ZOMBIE_WOODBREAK, 1.0F, 1.0F); // uncomment if you want this sound
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
}