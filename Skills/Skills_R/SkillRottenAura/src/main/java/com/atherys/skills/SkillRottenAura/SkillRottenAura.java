package com.atherys.skills.SkillRottenAura;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillRottenAura extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillRottenAura(Heroes plugin) {
        super(plugin, "RottenAura");
        setDescription("Toggle-able aura that applies hunger and nausea to nearby enemies.");
        setUsage("/skill RottenAura");
        setArgumentRange(0, 0);
        setIdentifiers("skill RottenAura");
        setTypes(SkillType.SILENCEABLE, SkillType.AREA_OF_EFFECT, SkillType.BUFFING, SkillType.DEBUFFING);
    }

    public String getDescription(Hero hero) {
        return getDescription();
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("on-text", "%hero%'s %skill% creates a foul stench!");
        node.set("off-text", "%hero%'s %skill% dissipates!");
        node.set("hunger-duration", 5000);
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set("inner-radius", 5);
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, "on-text", "%hero%'s %skill% creates a foul stench!").replace("%hero%", "$1").replace("%skill%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, "off-text", "%hero%'s %skill% dissipates!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    public SkillResult use(Hero hero, String[] args) {
        if (hero.hasEffect("RottenAura")) {
            hero.removeEffect(hero.getEffect("RottenAura"));
        } else {
            long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 5000, false);
            int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
            int innerRadius = SkillConfigManager.getUseSetting(hero, this, "inner-radius", 5, false);
            long hungerDuration = SkillConfigManager.getUseSetting(hero, this, "hunger-duration", 5000, false);
            hero.addEffect(new RottenAuraEffect(this, period, radius, innerRadius, hungerDuration));
        }
        return SkillResult.NORMAL;
    }

    public class RottenAuraEffect extends PeriodicEffect {
        private int radius;
        private int innerRadius;
        private long hungerDuration;
        private Skill skill;

        public RottenAuraEffect(Skill skill, long period, int radius, int innerRadius, long hungerDuration) {
            super(skill, "RottenAura", period);
            this.skill = skill;
            this.radius = radius;
            this.innerRadius = innerRadius;
            this.hungerDuration = hungerDuration;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.UNBREAKABLE);
            this.types.add(EffectType.HUNGER);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "RottenAura");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "RottenAura");
        }

        @Override
        public void tickHero(Hero hero) {
            super.tickHero(hero);
            Player player = hero.getPlayer();
            for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
                if (((entity instanceof LivingEntity)) && (damageCheck(player, (LivingEntity) entity))) {
                    CharacterTemplate character = plugin.getCharacterManager().getCharacter((LivingEntity) entity);
                    addSpellTarget(entity, hero);
                    boolean close = entity.getLocation().distanceSquared(player.getLocation()) <= innerRadius * innerRadius;
                    RottenEffect rottenEffect = new RottenEffect(skill, hungerDuration, close,hero);
                    character.addEffect(rottenEffect);
                }
            }
        }
    }

    public class RottenEffect extends ExpirableEffect {
        public RottenEffect(Skill skill, long hungerDuration, boolean close,Hero hero) {
            super(skill, "RottenEffect",hero.getPlayer(), hungerDuration);
            addMobEffect(17, (int) hungerDuration / 50, close ? 1 : 0, false);
        }
    }
}
