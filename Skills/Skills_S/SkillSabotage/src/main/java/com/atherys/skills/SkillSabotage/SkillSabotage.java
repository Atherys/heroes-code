package com.atherys.skills.SkillSabotage;

import com.atherys.heroesaddon.util.Lib;
import com.atherys.heroesaddon.util.Util;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillSabotage extends ActiveSkill {

    public SkillSabotage(Heroes plugin) {
        super(plugin, "Sabotage");
        setDescription("Place a time bomb which detonates after $1s, dealing $2 damage in a $3 block radius.");
        setUsage("/skill Sabotage");
        setArgumentRange(0, 0);
        setIdentifiers("skill Sabotage");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 3000);
        node.set(SkillSetting.DAMAGE.node(), 50);
        node.set(SkillSetting.RADIUS.node(), 5);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 3000, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 50, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", damage + "").replace("$3", r + "");
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player p = hero.getPlayer();
        HashSet<Byte> transparent = new HashSet<>();
        transparent.add((byte) Material.AIR.getId());
        transparent.add((byte) Material.SNOW.getId());
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block b = p.getTargetBlock(transparent, max);
        if (b.getType() == Material.AIR) {
            p.sendMessage(ChatColor.GRAY + "You must target a block.");
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        Block b1 = b.getRelative(BlockFace.UP);
        if (!(b1.getType() == Material.AIR || b1.getType() == Material.SNOW)) {
            p.sendMessage(ChatColor.GRAY + "Cannot be placed here");
            return SkillResult.CANCELLED;
        }
        values.add(b1);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 3000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 50, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        b1.setType(Material.REDSTONE_LAMP_ON);
        b1.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
        hero.addEffect(new SabotageEffect(this, duration, damage, r, b1, values,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SabotageEffect extends PeriodicExpirableEffect {
        private final double damage;
        private final int radius;
        private final Block b;
        private final Set<Block> values;
        private boolean isLit;

        public SabotageEffect(Skill skill, long duration, double damage, int radius, Block b, Set<Block> values,Hero hero) {
            super(skill, "SabotageEffect",hero.getPlayer(), 500L, duration);
            this.damage = damage;
            this.radius = radius;
            this.b = b;
            this.values = values;
            this.isLit = false;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.FORM);
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (this.isExpired()) {
                for (Entity e : b.getWorld().getNearbyEntities(b.getLocation(), radius, radius, radius)) {
                    if (e instanceof LivingEntity) {
                        if ((e instanceof Player) && (hero.getPlayer().equals(e))) {
                            if (!(hero.hasEffect("Invuln") || hero.hasEffect("DivineInterEffect"))) {
                                addSpellTarget(e, hero);
                                damageEntity((LivingEntity) e, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.BLOCK_EXPLOSION);
                            }
                        } else if (damageCheck(hero.getPlayer(), (LivingEntity) e)) {
                            addSpellTarget(e, hero);
                            damageEntity((LivingEntity) e, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.BLOCK_EXPLOSION);
                        }

                    }
                }

                Location loc = b.getLocation();
                loc.getWorld().playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 1.5F, 1F);
                loc.getWorld().spawnParticle(Particle.EXPLOSION_HUGE, loc, 1 );
                loc.getWorld().spawnParticle(Particle.CLOUD, loc, 5, 5, 3, 5);
                loc.getWorld().spawnParticle(Particle.SMOKE_LARGE, loc, 3, 3, 2, 3);
            }
            Lib.removeBlocks(values);
        }

        @Override
        public void tickHero(Hero hero) {
            boolean cont = true;
            for (Block x : values) {
                if (!(x.getType().equals(Material.REDSTONE_LAMP_OFF) || x.getType().equals(Material.REDSTONE_LAMP_ON))) {
                    cont = false;
                    break;
                }
            }
            if (cont) {
                if (isLit) {
                    try {
                        for (Block b : values) {
                            Util.switchLamp(b, false);
                        }
                        isLit = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        for (Block b : values) {
                            Util.switchLamp(b, true);
                        }
                        isLit = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public void tickMonster(Monster mnstr) {
        }
    }

}