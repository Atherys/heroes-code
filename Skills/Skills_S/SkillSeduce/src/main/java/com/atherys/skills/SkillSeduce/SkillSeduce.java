package com.atherys.skills.SkillSeduce;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;


/**
 * Created by Arthur on 10/14/2015.
 */
public class SkillSeduce extends TargettedSkill {

    public SkillSeduce(Heroes plugin) {
        super(plugin, "Seduce");
        setUsage("/skill Seduce");
        setArgumentRange(0, 0);
        setIdentifiers("skill Seduce");
        setDescription("Removes target's armor for a duration.");
        Bukkit.getPluginManager().registerEvents(new SeduceListener(), plugin);
    }

    public String getDescription(Hero hero) {
        return getDescription();
    }

    public void init() {
        super.init();
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(10));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(5000));
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] arg1) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.CANCELLED;
        }
        Hero tHero = plugin.getCharacterManager().getHero((Player) target);
        if (!tHero.hasEffect("SeduceEffect")) {
            if (!target.equals(player)) {
                if (damageCheck(player, target)) {
                    long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
                    tHero.addEffect(new SeduceEffect(this, duration, (Player) target, player));
                    target.getWorld().playSound(target.getLocation(), Sound.ENTITY_DONKEY_DEATH, 1F, 1F);
                    broadcastExecuteText(hero, target);
                    return SkillResult.NORMAL;
                }
                return SkillResult.CANCELLED;
            }
            return SkillResult.CANCELLED;
        }
        return SkillResult.CANCELLED;
    }

    public class SeduceEffect extends ExpirableEffect {
        private ItemStack[] armorStack;
        private Player caster;
        //ItemStack[] itemStack;

        public SeduceEffect(Skill skill, long duration, Player victim, Player caster) {
            super(skill, "SeduceEffect", caster, duration);
            this.armorStack = victim.getInventory().getArmorContents();
            this.caster = caster;
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.PHYSICAL); //kappa ʕ•ᴥ•ʔ
        }

        @Override
        @Deprecated
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player tPlayer = hero.getPlayer();
            ItemStack airStack = new ItemStack(Material.AIR);
            int invSpace = CheckSpace(tPlayer);
            if (invSpace >= 4) {
                if (!(tPlayer.getInventory().getBoots()==(null))){
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getBoots());
                    tPlayer.getInventory().setBoots(airStack);
                }
                if (!(tPlayer.getInventory().getLeggings()==(null))){
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getLeggings());
                    tPlayer.getInventory().setLeggings(airStack);
                }
                if (!(tPlayer.getInventory().getChestplate()==(null))){
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getChestplate());
                    tPlayer.getInventory().setChestplate(airStack);
                }
                if (!(tPlayer.getInventory().getHelmet()==(null))){
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getHelmet());
                    tPlayer.getInventory().setHelmet(airStack);
                }

            }else if (invSpace >= 3) {
                if (!(tPlayer.getInventory().getBoots()==(null))){
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getBoots());
                    tPlayer.getInventory().setBoots(airStack);
                }
                if (!(tPlayer.getInventory().getLeggings()==(null))){
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getLeggings());
                    tPlayer.getInventory().setLeggings(airStack);
                }
                if (!(tPlayer.getInventory().getChestplate()==(null))){
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getChestplate());
                    tPlayer.getInventory().setChestplate(airStack);
                }

            }else if (invSpace >= 2) {
                if (!(tPlayer.getInventory().getBoots()==(null))) {
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getBoots());
                    tPlayer.getInventory().setBoots(airStack);
                }
                if (!(tPlayer.getInventory().getLeggings()==(null))) {
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getLeggings());
                    tPlayer.getInventory().setLeggings(airStack);
                }
            }else if (invSpace >= 1) {
                if (!(tPlayer.getInventory().getBoots()==(null))) {
                    tPlayer.getInventory().addItem(tPlayer.getInventory().getBoots());
                    tPlayer.getInventory().setBoots(airStack);
                }
            } else {
                caster.sendMessage( "Target's inventory is full!");
            }

        }


        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            /*
            Player tPlayer = hero.getPlayer();
            removeArmorinv(tPlayer, armorStack,tPlayer.getInventory().getContents());
            for (ItemStack i : armorStack) {
                //boots
                if ((i.getType().equals(Material.DIAMOND_BOOTS)) || (i.getType().equals(Material.CHAINMAIL_BOOTS)) ||
                        (i.getType().equals(Material.GOLD_BOOTS)) || (i.getType().equals(Material.IRON_BOOTS)) ||
                        (i.getType().equals(Material.LEATHER_BOOTS))) {
                    tPlayer.getInventory().setBoots(i);
                }
                //pants
                if ((i.getType().equals(Material.DIAMOND_LEGGINGS)) || (i.getType().equals(Material.CHAINMAIL_LEGGINGS)) ||
                        (i.getType().equals(Material.GOLD_LEGGINGS)) || (i.getType().equals(Material.IRON_LEGGINGS)) ||
                        (i.getType().equals(Material.LEATHER_LEGGINGS))) {
                    tPlayer.getInventory().setLeggings(i);
                }
                //chestplate
                if ((i.getType().equals(Material.DIAMOND_CHESTPLATE)) || (i.getType().equals(Material.CHAINMAIL_CHESTPLATE)) ||
                        (i.getType().equals(Material.GOLD_CHESTPLATE)) || (i.getType().equals(Material.IRON_CHESTPLATE)) ||
                        (i.getType().equals(Material.LEATHER_CHESTPLATE))) {
                    tPlayer.getInventory().setChestplate(i);
                }
                //hat
                if ((i.getType().equals(Material.DIAMOND_HELMET)) || (i.getType().equals(Material.CHAINMAIL_HELMET)) ||
                        (i.getType().equals(Material.GOLD_HELMET)) || (i.getType().equals(Material.IRON_HELMET)) ||
                        (i.getType().equals(Material.LEATHER_HELMET))) {
                    tPlayer.getInventory().setHelmet(i);
                }
            }
*/
        }

        public void removeArmorinv(Player player, ItemStack[] itemStacks, ItemStack[] contents) {
            for (ItemStack armor : itemStacks) {
                for (ItemStack content : contents) {
                    if (content.equals(armor)) {
                        player.getInventory().removeItem(content);
                        player.updateInventory();
                    }
                }
            }
        }

        public int CheckSpace(Player player) {
            int space = 0;

            for (ItemStack content : player.getInventory().getContents()) {
                if (content == null) {
                    space++;
                }
            }
            return space;
        }
    }

    public class SeduceListener implements Listener {
        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onArmorEquip(InventoryClickEvent event) {
            Player player = (Player) event.getWhoClicked();
            if (plugin.getCharacterManager().getHero((Player) event.getWhoClicked()).hasEffect("SeduceEffect")) {
                ClickType x = event.getClick();
                if (event.getCurrentItem() == null) {
                    return;
                }
                //String y = event.getCurrentItem().getType().name();
                if (event.getSlotType().equals(InventoryType.SlotType.ARMOR) || ((x.equals(ClickType.SHIFT_LEFT) || x.equals(ClickType.SHIFT_RIGHT))
                        && (Util.isArmor(event.getCurrentItem().getType())))) {
                    event.setCancelled(true);
                }
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onPlayerInteract(PlayerInteractEvent event) {
            Player player = event.getPlayer();
            Action action = event.getAction();
            if (plugin.getCharacterManager().getHero(player).hasEffect("SeduceEffect") && (player.getInventory().getItemInMainHand() != null)) {
                //String y = player.getInventory().getItemInMainHand().getType().name();
                if (((action.equals(Action.RIGHT_CLICK_BLOCK)) || (action.equals(Action.RIGHT_CLICK_AIR)))
                        && ((Util.isArmor(player.getInventory().getItemInMainHand().getType())))
                           || (Util.isArmor(player.getInventory().getItemInOffHand().getType()))) {
                    event.setUseItemInHand(Event.Result.DENY);
                    player.updateInventory();
                }
            }
        }
    }
}