package com.atherys.skills.SkillSelflessness;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SkillSelflessness extends PassiveSkill {
    public SkillSelflessness(Heroes plugin) {
        super(plugin, "Selflessness");
        setDescription("Self healing is only $1% as effective.");
        Bukkit.getServer().getPluginManager().registerEvents(new HealListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("multiplier", 0.5D);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double m = SkillConfigManager.getUseSetting(hero, this, "multiplier", 0.5D, false);
        return getDescription().replace("$1", m * 100 + "");
    }

    public class HealListener implements Listener {
        private final Skill skill;

        public HealListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onPlayerHeal(HeroRegainHealthEvent event) {
            if ((!(event.getHero().hasEffect("Selflessness"))) || !(event.getHero().hasAccessToSkill("Selflessness")) || (!(event.getHero().equals(event.getHealer())))) {
                return;
            }
            double m = SkillConfigManager.getUseSetting(event.getHero(), skill, "multiplier", 0.5D, false);
            event.setAmount((int) (event.getDelta() * m));
        }
    }
}
