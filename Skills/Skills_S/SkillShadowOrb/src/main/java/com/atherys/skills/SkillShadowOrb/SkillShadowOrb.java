package com.atherys.skills.SkillShadowOrb;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SkillShadowOrb extends ActiveSkill {

    public SkillShadowOrb(Heroes plugin) {
        super(plugin, "ShadowOrb");
        setDescription("Shoots poisoned enderpearl.");
        setUsage("/skill ShadowOrb");
        setArgumentRange(0, 0);
        setIdentifiers("skill ShadowOrb");
        setTypes(SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(500));
        node.set("velocity-multiplier", Double.valueOf(1.5D));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        EnderPearl ep = player.launchProjectile(EnderPearl.class);
        ep.setMetadata("ShadowOrbEnderPearl", new FixedMetadataValue(plugin, hero));
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 1.5D, false);
        ep.setVelocity(ep.getVelocity().multiply(mult));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class DespairEffect extends ExpirableEffect {
        public DespairEffect(Skill skill, long duration,Hero hero) {
            super(skill, "Despair",hero.getPlayer(), duration);
            PotionEffect potionEffect= new PotionEffect(PotionEffectType.BLINDNESS, (int) (duration / 1000L * 20L), 3, false);
            addPotionEffect(potionEffect,false);
            //addMobEffect(15, (int) (duration / 1000L * 20L), 3, false);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BLIND);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }
    }

    public class SkillEntityListener
            implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onArcaneOrbLand(ProjectileHitEvent event) {
            Entity x = event.getEntity();
            Location z = x.getLocation();
            if (!(x instanceof EnderPearl) || !x.hasMetadata("ShadowOrbEnderPearl")) {
                return;
            }
            Hero shooter = (Hero) x.getMetadata("ShadowOrbEnderPearl").get(0).value();
            if (shooter.getPlayer() == null) {
                return;
            }
            double radius = SkillConfigManager.getUseSetting(shooter, skill, "explosion-radius", 3.0D, false);
            double damage = SkillConfigManager.getUseSetting(shooter, skill, SkillSetting.DAMAGE, 10, false);
            long blindDuration = SkillConfigManager.getUseSetting(shooter, skill, SkillSetting.DURATION, 500, false);
            z.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, z, 1);
	        z.getWorld().playSound(z, Sound.ENTITY_GENERIC_EXPLODE, 1F, 1F);
            boolean hasHit = false;
            for (Entity y : x.getWorld().getNearbyEntities(z, radius, radius, radius)) {
                if (y instanceof LivingEntity) {
                    LivingEntity livingEntity = (LivingEntity) y;
                    if (damageCheck(shooter.getPlayer(), livingEntity)) {
                        addSpellTarget(y, shooter);
                        skill.damageEntity(livingEntity, shooter.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                        if (livingEntity instanceof  Player){
                            Hero tHero = plugin.getCharacterManager().getHero((Player) livingEntity);
                            tHero.addEffect(new DespairEffect(skill, blindDuration,tHero));
                        }

                        hasHit = true;
                    }
                }
            }
            if (hasHit) {
                Lib.projectileHit(shooter.getPlayer());
            }
            x.remove();
        }
    }
}