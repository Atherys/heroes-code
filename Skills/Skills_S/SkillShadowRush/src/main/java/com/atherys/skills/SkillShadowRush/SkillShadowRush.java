package com.atherys.skills.SkillShadowRush;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.common.SafeFallEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillShadowRush extends ActiveSkill {
    public SkillShadowRush(Heroes plugin) {
        super(plugin, "ShadowRush");
        setDescription("Active\nDuring its duration, activate this skill to allow up to five short-range blinks.");
        setUsage("/skill ShadowRush");
        setArgumentRange(0, 0);
        setIdentifiers("skill ShadowRush");
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("number-of-blinks", Integer.valueOf(3));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(6));
	    node.set("safefall-Duration",Long.valueOf(3000));
        return node;
    }

    public boolean blink(Hero hero, int distance) {
        Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return false;
        }
        Block block = Lib.getTargetBlock(player, distance);
        if (block != null) {
            Location teleport;
            if (Util.transparentBlocks.contains(block.getRelative(BlockFace.UP).getType())) {
                teleport = block.getLocation().clone();
            } else {
                teleport = block.getRelative(BlockFace.DOWN).getLocation().clone();
            }
            if (!player.getLocation().getBlock().equals(teleport.getBlock()) && !player.getLocation().getBlock().getRelative(BlockFace.UP).equals(teleport.getBlock())) {
	            final long safefallDur = SkillConfigManager.getUseSetting(hero, this, "safefall-Duration", 3000, false);
	            hero.getPlayer().getWorld().playSound(hero.getPlayer().getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1.0F, 1.0F);
	            hero.getPlayer().getWorld().playEffect(hero.getPlayer().getLocation(), org.bukkit.Effect.ENDER_SIGNAL, 55);
	            hero.addEffect(new SafeFallEffect(this,"ShadowrushSafefall",hero.getPlayer(),safefallDur));
                teleport.add(0.5, 0, 0.5);
                teleport.setPitch(player.getLocation().getPitch());
                teleport.setYaw(player.getLocation().getYaw());
                player.teleport(teleport);
                return true;
            }
        }
        Messaging.send(player, "No location to blink to.");
        return false;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (hero.getCooldown("ShadowRushMiniCooldown") == null || hero.getCooldown("ShadowRushMiniCooldown") <= System.currentTimeMillis()) {
            hero.setCooldown("ShadowRushMiniCooldown", System.currentTimeMillis() + 500);
        } else {
            Messaging.send(hero.getPlayer(), "Wait 0.5 more seconds before recasting the skill.");
            return SkillResult.CANCELLED;
        }
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        if (!blink(hero, distance)) {
            return SkillResult.CANCELLED;
        }
        DoubleBlinkEffect effect;
        if (!hero.hasEffect("DoubleBlinkEffect")) {
            effect = new DoubleBlinkEffect(this, nb);
            hero.addEffect(effect);
        } else {
            effect = (DoubleBlinkEffect) hero.getEffect("DoubleBlinkEffect");
        }
        effect.decBlinksLeft();
        if (effect.getBlinksLeft() >= 1) {
            Messaging.send(hero.getPlayer(), "$1 " + (effect.getBlinksLeft() == 1 ? "blink" : "blinks") + " remaining.", effect.getBlinksLeft());
            return SkillResult.SKIP_POST_USAGE;
        } else {
            Messaging.send(hero.getPlayer(), "You have no more blinks left.");
            hero.removeEffect(effect);
            return SkillResult.NORMAL;
        }
    }

    @Override
    public String getDescription(Hero hero) {
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        return getDescription().replace("$1", distance + "").replace("$2", nb - 1 + "") + Lib.getSkillCostStats(hero, this);
    }

    public class DoubleBlinkEffect extends Effect {
        private int nb;

        public DoubleBlinkEffect(Skill skill, int nb) {
            super(skill, "DoubleBlinkEffect");
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.UNBREAKABLE);
            this.nb = nb;
        }

        public int getBlinksLeft() {
            return nb;
        }

        public void decBlinksLeft() {
            nb--;
        }
    }
}