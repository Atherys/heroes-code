package com.atherys.skills.SkillShadowStep;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.SafeFallEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;

public class SkillShadowStep extends ActiveSkill {

    public SkillShadowStep(Heroes plugin) {
        super(plugin, "ShadowStep");
        setDescription("Hero blinks $1 blocks forward and has $2 seconds to reactivate to be teleported back to original location.");
        setUsage("/skill ShadowStep");
        setArgumentRange(0, 0);
        setIdentifiers("skill ShadowStep");
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(7000));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(6));
	    node.set("safefall-Duration",Long.valueOf(3000));
	    return node;
    }

    public boolean blink(Hero hero, int distance) {
        final Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return false;
        }
        Block prev = null;
        BlockIterator iter = null;
        try {
            iter = new BlockIterator(player, distance);
        } catch (IllegalStateException e) {
            Messaging.send(player, "There was an error getting your blink location!");
            return false;
        }
        while (iter.hasNext()) {
            Block b = iter.next();
            if ((!Util.transparentBlocks.contains(b.getType())) || ((!Util.transparentBlocks.contains(b.getRelative(BlockFace.UP).getType())) && (!Util.transparentBlocks.contains(b.getRelative(BlockFace.DOWN).getType()))))
                break;
            prev = b;
        }
        if (prev != null) {
            Location teleport = prev.getLocation();
	        //final long safefallDur = SkillConfigManager.getUseSetting(hero, this, "safefall-Duration", 3000, false);
	        //hero.addEffect(new SafeFallEffect(this,"ShadowStepSafefall",hero.getPlayer(),safefallDur));
            //teleport.add(new Vector(0.5D, 0.5D, 0.5D));
            //teleport.setPitch(player.getLocation().getPitch());
            //teleport.setYaw(player.getLocation().getYaw());
            //player.teleport(teleport);
            return true;
        }
        else {
            Messaging.send(player, "There was an error getting your blink location!");
            return false;
        }
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 7000, false);
        long cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 25000, false);
        ShadowStepEffect effect = null;
        if (!hero.hasEffect("ShadowStepEffect")) {
            effect = new ShadowStepEffect(this, duration, cooldown, player.getName(), player.getLocation(),hero);
            if (blink(hero, distance)) {
                hero.addEffect(effect);
                Messaging.send(player, "Reactivate in $1 seconds to teleport back.", (duration / 1000L));
                return SkillResult.CANCELLED;
            } else {
                effect = null;
                return SkillResult.CANCELLED;
            }
        } else {
            effect = (ShadowStepEffect) hero.getEffect("ShadowStepEffect");
            final long safefallDur = SkillConfigManager.getUseSetting(hero, this, "safefall-Duration", 3000, false);
            hero.addEffect(new SafeFallEffect(this,"ShadowStepSafefall",hero.getPlayer(),safefallDur));
            Location loc = effect.getLocation();
            loc.setPitch(player.getLocation().getPitch());
            loc.setYaw(player.getLocation().getYaw());
            player.teleport(loc);
            Messaging.send(player, "You teleported back to your previous location.");
            hero.removeEffect(effect);
            return SkillResult.NORMAL;
        }
    }

    @Override
    public String getDescription(Hero hero) {
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        return getDescription().replace("$1", distance + "").replace("$2", nb - 1 + "") + Lib.getSkillCostStats(hero, this);
    }

    public class ShadowStepEffect extends ExpirableEffect {
        private long cooldown;
        private final String name;
        private Location loc;

        public ShadowStepEffect(Skill skill, long duration, long cooldown, String name, Location loc,Hero hero) {
            super(skill, "ShadowStepEffect",hero.getPlayer(), duration);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.UNBREAKABLE);
            this.cooldown = cooldown;
            this.name = name;
            this.loc = loc;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.setCooldown("ShadowStep", cooldown + System.currentTimeMillis());
        }

        public Location getLocation() {
            return loc;
        }
    }
}