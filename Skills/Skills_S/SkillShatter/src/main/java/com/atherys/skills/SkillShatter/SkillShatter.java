package com.atherys.skills.SkillShatter;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillShatter extends ActiveSkill {
    public SkillShatter(Heroes plugin) {
        super(plugin, "Shatter");
        setDescription("Removes effects that summon blocks on nearby enemies.");
        setUsage("/skill Shatter");
        setArgumentRange(0, 0);
        setIdentifiers("skill shatter");
        setTypes(SkillType.DISABLE_COUNTERING, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        boolean brokeSomething = false;
        for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
            if (e instanceof Player) {
                Player pl = (Player) e;
                if (damageCheck((org.bukkit.entity.LivingEntity) pl, hero.getPlayer())) {
                    Hero thero = plugin.getCharacterManager().getHero(pl);
                    if (thero.hasEffectType(EffectType.FORM)) {
                        for (Effect effect : thero.getEffects()) {
                            if (effect.isType(EffectType.FORM) && !(effect.isType(EffectType.UNBREAKABLE)) && effect.isType(EffectType.DISPELLABLE)) {
                                thero.removeEffect(effect);
                                if (!brokeSomething) {
                                    brokeSomething = true;
                                }
                                broadcast(pl.getLocation(), ChatColor.WHITE + pl.getDisplayName() + ChatColor.GRAY + "'s " +
                                        ChatColor.WHITE + effect.getSkill().getName() + ChatColor.GRAY + " got shattered!");
                            }
                        }
                    }
                }
            }
        }
        if (brokeSomething) {
            Location loc = hero.getPlayer().getLocation();
            loc.getWorld().playSound(loc, Sound.BLOCK_GLASS_BREAK, 1F, 1F);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}    
