package com.atherys.skills.SkillShatteringStrike;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Arthur on 10/28/2015.
 */
public class SkillShatteringStrike extends TargettedSkill {
	public SkillShatteringStrike(Heroes plugin) {
		super(plugin, "ShatteringStrike");
		setDescription("The Invuln effects (Invuln, GuardianAngel, DivineIntervention, etc) of an enemy within $1 blocks and deal $2 damage.");
		setUsage("/skill shatteringstrike");
		setArgumentRange(0, 0);
		setIdentifiers("skill shatteringstrike");
		setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE);
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.MAX_DISTANCE.node(), (double) 7);
		return node;
	}

	@Override
	public SkillResult use(Hero hero, LivingEntity target, String[] args) {
		Player player = hero.getPlayer();
		if (!(target instanceof Player)) {
			return SkillResult.CANCELLED;
		} else {
			boolean hasRemovedEffect=false;
			Hero tHero = plugin.getCharacterManager().getHero((Player) target);
			if (!(target.equals(player))) {
				if (!hero.hasParty()){
						broadcastExecuteText(hero, target);
						for (Effect x : tHero.getEffects()) {
							if (((x.isType(EffectType.INVULNERABILITY) || x.getName().equals("Bulwark"))) && !x.isType(EffectType.UNBREAKABLE)) {
								tHero.removeEffect(x);
								hasRemovedEffect=true;
							}
						}
						if (hasRemovedEffect){
							if (damageCheck(player, target)) {
								damageEntity(target, player, 5D, EntityDamageEvent.DamageCause.MAGIC);
							}
							target.getWorld().playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1F, 1F);
							return SkillResult.NORMAL;
						}
				}else if (hero.hasParty()){
					if (!(hero.getParty().getMembers().contains(tHero))) {
						return SkillResult.CANCELLED;
					}
				}
			}
			return SkillResult.CANCELLED;

		}

	}
}
