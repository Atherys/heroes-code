package com.atherys.skills.SkillShell;

import com.atherys.effects.BulwarkEffect;
import com.atherys.effects.KneebreakEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillShell extends ActiveSkill {

    public SkillShell(Heroes plugin) {
        super(plugin, "Shell");
        setDescription("Shield all party members within a range for a set amount of health. Bloom: All nearby enemies are temporarily unable to jump.");
        setUsage("/skill Shell");
        setArgumentRange(0, 0);
        setIdentifiers("skill shell");
        setNotes("Note: Using this skill also puts WildGrowth and Overgrowth on a 10 second cooldown.");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING, SkillType.DEBUFFING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("shell-size", 150);
        node.set("shell-duration", 4000);
        node.set("shell-radius", 10);
        node.set("debuff-duration", 4000);
        node.set("debuff-radius", 15);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        if (hero.hasEffect("BloomEffect")) {
            double size = SkillConfigManager.getUseSetting(hero, this, "shell-size", 150, false);
            long duration = SkillConfigManager.getUseSetting(hero, this, "shell-duration", 4000, false);
            if (!hero.hasParty()) {
                hero.addEffect(new BulwarkEffect(this, duration, size,hero));
                Messaging.send(hero.getPlayer(), "You are shielded by your Shell");
            } else {
                int r = SkillConfigManager.getUseSetting(hero, this, "shell-radius", 10, false);
                for (Hero pHero : hero.getParty().getMembers()) {
                    Player pPlayer = pHero.getPlayer();
                    if (!pPlayer.getWorld().equals(player.getWorld())) {
                        continue;
                    }
                    if (pPlayer.getLocation().distanceSquared(player.getLocation()) > r * r) {
                        continue;
                    }
                    pHero.addEffect(new BulwarkEffect(this, duration, size,pHero));
                    Messaging.send(pPlayer, "The have been shielded by " + ChatColor.WHITE + player.getName()
                            + ChatColor.GRAY + "'s Shell.");
                }
            }
        } else {
            int r = SkillConfigManager.getUseSetting(hero, this, "debuff-radius", 15, false);
            int duration = SkillConfigManager.getUseSetting(hero, this, "debuff-duration", 4000, false);
            for (Entity e : player.getNearbyEntities(r, r, r)) {
                if (e instanceof Player) {
                    if (damageCheck(player, (LivingEntity)e)) {
                        Hero targetHero = plugin.getCharacterManager().getHero((Player)e);
                        targetHero.addEffect(new KneebreakEffect(this, "ShellKneebreak", duration, "", "",targetHero));
                    }
                }
            }
        }
        long time = System.currentTimeMillis();
        if (hero.getCooldown("Overgrowth") == null || (hero.getCooldown("Overgrowth") - time) < 10000) {
            hero.setCooldown("Overgrowth", time + 10000);
        }
        if (hero.getCooldown("WildGrowth") == null || (hero.getCooldown("WildGrowth") - time) < 10000) {
            hero.setCooldown("WildGrowth", time + 10000);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}