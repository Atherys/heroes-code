package com.atherys.skills.SkillShield;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillShield extends PassiveSkill {
    public SkillShield(Heroes plugin) {
        super(plugin, "Shield");
        setDescription("You are able to use doors as shields to absorbs damage!");
        setArgumentRange(0, 0);
        setEffectTypes(EffectType.BENEFICIAL, EffectType.PHYSICAL);
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL);
        Bukkit.getServer().getPluginManager().registerEvents(new CustomListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("iron-door-physical", Double.valueOf(0.75D));
        node.set("wooden-door-physical", Double.valueOf(0.85D));
        node.set("trapdoor-physical", Double.valueOf(0.6D));

        node.set("iron-door-magic", Double.valueOf(0.75D));
        node.set("wooden-door-magic", Double.valueOf(0.85D));
        node.set("trapdoor-magic", Double.valueOf(0.6D));

        node.set("iron-door-projectile", Double.valueOf(0.75D));
        node.set("wooden-door-projectile", Double.valueOf(0.85D));
        node.set("trapdoor-projectile", Double.valueOf(0.6D));
        return node;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class CustomListener
            implements Listener {
        private final Skill skill;

        public CustomListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if ((event.getDamage() == 0) || (!(event.getEntity() instanceof Player))) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = SkillShield.this.plugin.getCharacterManager().getHero(player);

            if (hero.hasEffect(SkillShield.this.getName())) {
                if (!player.isSneaking()){
                    return;
                }
                double multiplier = 1.0D;

                //Physical Attacks
                if (event.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK){
                    if (player.getItemInHand().getType() == Material.IRON_DOOR)
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "iron-door-physical", 0.75D, true);
                    else if (player.getItemInHand().getType() == Material.WOOD_DOOR)
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "wooden-door-physical", 0.85D, true);
                    else if (player.getItemInHand().getType() == Material.TRAP_DOOR) {
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "trapdoor-physical", 0.6D, true);
                    }
                }

                //Magic Damage
                else if (event.getCause() == EntityDamageEvent.DamageCause.MAGIC){
                    if (player.getItemInHand().getType() == Material.IRON_DOOR)
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "iron-door-magic", 0.75D, true);
                    else if (player.getItemInHand().getType() == Material.WOOD_DOOR)
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "wooden-door-magic", 0.85D, true);
                    else if (player.getItemInHand().getType() == Material.TRAP_DOOR) {
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "trapdoor-magic", 0.6D, true);
                    }
                }

                //Projectile
                else if (event.getCause() == EntityDamageEvent.DamageCause.PROJECTILE){
                    if (player.getItemInHand().getType() == Material.IRON_DOOR)
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "iron-door-projectile", 0.75D, true);
                    else if (player.getItemInHand().getType() == Material.WOOD_DOOR)
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "wooden-door-projectile", 0.85D, true);
                    else if (player.getItemInHand().getType() == Material.TRAP_DOOR) {
                        multiplier = SkillConfigManager.getUseSetting(hero, this.skill, "trapdoor-projectile", 0.6D, true);
                    }
                }

                //All the other damage
                else {
                    return;
                }

                event.setDamage(event.getDamage() * multiplier);
            }
        }
    }
}
