package com.atherys.skills.SkillSiphon;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillSiphon extends PassiveSkill {

    public SkillSiphon(Heroes plugin) {
        super(plugin, "Siphon");
        setDescription("Passive\nSteals mana every time you hit an enemy every few seconds. Gives even more mana if your Absorb is active.");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE, SkillType.MANA_INCREASING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.COOLDOWN.node(), 3000);
        node.set("mana-steal", 5);
        node.set("absorb-mana", 5);
        return node;
    }

    public class SkillHeroListener implements Listener {
        private final Skill skill;

        public SkillHeroListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK || !(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Hero)) {
                return;
            }
	        Hero tHero = plugin.getCharacterManager().getHero((Player)event.getEntity());
	        Hero hero = (Hero)event.getDamager();
	        if (damageCheck((org.bukkit.entity.LivingEntity) hero.getPlayer(), tHero.getPlayer())) {
		         if (hero.hasEffect("Siphon")) {
			        long time = System.currentTimeMillis();
			        if (hero.getCooldown("Siphon") == null || hero.getCooldown("Siphon") <= time) {
				        long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN, 3000, false);
				        hero.setCooldown("Siphon", cooldown + time);
				        int manaSteal = SkillConfigManager.getUseSetting(hero, skill, "mana-steal", 5, false);

				        tHero.setMana(tHero.getMana() - manaSteal < 0 ? 0 : tHero.getMana() - manaSteal);
				        if (hero.hasEffect("Absorb")) {
					        int absorbMana = SkillConfigManager.getUseSetting(hero, skill, "absorb-mana", 5, false);
					        manaSteal += absorbMana;
				        }
				        hero.setMana(hero.getMana() + manaSteal > hero.getMaxMana() ? hero.getMaxMana() : hero.getMana() + manaSteal);
			        }
		        }
	        }
        }
    }
}