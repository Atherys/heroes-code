package com.atherys.skills.SkillSlit;

import com.atherys.effects.SilenceEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillSlit extends ActiveSkill {
    public SkillSlit(Heroes plugin) {
        super(plugin, "Slit");
        setDescription("Imbue\n Your next melee attack will silence your target and do extra damage, both effects amplified if you cast the imbue while invisible.");
        setUsage("/skill slit");
        setArgumentRange(0, 0);
        setIdentifiers("skill slit");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE, SkillType.BUFFING, SkillType.STEALTHY);
        Bukkit.getPluginManager().registerEvents(new SlitListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("number-of-attacks", 1);
        node.set("bonus-damage", 20);
        node.set(SkillSetting.DAMAGE.node(), 15);
        node.set("silence-duration", 3000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int attacks = SkillConfigManager.getUseSetting(hero, this, "number-of-attacks", 1, false);
        long silenceDuration = SkillConfigManager.getUseSetting(hero, this, "silence-duration", 3000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 15D, false);
        if (hero.hasEffect("Invisible")) {
            double bonusdamage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 20D, false);
            hero.addEffect(new SlitEffect(this, duration, attacks, (bonusdamage + damage), silenceDuration, true,hero));
            Messaging.send(player, "You deftly prepare to slit your target's throat!");
            return SkillResult.NORMAL;
        }
        //hero.addEffect(new SlitEffect(this, duration, attacks, damage, silenceDuration, false,hero));
       // Messaging.send(player, "You poorly prepare to slit your target's throat!");
        return SkillResult.FAIL;
    }

    private static class SlitEffect extends ExpirableEffect {
        private int attacks;
        private long silenceDuration;
        private double damage;
        private boolean isExtra;


        public SlitEffect(Skill skill, long duration, int attacks, double damage, long silenceDuration, boolean isExtra,Hero hero) {
            super(skill, "SlitEffect",hero.getPlayer(), duration);
            this.attacks = attacks;
            this.silenceDuration = silenceDuration;
            this.damage = damage;
            this.isExtra = isExtra;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Slit expired");
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }

        public long getSilenceDuration() {
            return silenceDuration;
        }

        public double getDamage() {
            return damage;
        }

        public boolean isExtra() {
            return isExtra;
        }
    }

    public class SlitListener implements Listener {
        private final Skill skill;

        public SlitListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero) ||
                    (event.isCancelled()) ||
                    !(event.getEntity() instanceof Player) ||
                    (event.getDamage() == 0) ||
                    (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero target = plugin.getCharacterManager().getHero((Player) event.getEntity());
            Hero hero = (Hero) event.getDamager();
            if (!event.getDamager().hasEffect("SlitEffect"))
                return;
            SlitEffect he = (SlitEffect) hero.getEffect("SlitEffect");
            if (he.isExtra()) {
                target.addEffect(new SilenceEffect(skill, he.getSilenceDuration(), true,hero));
                Lib.cancelDelayedSkill(target);
            }
            event.setDamage(event.getDamage() + he.getDamage());
            getAttacksLeft(hero, he);
        }

        private void getAttacksLeft(Hero hero, SlitEffect he) {
            if (he.getAttacksLeft() <= 1) {
                hero.removeEffect(he);
            } else {
                he.setAttacksLeft(he.getAttacksLeft() - 1);
            }
        }
    }
}
