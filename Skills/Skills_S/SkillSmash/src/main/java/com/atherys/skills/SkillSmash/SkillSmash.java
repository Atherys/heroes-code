package com.atherys.skills.SkillSmash;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillSmash extends ActiveSkill {
    public SkillSmash(Heroes plugin) {
        super(plugin, "Smash");
        setDescription("Players in front of caster are pushed up into the air three blocks and take $1 damage.");
        setUsage("/skill Smash");
        setArgumentRange(0, 0);
        setIdentifiers("skill Smash");
        setTypes(SkillType.DAMAGING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new DamageListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("vertical-vector", 3);
        node.set(SkillSetting.RADIUS.node(), 3);
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set("falling-damage-mult", 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        double vv = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 3, false);
        double dm = SkillConfigManager.getUseSetting(hero, this, "falling-damage-mult", 10, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        List<LivingEntity> nearby = new ArrayList<>();
        for (Entity entity : hero.getPlayer().getNearbyEntities(r, r, r)) {
            if ((entity instanceof LivingEntity)) {
                LivingEntity e = (LivingEntity) entity;
                if (damageCheck(hero.getPlayer(), e)) {
                    nearby.add(e);
                    addSpellTarget(e, hero);
                    damageEntity(e, hero.getPlayer(), damage, DamageCause.ENTITY_ATTACK);
                }
            }
        }
        Vector playerstartloc = hero.getPlayer().getLocation().toVector();
        Vector dir = hero.getPlayer().getLocation().getDirection();
        for (LivingEntity e : getEntitiesInCone(nearby, playerstartloc, r, 45, dir)) {
            plugin.getCharacterManager().getCharacter(e).addEffect(new SmashEffect(this, dm,hero));
            e.setVelocity(e.getVelocity().setY(vv));
        }
        long time = System.currentTimeMillis();
        if (hero.getCooldown("Groundpound") == null || (hero.getCooldown("Groundpound") - time) < 10000) {
            hero.setCooldown("Groundpound", time + 3000);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public static List<LivingEntity> getEntitiesInCone(List<LivingEntity> entities, Vector startPos, float radius, float degrees, Vector direction) {
        List<LivingEntity> newEntities = new ArrayList<LivingEntity>();
        float squaredRadius = radius * radius;
        for (Entity e : entities) {
            Vector relativePosition = e.getLocation().toVector();
            relativePosition.subtract(startPos);
            if ((relativePosition.lengthSquared() <= squaredRadius) &&
                    (getAngleBetweenVectors(direction, relativePosition) <= degrees)) {
                newEntities.add((LivingEntity) e);
            }
        }
        return newEntities;
    }

    public static float getAngleBetweenVectors(Vector v1, Vector v2) {
        return Math.abs((float) Math.toDegrees(v1.angle(v2)));
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "");
    }

    public class SmashEffect extends ExpirableEffect {
        private final double dm;

        public SmashEffect(Skill skill, double dm,Hero hero) {
            super(skill, "SmashEffect",hero.getPlayer(), 3000L);
            this.dm = dm;
        }

        public double getFallMult() {
            return dm;
        }
    }

    public class DamageListener implements Listener {
        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.isCancelled() || (!(event.getEntity() instanceof LivingEntity)) || (event.getCause() != DamageCause.FALL))
                return;
            CharacterTemplate ct = plugin.getCharacterManager().getCharacter((LivingEntity) event.getEntity());
            if (ct.hasEffect("SmashEffect")) {
                SmashEffect s = (SmashEffect) ct.getEffect("SmashEffect");
                event.setDamage((int) (event.getDamage() * s.getFallMult()));
            }
        }
    }
}