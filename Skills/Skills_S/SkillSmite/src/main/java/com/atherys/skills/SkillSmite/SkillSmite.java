package com.atherys.skills.SkillSmite;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillSmite extends TargettedSkill {
    public SkillSmite(Heroes plugin) {
        super(plugin, "Smite");
        setDescription("You smite the target for $1 light damage. Undead monsters recieve $2% damage from this spell.");
        setUsage("/skill smite");
        setArgumentRange(0, 0);
        setIdentifiers("skill smite");
        setTypes(SkillType.DAMAGING, SkillType.ABILITY_PROPERTY_LIGHT, SkillType.SILENCEABLE, SkillType.ABILITY_PROPERTY_LIGHTNING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10));
        node.set("multiplier", 2.0);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();

        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);

        if (target.getType() == target.getType().ZOMBIE || target.getType() == target.getType().SKELETON || target.getType() == target.getType().PIG_ZOMBIE || target.getType() == target.getType().WITHER) {
	        addSpellTarget(target, hero);
            damageEntity(target, player, (damage * (SkillConfigManager.getUseSetting(hero, this, "multiplier", 2.0, false))), EntityDamageEvent.DamageCause.MAGIC);
        } else if (target instanceof Player) {
            if (!(target.equals(player))) {
                    if (damageCheck(target, player)) {
                        addSpellTarget(target, hero);
                        damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
                        broadcastExecuteText(hero, target);
                        return SkillResult.NORMAL;
                    }
                return SkillResult.INVALID_TARGET_NO_MSG;
            }
            return SkillResult.INVALID_TARGET_NO_MSG;
        }else {
            addSpellTarget(target, hero);
            damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
            //broadcastExecuteText(hero);
        }
    /*FireworkEffectPlayer fplayer = new FireworkEffectPlayer();
        try {
            fplayer.playFirework(target.getWorld(), target.getLocation().add(0,1.5,0), FireworkEffect.builder().with(FireworkEffect.Type.BALL).withColor(Color.WHITE).withColor(Color.WHITE.mixColors(Color.YELLOW)).build());
        } catch (Exception ex) {
            Logger.getLogger(SkillSmite.class.getName()).log(Level.SEVERE, null, ex);
        } */
	    broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        double multiplier = SkillConfigManager.getUseSetting(hero, this, "multiplier", 2.0, false);
        return getDescription().replace("$1", damage + "").replace("$2", (100 * multiplier) + "");
    }
}