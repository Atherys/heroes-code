package com.atherys.skills.SkillSmokeBomb;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class SkillSmokeBomb extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillSmokeBomb(Heroes plugin) {
        super(plugin, "SmokeBomb");
        setDescription("Active\nThrows a Smoke Bomb that will damage and blind enemies on impact.");
        setUsage("/skill SmokeBomb");
        setArgumentRange(0, 0);
        setIdentifiers("skill SmokeBomb");
        setTypes(SkillType.ABILITY_PROPERTY_DARK, SkillType.SILENCEABLE, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new SmokeBombListener(this), plugin);
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% has blinded you with %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You have recovered your sight!");
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(5));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(5000));
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(5));
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% has blinded you with %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You have recovered your sight!");
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Potion potionItem = new Potion(PotionType.WATER, 1);
        potionItem.splash();
        ItemStack item = potionItem.toItemStack(1);
        ThrownPotion potion = player.launchProjectile(ThrownPotion.class);
        potion.setItem(item);
        //potion.getEffects().clear();
        potion.setMetadata("SmokeBomb", new FixedMetadataValue(plugin, true));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SmokeBombEffect extends ExpirableEffect {
        private final Player player;

        public SmokeBombEffect(Skill skill, long duration, Player player) {
            super(skill, "SmokeBomb",player, duration);
            this.player = player;
            addMobEffect(15, (int) (duration / 1000L * 20L), 3, false);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.BLIND);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Messaging.send(hero.getPlayer(), SkillSmokeBomb.this.applyText, this.player.getDisplayName(), "SmokeBomb");
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), SkillSmokeBomb.this.expireText);
        }
    }

    public class SmokeBombListener implements Listener {
        private Skill skill;

        public SmokeBombListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onPotionSplash(ProjectileHitEvent event) {
            Entity entity = event.getEntity();
            if (!(entity instanceof ThrownPotion)) {
                return;
            }
            if (event.getEntity().getShooter() instanceof Player) {
                if (event.getEntity().hasMetadata("SmokeBomb")) {
                    Player player = (Player) event.getEntity().getShooter();
                    Hero hero = plugin.getCharacterManager().getHero(player);
                    ThrownPotion potion = (ThrownPotion)event.getEntity();
                    double radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 5D, false);
                    long duration = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 5000, false);
                    double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 5D, false);
                    SmokeBombEffect smokeBombEffect = new SmokeBombEffect(skill, duration, player);
                    for (Entity e : potion.getNearbyEntities(radius, radius, radius)) {
                        if ((e instanceof Player)) {
                            if (damageCheck((LivingEntity) player, (Player) e)) {
                                Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                                addSpellTarget(tHero.getPlayer(), plugin.getCharacterManager().getHero(player));
                                damageEntity(tHero.getPlayer(), player, damage, DamageCause.MAGIC);
                                tHero.addEffect(smokeBombEffect);
                                Lib.cancelDelayedSkill(tHero);
                            }
                        } else if (e instanceof LivingEntity) {
                            if (damageCheck(player, (LivingEntity) e)) {
                                addSpellTarget((LivingEntity)e, plugin.getCharacterManager().getHero(player));
                                damageEntity((LivingEntity) e, player, damage, DamageCause.MAGIC);
                            }
                        }
                    }
                    Location loc = potion.getLocation();
                    loc.getWorld().spawnParticle(Particle.CLOUD, 5, 5, 3, 5);
                    loc.getWorld().spawnParticle(Particle.SMOKE_LARGE, 3, 3, 2, 3);
                    potion.getWorld().playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
                }
            }
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onSplashEffect(PotionSplashEvent event) {
            if (event.getPotion().getShooter() instanceof Player) {
                if (event.getEntity().hasMetadata("SmokeBomb")) {
                    event.setCancelled(true);
                }
            }
        }
    }
}