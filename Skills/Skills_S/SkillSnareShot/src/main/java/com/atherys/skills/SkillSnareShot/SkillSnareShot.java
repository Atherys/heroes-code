package com.atherys.skills.SkillSnareShot;

import com.atherys.effects.InvulnEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;

public class SkillSnareShot extends ActiveSkill {

    public SkillSnareShot(Heroes plugin) {
        super(plugin, "SnareShot");
        setDescription("Your next loosed arrow within $1s will stun your target for $2s, but also make them invulnerable for the duration.");
        setUsage("/skill snareshot");
        setArgumentRange(0, 0);
        setIdentifiers("skill snareshot");
        setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillDamageListener(this), plugin);
        //Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set("debuff-duration", Integer.valueOf(2000));
        node.set(SkillSetting.USE_TEXT.node(), "%hero% sets up a snare shot!");
        return node;
    }

    public void init() {
        super.init();
        setUseText("%hero% sets up a snare shot!".replace("%hero%", "$1"));
    }

    public SkillResult use(Hero hero, String[] args) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        hero.addEffect(new SnareShotEffect(this, duration, 1,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int debuffDuration = SkillConfigManager.getUseSetting(hero, this, "debuff-duration", 2000, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", debuffDuration / 1000 + "");
    }

    public class SkillDamageListener implements Listener {
        private final Skill skill;

        public SkillDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((!(event.getEntity() instanceof LivingEntity)) || (!(event instanceof EntityDamageByEntityEvent))) {
                return;
            }
            LivingEntity target = (LivingEntity) event.getEntity();
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            if (!(subEvent.getDamager() instanceof Arrow)) {
                return;
            }
            Arrow arrow = (Arrow) subEvent.getDamager();
            if (!(arrow.getShooter() instanceof Player)) {
                return;
            }
            Player player = (Player) arrow.getShooter();
            if (player.equals(target)) {
                return;
            }
            Hero hero = SkillSnareShot.this.plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("SnareShotEffect")&& (target instanceof Player)) {
                if (!Skill.damageCheck(player, target)) {
                    event.setCancelled(true);
                    return;
                }
                Player tPlayer = (Player) target;
                Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
                event.setCancelled(true);
                double damage = (hero.hasAccessToSkill("BowMastery")) ? (SkillConfigManager.getUseSetting(hero, plugin.getSkillManager().getSkill("BowMastery"), "damage", 20.0, false)) : 0.0;
                long debuffDuration = SkillConfigManager.getUseSetting(hero, this.skill, "debuff-duration", 2000, false);
                skill.damageEntity(target, player, damage, EntityDamageEvent.DamageCause.PROJECTILE);
                tHero.addEffect(new StunEffect(this.skill, debuffDuration,tHero));
                tHero.addEffect(new InvulnEffect(this.skill, debuffDuration,tHero));
                Lib.cancelDelayedSkill(tHero);
                hero.removeEffect(hero.getEffect("SnareShotEffect"));
            }
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onEntityShootBow(EntityShootBowEvent event) {
            if ((event.isCancelled()) || (!(event.getEntity() instanceof Player)) || (!(event.getProjectile() instanceof Arrow))) {
                return;
            }
            Hero hero = SkillSnareShot.this.plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("SnareShotEffect")) {
                int attacksLeft = ((SnareShotEffect) hero.getEffect("SnareShotEffect")).getAttacksLeft();
                if (attacksLeft!=0)
                    ((SnareShotEffect) hero.getEffect("SnareShotEffect")).setAttacksLeft(attacksLeft-1);
                else
                    hero.removeEffect(hero.getEffect("SnareShotEffect"));
            }
        }
    }

    private static class SnareShotEffect extends ExpirableEffect {
        private int attacks;
        public SnareShotEffect(Skill skill, long duration, int attacks,Hero hero) {
            super(skill, "SnareShotEffect",hero.getPlayer(), duration);
            this.attacks = attacks;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "SnareShot expired");
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }
    }
}
