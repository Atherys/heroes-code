package com.atherys.skills.SkillSneak;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class SkillSneak extends ActiveSkill {
    private boolean damageCancels;
    private boolean attackCancels;

    public SkillSneak(Heroes plugin) {
        super(plugin, "Sneak");
        setDescription("You crouch into the shadows.");
        setUsage("/skill stealth");
        setArgumentRange(0, 0);
        setIdentifiers("skill sneak");
        setTypes(SkillType.BUFFING, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.STEALTHY, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEventListener(), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(600000));
        node.set("damage-cancels", Boolean.valueOf(true));
        node.set("attacking-cancels", Boolean.valueOf(true));
        node.set("refresh-interval", Integer.valueOf(5000));
        return node;
    }

    public void init() {
        super.init();
        this.damageCancels = SkillConfigManager.getRaw(this, "damage-cancels", true).booleanValue();
        this.attackCancels = SkillConfigManager.getRaw(this, "attacking-cancels", true).booleanValue();
    }

    public SkillResult use(Hero hero, String[] args) {
        if (hero.hasEffect("Sneak")) {
            hero.removeEffect(hero.getEffect("Sneak"));
        } else {
            Messaging.send(hero.getPlayer(), "You are now sneaking");
            int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 600000, false);
            int period = SkillConfigManager.getUseSetting(hero, this, "refresh-interval", 5000, true);
            hero.addEffect(new SneakEffect(this, period, duration,hero));
        }
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class SneakEffect extends PeriodicExpirableEffect {
        private boolean vanillaSneaking;

        public SneakEffect(final Skill skill, final long period, final long duration,Hero hero) {
            super(skill, "Sneak",hero.getPlayer(), period, duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.SNEAK);
            this.setVanillaSneaking(false);
        }

        @Override
        public void applyToHero(final Hero hero) {
            super.applyToHero(hero);
            final Player player = hero.getPlayer();
            player.setSneaking(true);
        }

        @Override
        public void removeFromHero(final Hero hero) {
            super.removeFromHero(hero);
            final Player player = hero.getPlayer();
            player.setSneaking(false);
            Messaging.send((CommandSender) player, "You are no longer sneaking!", new Object[0]);
        }

        @Override
        public void tickHero(final Hero hero) {
            hero.getPlayer().setSneaking(false);
            hero.getPlayer().setSneaking(true);
        }

        @Override
        public void tickMonster(final Monster monster) {
        }

        public boolean isVanillaSneaking() {
            return this.vanillaSneaking;
        }

        public void setVanillaSneaking(final boolean vanillaSneaking) {
            this.vanillaSneaking = vanillaSneaking;
        }
    }

    public class SkillEventListener
            implements Listener {
        public SkillEventListener() {
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!SkillSneak.this.damageCancels) || (event.getDamage() == 0)) {
                return;
            }
            Player player = null;
            if ((event.getEntity() instanceof Player)) {
                player = (Player) event.getEntity();
                Hero hero = SkillSneak.this.plugin.getCharacterManager().getHero(player);
                if (hero.hasEffect("Sneak")) {
                    player.setSneaking(false);
                    hero.removeEffect(hero.getEffect("Sneak"));
                }
            }
            player = null;
            if ((SkillSneak.this.attackCancels) && ((event instanceof EntityDamageByEntityEvent))) {
                EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
                if ((subEvent.getDamager() instanceof Player))
                    player = (Player) subEvent.getDamager();
                else if (((subEvent.getDamager() instanceof Projectile)) &&
                        ((((Projectile) subEvent.getDamager()).getShooter() instanceof Player))) {
                    player = (Player) ((Projectile) subEvent.getDamager()).getShooter();
                }
                if (player != null) {
                    Hero hero = SkillSneak.this.plugin.getCharacterManager().getHero(player);
                    if (hero.hasEffect("Sneak")) {
                        player.setSneaking(false);
                        hero.removeEffect(hero.getEffect("Sneak"));
                    }
                }
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
            Hero hero = SkillSneak.this.plugin.getCharacterManager().getHero(event.getPlayer());
            if (hero.hasEffect("Sneak")) {
                event.getPlayer().setSneaking(true);
                event.setCancelled(true);
            }
        }
    }
}
