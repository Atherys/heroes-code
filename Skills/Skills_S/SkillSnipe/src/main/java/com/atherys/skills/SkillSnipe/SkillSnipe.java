package com.atherys.skills.SkillSnipe;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;

public class SkillSnipe extends ActiveSkill {

    public SkillSnipe(Heroes plugin) {
        super(plugin, "Snipe");
        setDescription("Your next loosed arrow within $1s will deal bonus damage per block travelled. Landing a headshot resets the cooldown of ExplosiveShot.");
        setUsage("/skill snipe");
        setArgumentRange(0, 0);
        setIdentifiers("skill snipe");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
	    Bukkit.getServer().getPluginManager().registerEvents(new SkillSnipeListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.USE_TEXT.node(), "%hero% prepares to snipe!");
        return node;
    }

    public void init() {
        super.init();
        setUseText("%hero% prepares to snipe!".replace("%hero%", "$1"));
    }

    public SkillResult use(Hero hero, String[] args) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        hero.addEffect(new SnipeEffect(this, duration, 1, hero.getPlayer().getLocation(),hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class SkillSnipeListener implements Listener {
	    private final Skill skill;

	    public SkillSnipeListener(Skill skill) {
		    this.skill = skill;
	    }


	    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((!(event.getEntity() instanceof LivingEntity)) || (!(event instanceof EntityDamageByEntityEvent))) {
                return;
            }
            LivingEntity target = (LivingEntity) event.getEntity();
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            if (!(subEvent.getDamager() instanceof Arrow)) {
                return;
            }
            Arrow arrow = (Arrow) subEvent.getDamager();
            if (!(arrow.getShooter() instanceof Player)) {
                return;
            }
            Player player = (Player) arrow.getShooter();
            if (player.equals(target)) {
                return;
            }
            Hero hero = SkillSnipe.this.plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("SnipeEffect") && (target instanceof Player)) {
	            if (arrow.hasMetadata("ArrowblastArrow")){
		            return;
	            }
	            if (arrow.hasMetadata("BuckshotArrow")){
		            return;
	            }


		        SnipeEffect sne = (SnipeEffect) hero.getEffect("SnipeEffect");
	            Player tPlayer = (Player) target;
	            double damage = SkillConfigManager.getUseSetting(hero, skill, "base-damage", 10, false);
	            damage += (sne.getCasterLocation().getWorld().equals(tPlayer.getLocation().getWorld())) ? (Math.hypot((sne.getCasterLocation().getX() - tPlayer.getLocation().getX()), (sne.getCasterLocation().getZ() - tPlayer.getLocation().getZ()))) : 0.0;

	            double arrowHeight = arrow.getLocation().getY();
	            double targetHeight = target.getLocation().getY();
	            if ((arrowHeight - targetHeight) > 1.5D) {
		            Messaging.send(player, "You scored a " + ChatColor.DARK_RED + ChatColor.BOLD + "headshot!");
		            Messaging.send(target, "You were " + ChatColor.DARK_RED + ChatColor.BOLD + "headshotted!");
		            double headShot = SkillConfigManager.getUseSetting(hero, skill, "headshot-bonus", 15, false);
		            damage += headShot;
		            //Messaging.send(player, "You sniped a enemy, cooldown reset for snipe");
		            //if (hero.hasAccessToSkill("Snipe")) {
			        //    hero.removeCooldown("Snipe");
		            //}

		            for (int i = 0; i < 9; i++) {
			            arrow.getWorld().playEffect(arrow.getLocation(), Effect.INSTANT_SPELL, i);
		            }
		            arrow.getWorld().playSound(arrow.getLocation(), Sound.ENTITY_FIREWORK_BLAST, 1.5F, 1.0F);
	            }

	            addSpellTarget(tPlayer, hero);
	            skill.damageEntity(tPlayer, player, damage, EntityDamageEvent.DamageCause.PROJECTILE);
	            hero.removeEffect(hero.getEffect("SnipeEffect"));
	           /* Messaging.send(player, "You sniped a enemy, cooldown reset for snipe");
	            hero.removeCooldown("Snipe");
	            */

            }
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onEntityShootBow(EntityShootBowEvent event) {
            if ((event.isCancelled()) || (!(event.getEntity() instanceof Player)) || (!(event.getProjectile() instanceof Arrow))) {
                return;
            }
            Hero hero = SkillSnipe.this.plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("SnipeEffect")) {
                int attacksLeft = ((SnipeEffect) hero.getEffect("SnipeEffect")).getAttacksLeft();
                if (attacksLeft != 0) {
                    ((SnipeEffect) hero.getEffect("SnipeEffect")).setAttacksLeft(attacksLeft - 1);
                    ((SnipeEffect) hero.getEffect("SnipeEffect")).setCasterLocation(event.getEntity().getLocation());
                } else {
                    hero.removeEffect(hero.getEffect("SnipeEffect"));
                }
            }
        }
    }

    private static class SnipeEffect extends ExpirableEffect {
        private int attacks;
        private Location loc;

        public SnipeEffect(Skill skill, long duration, int attacks, Location loc,Hero hero) {
            super(skill, "SnipeEffect",hero.getPlayer(), duration);
            this.attacks = attacks;
            this.loc = loc;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.IMBUE);
            this.types.add(EffectType.PHYSICAL);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Snipe expired");
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }

        public Location getCasterLocation() {
            return loc;
        }

        public void setCasterLocation(Location newLocation) {
            this.loc = newLocation;
        }
    }
}
