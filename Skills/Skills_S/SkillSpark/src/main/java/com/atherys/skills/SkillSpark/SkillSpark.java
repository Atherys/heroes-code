package com.atherys.skills.SkillSpark;

import com.atherys.effects.SparkStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillSpark extends ActiveSkill {

    public SkillSpark(Heroes plugin) {
        super(plugin, "Spark");
        setDescription("You launch a ball of lightning that deals $1 damage to your target and increases the damage of your next cast of Bolt for $2 seconds." +
                "Max Stacks: 3");
        setUsage("/skill spark");
        setArgumentRange(0, 0);
        setIdentifiers("skill spark");
        setTypes(SkillType.ABILITY_PROPERTY_LIGHTNING, SkillType.SILENCEABLE, SkillType.DAMAGING);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 15);
        node.set(SkillSetting.DURATION.node(), 8000);
        node.set("velocity-multiplier", Double.valueOf(2.0D));
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Snowball snowball = player.launchProjectile(Snowball.class);
        snowball.setMetadata("SparkFireball", new FixedMetadataValue(plugin, true));
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 2.0D, false);
        snowball.setVelocity(snowball.getVelocity().multiply(mult));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 8000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 15, false);
        return getDescription().replace("$1", damage + "").replace("$2", duration / 1000 + "");
    }

    public class SkillEntityListener
            implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof Snowball)) || (!projectile.hasMetadata("SparkFireball"))) {
                return;
            }

            Entity dmger = (Entity) ((Snowball) subEvent.getDamager()).getShooter();
            if (dmger instanceof Player) {
                Player player = (Player) dmger;
                Hero hero = SkillSpark.this.plugin.getCharacterManager().getHero(player);
                if (!Skill.damageCheck(player, (LivingEntity) event.getEntity())) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setFireTicks(0);
                double damage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE, 15, false);
                long duration = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DURATION, 8000, false);
                LivingEntity target = (LivingEntity) event.getEntity();
                if (hero.hasEffect("SparkStackEffect")) {
                    int stacks = ((SparkStackEffect) hero.getEffect("SparkStackEffect")).getStacks();
                    if (stacks < 3) {
                        hero.addEffect(new SparkStackEffect(this.skill, duration, stacks + 1,hero));
                    } else {
                        hero.addEffect(new SparkStackEffect(this.skill, duration, 3,hero));
                    }
                } else {
                    hero.addEffect(new SparkStackEffect(this.skill, duration, 1,hero));
                }
                int newStacks = ((SparkStackEffect) hero.getEffect("SparkStackEffect")).getStacks();
                Messaging.send(dmger, "You now have " + newStacks + (newStacks == 1 ? " spark stack." : " spark stacks."));
                SkillSpark.this.addSpellTarget(event.getEntity(), hero);
                skill.damageEntity(target, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                Lib.projectileHit(player);
                event.setCancelled(true);
            }
        }
    }
}
