package com.atherys.skills.SkillSpines;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillSpines extends PassiveSkill {
    public SkillSpines(Heroes plugin) {
        super(plugin, "Spines");
        setDescription("Passive $1% chance to shoot arrows when hit. CD:$2");
        setTypes(SkillType.MOVEMENT_PREVENTION_COUNTERING, SkillType.ABILITY_PROPERTY_PHYSICAL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
        Bukkit.getPluginManager().registerEvents(new SkillDamageListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        double chance = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE.node(), 0.1, false) +
                (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE_LEVEL.node(), 0, false) * hero.getSkillLevel(this))) * 100;
        chance = chance > 0 ? chance : 0;
        long cooldown = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 500, false) -
                (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this))) / 1000;
        cooldown = cooldown > 0 ? cooldown : 0;
        String description = getDescription().replace("$1", chance + "").replace("$2", cooldown + "");
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.CHANCE.node(), .1);
        node.set(SkillSetting.CHANCE_LEVEL.node(), 0);
        node.set(SkillSetting.COOLDOWN.node(), 500);
        node.set(SkillSetting.COOLDOWN_REDUCE.node(), 0);
        return node;
    }

    public class SkillHeroListener implements Listener {
        private Skill skill;

        public SkillHeroListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.isCancelled() || event.getDamage() == 0 || event.getCause() != DamageCause.ENTITY_ATTACK || !(event.getEntity() instanceof Player))
                return;
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("Spines")) {
                if (hero.getCooldown("Spines") == null || hero.getCooldown("Spines") <= System.currentTimeMillis()) {
                    double chance = (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.CHANCE.node(), 0.1, false) +
                            (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.CHANCE_LEVEL.node(), 0, false) * hero.getSkillLevel(skill)));
                    chance = chance > 0 ? chance : 0;
                    long cooldown = (long) (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN.node(), 500, false) -
                            (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(skill)));
                    cooldown = cooldown > 0 ? cooldown : 0;
                    hero.setCooldown("Spines", cooldown + System.currentTimeMillis());
                    if (Math.random() <= chance) {
                        double diff = 2 * Math.PI / 12;
                        for (double a = 0; a < 2 * Math.PI; a += diff) {
                            Vector vel = new Vector(Math.cos(a), 0, Math.sin(a));
                            Arrow shot = player.launchProjectile(Arrow.class);
                            shot.setVelocity(vel);
                            shot.setMetadata("SpinesArrow", new FixedMetadataValue(plugin, true));
                        }
                    }
                }
            }
        }
    }

    public class SkillDamageListener implements Listener {

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onProjectileHit(ProjectileHitEvent event) {
            if (!(event.getEntity() instanceof Arrow) || !event.getEntity().hasMetadata("SpinesArrow")) {
                return;
            }
            event.getEntity().remove();
        }
    }
}