package com.atherys.skills.SkillSpire;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class SkillSpire extends ActiveSkill {

    public SkillSpire(Heroes plugin) {
        super(plugin, "Spire");
        setDescription("Creates a 1x4 spire of stone knocks enemies back and away.");
        setUsage("/skill spire");
        setArgumentRange(0, 0);
        setIdentifiers("skill spire");
        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.BLOCK_CREATING, SkillType.SILENCEABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Long.valueOf(1000));
        node.set(SkillSetting.RADIUS.node(), Double.valueOf(5));
        node.set("horizontal-vector", Double.valueOf(2.0));
        node.set("vertical-vector", Double.valueOf(1.0));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        HashSet<Byte> transparent = new HashSet<>();
        transparent.add((byte) Material.AIR.getId());
        transparent.add((byte) Material.SNOW.getId());
        Block b = player.getTargetBlock(transparent, max);
        if (b.getType() == Material.AIR) {
            player.sendMessage(ChatColor.GRAY + "You must target a block.");
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        Block b1 = b.getRelative(BlockFace.UP);
        if (!(b1.getType() == Material.AIR || b1.getType() == Material.SNOW)) {
            player.sendMessage(ChatColor.GRAY + "Cannot be placed here");
            return SkillResult.CANCELLED;
        }

        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 5D, false);
        for (Entity e : b.getWorld().getNearbyEntities(b.getLocation(), radius, radius, radius)) {
            if (e instanceof Player && damageCheck((LivingEntity) player, (Player) e)) {
                double v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 2.0D, false);
                double v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 1.0D, false);
                Vector v = e.getLocation().toVector().subtract(b.getLocation().toVector()).normalize();
                v = v.multiply(v1);
                v.setY(v2);
                e.setVelocity(v);
            } else if (e instanceof LivingEntity && damageCheck(player, (LivingEntity) e)) {
                double damage = SkillConfigManager.getUseSetting(hero, this, "mob-damage", 20, false);
                damageEntity((LivingEntity)e, player, damage, EntityDamageEvent.DamageCause.MAGIC, false);
            }
        }

        for (int yDir = 0; yDir < 5; yDir++) {
            Block chBlock = b.getRelative(0, yDir, 0);
            if ((chBlock.getType() == Material.AIR) || (chBlock.getType() == Material.SNOW)) {
                chBlock.setType(Material.STONE);
                values.add(chBlock);
                chBlock.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
            }
        }

        long duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 1000, false));
        hero.addEffect(new SpireEffect(this, duration, values,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SpireEffect extends ExpirableEffect {
        private Set<Block> wBlocks;

        public SpireEffect(Skill skill, long duration, Set<Block> values,Hero hero) {
            super(skill, "SpireEffect",hero.getPlayer(), duration);
            this.wBlocks = values;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.FORM);
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Lib.removeBlocks(wBlocks);
        }
    }
}
