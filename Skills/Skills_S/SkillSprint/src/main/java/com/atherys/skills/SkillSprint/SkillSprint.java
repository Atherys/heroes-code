package com.atherys.skills.SkillSprint;

import com.atherys.effects.DashSprintingEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillSprint extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillSprint(Heroes plugin) {
        super(plugin, "Sprint");
        setDescription("You sprint even faster for $1 seconds.");
        setUsage("/skill sprint");
        setArgumentRange(0, 0);
        setIdentifiers("skill sprint");
        setTypes(SkillType.BUFFING, SkillType.MOVEMENT_INCREASING, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-multiplier", Integer.valueOf(2));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(15000));
        node.set("apply-text", "%hero% gained a burst of speed!");
        node.set("expire-text", "%hero% returned to normal speed!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% gained a burst of speed!").replace("%hero%", "$1");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero% returned to normal speed!").replace("%hero%", "$1");
    }

    public SkillResult use(Hero hero, String[] args) {
        int multiplier = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 2, false);
        if (hero.hasEffect("Dash")) {
            if (hero.hasEffect("DashSprintingFinal")) {
                hero.removeEffect(hero.getEffect("DashSprintingFinal"));
                multiplier += 3;
            } else {
                for (int i = 0; i <= 2; i++) {
                    if (hero.hasEffect("DashSprinting" + i)) {
                        multiplier += ((DashSprintingEffect)hero.getEffect("DashSprinting" + i)).getAmplifier();
                        hero.removeEffect(hero.getEffect("DashSprinting" + i));
                        break;
                    }
                }
            }
        }
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 15000, false);
        if (multiplier > 20) {
            multiplier = 20;
        }
        hero.addEffect(new QuickenEffect(this, getName(), duration, multiplier, this.applyText, this.expireText,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class QuickenEffect extends ExpirableEffect {
        private final String applyText;
        private final String expireText;

        public QuickenEffect(final Skill skill, final String name, final long duration, final int amplifier, final String applyText, final String expireText,Hero hero) {
            super(skill,name ,hero.getPlayer(), duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.SPEED);
            this.addMobEffect(1, (int)(duration / 1000L) * 20, amplifier, false);
            this.applyText = applyText;
            this.expireText = expireText;
        }

        @Override
        public void applyToHero(final Hero hero) {
            super.applyToHero(hero);
            final Player player = hero.getPlayer();
            this.broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        @Override
        public void removeFromHero(final Hero hero) {
            super.removeFromHero(hero);
            final Player player = hero.getPlayer();
            this.broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
    }
}
