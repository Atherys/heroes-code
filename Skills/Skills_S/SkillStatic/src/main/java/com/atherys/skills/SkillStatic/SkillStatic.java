package com.atherys.skills.SkillStatic;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillStatic extends ActiveSkill {
    public SkillStatic(Heroes plugin) {
        super(plugin, "Static");
        setDescription("Active\nYou teleport to closest ally in front of you.");
        setUsage("/skill Static");
        setArgumentRange(0, 0);
        setIdentifiers("skill Static");
        setTypes(SkillType.ABILITY_PROPERTY_LIGHTNING, SkillType.SILENCEABLE, SkillType.TELEPORTING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 15);
        node.set("degrees", 45);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        if (!hero.hasParty()) {
            Messaging.send(player, "You aren't in a party.");
            return SkillResult.CANCELLED;
        }
        List<Entity> applicablePlayers = new ArrayList<>();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 15, false);
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if ((entity instanceof Player)) {
                if (hero.getParty().isPartyMember((Player)entity) && player.hasLineOfSight(entity)) {
                    applicablePlayers.add(entity);
                }
            }
        }
        Vector playerVector = player.getLocation().toVector();
        float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
        Player target = null;
        for (Entity e : Lib.getEntitiesInCone(applicablePlayers, playerVector, (float)radius, degrees, player.getLocation().getDirection())) {
            if (target == null || target.getLocation().distanceSquared(player.getLocation()) > e.getLocation().distanceSquared(player.getLocation())) {
                target = (Player)e;
            }
        }
        if (target == null) {
            Messaging.send(player, "No valid allies in front of you!");
            return SkillResult.CANCELLED;
        }
        player.getWorld().strikeLightningEffect(player.getLocation());
        player.teleport(target);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}