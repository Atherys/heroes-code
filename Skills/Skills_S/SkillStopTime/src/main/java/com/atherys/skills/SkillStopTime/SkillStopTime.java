package com.atherys.skills.SkillStopTime;

import com.atherys.effects.StunEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillStopTime extends ActiveSkill {
    public SkillStopTime(Heroes plugin) {
        super(plugin, "StopTime");
        setDescription("Use this skill to stop the time around you and stun your enemies.");
        setUsage("/skill StopTime");
        setArgumentRange(0, 0);
        setIdentifiers("skill StopTime");
        setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Long.valueOf(10));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(2000L));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 15, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 4000L, false);
	    long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 4000L, false);
        if (hero.hasParty()) {
            for (Entity e : player.getNearbyEntities(r, r, r)) {
                if (e instanceof Player) {
                    Player target = (Player) e;
                    Hero tHero = this.plugin.getCharacterManager().getHero(target);
                    if (hero.getParty().isPartyMember(tHero)) {
                        tHero.addEffect(new InvulnerabilityEffect(this,period,duration,tHero));
                    } else if (damageCheck((org.bukkit.entity.LivingEntity) player, target)) {
                        tHero.addEffect(new StunEffect(this, duration,tHero));
                        tHero.addEffect(new InvulnerabilityEffect(this,period,duration,tHero));
                        Lib.cancelDelayedSkill(tHero);
                    }
                }
            }
        } else {
            for (Entity e : player.getNearbyEntities(r, r, r)) {
                if (e instanceof Player) {
                    Player target = (Player) e;
                    if (damageCheck((org.bukkit.entity.LivingEntity) player, target)) {
                        Hero tHero = this.plugin.getCharacterManager().getHero(target);
                        tHero.addEffect(new StunEffect(this, duration,tHero));
                        tHero.addEffect(new InvulnerabilityEffect(this,period,duration,tHero));
                        Lib.cancelDelayedSkill(tHero);
                    }
                }
            }
        }
        hero.addEffect(new InvulnerabilityEffect(this,period,duration,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class InvulnerabilityEffect extends PeriodicExpirableEffect {
        public InvulnerabilityEffect(Skill skill, long period,long duration,Hero hero) {
            super(skill, "Invuln",hero.getPlayer(),period,duration);
            this.types.add(EffectType.INVULNERABILITY);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }

	    @Override
	    public void tickMonster(Monster p0) {
	    }

	    @Override
	    public void tickHero(Hero hero) {
		    Player player = hero.getPlayer();
		    for (double[] coords : Lib.playerParticleCoords) {
			    Location location = player.getLocation();
			    location.add(coords[0], coords[1], coords[2]);
                location.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, location, 1);
		    }
	    }
        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }

    }
}