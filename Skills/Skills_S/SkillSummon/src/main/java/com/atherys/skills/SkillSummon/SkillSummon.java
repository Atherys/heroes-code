package com.atherys.skills.SkillSummon;

import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillSummon extends ActiveSkill {

    public SkillSummon(Heroes paramHeroes) {
        super(paramHeroes, "Summon");
        setDescription("Teleports selected party member to your location");
        setUsage("/skill Summon <PartyMemberName>");
        setArgumentRange(1, 1);
        setIdentifiers("skill summon");
        setTypes(SkillType.TELEPORTING, SkillType.SILENCEABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection localConfigurationSection = super.getDefaultConfig();
        localConfigurationSection.set(SkillSetting.COOLDOWN.node(), 3600000L);
        return localConfigurationSection;
    }

    public SkillResult use(Hero hero, String[] args) {
        if (!hero.hasParty()) {
            Messaging.send(hero.getPlayer(), "You  aren't in a party.");
            return SkillResult.FAIL;
        }
        Player ptarget = Bukkit.getPlayer(args[0]);
        if (ptarget == null) {
            Util.sendFMsg(hero.getPlayer(), "&7Player&f %s &7 does not exist.", args[0]);
            return SkillResult.CANCELLED;
        }
        if (ptarget.getName().equals(hero.getPlayer().getName())) {
            Messaging.send(hero.getPlayer(), "What would summoning yourself even be good for?");
            return SkillResult.CANCELLED;
        }
        if (hero.getParty().isPartyMember(ptarget)) {
            final Hero ftarget = plugin.getCharacterManager().getHero(ptarget);
            final Hero fhero = hero;
            final long cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 1800000, false);
            Confirmation.textConfirm(ptarget, "Summon: " + hero.getName(), "Accept", "Deny", player -> {
                Player phero = Bukkit.getPlayer(fhero.getPlayer().getName());
                if (phero == null) return;
                if (player == null) return;
                if (ftarget.isInCombat()) {
                    Messaging.send(player, "Summon failed because you're in combat!");
                    Messaging.send(phero, "Summon failed because your target is in combat!");
                    return;
                }
                if (fhero.isInCombat()) {
                    Messaging.send(player, "Summon failed because your summoner is in combat!");
                    Messaging.send(phero, "Summon failed because you're in combat!");
                    return;
                }
                player.teleport(phero);
                if (phero.hasMetadata("SkillSummon")) phero.removeMetadata("SkillSummon", plugin);
                long time = System.currentTimeMillis();
                if (!Util.isOnCooldown(fhero, "Summon", cooldown)) fhero.setCooldown("Summon", time + cooldown);
            },
            ign -> {
                Player p = hero.getPlayer();
                if (p != null) {
                    Util.sendFMsg(hero.getPlayer(), "%s &7 cancelled your summon request.", args[0]);
                    if (p.hasMetadata("SkillSummon")) p.removeMetadata("SkillSummon", plugin);
                }
            });
        } else {
            Messaging.send(hero.getPlayer(), "Target is not in your party.");
        }
        long time = System.currentTimeMillis();
        if (!Util.isOnCooldown(hero, "Summon", 10000)) hero.setCooldown("Summon", time + 10000);
        broadcastExecuteText(hero);
        return SkillResult.CANCELLED;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
