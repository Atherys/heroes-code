package com.atherys.skills.SkillSupport;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillSupport extends ActiveSkill {

	public SkillSupport(Heroes plugin) {
		super(plugin, "Support");
		setDescription("Increases your party's defense by $1% for $2 seconds.");
		setArgumentRange(0, 0);
		setUsage("/skill Support");
		setIdentifiers("skill support");
		setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
		Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
		node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
		node.set("magic-resist", Double.valueOf(0.25D));
		node.set("physical-resist", Double.valueOf(0.25D));
		return node;
	}

	@Override
	public String getDescription(Hero hero) {
		double mr = SkillConfigManager.getUseSetting(hero, this, "magic-resist", 0.25D, false);
		long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
		return getDescription().replace("$1", mr * 100.0D + "").replace("$2", d / 1000L + "");
	}

	@Override
	public SkillResult use(Hero hero, String[] strings) {
		Player player = hero.getPlayer();
		long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
		double magicResist = SkillConfigManager.getUseSetting(hero, this, "magic-resist", 0.25D, false);
		double physicalResist = SkillConfigManager.getUseSetting(hero, this, "physical-resist", 0.25D, false);
		if (!hero.hasParty()) {
			broadcastExecuteText(hero);
			hero.addEffect(new SupportEffect(this, duration, magicResist, physicalResist,hero));
			return SkillResult.NORMAL;
		}
		broadcastExecuteText(hero);
		int radius = SkillConfigManager.getUseSetting(hero, this, "radius", 10, true);
		for (Hero phero : hero.getParty().getMembers()) {
			Player p = hero.getPlayer();
			if (player.getWorld().equals(phero.getPlayer().getWorld()) && phero.getPlayer().getLocation().distanceSquared(p.getLocation()) <= radius * radius) {
				phero.addEffect(new SupportEffect(this, duration, magicResist, physicalResist,phero));
			}
		}
		return SkillResult.NORMAL;
	}

	public class SupportEffect extends ExpirableEffect {
		private final double magicResist;
		private final double physicalResist;

		public SupportEffect(Skill skill, long duration, double magicResist, double physicalResist,Hero hero) {
			super(skill, "Support",hero.getPlayer(), duration);
			this.magicResist = magicResist;
			this.physicalResist = physicalResist;
			this.types.add(EffectType.BENEFICIAL);
			this.types.add(EffectType.MAGIC);
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			Player p = hero.getPlayer();
			Messaging.send(p, "You are now Supported!");
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			Player p = hero.getPlayer();
			Messaging.send(p, "You are no longer Supported!");
		}

		public double getMagicResist() {
			return magicResist;
		}

		public double getPhysicalResist() {
			return physicalResist;
		}
	}

	public class HeroDamageListener implements Listener {
		@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
		public void onEntityDamage(EntityDamageEvent event) {
			if (!(event.getEntity() instanceof Player)) {
				return;
			}

			if (((event.getCause() == EntityDamageEvent.DamageCause.MAGIC) ||
					(event.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK))) {
				Player player = (Player) event.getEntity();
				Hero hero = plugin.getCharacterManager().getHero(player);
				if (!hero.hasEffect("Support")) {
					return;
				} else {
					SkillSupport.SupportEffect supportEffect = (SupportEffect) hero.getEffect("Support");
					if (event.getCause() == EntityDamageEvent.DamageCause.MAGIC) {
						event.setDamage((event.getDamage() * (1 - supportEffect.getMagicResist())));
					} else {
						event.setDamage((event.getDamage() * (1 - supportEffect.getPhysicalResist())));
					}
				}
			}
		}
	}
}