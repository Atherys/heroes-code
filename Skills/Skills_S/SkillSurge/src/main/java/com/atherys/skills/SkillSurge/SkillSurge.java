package com.atherys.skills.SkillSurge;

import com.atherys.effects.WaveStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillSurge extends ActiveSkill {

    public SkillSurge(Heroes plugin) {
        super(plugin, "Surge");
        setDescription("Heal your nearby allies. If you have 3 Wave stacks they will be expended and the skill costs 0 mana.");
        setUsage("/skill Surge");
        setArgumentRange(0, 0);
        setIdentifiers("skill Surge");
        setTypes(SkillType.SILENCEABLE, SkillType.FORCE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set("heal", 35);
        node.set("max-targets", 5);
        return node;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        if (!hero.hasParty()) {
            Messaging.send(player, "You're not in a party.");
            return SkillResult.CANCELLED;
        }
        int maxTarget = SkillConfigManager.getUseSetting(hero, this, "max-targets", 10, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, "heal", 35, false);
        int hit = 0;
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if (e instanceof Player) {
                Player tPlayer = (Player)e;
                if (hero.getParty().isPartyMember(tPlayer)) {
                    Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
                    Lib.healHero(tHero, heal, this, hero);
                    hit++;
                    if (hit >= maxTarget) {
                        break;
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        if (WaveStackEffect.chargeWave(hero)) {
            broadcast(player.getLocation(), "$1 expended their Wave stacks on $2!", player.getDisplayName(), "Surge");
            hero.setCooldown("Surge", SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 25000, false) + System.currentTimeMillis());
        } else {
            int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA, 40, false);
            hero.setMana(hero.getMana() - mana < 0 ? 0 : hero.getMana() - mana);
        }
        return SkillResult.SKIP_POST_USAGE;
    }
}
