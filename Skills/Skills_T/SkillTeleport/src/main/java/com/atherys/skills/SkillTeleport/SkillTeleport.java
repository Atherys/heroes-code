package com.atherys.skills.SkillTeleport;

import com.atherys.heroesaddon.util.Confirmation;
import com.atherys.heroesaddon.util.Lib;
import com.atherys.heroesaddon.util.Util;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SkillTeleport extends ActiveSkill {

    public SkillTeleport(Heroes plugin) {
        super(plugin, "Teleport");
        setDescription("Allows the player to teleport to party members");
        setUsage("/skill Teleport [Player]");
        setArgumentRange(1, 1);
        setIdentifiers("skill teleport");
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (!hero.hasParty()) {
            Messaging.send(hero.getPlayer(), "You aren't in a party.");
            return SkillResult.FAIL;
        }
        String target = args[0];
        Player ptarget = Bukkit.getPlayer(target);
        if (ptarget == null) {
            Messaging.send(hero.getPlayer(), "Player " + ChatColor.WHITE + target + ChatColor.GRAY + " does not exist.");
            return SkillResult.CANCELLED;
        }
        if (ptarget.getName().equals(hero.getPlayer().getName())) {
            Messaging.send(hero.getPlayer(), "What would teleporting to yourself even be good for?");
            return SkillResult.CANCELLED;
        }
        if (hero.getParty().isPartyMember(ptarget)) {
            Player player = hero.getPlayer();
            if (player.hasMetadata("SkillTeleport")) {
                Player tempPlayer = (Player) player.getMetadata("SkillTeleport").get(0).value();
                if (tempPlayer.isOnline()) player.removeMetadata("SkillTeleport", plugin);
            }
            final Hero ftarget = plugin.getCharacterManager().getHero(ptarget);
            final long cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 1800000, false);
            Confirmation.textConfirm(ptarget, "Teleport: " + hero.getName(), "Accept", "Deny", pl -> {
                Player phero = hero.getPlayer();
                if (phero == null) return;
                if (pl == null) return;
                if (ftarget.isInCombat()) {
                    Messaging.send(pl, "Teleport failed because you're in combat!");
                    Messaging.send(phero, "Teleport failed because your target is in combat!");
                    return;
                }
                if (hero.isInCombat()) {
                    Messaging.send(pl, "Teleport failed because they're in combat!");
                    Messaging.send(phero, "You can't Teleport while in combat!");
                    return;
                }
                phero.teleport(pl);
                if (phero.hasMetadata("SkillTeleport")) phero.removeMetadata("SkillTeleport", plugin);
                long time = System.currentTimeMillis();
                if (!Util.isOnCooldown(hero, "Teleport", cooldown)) hero.setCooldown("Teleport", time + cooldown);
            },
            ign -> {
                Player p = Bukkit.getPlayer(hero.getPlayer().getName());
                if (p != null) {
                    p.sendMessage(ftarget.getName() + ChatColor.GRAY + " cancelled your teleport request.");
                    if (p.hasMetadata("SkillTeleport")) p.removeMetadata("SkillTeleport", plugin);
                }
            });
        } else {
            Messaging.send(hero.getPlayer(), "Target is not in your party.");
        }
        long time = System.currentTimeMillis();
        if (!Util.isOnCooldown(hero, "Teleport", 10000)) hero.setCooldown("Teleport", time + 10000);
        broadcastExecuteText(hero);
        return SkillResult.SKIP_POST_USAGE;
    }
}
