package com.atherys.skills.SkillTerrify;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillTerrify extends TargettedSkill {

    public SkillTerrify(Heroes plugin) {
        super(plugin, "Terrify");
        setDescription("Blind your target and after a few seconds, if you are still close to your target stun them as well.");
        setUsage("/skill Terrify");
        setArgumentRange(0, 0);
        setIdentifiers("skill Terrify");
        setTypes(SkillType.SILENCEABLE, SkillType.DEBUFFING, SkillType.ABILITY_PROPERTY_DARK, SkillType.INTERRUPTING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 5000);
        node.set("required-distance", 5);
        node.set("stun-duration", 2000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }


    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (player.equals(target) || !damageCheck(player, target)) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        long stunDuration = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 2000, false);
        int requiredDistance = SkillConfigManager.getUseSetting(hero, this, "required-distance", 5, false);
        TerrifyEffect terrifyEffect = new TerrifyEffect(this, duration, player, stunDuration, requiredDistance,hero);
        broadcastExecuteText(hero, target);
        plugin.getCharacterManager().getCharacter(target).addEffect(terrifyEffect);
        return SkillResult.NORMAL;
    }

    public class TerrifyEffect extends ExpirableEffect {
        private Skill skill;
        private final long stunDuration;
        private final int requiredDistance;
        private Player applier;

        public TerrifyEffect(Skill skill, long duration, Player applier, long stunDuration, int requiredDistance,Hero hero) {
            super(skill, "Terrify",hero.getPlayer(), duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.DARK);
            this.types.add(EffectType.HARMFUL);
            this.skill = skill;
            this.stunDuration = stunDuration;
            this.requiredDistance = requiredDistance;
            this.applier = applier;
            addMobEffect(15, (int) (duration / 50), 3, false);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.DISPELLABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You have been Terrified by $1, get away from them!", applier.getDisplayName());
            player.playSound(player.getLocation(), Sound.ENTITY_WITHER_DEATH, 2.0F, 0.5F);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if (this.isExpired() && player.getWorld().equals(applier.getWorld()) && player.getLocation().distance(applier.getLocation()) <= requiredDistance) {
                StunEffect se = new StunEffect(skill, stunDuration,hero);
                hero.addEffect(se);
                broadcast(player.getLocation(), "$1 got stunned by $2's Terrify!", player.getDisplayName(), applier.getDisplayName());
            } else {
                broadcast(player.getLocation(), "$1 got away from $2's Terrify.", player.getDisplayName(), applier.getDisplayName());
            }
        }
    }
}