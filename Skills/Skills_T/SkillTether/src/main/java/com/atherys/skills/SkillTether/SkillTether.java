package com.atherys.skills.SkillTether;

import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillTether extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillTether(Heroes plugin) {
        super(plugin, "Tether");
        setDescription("Tethers enemies around you for $1s.");
        setUsage("/skill tether");
        setArgumentRange(0, 0);
        setIdentifiers("skill tether");
        setTypes(SkillType.DEBUFFING, SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.MOVEMENT_PREVENTING, SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0D, false) * hero.getSkillLevel(this)) / 1000L;
        duration = duration > 0L ? duration : 0L;
        String description = getDescription().replace("$1", duration + "");
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this);
        if (healthCost > 0) {
            description = description + " HP:" + healthCost;
        }
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (staminaCost > 0) {
            description = description + " FP:" + staminaCost;
        }
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description = description + " W:" + delay + "s";
        }
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description = description + " XP:" + exp;
        }
        return description;
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set("duration-increase", Integer.valueOf(0));
        node.set("exp-per-creature-tethered", Integer.valueOf(0));
        node.set("exp-per-player-tethered", Integer.valueOf(0));
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% was tethered!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% got away!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getUseSetting(null, this, SkillSetting.APPLY_TEXT.node(), "%target% was tethered!").replace("%target%", "$1");
        this.expireText = SkillConfigManager.getUseSetting(null, this, SkillSetting.EXPIRE_TEXT.node(), "%target% got away!").replace("%target%", "$1");
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!hero.getPlayer().hasLineOfSight(target)) {
            return SkillResult.INVALID_TARGET;
        }
        if (target instanceof Player) {
            Player player = hero.getPlayer();
            if ((target != player) && (Skill.damageCheck(player, target))) {
                long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0D, false) * hero.getSkillLevel(this));
                duration = duration > 0L ? duration : 0L;

                CurseEffect cEffect = new CurseEffect(this, duration, hero.getPlayer());
                Hero tHero = this.plugin.getCharacterManager().getHero((Player) target);
                tHero.addEffect(cEffect);

                for (Effect e : hero.getEffects()) {
                    if ((e.isType(EffectType.DISABLE)) || (e.isType(EffectType.STUN))) {
                        hero.removeEffect(e);
                    }
                }
                hero.addEffect(new SlowEffect(this, duration, 1, false, "", "", hero));
                broadcastExecuteText(hero, target);
                return SkillResult.NORMAL;
            }
        }

        return SkillResult.INVALID_TARGET;
    }

    public class CurseEffect extends PeriodicExpirableEffect {
        private Player caster;

        public CurseEffect(Skill skill, long duration, Player caster) {
            super(skill, "Tether",caster, 20L, duration);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.DISABLE);
            this.caster = caster;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillTether.this.applyText, player.getDisplayName());
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You are silenced!");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillTether.this.expireText, player.getDisplayName());
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You are no longer silenced!");
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // TODO Auto-generated method stub
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            if (!player.getWorld().equals(caster.getWorld()) || !caster.hasLineOfSight(player)) {
                this.expire();
            }
            else if (player.getLocation().distanceSquared(this.caster.getLocation()) > 3 * 3) {
                player.teleport(this.caster);
            }
        }
    }
}