package com.atherys.skills.SkillThwack;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;

public class SkillThwack extends ActiveSkill {
    public SkillThwack(Heroes plugin) {
        super(plugin, "Thwack");
        setDescription("Knock back melee attack.");
        setUsage("/skill Thwack");
        setArgumentRange(0, 0);
        setIdentifiers("skill thwack");
        Bukkit.getServer().getPluginManager().registerEvents(new DamageListener(), plugin);
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("buff-duration", 10000);
        node.set("vertical-vector", 3);
        node.set("horizontal-vector", 8);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        long d = (long) SkillConfigManager.getUseSetting(hero, this, "buff-duration", 10000L, false);
        int h = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 8, false);
        int v = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 3, false);
        hero.addEffect(new ThwackEffect(this, d, h, v,hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        long s = SkillConfigManager.getUseSetting(hero, this, "silence-duration", 10000, false);
        return getDescription().replace("$1", damage + "").replace("$2", s / 1000 + "");
    }

    public class ThwackEffect extends ExpirableEffect {
        private final int v;
        private final int h;

        public ThwackEffect(Skill skill, long duration, int h, int v,Hero hero) {
            super(skill, "ThwackEffect",hero.getPlayer(), duration);
            this.v = v;
            this.h = h;
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.IMBUE);
            types.add(EffectType.PHYSICAL);
        }

        public int getHorizontal() {
            return h;
        }

        public int getVertical() {
            return v;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Thwack buff expired");
        }
    }

    public class DamageListener implements Listener {
        @EventHandler(priority = EventPriority.NORMAL)
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.isCancelled() || !(event.getEntity() instanceof LivingEntity)) {
                return;
            }
            if (!(event.getDamager() instanceof Hero)) {
                return;
            }
            if (!(((Hero) event.getDamager()).getPlayer().getItemInHand().getType().equals(Material.BOW))) {
                return;
            }
            Hero h = (Hero) event.getDamager();
            if (h.hasEffect("ThwackEffect")) {
                ThwackEffect te = (ThwackEffect) h.getEffect("ThwackEffect");
                Vector v = event.getEntity().getLocation().add(0, 1, 0).toVector().subtract(h.getPlayer().getLocation().toVector()).normalize();
                v = v.multiply(te.getHorizontal());
                v = v.setY(te.getVertical());
                event.getEntity().setVelocity(v);
                h.removeEffect(te);
            }
        }
    }
}