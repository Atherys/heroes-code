package com.atherys.skills.SkillTimeTurner;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

/*
 * Made by pumpapa
 */
public class SkillTimeTurner extends ActiveSkill {
    private String expireText;

    public SkillTimeTurner(Heroes plugin) {
        super(plugin, "TimeTurner");
        setArgumentRange(0, 0);
        setDescription("Active\nAfter a duration, you get teleported back to where you casted TimeTurner with the same mana and health as when you casted it.");
        setIdentifiers("skill timeturner");
        setUsage("/skill timeturner");
        setTypes(SkillType.SILENCEABLE, SkillType.FORM_ALTERING, SkillType.TELEPORTING);
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("after-teleport-percentage", 75);
        node.set(SkillSetting.DURATION.node(), 5000);
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1's TimeTurner activated!");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        Damageable dplayer = player;
        double health = dplayer.getHealth();
        int mana = hero.getMana();
        Location loc = player.getLocation();
        TimeTurnerEffect tte = new TimeTurnerEffect(this, duration, health, mana, loc, expireText,hero);
        hero.addEffect(tte);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class TimeTurnerEffect extends PeriodicExpirableEffect {
        private final String expireText;
        private final double health;
        private final int mana;
        private final Location loc;

        public TimeTurnerEffect(Skill skill, long duration, double health, int mana, Location loc, String expireText,Hero hero) {
            super(skill, "TimeTurnerEffect",hero.getPlayer(), duration, 800);
            this.expireText = expireText;
            this.health = health;
            this.mana = mana;
            this.loc = loc;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if (this.isExpired()) {
                broadcast(player.getLocation(), this.expireText, player.getDisplayName());
                player.setHealth(health);
                hero.setMana(mana);
                player.teleport(loc, PlayerTeleportEvent.TeleportCause.COMMAND);
            }
            loc.getWorld().playSound(loc, Sound.ENTITY_ENDERMEN_TELEPORT, 1F, 0.5F);
            Location current = hero.getPlayer().getLocation();
            current.getWorld().playSound(current, Sound.ENTITY_ENDERMEN_TELEPORT, 1F, 0.5F);
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (double[] coords : Lib.playerParticleCoords) {
                Location location = player.getLocation();
                location.add(coords[0], coords[1], coords[2]);
                location.getWorld().spawnParticle(Particle.SPELL_WITCH, location, 1 );
            }
            loc.getWorld().playEffect(loc, Effect.ENDER_SIGNAL, 10);
        }

        @Override public void tickMonster(Monster monster) {} // Unused
    }
}