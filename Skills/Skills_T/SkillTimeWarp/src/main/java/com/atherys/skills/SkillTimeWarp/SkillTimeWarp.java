package com.atherys.skills.SkillTimeWarp;

import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillTimeWarp extends ActiveSkill {
    public SkillTimeWarp(Heroes plugin) {
        super(plugin, "TimeWarp");
        setDescription("Speeds up allies, slows foes for $1 duration.");
        setUsage("/skill TimeWarp");
        setArgumentRange(0, 0);
        setIdentifiers("skill timewarp");
        setTypes(SkillType.BUFFING, SkillType.SILENCEABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set("multiplier", 2);
        node.set(SkillSetting.DURATION.node(), 10000L);
        node.set("party-cooldown", 20000);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player caster = hero.getPlayer();
        int m = SkillConfigManager.getUseSetting(hero, this, "multiplier", 2, false);
        long du = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long pcd = (long) SkillConfigManager.getUseSetting(hero, this, "party-cooldown", 20000L, false);
        if (hero.hasParty()) {
            hero.addEffect(new QuickenEffect(this, getName(), du, m, "", "",hero));
            for (Entity e : caster.getNearbyEntities(r, r, r)) {
                if (e instanceof Player) {
                    Hero targetHero = this.plugin.getCharacterManager().getHero((Player) e);
                    if (hero.getParty().getMembers().contains(targetHero)) {
                        targetHero.addEffect(new QuickenEffect(this, getName(), du, m, "", "",targetHero));
                        if (targetHero.hasAccessToSkill(this) && (!hero.equals(targetHero)) && (hero.getCooldown("TimeWarp") == null || hero.getCooldown("TimeWarp") < pcd + System.currentTimeMillis())) {
                            targetHero.setCooldown("TimeWarp", pcd + System.currentTimeMillis());
                        }
                    } else {
                        if (damageCheck((LivingEntity) caster, targetHero.getPlayer())) {
                            targetHero.addEffect(new SlowEffect(this, getName(), du, m, true, "", "", targetHero));
                        }
                    }
                }
            }
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }

        hero.addEffect(new QuickenEffect(this, getName(), du, m, "", "",hero));
        for (Entity e : caster.getNearbyEntities(r, r, r)) {
            if (e instanceof Player) {
                if (damageCheck(caster, (LivingEntity) e)) {
                    Hero targetHero = this.plugin.getCharacterManager().getHero((Player) e);
                    targetHero.addEffect(new SlowEffect(this, getName(), du, m, true, "", "", hero));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        long d = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        return getDescription().replace("$1", "" + d / 1000);
    }

    public class QuickenEffect extends ExpirableEffect {
        private final String applyText;
        private final String expireText;

        public QuickenEffect(final Skill skill, final String name, final long duration, final int amplifier, final String applyText, final String expireText,Hero hero) {
            super(skill, name,hero.getPlayer(), duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.SPEED);
            this.addMobEffect(1, (int) (duration / 1000L) * 20, amplifier, false);
            this.applyText = applyText;
            this.expireText = expireText;
        }

        @Override
        public void applyToHero(final Hero hero) {
            super.applyToHero(hero);
            final Player player = hero.getPlayer();
            this.broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        @Override
        public void removeFromHero(final Hero hero) {
            super.removeFromHero(hero);
            final Player player = hero.getPlayer();
            this.broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
    }
}
