package com.atherys.skills.SkillTornado;

import com.atherys.effects.SkillCastingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class SkillTornado extends ActiveSkill {

	public SkillTornado(Heroes plugin) {
		super(plugin, "Tornado");
		setDescription("After channeling for a few seconds, you send all enemy players around flying in a spiral.");
		setUsage("/skill Tornado");
		setArgumentRange(0, 0);
		setIdentifiers("skill Tornado");
		setTypes(SkillType.SILENCEABLE, SkillType.FORCE);
	}

	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.RADIUS.node(), 5);
		node.set(SkillSetting.DURATION.node(), 5000);
		node.set("safefall-duration", 8000);
		node.set("warm-up", 6000);
		node.set("max-targets", 5);
		return node;
	}

	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	public SkillResult use(Hero hero, String[] args) {
		long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
		long warmup = SkillConfigManager.getUseSetting(hero, this, "warm-up", 6000, false);
		int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
		int maxTargets = SkillConfigManager.getUseSetting(hero, this, "max-targets", 5, false);
		long safefallDuration = SkillConfigManager.getUseSetting(hero, this, "safefall-duration", 8000, false);
		hero.addEffect(new CastingTornadoEffect(this, warmup, false, duration, radius, maxTargets, safefallDuration,hero));
		return SkillResult.NORMAL;
	}

	public class CastingTornadoEffect extends SkillCastingEffect {
		private long duration;
		private int radius;
		private int maxTargets;
		private long safefallDuration;

		public CastingTornadoEffect(Skill skill, long warmup, boolean slow, long duration, int radius, int maxTargets, long safefallDuration,Hero hero) {
			super(skill, warmup, slow, true,hero);
			this.duration = duration;
			this.radius = radius;
			this.maxTargets = maxTargets;
			this.safefallDuration = safefallDuration;
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			Player player = hero.getPlayer();
			if (this.isExpired()) {
				int counter = 0;
				for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
					if (e instanceof Player) {
						Player target = (Player) e;
						if (damageCheck(player, (org.bukkit.entity.LivingEntity) target)) {
							if (counter >= maxTargets) {
								break;
							}
							plugin.getCharacterManager().getHero(target).addEffect(new TornadoEffect(skill, duration, safefallDuration,hero));
							counter++;
						}
					}
				}
			}
		}
	}

	public class TornadoEffect extends PeriodicExpirableEffect {
		private long safefallDuration;
		private float ticker_vertical = 0.0f;
		private float ticker_horisontal = (float) (Math.random() * 2 * Math.PI);

		public TornadoEffect(Skill skill, long duration, long safefallDuration,Hero hero) {
			super(skill, "TornadoEffect",hero.getPlayer(), 200L, duration);
			this.safefallDuration = safefallDuration;
		}

		@Override
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			Messaging.send(hero.getPlayer(), "You've been caught in the $1!", "Tornado");
			hero.getPlayer().setAllowFlight(true);
		}

		@Override
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			hero.addEffect(new TornadoSafeFallEffect(skill, safefallDuration,hero));
			hero.getPlayer().setAllowFlight(false);
		}

		@Override
		public void tickHero(Hero hero) {
			Player player = hero.getPlayer();
			if (!Util.transparentBlocks.contains(player.getLocation().getBlock().getRelative(BlockFace.UP).getRelative(BlockFace.UP).getType())) {
				this.expire();
			}
			else {
				double radius = Math.sin(verticalTicker()) * 2;
				float horisontal = horisontalTicker();

				Vector v = new Vector(radius * Math.cos(horisontal), 0.6D, radius * Math.sin(horisontal));
				player.setVelocity(v);
			}
			
		}
		@Override
		public void tickMonster(Monster monster) {
			// N/A
		}

		private float verticalTicker() {
			if (ticker_vertical < 1.0f) {
				ticker_vertical += 0.05f;
			}
			return ticker_vertical;
		}

		private float horisontalTicker() {
			return (ticker_horisontal += 0.5f);
		}
	}

    public class TornadoSafeFallEffect extends ExpirableEffect {

        public TornadoSafeFallEffect(Skill skill, long duration,Hero hero) {
            super(skill, "TornadoSafeFall",hero.getPlayer(),duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.SAFEFALL);
            this.types.add(EffectType.MAGIC);
        }
    }
}
