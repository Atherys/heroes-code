package com.atherys.skills.SkillTrack;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillTrack extends ActiveSkill {

    public SkillTrack(Heroes plugin) {
        super(plugin, "Track");
        setDescription("Gives you tons of info on a player.");
        setUsage("/skill track [player]");
        setArgumentRange(0, 1);
        setIdentifiers("skill track");
        setTypes(SkillType.ABILITY_PROPERTY_EARTH, SkillType.SILENCEABLE);
    }

    public String getDescription(Hero hero) {
        String description = getDescription();
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this);
        if (healthCost > 0) {
            description = description + " HP:" + healthCost;
        }
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (staminaCost > 0) {
            description = description + " FP:" + staminaCost;
        }
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description = description + " W:" + delay + "s";
        }
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description = description + " XP:" + exp;
        }
        return description;
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(3000));
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {

        if ( args.length < 1 ) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Please specify a player name.");
            return SkillResult.CANCELLED;
        }

        Player player = hero.getPlayer();
        Player target = Bukkit.getServer().getPlayer(args[0]);

        if (target == null) {
            return SkillResult.INVALID_TARGET;
        }

        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3000, false);
        if (target.getWorld() != player.getWorld()) {
            broadcastExecuteText(hero);
            Messaging.send(player, "Your target is not in the same world as you!");
            return SkillResult.NORMAL;
        }
        Location location = target.getLocation();
        Location hLoc = hero.getPlayer().getLocation();
        Hero tHero = this.plugin.getCharacterManager().getHero(target);
        Messaging.send(player, "$1 is a level $2 $3 with $4 health!", tHero.getPlayer().getDisplayName(), Integer.valueOf(tHero.getLevel(tHero.getHeroClass())), tHero.getHeroClass().getName(), tHero.getPlayer().getHealth());

        if (hLoc.distanceSquared(location) > (radius * radius)) {
            Messaging.send(player, "$1 is beyond tracking range, but you catch their scent!", tHero.getPlayer().getDisplayName());
        } else {
            if (hero.hasParty()) {
                hero.getParty().messageParty("$1 was tracked at X: $2, Y: $3, Z: $4 by $5", target.getName(), Integer.valueOf((int) location.getX()), Integer.valueOf((int) location.getY()), Integer.valueOf((int) location.getZ()), player.getName());
                player.setCompassTarget(location);
            }
            else {
                Messaging.send(player, "$1 was tracked at X: $2, Y: $3, Z: $4 by $5", target.getName(), Integer.valueOf((int) location.getX()), Integer.valueOf((int) location.getY()), Integer.valueOf((int) location.getZ()), player.getName());
                player.setCompassTarget(location);
            }
        }

        Messaging.send(player, "Your current pos: $1: $2, $3, $4", hLoc.getWorld().getName(), Integer.valueOf((int) hLoc.getX()), Integer.valueOf((int) hLoc.getY()), Integer.valueOf((int) hLoc.getZ()));
        if (damageCheck((org.bukkit.entity.LivingEntity) player, tHero.getPlayer())) {
            Messaging.send(player, "You can hurt $1", tHero.getPlayer().getDisplayName());
        } else {
            Messaging.send(player, "You cannot hurt $1", tHero.getPlayer().getDisplayName());
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
