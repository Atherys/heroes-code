package com.atherys.skills.SkillTripwire;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillTripwire extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillTripwire(Heroes plugin) {
        super(plugin, "Tripwire");
        setDescription("Saboteur places tripwire for $1 seconds. When an entity crosses this location the entity will take $2 damage.");
        setUsage("/skill Tripwire");
        setArgumentRange(0, 0);
        setIdentifiers("skill Tripwire");
        setTypes(SkillType.SILENCEABLE);
        Bukkit.getPluginManager().registerEvents(new DamageListener(), plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        Block tarBlock = hero.getPlayer().getTargetBlock((Set<Material>) null, max);
        float dir = (float) Math.toDegrees(Math.atan2(player.getLocation().getBlockX() - tarBlock.getX(), tarBlock.getZ() - player.getLocation().getBlockZ()));
        BlockFace tarFace = getClosestFace(dir);
        int dist = getPath(tarBlock, tarFace, max);
        Set<Block> pathBlocks = new HashSet<Block>();
        if (dist <= 2) {
            Messaging.send(player, "No room to place tripwire here!");
            return SkillResult.CANCELLED;
        }
        Block block;
        // First Block
        block = tarBlock.getRelative(tarFace);
        block.setType(Material.TRIPWIRE_HOOK);
        block.setData(faceToData(tarFace));
        block.getState().update();
        block.setMetadata("TripwireStringOrHook", new FixedMetadataValue(plugin, hero.getPlayer().getName()));
        pathBlocks.add(block);
        // Last Block
        block = tarBlock.getRelative(tarFace, dist - 1);
        block.setType(Material.TRIPWIRE_HOOK);
        block.setData(faceToData(tarFace.getOppositeFace()));
        block.getState().update();
        block.setMetadata("TripwireStringOrHook", new FixedMetadataValue(plugin, hero.getPlayer().getName()));
        pathBlocks.add(block);
        // Tripwires
        for (int i = 2; i < dist - 1; i++) {
            block = tarBlock.getRelative(tarFace, i);
            block.setType(Material.TRIPWIRE);
            block.setMetadata("TripwireStringOrHook", new FixedMetadataValue(plugin, hero.getPlayer().getName()));
            pathBlocks.add(block);
        }
        hero.addEffect(new TripwireEffect(this, duration, damage, pathBlocks,hero));
        return SkillResult.NORMAL;
    }

    // Returns true if it isn't one
    private boolean canHold(Block b) {
        return (b.getType() != Material.AIR && b.getType() != Material.SNOW && b.getType() != Material.LAVA && b.getType() != Material.WATER && b.getType() != Material.LEAVES && b.getType() != Material.STEP && b.getType() != Material.VINE);
    }

    private boolean canSupport(Block b) {
        return (b.getType() != Material.LAVA && b.getType() != Material.WATER && b.getType() != Material.LEAVES && b.getType() != Material.STEP);
    }

    private boolean isReplaceable(Block b) {
        return (b.getType() == Material.AIR || b.getType() == Material.SNOW);
    }

    public BlockFace getClosestFace(float direction) {
        direction = direction % 360;
        if (direction < 0)
            direction += 360;
        int dir = Math.round(direction / 90);
        switch (dir) {
            case 0:
                return BlockFace.NORTH;
            case 1:
                return BlockFace.EAST;
            case 2:
                return BlockFace.SOUTH;
            case 3:
                return BlockFace.WEST;
            default:
                return BlockFace.NORTH;
        }
    }

    public int getPath(Block b, BlockFace face, int max) {
        Block curr = b;
        Block below;
        // If the block before the path can hold
        if (!canHold(curr))
            return 0;
        // Actual Blocks to be replaced
        int i;
        for (i = 1; i < max; i++) {
            curr = curr.getRelative(face);
            below = curr.getRelative(BlockFace.DOWN);
            if (!canSupport(below))
                return 0;
            if (!isReplaceable(curr))
                break;
        }
        // If the block after can hold
        if (!canHold(curr))
            return 0;
        return i;
    }

    public byte faceToData(BlockFace b) {
        switch (b) {
            case NORTH:
                return 2;
            case SOUTH:
                return 0;
            case EAST:
                return 3;
            case WEST:
                return 1;
            default:
                return 0;
        }
    }

    public class TripwireEffect extends ExpirableEffect {
        private final double damage;
        private final Set<Block> pathBlocks;
        private long time;

        public TripwireEffect(Skill skill, long duration, double damage, Set<Block> pathBlocks,Hero hero) {
            super(skill, "TripwireEffect",hero.getPlayer(), duration);
            this.damage = damage;
            this.pathBlocks = pathBlocks;
            this.types.add(EffectType.FORM);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
        }

        public double getDamage() {
            return damage;
        }

        public Long getTime() {
            return time;
        }

        public void setNewTime(long time) {
            this.time = time;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
	        Messaging.send(hero.getPlayer(), applytext, hero.getPlayer().getDisplayName(), "Tripwire");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            for (Block block : pathBlocks) {
                if (block.hasMetadata("TripwireStringOrHook")) {
                    block.removeMetadata("TripwireStringOrHook", plugin);
                }
            }
            Lib.removeBlocks(pathBlocks);
	        Messaging.send(hero.getPlayer(), expiretext, hero.getPlayer().getDisplayName(), "Tripwire");
        }
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.DAMAGE.node(), 100);
        node.set(SkillSetting.MAX_DISTANCE.node(), 15);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% placed %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% has been removed.");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% placed %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% has been removed.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 100, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", damage + "");
    }

    public class DamageListener implements Listener {
        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onPlayerInteract(PlayerInteractEvent event) {
            if (event.getAction() != Action.PHYSICAL)
                return;
            Block block = event.getClickedBlock();
            if (block.getType() != Material.TRIPWIRE || !block.hasMetadata("TripwireStringOrHook"))
                return;
            Player player = Bukkit.getPlayer(block.getMetadata("TripwireStringOrHook").get(0).asString());
            if (player == null)
                return;
            if (damageCheck(player, (org.bukkit.entity.LivingEntity) event.getPlayer())) {
                Hero caster = plugin.getCharacterManager().getHero(player);
                TripwireEffect tw = (TripwireEffect) caster.getEffect("TripwireEffect");
                if (tw.getTime() > System.currentTimeMillis()) return;
                addSpellTarget(event.getPlayer(), caster);
                damageEntity(event.getPlayer(), player, tw.getDamage(), DamageCause.ENTITY_EXPLOSION);
                tw.setNewTime(System.currentTimeMillis() + 1000L);
            }
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onBlockBreak(BlockBreakEvent event) {
            if (event.getBlock().hasMetadata("TripwireStringOrHook")) {
                event.setCancelled(true);
            }
        }
    }
}