package com.atherys.skills.SkillUndeath;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class SkillUndeath extends PassiveSkill {
    private String rebornText;

    public SkillUndeath(Heroes plugin) {
        super(plugin, "Undeath");
        setDescription("If you are about to die instead you regain $1% hp, can only trigger once every $2 seconds. $3s after rebirth you will die.");
        setTypes(SkillType.MOVEMENT_PREVENTION_COUNTERING, SkillType.ABILITY_PROPERTY_DARK);
        Bukkit.getServer().getPluginManager().registerEvents(new RebornListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("health-percent-on-rebirth", Double.valueOf(0.5D));
        node.set("health-increase", Double.valueOf(0.0D));
        node.set("on-reborn-text", "%hero% is saved from death, but weakened!");
        node.set("kill-after", 10000L);
        node.set(SkillSetting.COOLDOWN.node(), Integer.valueOf(600000));
        node.set(SkillSetting.COOLDOWN_REDUCE.node(), Integer.valueOf(0));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int health = (int) ((SkillConfigManager.getUseSetting(hero, this, "health-percent-on-rebirth", 0.5D, false) + SkillConfigManager.getUseSetting(hero, this, "health-increase", 0.0D, false) * hero.getSkillLevel(this)) * 100.0D);
        int cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 600000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        long d = SkillConfigManager.getUseSetting(hero, this, "kill-after", 10000, false);
        String description = getDescription().replace("$1", health + "").replace("$2", cooldown + "").replace("$3", "" + d / 1000);
        return description;
    }

    @Override
    public void init() {
        super.init();
        rebornText = SkillConfigManager.getUseSetting(null, this, "on-reborn-text", "%hero% is saved from death, but weakened!").replace("%hero%", "$1");
    }

    public class UndeathDeathEffect extends ExpirableEffect {
        private boolean isDead = false;

        public UndeathDeathEffect(Skill skill, long duration,Hero hero) {
            super(skill, "UndeathDeath",hero.getPlayer(), duration);
            addMobEffect(1, (int) (duration / 1000L) * 20, 1, false);
            this.types.add(EffectType.UNBREAKABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if (this.isExpired()) {
                if (!isDead) {
                    isDead = true;
                    broadcast(player.getLocation(), "$1 died from $2.", player.getName(), "Undeath");
                    player.setHealth(0);
                }
            } else {
                broadcast(player.getLocation(), "$1 saved himself from $2!", player.getName(), "Undeath");
            }
        }
    }

    public class RebornListener implements Listener {
        private final Skill skill;

        public RebornListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
                return;
            }
            final Player player = (Player) event.getEntity();
            double currentHealth = player.getHealth();
            if (currentHealth > event.getDamage()) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero(player);
            if ((hero.hasEffect("Undeath")) && (
                    (hero.getCooldown("Undeath") == null) || (hero.getCooldown("Undeath").longValue() <= System.currentTimeMillis()))) {
                double regainPercent = SkillConfigManager.getUseSetting(hero, skill, "health-percent-on-rebirth", 0.5D, false) + SkillConfigManager.getUseSetting(hero, this.skill, "health-increase", 0.0D, false) * hero.getSkillLevel(this.skill);
                double healthRegain = (player.getMaxHealth() * regainPercent);
                hero.heal(healthRegain);
                event.setDamage(0);
                event.setCancelled(true);
                long cooldown = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN.node(), 600000, false) + SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getLevel();
                long time = (long) SkillConfigManager.getUseSetting(hero, skill, "kill-after", 10000L, false);
                hero.setCooldown("Undeath", cooldown + System.currentTimeMillis());
                broadcast(player.getLocation(), rebornText, player.getDisplayName());
                player.getLocation().getWorld().spawnParticle(Particle.SMOKE_NORMAL, player.getLocation(), 1);
                player.getWorld().playSound(player.getLocation(), Sound.ENTITY_GHAST_SCREAM, 6, 1);
                UndeathDeathEffect udd = new UndeathDeathEffect(skill, time,hero);
                hero.addEffect(udd);
            }
        }

        @EventHandler
        public void onPlayerDeath(PlayerDeathEvent event) {
            if (event.getEntity().getKiller() != null && event.getEntity().getKiller().isOnline()) {
                Hero hero = plugin.getCharacterManager().getHero(event.getEntity().getKiller());
                if (hero.hasEffect("UndeathDeath")) {
                    hero.removeEffect(hero.getEffect("UndeathDeath"));
                }
            }
        }
    }
}