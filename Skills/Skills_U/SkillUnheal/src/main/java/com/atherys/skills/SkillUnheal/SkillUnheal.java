package com.atherys.skills.SkillUnheal;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.potion.PotionEffectType;

public class SkillUnheal extends TargettedSkill {
    private String expireText;
    private String missText;

    public SkillUnheal(Heroes plugin) {
        super(plugin, "Unheal");
        //this.plugin = plugin;
        setDescription("Causes all heals to become damage for $2s. R:$1");
        setUsage("/skill unheal");
        setArgumentRange(0, 0);
        setIdentifiers("skill unheal");
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(), plugin);
        //registerEvent(Type.ENTITY_REGAIN_HEALTH, new SkillEntityListener(), Priority.Normal);
        //registerEvent(Type.CUSTOM_EVENT, new SkillEventListener(), Priority.Normal);
        setTypes(SkillType.DEBUFFING, SkillType.SILENCEABLE, SkillType.HEALTH_FREEZING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), 15);
        node.set(SkillSetting.MAX_DISTANCE_INCREASE.node(), 0);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("duration-reduce", 0);
        node.set("miss-text", "%target%s heal was inverted!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the curse!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        missText = SkillConfigManager.getUseSetting(null, this, "miss-text", "%target%s heal was inverted!").replace("%target%", "$1");
        expireText = SkillConfigManager.getUseSetting(null, this, SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the curse!").replace("%target%", "$1");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        if (!(target instanceof Player) || !damageCheck(player, target)) {
            return SkillResult.INVALID_TARGET;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false);
        UnhealEffect unhealEffect = new UnhealEffect(this, duration, player);
        Player tplayer = (Player) target;
        Hero tHero = plugin.getCharacterManager().getHero(tplayer);
        if (tplayer.hasPotionEffect(PotionEffectType.REGENERATION)) {
            tplayer.removePotionEffect(PotionEffectType.REGENERATION);
        }
        tHero.addEffect(unhealEffect);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class UnhealEffect extends ExpirableEffect {
        private Player caster;

        public UnhealEffect(Skill skill, long duration, Player caster) {
            super(skill, "Unheal",caster, duration);
            this.caster = caster;
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.MAGIC);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.WITHER);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(20, tickDuration, 1, false);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName());
        }

        public Player getCaster() {
            return caster;
        }
    }

    public class SkillEntityListener implements Listener {
        @EventHandler (ignoreCancelled = true)
        public void onEntityRegainHeroHealth(HeroRegainHealthEvent event) {
            Hero hero = event.getHero();
            if (hero.hasEffect("Unheal")) {
                if (hero.getPlayer().hasPotionEffect(PotionEffectType.REGENERATION)) {
                    hero.getPlayer().removePotionEffect(PotionEffectType.REGENERATION);
                }
                double damage = event.getDelta();
                event.setDelta(0D);
                addSpellTarget(hero.getPlayer(), hero);
                UnhealEffect unhealEffect = (UnhealEffect) hero.getEffect("Unheal");
                damageEntity(hero.getPlayer(), unhealEffect.getCaster(), damage, DamageCause.MAGIC);
                broadcast(hero.getPlayer().getLocation(), missText, hero.getPlayer().getDisplayName());
            }
        }

        @EventHandler (ignoreCancelled = true)
        public void onEntityRegainRegularHealth(EntityRegainHealthEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("Unheal")) {
                if (hero.getPlayer().hasPotionEffect(PotionEffectType.REGENERATION)) {
                    hero.getPlayer().removePotionEffect(PotionEffectType.REGENERATION);
                }
                double damage = event.getAmount();
                event.setAmount(0D);
                addSpellTarget(hero.getPlayer(), hero);
                UnhealEffect unhealEffect = (UnhealEffect) hero.getEffect("Unheal");
                damageEntity(hero.getPlayer(), unhealEffect.getCaster(), damage, DamageCause.MAGIC);
                broadcast(hero.getPlayer().getLocation(), missText, hero.getPlayer().getDisplayName());
            }
        }
    }
}