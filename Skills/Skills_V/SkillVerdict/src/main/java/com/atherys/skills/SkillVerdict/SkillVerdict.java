package com.atherys.skills.SkillVerdict;


import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.atherys.effects.StunEffect;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;


public class SkillVerdict extends TargettedSkill {
	public SkillVerdict(Heroes plugin) {
		super(plugin, "Verdict");
		setDescription("$1s Stun but stun yourself as well");
		setUsage("/skill Verdict");
		setArgumentRange(0, 0);
		setIdentifiers("skill Verdict");
		setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.DEBUFFING);
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}

	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
		node.set(SkillSetting.MAX_DISTANCE.node(), 7);
		return node;
	}

	@Override
	public SkillResult use(Hero hero, LivingEntity target, String[] args) {
		Player player = hero.getPlayer();
		if (!(target instanceof Player)) {
			return SkillResult.INVALID_TARGET;
		}
		if (((Player) target).equals(player)) {
			return SkillResult.INVALID_TARGET;
		}
		Player tPlayer = (Player) target;
		if (!damageCheck(player, (LivingEntity) tPlayer)) {
			Messaging.send(player, "You can't harm that target", new Object[0]);
			return SkillResult.INVALID_TARGET_NO_MSG;
		}
		Hero tHero = this.plugin.getCharacterManager().getHero(tPlayer);
		broadcastExecuteText(hero, target);
		long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0D, false) * hero.getSkillLevel(this));
		duration = duration > 0L ? duration : 0L;
		if (duration > 0L) {
			tHero.addEffect(new StunEffect(this, duration,tHero));
			hero.addEffect(new StunEffect(this, duration,hero));
		}
		for (Effect effect : tHero.getEffects()) {
			if ((effect.isType(EffectType.INVULNERABILITY) || effect.getName().equals("Bulwark")) && !effect.isType(EffectType.UNBREAKABLE)) {
				tHero.removeEffect(effect);
			}
		}

		return SkillResult.NORMAL;
	}
}