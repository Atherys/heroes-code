package com.atherys.skills.SkillVoidBlade;

import com.atherys.effects.SilenceEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillVoidBlade extends PassiveSkill {
    private Skill voidblade;

    public SkillVoidBlade(Heroes plugin) {
        super(plugin, "VoidBlade");
        setDescription("Passive\nYou have a chance to silence your enemies on attack.");
        setTypes(SkillType.MOVEMENT_INCREASE_COUNTERING, SkillType.BUFFING, SkillType.SILENCEABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
        //registerEvent(Type.CUSTOM_EVENT, new SkillHeroListener(this), Priority.Normal);
    }

    @Override
    public String getDescription(Hero hero) {
        int level = hero.getSkillLevel(this);
        double chance = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE.node(), 0.2, false) +
                (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE_LEVEL.node(), 0.2, false) * level)) * 100;
        chance = chance > 0 ? chance : 0;
        double duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 2000, false) +
                (level * SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false))) / 1000;
        duration = duration > 0 ? duration : 0;
        String description = getDescription().replace("$1", chance + "").replace("$2", duration + "");
        //COOLDOWN
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false)
                - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description += " CD:" + cooldown + "s";
        }
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.CHANCE.node(), 0.2);
        node.set(SkillSetting.COOLDOWN.node(), 3000);
        node.set(SkillSetting.DURATION.node(), 2000);
        return node;
    }

    @Override
    public void init() {
        super.init();
        voidblade = this;
    }

    public class SkillHeroListener implements Listener {
        private final Skill skill;

        public SkillHeroListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
	        if (event.getDamage() == 0 || event.getCause() != DamageCause.ENTITY_ATTACK || !(event.getEntity() instanceof Player) || !(event instanceof EntityDamageByEntityEvent))
		        return;
	        EntityDamageByEntityEvent edby = (EntityDamageByEntityEvent) event;
	        Player tPlayer = (Player) event.getEntity();
	        if (edby.getDamager() instanceof Player) {
		        Player player = (Player) edby.getDamager();
		        Hero hero = plugin.getCharacterManager().getHero(player);
		        if (damageCheck(hero.getPlayer(), (org.bukkit.entity.LivingEntity) tPlayer)) {
			        if (hero.hasEffect("VoidBlade")) {
				        if (hero.getCooldown("VoidBlade") == null || hero.getCooldown("VoidBlade") <= System.currentTimeMillis()) {
					        double chance = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.CHANCE, 0.2, false);
					        chance = chance > 0 ? chance : 0;
					        long cooldown = (long) (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN, 0, false));
					        hero.setCooldown("VoidBlade", cooldown + System.currentTimeMillis());
					        if (Math.random() <= chance) {
						        long duration = (long) (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 2000, false));
						        duration = duration > 0 ? duration : 0;
						        Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
						        tHero.addEffect(new SilenceEffect(voidblade, duration, true,tHero));
						        Lib.cancelDelayedSkill(tHero);
					        }
				        }
			        }
		        }
	        } else if (edby.getDamager() instanceof Projectile) {
		        if (((Projectile) edby.getDamager()).getShooter() instanceof Player) {
			        Player player = (Player) ((Projectile) edby.getDamager()).getShooter();
			        Hero hero = plugin.getCharacterManager().getHero(player);
			        if (damageCheck(hero.getPlayer(), (org.bukkit.entity.LivingEntity) tPlayer)) {
				        if (hero.hasEffect("VoidBlade")) {
					        if (hero.getCooldown("VoidBlade") == null || hero.getCooldown("VoidBlade") <= System.currentTimeMillis()) {
						        double chance = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.CHANCE, 0.2, false);
						        chance = chance > 0 ? chance : 0;
						        long cooldown = (long) (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN, 0, false));
						        cooldown = cooldown > 0 ? cooldown : 0;
						        hero.setCooldown("VoidBlade", cooldown + System.currentTimeMillis());
						        if (Math.random() <= chance) {
							        long duration = (long) (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DURATION, 2000, false));
							        duration = duration > 0 ? duration : 0;
							        Hero tHero = plugin.getCharacterManager().getHero(tPlayer);
							        tHero.addEffect(new SilenceEffect(voidblade, duration, true,tHero));
							        Lib.cancelDelayedSkill(tHero);
						        }
					        }
				        }
			        }
		        }
	        }
        }
    }
}