package com.atherys.skills.SkillVoidGate;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.BlockIterator;

import java.util.HashSet;
import java.util.Set;

import static org.bukkit.Material.*;

public class SkillVoidGate extends ActiveSkill {

    public SkillVoidGate(Heroes plugin) {
        super(plugin, "VoidGate");
        setDescription("Places a VoidGate that gets activated once the caster re-uses the skill. Once someone clicks the VoidGate they get teleported to the exit point.");
        setUsage("/skill VoidGate");
        setArgumentRange(0, 0);
        setIdentifiers("skill VoidGate");
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), plugin);
        setTypes(SkillType.SILENCEABLE, SkillType.TELEPORTING, SkillType.STEALTHY);
    }


    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("activation-duration", 15000);
        node.set("portal-duration", 10000);
        node.set(SkillSetting.MAX_DISTANCE.node(), 5);
        node.set("max-portal-range", 50);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        if (!hero.hasEffect("VoidGate")) {
            BlockIterator iterator;
            Block block = null;
            int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 5, false);
            try {
                iterator = new BlockIterator(player, distance);
            } catch (IllegalStateException e) {
                return null;
            }
            while (iterator.hasNext()) {
                block = iterator.next();
                if (!(Util.transparentBlocks.contains(block.getType()) && (Util.transparentBlocks.contains(block.getRelative(BlockFace.UP).getType()) || Util.transparentBlocks.contains(block.getRelative(BlockFace.DOWN).getType())))) {
                    break;
                }
            }
            if (block == null || block.getType() == AIR || block.getRelative(BlockFace.UP).getType() != AIR) {
                player.sendMessage("You must target a block!");
                return SkillResult.CANCELLED;
            }
            if (hero.hasEffect("Invisible")) {
                hero.removeEffect(hero.getEffect("Invisible"));
            }
            block = block.getRelative(BlockFace.UP);
            block.setType(COAL_BLOCK);
            block.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
            long duration = SkillConfigManager.getUseSetting(hero, this, "activation-duration", 15000, false);
            hero.addEffect(new VoidGateEffect(this, duration, block,hero));
            broadcastExecuteText(hero);
            return SkillResult.SKIP_POST_USAGE;
        } else {
            VoidGateEffect voidGateEffect = (VoidGateEffect)hero.getEffect("VoidGate");
            Block block = voidGateEffect.getBlock();
            if (block.getType() == COAL_BLOCK) {
                int range = SkillConfigManager.getUseSetting(hero, this, "max-portal-range", 50, false);
                Location location = player.getLocation();
                if (location.getWorld().equals(block.getWorld()) && location.distanceSquared(block.getLocation()) > range * range) {
                    player.sendMessage("You are too far away from the portal!");
                    return SkillResult.CANCELLED;
                }
                hero.removeEffect(voidGateEffect);
                block.setType(OBSIDIAN);
                block.setMetadata("VoidGateObsidian", new FixedMetadataValue(plugin, location));
                long duration = SkillConfigManager.getUseSetting(hero, this, "portal-duration", 10000, false);
                hero.addEffect(new VoidGateActivatedEffect(this, duration, location, block,hero));
                broadcast(block.getLocation(), "$1 activated their $2!", player.getDisplayName(), "VoidGate");
                Messaging.send(player, "You activated your $1!", "VoidGate");
            }
            return SkillResult.NORMAL;
        }
    }

    public class VoidGateActivatedEffect extends PeriodicExpirableEffect {
        private Location location;
        private Block block;

        public VoidGateActivatedEffect(Skill skill, long duration, Location location, Block block,Hero hero) {
            super(skill, "VoidGateActivated",hero.getPlayer(), 1500, duration);
            this.location = location;
            this.block = block;
            this.types.add(EffectType.FORM);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void tickHero(Hero hero) {
            location.getWorld().playEffect(location, Effect.ENDER_SIGNAL, 10);
        }

        @Override
        public void tickMonster(Monster monster) {
            //N/A
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (block.hasMetadata("VoidGateObsidian")) {
                block.removeMetadata("VoidGateObsidian", plugin);
            }
            Set<Block> blocks = new HashSet<>();
            blocks.add(block);
            Lib.removeBlocks(blocks);
            if (this.isExpired()) {
                broadcast(block.getLocation(), "$1's $2 closed.", hero.getPlayer().getDisplayName(), "VoidGate");
            }
        }
    }


    public class VoidGateEffect extends ExpirableEffect {
        private Block block;

        public VoidGateEffect(Skill skill, long duration, Block block,Hero hero) {
            super(skill, "VoidGate",hero.getPlayer(), duration);
            this.block = block;
            types.add(EffectType.MAGIC);
            types.add(EffectType.FORM);
            types.add(EffectType.DISPELLABLE);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            if (block.hasMetadata("VoidGateObsidian")) {
                block.removeMetadata("VoidGateObsidian", plugin);
            }
            Set<Block> blocks = new HashSet<>();
            blocks.add(block);
            Lib.removeBlocks(blocks);
            if (this.isExpired()) {
                broadcast(block.getLocation(), "$1 was too late to activate their $2.", hero.getPlayer().getDisplayName(), "VoidGate");
                Messaging.send(hero.getPlayer(), "You were too late with reactivating your $1!", "VoidGate");
                hero.setCooldown("VoidGate", System.currentTimeMillis() + 10000);
            }
        }

        public Block getBlock() {
            return block;
        }
    }

    public class PlayerListener implements Listener {
        @EventHandler
        public void onPlayerInteract(PlayerInteractEvent event) {
            if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
                return;
            }
            Block b = event.getClickedBlock();
            if (b.getType() != OBSIDIAN) {
                return;
            }
            if (b.hasMetadata("VoidGateObsidian")) {
                Player player = event.getPlayer();
                player.teleport((Location) b.getMetadata("VoidGateObsidian").get(0).value());
                player.playSound(player.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, 1, 1);
            }
        }
    }
}
