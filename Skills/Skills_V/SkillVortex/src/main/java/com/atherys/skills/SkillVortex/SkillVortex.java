package com.atherys.skills.SkillVortex;

import com.atherys.effects.SkillCastingEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class SkillVortex extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillVortex(Heroes plugin) {
        super(plugin, "Vortex");
        setDescription("Creates a vortex on the target block, causing enemies within $1 blocks to be teleported there after $2 seconds.");
        setUsage("/skill Vortex");
        setArgumentRange(0, 0);
        setIdentifiers("skill Vortex");
        setTypes(SkillType.FORCE, SkillType.SILENCEABLE, SkillType.TELEPORTING);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(2000));
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% begins using %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero% used %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    public SkillResult use(Hero hero, String[] strings) {
        int d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block block = hero.getPlayer().getTargetBlock((Set<Material>) null, d);
        if (block.getType() == Material.AIR) {
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        Block b = block.getRelative(BlockFace.UP, 2);
        if (b.getType().equals(Material.AIR)) {
            b.setType(Material.STAINED_GLASS);
            b.setData((byte) 3);
            values.add(b);
            b.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
        }
        long obsidDuration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 2000L, false);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3, false);
        Location loc = block.getRelative(BlockFace.UP, 1).getLocation();
        hero.addEffect(new VortexEffect(this, obsidDuration, radius, values, loc,hero));
        return SkillResult.NORMAL;
    }

    public class VortexEffect extends SkillCastingEffect {
        private final double radius;
        private Set<Block> values;
        private final Location loc;

        public VortexEffect(Skill skill, long duration, double radius, Set<Block> values, Location loc,Hero hero) {
            super(skill, duration, false, true,hero);
            this.radius = radius;
            this.values = values;
            this.loc = loc;
            this.types.add(EffectType.FORM);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Lib.removeBlocks(values);
            if (this.isExpired()) {
                double v1 = SkillConfigManager.getUseSetting(hero, skill, "horizontal-vector", 0.2, false);
                double v2 = SkillConfigManager.getUseSetting(hero, skill, "vertical-vector", 1, false);

                for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                    if (e instanceof Player) {
                        Player target = (Player)e;
                        if (damageCheck((LivingEntity) player, target)) {
                            addSpellTarget(target, hero);
                            double cc = target.getLocation().distance(loc) * v1;
                            Vector v = loc.toVector().subtract(target.getLocation().toVector()).normalize();
                            v = v.multiply(cc);
                            v.setY(v2);
                            target.setVelocity(v);
                        }
                    }
                }
            }
        }
    }

    public String getDescription(Hero hero) {
        long obsidDuration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 2000L, false);
        double radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 3, false);
        return getDescription().replace("$1", radius + "").replace("$2", (double) (obsidDuration / 1000) + " ") + Lib.getSkillCostStats(hero, this);
    }
}
