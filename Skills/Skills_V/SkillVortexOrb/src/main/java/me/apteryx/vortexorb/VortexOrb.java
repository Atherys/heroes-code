package me.apteryx.vortexorb;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import me.apteryx.vortexorb.events.EnderPearlPort;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author apteryx
 * @time 8:00 PM
 * @since 10/24/2016
 */
public class VortexOrb extends ActiveSkill {

    private static VortexOrb vortexOrb;
    private double damage, radius, horizontalVel, verticalVel;
    private int slowMutl, slowDuration;

    public VortexOrb(Heroes heroes) {
        super(heroes, "VortexOrb");
        this.setIdentifiers("skill vortexorb");
        this.setUsage("/skill vortexorb");
        this.setArgumentRange(0, 0);
        this.setTypes(SkillType.MOVEMENT_SLOWING, SkillType.VELOCITY_INCREASING, SkillType.SILENCEABLE);
        this.setDescription("Launch an orb that deals %damage% magic damage and pulls enemies within %radius% blocks of the impact toward the impact and slows them for %slow-duration% seconds.");
        if (vortexOrb == null) {
            vortexOrb = this;
        }
        Bukkit.getPluginManager().registerEvents(new EnderPearlPort(), this.plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        damage = SkillConfigManager.getUseSetting(hero, this, "damage",3.0D, false);
        radius = SkillConfigManager.getUseSetting(hero, this, "radius", 3.0D, false);
        horizontalVel = SkillConfigManager.getUseSetting(hero, this, "horizontal-knockback", 1.5D, false);
        verticalVel = SkillConfigManager.getUseSetting(hero, this, "vertical-knockback", 0.8D, false);
        slowMutl = SkillConfigManager.getUseSetting(hero, this, "slow-muliplier", 3, false);
        slowDuration = SkillConfigManager.getUseSetting(hero, this, "slow-duration", 1500, false);
        EnderPearl enderPearl = hero.getPlayer().launchProjectile(EnderPearl.class);
        enderPearl.setMetadata("VortexOrb", new FixedMetadataValue(this.plugin, "Apteryx"));
        this.broadcastExecuteText(hero);
        enderPearl.setCustomName(hero.getName());
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return super.getDescription().replace("%damage%", String.valueOf(SkillConfigManager.getUseSetting(hero, this, "damage",3.0D, false))).replace("%radius%", String.valueOf(SkillConfigManager.getUseSetting(hero, this, "radius", 3.0D, false))).replace("%slow-duration%", String.valueOf(SkillConfigManager.getUseSetting(hero, this, "slow-duration", 1500, false) / 1000));
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("radius", 3.0D);
        node.set("damage", 3.0D);
        node.set("horizontal-pull", 2.0D);
        node.set("vertical-pull", 1.5D);
        node.set("slow-multiplier", 3);
        node.set("slow-duration", 1500);

        return node;
    }

    public int getSlowMutl() {
        return slowMutl;
    }

    public int getSlowDuration() {
        return slowDuration;
    }

    public double getDamage() {
        return damage;
    }

    public double getRadius() {
        return radius;
    }

    public double getHorizontalVel() {
        return horizontalVel;
    }

    public double getVerticalVel() {
        return verticalVel;
    }

    public static VortexOrb getInstance() {
        return vortexOrb;
    }

}
