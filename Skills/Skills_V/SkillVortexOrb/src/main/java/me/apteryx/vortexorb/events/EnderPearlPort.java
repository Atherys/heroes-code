package me.apteryx.vortexorb.events;

import me.apteryx.vortexorb.VortexOrb;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

/**
 * @author apteryx
 * @time 9:04 PM
 * @since 10/24/2016
 */
public class EnderPearlPort implements Listener {

    @EventHandler
    public void onPearlHit(ProjectileHitEvent event) {
        if (event.getEntity().hasMetadata("VortexOrb")) {
            //Bukkit.getLogger().info("EnderPearl is Projectile");
            event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 1.5F, 1.5F);
            event.getEntity().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, event.getEntity().getLocation(), 2);
            event.getEntity().getNearbyEntities(VortexOrb.getInstance().getRadius(), VortexOrb.getInstance().getRadius(), VortexOrb.getInstance().getRadius()).stream().filter(entity -> entity instanceof LivingEntity).forEach(entity -> {
                if (!entity.getName().equalsIgnoreCase(event.getEntity().getCustomName())) {
                    VortexOrb.getInstance().damageEntity((LivingEntity) entity, Bukkit.getPlayer(event.getEntity().getCustomName()), VortexOrb.getInstance().getDamage(), EntityDamageEvent.DamageCause.MAGIC, false);
                    ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, VortexOrb.getInstance().getSlowDuration() / 50, VortexOrb.getInstance().getSlowMutl()));
                    Vector velocity = event.getEntity().getLocation().toVector().subtract(entity.getLocation().toVector()).normalize();
                    velocity = velocity.multiply(VortexOrb.getInstance().getVerticalVel());
                    velocity.setY(VortexOrb.getInstance().getVerticalVel());
                    entity.setVelocity(velocity);
                }

            });
        }
    }


}
