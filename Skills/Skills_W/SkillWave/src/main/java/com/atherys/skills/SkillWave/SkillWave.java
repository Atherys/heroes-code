package com.atherys.skills.SkillWave;

import com.atherys.effects.WaveStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.List;

public class SkillWave extends ActiveSkill {

    public SkillWave(Heroes plugin) {
        super(plugin, "Wave");
        setDescription("Heal yourself and if you're in a party you throw a projectile which will heal allies near the landing zone. Gives you 1 Wave stack if you heal at least one ally.");
        setUsage("/skill Wave");
        setArgumentRange(0, 0);
        setIdentifiers("skill Wave");
        setTypes(SkillType.SILENCEABLE, SkillType.HEALING);
        Bukkit.getPluginManager().registerEvents(new SkillWaveListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 2);
        node.set(SkillSetting.DAMAGE.node(), 25);
        node.set("self-heal", 10);
        node.set("base-heal", 20);
        node.set("heal-per-ally", 10);
        node.set("wave-stacks", 1);
        node.set(SkillSetting.DURATION.node(), 8000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double heal = SkillConfigManager.getUseSetting(hero, this, "self-heal", 10, false);
        Lib.healHero(hero, heal, this, hero);
        Snowball Wave = player.launchProjectile(Snowball.class);
        Wave.setMetadata("WaveSnowball", new FixedMetadataValue(plugin, hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SkillWaveListener implements Listener {

        private final Skill skill;

        public SkillWaveListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
            if (!(event.getDamager() instanceof Snowball) || (!event.getDamager().hasMetadata("WaveSnowball"))) {
                return;
            }
            event.setCancelled(true);
            if ((event.getEntity() instanceof Player) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            Hero hero = (Hero) event.getDamager().getMetadata("WaveSnowball").get(0).value();
            try {
                if (hero.getPlayer() != null) {
                    double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 25, false);
                    damageEntity((LivingEntity) event.getEntity(), hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                    addSpellTarget(event.getEntity(), hero);
                }
                event.setCancelled(true);
            }catch (Exception e){
                //placeholder for error catching
            }
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
        public void onWaveLand(ProjectileHitEvent event) {
            Entity snowBall = event.getEntity();
            if (!(snowBall instanceof Snowball) || !snowBall.hasMetadata("WaveSnowball")) {
                return;
            }
            Hero hero = (Hero) snowBall.getMetadata("WaveSnowball").get(0).value();
            if (hero.getPlayer() == null) {
                return;
            }
            if (hero.hasParty()) {
                int radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 2, false);
                double heal = SkillConfigManager.getUseSetting(hero, skill, "base-heal", 20, false);
                List<Player> hit = new ArrayList<>();
                double healPerHit = SkillConfigManager.getUseSetting(hero, skill, "heal-per-ally", 10, false);
                for (Entity e : snowBall.getNearbyEntities(radius, radius, radius)) {
                    if (e instanceof Player) {
                        Player tplayer = (Player) e;
                        if (hero.getParty().isPartyMember(tplayer) && !(hero.getPlayer().equals(tplayer))) {
                            hit.add(tplayer);
                            heal += healPerHit;
                        }
                    }
                }
                if (!hit.isEmpty()) {
                    int stacks = SkillConfigManager.getUseSetting(hero, skill, "wave-stacks", 1, false);
                    WaveStackEffect.addWaveStacks(skill, hero, stacks);
                    int newStacks = ((WaveStackEffect) hero.getEffect("WaveStack")).getStacks();
                    Messaging.send(hero.getPlayer(), "You now have " + newStacks + (newStacks == 1 ? " wave stack." : " wave stacks."));
                    for (Player target : hit) {
                        Hero tHero = plugin.getCharacterManager().getHero(target);
                        Lib.healHero(tHero, heal / hit.size(), skill, hero);
                    }
                }
            }
            snowBall.getWorld().playSound(snowBall.getLocation(), Sound.ENTITY_BOBBER_SPLASH, 0.5F, 0.6F);
            snowBall.remove();
        }
    }
}