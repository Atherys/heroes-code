package com.atherys.skills.SkillWeb;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillWeb extends ActiveSkill {

    public SkillWeb(Heroes plugin) {
        super(plugin, "Web");
        setDescription("You conjure a web around the target location");
        setUsage("/skill web");
        setArgumentRange(0, 0);
        setIdentifiers("skill web");
        setTypes(SkillType.ABILITY_PROPERTY_EARTH, SkillType.SILENCEABLE, SkillType.DAMAGING);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block block = player.getTargetBlock(Util.transparentBlocks, max);
        if (block.getType() == Material.AIR) {
            Messaging.send(player, "You must target a block!");
            return SkillResult.CANCELLED;
        }
        Set<Block> values = new HashSet<>();
        for (int x = -2; x < 3; x++) {
            for (int z = -2; z < 3; z++) {
                if ((x == 0) || (z==0) || ((x==1||x==-1)&&(z==1||z==-1))) {
                    if (block.getRelative(x, 1, z).getType() == Material.AIR) {
                        Block bl = block.getRelative(x, 1, z);
                        values.add(bl);
                        bl.setType(Material.WEB);
                        bl.setMetadata("HeroesUnbreakableBlock", new FixedMetadataValue(plugin, true));
                    }
                }
            }
        }
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        WebEffect wEffect = new WebEffect(this, duration, values,hero);
        hero.addEffect(wEffect);
        broadcast(hero.getPlayer().getLocation(), "$1 used $2!", player.getDisplayName(), "Web");
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class WebEffect extends ExpirableEffect {
        private Set<Block> blocks;

        public WebEffect(Skill skill, long duration, Set<Block> values,Hero hero) {
            super(skill, "Web",hero.getPlayer(), duration);
            this.blocks = values;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.FORM);
            types.add(EffectType.BENEFICIAL);
            blocks = values;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Lib.removeBlocks(blocks);
            broadcast(player.getLocation(), "$1's $2 expired.", player.getDisplayName(), "Web");
        }
    }
}