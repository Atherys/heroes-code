package com.atherys.skills.SkillWildGrowth;

import com.atherys.effects.RootEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillWildGrowth extends ActiveSkill {
    public SkillWildGrowth(Heroes plugin) {
        super(plugin, "WildGrowth");
        setDescription("AoE root effect. Bloom: AoE heal over time and bonemeal effect, radius $1");
        setUsage("/skill WildGrowth");
        setArgumentRange(0, 0);
        setIdentifiers("skill wildgrowth");
        setNotes("Note: Using this skill also puts Shell and Overgrowth on a 10 second cooldown.");
        setTypes(SkillType.SILENCEABLE, SkillType.DAMAGING, SkillType.KNOWLEDGE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("tick-heal-amount", Integer.valueOf(10));
        node.set("heal-duration", 5000);
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set("chance-brown-mushroom", Double.valueOf(0.5D));
        node.set("chance-red-mushroom", Double.valueOf(0.5D));
        node.set("root-duration", 5000);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        String description = getDescription().replace("$1", radius + "");
        return description;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        if (!hero.hasEffect("BloomEffect")) {
            long duration = SkillConfigManager.getUseSetting(hero, this, "root-duration", 5000, false);
            for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                if (!(e instanceof Player)) {
                    continue;
                }
                if (damageCheck((org.bukkit.entity.LivingEntity) e, hero.getPlayer())) {
                    Hero tHero = plugin.getCharacterManager().getHero((Player) e);
                    tHero.addEffect(new RootEffect(this, duration,tHero));
                    Lib.cancelDelayedSkill(tHero);
                }
            }
        } else {
            double heal = SkillConfigManager.getUseSetting(hero, this, "tick-heal-amount", 10, false);
            int healDuration = SkillConfigManager.getUseSetting(hero, this, "heal-duration", 5000, false);
            if (hero.hasParty()) {
                for (Hero phero : hero.getParty().getMembers()) {
                    if (player.getWorld().equals(phero.getPlayer().getWorld()) && phero.getPlayer().getLocation().distanceSquared(player.getLocation()) <= radius * radius) {
                        WildGrowthHealEffect periodicHealEffect = new WildGrowthHealEffect(this, 1000, healDuration, heal, hero);
                        phero.addEffect(periodicHealEffect);
                    }
                }
            } else {
                WildGrowthHealEffect periodicHealEffect = new WildGrowthHealEffect(this, 1000, healDuration, heal, hero);
                hero.addEffect(periodicHealEffect);
            }
        }
        long time = System.currentTimeMillis();
        if (hero.getCooldown("Overgrowth") == null || (hero.getCooldown("Overgrowth") - time) < 10000) {
            hero.setCooldown("Overgrowth", time + 10000);
        }
        if (hero.getCooldown("Shell") == null || (hero.getCooldown("Shell") - time) < 10000) {
            hero.setCooldown("Shell", time + 10000);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class WildGrowthHealEffect extends PeriodicHealEffect {
        public WildGrowthHealEffect(Skill skill, long period, long duration, double heal, Hero caster) {
            super(skill, "WildGrowthHeal",caster.getPlayer(), period, duration, heal );
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.HEALING);
            this.types.add(EffectType.MAGIC);
        }
    }
}
