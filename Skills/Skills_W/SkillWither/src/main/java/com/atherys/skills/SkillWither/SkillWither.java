package com.atherys.skills.SkillWither;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SkillWither extends TargettedSkill {
    public SkillWither(Heroes plugin) {
        super(plugin, "Wither");
        setDescription("Withers the target's legs and makes them slow for $1 seconds.");
        setUsage("/skill Wither");
        setArgumentRange(0, 0);
        setIdentifiers("skill Wither");
        setTypes(SkillType.MOVEMENT_INCREASING, SkillType.SILENCEABLE, SkillType.DAMAGING);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("slow-mult", 3);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if ( target != null && target instanceof Player ) {
            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
            int m = SkillConfigManager.getUseSetting(hero, this, "slow-mult", 3, false);
            plugin.getCharacterManager().getCharacter(target).addEffect(new SlowEffect(this, duration, m, (Hero) target));
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
        return SkillResult.CANCELLED;
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class SlowEffect extends ExpirableEffect {
        public SlowEffect(Skill skill, long duration, int m,Hero hero) {
            super(skill, "Slow",hero.getPlayer(), duration);
            types.add(EffectType.SLOW);
            types.add(EffectType.WITHER);
            types.add(EffectType.HARMFUL);
            types.add(EffectType.DISPELLABLE);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(2, tickDuration, m, false);
            addMobEffect(8, tickDuration, -m, false);
            addPotionEffect(new PotionEffect(PotionEffectType.WITHER, tickDuration, 1), true);
        }
    }
}