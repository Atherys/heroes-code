package com.atherys.skills.SkillWound;

import com.atherys.effects.BleedPeriodicDamageEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;

public class SkillWound extends TargettedSkill {

    public SkillWound(Heroes plugin) {
        super(plugin, "Wound");
        setDescription("$2 damage and disables target's heals for $1s.");
        setUsage("/skill Wound");
        setArgumentRange(0, 0);
        setIdentifiers("skill wound");
        Bukkit.getServer().getPluginManager().registerEvents(new SkillWoundListener(), plugin);

        setTypes(SkillType.ABILITY_PROPERTY_PHYSICAL, SkillType.DEBUFFING, SkillType.DAMAGING, SkillType.SILENCEABLE, SkillType.DEBUFFING);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription();
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("mortalwound-duration", 10000);
        node.set("bleed-duration", 5000);
        node.set(SkillSetting.PERIOD.node(), 2000);
        node.set(SkillSetting.DAMAGE_TICK.node(), 1);
        node.set(SkillSetting.MAX_DISTANCE.node(), 7);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player)) {
            return SkillResult.FAIL;
        }

        if (!damageCheck(player, target)) {
            return SkillResult.INVALID_TARGET;
        }

        long mortalwoundDuration = SkillConfigManager.getUseSetting(hero, this, "mortalwound-duration", 10000, false);
        long bleedDuration = SkillConfigManager.getUseSetting(hero, this, "bleed-duration", 5000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, true);
        double tickDamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 1, false);

        CharacterTemplate characterTemplate = plugin.getCharacterManager().getCharacter(target);
        characterTemplate.addEffect(new WoundEffect(this, mortalwoundDuration,hero));
        characterTemplate.addEffect(new BleedPeriodicDamageEffect(this, "WoundBleed", player, period, bleedDuration, tickDamage, false));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class WoundEffect extends ExpirableEffect {

        public WoundEffect(Skill skill, long duration,Hero hero) {
            super(skill, "Wound",hero.getPlayer(), duration);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.WITHER);
	        this.types.add(EffectType.BLEED);
	        this.types.add(EffectType.POISON);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(20, tickDuration, 1, false);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 has been wounded!", player.getDisplayName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 is no longer wounded!", player.getDisplayName());
        }
    }

    public class SkillWoundListener implements Listener {

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onEntityRegainHeroHealth(HeroRegainHealthEvent event) {
            Hero hero = event.getHero();
            if (hero.hasEffect("Wound")) {
                event.setDelta(0D);
                broadcast(hero.getPlayer().getLocation(), "$1's heal was blocked!", hero.getPlayer().getDisplayName());
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onEntityRegainRegularHealth(EntityRegainHealthEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player)event.getEntity());
            if (hero.hasEffect("Wound")) {
                event.setAmount(0D);
                broadcast(hero.getPlayer().getLocation(), "$1's heal was blocked!", hero.getPlayer().getDisplayName());
            }
        }
    }
}