package com.atherys.skills.SkillWrath;

import com.atherys.effects.MightEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillWrath extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillWrath(Heroes plugin) {
        super(plugin, "Wrath");
        setDescription("You temporarily increase your Strength and Swiftness.");
        setUsage("/skill Wrath");
        setArgumentRange(0, 0);
        setIdentifiers("skill Wrath");
        setTypes(SkillType.FORCE, SkillType.BUFFING, SkillType.MOVEMENT_INCREASING, SkillType.SILENCEABLE);
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "Your muscles bulge with power!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You feel strength leave your body!");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 8000);
        node.set("bonus-damage", 2);
        node.set("speed-amplifier", 2);
        node.set(SkillSetting.APPLY_TEXT.node(), "Your muscles bulge with power!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You feel strength leave your body!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 8000, false);
        double bonusDamage = SkillConfigManager.getUseSetting(hero, this, "bonus-damage", 2, false);
        int speedAmplifier = SkillConfigManager.getUseSetting(hero, this, "speed-amplifier", 2, false);
        MightEffect mightEffect = new MightEffect(this, duration, bonusDamage, applyText, expireText,hero);
        QuickenEffect quickenEffect = new QuickenEffect(this, "WrathSpeed", duration, --speedAmplifier, "", "",hero);
        hero.addEffect(mightEffect);
        hero.addEffect(quickenEffect);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class QuickenEffect extends ExpirableEffect {
        private final String applyText;
        private final String expireText;

        public QuickenEffect(Skill skill, String name, long duration, int amplifier, String applyText, String expireText,Hero hero) {
            super(skill, name,hero.getPlayer(),duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.SPEED);
            this.addMobEffect(1, (int) (duration / 1000L) * 20, amplifier, false);
            this.applyText = applyText;
            this.expireText = expireText;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            this.broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            this.broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
    }
}