package com.atherys.skills.SkillZeal;

import com.atherys.effects.ZealEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillZeal extends TargettedSkill {
    private String ApplyText;
    private String ExpireText;

    public SkillZeal(Heroes plugin) {
        super(plugin, "Zeal");
        setDescription("Imposes your holy zeal upon the targeted party member to increase their damage by $1% for $2 seconds");
        setUsage("/skill Zeal");
        setArgumentRange(0, 0);
        setIdentifiers("skill zeal");
        setTypes(SkillType.SILENCEABLE, SkillType.BUFFING);
        Bukkit.getServer().getPluginManager().registerEvents(new HeroesDamageListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.10D, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        return getDescription().replace("$1", "" + amount * 100).replace("$2", duration / 1000 + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(0.1D));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(20000));
        node.set(SkillSetting.APPLY_TEXT.node(), "You feel stronger");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "Zeal effect expired");
        return node;
    }

    @Override
    public void init() {
        super.init();
        ApplyText = ChatColor.GRAY + SkillConfigManager.getUseSetting(null, this, SkillSetting.APPLY_TEXT.node(), "You feel stronger").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
        ExpireText = ChatColor.GRAY + SkillConfigManager.getUseSetting(null, this, SkillSetting.EXPIRE_TEXT.node(), "Zeal effect expired").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Player tplayer = (Player) target;
        if (!hero.hasParty() || !hero.getParty().isPartyMember(tplayer)) {
            return SkillResult.INVALID_TARGET;
        }
        double mult = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.1D, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000L, false);
        plugin.getCharacterManager().getHero(tplayer).addEffect(new ZealEffect(this, duration, mult, ApplyText, ExpireText,hero));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class HeroesDamageListener implements Listener {
        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.isCancelled()) {
                return;
            }
            if ((event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) || (event.getDamage() == 0)) {
                return;
            }
            CharacterTemplate player = event.getDamager();
	        if (player.hasEffect("Inflame")){
		        return;
	        }
            if (!player.hasEffect("Zeal")) {
                return;
            }
            double mult = ((ZealEffect) player.getEffect("Zeal")).getMultiplier();
            event.setDamage(event.getDamage() * (1 + mult));
        }
    }
}