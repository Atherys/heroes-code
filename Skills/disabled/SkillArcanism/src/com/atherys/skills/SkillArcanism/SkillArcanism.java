package com.atherys.skills.SkillArcanism;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillArcanism extends PassiveSkill {
    private Skill Arcanism;

    public SkillArcanism(Heroes plugin) {
        super(plugin, "Arcanism");
        setDescription("Adjusts the Mage's resistances appropriately.");
        setTypes(SkillType.COUNTER, SkillType.BUFF);
        setEffectTypes(EffectType.BENEFICIAL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("magic-damage-reduction", 0.25);
        node.set("physical-damage-reduction", 0.25);
        return node;
    }

    @Override
    public void init() {
        super.init();
        Arcanism = this;
    }

    public class SkillHeroListener implements Listener {
        @EventHandler(ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (!(event.getEntity() instanceof Player) || event.getDamage() < 1) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!hero.hasEffect("Arcanism")) {
                return;
            }
            if (event.getCause() == DamageCause.MAGIC) {
                double amount = SkillConfigManager.getUseSetting(hero, Arcanism, "magic-damage-reduction", 0.25, false);
                event.setDamage((int) (event.getDamage() * (1 - amount)));
            }
            if (event.getCause() == DamageCause.ENTITY_ATTACK) {
                double amount = SkillConfigManager.getUseSetting(hero, Arcanism, "physical-damage-reduction", 0.25, false);
                event.setDamage((int) (event.getDamage() * (1 - amount)));
            }
        }
    }
}
