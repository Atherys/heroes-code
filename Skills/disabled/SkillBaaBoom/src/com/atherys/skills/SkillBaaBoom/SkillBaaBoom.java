package com.atherys.skills.SkillBaaBoom;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Sheep;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import java.util.Set;

public class SkillBaaBoom extends ActiveSkill {
    public SkillBaaBoom(Heroes plugin) {
        super(plugin, "BaaBoom");
        setDescription("Active\nSpawns a sheep that explodes in $1s dealing $2 physical damage");
        setUsage("/skill BaaBoom");
        setArgumentRange(0, 0);
        setIdentifiers("skill BaaBoom");
        setTypes(SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.SUMMON);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 20, false);
        Location loc = hero.getPlayer().getTargetBlock((Set<org.bukkit.Material>) null, distance).getRelative(BlockFace.UP).getLocation();
        Sheep sheep = (Sheep) hero.getPlayer().getWorld().spawnEntity(loc, EntityType.SHEEP);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10D, false);
        plugin.getCharacterManager().getMonster(sheep).addEffect(new SuicideEffect(this, duration, radius, hero, damage));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10D, false);
        String description = getDescription().replace("$1", duration / 1000 + "").replace("$2", damage + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        node.set(SkillSetting.DAMAGE.node(), 10D);
        return node;
    }

    public class SuicideEffect extends ExpirableEffect {
        private final int radius;
        private final Hero caster;
        private final double damage;

        public SuicideEffect(Skill skill, long duration, int radius, Hero caster, double damage) {
            super(skill, "SuicideEffect", duration);
            this.radius = radius;
            this.caster = caster;
            this.damage = damage;
        }

        @Override
        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
            monster.getEntity().getWorld().createExplosion(monster.getEntity().getLocation(), 0.0F, false);
            for (Entity e : monster.getEntity().getNearbyEntities(radius, radius, radius)) {
                if (e instanceof LivingEntity) {
                    if (damageCheck(caster.getPlayer(), (LivingEntity) e)) {
                        damageEntity((LivingEntity) e, caster.getPlayer(), damage, DamageCause.ENTITY_EXPLOSION);
                        addSpellTarget(e, caster);
                    }
                }
            }
            monster.getEntity().damage(1000);
        }
    }
}