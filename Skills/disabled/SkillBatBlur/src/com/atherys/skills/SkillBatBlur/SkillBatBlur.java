package com.atherys.skills.SkillBatBlur;

import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import java.util.Set;

public class SkillBatBlur extends ActiveSkill {
    public SkillBatBlur(Heroes plugin) {
        super(plugin, "BatBlur");
        setDescription("Active\nSummons a swarm of bats.");
        setUsage("/skill BatBlur");
        setArgumentRange(0, 0);
        setIdentifiers("skill BatBlur");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("bats", 5);
        node.set("life-duration", 3000);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int n = SkillConfigManager.getUseSetting(hero, this, "bats", 6, false);
        long tick = (long) (SkillConfigManager.getUseSetting(hero, this, "life-duration", 3000L, false));
        Location loc = hero.getPlayer().getTargetBlock((Set<Material>) null, 3).getLocation();
        LivingEntity[] bats = new LivingEntity[n];
        for (int i = 0; i < n; i++) {
            bats[i] = (LivingEntity) hero.getPlayer().getWorld().spawnEntity(loc, EntityType.BAT);
            plugin.getCharacterManager().getMonster(bats[i]).setExperience(0);
            plugin.getCharacterManager().getMonster(bats[i]).setMaxHealth(1000.0D);
        }
        hero.addEffect(new BatEffect(this, tick, bats));
        broadcastExecuteText(hero);
        // Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new
        // removebats(bats, n), tick);
        return SkillResult.NORMAL;
    }

    public class BatEffect extends PeriodicExpirableEffect {
        private LivingEntity[] bats;

        public BatEffect(Skill skill, long duration, LivingEntity[] bats) {
            super(skill, "Bats", 1000L, duration);
            this.bats = bats;
        }

        @Override
        public void tickHero(Hero hero) {
            for (LivingEntity e : bats) {
                if (!e.isDead()) {
                    e.teleport(hero.getPlayer());
                }
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            for (LivingEntity e : bats) {
                e.remove();
            }
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    /*
     * public class removebats implements Runnable{ private int n; private
     * LivingEntity[] bats = new LivingEntity[n]; private
     * removebats(LivingEntity[] bats, int n) { this.n = n; this.bats = bats; }
     *
     * @Override public void run() { for (LivingEntity e : bats) e.damage(1000);
     * }
     *
     * }
     */
    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }
}
