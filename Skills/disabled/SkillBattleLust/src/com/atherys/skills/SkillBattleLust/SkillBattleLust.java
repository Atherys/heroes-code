package com.atherys.skills.SkillBattleLust;

/**
 * Created by Arthur on 5/15/2015.
 */

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
public class SkillBattleLust extends PassiveSkill {
    private String applyText, expireText;
    public SkillBattleLust(Heroes plugin) {
        super(plugin, "BattleLust");
        setDescription("Passive\nEach kill increases your physical attack damage. Sheds after one minute.");
        setUsage("/skill BattleLust");
        setArgumentRange(0, 0);
        setIdentifiers("skill BattleLust");
        Bukkit.getServer().getPluginManager().registerEvents(new KillListener(), plugin);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("max-stacks", 10);
        node.set("bonus-damage-per-kill", 2);
        node.set("max-damage", 0);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% gained %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero% lost %skill%.");
        return node;
    }
    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% gained %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero% lost %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
    }
    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
    @Override
    public void apply(Hero hero) {
        int max = SkillConfigManager.getUseSetting(hero, this, "max-stacks", 10, false);
        double dperkill = SkillConfigManager.getUseSetting(hero, this, "bonus-damage-per-kill", 2, false);
        double maxd = SkillConfigManager.getUseSetting(hero, this, "max-damage", 0, false);
        BattleLustEffect a = new BattleLustEffect(this,max,dperkill,maxd);
        a.setPersistent(true);
        hero.addEffect(a);
    }
    private class Decrase implements Runnable {
        private String name;
        public Decrase(String name) {
            this.name = name;
        }
        @Override
        public void run() {
            Player p = Bukkit.getPlayer(name);
            if (p == null)
                return;
            Hero hero = plugin.getCharacterManager().getHero(p);
            if (hero.hasEffect("BattleLust")) {
                BattleLustEffect b = (BattleLustEffect) hero.getEffect("BattleLust");
                b.shedKill();
            }
        }
    }
    public class BattleLustEffect extends Effect {
        private final double dperkill, maxd;
        private final int max;
        private int kills = 0;
        private Hero hero;
        public BattleLustEffect(Skill skill, int max, double dperkill, double maxd) {
            super(skill, "BattleLust");
            this.max = max;
            this.maxd = maxd;
            this.dperkill = dperkill;
        }
        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            this.hero = hero;
            broadcast(hero.getPlayer().getLocation(), applyText, hero.getName(), getName());
        }
        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expireText, hero.getName(), getName());
        }
        public int getMaxStacks() {
            return max;
        }
        public double getBonusDamagePerKill() {
            return dperkill;
        }
        public double getMaxDamage() {
            return maxd;
        }
        public int getKills() {
            return kills;
        }
        public void addKill() {
            if (!(getKills() + 1 > getMaxStacks())) {
                kills++;
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Decrase(hero.getName()), 1200);
            }
        }
        public void shedKill() {
            if (getKills() - 1 >= 0)
                kills--;
        }
    }
    public class KillListener implements Listener {
        @EventHandler(priority = EventPriority.HIGH)
        public void onKill(EntityDeathEvent event) {
            LivingEntity e = event.getEntity();
            if (!(e.getKiller() instanceof Player)) {
                return;
            }
            Hero h = plugin.getCharacterManager().getHero(e.getKiller());
            if (h.hasEffect("BattleLust")) {
                BattleLustEffect b = (BattleLustEffect) h.getEffect("BattleLust");
                if (b.getKills() + 1 <= b.getMaxStacks()) {
                    b.addKill();
                }
            }
        }
        @EventHandler(priority = EventPriority.NORMAL)
        public void onDamage(WeaponDamageEvent event) {
            if (event.isCancelled()) {
                return;
            }
            CharacterTemplate c = event.getDamager();
            if (c.hasEffect("BattleLust")) {
                BattleLustEffect b = (BattleLustEffect) c.getEffect("BattleLust");
                double damage = (event.getDamage() + (b.getBonusDamagePerKill() * b.getKills()));
                if ((b.getMaxDamage() != 0) && (damage > b.getMaxDamage())) {
                    damage = b.getMaxDamage();
                }
                if (damage != 0) {
                    event.setDamage(damage);
                }
            }
        }
    }
}
