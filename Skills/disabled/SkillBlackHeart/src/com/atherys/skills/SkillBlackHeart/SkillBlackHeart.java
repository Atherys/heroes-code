package com.atherys.skills.SkillBlackHeart;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillBlackHeart extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillBlackHeart(Heroes plugin) {
        super(plugin, "BlackHeart");
        setArgumentRange(0, 0);
        setDescription("Active\nTarget is afflicted with blackheart for $1s, taking $2 magic damage over $3s, spreads to a nearby enemies every $4s");
        setIdentifiers("skill blackheart");
        setUsage("/skill BlackHeart");
        setTypes(SkillType.DAMAGING, SkillType.DARK, SkillType.HARMFUL, SkillType.SILENCABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) / 1000;
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false) / 1000;
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 5, false);
        String description = getDescription().replace("$1", duration + "");
        description = description.replace("$2", damage * (duration / period) + "");
        description = description.replace("$3", duration / 1000 + "");
        description = description.replace("$4", period / 1000 + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(6000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(2000));
        node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(5));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set("duration-on-spread", Integer.valueOf(4000));
        node.set(SkillSetting.APPLY_TEXT.node(), "$1 is infected with Blackheart!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "$1 is no longer infected with Blackheart!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 is infected with Blackheart!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 is no longer infected with Blackheart!");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player) || !(target instanceof Player) || !(damageCheck(hero.getPlayer(), target)))
            return SkillResult.INVALID_TARGET;
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 5, false);
        long durations = SkillConfigManager.getUseSetting(hero, this, "duration-on-spread", 4000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        WitherEffect effect = new WitherEffect(this, durations, damage, period, applyText, expireText, player);
        targetHero.addEffect(new BlackHeartEffect(this, duration, damage, period, effect, radius, applyText, expireText, hero));
        return SkillResult.NORMAL;
    }

    public class BlackHeartEffect extends PeriodicDamageEffect {
        private final String applyText;
        private final String expireText;
        private final int radius;
        private final WitherEffect effect;
        private final Hero caster;

        public BlackHeartEffect(Skill skill, long duration, double damage, long period, WitherEffect effect, int radius, String applyText, String expireText, Hero caster) {
            super(skill, "BlackHeartEffect", period, duration, damage, caster.getPlayer());
            this.applyText = applyText;
            this.expireText = expireText;
            this.radius = radius;
            this.effect = effect;
            this.caster = caster;
            this.types.add(EffectType.DARK);
            this.types.add(EffectType.DISEASE);
            this.types.add(EffectType.WITHER);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(9, tickDuration, 240, false);
            addMobEffect(20, tickDuration, 0, false);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }

        public void tickHero(Hero hero) {
            super.tickHero(hero);
            for (Entity e : hero.getPlayer().getNearbyEntities(this.radius, this.radius, this.radius)) {
                if (((e instanceof LivingEntity) && damageCheck(this.caster.getPlayer(), (LivingEntity) e))) {
                    CharacterTemplate ent = this.plugin.getCharacterManager().getCharacter((LivingEntity) e);
                    if (ent.hasEffect("BlackHeart")) {
                        continue;
                    }
                    ent.addEffect(this.effect);
                    this.skill.addSpellTarget(e, caster);
                    return;
                }
            }
        }

        public void tickMonster(Monster mob) {
            super.tickMonster(mob);
            for (Entity e : (mob.getEntity().getNearbyEntities(this.radius, this.radius, this.radius))) {
                if (((e instanceof LivingEntity) && damageCheck(this.caster.getPlayer(), (LivingEntity) e))) {
                    this.plugin.getCharacterManager().getCharacter((LivingEntity) e).addEffect(this.effect);
                    return;
                }
            }
        }
    }

    public class WitherEffect extends PeriodicDamageEffect {
        private final String applyText;
        private final String expireText;

        public WitherEffect(Skill skill, long duration, double damage, long period, String applyText, String expireText, Player caster) {
            super(skill, "BlackHeart", period, duration, damage, caster);
            this.applyText = applyText;
            this.expireText = expireText;
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.BLEED);
            this.types.add(EffectType.WITHER);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(20, tickDuration, 20, false);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
    }
}
