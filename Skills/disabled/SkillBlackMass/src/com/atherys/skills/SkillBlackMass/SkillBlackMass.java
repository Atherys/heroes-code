package com.atherys.skills.SkillBlackMass;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillBlackMass extends ActiveSkill {
    private String applytext, expiretext;

    public SkillBlackMass(Heroes plugin) {
        super(plugin, "BlackMass");
        setDescription("Toggle\nCaster has Slow I, returns a percent of damage dealt to it, has increased physical damage, and takes $2% less damage from attacks.");
        setUsage("/skill BlackMass");
        setArgumentRange(0, 0);
        setIdentifiers("skill BlackMass");
        setTypes(SkillType.BUFF, SkillType.SILENCABLE);
        Bukkit.getPluginManager().registerEvents(new DdmgListener(), plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("BlackMass")) {
            hero.removeEffect(hero.getEffect("BlackMass"));
            return SkillResult.NORMAL;
        }
        int slowmult = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 1, false);
        double dret = SkillConfigManager.getUseSetting(hero, this, "damage-returned", 0.25, false);
        double dred = SkillConfigManager.getUseSetting(hero, this, "damage-reduced", 0.25, false);
        double db = SkillConfigManager.getUseSetting(hero, this, "damage-addition", 3, false);
        double mr = SkillConfigManager.getUseSetting(hero, this, "magic-resist-multiplier", 0.25, false);
        hero.addEffect(new BlackMassEffect(this, slowmult, dret, dred, db, mr));
        return SkillResult.NORMAL;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%.").replace("%hero%", "$2").replace("%skill%", "$1");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%skill% from %hero% expired.").replace("%hero%", "$2").replace("%skill%", "$1");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("slow-multiplier", 1);
        node.set("damage-addition", 3);
        node.set("damage-returned", 0.25);
        node.set("damage-reduced", 0.25);
        node.set("magic-resist-multiplier", 0.25);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% from %hero% expired.");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double dr = SkillConfigManager.getUseSetting(hero, this, "damage-reduced", 0.25, false);
        String description = getDescription().replace("$2", "" + dr * 100 + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    public class BlackMassEffect extends PeriodicEffect {
        private final double dret;
        private final double db;
        private final double dred;
        private final double mr;

        public BlackMassEffect(Skill skill, int slowmult, double dret, double dred, double db, double mr) {
            super(skill, "BlackMass", 5000);
            this.dred = dred;
            this.db = db;
            this.dret = dret;
            this.mr = mr;
            types.add(EffectType.BENEFICIAL);
            addMobEffect(2, (int) (getPeriod() + 2000 / 50), slowmult, false);
        }

        public double getDamageBonus() {
            return db;
        }

        public double getDamageReduction() {
            return dred;
        }

        public double getDamageReturned() {
            return dret;
        }

        public double getMagicResist() {
            return mr;
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, "BlackMass", hero.getName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, "BlackMass", hero.getName());
        }

        @Override
        public void tickHero(Hero hero) {
            reapplyToHero(hero);
        }
    }

    public class DdmgListener implements Listener {
        @EventHandler(priority = EventPriority.NORMAL)
        public void onDamage(WeaponDamageEvent event) {
            if (event.isCancelled() || (!(event.getEntity() instanceof Player)) || (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero blcaster = plugin.getCharacterManager().getHero((Player) event.getEntity());
            CharacterTemplate damager = event.getDamager();
            if (blcaster.hasEffect("BlackMass")) {
                BlackMassEffect bm = (BlackMassEffect) blcaster.getEffect("BlackMass");
                double preDamage = event.getDamage();
                event.setDamage(event.getDamage() - (event.getDamage() * bm.getDamageReduction()));
                if (Skill.damageCheck(blcaster.getPlayer(), damager.getEntity())) {
                    addSpellTarget(damager.getEntity(), blcaster);
                    Skill.damageEntity(damager.getEntity(), blcaster.getPlayer(), (bm.getDamageReturned() * preDamage), DamageCause.MAGIC);
                }
            }
        }

        @EventHandler(priority = EventPriority.NORMAL)
        public void onMagicDamage(EntityDamageByEntityEvent event) {
            if (event.isCancelled() || (!(event.getEntity() instanceof Player)) || (event.getCause() != DamageCause.MAGIC)) {
                return;
            }
            CharacterTemplate hero = plugin.getCharacterManager().getCharacter((Player) event.getEntity());
            if (hero.hasEffect("BlackMass")) {
                BlackMassEffect blm = (BlackMassEffect) hero.getEffect("BlackMass");
                event.setDamage(event.getDamage() - event.getDamage() * blm.getMagicResist());
            }

        }

        @EventHandler(priority = EventPriority.LOW)
        public void onAttack(WeaponDamageEvent event) {
            if (event.isCancelled() || (!(event.getEntity() instanceof Player)) || (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            CharacterTemplate damager = event.getDamager();
            if (damager.hasEffect("BlackMass")) {
                BlackMassEffect bm = (BlackMassEffect) damager.getEffect("BlackMass");
                event.setDamage((event.getDamage() + bm.getDamageBonus()));
            }
        }
    }
}
