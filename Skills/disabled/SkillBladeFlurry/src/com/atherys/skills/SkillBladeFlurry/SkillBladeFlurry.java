package com.atherys.skills.SkillBladeFlurry;

/**
 * Created by Arthur on 5/15/2015.
 */
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
public class SkillBladeFlurry extends ActiveSkill {
    public SkillBladeFlurry(Heroes plugin) {
        super(plugin, "BladeFlurry");
        setDescription("Active\nEnemies in a radius of $1 take $2 physical damage over $3 seconds");
        setArgumentRange(0, 0);
        setIdentifiers("skill bladeflurry");
        setUsage("/skill bladeflurry");
        setTypes(SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL, SkillType.PHYSICAL);
    }
    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        int damageTick = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK.node(), 2, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 6000, false);
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 3000, false);
        int damage = (int) (duration / period * damageTick);
        String description = getDescription().replace("$1", radius + "");
        description = description.replace("$2", damage + "");
        description = description.replace("$3", duration + "");
        description = description + Lib.getSkillCostStats(hero,this);
        return description;
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(6000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(3000));
        node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(2));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        return node;
    }
    @Override
    public SkillResult use(Hero hero, String[] args) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        double damageTick = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK.node(), 2, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 6000, false);
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 3000, false);
        Player player = hero.getPlayer();
        boolean hit = false;
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if ((e instanceof LivingEntity) && (damageCheck(player, (LivingEntity) e))) {
                CharacterTemplate character = this.plugin.getCharacterManager().getCharacter((LivingEntity) e);
                hit = true;
                character.addEffect(new BleedEffect(this, period, duration, damageTick, player));
            }
        }
        if (!hit) {
            Messaging.send(player, "No nearby targets!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
    public class BleedEffect extends PeriodicDamageEffect {
        public BleedEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "Bleed", period, duration, tickDamage, applier);
            this.types.add(EffectType.BLEED);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.HARMFUL);
        }
        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
        }
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 is bleeding!", player.getDisplayName());
        }
        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
        }
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 has stopped bleeding!", player.getDisplayName());
        }
    }
}

