package com.atherys.skills.SkillBlaze;

/**
 * Created by Arthur on 5/15/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import java.util.Set;

public class SkillBlaze extends ActiveSkill {
    public SkillBlaze(Heroes plugin) {
        super(plugin, "Blaze");
        setDescription("Active\n$1% chance to spawn 1 blaze, $2% for 2, and $3% for 3.");
        setUsage("/skill blaze");
        setArgumentRange(0, 0);
        setIdentifiers("skill blaze");
        setTypes(SkillType.DARK, SkillType.SUMMON, SkillType.SILENCABLE);
    }
    public String getDescription(Hero hero) {
        int chance2x = (int) (SkillConfigManager.getUseSetting(hero, this, "chance-2x", 0.2D, false) * 100.0D + SkillConfigManager.getUseSetting(hero, this, "added-chance-2x-per-level", 0.0D, false) * hero.getSkillLevel(this));
        int chance3x = (int) (SkillConfigManager.getUseSetting(hero, this, "chance-3x", 0.1D, false) * 100.0D + SkillConfigManager.getUseSetting(hero, this, "added-chance-3x-per-level", 0.0D, false) * hero.getSkillLevel(this));
        String description = getDescription().replace("$1", 100 - (chance2x + chance3x) + "").replace("$2", chance2x + "").replace("$3", chance3x + "");
        description = description + Lib.getSkillCostStats(hero,this);
        return description;
    }
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("chance-2x", Double.valueOf(0.2D));
        node.set("chance-3x", Double.valueOf(0.1D));
        node.set("added-chance-2x-per-level", Double.valueOf(0.0D));
        node.set("added-chance-3x-per-level", Double.valueOf(0.0D));
        return node;
    }
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        broadcastExecuteText(hero);
        double chance2x = SkillConfigManager.getUseSetting(hero, this, "chance-2x", 0.2D, false);
        double chance3x = SkillConfigManager.getUseSetting(hero, this, "chance-3x", 0.1D, false);
        Block wTargetBlock = player.getTargetBlock((Set<Material>) null, 20).getRelative(BlockFace.UP);
        double rand = Math.random();
        LivingEntity le = player.getWorld().spawn(wTargetBlock.getLocation(), Blaze.class);
        plugin.getCharacterManager().getMonster(le).setExperience(0);
        plugin.getCharacterManager().getMonster(le).setSpawnReason(SpawnReason.CUSTOM);
        for (int i = 0; i < 9; i++) {
            le.getWorld().playEffect(le.getLocation(), Effect.SMOKE, i);
        }
        for (int i = 0; i < 9; i++) {
            le.getWorld().playEffect(le.getLocation(), Effect.SMOKE, i);
        }
        for (int i = 0; i < 9; i++) {
            le.getWorld().playEffect(le.getLocation(), Effect.SMOKE, i);
        }
        int count = 1;
        if (rand > 1.0D - chance2x - chance3x) {
            le = player.getWorld().spawn(wTargetBlock.getLocation(), Blaze.class);
            plugin.getCharacterManager().getMonster(le).setExperience(0);
            plugin.getCharacterManager().getMonster(le).setSpawnReason(SpawnReason.CUSTOM);
            count++;
        }
        if (rand > 1.0D - chance3x) {
            le = player.getWorld().spawn(wTargetBlock.getLocation(), Blaze.class);
            plugin.getCharacterManager().getMonster(le).setExperience(0);
            plugin.getCharacterManager().getMonster(le).setSpawnReason(SpawnReason.CUSTOM);
            count++;
        }
        broadcast(player.getLocation(), "" + count + "x Multiplier!");
        return SkillResult.NORMAL;
    }
}
