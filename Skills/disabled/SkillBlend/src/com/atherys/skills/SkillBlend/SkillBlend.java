package com.atherys.skills.SkillBlend;

import com.atherys.heroesaddon.util.*; import com.atherys.effects.*; import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class SkillBlend extends ActiveSkill {
    public SkillBlend(Heroes plugin) {
        super(plugin, "Blend");
        setDescription("Active\nIn certain levels of light, the trueshot becomes stealthed after standing still for 2 seconds.");
        setUsage("/skill Blend");
        setArgumentRange(0, 0);
        setIdentifiers("skill Blend");
        setTypes(SkillType.ILLUSION, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("buff-duration", 50000);
        node.set("min-lightlevel", 5);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("BlendEffect")) {
            hero.removeEffect(hero.getEffect("BlendEffect"));
            this.broadcast(hero.getPlayer().getLocation(), hero.getName() + " lost " + this.getName() + "!");
            return SkillResult.CANCELLED;
        }
        long d = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 50000, false);
        byte min = (byte) SkillConfigManager.getUseSetting(hero, this, "min-lightlevel", 5, false);
        hero.addEffect(new BlendEffect(this, d, min));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class BlendEffect extends PeriodicExpirableEffect {
        private final byte min;
        private int c = 0;
        private Location loc;

        public BlendEffect(Skill skill, long duration, byte min) {
            super(skill, "BlendEffect", 500L, duration);
            this.min = min;
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            loc = hero.getPlayer().getLocation();
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            double cX = hero.getPlayer().getLocation().getX();
            double cZ = hero.getPlayer().getLocation().getZ();
            if ((loc.getX() == cX) && (loc.getZ() == cZ) && (hero.getPlayer().getLocation().getBlock().getLightLevel() <= min)) {
                if (!hero.hasEffect("Invisible") && (c >= 2000)) {
                    hero.addEffect(new BetterInvis(skill, getDuration(), "You blend into the shadows!", "You reappeared", hero));
                } else {
                    c += getPeriod();
                    return;
                }
            } else {
                loc = hero.getPlayer().getLocation();
                if (hero.hasEffect("Invisible"))
                    if (hero.getEffect("Invisible").getSkill().equals(this.skill))
                        hero.removeEffect(hero.getEffect("Invisible"));
                c = 0;
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }
    }
}