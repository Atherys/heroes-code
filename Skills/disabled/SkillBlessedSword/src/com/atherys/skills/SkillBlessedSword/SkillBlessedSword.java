package com.atherys.skills.SkillBlessedSword;

/**
 * Created by Arthur on 5/15/2015.
 */

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
public class SkillBlessedSword extends PassiveSkill {
    public SkillBlessedSword(Heroes plugin) {
        super(plugin, "BlessedSword");
        setDescription("Passive\nAllows your melee attacks to heal your party members and harm your enemies while using a Gold Sword");
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroesListener(this), plugin);
        setTypes(SkillType.BUFF, SkillType.HEAL, SkillType.DAMAGING);
    }
    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.COOLDOWN.node(), Long.valueOf(3000));
        node.set("heal-multiplier", Double.valueOf(1D));
        return node;
    }
    public class SkillHeroesListener implements Listener {
        private final Skill skill;
        public SkillHeroesListener(Skill skill) {
            this.skill = skill;
        }
        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero)) {
                return;
            }
            if (!(event.getEntity() instanceof LivingEntity)) {
                return;
            }
            Hero hero = (Hero) event.getDamager();
            Player player = hero.getPlayer();
            if (hero.hasEffect("BlessedSword")) {
                if (hero.getCooldown("BlessedSword") == null || hero.getCooldown("BlessedSword") <= System.currentTimeMillis()) {
                    double mult = SkillConfigManager.getUseSetting(hero, skill, "heal-multiplier", 1D, false);
                    double radius = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.RADIUS, 5, false);
                    long cd = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN.node(), 3000, false);
                    hero.setCooldown("BlessedSword", cd + System.currentTimeMillis());
                    if (player.getItemInHand().getType() == Material.GOLD_SWORD) {
                        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
                            if (!(entity instanceof LivingEntity)) {
                                continue;
                            }
                            if (damageCheck(player, (LivingEntity) entity)) {
                                damageEntity((LivingEntity) entity, player, event.getDamage(), EntityDamageEvent.DamageCause.ENTITY_ATTACK);
                                addSpellTarget(entity,hero);
                                continue;
                            }
                            if (!hero.hasParty() || !(entity instanceof Player) || !(hero.getParty().isPartyMember((Player)entity))) {
                                continue;
                            }
                            Hero thero = plugin.getCharacterManager().getHero((Player) entity);
                            HeroRegainHealthEvent healev = new HeroRegainHealthEvent(thero, (event.getDamage() * mult), skill);
                            plugin.getServer().getPluginManager().callEvent(healev);
                            if (!healev.isCancelled()) {
                                thero.heal(healev.getAmount());
                            }
                        }
                    }
                }
            }
        }
    }
}

