package com.atherys.skills.SkillBloodBath;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillBloodBath extends ActiveSkill {
    public SkillBloodBath(Heroes plugin) {
        super(plugin, "BloodBath");
        setDescription("Active\nCauses nearby enemies to gush blood. $1 physical damage over $2 seconds. Heals the user based on number of people it hits.");
        setUsage("/skill BloodBath");
        setArgumentRange(0, 0);
        setIdentifiers("skill BloodBath");
        setTypes(SkillType.PHYSICAL, SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.DURATION.node(), 10000L);
        node.set(SkillSetting.PERIOD.node(), 1000L);
        node.set("heal-per-hit", 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        double d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long du = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long p = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000L, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        double heal = SkillConfigManager.getUseSetting(hero, this, "heal-per-hit", 10, false);
        int h = 0;
        for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
            if (e instanceof Player) {
                Player pl = (Player) e;
                if (damageCheck(hero.getPlayer(), pl)) {
                    plugin.getCharacterManager().getHero(pl).addEffect(new BloodBathEffect(this, p, du, d, hero.getPlayer()));
                    h++;
                }
            }
        }
        HeroRegainHealthEvent event = new HeroRegainHealthEvent(hero, h * heal, this);
        plugin.getServer().getPluginManager().callEvent(event);
        if (!(event.isCancelled())) {
            hero.heal(event.getAmount());
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long du = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        String description = getDescription().replace("$1t", "" + d).replace("$2", du / 1000 + "");
        description = description + Lib.getSkillCostStats(hero, this);
        return description;
    }

    public class BloodBathEffect extends PeriodicDamageEffect {
        public BloodBathEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "BloodBathEffect", period, duration, tickDamage, applier);
            types.add(EffectType.BLEED);
            types.add(EffectType.PHYSICAL);
        }
    }
}
