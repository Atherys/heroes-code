package com.atherys.skills.SkillBloodshed;

/**
 * Created by Arthur on 5/16/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
public class SkillBloodshed extends TargettedSkill {
    public SkillBloodshed(Heroes plugin) {
        super(plugin, "Bloodshed");
        setDescription("Targeted\nTarget takes $1 physical damage, party takes $2 physical damage. $2 is split up among the party equally");
        setUsage("/skill Bloodshed");
        setArgumentRange(0, 0);
        setIdentifiers("skill bloodshed");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(4));
        node.set("damage-for-party", 50);
        return node;
    }
    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        double dp = SkillConfigManager.getUseSetting(hero, this, "damage-for-party", 50, false);
        damageEntity(target, player, damage, DamageCause.MAGIC);
        if (hero.hasParty()) {
            for (Hero phero : hero.getParty().getMembers()) {
                damageEntity(phero.getPlayer(), target, (dp / hero.getParty().getMembers().size()), DamageCause.ENTITY_ATTACK);
                addSpellTarget(phero.getPlayer(),hero);
            }
        } else {
            damageEntity(hero.getPlayer(), target, dp, DamageCause.ENTITY_ATTACK);
            addSpellTarget(target,hero);
        }
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        int damageforparty = SkillConfigManager.getUseSetting(hero, this, "damage-for-party", 50, false);
        String description = getDescription().replace("$1", damage + "").replace("$2", damageforparty + "");
        description = description + Lib.getSkillCostStats(hero,this);
        return description;
    }
}
