package com.atherys.skills.SkillBolt;

import com.atherys.effects.SparkStackEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillBolt extends TargettedSkill {
    public SkillBolt(Heroes plugin) {
        super(plugin, "Bolt");
        setDescription("Targeted\nCalls a bolt of lightning down on the target dealing $1 magic damage,"
                + "increasing by $2 per stack of Spark");
        setUsage("/skill bolt <target>");
        setArgumentRange(0, 0);
        setIdentifiers("skill bolt");
        setTypes(SkillType.LIGHTNING, SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 20);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 0);
        node.set("stack-damage", 10);
        return node;
    }

    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        target.getWorld().strikeLightningEffect(target.getLocation());
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 20, false);
        damage += SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0, false) * hero.getSkillLevel(this);
        if (hero.hasEffect("SparkStackEffect")) {
            int stacks = ((SparkStackEffect) hero.getEffect("SparkStackEffect")).getStacks();
            damage += stacks * SkillConfigManager.getUseSetting(hero, this, "stack-damage", 10, false);
            hero.removeEffect(hero.getEffect("SparkStackEffect"));
            Messaging.send(player, "Your bolt was enhanced by your " + stacks + (stacks == 1 ? " spark stack." : " spark stacks."));
        }
        if (target instanceof Player) {
            Hero tHero = plugin.getCharacterManager().getHero((Player)target);
            if (tHero.hasEffect("Grounded") && ((LivingEntity)target).isOnGround()) {
                damage *= (1-SkillConfigManager.getUseSetting(tHero, plugin.getSkillManager().getSkill("Grounded"), "damage-reduction", 0.5, false));
            }
        }
        this.plugin.getDamageManager().addSpellTarget(target, hero, this);
        damageEntity(target, player, damage, EntityDamageEvent.DamageCause.MAGIC);
        addSpellTarget(target, hero);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 20, false);
        damage += SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0, false) * hero.getSkillLevel(this);
        double stackDamage = SkillConfigManager.getUseSetting(hero, this, "stack-damage", 10, false);
        return getDescription().replace("$1", damage + "").replace("$2", stackDamage + "") + Lib.getSkillCostStats(hero, this);
    }
}