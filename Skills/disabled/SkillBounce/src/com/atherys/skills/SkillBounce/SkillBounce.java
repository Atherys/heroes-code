package com.atherys.skills.SkillBounce;

/**
 * Created by Arthur on 5/16/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
public class SkillBounce extends ActiveSkill {
    public SkillBounce(Heroes plugin) {
        super(plugin, "Bounce");
        setDescription("Active\n Grants the caster Jump Boost for $1s");
        setUsage("/skill Bounce");
        setArgumentRange(0, 0);
        setIdentifiers("skill Bounce");
        setTypes(SkillType.SILENCABLE, SkillType.BUFF);
    }
    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int duration = SkillConfigManager.getUseSetting(hero, this, "duration", 20000, false);
        int amplifier = SkillConfigManager.getUseSetting(hero, this, "amplifier", 2, false);
        hero.addEffect(new BounceEffect(this, duration, amplifier));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
    public class BounceEffect extends ExpirableEffect {
        public BounceEffect(Skill skill, long duration, int a) {
            super(skill, "Bounce", duration);
            addMobEffect(8, (int) (duration / 1000L * 20L), a, false);
            types.add(EffectType.SAFEFALL);
        }
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("duration", 20000);
        node.set("amplifier", Integer.valueOf(1));
        return node;
    }
    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, "duration", 20000, false);
        return getDescription().replace("$2", duration/1000+"") + Lib.getSkillCostStats(hero, this);
    }
}
