package com.atherys.skills.SkillBreakRank;

/**
 * Created by Arthur on 5/16/2015.
 */

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillBreakRank extends ActiveSkill {
    private String enderpearlDenyText;

    public SkillBreakRank(Heroes plugin) {
        super(plugin, "BreakRank");
        setDescription("Active\nThrow enderbomb");
        setUsage("/skill BreakRank");
        setArgumentRange(0, 0);
        setIdentifiers("skill BreakRank");
        setTypes(SkillType.DAMAGING, SkillType.FIRE, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEnderBombListener(), plugin);
    }

    @Override
    public void init() {
        super.init();
        enderpearlDenyText = SkillConfigManager.getRaw(this, "enderpearl-deny-text", "Not allowed to use enderpearl");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection defaultConfig = super.getDefaultConfig();
        defaultConfig.set(SkillSetting.HEALTH_COST.node(), 1);
        defaultConfig.set("explosion-radius", 3.0);
        defaultConfig.set("entity-damage-multiplier", 1.5);
        defaultConfig.set("fire", false);
        defaultConfig.set("prevent-wilderness-block-damage-and-fire", true);
        defaultConfig.set("prevent-enderpearl-use", false);
        defaultConfig.set("enderpearl-deny-text", "Not allowed to use enderpearl");
        return defaultConfig;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        float explosionRadius = (float) SkillConfigManager.getUseSetting(hero, this, "explosion-radius", 3.0, false);
        double entityDamageMultiplier = SkillConfigManager.getUseSetting(hero, this, "entity-damage-multiplier", 1.5, false);
        boolean fire = SkillConfigManager.getUseSetting(hero, this, "fire", false);
        boolean preventWildDamage = SkillConfigManager.getUseSetting(hero, this, "prevent-wilderness-block-damage-and-fire", true);
        Player player = hero.getPlayer();
        EnderPearl enderBomb = player.launchProjectile(EnderPearl.class);
        player.setMetadata("BreakRankThrower", new FixedMetadataValue(plugin, true));
        enderBomb.setMetadata("BreakRankEnderPearl", new FixedMetadataValue(plugin, new EnderBomb(hero, explosionRadius, fire, preventWildDamage, entityDamageMultiplier)));
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public boolean isPreventEnderPearlUseFor(Hero hero) {
        return SkillConfigManager.getUseSetting(hero, this, "prevent-enderpearl-use", false);
    }

    public class SkillEnderBombListener implements Listener {
        /*
         * Ender bomb explosion detector
         * onProjectileHit, onEntityExplode and onEntityDamage executes in the same tick and location +/- for enderbomb hit
         */
        private long enderbombHitTick = 0;
        private Location enderbombLandingLoc;
        private double currentEntityDamageMultiplier = 1.0;
        private boolean currentPreventWildDamage = false;

        // enderpearl teleport executes before onProjectileHit, so handle this
        @EventHandler(priority = EventPriority.HIGH)
        public void onPlayerTeleport(PlayerTeleportEvent event) {
            if (event.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
                if (event.getPlayer().hasMetadata("BreakRankThrower")) {
                    event.setCancelled(true);
                }
            }
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onProjectileHit(ProjectileHitEvent event) {
            if (event.getEntityType() == EntityType.ENDER_PEARL) {
                EnderPearl enderPearl = (EnderPearl) event.getEntity();
                if (enderPearl.hasMetadata("BreakRankbEnderPearl")) {
                    EnderBomb enderBomb = (EnderBomb) enderPearl.getMetadata("BreakRankEnderPearl").get(0).value();
                    enderbombHitTick = getFirstWorldTime();
                    enderbombLandingLoc = enderPearl.getLocation();
                    currentEntityDamageMultiplier = enderBomb.getEntityDamageMultiplier();
                    currentPreventWildDamage = enderBomb.isPreventWildDamage();
                    enderPearl.getWorld().createExplosion(enderPearl.getLocation(), enderBomb.getRadius(), enderBomb.isFire());
                    LivingEntity shooter = (LivingEntity) enderPearl.getShooter();
                    if (shooter instanceof Player) {
                        shooter.removeMetadata("BreakRankThrower", plugin);
                    }
                    enderPearl.removeMetadata("BreakRankbEnderPearl", plugin);
                    enderPearl.remove();
                }
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onEntityExplode(EntityExplodeEvent event) {
            if (getFirstWorldTime() == enderbombHitTick && event.getEntity() == null &&
                    enderbombLandingLoc.distanceSquared(event.getLocation()) < 4) { // if enderbomb
                if (currentPreventWildDamage || event.isCancelled()) {
                    event.blockList().clear();
                    event.setYield(0.0F);
                }
                // create explosion, even if protection plugin canceled it
                event.setCancelled(false);
            }
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (getFirstWorldTime() == enderbombHitTick && event.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION)
                if (currentEntityDamageMultiplier > 0) {
                    if (event.getEntity() instanceof LivingEntity) {
                        LivingEntity living = (LivingEntity) event.getEntity();
                        living.setNoDamageTicks(0);
                    }
                    event.setDamage(event.getDamage() * currentEntityDamageMultiplier);
                } else {
                    event.setCancelled(true);
                }
        }

        private long getFirstWorldTime() {
            return Bukkit.getWorlds().get(0).getFullTime();
        }

        @EventHandler(priority = EventPriority.HIGH)
        public void onPlayerInteract(PlayerInteractEvent event) {
            Action action = event.getAction();
            if (action != Action.RIGHT_CLICK_BLOCK && action != Action.RIGHT_CLICK_AIR) return;
            Player player = event.getPlayer();
            if (player.getGameMode() == GameMode.CREATIVE) return;
            if (player.getItemInHand() == null || player.getItemInHand().getType() != Material.ENDER_PEARL) return;
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (isPreventEnderPearlUseFor(hero)) {
                event.setUseItemInHand(Event.Result.DENY);
                Messaging.send(player, enderpearlDenyText);
            }
        }
    }

    public class EnderBomb {
        private final Hero hero;
        private final float radius;
        private final boolean fire;
        private final double entityDamageMultiplier;
        private final boolean preventWildDamage;

        public EnderBomb(Hero hero, float radius, boolean fire, boolean preventWildDamage, double entityDamageMultiplier) {
            this.radius = radius;
            this.hero = hero;
            this.fire = fire;
            this.preventWildDamage = preventWildDamage;
            this.entityDamageMultiplier = entityDamageMultiplier;
        }

        public Hero getHero() {
            return this.hero;
        }

        public float getRadius() {
            return this.radius;
        }

        public boolean isFire() {
            return this.fire;
        }

        public double getEntityDamageMultiplier() {
            return this.entityDamageMultiplier;
        }

        public boolean isPreventWildDamage() {
            return this.preventWildDamage;
        }
    }

}