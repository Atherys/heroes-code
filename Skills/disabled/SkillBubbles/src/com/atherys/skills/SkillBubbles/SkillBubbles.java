package com.atherys.skills.SkillBubbles;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillBubbles extends ActiveSkill {
    private String expireText;

    public SkillBubbles(Heroes plugin) {
        super(plugin, "Bubbles");
        setDescription("Gives party a buff that absorbs all damage for $1 duration or up to a certain amount of damage. Phase: small AoE that deals damage over time to enemies");
        setUsage("/skill Bubbles");
        setArgumentRange(0, 0);
        setIdentifiers("skill Bubbles");
        setTypes(SkillType.SILENCABLE);
        Bukkit.getPluginManager().registerEvents(new EntityDamageListener(), plugin);
    }

    @Override
    public void init() {
        super.init();
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2").replace("%target%", "$3");
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000,
                false);
        int size = SkillConfigManager.getUseSetting(hero, this, "barrier-size", 50, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", size + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("bubble-size", Integer.valueOf(50));
        node.set("bubble-duration", Integer.valueOf(10000));
        node.set("invuln-duration", 10000);
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        if (hero.hasEffect("PhaseEffect")) {
            long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000, false);
            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10, false);
            double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
            for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
                if (e instanceof Player) {
                    plugin.getCharacterManager().getHero((Player) e).addEffect(new PeriodicDamageEffect(this, "BubblesDEffect,", period, duration, damage, hero.getPlayer(), false));
                }
            }
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;

        }
        Player player = hero.getPlayer();
        int size = SkillConfigManager.getUseSetting(hero, this, "bubble-size", 50, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, "bubble-duration", 5000, false);
        if (!hero.hasParty()) {
            if (!hero.hasEffect("ShellEffect")) {
                hero.addEffect(new ShellEffect(this, duration, size, player.getName()));
            }
        } else {
            for (Hero pHero : hero.getParty().getMembers()) {
                Player pPlayer = pHero.getPlayer();
                if (pPlayer.getWorld().equals(player.getWorld()) && pPlayer.getLocation().distanceSquared(player.getLocation()) <= r * r) {
                    pHero.addEffect(new ShellEffect(this, duration, size, player.getName()));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class EntityDamageListener implements Listener {
        public EntityDamageListener() {
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.isCancelled() || !(event.getEntity() instanceof Player)
                    || event.getDamage() == 0 || !(event instanceof EntityDamageByEntityEvent)) {
                return;
            }
            Hero h = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (!h.hasEffect("ShellEffect")) {
                return;
            }
            EntityDamageByEntityEvent se = (EntityDamageByEntityEvent) event;
            if (!(se.getDamager() instanceof LivingEntity)) {
                return;
            }
            ShellEffect il = (ShellEffect) h.getEffect("ShellEffect");
            il.setNewSize(event.getDamage(), h, (LivingEntity) se.getDamager(), event.getCause());
            event.setDamage(0D);
        }
    }

    public class ShellEffect extends ExpirableEffect {
        private final int size;
        private int cursize;
        private final String caster;

        public ShellEffect(Skill skill, long duration, int size, String caster) {
            super(skill, "ShellEffect", duration);
            this.types.add(EffectType.BENEFICIAL);
            this.cursize = size;
            this.size = size;
            this.caster = caster;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.applyToHero(hero);
            if (hero.getPlayer().getName().equals(caster))
                broadcast(hero.getPlayer().getLocation(), expireText, caster, "Bubble", hero.getName());
        }

        public int getSize() {
            return size - cursize;
        }

        public void setNewSize(double damage, Hero hero, LivingEntity damager, EntityDamageEvent.DamageCause cause) {
            if (cursize <= damage) {
                hero.removeEffect(this);
                damageEntity(hero.getPlayer(), damager, Math.abs(getSize() - damage), cause);
            } else {
                cursize -= damage;
                hero.getPlayer().sendMessage(ChatColor.GRAY + "Your shield blocked " + getSize() + " damage.");
            }
        }
    }
}
