package com.atherys.skills.SkillBullseye;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.SneakEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillBullseye extends TargettedSkill {
    public SkillBullseye(Heroes plugin) {
        super(plugin, "Bullseye");
        setDescription("Targeted\nStuns the user for $1 seconds, gives user the sneak effects, at end of self-stun, deals $2 true damage to the target");
        setUsage("/skill BullsEye");
        setArgumentRange(0, 0);
        setIdentifiers("skill BullsEye");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", damage + "") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(6));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }
        Player tPlayer = (Player) target;
        if (!damageCheck(player, tPlayer)) {
            Messaging.send(player, "You can't harm that target");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        hero.addEffect(new BullseyeEffect(this, duration, hero, damage, target));
        hero.addEffect(new SneakEffect(this, 5000L, duration));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class BullseyeEffect extends PeriodicExpirableEffect {
        private Location castloc;
        private final LivingEntity target;
        private final Hero caster;
        private final double damage;

        public BullseyeEffect(Skill skill, long duration, Hero caster, double damage, LivingEntity target) {
            super(skill, "BullseyeEffect", 20L, duration);
            this.target = target;
            this.caster = caster;
            this.damage = damage;
            types.add(EffectType.STUN);
            types.add(EffectType.HARMFUL);
            addMobEffect(9, (int) (duration / 1000L) * 20, 127, false);
        }

        @Override
        public void tickMonster(Monster mnstr) {
        }

        @Override
        public void tickHero(Hero hero) {
            Location location = hero.getPlayer().getLocation();
            if (location == null) {
                return;
            }
            if ((location.getX() != castloc.getX()) || (location.getY() != castloc.getY()) || (location.getZ() != castloc.getZ())) {
                castloc.setYaw(location.getYaw());
                castloc.setPitch(location.getPitch());
                hero.getPlayer().teleport(castloc);
            }
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            castloc = hero.getPlayer().getLocation();
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = caster.getPlayer();
            damageEntity(target, player, damage, DamageCause.CUSTOM);
            addSpellTarget(target, caster);
        }
    }
}