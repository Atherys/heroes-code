package com.atherys.skills.SkillCastShadow;
import com.atherys.effects.*; import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillCastShadow extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillCastShadow(Heroes plugin) {
        super(plugin, "CastShadow");
        setDescription("Cloak a nearby ally within $1 blocks for $2 seconds.");
        setUsage("/skill castshadow");
        setArgumentRange(0, 0);
        setIdentifiers("skill castshadow");
        setTypes(SkillType.ILLUSION, SkillType.BUFF, SkillType.COUNTER, SkillType.STEALTHY, SkillType.SILENCABLE);
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "You sink into the shadows!");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "You step out of the shadows!");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player p = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false);
        boolean found = false;
        if (hero.hasParty()) {
            for (Hero phero : hero.getParty().getMembers()) {
                if (phero != hero) {
                    if (p.hasLineOfSight(phero.getPlayer()) && ((p.getLocation().distanceSquared(phero.getPlayer().getLocation())) <= (radius * radius)) && !found) {
                        Hero cloakTarget = phero;
                        cloakTarget.addEffect(new BetterInvis(this, duration, this.applyText, this.expireText, cloakTarget));
                        hero.getParty().messageParty(ChatColor.GRAY + hero.getName() + " has cloaked " + cloakTarget.getName() + "!");
                        found = true;
                    }
                }
            }
            if(!found){
                p.sendMessage(ChatColor.GRAY + "There are no nearby party members to cloak!");
                return SkillResult.CANCELLED;
            }
        } else {
            p.sendMessage(ChatColor.GRAY + "You need to be in a party to use this skill!");
            return SkillResult.CANCELLED;
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false);
        String description = getDescription().replace("$1", radius + "").replace("$2", duration/1000 + "");
        int cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 45, false);
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        return description;
    }
    
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set(SkillSetting.DURATION.node(), 30000L);
        return node;
    }
}