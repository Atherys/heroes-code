package com.atherys.skills.SkillClot;

/**
 * Created by Arthur on 5/16/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillType;
public class SkillClot extends ActiveSkill
{
    public SkillClot(Heroes plugin)
    {
        super(plugin, "Clot");
        setDescription("Active\n Forces your own blood to clot, stopping bleed damage on yourself.");
        setUsage("/skill Clot");
        setArgumentRange(0, 0);
        setIdentifiers("skill Clot");
        setTypes(SkillType.SILENCABLE,SkillType.COUNTER);
    }
    @Override
    public SkillResult use(Hero hero, String[] args)
    {
        for (Effect e : hero.getEffects()) {
            if (e.isType(EffectType.BLEED)) {
                hero.removeEffect(e);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
    @Override
    public String getDescription(Hero hero)
    {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}

