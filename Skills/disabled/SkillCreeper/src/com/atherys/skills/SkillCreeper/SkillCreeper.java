package com.atherys.skills.SkillCreeper;

/**
 * Created by Arthur on 5/16/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
public class SkillCreeper extends ActiveSkill {
    public SkillCreeper(Heroes plugin) {
        super(plugin, "Creeper");
        setDescription("Active\n $1% chance to spawn 1 creeper, $2% for 2, and $3% for 3.");
        setUsage("/skill creeper");
        setArgumentRange(0, 0);
        setIdentifiers("skill creeper");
        setTypes(SkillType.DARK, SkillType.SUMMON, SkillType.SILENCABLE);
    }
    @Override
    public String getDescription(Hero hero) {
        int chance2x = (int) (SkillConfigManager.getUseSetting(hero, this, "chance-2x", 0.2, false) * 100 + SkillConfigManager.getUseSetting(hero, this, "added-chance-2x-per-level", 0.0, false) * hero.getSkillLevel(this));
        int chance3x = (int) (SkillConfigManager.getUseSetting(hero, this, "chance-3x", 0.1, false) * 100 + SkillConfigManager.getUseSetting(hero, this, "added-chance-3x-per-level", 0.0, false) * hero.getSkillLevel(this));
        String description = getDescription().replace("$1", (100 - (chance2x + chance3x)) + "").replace("$2", chance2x + "").replace("$3", chance3x + "");
        return description + Lib.getSkillCostStats(hero, this);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("chance-2x", 0.2);
        node.set("chance-3x", 0.1);
        node.set("added-chance-2x-per-level", 0.0);
        node.set("added-chance-3x-per-level", 0.0);
        return node;
    }
    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        broadcastExecuteText(hero);
        double chance2x = SkillConfigManager.getUseSetting(hero, this, "chance-2x", 0.2, false);
        double chance3x = SkillConfigManager.getUseSetting(hero, this, "chance-3x", 0.1, false);
        Block wTargetBlock = player.getTargetBlock((Set<Material>) null, 20).getRelative(BlockFace.UP);
        double rand = Math.random();
        Set<Monster> summons = new HashSet<Monster>();
        LivingEntity le1 = player.getWorld().spawn(wTargetBlock.getLocation(), Creeper.class);
        summons.add(plugin.getCharacterManager().getMonster(le1));
        le1.getWorld().playEffect(le1.getLocation(), Effect.SMOKE, 3);
        int count = 1;
        if (rand > (1 - chance2x - chance3x)) {
            LivingEntity le2 = player.getWorld().spawn(wTargetBlock.getLocation(), Creeper.class);
            summons.add(plugin.getCharacterManager().getMonster(le2));
            le2.getWorld().playEffect(le2.getLocation(), Effect.SMOKE, 3);
            count++;
        }
        if (rand > (1 - chance3x)) {
            LivingEntity le3 = player.getWorld().spawn(wTargetBlock.getLocation(), Creeper.class);
            summons.add(plugin.getCharacterManager().getMonster(le3));
            le3.getWorld().playEffect(le3.getLocation(), Effect.SMOKE, 3);
            count++;
        }
        for (Monster mob : summons) {
            mob.setExperience(0);
        }
        for (Monster mob : summons) {
            mob.getEntity().setCustomName(hero.getName() + "'s Creeper");
        }
        broadcast(player.getLocation(), "" + count + "x Multiplier!");
        return SkillResult.NORMAL;
    }
}
