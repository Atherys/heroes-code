package com.atherys.skills.SkillCure;

/**
 * Created by Arthur on 5/16/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
public class SkillCure extends ActiveSkill {
    public SkillCure(Heroes plugin) {
        super(plugin, "Cure");
        setDescription("Active\nRemoves $1 afflictions from partymembers within a radius of $2.");
        setUsage("/skill Cure");
        setArgumentRange(0, 0);
        setIdentifiers("skill Cure");
        setTypes(SkillType.FORCE, SkillType.COUNTER,SkillType.SILENCABLE);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set("removes-max", 5);
        return node;
    }
    @Override
    public String getDescription(Hero hero) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int rem = SkillConfigManager.getUseSetting(hero, this, "removes-max", 5, false);
        return getDescription().replace("$1", rem + "").replace("$2", r + "") + Lib.getSkillCostStats(hero, this);
    }
    @Override
    public SkillResult use(Hero hero, String[] args) {
        // Player p = hero.getPlayer();
        int rem = SkillConfigManager.getUseSetting(hero, this, "removes-max", 5, false);
        if (!hero.hasParty()) {
            for (Effect e : hero.getEffects()) {
                if (e.isType(EffectType.BENEFICIAL)) {
                    if (rem != 0) {
                        hero.removeEffect(e);
                        rem--;
                    }
                }
            }
        } else {
            int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
            for (Hero phero : hero.getParty().getMembers()) {
                if (phero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= r * r) {
                    for (Effect e : hero.getEffects()) {
                        if (e.isType(EffectType.BENEFICIAL)) {
                            if (rem != 0) {
                                hero.removeEffect(e);
                                rem--;
                            }
                        }
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}

