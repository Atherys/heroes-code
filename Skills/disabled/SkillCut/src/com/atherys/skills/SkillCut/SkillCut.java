package com.atherys.skills.SkillCut;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillCut extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillCut(Heroes plugin) {
        super(plugin, "Cut");
        setDescription("Active\nFor $1s melee swings will cause the target to bleed taking $2 physical damage over $3 seconds");
        setArgumentRange(0, 0);
        setIdentifiers("skill cut");
        setUsage("/skill cut");
        setTypes(SkillType.BUFF, SkillType.SILENCABLE, SkillType.PHYSICAL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) / 1000;
        long bleedDuration = SkillConfigManager.getUseSetting(hero, this, "Bleed-Duration", 3000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        double tickDamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 1, false);
        int damage = (int) (tickDamage * (bleedDuration / period));
        String description = getDescription().replace("$1", duration + "");
        description = description.replace("$2", damage + "");
        description = description.replace("$3", (bleedDuration / 1000) + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set("Bleed-Duration", 3000);
        node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(1));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(1000));
        node.set(SkillSetting.APPLY_TEXT.node(), "Your strikes now cut deeper!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "Your strikes no longer cut deep!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "Your strikes now cut deeper!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "Your strikes no longer cut deep!");
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        Player player = hero.getPlayer();
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        long bleedDuration = SkillConfigManager.getUseSetting(hero, this, "Bleed-Duration", 3000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        int tickDamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 1, false);
        BleedEffect bEffect = new BleedEffect(this, period, bleedDuration, tickDamage, player);
        CutEffect cEffect = new CutEffect(this, duration, player, bEffect, applyText, expireText);
        hero.addEffect(cEffect);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class CutEffect extends ExpirableEffect {
        private final BleedEffect dot;
        private final String applyText;
        private final String expireText;

        public CutEffect(Skill skill, long duration, Player player, BleedEffect dot, String applyText, String expireText) {
            super(skill, "Cut", duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.PHYSICAL);
            this.dot = dot;
            this.applyText = applyText;
            this.expireText = expireText;
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Messaging.send(hero.getPlayer(), this.applyText);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), this.expireText);
        }

        public BleedEffect getBleedEffect() {
            return dot;
        }
    }

    public class BleedEffect extends PeriodicDamageEffect {
        public BleedEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "Bleed", period, duration, tickDamage, applier);
            this.types.add(EffectType.BLEED);
            this.types.add(EffectType.PHYSICAL);
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 is bleeding!", player.getDisplayName());
        }

        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 has stopped bleeding!", player.getDisplayName());
        }
    }

    public class SkillEntityListener implements Listener {
        @EventHandler(ignoreCancelled = true)
        public void onEntityDamageEntity(WeaponDamageEvent event) {
            if ((!(event.getDamager() instanceof Hero)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            Hero dmgr = (Hero) event.getDamager();
            CharacterTemplate target = plugin.getCharacterManager().getCharacter((LivingEntity) event.getEntity());
            if (dmgr.hasEffect("Cut") && !(target.hasEffect("Bleed"))) {
                BleedEffect bEffect = ((CutEffect) dmgr.getEffect("Cut")).getBleedEffect();
                target.addEffect(bEffect);
            }
        }
    }
}
