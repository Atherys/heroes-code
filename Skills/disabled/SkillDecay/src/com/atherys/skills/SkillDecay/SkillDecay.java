package com.atherys.skills.SkillDecay;

/**
 * Created by Arthur on 5/16/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
public class SkillDecay extends TargettedSkill {
    public SkillDecay(Heroes plugin) {
        super(plugin, "Decay");
        setDescription("Targeted\n Damages all armor worn by target by $1");
        setUsage("/skill Decay");
        setArgumentRange(0, 0);
        setIdentifiers("skill Decay");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }
    @Override
    public String getDescription(Hero hero) {
        short damage = (short) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1",damage+"") + Lib.getSkillCostStats(hero, this);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 10);
        return node;
    }
    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player))
            return SkillResult.INVALID_TARGET_NO_MSG;
        short damage = (short) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        Player target = (Player) le;
        for( ItemStack armor : target.getInventory().getArmorContents()){
            if(armor.getType().equals(Material.AIR))
                continue;
            damageItem(armor, damage, 3, target, armor.getType().getMaxDurability());
        }
        broadcastExecuteText(hero, le);
        return SkillResult.NORMAL;
    }
    @SuppressWarnings("deprecation")
    private void damageItem(ItemStack item, short damage, int index, Player target, int maxdurability) {
        ItemStack[] armor = target.getInventory().getArmorContents();
        item.setDurability((short) (item.getDurability() + damage));
        if (item.getDurability() >= maxdurability) {
            armor[index].setType(Material.AIR);
            target.getInventory().setArmorContents(armor);
            target.updateInventory();
        }
    }
}
