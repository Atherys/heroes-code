package com.atherys.skills.SkillDefender;

/**
 * Created by Arthur on 5/16/2015.
 */
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
public class SkillDefender extends TargettedSkill {
    private String applyText;
    private String expireText;
    public SkillDefender(Heroes plugin) {
        super(plugin, "Defender");
        setDescription("For $1 seconds, allies in a radius of $2 of target gain gain increased defense by $3% for $4s, every $5 seconds");
        setArgumentRange(0, 1);
        setIdentifiers("skill defender");
        setUsage("/skill defender");
        setTypes(SkillType.SILENCABLE, SkillType.BUFF);
        Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
    }
    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false) / 1000;
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000, false) / 1000;
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, "bolst-amount", 0.2, false);
        int bolstDur = SkillConfigManager.getUseSetting(hero, this, "bolst-duration", 5000, false) / 1000;
        String description = getDescription().replace("$1", duration + "");
        description = description.replace("$2", radius + "");
        description = description.replace("$3", amount + "");
        description = description.replace("$4", bolstDur + "");
        description = description.replace("$5", period + "");
        // COOLDOWN
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description += " CD:" + cooldown + "s";
        }
        // MANA
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
        if (mana > 0) {
            description += " M:" + mana;
        }
        // HEALTH_COST
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this));
        if (healthCost > 0) {
            description += " HP:" + healthCost;
        }
        // STAMINA
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
        if (staminaCost > 0) {
            description += " FP:" + staminaCost;
        }
        // DELAY
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description += " W:" + delay + "s";
        }
        // EXP
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description += " XP:" + exp;
        }
        return description;
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(30000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(10000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(5));
        node.set("bolst-amount", Double.valueOf(0.2));
        node.set("bolst-duration", Integer.valueOf(5000));
        node.set("bolst-applytext", "Your Defences are increased!");
        node.set("bolst-expiretext", "Your Defences are no longer increased!");
        return node;
    }
    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 is Defending!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 is no longer Defending!");
    }
    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player))
            return SkillResult.INVALID_TARGET;
        if(hero.hasParty()){
            if ( !(hero.getParty().isPartyMember((Player) target)))
                return SkillResult.INVALID_TARGET;
        }
        else if (!(target.equals(player))){
            return SkillResult.INVALID_TARGET;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        long BolstDur = SkillConfigManager.getUseSetting(hero, this, "bolst-duration", 5000, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, "bolst-amount", 0.2, false);
        String apply = SkillConfigManager.getUseSetting(hero, this, "bolst-applytext", "Your Defences are increased!");
        String expire = SkillConfigManager.getUseSetting(hero, this, "bolst-expiretext", "Your Defences are no longer increased!");
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        BolsterEffect effect = new BolsterEffect(this, BolstDur, player, amount, apply, expire);
        targetHero.addEffect(new DefenderEffect(this, period, duration, radius, effect, applyText, expireText));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
    public class DefenderEffect extends PeriodicExpirableEffect {
        private final String applyText;
        private final String expireText;
        private final int radius;
        private final BolsterEffect effect;
        public DefenderEffect(Skill skill, long period, long duration, int radius, BolsterEffect effect, String applyText, String expireText) {
            super(skill, "DefenderEffect", period, duration);
            this.applyText = applyText;
            this.expireText = expireText;
            this.radius = radius;
            this.effect = effect;
            this.types.add(EffectType.BENEFICIAL);
        }
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
        @Override
        public void tickHero(Hero hero) {
            hero.addEffect(effect);
            for (Entity e : hero.getPlayer().getNearbyEntities(this.radius, this.radius, this.radius)) {
                if (((e instanceof Player) && hero.getParty().isPartyMember((Player) e))) {
                    CharacterTemplate target = this.plugin.getCharacterManager().getCharacter((LivingEntity) e);
                    target.addEffect(this.effect);
                }
            }
        }
        @Override
        public void tickMonster(Monster mob) {
        }
    }
    public class BolsterEffect extends ExpirableEffect {
        private final String applyText;
        private final String expireText;
        private final double amount;
        public BolsterEffect(Skill skill, long duration, Player caster, double amount, String applyText, String expireText) {
            super(skill, "DBolster", duration);
            this.amount = amount;
            this.applyText = applyText;
            this.expireText = expireText;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.MAGIC);
        }
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, this.applyText);
        }
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, this.expireText);
        }
        public double getAmount() {
            return this.amount;
        }
    }
    public class HeroDamageListener implements Listener {
        public HeroDamageListener() {
        }
        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getEntity() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("DBolster")) {
                BolsterEffect b = (BolsterEffect) hero.getEffect("DBolster");
                event.setDamage(event.getDamage() - (event.getDamage() * b.getAmount()));
            }
        }
    }
}

