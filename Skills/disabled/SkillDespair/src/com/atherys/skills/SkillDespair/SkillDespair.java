package com.atherys.skills.SkillDespair;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillDespair extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillDespair(Heroes plugin) {
        super(plugin, "Despair");
        setDescription("Active\nBlinds all enemies within $1 blocks for $2 seconds and deals $3 magic damage.");
        setUsage("/skill despair");
        setArgumentRange(0, 0);
        setIdentifiers("skill despair");
        setTypes(SkillType.DARK, SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% has blinded you with %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You have recovered your sight!");
    }

    public String getDescription(Hero hero) {
        long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false)) / 1000L;
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 0, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        String description = getDescription().replace("$1", radius + "").replace("$2", duration + "").replace("$3", damage + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(5));
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% has blinded you with %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You have recovered your sight!");
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
        Player player = hero.getPlayer();
        DespairEffect dEffect = new DespairEffect(this, duration, player);
        boolean hit = false;
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if ((e instanceof Player)) {
                if (damageCheck(player, (Player) e)) {
                    //BetterInvis
                    Hero tHero = this.plugin.getCharacterManager().getHero((Player) e);
                    if (tHero.hasEffect("Invisible") && (hero.hasParty() && !hero.getParty().getMembers().contains(tHero))) {
                        tHero.removeEffect(tHero.getEffect("Invisible"));
                    }
                    tHero.cancelDelayedSkill();
                    damageEntity(tHero.getPlayer(), player, damage, DamageCause.MAGIC);
                    tHero.addEffect(dEffect);
                    hit = true;
                }
            } else if (e instanceof LivingEntity) {
                if (damageCheck(player, (LivingEntity) e)) {
                    damageEntity((LivingEntity) e, player, damage, DamageCause.MAGIC);
                    hit = true;
                }
            }
        }
        if (!hit) {
            Messaging.send(player, "No valid targets within range!");
            return SkillResult.CANCELLED;
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class DespairEffect extends ExpirableEffect {
        private final Player player;

        public DespairEffect(Skill skill, long duration, Player player) {
            super(skill, "Despair", duration);
            this.player = player;
            addMobEffect(15, (int) (duration / 1000L * 20L), 3, false);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.DARK);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Messaging.send(hero.getPlayer(), SkillDespair.this.applyText, this.player.getDisplayName(), "Despair");
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), SkillDespair.this.expireText);
        }
    }
}