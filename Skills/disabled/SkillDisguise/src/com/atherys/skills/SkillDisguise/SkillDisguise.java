package com.atherys.skills.SkillDisguise;

import com.atherys.effects.DisguiseEffect;
import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.util.Lib;
import com.atherys.heroesaddon.util.ParticleEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

public class SkillDisguise extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillDisguise(Heroes plugin) {
        super(plugin, "Disguise");
        setDescription("You disguise yourself as the given target.");
        setUsage("/skill disguise <target>");
        setArgumentRange(1, 1);
        setIdentifiers("skill disguise");
        setNotes("Note: Taking damage removes the effect");
        setTypes(SkillType.ILLUSION, SkillType.BUFF, SkillType.COUNTER, SkillType.STEALTHY, SkillType.SILENCABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(30000));
        node.set(SkillSetting.APPLY_TEXT.node(), "You disguised yourself!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You returned to normal!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "You disguised yourself as $1!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You returned to normal!");
    }

    public SkillResult use(Hero hero, String[] args) {
        String target = args[0];
        Player targetPlayer = Bukkit.getPlayer(target);
        Player player = hero.getPlayer();
        if (targetPlayer == null) {
            Messaging.send(player, "Player " + ChatColor.WHITE + target + ChatColor.GRAY + " does not exist.");
            return SkillResult.CANCELLED;
        }
        if (player.equals(targetPlayer)) {
            Messaging.send(player, "What would disguising as yourself even be good for?");
            return SkillResult.CANCELLED;
        }
        Team team = Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(targetPlayer);
        if (!(team == null) && (team.getName().equals("admin") || team.getName().equals("mod"))) {
            Messaging.send(player, "You can't disguise yourself as a staff member.");
            return SkillResult.CANCELLED;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false);
        ParticleEffect.SMOKE_NORMAL.display(0, 0, 0, 0, 1, player.getLocation(), HeroesAddon.PARTICLE_RANGE);
        hero.addEffect(new DisguiseEffect(this, duration, this.applyText, this.expireText, targetPlayer));
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
