package com.atherys.skills.SkillDoom;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillDoom extends TargettedSkill {
    private String applytext;
    private String expiretext;

    public SkillDoom(Heroes plugin) {
        super(plugin, "Doom");
        setDescription("Targeted\nMarks the targets doom, dealing $1 magic damage after $2s");
        setUsage("/skill Doom");
        setArgumentRange(0, 0);
        setIdentifiers("skill Doom");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "").replace("$2", duration / 1000 + "") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill% on %target%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired from %target%.");
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        plugin.getCharacterManager().getHero((Player) le).addEffect(new DoomEffect(this, damage, duration, hero.getPlayer()));
        return SkillResult.NORMAL;
    }

    public class DoomEffect extends ExpirableEffect {
        private final double damage;
        private final Player caster;

        public DoomEffect(Skill skill, double damage, long duration, Player caster) {
            super(skill, "Doom", duration);
            this.damage = damage;
            this.caster = caster;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, "Doom", caster.getName(), hero.getName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            damageEntity(hero.getPlayer(), caster, damage, DamageCause.MAGIC);
            broadcast(hero.getPlayer().getLocation(), expiretext, "Doom", caster.getName(), hero.getName());
        }
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill% on %target%.").replace("%skill%", "$1").replace("%hero%", "$2").replace("%target%", "$3");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired from %target%.").replace("%skill%", "$1").replace("%hero%", "$2").replace("%target%", "$3");
    }
}