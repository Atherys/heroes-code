package com.atherys.skills.SkillDoubleBlink;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class SkillDoubleBlink extends ActiveSkill {
    public SkillDoubleBlink(Heroes plugin) {
        super(plugin, "DoubleBlink");
        setDescription("Active\nTeleports you up to $1 blocks away and can be cast another $2 times");
        setUsage("/skill DoubleBlink");
        setArgumentRange(0, 0);
        setIdentifiers("skill DoubleBlink");
        setTypes(SkillType.SILENCABLE, SkillType.TELEPORT);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("number-of-blinks", Integer.valueOf(3));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(6));
        return node;
    }

    public boolean blink(Hero hero, int distance) {
        Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return false;
        }
        Block prev = null;
        BlockIterator iter = null;
        try {
            iter = new BlockIterator(player, distance);
        } catch (IllegalStateException e) {
            Messaging.send(player, "There was an error getting your blink location!");
            return false;
        }
        while (iter.hasNext()) {
            Block b = iter.next();
            if ((!Util.transparentBlocks.contains(b.getType())) || ((!Util.transparentBlocks.contains(b.getRelative(BlockFace.UP).getType())) && (!Util.transparentBlocks.contains(b.getRelative(BlockFace.DOWN).getType()))))
                break;
            prev = b;
        }
        if (prev != null) {
            Location teleport = prev.getLocation().clone();
            teleport.add(new Vector(0.5D, 0.5D, 0.5D));
            teleport.setPitch(player.getLocation().getPitch());
            teleport.setYaw(player.getLocation().getYaw());
            player.teleport(teleport);
        }
        return true;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        if (!blink(hero, distance))
            return SkillResult.CANCELLED;
        DoubleBlinkEffect effect = null;
        if (!hero.hasEffect("doubleBlinkEffect")) {
            effect = new DoubleBlinkEffect(this, nb);
            hero.addEffect(effect);
        } else {
            effect = (DoubleBlinkEffect) hero.getEffect("doubleBlinkEffect");
        }
        effect.decBlinksLeft();
        if (effect.getBlinksLeft() >= 1) {
            Messaging.send(hero.getPlayer(), effect.getBlinksLeft() + " blink(s) remaining.");
            broadcastExecuteText(hero);
            return SkillResult.CANCELLED;
        }
        if (effect.getBlinksLeft() == 0) {
            hero.removeEffect(effect);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        return getDescription().replace("$1", distance + "").replace("$2", nb - 1 + "") + Lib.getSkillCostStats(hero, this);
    }

    public class DoubleBlinkEffect extends Effect {
        private int nb;

        public DoubleBlinkEffect(Skill skill, int nb) {
            super(skill, "doubleBlinkEffect");
            types.add(EffectType.BENEFICIAL);
            this.nb = nb;
        }

        public int getBlinksLeft() {
            return nb;
        }

        public void decBlinksLeft() {
            nb--;
        }
    }
}