package com.atherys.skills.SkillEarthToss;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillEarthToss extends ActiveSkill implements Listener {

    public SkillEarthToss(Heroes plugin) {
        super(plugin, "EarthToss");
        setDescription("Throw the earth under you");
        setUsage("/skill earthtoss");
        setArgumentRange(0, 0);
        setIdentifiers("skill earthtoss");
        setTypes(SkillType.PHYSICAL, SkillType.HARMFUL, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new EarthTossListener(this), plugin);
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 4);
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Vector direction = player.getEyeLocation().getDirection().multiply(1.5);
        Location starter = player.getLocation();
        ItemStack dirt = new ItemStack(Material.DIRT, 1);
        @SuppressWarnings("deprecation")
        FallingBlock block = player.getWorld().spawnFallingBlock(starter, dirt.getType(), dirt.getData().getData());
        dirt = null;
        block.setDropItem(false);
        block.setVelocity(direction);
        block.setMetadata("EarthTossFallingBlock", new FixedMetadataValue(plugin, true));
        Snowball snowball = player.launchProjectile(Snowball.class);
        snowball.setVelocity(direction);
        snowball.setMetadata("EarthTossProjectile", new FixedMetadataValue(plugin, block));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class EarthTossListener implements Listener {
        private Skill skill;

        public EarthTossListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onBecomeItem(ItemSpawnEvent event) {
            if (event.getEntity().getItemStack().getType() == Material.DIRT) {
                for (Entity e : event.getEntity().getNearbyEntities(5, 5, 5)) {
                    if (e instanceof FallingBlock) {
                        if (e.hasMetadata("EarthTossFallingBlock")) {
                            event.getEntity().remove();
                            if (e.isValid()) {
                                e.remove();
                            }
                        }
                    }
                }
            }
        }

        @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
        public void onFormBlock(EntityChangeBlockEvent event) {
            if (event.getEntityType() == EntityType.FALLING_BLOCK) {
                FallingBlock block = (FallingBlock) event.getEntity();
                if (block.hasMetadata("EarthTossFallingBlock")) {
                    event.setCancelled(true);
                    if (block.isValid()) {
                        block.remove();
                    }
                }
            }
        }


        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageByEntityEvent event) {
            if (!(event.getDamager() instanceof Snowball) || !(event.getEntity() instanceof LivingEntity) || !(event.getDamager().hasMetadata("EarthTossProjectile"))) {
                return;
            }
            LivingEntity entity = (LivingEntity) event.getEntity();
            Entity dmger = (Entity) ((Snowball) event.getDamager()).getShooter();
            if ((dmger instanceof Player)) {
                if (Skill.damageCheck((Player) dmger, entity)) {
                    Hero hero = plugin.getCharacterManager().getHero((Player) dmger);
                    addSpellTarget(entity, hero);
                    double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 4, false);
                    Skill.damageEntity(entity, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                }
                event.setCancelled(true);
            }
            FallingBlock block = (FallingBlock) event.getDamager().getMetadata("EarthTossProjectile");
            if (block.isValid()) {
                block.remove();
            }
        }
    }
}
