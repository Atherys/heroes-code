package com.atherys.skills.SkillEggBarrage;

/**
 * Created by Arthur on 5/16/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Player;
public class SkillEggBarrage extends ActiveSkill
{
    public SkillEggBarrage(Heroes plugin)
    {
        super(plugin, "EggBarrage");
        setDescription("Active\nYou will throw eggs for $1s");
        setUsage("/skill EggBarrage");
        setArgumentRange(0, 0);
        setIdentifiers("skill eggbarrage");
        setTypes(SkillType.SILENCABLE);
    }
    @Override
    public ConfigurationSection getDefaultConfig()
    {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set("interval", Integer.valueOf(100));
        return node;
    }
    @Override
    public String getDescription(Hero hero)
    {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false)/1000;
        String description = getDescription().replace("$1", duration + "");
        return description + Lib.getSkillCostStats(hero, this);
    }
    @Override
    public SkillResult use(Hero hero, String[] args)
    {
        Player player = hero.getPlayer();
        final long interval = SkillConfigManager.getUseSetting(hero, this, "interval", 100, true);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        EggEffect ee = new SkillEggBarrage.EggEffect(this, duration, interval, player);
        hero.addEffect(ee);
        return SkillResult.NORMAL;
    }
    public class EggEffect extends PeriodicExpirableEffect {
        Player caster;
        public EggEffect(Skill skill, long duration, long interval, Player caster) {
            super(skill, "EggEffect", interval , duration);
            this.caster = caster;
        }
        @Override
        public void applyToHero(Hero hero)
        {
            super.applyToHero(hero);
        }
        @Override
        public void tickHero(Hero hero)
        {
            this.caster.launchProjectile(Egg.class);
        }
        @Override
        public void removeFromHero(Hero hero)
        {
            super.removeFromHero(hero);
            broadcast(this.caster.getLocation(), "EggBarage Expired");
        }
        @Override
        public void tickMonster(Monster mnstr) {}
    }
}

