package com.atherys.skills.SkillElementalAura;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SkillElementalAura extends ActiveSkill {
    private String applytext;
    private String expiretext;

    public SkillElementalAura(Heroes plugin) {
        super(plugin, "ElementalAura");
        setDescription("Toggle\nChance to give user one of these auras: a static aura, slow aura, burning presence ( dot aura )");
        setUsage("/skill ElementalAura");
        setArgumentRange(0, 0);
        setIdentifiers("skill ElementalAura");
        setTypes(SkillType.FORCE, SkillType.DAMAGING, SkillType.HARMFUL,SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("stun-chance", 0.5);
        node.set("stun-duration", 1000);
        node.set("slow-multiplier", 2);
        node.set("slow-duration", 1000);
        node.set("damage", 10);
        node.set("eauramanacost", 10);
        node.set("eaurarad", 5);
        node.set("eauraperiod", 5000);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%' %skill% was cancelled!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%' %skill% was cancelled!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (hero.hasEffect("ElementalEffectAura")) {
            hero.removeEffect(hero.getEffect("ElementalEffectAura"));
            return SkillResult.NORMAL;
        }
        double schance = SkillConfigManager.getUseSetting(hero, this, "stun-chance", 0.5, false);
        long sduration = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 1000, false);
        int slmult = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 2, false);
        long slduration = SkillConfigManager.getUseSetting(hero, this, "slow-duration", 1000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, "damage", 10, false);
        int eauramanacost = SkillConfigManager.getUseSetting(hero, this, "eauramanacost", 10, false);
        int eaurarad = SkillConfigManager.getUseSetting(hero, this, "eaurarad", 5, false);
        long eauraperiod = SkillConfigManager.getUseSetting(hero, this, "eauraperiod", 5000, false);
        hero.addEffect(new ElementalEffectAura(this, schance, sduration, slmult, slduration, damage, eauramanacost, eaurarad, eauraperiod, hero.getPlayer()));
        return SkillResult.NORMAL;
    }

    public void manaCost(Hero hero, int manacost, Effect effect) {
        if (hero.getMana() > manacost) {
            hero.setMana(hero.getMana() - manacost);
        } else {
            hero.setMana(0);
            hero.removeEffect(effect);
        }
    }

    public class ElementalEffectAura extends PeriodicEffect {
        private Player caster;
        private double schance;
        private long sduration;
        private int slmult;
        private long slduration;
        private double damage;
        private int eauramanacost;
        private int eaurarad;
        private int identifier;

        public ElementalEffectAura(Skill skill, double schance, long sduration, int slmult, long slduration, double damage, int eauramanacost, int eaurarad, long eauraperiod, Player caster) {
            super(skill, "ElementalEffectAura", eauraperiod);
            this.schance = schance;
            this.sduration = sduration;
            this.slmult = slmult;
            this.slduration = slduration;
            this.damage = damage;
            this.eauramanacost = eauramanacost;
            this.eaurarad = eaurarad;
            this.caster = caster;
            this.identifier = 0;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "ElementalAura");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "ElementalAura");
        }

        @Override
        public void tickHero(Hero hero) {
            if (this.identifier == 0) {
                int tickDuration = (int) (slduration / 1000L) * 20;
                for (Entity e : caster.getNearbyEntities(eaurarad, eaurarad, eaurarad)) {
                    if (!(e instanceof LivingEntity)) {
                        continue;
                    }
                    LivingEntity l = (LivingEntity) e;
                    if (damageCheck(hero.getPlayer(), l)) {
                        l.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, tickDuration, slmult, false, false), false);
                        l.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, tickDuration, -slmult, false, false), false);
                    }
                }
                this.identifier = 1;
            } else if (this.identifier == 1) {
                for (Entity e : caster.getNearbyEntities(eaurarad, eaurarad, eaurarad)) {
                    if (!(e instanceof LivingEntity)) {
                        continue;
                    }
                    LivingEntity l = (LivingEntity) e;
                    if (damageCheck(hero.getPlayer(), l)) {
                        damageEntity(l, hero.getPlayer(), damage, DamageCause.MAGIC);
                    }
                }
                this.identifier = 2;
            } else if (this.identifier == 2) {
                for (Entity e : caster.getNearbyEntities(eaurarad, eaurarad, eaurarad)) {
                    if (!(e instanceof LivingEntity)) {
                        continue;
                    }
                    LivingEntity l = (LivingEntity) e;
                    if (damageCheck(hero.getPlayer(), l)) {
                        if (Util.nextRand() <= schance) {
                            plugin.getCharacterManager().getCharacter(l).addEffect(new StunEffect(skill, sduration));
                        }
                    }
                }
                this.identifier = 0;
            }

            if (eauramanacost > 0) {
                manaCost(hero, eauramanacost, this);
            }
        }
    }

    public class StunEffect extends PeriodicExpirableEffect {
        private static final long period = 100L;
        private Location loc;

        public StunEffect(Skill skill, long duration) {
            super(skill, "Stun", 100L, duration);
            this.types.add(EffectType.STUN);
            this.types.add(EffectType.HARMFUL);
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.DISABLE);
            this.addMobEffect(9, (int) (duration / 1000L) * 20, 127, false);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Messaging.send(hero.getPlayer(), "You are stunned!");
            this.loc = hero.getPlayer().getLocation();
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "You are no longer stunned!");
        }

        public void tickHero(Hero hero) {
            Location location = hero.getPlayer().getLocation();
            if (location != null) {
                if (location.getX() != this.loc.getX() || location.getY() != this.loc.getY() || location.getZ() != this.loc.getZ()) {
                    this.loc.setYaw(location.getYaw());
                    this.loc.setPitch(location.getPitch());
                    hero.getPlayer().teleport(this.loc);
                }

            }
        }

        public void tickMonster(Monster monster) {
        }
    }
}
