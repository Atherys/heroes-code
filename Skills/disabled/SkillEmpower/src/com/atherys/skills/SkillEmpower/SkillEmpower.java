package com.atherys.skills.SkillEmpower;

/**
 * Created by Arthur on 5/17/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
public class SkillEmpower extends ActiveSkill {
    private String applytext;
    private String expiretext;
    public SkillEmpower(Heroes plugin) {
        super(plugin, "Empower");
        setDescription("Active\n Increases you and your parties melee damage by $1% for 2%");
        setArgumentRange(0, 0);
        setUsage("/skill Empower");
        setIdentifiers("skill Empower");
        setTypes(SkillType.PHYSICAL,SkillType.SILENCABLE);
        Bukkit.getPluginManager().registerEvents(new DamageListener(), plugin);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.AMOUNT.name(), 1.2D);
        node.set(SkillSetting.APPLY_TEXT.node(), "Empower was activated!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "Empower has expired!");
        return node;
    }
    @Override
    public String getDescription(Hero hero) {
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 1.2, false);
        return getDescription().replace("$2",amount*100+"").replace("$2", d / 1000L + "") + Lib.getSkillCostStats(hero, this);
    }
    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 1.2, false);
        EmpowerEffect e = new EmpowerEffect(this, duration, amount);
        if (!hero.hasParty()) {
            hero.addEffect(e);
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        int radius = SkillConfigManager.getUseSetting(hero, this, "radius", 20, true);
        for (Hero phero : hero.getParty().getMembers()) {
            Player p = hero.getPlayer();
            if (phero.getPlayer().getLocation().distanceSquared(p.getLocation()) <= radius * radius) {
                phero.addEffect(e);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "You feel empowered");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You no longer feel empowered");
    }
    public class EmpowerEffect extends ExpirableEffect {
        private final double amount;
        public EmpowerEffect(Skill skill, long duration, double amount) {
            super(skill, "EmpowerEffect", duration);
            this.amount = amount;
            types.add(EffectType.BENEFICIAL);
        }
        public double getAmount() {
            return amount;
        }
        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + applytext);
        }
        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + expiretext);
        }
    }
    public class DamageListener implements Listener {
        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (event.isCancelled()) {
                return;
            }
            if (!event.getDamager().hasEffect("EmpowerEffect")) {
                return;
            }
            EmpowerEffect i = (EmpowerEffect) event.getDamager().getEffect("EmpowerEffect");
            event.setDamage(event.getDamage() * 1 + i.getAmount());
        }
    }
}

