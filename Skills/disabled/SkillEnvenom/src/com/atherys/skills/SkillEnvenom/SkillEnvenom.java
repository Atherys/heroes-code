package com.atherys.skills.SkillEnvenom;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import java.util.HashSet;
import java.util.Set;

public class SkillEnvenom extends ActiveSkill {
    private Set<Hero> partypeople = new HashSet<>();

    public SkillEnvenom(Heroes plugin) {
        super(plugin, "Envenom");
        setDescription("Active\n Partywide assassinsblade (needs new desc)");
        setUsage("/skill Envenom");
        setArgumentRange(0, 0);
        setIdentifiers("skill Envenom");
        setTypes(SkillType.BUFF,SkillType.SILENCABLE);
        Bukkit.getPluginManager().registerEvents(new HeroDamageListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 5000L);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.PERIOD.node(), 2500L);
        node.set("number-of-attacks", 1);
        node.set("dot-damage", 2.5D);
        node.set("dot-duration", 8000L);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2500, false);
        int attacks = SkillConfigManager.getUseSetting(hero, this, "number-of-attacks", 1, false);
        double dotDamage = SkillConfigManager.getUseSetting(hero, this, "dot-damage", 2D, false);
        long dotDuration = (long) SkillConfigManager.getUseSetting(hero, this, "dot-duration", 5000L, false);
        if (hero.hasParty()) {
            partypeople.add(hero);
            //hero.addEffect(new EnvenomEffect(this, duration, period, attacks, dotDamage, dotDuration));
            //Redundant because the following for loop would apply the effect to the caster
            for (Hero x : hero.getParty().getMembers()) {
                if ((x.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= radius * radius)) {
                    x.addEffect(new EnvenomEffect(this, duration, period, attacks, dotDamage, dotDuration));
                    partypeople.add(x);
                }
            }
        } else {
            hero.addEffect(new EnvenomEffect(this, duration, period, attacks, dotDamage, dotDuration));
            partypeople.add(hero);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    private class EnvenomEffect extends ExpirableEffect {
        private int attacks;
        private double dotDamage;
        private long dotDuration;
        private long period;

        public EnvenomEffect(Skill skill, long duration, long period, int attacks, double dotDamage, long dotDuration) {
            super(skill, "EnvenomEffect", duration);
            this.attacks = attacks;
            this.dotDamage = dotDamage;
            this.dotDuration = dotDuration;
            this.period = period;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Envenom expired.");
            partypeople.remove(hero);
        }

        public int getAttacksLeft() {
            return attacks;
        }

        public void setAttacksLeft(int AttacksLeft) {
            this.attacks = AttacksLeft;
        }

        public double getDotDamage() {
            return dotDamage;
        }

        public long getDotDuration() {
            return dotDuration;
        }

        public long getPeriod() {
            return period;
        }
    }

    public class HeroDamageListener implements Listener {
        private final Skill skill;

        public HeroDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if (!(event.getDamager() instanceof Hero) ||
                    (event.isCancelled()) ||
                    !(event.getEntity() instanceof Player) ||
                    (event.getDamage() == 0) ||
                    (event.getCause() != DamageCause.ENTITY_ATTACK)) {
                return;
            }
            Hero target = plugin.getCharacterManager().getHero((Player) event.getEntity());
            Hero hero = (Hero) event.getDamager();
            if (!partypeople.contains(hero)) {
                return;
            }
            if (!hero.hasEffect("EnvenomEffect"))
                return;
            EnvenomEffect he = (EnvenomEffect) hero.getEffect("EnvenomEffect");
            target.addEffect(new PeriodicDamageEffect(skill, "EnvenomDoTEffect", he.getPeriod(), he.getDotDuration(), he.getDotDamage(), hero.getPlayer(), false));
            getAttacksLeft(hero, he);
        }

        private void getAttacksLeft(Hero hero, EnvenomEffect he) {
            if (he.getAttacksLeft() <= 1) {
                hero.removeEffect(hero.getEffect("EnvenomEffect"));
            } else {
                he.setAttacksLeft(he.getAttacksLeft() - 1);
            }
        }
    }
}
