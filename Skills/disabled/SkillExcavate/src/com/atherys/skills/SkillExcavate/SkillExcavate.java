package com.atherys.skills.SkillExcavate;

/**
 * Created by Arthur on 5/17/2015.
 */
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
public class SkillExcavate extends ActiveSkill {
    private String applyText;
    private String expireText;
    public SkillExcavate(Heroes plugin) {
        super(plugin, "Excavate");
        setDescription("Active\n You gain a increased digging speed, and instant breaking of dirt for $1 seconds.");
        setUsage("/skill excavate");
        setArgumentRange(0, 0);
        setIdentifiers("skill excavate");
        setTypes(SkillType.BUFF, SkillType.EARTH, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillBlockListener(), plugin);
    }
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-multiplier", Integer.valueOf(2));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(0));
        node.set(SkillSetting.DURATION_INCREASE.node(), Integer.valueOf(100));
        node.set("apply-text", "%hero% begins excavating!");
        node.set("expire-text", "%hero% is no longer excavating!");
        return node;
    }
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, "apply-text", "%hero% begins excavating!").replace("%hero%", "$1");
        this.expireText = SkillConfigManager.getRaw(this, "expire-text", "%hero% is no longer excavating!").replace("%hero%", "$1");
    }
    public SkillResult use(Hero hero, String[] args) {
        broadcastExecuteText(hero);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 0, false);
        duration += SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 100, false) * hero.getSkillLevel(this);
        int multiplier = SkillConfigManager.getUseSetting(hero, this, "speed-multiplier", 2, false);
        if (multiplier > 20) {
            multiplier = 20;
        }
        hero.addEffect(new ExcavateEffect(this, duration, multiplier));
        return SkillResult.NORMAL;
    }
    private boolean isExcavatable(Material m) {
        switch (m.ordinal()) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return true;
        }
        return false;
    }
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 0, false);
        duration += SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 100, false) * hero.getSkillLevel(this);
        return getDescription().replace("$1", duration / 1000 + "") + Lib.getSkillCostStats(hero, this);
    }
    public class SkillBlockListener implements Listener {
        public SkillBlockListener() {
        }
        @EventHandler(priority = EventPriority.HIGHEST)
        public void onBlockDamage(BlockDamageEvent event) {
            if ((event.isCancelled()) || (!SkillExcavate.this.isExcavatable(event.getBlock().getType()))) {
                return;
            }
            if (event.getBlock().getType() == Material.BEDROCK){
                event.setCancelled(true);
                return;
            }
            Hero hero = SkillExcavate.this.plugin.getCharacterManager().getHero(event.getPlayer());
            if (!hero.hasEffect("Excavate")) {
                return;
            }
            event.setInstaBreak(true);
        }
    }
    public class ExcavateEffect extends ExpirableEffect {
        public ExcavateEffect(Skill skill, long duration, int amplifier) {
            super(skill, "Excavate", duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            addMobEffect(3, (int) (duration / 1000L) * 20, amplifier, false);
        }
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillExcavate.this.applyText, player.getDisplayName());
        }
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillExcavate.this.expireText, player.getDisplayName());
        }
    }
}

