package com.atherys.skills.SkillExecute;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillExecute extends TargettedSkill {
    private String killtext, failtext;

    public SkillExecute(Heroes plugin) {
        super(plugin, "Execute");
        setDescription("Targeted\n Kills the target if target is below $1 percent health. Otherwise fails the execution and starts the cooldown.");
        setUsage("/skill Execute");
        setArgumentRange(0, 0);
        setIdentifiers("skill Execute");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player))
            return SkillResult.INVALID_TARGET;
        Player target = (Player) le;
        double d = SkillConfigManager.getUseSetting(hero, this, "percent-hp-for-kill", 0.15, false);
        if ((target.getHealth() / target.getMaxHealth()) > d) {
            broadcast(hero.getPlayer().getLocation(), failtext, hero.getName(), target.getName());
            return SkillResult.NORMAL;
        }
        damageEntity(target, hero.getPlayer(), 1000D, DamageCause.VOID);
        broadcast(hero.getPlayer().getLocation(), killtext, target.getName(), hero.getName());
        return SkillResult.CANCELLED;
    }

    @Override
    public void init() {
        super.init();
        failtext = SkillConfigManager.getRaw(this, "fail-text", "%hero% tried to execute %target%, but failed!").replace("%hero%", "$1").replace("%target%", "$2");
        killtext = SkillConfigManager.getRaw(this, "kill-text", "%target% was executed by %hero%!").replace("%target%", "$1").replace("%hero%", "$2");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("percent-hp-for-kill", 0.15);
        node.set("fail-text", "%hero% tried to execute %target%, but failed!");
        node.set("kill-text", "%target% was executed by %hero%!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, "percent-hp-for-kill", 0.15, false);
        return getDescription().replace("$1", "" + damage) + Lib.getSkillCostStats(hero, this);
    }
}
