package com.atherys.skills.SkillFeast;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillFeast extends TargettedSkill {
    public SkillFeast(Heroes plugin) {
        super(plugin, "Feast");
        setDescription("Deals 30 physical damage to the target and restores some of the caster's hunger"); //
        setUsage("/skill feast");
        setArgumentRange(0, 0);
        setIdentifiers("skill feast");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL, SkillType.DARK);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        target.damage(30D);
        Player player = hero.getPlayer();
        player.setFoodLevel(player.getFoodLevel() + 4);
        return SkillResult.NORMAL;
    }
}
