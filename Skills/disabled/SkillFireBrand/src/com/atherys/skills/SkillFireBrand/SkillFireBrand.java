package com.atherys.skills.SkillFireBrand;

/**
 * Created by Arthur on 5/17/2015.
 */

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.party.HeroParty;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillFireBrand extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillFireBrand(Heroes plugin)
    {
        super(plugin, "FireBrand");
        setDescription("Toggleable Passive\n Enemies hit are set on fire for $1 seconds.");
        setTypes(SkillType.PHYSICAL, SkillType.FIRE, SkillType.SILENCABLE);
        setArgumentRange(0, 0);
        setIdentifiers("skill firebrand");
        Bukkit.getServer().getPluginManager().registerEvents(new WeaponDamageListener(this), plugin);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("fire-ticks", 500);
        node.set("on-text", "%hero% starts %skill%!");
        node.set("off-text", "%hero% stops %skill%!");
        return node;
    }



    @Override
    public String getDescription(Hero hero) {
        int f = SkillConfigManager.getUseSetting(hero, this, "fire-ticks", 50, false);
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, "on-text", "%hero% starts %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, "off-text", "%hero% stops %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("FireBrand")) {
            hero.removeEffect(new FireBrandEffect(this, "FireBrand"));
            return SkillResult.NORMAL;
        }
        hero.addEffect(new FireBrandEffect(this, "FireBrand"));
        return SkillResult.NORMAL;
    }

    public class FireBrandEffect extends Effect
    {

        public FireBrandEffect(Skill skill, String name)
        {
            super(skill, name);

        }

        @Override
        public void applyToHero(Hero hero)
        {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "FireBrand");
        }

        @Override
        public void removeFromHero(Hero hero)
        {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "FireBrand");
        }

    }
    public class WeaponDamageListener implements Listener {
        private final Skill skill;
        public WeaponDamageListener(Skill skill) {
            this.skill = skill;
        }
        @EventHandler(priority = EventPriority.HIGHEST)
        public void onWeaponDamage(WeaponDamageEvent event) {

            if (event.isCancelled() || (event.getDamage() == 0 || event.getCause() != DamageCause.ENTITY_ATTACK) || (!(event.getEntity() instanceof LivingEntity)) || (!(event.getDamager() instanceof Hero)))
            {

                return;
            }



            Hero hero = (Hero) event.getDamager();

            if (event.getEntity() instanceof Player)
            {
                Player p = (Player) event.getEntity();
                HeroParty party = hero.getParty();
                if ((party != null) && (party.isPartyMember(p))) return;

            }


            if (hero.hasEffect("FireBrand"))
            {
                event.getEntity().setFireTicks(SkillConfigManager.getUseSetting(hero, skill, "fire-ticks", 500, false));
            }
        }
    }
}
