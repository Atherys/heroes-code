package com.atherys.skills.SkillFireEyes;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillFireEyes extends ActiveSkill {
    public SkillFireEyes(Heroes plugin) {
        super(plugin, "FireEyes");
        setDescription("Active\n Lights all visible enemies on fire for $1s");
        setUsage("/skill FireEyes");
        setArgumentRange(0, 0);
        setIdentifiers("skill fireeyes");
        setTypes(SkillType.FORCE, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(20));
        node.set("degrees", Float.valueOf(45));
        node.set("fire-ticks", Integer.valueOf(100));
        node.set("place-dot-buff", Boolean.valueOf(true));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000L));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(2));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        List<Entity> nearbyEntities = new ArrayList<Entity>();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 20, false);
        Player player = hero.getPlayer();
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if ((entity instanceof LivingEntity)) {
                if (damageCheck(player, (LivingEntity)entity)) {
                    nearbyEntities.add(entity);
                }
            }
        }
        Vector playerstartloc = player.getLocation().toVector();
        float r2 = (float) radius;
        float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
        int fireticks = SkillConfigManager.getUseSetting(hero, this, "fire-ticks", 100, false);
        Vector dir = player.getLocation().getDirection();
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        long duration = fireticks * 50;
        double tickdamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
        for (Entity e : Lib.getEntitiesInCone(nearbyEntities, playerstartloc, r2, degrees, dir)) {
            if ((e instanceof Player)) {
                plugin.getCharacterManager().getHero((Player) e).addEffect(new DamageEffect(this, period, duration, tickdamage, player));
                e.setFireTicks(fireticks);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class DamageEffect extends PeriodicDamageEffect {
        public DamageEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "defireeyes", period, duration, tickDamage, applier);
        }
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        return description;
    }
}
