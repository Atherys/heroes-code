package com.atherys.skills.SkillFireShield;

/**
 * Created by Arthur on 5/17/2015.
 */
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
public class SkillFireShield extends PassiveSkill {
    public SkillFireShield(Heroes plugin) {
        super(plugin, "FireShield");
        setDescription("Passive\n Chance $1% to set attacker on fire!");
        setArgumentRange(0, 0);
        setTypes(SkillType.BUFF, SkillType.FIRE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillDamageListener(this), plugin);
    }
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("fire-ticks", Integer.valueOf(500));
        node.set("chance", Double.valueOf(0.2D));
        return node;
    }
    @Override
    public String getDescription(Hero hero) {
        double chance = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE.node(), 0.2D, false) * 100.0D);
        return getDescription().replace("$1", chance + "");
    }
    public class SkillDamageListener implements Listener {
        private final Skill skill;
        public SkillDamageListener(Skill skill) {
            this.skill = skill;
        }
        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (event.getDamage() == 0) || (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) || (!(event.getEntity() instanceof Player)) || (!(event instanceof EntityDamageByEntityEvent))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            if (!(subEvent.getDamager() instanceof LivingEntity)) {
                return;
            }
            Player player = (Player) event.getEntity();
            LivingEntity damager = (LivingEntity) subEvent.getDamager();
            Hero hero = SkillFireShield.this.plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect(SkillFireShield.this.getName())) {
                int ft = SkillConfigManager.getUseSetting(hero, this.skill, "fire-ticks", 500, false);
                double chance = SkillConfigManager.getUseSetting(hero, this.skill, "chance", 0.2D, false);
                Random random = new Random();
                if (random.nextDouble() <= chance && ft > damager.getFireTicks()) {
                    damager.setFireTicks(ft);
                }
            }
        }
    }
}

