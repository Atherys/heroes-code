package com.atherys.skills.SkillFrostBurn;


import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillFrostBurn extends ActiveSkill {

    public SkillFrostBurn(Heroes plugin) {
        super(plugin, "FrostBurn");
        setDescription("Active\n You shoot a ball of Frost that deals $1 magic damage and slows for $2");
        setUsage("/skill FrostBurn");
        setArgumentRange(0, 0);
        setIdentifiers("skill FrostBurn");
        setTypes(SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 4);
        node.set("velocity-multiplier", 1.5);
        node.set("slow-duration", 5000);
        node.set("slow-multiplier", 2);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Snowball fireball = player.launchProjectile(Snowball.class);
        fireball.setMetadata("FrostBurnFireball", new FixedMetadataValue(plugin, true));
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 1.5D, false);
        fireball.setVelocity(fireball.getVelocity().multiply(mult));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 1, false);
        damage += (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0.0D, false) * hero.getSkillLevel(this));
        long d = SkillConfigManager.getUseSetting(hero, this, "slow-duration", 5000, false);
        return getDescription().replace("$1", damage + "").replace("$2", d / 1000 + "") + Lib.getSkillCostStats(hero, this);
    }

    public class SkillEntityListener implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        /*
         * @EventHandler(priority = EventPriority.LOW) public void
         * onProjectileHit(ProjectileHitEvent event){ if (!(event.getEntity()
         * instanceof Snowball))return; Snowball sb =
         * (Snowball)event.getEntity(); if (!FrostBurnmap.containsKey(sb)) {
         * return; } Block b = sb.getLocation().getBlock();
         * b.getWorld().playEffect(b.getLocation(), Effect.POTION_BREAK, 2);
         *
         * }
         */
        @EventHandler(priority = EventPriority.NORMAL)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            if (!(subEvent.getDamager() instanceof Snowball)) {
                return;
            }
            Entity projectile = subEvent.getDamager();
            if (!projectile.hasMetadata("FrostBurnFireball")) {
                return;
            }

            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            Entity dmger = (Entity) ((Snowball) projectile).getShooter();
            if ((dmger instanceof Player)) {
                Hero hero = plugin.getCharacterManager().getHero((Player) dmger);
                addSpellTarget(entity, hero);
                double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 4, false);
                damageEntity(entity, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                entity.getWorld().playEffect(entity.getLocation(), Effect.POTION_BREAK, 2);
                long d = SkillConfigManager.getUseSetting(hero, skill, "slow-duration", 5000, false);
                int b = SkillConfigManager.getUseSetting(hero, skill, "slow-multiplier", 2, false);
                plugin.getCharacterManager().getCharacter(entity).addEffect(new SlowEffect(skill, d, b, true, "$1 was slowed by $2", "$1 is no longer slowed.", hero));
                event.setCancelled(true);
            }
        }
    }
}