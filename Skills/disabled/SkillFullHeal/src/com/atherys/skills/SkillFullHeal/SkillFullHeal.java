package com.atherys.skills.SkillFullHeal;

/**
 * Created by Arthur on 5/17/2015.
 */

import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillFullHeal extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillFullHeal(Heroes plugin) {
        super(plugin, "FullHeal");
        setDescription("Active\n Completely heals you over $1 if you dont take damage.");
        setUsage("/skill fullheal");
        setArgumentRange(0, 0);
        setIdentifiers("skill fullheal");
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(), plugin);
        setTypes(SkillType.HEAL, SkillType.SILENCABLE);
    }

    public String getDescription(Hero hero) {
        long duration = (long) ((SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0, false) * hero.getSkillLevel(this)) / 1000L);
        duration = duration > 0L ? duration : 0L;
        String description = getDescription().replace("$1", duration + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set("duration-increase", Integer.valueOf(0));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(1000));
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% begins healing!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero% is completely healed!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getUseSetting(null, this, SkillSetting.APPLY_TEXT.node(), "%hero% begins healing!").replace("%hero%", "$1");
        this.expireText = SkillConfigManager.getUseSetting(null, this, SkillSetting.EXPIRE_TEXT.node(), "%hero% is completely healed!").replace("%hero%", "$1");
    }

    public SkillResult use(Hero hero, String[] args) {
        long duration = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false) + SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0.0D, false) * hero.getSkillLevel(this));
        Player player = hero.getPlayer();
        duration = duration > 0L ? duration : 0L;
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 1000, false);
        double multiplier = duration / period;
        double amount = (hero.resolveMaxHealth() - player.getHealth()) / multiplier;
        amount = amount * multiplier < hero.resolveMaxHealth() - player.getHealth() ? amount + 1 : amount;
        if (amount > 0) {
            FullHealEffect cEffect = new FullHealEffect(this, period, duration, amount, hero.getPlayer());
            hero.addEffect(cEffect);
            return SkillResult.NORMAL;
        }
        return SkillResult.INVALID_TARGET;
    }

    public class SkillEntityListener implements Listener {
        public SkillEntityListener() {
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (event.getDamage() < 1) || (!(event.getEntity() instanceof Player))) {
                return;
            }
            Hero hero = SkillFullHeal.this.plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (hero.hasEffect("FullHeal"))
                hero.removeEffect(hero.getEffect("FullHeal"));
        }
    }

    public class FullHealEffect extends PeriodicHealEffect {
        private final double amount;

        public FullHealEffect(Skill skill, long period, long duration, double amount, Player caster) {
            super(skill, "FullHeal", period, duration, amount, caster);
            this.types.add(EffectType.HEAL);
            this.types.add(EffectType.BENEFICIAL);
            this.amount = amount;
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), SkillFullHeal.this.applyText, hero.getPlayer().getDisplayName());
        }

        public void tickHero(Hero hero) {
            super.tickHero(hero);
            HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(hero, this.amount, this.skill);
            this.plugin.getServer().getPluginManager().callEvent(hrhEvent);
            if (hrhEvent.isCancelled()) {
                return;
            }
            Player player = hero.getPlayer();
            hero.heal(player.getHealth() + hrhEvent.getAmount());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), SkillFullHeal.this.expireText, hero.getPlayer().getDisplayName());
        }
    }
}

