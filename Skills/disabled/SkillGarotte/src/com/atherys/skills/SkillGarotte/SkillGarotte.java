package com.atherys.skills.SkillGarotte;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillGarotte extends TargettedSkill {
    public SkillGarotte(Heroes plugin) {
        super(plugin, "Garotte");
        setDescription(" As you foe struggles to ready his defenses, you make a subtle gesture in the air. A noose formed of shadow drops from above him, wraps around his throat, and hoists him aloft.");
        setUsage("/skill Garotte");
        setArgumentRange(0, 0);
        setIdentifiers("skill Garotte");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
        Bukkit.getPluginManager().registerEvents(new HoldListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("max-damage", 50);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        if (!((target.getLocation().getBlock().getRelative(BlockFace.UP, 3).getType() == Material.AIR)
                && (target.getLocation().getBlock().getRelative(BlockFace.UP, 4).getType() == Material.AIR))) {
            return SkillResult.CANCELLED;
        }
        final long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1000L, false);
        final Hero ct = (Hero) plugin.getCharacterManager().getCharacter(target);
        final Skill skill = this;
        final int maxdamage = SkillConfigManager.getUseSetting(hero, skill, "max-damage", 50, false);
        target.teleport(target.getLocation().add(0, 3, 0));
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                ct.addEffect(new HoldEffect(skill, duration, maxdamage));
            }
        }, 10L);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1000L, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class HoldEffect extends PeriodicExpirableEffect {
        private final int maxdamage;
        private Location loc;
        private double damage = 0;

        public HoldEffect(Skill skill, long duration, int maxdamage) {
            super(skill, "GarotteEffect", 100L, duration);
            types.add(EffectType.STUN);
            types.add(EffectType.HARMFUL);
            types.add(EffectType.PHYSICAL);
            types.add(EffectType.DISABLE);
            this.maxdamage = maxdamage;
            addMobEffect(9, (int) (duration / 1000L) * 20, 127, false);
        }

        public double getDamage() {
            return this.damage;
        }

        public int getMaxDamage() {
            return this.maxdamage;
        }

        public void addDamage(double damage) {
            this.damage = +damage;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            loc = hero.getPlayer().getLocation();
            broadcast(player.getLocation(), "$1 is stunned!", player.getDisplayName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), "$1 is no longer stunned!", player.getDisplayName());
        }

        @Override
        public void tickHero(Hero hero) {
            Location location = hero.getPlayer().getLocation();
            if (location == null) {
                return;
            }
            if ((location.getX() != loc.getX()) || (location.getY() != loc.getY()) || (location.getZ() != loc.getZ())) {
                loc.setYaw(location.getYaw());
                loc.setPitch(location.getPitch());
                hero.getPlayer().teleport(loc);
            }
        }

        @Override
        public void tickMonster(Monster monster) {
        }
    }

    public class HoldListener implements Listener {
        @EventHandler
        public void onPlayerDamage(EntityDamageEvent event) {
            if (event.isCancelled() || !(event.getEntity() instanceof Player) || event.getDamage() == 0) {
                return;
            }
            Hero h = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (!h.hasEffect("GarotteEffect")) {
                return;
            }
            HoldEffect e = (HoldEffect) h.getEffect("GarotteEffect");
            e.addDamage(event.getDamage());
            if (e.getDamage() >= e.getMaxDamage()) {
                h.removeEffect(e);
            }
        }
    }
}