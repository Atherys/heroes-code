package com.atherys.skills.SkillGiftThatGives;

/**
 * Created by Arthur on 5/17/2015.
 */
import com.atherys.heroesaddon.util.*; import com.atherys.effects.*; import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
public class SkillGiftThatGives extends ActiveSkill {
    public SkillGiftThatGives(Heroes plugin) {
        super(plugin, "GiftThatGives");
        setDescription("Active\n Your next melee attack will pass along all of your current diseases to your target.");
        setUsage("/skill GiftThatGives");
        setArgumentRange(0, 0);
        setIdentifiers("skill GiftThatGives");
        setTypes(SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL);
        Bukkit.getServer().getPluginManager().registerEvents(new DamageListener(), plugin);
    }
    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("MasochismEffect")) {
            MasochismEffect m = (MasochismEffect) hero.getEffect("MasochismEffect");
            if (m.getEffects().isEmpty()) {
                hero.getPlayer().sendMessage(ChatColor.GRAY + "You cant use this skill now");
                return SkillResult.CANCELLED;
            }
            broadcastExecuteText(hero);
            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
            hero.addEffect(new gtgEffect(this, duration));
            return SkillResult.NORMAL;
        }
        hero.getPlayer().sendMessage(ChatColor.GRAY + "You cant use this skill now.");
        return SkillResult.CANCELLED;
    }
    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
    public class gtgEffect extends ExpirableEffect {
        public gtgEffect(Skill skill, long duration) {
            super(skill, "gtgEffect", duration);
        }
        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + skill.getName() + " expired.");
        }
    }
    public class DamageListener implements Listener {
        @EventHandler
        public void onweapondamage(WeaponDamageEvent event) {
            if (event.isCancelled()) {
                return;
            }
            if (!(event.getDamager() instanceof Hero)) {
                return;
            }
            Hero hero = (Hero) event.getDamager();
            if (hero.hasEffect("gtgEffect") && (hero.hasEffect("MasochismEffect")) && (event.getEntity() instanceof Player)) {
                Hero thero = plugin.getCharacterManager().getHero((Player) event.getEntity());
                MasochismEffect m = (MasochismEffect) hero.getEffect("MasochismEffect");
                for (Effect effect : m.getEffects()) {
                    if (m.canBeApplied(effect)) {
                        if (thero.hasEffect(effect.getName())){
                            if (effect instanceof Disaster){
                                Disaster e = (Disaster)effect;
                                e.setPeriodicDamage(e.getPeriodicDamage()*2);
                                thero.addEffect(e);
                                hero.removeEffect(effect);
                                continue;
                            }
                        }
                        thero.addEffect(effect);
                        m.removeFromList(effect);
                        hero.removeEffect(effect);
                    }
                }
                hero.removeEffect(hero.getEffect("gtgEffect"));
            }
        }
    }
}

