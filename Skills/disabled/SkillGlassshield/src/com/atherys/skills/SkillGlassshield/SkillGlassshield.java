package com.atherys.skills.SkillGlassshield;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class SkillGlassshield extends ActiveSkill {
    public String applyText;
    public String expireText;

    public SkillGlassshield(Heroes plugin) {
        super(plugin, "Glassshield");
        setDescription("Active\n Create moving shield of glass for $1-$2s");
        setUsage("/skill glassshield");
        setArgumentRange(0, 0);
        setIdentifiers("skill glassshield");
        setTypes(SkillType.BUFF, SkillType.COUNTER, SkillType.SILENCABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        long addDur = SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false) * hero.getSkillLevel(this);
        long minDur = SkillConfigManager.getUseSetting(hero, this, "min_duration", 20000, false) + addDur;
        long maxDur = SkillConfigManager.getUseSetting(hero, this, "max_duration", 40000, false) + addDur;
        String description = getDescription().replace("$1", minDur + "").replace("$2", maxDur + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("min_duration", 20000);
        node.set("max_duration", 40000);
        node.set("duration-increase", 0);
        node.set("apply-text", "%hero% uses the air to create a %skill%!");
        node.set("expire-text", "%hero%s %skill% fades as the wind dies down!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getUseSetting(null, this, "apply-text", "%hero% uses the air to create a %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getUseSetting(null, this, "expire-text", "%hero%s %skill% fades as the wind dies down!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        long addDur = SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false) * hero.getSkillLevel(this);
        long minDuration = SkillConfigManager.getUseSetting(hero, this, "min_duration", 20000, false) + addDur;
        if (minDuration < 1000 || minDuration > 60000) {
            minDuration = 20000;
        }
        long maxDuration = SkillConfigManager.getUseSetting(hero, this, "max_duration", 40000, false) + addDur;
        if (maxDuration > 60000) {
            maxDuration = 40000;
        }
        if (maxDuration < minDuration) {
            maxDuration = minDuration;
        }
        long randDuration = maxDuration - minDuration;
        long duration = (long) ((Math.random() * randDuration) + minDuration);
        broadcastExecuteText(hero);
        Messaging.send(player, "Duration " + duration / 1000 + "s");
        GlassshieldEffect gEffect = new GlassshieldEffect(this, "Glassshield", duration);
        hero.addEffect(gEffect);
        return SkillResult.NORMAL;
    }

    public class GlassshieldEffect extends PeriodicExpirableEffect {
        private boolean seed;
        private Block prevRefBlock;
        private ArrayList<Block> glassList = new ArrayList<Block>();

        public GlassshieldEffect(Skill skill, String name, long duration) {
            super(skill, name, 200, duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.ICE);
            this.types.add(EffectType.LIGHT);
            seed = true;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "Glassshield");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "Glassshield");
            updateShield(null, seed);
        }

        @Override
        public void tickHero(Hero hero) {
            if (!hero.hasEffect("Glassshield")) {
                return;
            }
            updateShield(hero.getPlayer().getLocation().getBlock(), seed);
            seed = !seed;
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //:P I'm really tired
        }

        private void updateShield(Block refBlock, boolean seed) {
            if (prevRefBlock != null) {
                for (int x = -3; x < 3; x++) {
                    for (int y = -3; y < 3; y++) {
                        for (int z = -2; z < 4; z++) {
                            replaceGlass(retrieveBlock(prevRefBlock, x, y, z));
                        }
                    }
                }
                fixGlass();
            }
            if (refBlock != null) {
                if (seed) {

                    for (int x = -3; x < 3; x++) {
                        for (int y = -3; y < 3; y++) {
                            for (int z = -2; z < 4; z++) {
                                replaceIfAir(retrieveBlock(refBlock, x, y, z), Material.AIR);
                                replaceIfAir(retrieveBlock(refBlock, x, y, z), Material.GLASS);
                            }
                        }
                    }
                }
            }
            prevRefBlock = refBlock;
        }

        /**
         * Retrieves the block at the relative co-ordinates.
         *
         * @param referenceBlock, relativeX, relativeY, relativeZ
         * @return Block
         */
        private void replaceIfAir(Block checkBlock, Material replaceWith) {
            if (checkBlock.getType() == Material.GLASS) {
                glassList.add(checkBlock);
            }
            if (checkBlock.getType() == Material.AIR) {
                checkBlock.setType(replaceWith);
            }
        }

        /**
         * Sets the block material to AIR if the block was GLASS.
         *
         * @param referenceBlock
         * @return Block
         */
        private void replaceGlass(Block checkBlock) {
            if (checkBlock.getType() == Material.GLASS) {
                checkBlock.setType(Material.AIR);
            }
        }

        /**
         * Corrects missing GLASS from the replaceGlass Method
         *
         * @param referenceBlock
         * @return Block
         */
        private void fixGlass() {
            for (int i = 0; i < glassList.size(); i++) {
                glassList.get(i).setType(Material.GLASS);
            }
            glassList.clear();
        }

        private Block retrieveBlock(Block refBlock, int relX, int relY, int relZ) {
            Block returnBlock = refBlock;
            while (0 < Math.abs(relX)) {
                if (relX > 0) {
                    returnBlock = returnBlock.getRelative(BlockFace.NORTH);
                    relX--;
                } else {
                    returnBlock = returnBlock.getRelative(BlockFace.SOUTH);
                    relX++;
                }
            }
            while (0 < Math.abs(relY)) {
                if (relY > 0) {
                    returnBlock = returnBlock.getRelative(BlockFace.EAST);
                    relY--;
                } else {
                    returnBlock = returnBlock.getRelative(BlockFace.WEST);
                    relY++;
                }
            }
            while (0 < Math.abs(relZ)) {
                if (relZ > 0) {
                    returnBlock = returnBlock.getRelative(BlockFace.UP);
                    relZ--;
                } else {
                    returnBlock = returnBlock.getRelative(BlockFace.DOWN);
                    relZ++;
                }
            }
            return returnBlock;
        }
    }
}