package com.atherys.skills.SkillGlimpseDeath;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillGlimpseDeath extends TargettedSkill {
    private String applytext, expiretext;

    public SkillGlimpseDeath(Heroes plugin) {
        super(plugin, "GlimpseDeath");
        setDescription("Caster ports to target entity. First in front, then behind, and deals $1 damage. After damage is dealt, caster will be back at their starting position.");
        setUsage("/skill GlimpseDeath");
        setArgumentRange(0, 0);
        setIdentifiers("skill GlimpseDeath");
        setTypes(SkillType.BUFF, SkillType.SILENCABLE, SkillType.TELEPORT, SkillType.HARMFUL);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player)) {
            return SkillResult.CANCELLED;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 40, false);
        Location loc = hero.getPlayer().getLocation();
        hero.addEffect(new GlimpseDeathEffect(this, duration, (Player) le, loc, damage));
        return SkillResult.NORMAL;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill% on %target%.").replace("%hero%", "$1").replace("%skill%", "$2").replace("%target%", "$3");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%skill% - %hero% -%target%").replace("%hero%", "$2").replace("%skill%", "$1").replace("%target%", "$3");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.DAMAGE.node(), 40);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill% on %target%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% - %hero% -%target%");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 40, false);
        return getDescription().replace("$2", "" + duration / 1000).replace("$1", +damage + "");
    }

    public class GlimpseDeathEffect extends PeriodicExpirableEffect {
        private final Player target;
        private int tick = 0;
        private final Location castloc;
        private final double damage;

        public GlimpseDeathEffect(Skill skill, long duration, Player target, Location castloc, double damage) {
            super(skill, "GlimpseDeath", duration / 3, duration);
            this.target = target;
            this.damage = damage;
            this.castloc = castloc;
            types.add(EffectType.DARK);
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            damageEntity(target, hero.getPlayer(), damage, DamageCause.ENTITY_ATTACK);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "GlimpseDeath", target.getName());
        }

        @Override
        public void tickHero(Hero hero) {
            switch (tick) {
                case 0:
                    Location location = target.getLocation().toVector().add(target.getLocation().getDirection().multiply(2)).toLocation(target.getWorld());
                    location.setPitch(target.getLocation().getPitch());
                    location.setYaw(target.getLocation().getYaw() - 180);
                    Block b = location.getBlock();
                    hero.getPlayer().teleport(location);
                    tick++;
                    break;
                case 1:
                    Location loc = target.getLocation().toVector().add(target.getLocation().getDirection().multiply(-1)).toLocation(target.getWorld());
                    loc.setYaw(target.getLocation().getYaw());
                    loc.setPitch(target.getLocation().getPitch());
                    Block bb = loc.getBlock();
                    hero.getPlayer().teleport(loc);
                    tick++;
                    break;
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "GlimpseDeath", target.getName());
            hero.getPlayer().teleport(castloc);
        }
    }
}
