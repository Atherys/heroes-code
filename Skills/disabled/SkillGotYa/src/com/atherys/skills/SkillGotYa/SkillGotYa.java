package com.atherys.skills.SkillGotYa;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.common.SafeFallEffect;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

public class SkillGotYa extends TargettedSkill {
    public SkillGotYa(Heroes plugin) {
        super(plugin, "GotYa");
        setDescription("Targeted\n Launches the user backwards and deals $1 physical damage to their target.");
        setUsage("/skill GotYa");
        setArgumentRange(0, 0);
        setIdentifiers("skill gotya");
        setTypes(SkillType.HARMFUL, SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        int v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 10, false);
        int v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 10, false);
        long dur = SkillConfigManager.getUseSetting(hero, this, "safefall-duration", 10, false);
        hero.addEffect(new SafeFallEffect(this, dur));
        addSpellTarget(target, hero);
        damageEntity(target, player, damage, DamageCause.ENTITY_ATTACK);
        Vector v = player.getLocation().add(0, 1, 0).toVector().subtract(target.getLocation().toVector()).normalize();
        v = v.multiply(v1);
        v = v.setY(v2);
        player.setVelocity(v);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        return getDescription().replace("$1", damage + "") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set("vertical-vector", Integer.valueOf(10));
        node.set("horizontal-vector", Integer.valueOf(10));
        node.set("safefall-duration", Integer.valueOf(10));
        return node;
    }
}
