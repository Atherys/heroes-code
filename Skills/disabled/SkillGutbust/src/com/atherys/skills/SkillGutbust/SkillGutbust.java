package com.atherys.skills.SkillGutbust;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SkillGutbust extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillGutbust(Heroes plugin) {
        super(plugin, "Gutbust");
        setDescription("Targetted\n You deal $1 magic damage to a target and empty their stomach for $2 seconds.");
        setUsage("/skill Gutbust");
        setArgumentRange(0, 0);
        setIdentifiers("skill Gutbust");
        setTypes(SkillType.BUFF, SkillType.SILENCABLE);
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% starts vomiting!").replace("%target%", "$1");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% is no longer feeling ill!").replace("%target%", "$1");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 20000L);
        node.set("dot-damage", 4);
        node.set("hunger-amplifier", 12);
        node.set("hunger-duration", 6000L);
        node.set(SkillSetting.PERIOD.node(), 2000L);

        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long dotDuration = (long) SkillConfigManager.getUseSetting(hero, this, "dot-duration", 5000L, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 35, false);
        return getDescription().replace("$1", damage + "").replace("$2", dotDuration / 1000 + "");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] strings) {
        if (!damageCheck(hero.getPlayer(), target)) {
            return SkillResult.INVALID_TARGET;
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        long hungerDuration = SkillConfigManager.getUseSetting(hero, this, "hunger-duration", 6000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, "dot-damage", 4, false);
        int amplifier = SkillConfigManager.getUseSetting(hero, this, "hunger-amplifier", 24, false);
        if (target instanceof Player) {
            Hero tHero = this.plugin.getCharacterManager().getHero((Player) target);
            ((Player) target).setFoodLevel((int) (((Player) target).getFoodLevel() * 0.9));
            ((Player) target).setSaturation(0);
            tHero.addEffect(new GutbustEffect(this, duration, period, damage, hero.getPlayer(), amplifier, hungerDuration, true));
        } else {
            this.plugin.getCharacterManager().getMonster(target).addEffect(new GutbustEffect(this, duration, period, damage, hero.getPlayer(), amplifier, hungerDuration, false));
        }
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    private class GutbustEffect extends PeriodicDamageEffect {

        public GutbustEffect(Skill skill, long duration, long period, double damage, Player caster, int hungerAmplifier, long hungerDuration, boolean isPlayer) {
            super(skill, "GutbustEffect", period, duration, damage, caster);
            if (isPlayer) {
                int tickDuration = (int) (hungerDuration / 1000L) * 20;
                addMobEffect(17, tickDuration, hungerAmplifier, false);
            }
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            hero.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 1));
            broadcast(hero.getPlayer().getLocation(), applyText, hero.getPlayer().getName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expireText, hero.getPlayer().getName());
        }
    }
}
