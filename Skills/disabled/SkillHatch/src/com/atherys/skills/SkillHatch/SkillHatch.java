package com.atherys.skills.SkillHatch;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.Random;

/*
 * Made by pumpapa
 */
public class SkillHatch extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillHatch(Heroes plugin) {
        super(plugin, "Hatch");
        setArgumentRange(0, 0);
        setDescription("Spawns $1-$2 spiders on the target and deals $3 damage for every spider after $4 seconds.");
        setIdentifiers("skill hatch");
        setUsage("/skill hatch <target>");
        setTypes(SkillType.DAMAGING, SkillType.HARMFUL, SkillType.SILENCABLE, SkillType.SUMMON);
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) / 1000;
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
        int maxspiders = SkillConfigManager.getUseSetting(hero, this, "max-spiders", 3, false);
        int minspiders = SkillConfigManager.getUseSetting(hero, this, "min-spiders", 1, false);
        String description = getDescription().replace("$1", "" + minspiders).replace("$2", "" + maxspiders).replace("$3", "" + (int) damage).replace("$4", "" + duration);
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10));
        node.set("food-loss-percentage", Integer.valueOf(50));
        node.set(SkillSetting.APPLY_TEXT.node(), "$1 has been filled with Spider Eggs!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "Spiders started erupting out of $1!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 has been filled with Spider Eggs!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "Spiders started erupting out of $1!");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player) || !(target instanceof Player) || !(damageCheck(hero.getPlayer(), target)))
            return SkillResult.INVALID_TARGET;
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);
        int maxspiders = SkillConfigManager.getUseSetting(hero, this, "max-spiders", 3, false);
        int minspiders = SkillConfigManager.getUseSetting(hero, this, "min-spiders", 1, false);
        targetHero.addEffect(new HatchEffect(this, duration, damage, minspiders, maxspiders, applyText, expireText, player));
        damageEntity(target, player, damage);
        return SkillResult.NORMAL;
    }

    public class HatchEffect extends ExpirableEffect {
        protected Player caster;
        private final double damage;
        private final String applyText;
        private final String expireText;
        private final int minspiders;
        private final int maxspiders;

        public HatchEffect(Skill skill, long duration, double damage, int minspiders, int maxspiders, String applyText, String expireText, Player caster) {
            super(skill, "HatchEffect", duration);
            this.caster = caster;
            this.damage = damage;
            this.applyText = applyText;
            this.expireText = expireText;
            this.minspiders = minspiders;
            this.maxspiders = maxspiders;
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.HARMFUL);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if (this.isExpired()) {
                double totalDamage = 0D;
                int spiders = new Random().nextInt((maxspiders - minspiders) + 1) + minspiders;
                for (int i = 0; i < spiders; i++) {
                    CaveSpider cs = (CaveSpider) player.getWorld().spawnEntity(player.getEyeLocation(), EntityType.CAVE_SPIDER);
                    cs.setTarget(player);
                    Monster mcs = plugin.getCharacterManager().getMonster(cs);
                    mcs.setExperience(0);
                    totalDamage += damage;
                }
                damageEntity(player, caster, totalDamage);
                broadcast(player.getLocation(), this.expireText, player.getDisplayName());
            }
        }
    }
}