 package com.atherys.skills.SkillHerbalism;

 import com.herocraftonline.heroes.Heroes;
 import com.herocraftonline.heroes.characters.Hero;
 import com.herocraftonline.heroes.characters.effects.EffectType;
 import com.herocraftonline.heroes.characters.skill.*;
 import com.herocraftonline.heroes.listeners.HBlockListener;
 import com.herocraftonline.heroes.util.Util;
 import org.bukkit.Bukkit;
 import org.bukkit.Material;
 import org.bukkit.block.Block;
 import org.bukkit.configuration.ConfigurationSection;
 import org.bukkit.event.EventHandler;
 import org.bukkit.event.EventPriority;
 import org.bukkit.event.Listener;
 import org.bukkit.event.block.BlockBreakEvent;
 import org.bukkit.inventory.ItemStack;

 public class SkillHerbalism extends PassiveSkill
 {
   public SkillHerbalism(Heroes plugin)
   {
     super(plugin, "Herbalism");
     setDescription("You have a $1% chance to harvest extra herbs, fruits, and vegetables.");
     setEffectTypes(EffectType.BENEFICIAL);
     setTypes(SkillType.KNOWLEDGE, SkillType.EARTH, SkillType.BUFF);
     Bukkit.getServer().getPluginManager().registerEvents(new SkillBlockListener(this), plugin);
   }
   public ConfigurationSection getDefaultConfig()
   {
     ConfigurationSection node = super.getDefaultConfig();
     node.set(SkillSetting.CHANCE_LEVEL.node(), Double.valueOf(0.001D));
     return node;
   }
   public String getDescription(Hero hero)
   {
     double chance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE_LEVEL, 0.001D, false);
     int level = hero.getSkillLevel(this);
     if (level < 1) {
       level = 1;
     }
     return getDescription().replace("$1", Util.stringDouble(chance * level * 100.0D));
   }
   public class SkillBlockListener
     implements Listener
   {
     private final Skill skill;
     SkillBlockListener(Skill skill)
     {
       this.skill = skill;
     }
     @EventHandler(priority=EventPriority.MONITOR)
     public void onBlockBreak(BlockBreakEvent event) {
       if (event.isCancelled()) {
         return;
       }
       Block block = event.getBlock();
       if (HBlockListener.placedBlocks.containsKey(block.getLocation())) {
         return;
       }
       int extraDrops = 0;
       Material mat = null;
       switch (block.getType().ordinal()) {
       case 1:
         extraDrops = 3;
         mat = Material.WHEAT;
         break;
       case 2:
         mat = Material.SUGAR_CANE;
         extraDrops = 2;
         break;
       case 3:
         mat = Material.MELON;
         extraDrops = 7;
         break;
       case 4:
       case 5:
       case 6:
       case 7:
       case 8:
       case 9:
       case 10:
       case 11:
       case 12:
       case 13:
         break;
       default:
         return;
       }
       Hero hero = SkillHerbalism.this.plugin.getCharacterManager().getHero(event.getPlayer());
       if ((!hero.hasEffect("Herbalism")) || (Util.nextRand() >= SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.CHANCE_LEVEL, 0.001D, false) * hero.getSkillLevel(this.skill))) {
         return;
       }
       if (extraDrops != 0)
         extraDrops = Util.nextInt(extraDrops) + 1;
       else {
         extraDrops = 1;
       }
       if (mat != null)
         block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(mat, extraDrops));
       else
         block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(block.getType(), extraDrops, (short)block.getData()));
     }
   }
 }
