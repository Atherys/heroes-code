package com.atherys.skills.SkillIceAura;

import com.atherys.effects.*; import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillIceAura extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillIceAura(Heroes plugin) {
        super(plugin, "IceAura");
        setDescription("Toggle-able passive aoe damage around self that slows targets in radius.");
        setUsage("/skill iceaura");
        setArgumentRange(0, 0);
        setIdentifiers("skill iceaura");
        setTypes(SkillType.ICE, SkillType.SILENCABLE, SkillType.HARMFUL, SkillType.BUFF);
    }

    public String getDescription(Hero hero) {
        int damage = (int) (SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1.0D, false) + SkillConfigManager.getUseSetting(hero, this, "tick-damage-increase", 0.0D, false) * hero.getSkillLevel(this));
        damage = damage > 0 ? damage : 0;
        int radius = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "radius-increase", 0.0D, false) * hero.getSkillLevel(this));
        radius = radius > 1 ? radius : 1;
        String description = getDescription().replace("$1", damage + "").replace("$2", radius + "");
        return description;
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("on-text", "%hero% chills the air with their %skill%!");
        node.set("off-text", "%hero% stops their %skill%!");
        node.set("tick-damage", Integer.valueOf(1));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(1000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, "on-text", "%hero% chills the air with their %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, "off-text", "%hero% stops their %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    public SkillResult use(Hero hero, String[] args) {
        if (hero.hasEffect("IceAura")) {
            hero.removeEffect(hero.getEffect("IceAura"));
        } else {
            long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD.node(), 1000, false);
            double tickDamage = (SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1.0D, false));
            tickDamage = tickDamage > 0 ? tickDamage : 0;
            int range = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 1.0D, false));
            range = range > 1 ? range : 1;
            hero.addEffect(new IcyAuraEffect(this, period, tickDamage, range, 3000));
        }
        return SkillResult.NORMAL;
    }

    public class IcyAuraEffect extends PeriodicEffect {
        private double tickDamage;
        private int range;
        private double duration;
        private Skill skill;

        public IcyAuraEffect(SkillIceAura skill, long period, double tickDamage, int range, int duration) {
            super(skill, "IceAura", period);
            this.skill = skill;
            this.tickDamage = tickDamage;
            this.range = range;
            this.duration = duration;
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.ICE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "IceAura");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "IceAura");
        }

        @Override
        public void tickHero(Hero hero) {
            super.tickHero(hero);
            Player player = hero.getPlayer();
            for (Entity entity : player.getNearbyEntities(range, range, range)) {
                if (((entity instanceof LivingEntity)) && (SkillIceAura.damageCheck(player, (LivingEntity) entity))) {
                    CharacterTemplate character = plugin.getCharacterManager().getCharacter((LivingEntity) entity);
                    if ((character instanceof Hero)) {
                        ((Hero) character).cancelDelayedSkill();
                    }
                    SkillIceAura.this.addSpellTarget(entity, hero);
                    SkillIceAura.damageEntity(character.getEntity(), player, tickDamage, EntityDamageEvent.DamageCause.MAGIC);
                    SlowEffect effect = new SlowEffect(skill, (int) duration, 2, true, "", "", hero);
                    character.addEffect(effect);
                }
            }
        }
    }
}
