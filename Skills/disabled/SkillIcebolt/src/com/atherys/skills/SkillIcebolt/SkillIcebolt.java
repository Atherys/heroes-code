package com.atherys.skills.SkillIcebolt;

import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillIcebolt extends ActiveSkill {

    private String applyText;
    private String expireText;

    public SkillIcebolt(Heroes plugin) {
        super(plugin, "Icebolt");
        setDescription("You launch a ball of ice that deals $1 damage to your target and slows them for $2 seconds.");
        setUsage("/skill icebolt");
        setArgumentRange(0, 0);
        setIdentifiers("skill icebolt");
        setTypes(SkillType.ICE, SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(3));
        node.set("slow-duration", Integer.valueOf(5000));
        node.set("speed-multiplier", Integer.valueOf(2));
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% has been slowed by %hero%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% is no longer slowed!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% has been slowed by %hero%!").replace("%target%", "$1").replace("%hero%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% is no longer slowed!").replace("%target%", "$1");
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Snowball snowball = player.launchProjectile(Snowball.class);
        snowball.setMetadata("IceboltSnowball", new FixedMetadataValue(plugin, true));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        return getDescription().replace("$1", damage + "").replace("$2", duration / 1000 + "");
    }

    public class SkillEntityListener
            implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof Snowball)) || (!projectile.hasMetadata("IceboltSnowball"))) {
                return;
            }
            Entity dmger = (Entity) ((Snowball) subEvent.getDamager()).getShooter();
            if ((dmger instanceof Player)) {
                Hero hero = SkillIcebolt.this.plugin.getCharacterManager().getHero((Player) dmger);
                if (!Skill.damageCheck((Player) dmger, (LivingEntity) event.getEntity())) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setFireTicks(0);
                double damage = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.DAMAGE, 3, false);
                long duration = SkillConfigManager.getUseSetting(hero, this.skill, "slow-duration", 10000, false);
                int amplifier = SkillConfigManager.getUseSetting(hero, this.skill, "speed-multiplier", 2, false);
                SlowEffect iceSlowEffect = new SlowEffect(this.skill, duration, amplifier, false, SkillIcebolt.this.applyText, SkillIcebolt.this.expireText, hero);
                LivingEntity target = (LivingEntity) event.getEntity();
                SkillIcebolt.this.plugin.getCharacterManager().getCharacter(target).addEffect(iceSlowEffect);
                SkillIcebolt.this.addSpellTarget(event.getEntity(), hero);
                Skill.damageEntity(target, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                event.setCancelled(true);
            }
        }
    }
}