package com.atherys.skills.SkillIgnite;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillIgnite extends ActiveSkill {
    public SkillIgnite(Heroes plugin) {
        super(plugin, "Ignite");
        setDescription("Sets everyone that isn't party on fire.");
        setUsage("/skill ignite");
        setArgumentRange(0, 0);
        setIdentifiers("skill Ignite");
        setTypes(SkillType.FIRE, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("fire-ticks", Integer.valueOf(100));
        node.set("place-dot-buff", Boolean.valueOf(true));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000L));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(2));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int ft = SkillConfigManager.getUseSetting(hero, this, "fire-ticks", 100, false);
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if (!(entity instanceof Player)) {
                continue;
            }
            Player tplayer = (Player) entity;
            if (damageCheck(tplayer, player)) {
                Hero thero = plugin.getCharacterManager().getHero(tplayer);
                if (SkillConfigManager.getUseSetting(hero, this, "place-dot-buff", false)) {
                    long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
                    long duration = ft * 50;
                    double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 2, false);
                    thero.addEffect(new Damageeffect(this, period, duration, damage, hero.getPlayer()));
                }
                tplayer.setFireTicks(ft);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class Damageeffect extends PeriodicDamageEffect {
        public Damageeffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "deignite", period, duration, tickDamage, applier);
        }
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}