package com.atherys.skills.SkillInspired;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;

public class SkillInspired extends PassiveSkill {
	public SkillInspired(Heroes plugin) {
		super(plugin, "Inspired");
		setDescription("When you are near your obelisk you gain nightvision, might, and bolster.");
	}
	@Override
	public String getDescription(Hero hero) {
		return getDescription() + Lib.getSkillCostStats(hero, this);
	}
}
