package com.atherys.skills.SkillInvis;

import com.atherys.effects.BetterInvis;
import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.util.Lib;
import com.atherys.heroesaddon.util.ParticleEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillInvis extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillInvis(Heroes plugin) {
        super(plugin, "Invis");
        setDescription("You completely disappear from view.");
        setUsage("/skill invis");
        setArgumentRange(0, 0);
        setIdentifiers("skill invis");
        setNotes("Note: Taking damage removes the effect");
        setTypes(SkillType.ILLUSION, SkillType.BUFF, SkillType.COUNTER, SkillType.STEALTHY, SkillType.SILENCABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(20000));
        node.set(SkillSetting.APPLY_TEXT.node(), "You turn invisible!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You reappeared!");
        return node;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "You turn invisible!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You reappeared");
    }

    public SkillResult use(Hero hero, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        Player player = hero.getPlayer();
        ParticleEffect.SMOKE_NORMAL.display(0, 0, 0, 0, 1, player.getLocation(), HeroesAddon.PARTICLE_RANGE);
        hero.addEffect(new BetterInvis(this, duration, this.applyText, this.expireText, hero));
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
