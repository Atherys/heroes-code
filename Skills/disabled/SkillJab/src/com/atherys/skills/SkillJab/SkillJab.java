package com.atherys.skills.SkillJab;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillJab extends TargettedSkill {
	private String applyText;
	private String expireText;
	public SkillJab(Heroes plugin) {
		super(plugin, "Jab");
		setArgumentRange(0, 0);
		setDescription("Target is afflicted with nausea dealing $1 damage over $2 seconds");
		setIdentifiers("skill jab");
		setUsage("/skill jab");
		setTypes(SkillType.DAMAGING, SkillType.DARK, SkillType.HARMFUL, SkillType.SILENCABLE);
	}
	@Override
	public String getDescription(Hero hero) {
		int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false) / 1000;
		int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) / 1000;
		int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 5, false) * (duration/period);
		String description = getDescription().replace("$2", duration + "");
		description = description.replace("$1", damage + "");
		// COOLDOWN
		int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
		if (cooldown > 0) {
			description += " CD:" + cooldown + "s";
		}
		// MANA
		int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
		if (mana > 0) {
			description += " M:" + mana;
		}
		// HEALTH_COST
		int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this));
		if (healthCost > 0) {
			description += " HP:" + healthCost;
		}
		// STAMINA
		int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
		if (staminaCost > 0) {
			description += " FP:" + staminaCost;
		}
		// DELAY
		int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
		if (delay > 0) {
			description += " W:" + delay + "s";
		}
		// EXP
		int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
		if (exp > 0) {
			description += " XP:" + exp;
		}
		return description;
	}
	@Override
	public ConfigurationSection getDefaultConfig() {
		ConfigurationSection node = super.getDefaultConfig();
		node.set(SkillSetting.DURATION.node(), Integer.valueOf(6000));
		node.set(SkillSetting.PERIOD.node(), Integer.valueOf(2000));
		node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(5));
		node.set(SkillSetting.APPLY_TEXT.node(), "$1 is feeling ill!");
		node.set(SkillSetting.EXPIRE_TEXT.node(), "$1 is no longer feeling ill!");
		return node;
	}
	@Override
	public void init() {
		super.init();
		this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 is feeling ill!");
		this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 is no longer feeling ill!");
	}
	@Override
	public SkillResult use(Hero hero, LivingEntity target, String[] args) {
		Player player = hero.getPlayer();
		if (target.equals(player) || !(target instanceof Player) || !(damageCheck(hero.getPlayer(), target)))
			return SkillResult.INVALID_TARGET;
		Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
		long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, false);
		long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
		double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 5, false);
		targetHero.addEffect(new JabEffect(this,duration,period,damage,applyText,expireText,player));
		return SkillResult.NORMAL;
	}
	public class JabEffect extends PeriodicDamageEffect {
		private final String applyText;
		private final String expireText;
		public JabEffect(Skill skill, long duration, long period, double tickDamage, String applyText, String expireText, Player applier) {
			super(skill, "JabEffect", period, duration, tickDamage, applier);
			this.applyText = applyText;
			this.expireText = expireText;
			this.types.add(EffectType.PHYSICAL);
			this.types.add(EffectType.HARMFUL);
			this.types.add(EffectType.BLEED);
			int tickDuration = (int)(duration / 1000L) * 20;
            addMobEffect(9, tickDuration, 200, false);
            addMobEffect(17, tickDuration, 10, false);
		}
		public void applyToHero(Hero hero) {
			super.applyToHero(hero);
			Player player = hero.getPlayer();
			broadcast(player.getLocation(), this.applyText, player.getDisplayName());
		}
		public void removeFromHero(Hero hero) {
			super.removeFromHero(hero);
			Player player = hero.getPlayer();
			broadcast(player.getLocation(), this.expireText, player.getDisplayName());
		}
	}
}
