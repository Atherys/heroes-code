package com.atherys.skills.SkillJudge;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillJudge extends TargettedSkill {
    public SkillJudge(Heroes plugin) {
        super(plugin, "Judge");
        setDescription("The higher your target's hp, the more damage this spell will do.");
        setUsage("/skill Judge");
        setArgumentRange(0, 0);
        setIdentifiers("skill Judge");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage-per-1-hp", Double.valueOf(0.07D));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Player tplayer = (Player) target;
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET;
        }
        double d = SkillConfigManager.getUseSetting(hero, this, "damage-per-1-hp", 0.07, false);
        damageEntity(tplayer, hero.getPlayer(), tplayer.getHealth() * d);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
}