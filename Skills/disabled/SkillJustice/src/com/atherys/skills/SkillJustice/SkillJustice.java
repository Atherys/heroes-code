package com.atherys.skills.SkillJustice;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Collections;
import java.util.List;

public class SkillJustice extends TargettedSkill {
    public SkillJustice(Heroes plugin) {
        super(plugin, "Justice");
        setDescription("Deals $1 physical damage of Swift Justice to Target");
        setUsage("/skill Justice");
        setArgumentRange(0, 0);
        setIdentifiers("skill justice");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, "base-damage", 20, false);
        return getDescription().replace("$1", damage + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damages", Collections.<String>emptyList());
        node.set("base-damage", 20);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }
        double damage = 0;
        if ((target instanceof Player)) {
            String heroclass = plugin.getCharacterManager().getHero((Player) target).getHeroClass().getName();
            List<String> classes = SkillConfigManager.getUseSetting(hero, this, "damages", Collections.<String>emptyList());
            for (String s : classes) {
                String[] sp = s.split("-");
                if (sp[0].equalsIgnoreCase(heroclass)) {
                    damage = Integer.parseInt(sp[1]);
                    break;
                }
            }
        }
        if (damage == 0) {
            damage = SkillConfigManager.getUseSetting(hero, this, "base-damage", 20, false);
        }
        addSpellTarget(target, hero);
        damageEntity(target, player, damage, EntityDamageEvent.DamageCause.ENTITY_ATTACK);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
}