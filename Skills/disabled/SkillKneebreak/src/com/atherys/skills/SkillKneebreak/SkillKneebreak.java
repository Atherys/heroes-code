package com.atherys.skills.SkillKneebreak;

import com.atherys.effects.KneebreakEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillKneebreak extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillKneebreak(Heroes plugin) {
        super(plugin, "Kneebreak");
        setArgumentRange(0, 0);
        setDescription("Target is slowed and takes $1 damage over $2 seconds");
        setIdentifiers("skill kneebreak");
        setUsage("/skill kneebreak");
        setTypes(SkillType.DEBUFF, SkillType.HARMFUL, SkillType.SILENCABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE_TICK.node(), Integer.valueOf(1));
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.PERIOD.node(), Integer.valueOf(1000));
        node.set("speed-multiplier", Integer.valueOf(2));
        node.set(SkillSetting.APPLY_TEXT.node(), "$1 is Slowed and Bleeding!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "$1 is no longer Slowed and Bleeding!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 is Slowed and Bleeding!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 is no longer Slowed and Bleeding!");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player))
            return SkillResult.INVALID_TARGET;
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        if (hero.getParty() != null && hero.getParty().isPartyMember(targetHero))
            return SkillResult.INVALID_TARGET;
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        broadcastExecuteText(hero);
        targetHero.addEffect(new KneebreakEffect(this, duration, applyText, expireText));
        return SkillResult.NORMAL;
    }
}
