package com.atherys.skills.SkillLastResort;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillLastResort extends ActiveSkill {
    public SkillLastResort(Heroes plugin) {
        super(plugin, "LastResort");
        setDescription("Caster dies, but deals $1% health damage to all within a $2 block radius. This includes party members.");
        setUsage("/skill LastResort");
        setArgumentRange(0, 0);
        setIdentifiers("skill LastResort");
        setTypes(SkillType.EARTH, SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.25D, false);
        double damage = (int) (hero.getPlayer().getMaxHealth() * amount);
        for (Entity e : hero.getPlayer().getNearbyEntities(radius, radius, radius)) {
            if (e instanceof Player) {
                Player p = (Player) e;
                if (damageCheck(hero.getPlayer(), p)) {
                    damageEntity(p, hero.getPlayer(), damage, DamageCause.MAGIC);
                    continue;
                }
                Hero h = plugin.getCharacterManager().getHero(p);
                if (h.hasParty()) {
                    if (h.getParty().getMembers().contains(hero)) {
                        if (hero.getPlayer().getHealth() > hero.getPlayer().getHealth() - damage) {
                            h.getPlayer().setHealth((int) (h.getPlayer().getHealth() - hero.getPlayer().getHealth() * amount));
                        } else {
                            h.getPlayer().setHealth(0);
                        }
                    }
                }
            }
        }
        hero.getPlayer().setHealth(0);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.25D, false);
        return getDescription().replace("$1", amount * 100 + "").replace("$2", radius + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.AMOUNT.node(), 0.25D);
        return node;
    }
}