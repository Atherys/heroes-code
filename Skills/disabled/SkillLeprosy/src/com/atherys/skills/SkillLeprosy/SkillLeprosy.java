package com.atherys.skills.SkillLeprosy;

import com.atherys.effects.Disaster;
import com.atherys.effects.MasochismEffect;
import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.DisarmEffect;
import com.herocraftonline.heroes.characters.effects.common.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Random;

public class SkillLeprosy extends TargettedSkill {
    public SkillLeprosy(Heroes plugin){
        super(plugin, "Leprosy");
        setDescription("Causes your target to be either slowed, disarmed, stunned, confused or take damage over time for a duration.");
        setUsage("/skill Leprosy");
        setArgumentRange(0, 0);
        setIdentifiers("skill Leprosy");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL) ;
    }
    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        Player target = (Player)le;
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Hero thero = plugin.getCharacterManager().getHero(target);
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long duration = (long)SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long period = (long)SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000L, false);
        Random r = new Random();
        switch (r.nextInt(4)){
            case 0:
                SlowEffect eff = new SlowEffect(this, duration,2,true,"","",hero);
                addEffectToList(hero,eff,duration,thero);
                break;
            case 1:
                DisarmEffect d = new DisarmEffect(this, duration,"","");
                addEffectToList(hero,d,duration,thero);
                break;
            case 2:
                StunEffect s = new StunEffect(this,duration);
                addEffectToList(hero,s,duration,thero);
                break;                
            case 3:
                int drift = SkillConfigManager.getUseSetting(hero, this, "max-drift", 2, false);
                ConfuseEffect c = new ConfuseEffect(this,duration,period,drift);
                addEffectToList(hero,c,duration,thero); 
                break;
            case 4:
                LeprosyEffect dm = new LeprosyEffect(this,period,duration,damage,hero.getPlayer(),true);
                if (hero.hasEffect("MasochismEffect")) {
                   MasochismEffect me = (MasochismEffect)hero.getEffect("MasochismEffect");
                   hero.addEffect(new LeprosyEffect(this, period, duration, damage/2, hero.getPlayer(),false));
                   me.addToList(dm, duration);
                }                
                thero.addEffect(dm);
                break;
        }
        broadcastExecuteText(hero,target);
        return SkillResult.NORMAL;
    }
    public void addEffectToList(Hero hero, Effect e, long duration, Hero target){
        if (hero.hasEffect("MasochismEffect")){
            MasochismEffect me = (MasochismEffect)hero.getEffect("MasochismEffect");
            me.addToList(e,duration);
            hero.addEffect(e);
        }    
        target.addEffect(e);
    }
    @Override
    public ConfigurationSection getDefaultConfig()
    {
      ConfigurationSection node = super.getDefaultConfig();
      node.set(SkillSetting.DAMAGE.node(), 10);
      node.set(SkillSetting.DURATION.node(), 10000);
      node.set(SkillSetting.PERIOD.node(), 1000);
      node.set("max-drift", 2);
      return node;
    }
    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }    
    public class LeprosyEffect extends Disaster{
        //private final Player caster;
        public LeprosyEffect(Skill skill, long period,long duration, int damage, Player caster,boolean kb){
            super(skill, "LeprosyEffect", period, duration,damage,caster);
            //this.caster = caster;
            types.add(EffectType.DISPELLABLE);
        }
    }
  public class ConfuseEffect extends PeriodicExpirableEffect
  {
    private final float maxDrift;
    private final Random r = new Random();
    public ConfuseEffect(Skill skill, long duration, long period, float maxDrift)
    {
      super(skill,"Confuse", period, duration);
      this.maxDrift = maxDrift;
      this.types.add(EffectType.HARMFUL);
      this.types.add(EffectType.DISPELLABLE);
      this.types.add(EffectType.MAGIC);
      addMobEffect(9, (int)(duration / 1000L) * 20, 127, false);
    }
    public void adjustVelocity(LivingEntity lEntity) {
      Vector velocity = lEntity.getVelocity();
      float angle = r.nextFloat() * 2.0F * 3.14159F;
      float xAdjustment = maxDrift * (float) Math.cos(Math.toRadians(angle));
      float zAdjustment = maxDrift * (float) Math.sin(Math.toRadians(angle));
      velocity.add(new Vector(xAdjustment, 0.0F, zAdjustment));
      velocity.setY(0);
      lEntity.setVelocity(velocity);
    }
      @Override
    public void tickHero(Hero hero)
    {
      adjustVelocity(hero.getPlayer());
    }
        @Override
        public void tickMonster(Monster mnstr) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
  }    
 }
