package com.atherys.skills.SkillMagicMissile;

import com.atherys.heroesaddon.util.IDAPI;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.party.HeroParty;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;

public class SkillMagicMissile extends ActiveSkill {

    public SkillMagicMissile(Heroes plugin) {
        super(plugin, "MagicMissile");
        setDescription("Throw MagicMissile, which deals $1 + explosion damage. Explosion radius: $2.");
        setUsage("/skill MagicMissile");
        setArgumentRange(0, 0);
        setIdentifiers("skill MagicMissile");
        setTypes(SkillType.DAMAGING, SkillType.HARMFUL, SkillType.SILENCABLE);
        Bukkit.getPluginManager().registerEvents(new SkillMagicMissileListener(), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.REAGENT.node(), IDAPI.getIdOfMaterial(Material.ENDER_PEARL));
        node.set(SkillSetting.REAGENT_COST.node(), 1);
        node.set(SkillSetting.DAMAGE.node(), 15.0);
        node.set(SkillSetting.DAMAGE_INCREASE.node(), 0.3);
        node.set("explosion-radius", 3.0);
        node.set("fire", false);
        node.set("prevent-wilderness-block-damage-and-fire", true);
        node.set("damage-friendly", false);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        StringBuilder descr = new StringBuilder(getDescription()
                .replace("$1", String.valueOf(getDamage(hero)))
                .replace("$2", Util.stringDouble(getExplosionRadius(hero)))
        );
        if (!isDamageFriendly(hero)) {
            descr.append(" No friendly damage.");
        }
        int cd = getCooldown(hero);
        if (cd > 0) {
            descr.append(" CD:");
            descr.append(Util.stringDouble(cd / 1000.0));
            descr.append("s");
        }
        int mana = getMana(hero);
        if (mana > 0) {
            descr.append(" M:");
            descr.append(mana);
        }
        return descr.toString();
    }

    private double getDamage(Hero hero) {
        return (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 15.0, false)
                + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0.3, false) * hero.getSkillLevel(this));
    }

    private float getExplosionRadius(Hero hero) {
        return (float) SkillConfigManager.getUseSetting(hero, this, "explosion-radius", 3.0, false);
    }

    private boolean isFire(Hero hero) {
        return SkillConfigManager.getUseSetting(hero, this, "fire", false);
    }

    private boolean isNoGrief(Hero hero) {
        return SkillConfigManager.getUseSetting(hero, this, "prevent-wilderness-block-damage-and-fire", true);
    }

    private boolean isDamageFriendly(Hero hero) {
        return SkillConfigManager.getUseSetting(hero, this, "damage-friendly", false);
    }

    private int getCooldown(Hero hero) {
        return Math.max(0, SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 0, true)
                - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE, 0, false) * hero.getSkillLevel(this));
    }

    private int getMana(Hero hero) {
        return (int) Math.max(0.0, SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA, 0.0, true)
                - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE, 0.0, false) * hero.getSkillLevel(this));
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        EnderPearl MagicMissile = player.launchProjectile(EnderPearl.class);
        player.setMetadata("MagicMissileThrower", new FixedMetadataValue(plugin, true));
        MagicMissile.setMetadata("MagicMissileEnderPearl", new FixedMetadataValue(plugin, hero));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SkillMagicMissileListener implements Listener {
        /*
         * Ender bomb explosion detector
         * onProjectileHit, onEntityExplode and onEntityDamage calls in the same tick
         * onEntityExplode and onEntityDamage are close to each other
         */

        private long currentTick = 0;
        private Location currentLandingLoc;
        private Hero currentThrower;

        // enderpearl teleport fires before onProjectileHit, check if player throwed MagicMissile earlier
        @EventHandler(priority = EventPriority.HIGH)
        public void onPlayerTeleport(PlayerTeleportEvent event) {
            if (event.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
                if (event.getPlayer().hasMetadata("MagicMissileThrower")) {
                    event.setCancelled(true);
                }
            }
        }

        // fires before onProjectileHit
        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
            if (event.getRemover() instanceof Player) {
                Player remover = (Player) event.getRemover();
                if (remover.hasMetadata("MagicMissileThrower")) {
                    event.setCancelled(true);
                }
            }
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onProjectileHit(ProjectileHitEvent event) {
            if (event.getEntityType() == EntityType.ENDER_PEARL) {
                EnderPearl enderPearl = (EnderPearl) event.getEntity();
                if (enderPearl.hasMetadata("MagicMissileEnderPearl")) {
                    Hero thrower = (Hero) enderPearl.getMetadata("MagicMissileEnderPearl").get(0).value();
                    if (thrower.getPlayer().isOnline()) {
                        currentThrower = thrower;
                        currentTick = getFirstWorldTime();
                        currentLandingLoc = enderPearl.getLocation();
                        enderPearl.getWorld().createExplosion(currentLandingLoc, getExplosionRadius(thrower), isFire(thrower));
                    }
                    thrower.getPlayer().removeMetadata("MagicMissileThrower", plugin);
                    enderPearl.remove();
                }
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void onEntityExplode(BlockExplodeEvent event) {
            event.setCancelled(true);
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onEntityDamage(EntityDamageEvent event) {
            if (event.getCause() != EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) {
                return;
            }
            if (getFirstWorldTime() != currentTick) {
                return;
            }
            if (!(event.getEntity() instanceof LivingEntity)) {
                event.setCancelled(true); // do not damage items, etc
                return;
            }
            LivingEntity target = (LivingEntity) event.getEntity();
            if (currentLandingLoc.distanceSquared(target.getLocation()) > 625) {
                return; // not current MagicMissile explosion
            }
            if (isDamageFriendly(currentThrower) || !isFriendlyTo(target, currentThrower) && damageCheck(currentThrower.getPlayer(), target)) {
                damageEntity(target, currentThrower.getPlayer(), getDamage(currentThrower), EntityDamageEvent.DamageCause.MAGIC);
            } else {
                event.setCancelled(true);
            }
        }

        private long getFirstWorldTime() {
            return Bukkit.getWorlds().get(0).getFullTime();
        }

        private boolean isFriendlyTo(LivingEntity target, Hero hero) {
            Player player = hero.getPlayer();
            // self
            if (target.equals(player)) {
                return true;
            }
            Set<String> friendlyNames = new HashSet<String>();
            friendlyNames.add(player.getName());
            // party
            HeroParty party = hero.getParty();
            if (party != null) {
                if (target instanceof Player && party.isPartyMember((Player) target)) {
                    return true;
                } else {
                    for (Hero partyMember : party.getMembers()) {
                        friendlyNames.add(partyMember.getName());
                    }
                }
            }
            // pet
            if (target instanceof Tameable) {
                Tameable tameable = (Tameable) target;
                if (tameable.isTamed() && friendlyNames.contains(tameable.getOwner().getName())) {
                    return true;
                }
            }
            return false;
        }
    }
}
