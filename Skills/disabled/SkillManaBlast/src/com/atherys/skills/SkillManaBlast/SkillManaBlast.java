package com.atherys.skills.SkillManaBlast;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillManaBlast extends ActiveSkill {
    public SkillManaBlast(Heroes plugin) {
        super(plugin, "ManaBlast");
        setDescription("Causes enemies around caster to be knocked up and back and deals damage.");
        setUsage("/skill ManaBlast");
        setArgumentRange(0, 0);
        setIdentifiers("skill ManaBlast");
        setTypes(SkillType.DEBUFF, SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int v = SkillConfigManager.getUseSetting(hero, this, "velocity", 1, false);
        for (Entity e : hero.getPlayer().getNearbyEntities(radius, radius, radius)) {
            if (e instanceof LivingEntity) {
                LivingEntity li = (LivingEntity) e;
                if (damageCheck(hero.getPlayer(), li)) {
                    damageEntity(li, hero.getPlayer(), damage, DamageCause.MAGIC);
                    li.setVelocity(li.getLocation().add(0, v, 0).toVector().subtract(li.getLocation().toVector()).normalize());
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
