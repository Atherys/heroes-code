package com.atherys.skills.SkillManaDrain;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillManaDrain extends PassiveSkill {
    public SkillManaDrain(Heroes plugin) {
        super(plugin, "ManaDrain");
        setDescription("Passive $1 mana drain on attack. CD:$2");
        setTypes(SkillType.COUNTER, SkillType.BUFF, SkillType.MANA);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillHeroListener(this), plugin);
    }

    public String getDescription(Hero hero) {
        int drain = (int) (SkillConfigManager.getUseSetting(hero, this, "mana-drain-per-attack", 4, false) + SkillConfigManager.getUseSetting(hero, this, "drain-increase", 0.0D, false) * hero.getSkillLevel(this));
        drain = drain > 0 ? drain : 0;
        int cooldown = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 500, false) - SkillConfigManager.getUseSetting(hero, this, "cooldown-decrease", 0.0D, false) * hero.getSkillLevel(this)) / 1000;
        cooldown = cooldown > 0 ? cooldown : 0;
        String description = getDescription().replace("$1", drain + "").replace("$2", cooldown + "");
        return description;
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("mana-drain-per-attack", Integer.valueOf(4));
        node.set("drain-increase", Integer.valueOf(0));
        node.set(SkillSetting.COOLDOWN.node(), Integer.valueOf(500));
        node.set("cooldown-decrease", Integer.valueOf(0));
        node.set("exp-per-drain", Integer.valueOf(0));
        return node;
    }

    public class SkillHeroListener implements Listener {
        private Skill skill;

        public SkillHeroListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) || (event.isCancelled()) || (event.getDamage() == 0) || (!(event instanceof EntityDamageByEntityEvent)))
                return;
            EntityDamageByEntityEvent edby = (EntityDamageByEntityEvent) event;
            if (((edby.getDamager() instanceof Player)) && ((event.getEntity() instanceof Player))) {
                Player player = (Player) edby.getDamager();
                Hero hero = SkillManaDrain.this.plugin.getCharacterManager().getHero(player);
                if ((hero.hasEffect("ManaDrain")) && ((hero.getCooldown("ManaDrain") == null) || (hero.getCooldown("ManaDrain").longValue() <= System.currentTimeMillis()))) {
                    Hero tHero = SkillManaDrain.this.plugin.getCharacterManager().getHero((Player) event.getEntity());
                    int drain = (int) (SkillConfigManager.getUseSetting(hero, this.skill, "mana-drain-per-attack", 4, false) + SkillConfigManager.getUseSetting(hero, this.skill, "drain-increase", 0.0D, false) * hero.getSkillLevel(this.skill));
                    drain = drain > 0 ? drain : 0;
                    int cooldown = (int) (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN.node(), 500, false) - SkillConfigManager.getUseSetting(hero, this.skill, "cooldown-decrease", 0.0D, false) * hero.getSkillLevel(this.skill));
                    cooldown = cooldown > 0 ? cooldown : 0;
                    hero.setCooldown("ManaDrain", cooldown + System.currentTimeMillis());
                    if (tHero.getMana() - drain <= 0) {
                        tHero.setMana(0);
                    } else {
                        tHero.setMana(tHero.getMana() - drain);
                    }
                    if (hero.getMana() + drain >= 100) {
                        hero.setMana(100);
                    } else {
                        hero.setMana(hero.getMana() + drain);
                    }
                }
            } else if (((edby.getDamager() instanceof Projectile)) && ((((Projectile) edby.getDamager()).getShooter() instanceof Player))) {
                Player player = (Player) ((Projectile) edby.getDamager()).getShooter();
                Hero hero = SkillManaDrain.this.plugin.getCharacterManager().getHero(player);
                if ((hero.hasEffect("ManaDrain")) && ((hero.getCooldown("ManaDrain") == null) || (hero.getCooldown("ManaDrain").longValue() <= System.currentTimeMillis()))) {
                    Hero tHero = SkillManaDrain.this.plugin.getCharacterManager().getHero((Player) event.getEntity());
                    int drain = (int) (SkillConfigManager.getUseSetting(hero, this.skill, "mana-drain-per-attack", 4, false) + SkillConfigManager.getUseSetting(hero, this.skill, "drain-increase", 0.0D, false) * hero.getSkillLevel(this.skill));
                    drain = drain > 0 ? drain : 0;
                    int cooldown = (int) (SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN.node(), 500, false) - SkillConfigManager.getUseSetting(hero, this.skill, "cooldown-decrease", 0.0D, false) * hero.getSkillLevel(this.skill));
                    cooldown = cooldown > 0 ? cooldown : 0;
                    hero.setCooldown("ManaDrain", cooldown + System.currentTimeMillis());
                    if (tHero.getMana() - drain <= 0) {
                        tHero.setMana(0);
                    } else {
                        tHero.setMana(tHero.getMana() - drain);
                    }
                    if (hero.getMana() + drain >= 100) {
                        hero.setMana(100);
                    } else {
                        hero.setMana(hero.getMana() + drain);
                    }
                    return;
                }
            }
        }
    }
}
