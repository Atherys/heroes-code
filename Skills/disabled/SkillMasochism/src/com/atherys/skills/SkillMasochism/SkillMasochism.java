package com.atherys.skills.SkillMasochism;

import com.atherys.effects.MasochismEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillType;

public class SkillMasochism extends ActiveSkill {
    public SkillMasochism(Heroes plugin) {
        super(plugin, "Masochism");
        setDescription("Toggleable passive, their plaguebearer spells also apply to themselves for 1/2 damage.");
        setUsage("/skill Masochism");
        setArgumentRange(0, 0);
        setIdentifiers("skill Masochism");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("MasochismEffect")) {
            hero.removeEffect(hero.getEffect("MasochismEffect"));
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        hero.addEffect(new MasochismEffect(this));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
