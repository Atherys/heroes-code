package com.atherys.skills.SkillMassPositionSwap;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class SkillMassPositionSwap extends ActiveSkill {
    public SkillMassPositionSwap(Heroes plugin) {
        super(plugin, "MassPositionSwap");
        setDescription("Changes location of every player in $1 block around you.");
        setUsage("/skill MassPositionSwap");
        setArgumentRange(0, 0);
        setIdentifiers("skill masspositionswap");
        setTypes(SkillType.FORCE, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        return getDescription().replace("$1", radius + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        ArrayList<Player> nearbyPlayers = new ArrayList<>();
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false);
        Location loc = player.getLocation();
        if (hero.hasParty()) {
            for (Hero pHero : hero.getParty().getMembers()) {
                if (pHero.getPlayer().getLocation().distanceSquared(loc) <= radius * radius) {
                    Player pplayer = pHero.getPlayer();
                    nearbyPlayers.add(pplayer);
                }
            }
        }
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if (!(entity instanceof Player)) {
                continue;
            }
            Player newPlayer = (Player) entity;
            if (damageCheck(newPlayer, player)) {
                nearbyPlayers.add(newPlayer);
            }
        }
        Player p1, p2;
        for (int i = 0; i < nearbyPlayers.size(); i += 2) {
            if (nearbyPlayers.size() <= 1) {
                break;
            }
            if ((nearbyPlayers.size() % 2 == 1)) {
                nearbyPlayers.remove(nearbyPlayers.size() - 1);
            }
            try {
                p1 = nearbyPlayers.get(i);
                p2 = nearbyPlayers.get(i + 1);
            } catch (ArrayIndexOutOfBoundsException ex) {
                break;
            }
            Location temp = p2.getLocation();
            p2.teleport(p1.getLocation());
            p1.teleport(temp);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
