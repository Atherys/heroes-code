package com.atherys.skills.SkillMeld;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillMeld extends ActiveSkill {
    public SkillMeld(Heroes plugin) {
        super(plugin, "Meld");
        setDescription("In a certain level of darkness, the warden will be hidden when standing still");
        setUsage("/skill Meld");
        setArgumentRange(0, 0);
        setIdentifiers("skill Meld");
        setTypes(SkillType.ILLUSION, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("buff-duration", 50000);
        node.set("min-lightlevel", 5);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long d = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 50000, false);
        byte min = (byte) SkillConfigManager.getUseSetting(hero, this, "min-lightlevel", 5, false);
        hero.addEffect(new MeldEffect(this, d, min));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class MeldInvis extends ExpirableEffect {
        private final String applyText;
        private final String expireText;

        public MeldInvis(Skill skill, long duration, String applyText, String expireText) {
            super(skill, "MeldInvis", duration);
            this.applyText = applyText;
            this.expireText = expireText;
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.INVIS);
            this.types.add(EffectType.UNTARGETABLE_NO_MSG);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            for (Player onlinePlayer : plugin.getServer().getOnlinePlayers())
                if ((!onlinePlayer.equals(player)) && (!onlinePlayer.hasPermission("heroes.admin.seeinvis"))) {
                    onlinePlayer.hidePlayer(player);
                }
            if ((applyText != null) && (applyText.length() > 0))
                Messaging.send(player, applyText, player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            for (Player onlinePlayer : this.plugin.getServer().getOnlinePlayers())
                if (!onlinePlayer.equals(player)) {
                    onlinePlayer.showPlayer(player);
                }
            if ((expireText != null) && (expireText.length() > 0))
                Messaging.send(player, expireText, player.getDisplayName());
        }
    }

    public class MeldEffect extends PeriodicExpirableEffect {
        private final byte min;
        private Location loc;

        public MeldEffect(Skill skill, long duration, byte min) {
            super(skill, "MeldEffect", 1000L, duration);
            this.min = min;
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            loc = hero.getPlayer().getLocation();
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            double cX = hero.getPlayer().getLocation().getX();
            double cZ = hero.getPlayer().getLocation().getZ();
            if ((loc.getX() == cX) && (loc.getZ() == cZ) && (hero.getPlayer().getLocation().getBlock().getLightLevel() <= min)) {
                if (!hero.hasEffect("MeldInvis")) {
                    hero.addEffect(new MeldInvis(skill, getDuration(), "You vanish in a cloud of smoke!", "You reappeared"));
                }
            } else {
                loc = hero.getPlayer().getLocation();
                if (hero.hasEffect("MeldInvis"))
                    hero.removeEffect(hero.getEffect("MeldInvis"));
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }
    }
} 