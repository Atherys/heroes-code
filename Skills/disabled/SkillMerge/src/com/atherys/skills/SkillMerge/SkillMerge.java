package com.atherys.skills.SkillMerge;

import com.atherys.effects.BetterInvis;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillMerge extends ActiveSkill {
    private String applytext, expiretext;

    public SkillMerge(Heroes plugin) {
        super(plugin, "Merge");
        setDescription("While in a forest user may become one with the forest, entering a weak version of stealth but slowing them.");
        setUsage("/skill Merge");
        setArgumentRange(0, 0);
        setIdentifiers("skill merge");
        setTypes(SkillType.EARTH, SkillType.ILLUSION, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("leaves", 20);
        node.set("wood-blocks", 5);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.RADIUS.node(), 8);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 8, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        int l = SkillConfigManager.getUseSetting(hero, this, "leaves", 20, false);
        int w = SkillConfigManager.getUseSetting(hero, this, "wood-blocks", 5, false);
        int mult = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 2, false);
        int nl = 0;
        int wl = 0;
        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                for (int z = -radius; z <= radius; z++) {
                    int m = player.getLocation().getBlock().getRelative(x, y, z).getLocation().getBlock().getTypeId();
                    switch (m) {
                        case 17:
                            nl++;
                            break;
                        case 18:
                            wl++;
                            break;
                    }
                }
            }
        }
        if ((nl >= l) && (wl >= w)) {
            hero.addEffect(new BetterInvis(this, duration, "You blend into the trees!", "You reappeared", hero));
            hero.addEffect(new MergeEffect(this, duration, mult));
            return SkillResult.NORMAL;
        } else {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You must be in a Forest to use this skill!");
            return SkillResult.CANCELLED;
        }
    }

    public class MergeEffect extends ExpirableEffect {
        public MergeEffect(Skill skill, long duration, int multi) {
            super(skill, "MergeEffect", duration);
            int tickDuration = (int) (duration / 1000L) * 20;
            addMobEffect(2, tickDuration, multi, false);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Merge");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Merge");
        }
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}