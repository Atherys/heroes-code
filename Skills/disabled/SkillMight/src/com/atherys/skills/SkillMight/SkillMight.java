package com.atherys.skills.SkillMight;

import com.atherys.effects.MightEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillMight extends ActiveSkill {

    private String applyText;
    private String expireText;

    public SkillMight(Heroes plugin) {
        super(plugin, "Might");
        setDescription("Grants bonus damage to yourself and allies.");
        setUsage("/skill might");
        setArgumentRange(0, 0);
        setIdentifiers("skill might");
        setTypes(SkillType.BUFF, SkillType.SILENCABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        if (radius == 0)
            description = "Grants bonus damage to yourself.";

        //DURATION
        int duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false)) + (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false)) / 1000;
        if (duration > 0) {
            description += " Lasts: " + duration + "s";
        }
        //RADIUS
        if (radius > 0) {
            description += " Radius: " + radius + "m";
        }
        //COOLDOWN
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description += " CD: " + cooldown + "s";
        }
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.DURATION.node(), 30000);
        node.set(SkillSetting.APPLY_TEXT.node(), "Your muscles bulge with power!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "You feel strength leave your body!");
        node.set("give-damage", 10);
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "Your muscles bulge with power!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "You feel strength leave your body!");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, "give-damage", 10, false);
        long duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 30000, false)) + (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false));
        hero.addEffect(new MightEffect(this, duration, damage, applyText, expireText));
        if (hero.hasParty()) {
            for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
                if (e instanceof Player) {
                    LivingEntity le = (LivingEntity) e;
                    Hero chero = plugin.getCharacterManager().getHero((Player) le);
                    if (hero.getParty().isPartyMember(chero) && !chero.equals(hero)) {
                        chero.addEffect(new MightEffect(this, duration, damage, applyText, expireText));
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
