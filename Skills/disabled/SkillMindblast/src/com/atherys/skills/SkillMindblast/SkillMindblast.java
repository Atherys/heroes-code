package com.atherys.skills.SkillMindblast;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/*
 * Made by pumpapa
 */
public class SkillMindblast extends ActiveSkill {
    public SkillMindblast(Heroes plugin) {
        super(plugin, "Mindblast");
        setArgumentRange(0, 0);
        setDescription("Nauseates and slows enemies within $1 blocks for $2 seconds and drains $3 mana from them.");
        setIdentifiers("skill mindblast");
        setUsage("/skill mindblast");
        setTypes(SkillType.SILENCABLE, SkillType.DEBUFF);
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 15, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, false);
        int manadrain = SkillConfigManager.getUseSetting(hero, this, "manadrain", 15, false);
        return description.replace("$1", "" + r).replace("$2", "" + (duration / 1000)).replace("$3", "" + manadrain);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(15));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(6000));
        node.set("amplifier", Integer.valueOf(1));
        node.set("manadrain", Integer.valueOf(15));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 15, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 6000, false);
        int amplifier = SkillConfigManager.getUseSetting(hero, this, "amplifier", 1, false);
        int manadrain = SkillConfigManager.getUseSetting(hero, this, "manadrain", 15, false);
        for (Entity e : player.getNearbyEntities(r, r, r)) {
            if (e instanceof Player) {
                if (damageCheck(player, (LivingEntity) e)) {
                    Player tplayer = (Player) e;
                    MindblastEffect me = new MindblastEffect(this, duration, amplifier);
                    Hero thero = plugin.getCharacterManager().getHero(tplayer);
                    thero.addEffect(me);
                    if (thero.getMana() > manadrain) {
                        thero.setMana(thero.getMana() - manadrain);
                    } else {
                        thero.setMana(0);
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class MindblastEffect extends ExpirableEffect {
        public MindblastEffect(Skill skill, long duration, int amplifier) {
            super(skill, "MindblastEffect", duration);
            this.types.add(EffectType.CONFUSION);
            this.types.add(EffectType.NAUSEA);
            this.types.add(EffectType.SLOW);
            addMobEffect(2, (int) ((duration / 1000) * 20), amplifier, false);
            addMobEffect(9, (int) ((duration / 1000) * 20), amplifier, false);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }
    }
}