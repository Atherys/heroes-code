package com.atherys.skills.SkillMobRange;

/**
 * Created by JCastro on 5/14/2015.
 */

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import net.elseland.xikage.MythicMobs.API.ThreatTables;
import net.elseland.xikage.MythicMobs.Mobs.MobSpawner;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillMobRange extends TargettedSkill {


    public SkillMobRange(Heroes plugin) {
        super(plugin, "MobRange");
        setUsage("/skill MobRange");
        setArgumentRange(0, 0);
        setIdentifiers("skill MobRange");
        setDescription("Summon up to $1 $2 to attack another player at range");
        setTypes(SkillType.SILENCABLE);
    }

    public String getDescription(Hero hero) {
        int mobcount = SkillConfigManager.getUseSetting(hero, this, "mobcount", 2, false);
        String mobdisplay = SkillConfigManager.getUseSetting(hero, this, "MobDisplay", "Undead Bandit");
        String mobtype = SkillConfigManager.getUseSetting(hero, this, "MobType", "HeroBandit");
        return getDescription().replace("$1", mobcount + "").replace("$2", mobdisplay);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("mobcount", Integer.valueOf(2));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity livingEntity, String[] strings) {
        if ((livingEntity instanceof Player) && (livingEntity != hero.getEntity())) {
            Player target = ((Player) livingEntity).getPlayer();
            int mobcount = SkillConfigManager.getUseSetting(hero, this, "mobcount", 2, false);
            String MobType = SkillConfigManager.getUseSetting(hero, this, "MobType", "HeroBandit");
            for (int mcount = 0; mcount < mobcount; mcount++) {
                Entity reaper = MobSpawner.SpawnMythicMob(MobType, target.getLocation());
                plugin.getCharacterManager().getMonster((LivingEntity) reaper).setExperience(0);
                ThreatTables.taunt(reaper, target);
            }
            broadcastExecuteText(hero, livingEntity);
            return SkillResult.NORMAL;
        }
        return SkillResult.CANCELLED;
    }
}
