package com.atherys.skills.SkillMoonlight;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.common.BlindEffect;
import com.herocraftonline.heroes.characters.effects.common.NightvisionEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

public class SkillMoonlight extends ActiveSkill {
    public SkillMoonlight(Heroes plugin) {
        super(plugin, "Moonlight");
        setDescription("Gives party Nightvision buff for a duration. Phase: blinds enemies and removes enemy night vision effect.");
        setUsage("/skill Moonlight");
        setArgumentRange(0, 0);
        setIdentifiers("skill Moonlight");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        if (!hero.hasEffect("PhaseEffect")) {
            if (hero.hasParty()) {
                for (Hero phero : hero.getParty().getMembers()) {
                    phero.addEffect(new NightvisionEffect(this, duration, "", ""));
                }
            }
        } else {
            int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
            for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
                if (e instanceof Player) {
                    Player p = (Player) e;
                    if (damageCheck(p, hero.getPlayer())) {
                        plugin.getCharacterManager().getHero(p).addEffect(new BlindEffect(this, duration, "", ""));
                        if (p.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
                            p.removePotionEffect(PotionEffectType.NIGHT_VISION);
                        }
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }
}