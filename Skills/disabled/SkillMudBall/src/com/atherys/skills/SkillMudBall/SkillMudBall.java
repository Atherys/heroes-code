package com.atherys.skills.SkillMudBall;

import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class SkillMudBall extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillMudBall(Heroes paramHeroes) {
        super(paramHeroes, "MudBall");
        setDescription("You shoot a ball of mud that deals $1 damage and slows your target");
        setUsage("/skill mudball");
        setArgumentRange(0, 0);
        setIdentifiers("skill mudball");
        setTypes(SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), paramHeroes);
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% has been slowed by %hero%!").replace("%target%", "$1").replace("%hero%", "$2");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% is no longer slowed!").replace("%target%", "$1");
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection localConfigurationSection = super.getDefaultConfig();
        localConfigurationSection.set(SkillSetting.DAMAGE.node(), Integer.valueOf(4));
        localConfigurationSection.set(SkillSetting.DAMAGE_INCREASE.node(), Double.valueOf(0.0D));
        localConfigurationSection.set("velocity-multiplier", Double.valueOf(1.5D));
        localConfigurationSection.set("slow-duration", Integer.valueOf(5000));
        localConfigurationSection.set("speed-multiplier", Integer.valueOf(2));
        localConfigurationSection.set(SkillSetting.APPLY_TEXT.node(), "%target% has been slowed by %hero%!");
        localConfigurationSection.set(SkillSetting.EXPIRE_TEXT.node(), "%target% is no longer slowed!");
        return localConfigurationSection;
    }

    public SkillResult use(Hero paramHero, String[] paramArrayOfString) {
        Player localPlayer = paramHero.getPlayer();
        Snowball localSnowball = localPlayer.launchProjectile(Snowball.class);
        localSnowball.setFireTicks(100);
        localSnowball.setMetadata("MudballFireball", new FixedMetadataValue(plugin, true));
        double d = SkillConfigManager.getUseSetting(paramHero, this, "velocity-multiplier", 1.5D, false);
        localSnowball.setVelocity(localSnowball.getVelocity().multiply(d));
        broadcastExecuteText(paramHero);
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero paramHero) {
        int i = SkillConfigManager.getUseSetting(paramHero, this, SkillSetting.DAMAGE, 1, false);
        i += (int) (SkillConfigManager.getUseSetting(paramHero, this, SkillSetting.DAMAGE_INCREASE, 0.0D, false) * paramHero.getSkillLevel(this));
        return getDescription().replace("$1", i + "");
    }

    public class SkillEntityListener
            implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill paramSkill) {
            this.skill = paramSkill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent paramEntityDamageEvent) {
            if ((paramEntityDamageEvent.isCancelled()) || (!(paramEntityDamageEvent instanceof EntityDamageByEntityEvent)) || (!(paramEntityDamageEvent.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent localEntityDamageByEntityEvent = (EntityDamageByEntityEvent) paramEntityDamageEvent;
            Entity localEntity = localEntityDamageByEntityEvent.getDamager();
            if ((!(localEntity instanceof Snowball)) || (!localEntity.hasMetadata("MudballFireball"))) {
                return;
            }
            Player localLivingEntity1 = (Player) ((Snowball) localEntityDamageByEntityEvent.getDamager()).getShooter();
            Hero localHero = SkillMudBall.this.plugin.getCharacterManager().getHero(localLivingEntity1);
            if (!Skill.damageCheck(localLivingEntity1, (LivingEntity) paramEntityDamageEvent.getEntity())) {
                paramEntityDamageEvent.setCancelled(true);
                return;
            }
            paramEntityDamageEvent.getEntity().setFireTicks(0);
            double d = SkillConfigManager.getUseSetting(localHero, this.skill, SkillSetting.DAMAGE, 3, false);
            long l = SkillConfigManager.getUseSetting(localHero, this.skill, "slow-duration", 10000, false);
            int i = SkillConfigManager.getUseSetting(localHero, this.skill, "speed-multiplier", 2, false);
            SlowEffect localSlowEffect = new SlowEffect(this.skill, l, i, false, SkillMudBall.this.applyText, SkillMudBall.this.expireText, localHero);
            LivingEntity localLivingEntity2 = (LivingEntity) paramEntityDamageEvent.getEntity();
            SkillMudBall.this.plugin.getCharacterManager().getCharacter(localLivingEntity2).addEffect(localSlowEffect);
            SkillMudBall.this.addSpellTarget(paramEntityDamageEvent.getEntity(), localHero);
            Skill.damageEntity(localLivingEntity2, localHero.getPlayer(), d, EntityDamageEvent.DamageCause.MAGIC);
            paramEntityDamageEvent.setCancelled(true);
        }
    }
}