package com.atherys.skills.SkillMudShield;

import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class SkillMudShield extends ActiveSkill {
    public String applyText;
    public String expireText;

    public SkillMudShield(Heroes plugin) {
        super(plugin, "Mudshield");
        setDescription("Active\n Create moving shield of mud for $1-$2s");
        setUsage("/skill mudshield");
        setArgumentRange(0, 0);
        setIdentifiers("skill mudshield");
        setTypes(SkillType.BUFF, SkillType.COUNTER, SkillType.SILENCABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        long addDur = SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false) * hero.getSkillLevel(this);
        long minDur = SkillConfigManager.getUseSetting(hero, this, "min_duration", 20000, false) + addDur;
        long maxDur = SkillConfigManager.getUseSetting(hero, this, "max_duration", 40000, false) + addDur;
        String description = getDescription().replace("$1", minDur + "").replace("$2", maxDur + "");
        return description + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("min_duration", 20000);
        node.set("max_duration", 40000);
        node.set("duration-increase", 0);
        node.set("apply-text", "The ground becomes muddy as %hero% creates a %skill%"/*"%hero% uses the air to create a %skill%!"*/);
        node.set("expire-text", "%hero%s %skill% crumbles as the mud dries up!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getUseSetting(null, this, "apply-text", "The ground becomes muddy as %hero% creates a %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getUseSetting(null, this, "expire-text", "%hero%s %skill% crumbles as the mud dries up!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        long addDur = SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false) * hero.getSkillLevel(this);
        long minDuration = SkillConfigManager.getUseSetting(hero, this, "min_duration", 20000, false) + addDur;
        if (minDuration < 1000 || minDuration > 60000) {
            minDuration = 20000;
        }
        long maxDuration = SkillConfigManager.getUseSetting(hero, this, "max_duration", 40000, false) + addDur;
        if (maxDuration > 60000) {
            maxDuration = 40000;
        }
        if (maxDuration < minDuration) {
            maxDuration = minDuration;
        }
        long randDuration = maxDuration - minDuration;
        long duration = (long) ((Math.random() * randDuration) + minDuration);
        broadcastExecuteText(hero);
        Messaging.send(player, "Duration " + duration / 1000 + "s");
        MudshieldEffect gEffect = new MudshieldEffect(this, "Mudshield", duration);
        hero.addEffect(gEffect);
        return SkillResult.NORMAL;
    }

    public class MudshieldEffect extends PeriodicExpirableEffect {
        private boolean seed;
        private Block prevRefBlock;
        private ArrayList<Block> dirtlist = new ArrayList<Block>();

        public MudshieldEffect(Skill skill, String name, long duration) {
            super(skill, name, 200, duration);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.FORM);
            seed = true;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "Mudshield");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            if(this.isExpired()) {
                broadcast(player.getLocation(), expireText, player.getDisplayName(), "Mudshield");
            }
            updateShield(null, seed);
        }

        @Override
        public void tickHero(Hero hero) {
            if (!hero.hasEffect("Mudshield")) {
                return;
            }
            updateShield(hero.getPlayer().getLocation().getBlock(), seed);
            seed = !seed;
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //:P I'm really tired
        }

        private void updateShield(Block refBlock, boolean seed) {
            if (prevRefBlock != null) {
                //////////NORTH
                replaceDirt(retrieveBlock(prevRefBlock, 3, 0, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 0, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 0, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 0, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 0, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 1, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 1, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 1, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 1, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 1, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 2, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 2, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 2, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 2, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 3, 2, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -1, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -1, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -1, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -1, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -1, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -2, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -2, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -2, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -2, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 3, -2, 3));
                /////////EAST
                replaceDirt(retrieveBlock(prevRefBlock, 0, 3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 3, 3));
                ///////////WEST
                replaceDirt(retrieveBlock(prevRefBlock, 0, -3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -3, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -3, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -3, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -3, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -3, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -3, 3));
                //////////SOUTH
                replaceDirt(retrieveBlock(prevRefBlock, -3, 0, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 0, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 0, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 0, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 0, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 1, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 1, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 1, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 1, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 1, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 2, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 2, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 2, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 2, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -3, 2, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -1, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -1, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -1, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -1, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -1, 3));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -2, -1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -2, 0));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -2, 1));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -2, 2));
                replaceDirt(retrieveBlock(prevRefBlock, -3, -2, 3));
                //////////UP
                replaceDirt(retrieveBlock(prevRefBlock, 0, 0, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 0, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 0, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 0, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 0, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -1, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -2, 4));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -2, 4));
                /////////DOWN
                replaceDirt(retrieveBlock(prevRefBlock, 0, 0, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 0, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 0, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 0, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 0, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 0, 2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -2, 2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -1, 2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 1, 2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 2, 2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -1, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 0, -2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -2, -2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, -1, -2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 1, -2, -2));
                replaceDirt(retrieveBlock(prevRefBlock, 2, -2, -2));
                fixDirt();
            }
            if (refBlock != null) {
                if (seed) {
                    /////////NORTH
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 3), Material.AIR);
                    //////////EAST
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 3), Material.DIRT);
                    //////////WEST
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 3), Material.DIRT);
                    //////////SOUTH
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 3), Material.AIR);
                    /////////UP
                    replaceIfDirt(retrieveBlock(refBlock, 0, 0, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 0, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 0, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 0, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 0, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -2, 4), Material.AIR);
                    //////////DOWN
                    replaceIfDirt(retrieveBlock(refBlock, 0, 0, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 0, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 0, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 0, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 0, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -2, -2), Material.AIR);
                } else {
                    /////NORTH
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 0, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 2, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -1, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, 1, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 3, -2, 3), Material.DIRT);
                    //////EAST
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 3, 3), Material.AIR);
                    ///////WEST
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -3, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -3, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -3, 3), Material.AIR);
                    ///////SOUTH
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 0, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 1, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, 2, 3), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, -1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 0), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 1), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -1, 3), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, -1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 0), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 1), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -3, -2, 3), Material.DIRT);
                    ///////UP
                    replaceIfDirt(retrieveBlock(refBlock, 0, 0, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 0, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 0, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 0, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 0, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -1, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -1, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -2, 4), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -2, 4), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -2, 4), Material.DIRT);
                    ////////DOWN
                    replaceIfDirt(retrieveBlock(refBlock, 0, 0, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 0, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 0, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 0, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 0, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, 2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, 2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, 2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, 2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, 2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -1, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -1, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 0, -2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, -1, -2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, -2, -2, -2), Material.DIRT);
                    replaceIfDirt(retrieveBlock(refBlock, 1, -2, -2), Material.AIR);
                    replaceIfDirt(retrieveBlock(refBlock, 2, -2, -2), Material.DIRT);
                }
            }
            prevRefBlock = refBlock;
        }

        /**
         * Retrieves the block at the relative co-ordinates.
         *
         * @param referenceBlock, relativeX, relativeY, relativeZ
         * @return Block
         */
        private void replaceIfDirt(Block checkBlock, Material replaceWith) {
            if (checkBlock.getType() == Material.DIRT) {
                dirtlist.add(checkBlock);
            }
            if (checkBlock.getType() == Material.AIR) {
                checkBlock.setType(replaceWith);
            }
        }

        /**
         * Sets the block material to AIR if the block was DIRT.
         *
         * @param referenceBlock
         * @return Block
         */
        private void replaceDirt(Block checkBlock) {
            if (checkBlock.getType() == Material.DIRT) {
                checkBlock.setType(Material.AIR);
            }
        }

        /**
         * Corrects missing DIRT from the replaceDirt Method
         *
         * @param referenceBlock
         * @return Block
         */
        private void fixDirt() {
            for (int i = 0; i < dirtlist.size(); i++) {
                dirtlist.get(i).setType(Material.DIRT);
            }
            dirtlist.clear();
        }

        private Block retrieveBlock(Block refBlock, int relX, int relY, int relZ) {
            Block returnBlock = refBlock;
            while (0 < Math.abs(relX)) {
                if (relX > 0) {
                    returnBlock = returnBlock.getRelative(BlockFace.NORTH);
                    relX--;
                } else {
                    returnBlock = returnBlock.getRelative(BlockFace.SOUTH);
                    relX++;
                }
            }
            while (0 < Math.abs(relY)) {
                if (relY > 0) {
                    returnBlock = returnBlock.getRelative(BlockFace.EAST);
                    relY--;
                } else {
                    returnBlock = returnBlock.getRelative(BlockFace.WEST);
                    relY++;
                }
            }
            while (0 < Math.abs(relZ)) {
                if (relZ > 0) {
                    returnBlock = returnBlock.getRelative(BlockFace.UP);
                    relZ--;
                } else {
                    returnBlock = returnBlock.getRelative(BlockFace.DOWN);
                    relZ++;
                }
            }
            return returnBlock;
        }
    }
}