package com.atherys.skills.SkillMultiBlink;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class SkillMultiBlink extends ActiveSkill {
    public SkillMultiBlink(Heroes plugin) {
        super(plugin, "MultiBlink");
        setDescription("Teleports you every $1s $2 times");
        setUsage("/skill MultiBlink");
        setArgumentRange(0, 0);
        setIdentifiers("skill multiblink");
        setTypes(SkillType.SILENCABLE, SkillType.TELEPORT);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000L));
        node.set("number-of-blinks", Integer.valueOf(3));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(6));
        node.set("restrict-ender-pearl", Boolean.valueOf(true));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(5));
        return node;
    }

    public void blink(Hero hero) {
        Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return;
        }
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        Block prev = null;
        BlockIterator iter = null;
        try {
            iter = new BlockIterator(player, distance);
        } catch (IllegalStateException e) {
            Messaging.send(player, "There was an error getting your blink location!");
            return;
        }
        while (iter.hasNext()) {
            Block b = iter.next();
            if ((!Util.transparentBlocks.contains(b.getType())) || ((!Util.transparentBlocks.contains(b.getRelative(BlockFace.UP).getType())) && (!Util.transparentBlocks.contains(b.getRelative(BlockFace.DOWN).getType()))))
                break;
            prev = b;
        }
        if (prev != null) {
            Location teleport = prev.getLocation().clone();
            teleport.add(new Vector(0.5D, 0.5D, 0.5D));
            teleport.setPitch(player.getLocation().getPitch());
            teleport.setYaw(player.getLocation().getYaw());
            player.teleport(teleport);
        }
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000L, false);
        hero.addEffect(new BEffect(this, period, (period * (nb - 1))));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        long period = (long) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000L, false) / 1000);
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        return getDescription().replace("$1", period + "").replace("$2", nb + "");
    }

    public class BEffect extends PeriodicExpirableEffect {
        public BEffect(Skill skill, long period, long duration) {
            super(skill, "BEffect", period, duration);
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
        }

        @Override
        public void tickHero(Hero hero) {
            blink(hero);
        }

        @Override
        public void tickMonster(Monster mnstr) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}