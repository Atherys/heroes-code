package com.atherys.skills.SkillNocturne;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillNocturne extends ActiveSkill {
    public SkillNocturne(Heroes plugin) {
        super(plugin, "Nocturne");
        setDescription("A song that removes all positive status effects from nearby enemies around the caster.");
        setArgumentRange(0, 0);
        setUsage("/skill Nocturne");
        setIdentifiers("skill Nocturne");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set("miss-text", "You missed your attack");
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% has been cursed!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% has recovered from the curse!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
            if (!(e instanceof Player)) continue;
            if (damageCheck(hero.getPlayer(), (Player) e)) {
                Hero h = plugin.getCharacterManager().getHero((Player) e);
                for (Effect eff : h.getEffects()) {
                    if (eff.isType(EffectType.HEAL) || (eff.isType(EffectType.BENEFICIAL)) || eff.isType(EffectType.INVIS)
                            || eff.isType(EffectType.INVULNERABILITY) || eff.isType(EffectType.SAFEFALL)) {
                        h.removeEffect(eff);
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}