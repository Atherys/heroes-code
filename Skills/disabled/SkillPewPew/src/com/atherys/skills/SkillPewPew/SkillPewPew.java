package com.atherys.skills.SkillPewPew;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillPewPew extends ActiveSkill {
    public SkillPewPew(Heroes plugin) {
        super(plugin, "PewPew");
        setDescription("Creates cone of arrows");
        setUsage("/skill PewPew");
        setArgumentRange(0, 0);
        setIdentifiers("skill PewPew");
        setTypes(SkillType.DEBUFF, SkillType.SILENCABLE, SkillType.HARMFUL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillDamageListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10000));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        // int damage = SkillConfigManager.getUseSetting(hero,this,
        // SkillSetting.DAMAGE,10,false);
        for (int x = -3; x <= 2; x++) {
            for (double y = 1; y <= 2; y += 0.5) {
                Arrow arrow = hero.getPlayer().launchProjectile(Arrow.class);
                // float a = x/10+1;
                // float b = y/10+1;
                arrow.setVelocity(arrow.getVelocity().add(new Vector(Math.cos(x), Math.cos(y), Math.sin(x))));
                arrow.getVelocity().multiply(2);
                arrow.setMetadata("PewPewArrow", new FixedMetadataValue(plugin, true));
            }
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class SkillDamageListener implements Listener {
        private final Skill skill;

        public SkillDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof Arrow)) || (!projectile.hasMetadata("PewPewArrow"))) {
                return;
            }
            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            Entity dmger = (Entity) ((Arrow) projectile).getShooter();
            if (!(dmger instanceof Player)) {
                return;
            }
            Player player = (Player) dmger;
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!damageCheck(player, entity)) {
                event.setCancelled(true);
                return;
            }
            addSpellTarget(entity, hero);
            double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 4, false);
            if (entity instanceof Player) {
                Hero t = plugin.getCharacterManager().getHero((Player) entity);
                if (t.hasEffect("dcPP")) {
                    return;
                } else {
                    t.addEffect(new DamageCheckPP(skill));
                }
            }
            damageEntity(entity, hero.getPlayer(), damage, DamageCause.ENTITY_ATTACK);
            event.setCancelled(true);
        }
    }

    public class DamageCheckPP extends ExpirableEffect {
        public DamageCheckPP(Skill skill) {
            super(skill, "dcPP", 20L);
        }
    }
}
