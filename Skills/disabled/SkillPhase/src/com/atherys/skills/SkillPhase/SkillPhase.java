package com.atherys.skills.SkillPhase;

import com.herocraftonline.heroes.Heroes;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;

public class SkillPhase extends ActiveSkill {
    public SkillPhase(Heroes plugin) {
        super(plugin, "Phase");
        setDescription("Toggle-able self buff - Phase gives spells a different effect");
        setUsage("/skill Phase");
        setArgumentRange(0, 0);
        setIdentifiers("skill Phase");
        setTypes(SkillType.SILENCABLE, SkillType.BUFF);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (!hero.hasEffect("PhaseEffect")) {
            hero.addEffect(new Effect(this, "PhaseEffect"));
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Phase effect activated");
        } else {
            hero.removeEffect(hero.getEffect("PhaseEffect"));
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Phase effect deactivated");
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class PhaseEffect extends Effect {
        public PhaseEffect(Skill skill) {
            super(skill, "PhaseEffect");
            types.add(EffectType.BENEFICIAL);
        }
    }
}
