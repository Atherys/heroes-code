package com.atherys.skills.SkillPickPocket;

import com.herocraftonline.heroes.Heroes;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import com.palmergames.bukkit.towny.utils.CombatUtil;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;


public class SkillPickPocket extends TargettedSkill {
    private String failMessage;
    private String noisySuccessMessage;

    public SkillPickPocket(Heroes plugin) {
        super(plugin, "PickPocket");
        setDescription("You attempt to steal an item from your target.");
        setUsage("/skill pickpocket");
        setArgumentRange(0, 0);
        setIdentifiers("skill pickpocket", "skill ppocket", "skill pickp");
        setTypes(SkillType.PHYSICAL, SkillType.HARMFUL, SkillType.SILENCABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("base-chance", Double.valueOf(0.1D));
        node.set("chance-per-level", Double.valueOf(0.003D));
        node.set("failure-message", "%hero% failed to steal from %target%!");
        node.set("noisy-success-message", "%hero% stole %target%s %item%!");
        node.set(SkillSetting.USE_TEXT.node(), "");
        node.set("disallowed-items", Arrays.asList(""));
        node.set("always-steal-all", Boolean.valueOf(true));
        node.set("max-stolen", Integer.valueOf(64));
        return node;
    }

    public void init() {
        super.init();
        this.failMessage = SkillConfigManager.getRaw(this, "failure-message", "%hero% failed to steal from %target%!").replace("%hero%", "$1").replace("%target%", "$2");
        this.noisySuccessMessage = SkillConfigManager.getRaw(this, "noisy-success-message", "%hero% stole %target%s %item%!").replace("%hero", "$1").replace("%target%", "$2").replace("%item%", "$3");
    }

    @SuppressWarnings("deprecation")
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }

        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }

        if (target.hasMetadata("NPC")) {
            return SkillResult.INVALID_TARGET;
        }

        if (target.isOp()) {
            Messaging.send(player, "Now now now...");
            player.setHealth(5);
            player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 1));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 20));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 20));
            return SkillResult.INVALID_TARGET;
        }

        if (CombatUtil.preventDamageCall(null, player, target)) {
            Messaging.send(player, "Can't Steal in No-Pvp");
            return SkillResult.CANCELLED;
        }

        Player tPlayer = (Player) target;
        double chance = SkillConfigManager.getUseSetting(hero, this, "base-chance", 0.1D, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE_LEVEL, 0.02D, false) * hero.getSkillLevel(this);
        if (Util.nextRand() >= chance) {
            if (Util.nextRand() >= chance) {
                broadcast(player.getLocation(), this.failMessage, player.getDisplayName(), tPlayer.getDisplayName());
            }
            Messaging.send(player, "You failed to steal anything from $1", tPlayer.getDisplayName());
            return SkillResult.NORMAL;
        }
        Inventory tInventory = tPlayer.getInventory();
        ItemStack[] items = tInventory.getContents();
        int slot = Util.nextInt(27) + 9;
        Set<String> disallowed = new HashSet<String>(SkillConfigManager.getUseSetting(hero, this, "disallowed-items", new ArrayList<String>()));
        if ((items[slot] == null) || (items[slot].getType() == Material.AIR) || (disallowed.contains(items[slot].getType().name()))) {
            Messaging.send(player, "You failed to steal anything from $1", tPlayer.getDisplayName());
            return SkillResult.NORMAL;
        }
        int stealAmount = items[slot].getAmount();
        if (!SkillConfigManager.getUseSetting(hero, this, "always-steal-all", true)) {
            int maxSteal = SkillConfigManager.getUseSetting(hero, this, "max-stolen", 64, false);
            if (stealAmount > maxSteal) {
                stealAmount = maxSteal;
            }
            stealAmount = Util.nextInt(stealAmount) + 1;
        }
        if (stealAmount == items[slot].getAmount()) {
            tInventory.clear(slot);
        } else {
            items[slot].setAmount(stealAmount);
            tInventory.setItem(slot, items[slot]);
        }
        tPlayer.updateInventory();
        HashMap<Integer, ItemStack> leftOvers = player.getInventory().addItem(items[slot]);
        if ((leftOvers != null) && (!leftOvers.isEmpty())) {
            for (ItemStack is : leftOvers.values()) {
                player.getWorld().dropItemNaturally(player.getLocation(), is);
            }
        }
        player.updateInventory();
        if (Math.random() >= chance) {
            broadcast(player.getLocation(), this.noisySuccessMessage, player.getDisplayName(), tPlayer.getDisplayName(), items[slot].getType().name().replace("_", " ").toLowerCase());
        }
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
