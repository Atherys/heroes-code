package com.atherys.skills.SkillPiety;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillPiety extends PassiveSkill {
    public SkillPiety(Heroes plugin) {
        super(plugin, "Piety");
        setDescription("Strengthened by their beliefs, the acolyte takes $1% less damage from attacks");
        setTypes(SkillType.SILENCABLE, SkillType.BUFF);
        setEffectTypes(EffectType.BENEFICIAL);
        Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 0.5D, false);
        return getDescription().replace("$1", "" + amount * 100);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), Double.valueOf(0.5D));
        return node;
    }

    public class HeroDamageListener implements Listener {
        private final Skill skill;

        public HeroDamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onWeaponDamage(WeaponDamageEvent event) {
            if ((event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) || (event.getDamage() == 0) || (!(event.getEntity() instanceof Player))) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect(SkillPiety.this.getName())) {
                double multiplier = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.AMOUNT, 0.75D, true);
                event.setDamage(event.getDamage() - event.getDamage() * multiplier);
            }
        }
    }
}