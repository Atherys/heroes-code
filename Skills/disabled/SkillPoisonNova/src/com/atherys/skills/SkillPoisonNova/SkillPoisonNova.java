package com.atherys.skills.SkillPoisonNova;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SkillPoisonNova extends ActiveSkill {

    public SkillPoisonNova(Heroes plugin) {
        super(plugin, "PoisonNova");
        setDescription("Creates ring of poison.");
        setUsage("/skill PoisonNova");
        setArgumentRange(0, 0);
        setIdentifiers("skill PoisonNova");
        setTypes(SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("poison-duration", 5000);
        node.set("poison-period", 1000);
        node.set("poison-damage", 10);
        node.set("velocity-multiplier", 1.5D);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 1.5D, false);
        double diff = 2 * Math.PI / 36;
        for (double a = 0; a < 2 * Math.PI; a += diff) {
            Vector vel = new Vector(Math.cos(a), 0, Math.sin(a));
            EnderPearl ep = player.launchProjectile(EnderPearl.class);
            ep.setMetadata("PoisonNovaEnderPearl", new FixedMetadataValue(plugin, true));
            ep.setVelocity(vel.multiply(mult));
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SkillEntityListener
            implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGH)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof EnderPearl)) || (!projectile.hasMetadata("PoisonNovaEnderPearl"))) {
                return;
            }
            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            Entity dmger = (Entity) ((EnderPearl) projectile).getShooter();
            if (!(dmger instanceof Player)) {
                return;
            }
            Player player = (Player) dmger;
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!Skill.damageCheck(player, entity)) {
                event.setCancelled(true);
                return;
            }
            addSpellTarget(entity, hero);
            long period = SkillConfigManager.getUseSetting(hero, skill, "poison-period", 10000, false);
            long duration = SkillConfigManager.getUseSetting(hero, skill, "poison-duration", 10000, false);
            double damage = SkillConfigManager.getUseSetting(hero, skill, "poison-damage", 10, false);
            if (subEvent.getEntity() instanceof Player) {
                plugin.getCharacterManager().getHero((Player) event.getEntity()).addEffect(new PoisonEffect(skill, period, duration, damage, hero.getPlayer()));
            } else {
                plugin.getCharacterManager().getMonster((LivingEntity) event.getEntity());
            }
            event.setCancelled(true);
        }

        @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
        public void onPlayerTeleport(PlayerTeleportEvent event) {
            //Hero hero = plugin.getCharacterManager().getHero(event.getPlayer());
            if (event.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
                event.setCancelled(true);
            }
        }
    }

    public class PoisonEffect extends PeriodicDamageEffect {
        public PoisonEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "PoisonEffect", period, duration, tickDamage, applier);
            types.add(EffectType.POISON);
            addMobEffect(19, (int) (duration / 1000L) * 20, 0, true);
        }
    }
}
