package com.atherys.skills.SkillPort;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class SkillPort extends ActiveSkill {
    private String casterReagent;
    private int casterCost;
    private String travelerReagent;
    private int travelerCost;

    public SkillPort(Heroes plugin) {
        super(plugin, "Port");

        setUsage("/skill port <location>|list");
        setArgumentRange(1, 1);
        setIdentifiers("skill port");
        setTypes(SkillType.TELEPORT, SkillType.SILENCABLE);

        String desc = "Teleport yourself and your party within $1 blocks to the set location!";
        casterReagent = SkillConfigManager.getUseSetting(null, this, SkillSetting.REAGENT, "DIAMOND");
        casterCost = SkillConfigManager.getUseSetting(null, this, SkillSetting.REAGENT_COST, 1, false);
        if (casterCost > 0)
            desc += " Uses $2 $3.";
        if (travelerCost > 0)
            desc += " Party members need $4 $5.";
        setDescription(desc);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.NO_COMBAT_USE.node(), Boolean.valueOf(true));
        node.set("cross-world", Boolean.valueOf(false));
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        List<String> keys = new ArrayList<String>(SkillConfigManager.getUseSettingKeys(hero, this, null));
        for (SkillSetting setting : SkillSetting.values()) {
            keys.remove(setting.node());
        }

        if (args[0].equalsIgnoreCase("list")) {
            for (String n : keys) {
                String retrievedNode = SkillConfigManager.getUseSetting(hero, this, n, (String) null);
                if (retrievedNode != null) {
                    Messaging.send(player, "$1 - $2", n, retrievedNode);
                }
            }
            return SkillResult.SKIP_POST_USAGE;
        } //end if just list
        else {

            if (hero.isInCombat()) {
                hero.getPlayer().sendMessage(ChatColor.RED + "You cannot use this skill while in combat!");
                return SkillResult.CANCELLED;
            } //end if in combat
            else {
                String portinfo = SkillConfigManager.getUseSetting(hero, this, args[0].toLowerCase(), (String) null);
                if (portinfo != null) {
                    String[] splitArg = portinfo.split(":");
                    int levelRequirement = Integer.parseInt(splitArg[4]);
                    if (hero.getSkillLevel(this) < levelRequirement) {
                        hero.getPlayer().sendMessage(ChatColor.GRAY + args[0] + " requires a skill level of: " + levelRequirement);
                        return SkillResult.CANCELLED;
                    } else {
                        ItemStack cost = new ItemStack(Material.getMaterial(casterReagent), casterCost);
                        if (hero.getPlayer().getInventory().containsAtLeast(cost, cost.getAmount())) {
                            Location portloc = new Location(plugin.getServer().getWorld(splitArg[0]), Double.parseDouble(splitArg[1]), Double.parseDouble(splitArg[2]), Double.parseDouble(splitArg[3]));
                            if (hero.hasParty()) {
                                broadcastExecuteText(hero);
                                for (Hero phero : hero.getParty().getMembers()) {
                                    if (hero.getPlayer().getLocation().distanceSquared(phero.getPlayer().getLocation()) < 2500) {
                                        phero.getPlayer().teleport(portloc, PlayerTeleportEvent.TeleportCause.PLUGIN);
                                        phero.getPlayer().sendMessage(ChatColor.GRAY + "Successfully ported to " + args[0]);
                                    } //end if within range
                                    else {
                                        phero.getPlayer().sendMessage(ChatColor.RED + "Out of range for port");
                                    } //end if out of range
                                } //end for member
                                hero.getPlayer().teleport(portloc, PlayerTeleportEvent.TeleportCause.PLUGIN);
                                hero.getPlayer().sendMessage(ChatColor.GRAY + "Successfully ported to " + args[0]);
                                hero.getPlayer().getInventory().removeItem(cost);
                                return SkillResult.NORMAL;
                            } //end if in party
                            else {
                                hero.getPlayer().teleport(portloc, PlayerTeleportEvent.TeleportCause.PLUGIN);
                                hero.getPlayer().sendMessage(ChatColor.GRAY + "Successfully ported to " + args[0]);
                                hero.getPlayer().getInventory().removeItem(cost);
                                return SkillResult.NORMAL;
                            } //end if not in party
                        }  // has caster cost
                        else {
                            broadcastExecuteText(hero);
                            hero.getPlayer().sendMessage(ChatColor.GRAY + "Port requires " + casterCost + " " + casterReagent);
                            return SkillResult.CANCELLED;
                        }
                    } //matches skill level

                } //end if port info is okay
                else {
                    hero.getPlayer().sendMessage(ChatColor.GRAY + "The following location is not a valid port point: " + args[0]);
                    return SkillResult.CANCELLED;
                } //end argument invalid
            } //end if not in combat
        } //end if porting to location
    }


    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        radius += (int) SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS_INCREASE, 0.0D, false) * hero.getSkillLevel(this);
        String ret = getDescription();
        ret = ret.replace("$1", radius + "");
        ret = ret.replace("$2", new Integer(casterCost).toString());
        ret = ret.replace("$3", casterReagent);
        return ret;
    }
}
