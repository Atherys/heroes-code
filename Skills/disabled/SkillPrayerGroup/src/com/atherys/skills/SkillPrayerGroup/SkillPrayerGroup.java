package com.atherys.skills.SkillPrayerGroup;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SkillPrayerGroup extends ActiveSkill {
    public SkillPrayerGroup(Heroes plugin) {
        super(plugin, "PrayerGroup");
        setDescription("The three members of party with lowest health are healed.");
        setUsage("/skill PrayerGroup");
        setArgumentRange(0, 0);
        setIdentifiers("skill PrayerGroup");
        setTypes(SkillType.HEAL, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("heal", 10);
        node.set(SkillSetting.RADIUS.node(), 30);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player p = hero.getPlayer();
        double heal = SkillConfigManager.getUseSetting(hero, this, "heal", 10, false);
        if (!hero.hasParty()) {
            HeroRegainHealthEvent hrEvent5 = new HeroRegainHealthEvent(hero, heal, this, hero);
            this.plugin.getServer().getPluginManager().callEvent(hrEvent5);
            if (!hrEvent5.isCancelled()) {
                hero.heal(hrEvent5.getAmount());
            }
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 30, false);
        Hero a = null, b = null, c = null;
        for (Hero pHero : hero.getParty().getMembers()) {
            if (((((pHero.getPlayer()).getLocation()).distanceSquared(p.getLocation())) <= radius * radius) && (!(pHero.equals(hero)))) {
                if (a == null)
                    a = pHero;
                else if (b == null)
                    b = pHero;
                else if (c == null)
                    c = pHero;
                else if (a.getPlayer().getHealth() > pHero.getPlayer().getHealth())
                    a = pHero;
                else if (b.getPlayer().getHealth() > pHero.getPlayer().getHealth())
                    b = pHero;
                else if (c.getPlayer().getHealth() > pHero.getPlayer().getHealth())
                    c = pHero;
            }
        }
        Lib.healHero(hero, heal, this, hero);
        if (a != null) {
            Lib.healHero(a, heal, this, hero);
        }
        if (b != null) {
            Lib.healHero(b, heal, this, hero);
        }
        if (c != null) {
            Lib.healHero(c, heal, this, hero);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}