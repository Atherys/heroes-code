package com.atherys.skills.SkillPrecisionStrike;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillPrecisionStrike extends TargettedSkill {
    public SkillPrecisionStrike(Heroes plugin) {
        super(plugin, "PrecisionStrike");
        setDescription("Bypasses armor and does $1 dmg to target.");
        setUsage("/skill PrecisionStrike");
        setArgumentRange(0, 0);
        setIdentifiers("skill precisionstrike");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL, SkillType.DAMAGING, SkillType.DEBUFF);
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE.node(), 35, false);
        damage = damage > 0 ? damage : 0;
        String description = getDescription().replace("$1", damage + "");
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(35));
        node.set("fake-damage", Integer.valueOf(0));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if ((target instanceof Player)) {
            Player tplayer = (Player) target;
            Player tPlayer = (Player) target;
            if (!damageCheck(player, tPlayer)) {
                Messaging.send(player, "You can't harm that target");
                return SkillResult.INVALID_TARGET_NO_MSG;
            }
            broadcastExecuteText(hero, target);
            double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE.node(), 35, false);
            damageEntity(target, player, damage, EntityDamageEvent.DamageCause.CUSTOM);
            return SkillResult.NORMAL;
        }
        return SkillResult.INVALID_TARGET;
    }
}