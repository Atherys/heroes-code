package com.atherys.skills.SkillPrettyPoison;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillPrettyPoison extends ActiveSkill {
    public SkillPrettyPoison(Heroes plugin) {
        super(plugin, "PrettyPoison");
        setDescription("Applies the assassins blade buff to party essentially. Bloom: removes 1 poison effect from your party members");
        setUsage("/skill PrettyPoison");
        setArgumentRange(0, 0);
        setIdentifiers("skill PrettyPoison");
        setTypes(SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set("attacks", 5);
        node.set("buff-duration", 20000);
        node.set("damage-period", 1000);
        node.set("poison-duration", 10000);
        node.set("poison-damage", 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        if (hero.hasEffect("BloomEffect")) {
            long duration = SkillConfigManager.getUseSetting(hero, this, "buff-duration", 10000, true);
            int a = SkillConfigManager.getUseSetting(hero, this, "attacks", 5, true);
            if (hero.hasParty()) {
                for (Hero partyhero : hero.getParty().getMembers()) {
                    if (partyhero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= r * r) {
                        partyhero.addEffect(new PBuffEffect(this, duration, a));
                    }
                }
            } else {
                hero.addEffect(new PBuffEffect(this, duration, a));
            }
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        if (hero.hasParty()) {
            for (Hero partyhero : hero.getParty().getMembers()) {
                for (Effect effects : partyhero.getEffects()) {
                    if (partyhero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= r * r) {
                        if (effects.isType(EffectType.POISON)) {
                            partyhero.removeEffect(effects);
                            continue;
                        }
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class PBuffEffect extends ExpirableEffect {
        private int a;

        public PBuffEffect(Skill skill, long duration, int a) {
            super(skill, "PbuffEffect", duration);
            this.a = a;
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Messaging.send(hero.getPlayer(), "Your blade is poisoned!");
        }

        public int getAttacksLeft() {
            return this.a;
        }

        public void setattacksLeft(int attacks) {
            this.a = attacks;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "Your blade is no longer poisoned!");
        }
    }

    public class PoisonEffect extends PeriodicDamageEffect {
        public PoisonEffect(Skill skill, long period, long duration, double damage, Player player) {
            super(skill, "Prettypoison", period, duration, damage, player);
            types.add(EffectType.POISON);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(hero.getPlayer(), "Your body recovered from poison.");
        }
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class SkillEntityListener implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(WeaponDamageEvent event) {
            if (event.isCancelled() || (event.getDamage() == 0)) {
                return;
            }
            if (!(event.getDamager() instanceof Hero)) {
                return;
            }
            Hero hero = (Hero) event.getDamager();
            if (!(hero.hasEffect("PbuffEffect"))) {
                return;
            }
            PBuffEffect pb = (PBuffEffect) hero.getEffect("PBuffEffect");
            if (pb.getAttacksLeft() > 0) {
                pb.setattacksLeft(pb.getAttacksLeft() - 1);
                long period = SkillConfigManager.getUseSetting(hero, skill, "damage-period", 1000, false);
                long duration = SkillConfigManager.getUseSetting(hero, skill, "poison-duration", 10, false);
                double damage = SkillConfigManager.getUseSetting(hero, skill, "poison-damage", 10, false);
                if (event.getEntity() instanceof Player) {
                    plugin.getCharacterManager().getHero((Player) event.getEntity()).addEffect(new PoisonEffect(skill, period, duration, damage, hero.getPlayer()));
                } else {
                    plugin.getCharacterManager().getMonster((LivingEntity) event.getEntity()).addEffect(new PoisonEffect(skill, period, duration, damage, hero.getPlayer()));
                }
            } else {
                hero.removeEffect(hero.getEffect("PBuffEffect"));
            }
        }
    }
}
