package com.atherys.skills.SkillQuestion;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.effects.common.StunEffect;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillQuestion extends TargettedSkill {
    public SkillQuestion(Heroes plugin) {
        super(plugin, "Question");
        setDescription("Puts your target to the question... Painfully.");
        setUsage("/skill Question");
        setArgumentRange(0, 0);
        setIdentifiers("skill Question");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("stun-duration", 10000);
        node.set("dot-duration", 10000);
        node.set("period", 1000);
        node.set("damage", 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Player tplayer = (Player) target;
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET;
        }
        double d = SkillConfigManager.getUseSetting(hero, this, "damage", 10, false);
        int sd = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 1000, false);
        long p = SkillConfigManager.getUseSetting(hero, this, "period", 1000, false);
        long dd = SkillConfigManager.getUseSetting(hero, this, "dot-duration", 1000, false);
        Hero thero = plugin.getCharacterManager().getHero(tplayer);
        thero.addEffect(new StunEffect(this, sd));
        thero.addEffect(new PeriodicDamageEffect(this, "QuestionEffect", p, dd, d, hero.getPlayer()));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }
}