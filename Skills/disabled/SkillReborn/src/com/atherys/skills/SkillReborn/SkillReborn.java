package com.atherys.skills.SkillReborn;

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.util.ParticleEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillReborn extends PassiveSkill {
    private String rebornText;

    public SkillReborn(Heroes plugin) {
        super(plugin, "Reborn");
        setDescription("If you are about to die instead you regain $1% hp, can only trigger once every $2 seconds.");
        setTypes(SkillType.COUNTER, SkillType.DARK);
        Bukkit.getServer().getPluginManager().registerEvents(new RebornListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("health-percent-on-rebirth", Double.valueOf(0.5D));
        node.set("health-increase", Double.valueOf(0.0D));
        node.set("on-reborn-text", "%hero% is saved from death, but weakened!");
        node.set(SkillSetting.COOLDOWN.node(), Integer.valueOf(600000));
        node.set(SkillSetting.COOLDOWN_REDUCE.node(), Integer.valueOf(0));
        return node;
    }

    public String getDescription(Hero hero) {
        int health = (int) ((SkillConfigManager.getUseSetting(hero, this, "health-percent-on-rebirth", 0.5D, false) + SkillConfigManager.getUseSetting(hero, this, "health-increase", 0.0D, false) * hero.getSkillLevel(this)) * 100.0D);
        int cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 600000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        String description = getDescription().replace("$1", health + "").replace("$2", cooldown + "");
        return description;
    }

    public void init() {
        super.init();
        this.rebornText = SkillConfigManager.getUseSetting(null, this, "on-reborn-text", "%hero% is saved from death, but weakened!").replace("%hero%", "$1");
    }

    public class RebornListener implements Listener {
        private final Skill skill;

        public RebornListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = SkillReborn.this.plugin.getCharacterManager().getHero(player);
            double currentHealth = player.getHealth();
            if (currentHealth > event.getDamage()) {
                return;
            }
            if ((hero.hasEffect("Reborn")) && (
                    (hero.getCooldown("Reborn") == null) || (hero.getCooldown("Reborn").longValue() <= System.currentTimeMillis()))) {
                double regainPercent = SkillConfigManager.getUseSetting(hero, this.skill, "health-percent-on-rebirth", 0.5D, false) + SkillConfigManager.getUseSetting(hero, this.skill, "health-increase", 0.0D, false) * hero.getSkillLevel(this.skill);
                double healthRegain = (player.getMaxHealth() * regainPercent);
                HeroRegainHealthEvent hrh = new HeroRegainHealthEvent(hero, healthRegain, this.skill, hero);
                if ((hrh.isCancelled()) || (hrh.getAmount() == 0)) {
                    return;
                }
                event.setDamage(0);
                event.setCancelled(true);
                hero.heal(hrh.getAmount());
                long cooldown = SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN.node(), 600000, false) + SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getLevel();
                hero.setCooldown("Reborn", cooldown + System.currentTimeMillis());
                SkillReborn.this.broadcast(player.getLocation(), SkillReborn.this.rebornText, player.getDisplayName());
                ParticleEffect.SMOKE_NORMAL.display(0, 0, 0, 0, 1, player.getLocation(), HeroesAddon.PARTICLE_RANGE);
            }
        }
    }
}
