package com.atherys.skills.SkillRebuttal;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.common.InvulnerabilityEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class SkillRebuttal extends ActiveSkill {
    public SkillRebuttal(Heroes plugin) {
        super(plugin, "Rebuttal");
        setDescription("Gives you and your party invulnerability for $2s.");
        setArgumentRange(0, 0);
        setUsage("/skill Rebuttal");
        setIdentifiers("skill rebuttal");
        setTypes(SkillType.PHYSICAL, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        return getDescription().replace("$2", d / 1000L + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        InvulnerabilityEffect i = new InvulnerabilityEffect(this, duration);
        if (!hero.hasParty()) {
            hero.addEffect(i);
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        int radius = SkillConfigManager.getUseSetting(hero, this, "radius", 10, true);
        Location ploc = hero.getPlayer().getLocation();
        for (Hero phero : hero.getParty().getMembers()) {
            if (phero.getPlayer().getLocation().distanceSquared(ploc) <= radius * radius) {
                phero.addEffect(i);
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
