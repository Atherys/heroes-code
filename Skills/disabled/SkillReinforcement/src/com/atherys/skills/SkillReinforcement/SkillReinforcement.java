package com.atherys.skills.SkillReinforcement;

import com.atherys.skills.SkillRift.SkillRift.RiftEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;

public class SkillReinforcement extends ActiveSkill {
    public SkillReinforcement(Heroes plugin) {
        super(plugin, "Reinforcement");
        setArgumentRange(0, 0);
        setUsage("/skill Reinforcement");
        setIdentifiers("skill Reinforcement");
        setTypes(SkillType.SILENCABLE);
        setDescription("Summons $1 random mobs at the obelisk.");
    }

    @Override
    public String getDescription(Hero hero) {
        int a = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 10, false);
        return getDescription().replace("$1", a + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (!hero.hasEffect("Rift")) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You do not have an active obelisk");
            return SkillResult.CANCELLED;
        }
        RiftEffect r = (RiftEffect) hero.getEffect("Rift");
        int n = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 10, false);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
