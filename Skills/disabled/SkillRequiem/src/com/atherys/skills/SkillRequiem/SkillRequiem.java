package com.atherys.skills.SkillRequiem;

import com.atherys.effects.*; import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.common.StunEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import java.util.Random;

public class SkillRequiem extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillRequiem(Heroes plugin) {
        super(plugin, "Requiem");
        setDescription("A song that causes nearby enemies to slow for $1 seconds. Has $2% chance to stun for $3 seconds");
        setUsage("/skill Requiem");
        setArgumentRange(0, 0);
        setIdentifiers("skill Requiem");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("slow-duration", 5000);
        node.set("stun-duration", 5000);
        node.set("stun-chance", 0.5D);
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% has been slowed by %hero%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%target% is no longer slowed!");
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set("slow-multiplier", 1);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long st = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 5000, false);
        long sl = SkillConfigManager.getUseSetting(hero, this, "slow-duration", 5000, false);
        double chance = SkillConfigManager.getUseSetting(hero, this, "stun-chance", 0.5D, false);
        return getDescription().replace("$1", "" + sl / 1000).replace("$2", chance * 100 + "").replace("$3", st / 1000 + "");
    }

    @Override
    public void init() {
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% has been slowed by %hero%!").replace("%target%", "$1").replace("%hero%", "$2");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% is no longer slowed!").replace("%target%", "$1");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long st = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 5000, false);
        long sl = SkillConfigManager.getUseSetting(hero, this, "slow-duration", 5000, false);
        double chance = SkillConfigManager.getUseSetting(hero, this, "stun-chance", 0.5D, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int m = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 1, false);
        Random ra = new Random();
        for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
            if (!(e instanceof LivingEntity)) {
                continue;
            }
            if (!damageCheck(hero.getPlayer(), (LivingEntity) e)) {
                continue;
            }
            plugin.getCharacterManager().getCharacter((LivingEntity) e).addEffect(new SlowEffect(this, sl, m, true, applyText, expireText, hero));
            if (ra.nextDouble() <= chance) {
                plugin.getCharacterManager().getCharacter((LivingEntity) e).addEffect(new StunEffect(this, st));
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
} 