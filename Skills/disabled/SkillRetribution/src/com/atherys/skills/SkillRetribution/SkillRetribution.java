package com.atherys.skills.SkillRetribution;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.projectiles.ProjectileSource;

public class SkillRetribution extends TargettedSkill {
    public SkillRetribution(Heroes plugin) {
        super(plugin, "Retribution");
        setDescription("The next attack you receive is also felt by your target.");
        setUsage("/skill Retribution");
        setArgumentRange(0, 0);
        setIdentifiers("skill Retribution");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
        Bukkit.getPluginManager().registerEvents(new HeroDamageListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Player tplayer = (Player) target;
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET;
        }
        int d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        hero.addEffect(new REffect(this, d, tplayer));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class REffect extends ExpirableEffect {
        private Player player;

        public REffect(Skill skill, long duration, Player player) {
            super(skill, "REffect", duration);
            this.player = player;
        }

        public Player getPlayer() {
            return player;
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Retribution expired");
        }
    }

    public class HeroDamageListener implements Listener {
        @EventHandler(ignoreCancelled = true)
        public void onEntityDamage(EntityDamageByEntityEvent event) {
            if (event.getDamage() == 0 || !(event.getEntity() instanceof Player)) {
                return;
            }
            Entity damager = event.getDamager();
            if (damager instanceof Arrow) {
                ProjectileSource pdamager = ((Arrow) damager).getShooter();
            }
            Hero defender = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (defender.hasEffect("REffect")) {
                Messaging.send(defender.getPlayer(), "WeaponDamage!");
                REffect effect = (REffect) defender.getEffect("REffect");
                Player target = effect.getPlayer();
                addSpellTarget(target, defender);
                damageEntity(target, defender.getPlayer(), event.getDamage(), DamageCause.CUSTOM);
                defender.removeEffect(defender.getEffect("REffect"));
            }
        }
    }
}