package com.atherys.skills.SkillReveal;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillReveal extends ActiveSkill {
    public SkillReveal(Heroes plugin) {
        super(plugin, "Reveal");
        setDescription("Reveals all invisible targets within $2 blocks");
        setUsage("/skill Reveal");
        setIdentifiers("skill Reveal");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        return getDescription().replace("$1", radius + "");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        boolean targets = false;
        for (Entity ent : player.getNearbyEntities(radius, radius, radius)) {
            if (ent instanceof Player) {
                Hero target = plugin.getCharacterManager().getHero(((Player) ent));
                for (Effect e : target.getEffects()) {
                    if (e.isType(EffectType.INVIS) || e.isType(EffectType.INVISIBILITY))
                        target.removeEffect(e);
                }
            }
        }
        if (!targets) {
            Messaging.send(player, "No valid targets within range!");
            return SkillResult.CANCELLED;
        }
        return SkillResult.NORMAL;
    }
}
