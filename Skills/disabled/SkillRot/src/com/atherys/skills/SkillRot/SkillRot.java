package com.atherys.skills.SkillRot;

import com.atherys.heroesaddon.util.*; import com.atherys.effects.*; import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillRot extends TargettedSkill {
    private String applytext;
    private String expiretext;

    public SkillRot(Heroes plugin) {
        super(plugin, "Rot");
        setDescription("Targeted\nCauses your target to empty their stomach causing their hungerbar to decrease by $1 with a $2% chance to deal $3 physical damage every $4s for $5s");
        setUsage("/skill Rot");
        setArgumentRange(0, 0);
        setIdentifiers("skill Rot");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        Player target = (Player) le;
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Hero thero = plugin.getCharacterManager().getHero(target);
        Player player = hero.getPlayer();
        int fdamage = SkillConfigManager.getUseSetting(hero, this, "foodbar-damage", 10, false);
        int damage = 0;
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000L, false);
        double chance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE, 0.2, false);
        if (Math.random() <= chance) {
            damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        }
        RotEffect ee = new RotEffect(this, period, duration, damage, player, fdamage);
        if (hero.hasEffect("MasochismEffect")) {
            hero.addEffect(new RotEffect(this, period, duration, damage / 2, player, fdamage / 2));
            MasochismEffect me = (MasochismEffect) hero.getEffect("MasochismEffect");
            me.addToList(ee, duration);
            thero.addEffect(ee);
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
        thero.addEffect(ee);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set("foodbar-damage", 5);
        node.set(SkillSetting.CHANCE.name(), 0.2D);
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% expired from %target%");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int fdamage = SkillConfigManager.getUseSetting(hero, this, "foodbar-damage", 10, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000L, false);
        double chance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE, 0.2, false);
        return getDescription().replace("$1", fdamage + "").replace("$2", chance * 100 + "").replace("$3", damage + "").replace("$4", period / 1000 + "").replace("$5", duration / 1000 + "") + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% was infected with %skill%").replace("%target%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%skill% expired from %target%").replace("%target%", "$1").replace("%skill%", "$2");
    }

    public class RotEffect extends PeriodicExpirableEffect {
        private final Player caster;
        private int damage;
        private int fdamage;

        public RotEffect(Skill skill, long period, long duration, int damage, Player caster, int fdamage) {
            super(skill, "RotEffect", period, duration);
            this.caster = caster;
            this.fdamage = fdamage;
            this.damage = damage;
            types.add(EffectType.DISPELLABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Rot");
            Player player = hero.getPlayer();
            if (player.getFoodLevel() - fdamage < 0) {
                player.setFoodLevel(0);
            } else {
                player.setFoodLevel(player.getFoodLevel() - fdamage);
            }
        }

        @Override
        public void tickHero(Hero hero) {
            if (damage > 0) {
                if (damageCheck(hero.getPlayer(), caster) || ((caster.equals(hero.getPlayer())) && (!plugin.getCharacterManager().getHero(caster).hasEffectType(EffectType.INVULNERABILITY)))) {
                    damageEntity(hero.getPlayer(), caster, damage, DamageCause.CUSTOM, false);
                }
            }
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Rot");
        }

        @Override
        public void tickMonster(Monster mnstr) {
        }
    }
}