package com.atherys.skills.SkillSacrifice;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillSacrifice extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillSacrifice(Heroes plugin) {
        super(plugin, "Sacrifice");
        setDescription("Takes all damage for party for $1 seconds.");
        setArgumentRange(0, 0);
        setUsage("/skill Sacrifice");
        setIdentifiers("skill Sacrifice");
        setTypes(SkillType.PHYSICAL, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(5000));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill% on %target%!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% on %target% expired!");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        long d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        return getDescription().replace("$1", d / 1000 + "");
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill% on %target%!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%skill% on %target% expired!").replace("%hero%", "$1").replace("%target%", "$2").replace("%skill%", "$3");
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player player = hero.getPlayer();
        int radius = SkillConfigManager.getUseSetting(hero, this, "radius", 10, true);
        long duration = SkillConfigManager.getUseSetting(hero, this, "duration", 10000, false);
        if (!hero.hasParty() || (hero.getParty().getMembers().size() == 1)) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You arent in a party.");
            return SkillResult.CANCELLED;
        }
        for (Hero phero : hero.getParty().getMembers()) {
            if (phero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) < radius * radius) {
                if (!phero.equals(hero)) {
                    phero.addEffect(new SacrificeEffect(this, duration, player));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class HeroDamageListener implements Listener {
        @EventHandler
        public void onPlayerDamage(EntityDamageEvent event) {
            if (event.isCancelled() || (event.getDamage() == 0) || (!(event.getEntity() instanceof Player))) {
                return;
            }
            if (!(event instanceof EntityDamageByEntityEvent)) {
                return;
            }
            EntityDamageByEntityEvent subevent = (EntityDamageByEntityEvent) event;
            if (!(subevent.getDamager() instanceof LivingEntity)) {
                return;
            }
            Player player = (Player) subevent.getEntity();
            LivingEntity damager = (LivingEntity) subevent.getDamager();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("Sacrifice")) {
                Player caster = ((SacrificeEffect) hero.getEffect("Sacrifice")).getCaster();
                if (caster.equals(damager) || caster.equals(player))
                    return;
                damageEntity(caster, damager, event.getDamage() / 10D, event.getCause());
                event.setDamage(0);
            }
        }
    }

    public class SacrificeEffect extends ExpirableEffect {
        private final Player caster;

        public SacrificeEffect(Skill skill, long duration, Player caster) {
            super(skill, "Sacrifice", duration);
            this.caster = caster;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), applyText, caster.getDisplayName(), p.getDisplayName(), "Sacrifice");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player p = hero.getPlayer();
            broadcast(p.getLocation(), expireText, caster.getDisplayName(), p.getDisplayName(), "Sacrifice");
        }

        public Player getCaster() {
            return caster;
        }
    }
}
