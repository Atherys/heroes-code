package com.atherys.skills.SkillSalvation;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

public class SkillSalvation extends ActiveSkill {
    public SkillSalvation(Heroes plugin) {
        super(plugin, "Salvation");
        setDescription("Restore health to all nearby allies, with the heal diminishing the further your ally is away from you.");
        setUsage("/skill Salvation");
        setArgumentRange(0, 0);
        setIdentifiers("skill Salvation");
        setTypes(SkillType.SILENCABLE, SkillType.HEAL);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.AMOUNT.node(), 0.5D);
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (!hero.hasParty()) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You dont have a party.");
            return SkillResult.CANCELLED;
        }
        double amount = SkillConfigManager.getUseSetting(hero, this, SkillSetting.AMOUNT, 50D, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        for (Hero phero : hero.getParty().getMembers()) {
            if (phero.getPlayer().getWorld().equals(hero.getPlayer().getWorld())) {
                int distance = (int)Math.round(Math.sqrt(phero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation())));
                if (distance <= r) {
                    double a = amount;
                    a -= distance * 5;
                    if (a > 0) {
                        Lib.healHero(phero, a, this, hero);
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
