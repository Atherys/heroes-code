package com.atherys.skills.SkillScareTactic;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SkillScareTactic extends ActiveSkill {
    private String applytext, expiretext;

    public SkillScareTactic(Heroes plugin) {
        super(plugin, "ScareTactic");
        setDescription("Caster receives slow and a base damage buff of $1 damage for $2 seconds.");
        setUsage("/skill ScareTactic");
        setArgumentRange(0, 0);
        setIdentifiers("skill ScareTactic");
        setTypes(SkillType.BUFF, SkillType.SILENCABLE);
        Bukkit.getPluginManager().registerEvents(new DamageListener(), plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int slowmult = SkillConfigManager.getUseSetting(hero, this, "slow-multiplier", 1, false);
        int damage = SkillConfigManager.getUseSetting(hero, this, "damage-bonus", 15, false);
        hero.addEffect(new ScareTacticEffect(this, duration, slowmult, damage));
        return SkillResult.NORMAL;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% used %skill%.").replace("%hero%", "$2").replace("%skill%", "$1");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%skill% from %hero% expired.").replace("%hero%", "$2").replace("%skill%", "$1");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("slow-multiplier", 1);
        node.set("damage-bonus", 15);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% used %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% from %hero% expired.");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, "damage-bonus", 1, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", "" + damage).replace("$2", "" + duration / 1000);
    }

    public class ScareTacticEffect extends PeriodicExpirableEffect {
        private final int damage;

        public ScareTacticEffect(Skill skill, long duration, int slowmult, int damage) {
            super(skill, "ScareTactic", 1000L, duration);
            this.damage = damage;
            types.add(EffectType.BENEFICIAL);
            addMobEffect(2, (int) (duration / 50), slowmult, false);
        }

        public int getDamageBonus() {
            return damage;
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, "ScareTactic", hero.getName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, "ScareTactic", hero.getName());
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            player.getWorld().playEffect(player.getLocation().getBlock().getRelative(BlockFace.UP).getLocation(), Effect.ENDER_SIGNAL, 3);
        }
    }

    public class DamageListener implements Listener {
        @EventHandler
        public void DamageEvent(WeaponDamageEvent event) {
            if (event.isCancelled() || (event.getDamage() == 0) || (!(event.getDamager() instanceof Hero))) {
                return;
            }
            CharacterTemplate ct = event.getDamager();
            if (!(ct.hasEffect("ScareTactic"))) {
                return;
            }
            ScareTacticEffect st = (ScareTacticEffect) ct.getEffect("ScareTactic");
            event.setDamage(event.getDamage() + st.getDamageBonus());
        }
    }
}
