package com.atherys.skills.SkillScout;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/*
 * Made by pumpapa
 */
public class SkillScout extends ActiveSkill {
    public SkillScout(Heroes plugin) {
        super(plugin, "Scout");
        setArgumentRange(0, 0);
        setDescription("Shows some info about all players within a $1 blocks area around you.");
        setIdentifiers("skill scout");
        setUsage("/skill scout");
        setTypes(SkillType.SILENCABLE, SkillType.KNOWLEDGE);
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 150, false);
        return description.replace("$1", "" + r);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(150));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 150, false);
        int hit = 0;
        Messaging.send(player, "Nearby players:");
        for (Entity e : player.getNearbyEntities(r, r, r)) {
            if (e instanceof Player) {
                Player tplayer = (Player) e;
                Hero thero = plugin.getCharacterManager().getHero(tplayer);
                Messaging.send(player, "Player: " + tplayer.getName().toString() + ". Class: " + thero.getHeroClass().toString() + ". Level: " + thero.getLevel());
                hit++;
            }
        }
        Messaging.send(player, "There are " + hit + " players within " + r + " blocks from you.");
        return SkillResult.NORMAL;
    }
}