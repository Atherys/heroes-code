package com.atherys.skills.SkillSecondChance;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.ArrayList;
import java.util.List;

public class SkillSecondChance extends ActiveSkill {
    private String aurastarttext;
    private String aurastoptext;
    private String playersavetext;

    public SkillSecondChance(Heroes plugin) {
        super(plugin, "SecondChance");
        setDescription("Gives nearby partymembers $1% chance to activate reborn upon dying.");
        setUsage("/skill SecondChance");
        setArgumentRange(0, 0);
        setIdentifiers("skill SecondChance");
        Bukkit.getServer().getPluginManager().registerEvents(new HeroDamageListener(), plugin);
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        double a = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE, 0.2, false);
        return getDescription().replace("$1", a * 100 + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.COOLDOWN.name(), 10000);
        node.set(SkillSetting.CHANCE.node(), 0.2);
        node.set("mana-tick", 10);
        node.set("aurastarttext", "%hero% starts %skill%");
        node.set("aurastoptext", "%hero% stops %skill%");
        node.set("playersavetext", "%hero% was saved by $2's aura.");
        node.set("%afterrebrith", 0.5D);
        return node;
    }

    @Override
    public void init() {
        super.init();
        aurastarttext = SkillConfigManager.getRaw(this, "aurastarttext", "%hero% starts %skill%").replace("%hero%", "$1").replace("%skill%", "$2");
        aurastoptext = SkillConfigManager.getRaw(this, "aurastoptext", "%hero% stops %skill%").replace("%hero%", "$1").replace("%skill%", "$2");
        playersavetext = SkillConfigManager.getRaw(this, "playersavetext", "%hero% was saved by %caster% aura.").replace("%hero%", "$1").replace("%caster%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (hero.hasEffect("SCAura")) {
            hero.removeEffect(hero.getEffect("SCAura"));
            return SkillResult.NORMAL;
        }
        int cd = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 10000, false);
        double chance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE, 0.2, false);
        long auraperiod = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2500, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 5, false);
        int mana = SkillConfigManager.getUseSetting(hero, this, "mana-tick", 5, false);
        double amount = SkillConfigManager.getUseSetting(hero, this, "%afterrebrith", 0.2D, false);
        hero.addEffect(new SAuraEffect(this, auraperiod, chance, cd, hero.getName(), radius, amount, mana));
        return SkillResult.NORMAL;
    }

    public class SAuraEffect extends PeriodicEffect {
        private final int cd;
        private final String caster;
        private final int radius;
        private final double chance;
        private final double amount;
        private final long auraperiod;
        private final int mana;
        private final List<String> players = new ArrayList<>();

        public SAuraEffect(Skill skill, long auraperiod, double chance, int cd, String caster, int radius, double amount, int mana) {
            super(skill, "SCAura", auraperiod);
            this.caster = caster;
            this.cd = cd;
            this.amount = amount;
            this.chance = chance;
            this.mana = mana;
            this.radius = radius;
            this.auraperiod = auraperiod;
        }

        public void setNewHealthFor(Hero hero) {
            HeroRegainHealthEvent hrh = new HeroRegainHealthEvent(hero, (hero.getPlayer().getMaxHealth() * amount), skill);
            if (!hrh.isCancelled()) {
                hero.heal(hrh.getAmount());
            } else {
                hero.getPlayer().setHealth(0);
            }
        }

        public String getCaster() {
            return caster;
        }

        public int getCooldown() {
            return cd;
        }

        public boolean canSave() {
            return !(players.contains(caster) && (Math.random() <= chance));
        }

        public void removeFromList(Hero hero) {
            if (players.contains(hero.getName())) {
                players.remove(hero.getName());
            }
        }

        public void setCooldown(int cooldown) {
            int cdToTicks = cooldown / 50;
            players.add(caster);
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                @Override
                public void run() {
                    players.remove(caster);
                }
            }, cdToTicks);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), aurastarttext, hero.getName(), "Second Chance");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            removeFromList(hero);
            broadcast(hero.getPlayer().getLocation(), aurastoptext, hero.getName(), "Second Chance");
        }

        public void manacost(Hero hero) {
            if (hero.getMana() - mana <= 0) {
                hero.setMana(0);
                hero.removeEffect(hero.getEffect("SCAura"));
            } else {
                hero.setMana(hero.getMana() - mana);
            }
        }

        @Override
        public void tickHero(Hero hero) {
            super.tickHero(hero);
            if (hero.getName().equalsIgnoreCase(caster)) {
                if (hero.hasParty()) {
                    for (Hero phero : hero.getParty().getMembers()) {
                        if (phero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= radius * radius) {
                            //                         if (!phero.hasEffect("SCAura2")) {
                            phero.addEffect(new SAuraEffect2(skill, auraperiod, getCaster()));
                            //                         }
                        }
                    }
                }
            }
            if (mana != 0) {
                manacost(hero);
            }
        }
    }

    public class SAuraEffect2 extends ExpirableEffect {
        private final String caster;

        public SAuraEffect2(Skill skill, long duration, String caster) {
            super(skill, "secondchance", duration);
            this.caster = caster;
        }

        public String getCaster() {
            return caster;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }
    }

    public class HeroDamageListener implements Listener {
        @EventHandler
        public void onPlayerDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event.getEntity() instanceof Player)) || (event.getDamage() == 0)) {
                return;
            }
            Player player = (Player) event.getEntity();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!hero.hasEffect("secondchance")) {
                return;
            }
            double currentHealth = hero.getPlayer().getHealth();
            if (currentHealth > event.getDamage()) {
                return;
            }
            event.setDamage(0);
            event.setCancelled(true);
            SAuraEffect2 a2 = (SAuraEffect2) hero.getEffect("secondchance");
            Player oplayer = Bukkit.getPlayer(a2.getCaster());
            Hero chero = plugin.getCharacterManager().getHero(oplayer);
            if (!chero.hasEffect("SCAura")) {
                return;
            }
            broadcast(player.getLocation(), chero.getName());
            SAuraEffect a1 = (SAuraEffect) chero.getEffect("SCAura");
            if (a1.canSave()) {
                a1.setNewHealthFor(hero);
                broadcast(hero.getPlayer().getLocation(), playersavetext, hero.getName(), a1.getCaster());
                a1.setCooldown(a1.getCooldown());
            }
        }
    }
}
