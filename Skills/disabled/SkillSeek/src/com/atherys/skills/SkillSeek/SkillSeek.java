package com.atherys.skills.SkillSeek;

import ca.xshade.bukkit.questioner.Questioner;
import ca.xshade.questionmanager.Option;
import ca.xshade.questionmanager.Question;
import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Biome;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class SkillSeek extends ActiveSkill {
    private Questioner questioner = (Questioner) Bukkit.getServer().getPluginManager().getPlugin("Questioner");

    public SkillSeek(Heroes plugin) {
        super(plugin, "Seek");
        setDescription("While merged, the user can seek out another player who is in a forest and be teleported to them.");
        setUsage("/skill Seek [Player]");
        setArgumentRange(1, 1);
        setIdentifiers("skill seek");
        setTypes(SkillType.EARTH, SkillType.TELEPORT, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("leaves", 20);
        node.set("wood-blocks", 5);
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(8));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(300));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (!hero.hasEffect("MergeEffect")) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You must be merged with a forest to use this skill!");
            return SkillResult.CANCELLED;
        }
        Player target = plugin.getServer().getPlayer(args[0]);
        if (target == null) {
            return SkillResult.INVALID_TARGET;
        }
        int maxDist = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 300, false);
        if (target.getLocation().getBlock().getBiome() == Biome.JUNGLE || target.getLocation().getBlock().getBiome() == Biome.JUNGLE_HILLS) {
            if (target.getLocation().distance(hero.getPlayer().getLocation()) < maxDist) {
                if (hero.hasEffect("BloomEffect")) {
                    String str = args[0];
                    Hero targetHero = this.plugin.getCharacterManager().getHero(target);
                    if (targetHero.isInCombat()) {
                        hero.getPlayer().sendMessage((new StringBuilder()).append(ChatColor.GRAY).append("Target is in combat").toString());
                        return SkillResult.CANCELLED;
                    }
                    ArrayList localArrayList = new ArrayList();
                    this.questioner.loadClasses();
                    localArrayList.add(new Option("accept", new Runnable() {
                        @Override
                        public void run() {

                        }
                    }));
                    localArrayList.add(new Option("deny", new Runnable() {
                        @Override
                        public void run() {

                        }
                    }));
                    Question localQuestion = new Question(targetHero.getName().toLowerCase(), hero.getName() + " has summoned you.", localArrayList);
                    try {
                        this.questioner.appendQuestion(localQuestion);
                    } catch (Exception localException) {
                    }
                    broadcastExecuteText(hero);
                    return SkillResult.NORMAL;
                }
                hero.getPlayer().teleport(target);
                broadcastExecuteText(hero);
                return SkillResult.NORMAL;
            }
        }
        int l = SkillConfigManager.getUseSetting(hero, this, "leaves", 20, false);
        int w = SkillConfigManager.getUseSetting(hero, this, "wood-blocks", 5, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 8, false);
        int nl = 0;
        int wl = 0;
        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                for (int z = -radius; z <= radius; z++) {
                    int m = target.getLocation().getBlock().getRelative(x, y, z).getLocation().getBlock().getTypeId();
                    switch (m) {
                        case 17:
                            nl++;
                            break;
                        case 18:
                            wl++;
                            break;
                    }
                }
            }
        }
        if ((nl >= l) && (wl >= w)) {
            if (target.getLocation().distance(hero.getPlayer().getLocation()) < maxDist)
                if (hero.hasEffect("BloomEffect")) {
                    String str = args[0];
                    Hero targetHero = this.plugin.getCharacterManager().getHero(target);
                    if (targetHero.isInCombat()) {
                        hero.getPlayer().sendMessage((new StringBuilder()).append(ChatColor.GRAY).append("Target is in combat").toString());
                        return SkillResult.CANCELLED;
                    }
                    ArrayList localArrayList = new ArrayList();
                    this.questioner.loadClasses();
                    localArrayList.add(new Option("accept", new Runnable() {
                        @Override
                        public void run() {

                        }
                    }));
                    localArrayList.add(new Option("deny", new Runnable() {
                        @Override
                        public void run() {

                        }
                    }));
                    Question localQuestion = new Question(targetHero.getName().toLowerCase(), hero.getName() + " has summoned you.", localArrayList);
                    try {
                        this.questioner.appendQuestion(localQuestion);
                    } catch (Exception localException) {
                    }
                    broadcastExecuteText(hero);
                    return SkillResult.NORMAL;
                }
            hero.getPlayer().teleport(target);
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        } else {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Your target is not in this forest!");
            return SkillResult.CANCELLED;
        }
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}