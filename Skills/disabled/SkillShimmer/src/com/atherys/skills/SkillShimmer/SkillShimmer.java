package com.atherys.skills.SkillShimmer;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillShimmer extends ActiveSkill {
    public SkillShimmer(Heroes plugin) {
        super(plugin, "Shimmer");
        setDescription("Heals the party member with the lowest hp slightly, followed by a heal over time ability. Phase - Does damage over time.");
        setUsage("/skill Shimmer");
        setArgumentRange(0, 0);
        setIdentifiers("skill Shimmer");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("PhaseEffect")) {
            long dd = SkillConfigManager.getUseSetting(hero, this, "damage-duration", 10000, false);
            long dp = SkillConfigManager.getUseSetting(hero, this, "damage-period", 1000, false);
            double damage = SkillConfigManager.getUseSetting(hero, this, "damage", 10, false);
            int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
            for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
                if (e instanceof Player) {
                    Player p = (Player) e;
                    if (damageCheck(hero.getPlayer(), p)) {
                        plugin.getCharacterManager().getHero(p).addEffect(new PeriodicDamageEffect(this, "ShimmerDamage", dp, dd, damage, hero.getPlayer()));
                    }
                }
            }
        } else {
            if (hero.hasParty()) {
                Hero p = hero;
                long hd = SkillConfigManager.getUseSetting(hero, this, "heal-duration", 10000, false);
                long hp = SkillConfigManager.getUseSetting(hero, this, "heal-period", 1000, false);
                double ah = SkillConfigManager.getUseSetting(hero, this, "heal-amount", 10, false);
                double htick = SkillConfigManager.getUseSetting(hero, this, "heal-tick", 10, false);
                for (Hero phero : hero.getParty().getMembers()) {
                    if (phero.getPlayer().getHealth() <= p.getPlayer().getHealth()) {
                        p = phero;
                    }
                }
                HeroRegainHealthEvent event = new HeroRegainHealthEvent(p, ah, this, hero);
                plugin.getServer().getPluginManager().callEvent(event);
                if (!event.isCancelled()) {
                    p.heal(event.getAmount());
                    p.addEffect(new PeriodicHealEffect(this, "ShimmerHeal", hp, hd, htick, hero.getPlayer()));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("heal-amount", 10);
        node.set("heal-tick", 10);
        node.set("heal-period", 1000);
        node.set("heal-duration", 10000);
        node.set("damage", 10);
        node.set("damage-period", 1000);
        node.set("damage-duration", 10000);
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }
}
