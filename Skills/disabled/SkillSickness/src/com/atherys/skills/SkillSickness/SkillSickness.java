package com.atherys.skills.SkillSickness;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillSickness extends TargettedSkill {
    private String applyText;
    private String expireText;

    public SkillSickness(Heroes plugin) {
        super(plugin, "Sickness");
        setArgumentRange(0, 0);
        setDescription("Target becomes ill and after $1 seconds empties their stomach, dealing $2 damage and cutting their hunger-bar by $3%");
        setIdentifiers("skill sickness");
        setUsage("/skill sickness");
        setTypes(SkillType.DEBUFF, SkillType.DAMAGING, SkillType.HARMFUL, SkillType.SILENCABLE);
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 1, false);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) / 1000;
        int hungdec = (int) (SkillConfigManager.getUseSetting(hero, this, "Hunger-Decrease-Percent", 0.5, false) * 100);
        String description = getDescription().replace("$1", duration + "");
        description = description.replace("$2", damage + "");
        description = description.replace("$3", hungdec + "");
        // COOLDOWN
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description += " CD:" + cooldown + "s";
        }
        // MANA
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
        if (mana > 0) {
            description += " M:" + mana;
        }
        // HEALTH_COST
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this));
        if (healthCost > 0) {
            description += " HP:" + healthCost;
        }
        // STAMINA
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - (SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this));
        if (staminaCost > 0) {
            description += " FP:" + staminaCost;
        }
        // DELAY
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description += " W:" + delay + "s";
        }
        // EXP
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description += " XP:" + exp;
        }
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(60000));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10));
        node.set("Hunger-Decrease-Percent", Double.valueOf(0.5));
        node.set(SkillSetting.APPLY_TEXT.node(), "$1 looks sickly!");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "$1 empties their stomach!");
        return node;
    }

    @Override
    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "$1 looks sickly!");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "$1 empties their stomach!");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (target.equals(player) || !(target instanceof Player))
            return SkillResult.INVALID_TARGET;
        Hero targetHero = plugin.getCharacterManager().getHero((Player) target);
        if (hero.getParty() != null && hero.getParty().isPartyMember(targetHero))
            return SkillResult.INVALID_TARGET;
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_TICK, 1, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        double hungdec = SkillConfigManager.getUseSetting(hero, this, "Hunger-Decrease-Percent", 0.5, false);
        targetHero.addEffect(new SicknessEffect(this, duration, damage, hungdec, applyText, expireText, player));
        return SkillResult.NORMAL;
    }

    public class SicknessEffect extends ExpirableEffect {
        private final String applyText;
        private final String expireText;
        private final double damage;
        private final Player caster;
        private final double hungdec;

        public SicknessEffect(Skill skill, long duration, double damage, double hungdec, String applyText, String expireText, Player caster) {
            super(skill, "Sickness", duration);
            this.damage = damage;
            this.caster = caster;
            this.hungdec = hungdec;
            this.applyText = applyText;
            this.expireText = expireText;
            this.types.add(EffectType.PHYSICAL);
            this.types.add(EffectType.HARMFUL);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            player.sendMessage("You Feel Unwell!");
            broadcast(player.getLocation(), this.applyText, player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            damageEntity(player, caster, damage, DamageCause.MAGIC);
            int hunger = player.getFoodLevel();
            player.setFoodLevel((int) (hunger - (hunger * hungdec)));
            broadcast(player.getLocation(), this.expireText, player.getDisplayName());
        }
    }
}
