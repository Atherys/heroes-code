package com.atherys.skills.SkillSink;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class SkillSink extends TargettedSkill {
    private String applytext;
    private String expiretext;

    public SkillSink(Heroes plugin) {
        super(plugin, "Sink");
        setUsage("/skill Sink");
        setArgumentRange(0, 0);
        setIdentifiers("skill Sink");
        setDescription("Target cannot swim for $1 seconds.");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL, SkillType.DAMAGING);
    }

    @Override
    public String getDescription(Hero hero) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% expired from %target%.");
        node.set(SkillSetting.APPLY_TEXT.node(), "%target% catch %skill%.");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%skill% expired from %target%.").replace("%target%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% catch %skill%.").replace("%target%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        Hero tHero = plugin.getCharacterManager().getHero((Player)target);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        if (tHero.hasEffect("OlympicSwimmer")) {
            tHero.removeEffect(tHero.getEffect("OlympicSwimmer"));
        }
        if (tHero.hasEffect("CurrentE")) {
            tHero.removeEffect(tHero.getEffect("CurrentE"));
        }
        plugin.getCharacterManager().getHero((Player) target).addEffect(new SinkEffect(this, duration));
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    public class SinkEffect extends PeriodicExpirableEffect {
        public SinkEffect(Skill skill, long duration) {
            super(skill, "SinkEffect", 500L, duration);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "Sink");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Sink");
        }

        @Override
        public void tickMonster(Monster mnstr) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            Player tPlayer = hero.getPlayer();
            if (tPlayer.getLocation().getBlock().getType() == Material.STATIONARY_WATER) {
                tPlayer.setVelocity(tPlayer.getVelocity().add(new Vector(0, -2, 0)));
            }
        }
    }
}
