package com.atherys.skills.SkillSonicBoom;

import com.atherys.effects.SilenceEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Effect;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class SkillSonicBoom extends ActiveSkill {
    public SkillSonicBoom(Heroes plugin) {
        super(plugin, "SonicBoom");
        setDescription("You creat a clap of thunder dealing $3 magic damage and silencing everyone within $1 blocks for $2 seconds.");
        setUsage("/skill sonicboom");
        setArgumentRange(0, 0);
        setIdentifiers("skill sonicboom");
        setTypes(SkillType.FORCE, SkillType.DAMAGING, SkillType.HARMFUL);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        node.set(SkillSetting.DURATION_INCREASE.node(), Integer.valueOf(0));
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(10));
        node.set(SkillSetting.RADIUS_INCREASE.node(), Integer.valueOf(0));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(0));
        node.set(SkillSetting.DAMAGE_INCREASE.node(), Integer.valueOf(0));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 30, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS_INCREASE, 0, false) * hero.getSkillLevel(this);
        int duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 0, false) * hero.getSkillLevel(this)) / 1000;
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 0, false) + SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0, false) * hero.getSkillLevel(this);
        String description = getDescription().replace("$1", radius + "").replace("$2", duration + "").replace("$3", damage + "");
        int cooldown = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE.node(), 0, false) * hero.getSkillLevel(this)) / 1000;
        if (cooldown > 0) {
            description = description + " CD:" + cooldown + "s";
        }
        int mana = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 10, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        int healthCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST, 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.HEALTH_COST_REDUCE, mana, true) * hero.getSkillLevel(this);
        if (healthCost > 0) {
            description = description + " HP:" + healthCost;
        }
        int staminaCost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA.node(), 0, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.STAMINA_REDUCE.node(), 0, false) * hero.getSkillLevel(this);
        if (staminaCost > 0) {
            description = description + " FP:" + staminaCost;
        }
        int delay = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            description = description + " W:" + delay + "s";
        }
        int exp = SkillConfigManager.getUseSetting(hero, this, SkillSetting.EXP.node(), 0, false);
        if (exp > 0) {
            description = description + " XP:" + exp;
        }
        return description;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 30, false);
        radius += SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS_INCREASE, 0, false) * hero.getSkillLevel(this);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 0, false);
        damage += SkillConfigManager.getUseSetting(hero, this, "damage-increase", 0, false) * hero.getSkillLevel(this);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        duration += SkillConfigManager.getUseSetting(hero, this, "duration-increase", 0, false) * hero.getSkillLevel(this);
        Player player = hero.getPlayer();
        boolean hit = false;
        for (Entity e : player.getNearbyEntities(radius, radius, radius)) {
            if ((e instanceof Player)) {
                if (damageCheck(player, (Player) e)) {
                    //BetterInvis
                    Hero tHero = this.plugin.getCharacterManager().getHero((Player) e);
                    if (tHero.hasEffect("Invisible") && (hero.hasParty() && !hero.getParty().getMembers().contains(tHero))) {
                        tHero.removeEffect(tHero.getEffect("Invisible"));
                    }
                    tHero.cancelDelayedSkill();
                    damageEntity(tHero.getPlayer(), player, damage, DamageCause.MAGIC);
                    tHero.addEffect(new SilenceEffect(this, duration, true));
                    hit = true;
                }
            } else if (e instanceof LivingEntity) {
                if (damageCheck(player, (LivingEntity) e)) {
                    damageEntity((LivingEntity) e, player, damage, DamageCause.MAGIC);
                    hit = true;
                }
            }
        }
        if (!hit) {
            Messaging.send(player, "No nearby targets!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        player.getWorld().playEffect(player.getLocation(), Effect.SMOKE, 3);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}