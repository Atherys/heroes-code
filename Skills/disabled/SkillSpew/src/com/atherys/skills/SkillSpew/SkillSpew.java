package com.atherys.skills.SkillSpew;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class SkillSpew extends ActiveSkill {
    private String expireText;

    public SkillSpew(Heroes plugin) {
        super(plugin, "Spew");
        setDescription("Spews poison on all visible targets..");
        setUsage("/skill Spew");
        setArgumentRange(0, 0);
        setIdentifiers("skill Spew");
        setTypes(SkillType.FORCE, SkillType.SILENCABLE);
    }

    public static List<Entity> getEntitiesInCone(List<Entity> entities, Vector startPos, float radius, float degrees, Vector direction) {
        List<Entity> newEntities = new ArrayList<Entity>();        //    Returned list
        float squaredRadius = radius * radius;                     //    We don't want to use square root
        for (Entity e : entities) {
            Vector relativePosition = e.getLocation().toVector();                            //    Position of the entity relative to the cone origin
            relativePosition.subtract(startPos);
            if (relativePosition.lengthSquared() > squaredRadius) {
                continue;
            }                    //    First check : distance
            if (getAngleBetweenVectors(direction, relativePosition) > degrees) {
                continue;
            }    //    Second check : angle
            newEntities.add(e);                                                                //    The entity e is in the cone
        }
        return newEntities;
    }

    public static float getAngleBetweenVectors(Vector v1, Vector v2) {
        return Math.abs((float) Math.toDegrees(v1.angle(v2)));
    }

    @Override
    public void init() {
        super.init();
        expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%target% has recovered from the poison!").replace("%target%", "$1");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.RADIUS.node(), Integer.valueOf(20));
        node.set("degrees", Float.valueOf(45));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(1000L));
        node.set(SkillSetting.PERIOD.node(), Long.valueOf(1000L));
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(2));
        node.set(SkillSetting.EXPIRE_TEXT.node(), String.valueOf("%target% has recovered from the poison!"));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        List<Entity> nearbyPlayers = new ArrayList<Entity>();
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 20, false);
        Player player = hero.getPlayer();
        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
            if ((entity instanceof Player)) {
                Player newPlayer = (Player) entity;
                if (damageCheck(newPlayer, player)) {
                    nearbyPlayers.add(newPlayer);
                }
            }
        }
        Vector playerstartloc = player.getLocation().toVector();
        float r2 = (float) radius;
        float degrees = SkillConfigManager.getUseSetting(hero, this, "degrees", 45, false);
        Vector dir = player.getLocation().getDirection();
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        double tickdamage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 5, false);

        for (Entity e : getEntitiesInCone(nearbyPlayers, playerstartloc, r2, degrees, dir)) {
            if ((e instanceof LivingEntity)) {

                if ((e instanceof Player) && ((player.getEyeLocation().getDirection().angle(((LivingEntity) e).getEyeLocation().getDirection().multiply(-1)) * (180 / Math.PI)) <= 45)) {
                    plugin.getCharacterManager().getHero((Player) e).addEffect(new PEffect(this, period, duration, tickdamage, player));
                } else if ((player.getEyeLocation().getDirection().angle(((LivingEntity) e).getEyeLocation().getDirection().multiply(-1)) * (180 / Math.PI)) <= 45) {
                    plugin.getCharacterManager().getMonster((LivingEntity) e).addEffect(new PEffect(this, period, duration, tickdamage, player));
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        String description = getDescription();
        return description;
    }

    public class PEffect extends PeriodicDamageEffect {
        public PEffect(Skill skill, long period, long duration, double tickDamage, Player applier) {
            super(skill, "SpweEfect", period, duration, tickDamage, applier);
            this.types.add(EffectType.POISON);
            addMobEffect(19, (int) (duration / 1000L) * 20, 0, true);
        }

        @Override
        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You have recovered from the poison!");
        }
    }
}
