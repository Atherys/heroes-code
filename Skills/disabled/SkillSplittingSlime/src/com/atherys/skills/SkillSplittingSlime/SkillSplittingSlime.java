package com.atherys.skills.SkillSplittingSlime;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class SkillSplittingSlime extends ActiveSkill {
    public SkillSplittingSlime(Heroes plugin) {
        super(plugin, "SplittingSlime");
        setDescription("Spawn 1 slime that will multiply $1 times if not killed.");
        setUsage("/skill SplittingSlime");
        setArgumentRange(0, 0);
        setIdentifiers("skill SplittingSlime");
        setTypes(SkillType.SUMMON, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SlimeListener(), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        int mu = SkillConfigManager.getUseSetting(hero, this, "multiplier", 2, false);
        return getDescription().replace("$1", mu + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("multiplier", Integer.valueOf(2));
        node.set("multiply-delay", Integer.valueOf(5000));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        broadcastExecuteText(hero);
        Block wTargetBlock = player.getTargetBlock((Set<Material>) null, 20).getRelative(BlockFace.UP);
        final Slime le = player.getWorld().spawn(wTargetBlock.getLocation(), Slime.class);
        final Monster mob = plugin.getCharacterManager().getMonster(le);
        mob.setExperience(0);
        if (le == null || le.isDead()) {
            return SkillResult.INVALID_TARGET;
        }
        le.getWorld().playEffect(le.getLocation(), Effect.SMOKE, 3);
        int multiplyTimes = SkillConfigManager.getUseSetting(hero, this, "multiplier", 2, false);
        long multiplyDelay = (long) SkillConfigManager.getUseSetting(hero, this, "multiply-delay", 5000, false);
        multiplyDelay = multiplyDelay < 0 ? 0 : multiplyDelay;
        mob.addEffect(new slimeMultiEffect(this, multiplyDelay, multiplyTimes));
        return SkillResult.NORMAL;
    }

    public class slimeMultiEffect extends ExpirableEffect {
        private final int multiplyTimes;

        public slimeMultiEffect(Skill skill, long duration, int multiplyTimes) {
            super(skill, "SlimeMulti", duration);
            this.multiplyTimes = multiplyTimes;
        }

        public void applyToMonster(Monster monster) {
            super.applyToMonster(monster);
        }

        public void removeFromMonster(Monster monster) {
            super.removeFromMonster(monster);
            Slime le = (Slime) monster.getEntity();
            for (int i = 0; i < this.multiplyTimes; i++) {
                Slime ret = le.getWorld().spawn(le.getLocation(), Slime.class);
                plugin.getCharacterManager().getMonster(ret).setExperience(0);
            }
        }
    }

    public class SlimeListener implements Listener {
        private LinkedHashMap<Location, Long> slimes = new LinkedHashMap<Location, Long>(20) {
            private static final long serialVersionUID = 4329526013158603250L;
            private static final int MAX_ENTRIES = 20;

            protected boolean removeEldestEntry(Map.Entry<Location, Long> eldest) {
                return (size() > MAX_ENTRIES) || (eldest.getValue().longValue() + 1000L <= System.currentTimeMillis());
            }
        };

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void SlimeSplit(CreatureSpawnEvent event) {
            if (!(event.getEntity() instanceof Slime) || !(event.getSpawnReason().equals(SpawnReason.SLIME_SPLIT))) {
                return;
            }
            Slime sl = (Slime) event.getEntity();
            Monster mob = plugin.getCharacterManager().getMonster(sl);
            Location loc1 = sl.getLocation();
            Iterator<Location> it = slimes.keySet().iterator();
            while (it.hasNext()) {
                Location loc2 = it.next();
                if (loc1.distanceSquared(loc2) < 20)
                    mob.setExperience(0);
            }
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void SlimeDeath(EntityDeathEvent event) {
            if (!(event.getEntity() instanceof Slime)) {
                return;
            }
            Slime sl = (Slime) event.getEntity();
            Monster mob = plugin.getCharacterManager().getMonster(sl);
            if (mob.getExperience() == 0) {
                slimes.put(sl.getLocation(), Long.valueOf(System.currentTimeMillis()));
            }
        }
    }
}