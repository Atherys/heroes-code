package com.atherys.skills.SkillStaticEarth;


import com.atherys.heroesaddon.util.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import java.util.HashSet;

public class SkillStaticEarth extends ActiveSkill {
    //private Set<Block> blocks = new HashSet<>();
    private String applytext;
    private String expiretext;

    public SkillStaticEarth(Heroes plugin) {
        super(plugin, "StaticEarth");
        setDescription("Lightning strikes the ground and places a field on the ground that deals damage over time. Has an $1 chance to stun for a short duration.");
        setUsage("/skill StaticEarth");
        setArgumentRange(0, 0);
        setIdentifiers("skill StaticEarth");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        Player p = hero.getPlayer();
        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        HashSet<Byte> tr = new HashSet<Byte>();
        tr.add(Byte.valueOf((byte) Material.AIR.getId()));
        tr.add(Byte.valueOf((byte) Material.SNOW.getId()));
        Block b = p.getTargetBlock(tr, max);
        if (b.getType() == Material.AIR) {
            return SkillResult.CANCELLED;
        }
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        double chance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE, 0.5, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 2000, false);
        int radius = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long sduration = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 2000, false);
        hero.addEffect(new SkillStaticEarth.StaticEarthEffect(this, period, duration, b, chance, damage, radius, sduration));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set(SkillSetting.MAX_DISTANCE.node(), 15);
        node.set(SkillSetting.CHANCE.node(), 0.5);
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% placed %skill%.");
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s %skill% expired");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% placed %skill%.").replace("%hero%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public String getDescription(Hero hero) {
        double ch = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE, 0.5, false);
        return getDescription().replace("$1", ch * 100 + "");
    }

    public class StaticEarthEffect extends PeriodicExpirableEffect {
        private final Block b;
        private final double chance;
        private final double damage;
        private final int radius;
        private final long sduration;

        public StaticEarthEffect(Skill skill, long period, long duration, Block b, double chance, double damage, int radius, long sduration) {
            super(skill, "StaticEarth", period, duration);
            this.b = b;
            this.chance = chance;
            this.sduration = sduration;
            this.radius = radius;
            this.damage = damage;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            hero.getPlayer().getWorld().strikeLightningEffect(b.getLocation());
            broadcast(hero.getPlayer().getLocation(), applytext, hero.getName(), "StaticEarth");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "StaticEarth");
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void tickHero(Hero hero) {
            for (Entity e : b.getLocation().getWorld().getNearbyEntities(b.getLocation(), radius, radius, radius)) {
                if (!(e instanceof Player)) {
                    continue;
                }
                Player p = (Player) e;
                if (damageCheck(hero.getPlayer(), p)) {
                    damageEntity(p, hero.getPlayer(), damage, DamageCause.LIGHTNING);
                    if (Util.nextRand() <= chance) {
                        plugin.getCharacterManager().getHero(p).addEffect(new StunEffect(skill, sduration));
                        p.getWorld().strikeLightningEffect(p.getLocation());
                    }
                }
            }
        }
    }
}    