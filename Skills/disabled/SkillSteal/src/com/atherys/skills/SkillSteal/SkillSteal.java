package com.atherys.skills.SkillSteal;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Random;

public class SkillSteal extends TargettedSkill {
    public SkillSteal(Heroes plugin) {
        super(plugin, "Steal");
        setUsage("/skill Steal");
        setArgumentRange(0, 0);
        setIdentifiers("skill steal");
        setDescription("Steals random item from player inventory.");
        setTypes(SkillType.SILENCABLE, SkillType.ITEM);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("chance", 1D);
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        Player player = hero.getPlayer();
        if (!(target instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }

        if (target.equals(player)) {
            return SkillResult.INVALID_TARGET;
        }

        if (target.hasMetadata("NPC")) {
            return SkillResult.INVALID_TARGET;
        }

        if (target.isOp()) {
            Messaging.send(player, "Now now now...");
            player.setHealth(5);
            player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 1));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 20));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 20));
            return SkillResult.INVALID_TARGET;
        }

        Player tPlayer = (Player) target;

        if (!(damageCheck(player, target))) {
            hero.getPlayer().sendMessage(ChatColor.RED + "The target is in a No PVP zone");
            return SkillResult.INVALID_TARGET;
        }

        double chance = SkillConfigManager.getUseSetting(hero, this, "chance", 1D, false);
        if (Math.random() > chance) {
            broadcast(hero.getPlayer().getLocation(), hero.getName() + ChatColor.GRAY + " tried to steal from " + ChatColor.WHITE + tPlayer.getName() + ChatColor.GRAY + " but failed.");
            return SkillResult.NORMAL;
        }

        if (stealFromInventorySlots(tPlayer, player)) {
            if (hero.hasEffect("VanishEff")) {
                Effect e = hero.getEffect("VanishEff");
                hero.removeEffect(e);
            }
            long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 10000, false);
            StealEffect sEff = new StealEffect(this, duration, tPlayer);
            hero.addEffect(sEff);
            return SkillResult.NORMAL;
        } else {
            return SkillResult.FAIL;
        }
    }

    public class StealEffect extends ExpirableEffect {
        private final Player tPlayer;

        public StealEffect(Skill skill, long duration, Player tPlayer) {
            super(skill, "Steal", duration);
            this.tPlayer = tPlayer;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            Messaging.send(player, "You used Steal on " + tPlayer.getName() + ".");
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Messaging.send(tPlayer, "Your pockets feel lighter.");
        }
    }

    @SuppressWarnings({"deprecation"})
    private boolean stealFromInventorySlots(Player target, Player player) {
        ItemStack[] itemslots = target.getInventory().getContents();
        Random r = new Random();
        int slots = itemslots.length - 9;
        int firstslot = r.nextInt(slots) + 9;
        ItemStack stuff = null;
        int index = 0;
        for (int i = 0; i < slots; i++) {
            index = ((i + firstslot) % slots) + 9;
            stuff = itemslots[index];
            if (stuff != null) {
                break;
            }
        }
        if (stuff == null)
            return false;
        target.getInventory().clear(index);
        target.updateInventory();
        HashMap<Integer, ItemStack> leftOvers = player.getInventory().addItem(stuff);
        for (ItemStack is : leftOvers.values()) {
            player.getWorld().dropItemNaturally(player.getLocation(), is);
        }
        player.updateInventory();
        return true;
    }
}
