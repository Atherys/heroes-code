package com.atherys.skills.SkillSummonUndead;

/**
 * Created by JCastro on 5/14/2015.
 */

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import net.elseland.xikage.MythicMobs.Mobs.ActiveMob;
import net.elseland.xikage.MythicMobs.Mobs.ActiveMobHandler;
import net.elseland.xikage.MythicMobs.Mobs.MobSpawner;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class SkillSummonUndead extends TargettedSkill {

    public SkillSummonUndead(Heroes plugin) {
        super(plugin, "SummonUndead");
        setUsage("/skill SummonUndead");
        setArgumentRange(0, 0);
        setIdentifiers("skill SummonUndead");
        setDescription("Summon up to $1 $2 to attack another player");
        setTypes(SkillType.SILENCABLE);
    }

    public String getDescription(Hero hero) {
        int mobcount = SkillConfigManager.getUseSetting(hero, this, "mobcount", 2, false);
        String mobdisplay = SkillConfigManager.getUseSetting(hero, this, "MobDisplay", "Reaper Zombie");
        String mobtype = SkillConfigManager.getUseSetting(hero, this, "MobType", "HeroReaper");
        return getDescription().replace("$1", mobcount + "").replace("$2", mobdisplay);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("mobcount", Integer.valueOf(2));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity livingEntity, String[] strings) {
        if ((livingEntity instanceof Player) && (livingEntity != hero.getEntity()) && damageCheck(hero.getPlayer(), livingEntity)) {
            Player target = ((Player) livingEntity).getPlayer();
            int mobcount = SkillConfigManager.getUseSetting(hero, this, "mobcount", 2, false);
            long removalDuration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 45000, false);
            String MobType = SkillConfigManager.getUseSetting(hero, this, "MobType", "HeroReaper");
            Hero tHero = this.plugin.getCharacterManager().getHero(target);
            Set<ActiveMob> mobs = new HashSet<>();

            for (int mcount = 0; mcount < mobcount; mcount++) {
                Entity reaper = MobSpawner.SpawnMythicMob(MobType, target.getLocation());
                plugin.getCharacterManager().getMonster((LivingEntity) reaper).setExperience(0);
                if (ActiveMobHandler.isRegisteredMob(reaper)) {
                    ActiveMob am = ActiveMobHandler.getMythicMobInstance(reaper);
                    mobs.add(am);
                    if (am.getThreatTable() != null) {
                        am.getThreatTable().threatGain(target, 1000000);
                    }
                }
                if (tHero.hasParty()) {
                    for (Hero x : tHero.getParty().getMembers()) {
                        if (ActiveMobHandler.isRegisteredMob(reaper)) {
                            ActiveMob am = ActiveMobHandler.getMythicMobInstance(reaper);
                            if (am.getThreatTable() != null) {
                                am.getThreatTable().threatGain(x.getPlayer(), 100000);
                            }
                        }
                    }
                }
                ((Creature) reaper).setTarget(target);
            }
            hero.addEffect(new MythicMobUndeadRemovalEffect(this, removalDuration, mobs));
            broadcastExecuteText(hero, livingEntity);
            return SkillResult.NORMAL;
        }
        return SkillResult.CANCELLED;
    }

    public class MythicMobUndeadRemovalEffect extends ExpirableEffect {
        private Set<ActiveMob> mobs = new HashSet<>();

        public MythicMobUndeadRemovalEffect(Skill skill, long duration, Set<ActiveMob> mobs) {
            super(skill, "MythicMobUndeadRemovalEffect", duration);
            this.mobs = mobs;
            this.types.add(EffectType.UNBREAKABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            for (ActiveMob x : mobs) {
                if (x != null) {
                    x.setDead();
                    ActiveMobHandler.unregisterActiveMob(x);
                    x.getEntity().remove();
                }
            }
            broadcast(hero.getPlayer().getLocation(), hero.getPlayer().getDisplayName() + "'s mobs have expired!");
        }
    }
}
