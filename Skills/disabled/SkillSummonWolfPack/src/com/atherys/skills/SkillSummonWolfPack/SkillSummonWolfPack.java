package com.atherys.skills.SkillSummonWolfPack;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;

import java.util.Collection;
import java.util.Set;

public class SkillSummonWolfPack extends ActiveSkill {
    public SkillSummonWolfPack(Heroes plugin) {
        super(plugin, "SummonWolfPack");
        setDescription("100% chance to spawn 1 wolf, $1% for 2, and $2% for 3.");
        setUsage("/skill wolfpack");
        setArgumentRange(0, 0);
        setIdentifiers("skill wolfpack", "skill summonwolfpack");
        setTypes(SkillType.SUMMON, SkillType.SILENCABLE, SkillType.EARTH);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection defaultConfig = super.getDefaultConfig();
        defaultConfig.set(SkillSetting.MAX_DISTANCE.node(), 5.0);
        defaultConfig.set(SkillSetting.MAX_DISTANCE_INCREASE.node(), 0.1);
        defaultConfig.set(SkillSetting.REAGENT.node(), 352);
        defaultConfig.set(SkillSetting.REAGENT_COST.node(), 0);
        defaultConfig.set("base-chance-2x", 0.75);
        defaultConfig.set("chance-2x-per-level", 0.0);
        defaultConfig.set("base-chance-3x", 0.5);
        defaultConfig.set("chance-3x-per-level", 0.0);
        return defaultConfig;
    }

    public String getDescription(Hero hero) {
        StringBuilder descr = new StringBuilder(getDescription()
                .replace("$1", Util.stringDouble(getChance(hero, 2) * 100))
                .replace("$2", Util.stringDouble(getChance(hero, 3) * 100))
        );
        int cd = getCooldown(hero);
        if (cd > 0) {
            descr.append(" CD:");
            descr.append(Util.stringDouble(cd / 1000.0));
            descr.append("s");
        }
        int mana = getMana(hero);
        if (mana > 0) {
            descr.append(" M:");
            descr.append(mana);
        }
        descr.append(" MaxDist:");
        descr.append(String.valueOf(getMaxDistance(hero)));
        return descr.toString();
    }

    public int getMaxDistance(Hero hero) {
        return (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 5.0, false) +
                SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE_INCREASE, 0.1, false) * hero.getSkillLevel(this));
    }

    public int getCooldown(Hero hero) {
        return Math.max(0, SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 0, true) -
                SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN_REDUCE, 0, false) * hero.getSkillLevel(this));
    }

    public int getMana(Hero hero) {
        return (int) Math.max(0.0, SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA, 0.0, true) -
                SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE, 0.0, false) * hero.getSkillLevel(this));
    }

    public double getChance(Hero hero, int count) {
        String countString = String.valueOf(count);
        double chance = SkillConfigManager.getUseSetting(hero, this, "base-chance-$x".replace("$", countString), count == 2 ? 0.75 : 0.5, false) +
                SkillConfigManager.getUseSetting(hero, this, "chance-$x-per-level".replace("$", countString), 0.0, false) * hero.getSkillLevel(this);
        return Math.min(chance, 1.0);
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double chance2x = getChance(hero, 2);
        double chance3x = getChance(hero, 3);
        int maxDistance = getMaxDistance(hero);
        Block tloc = player.getTargetBlock((Set<Material>) null, maxDistance);
        if (tloc.getRelative(BlockFace.UP, 2).getType().equals(Material.AIR)) {
            // find owned wolves within 100 blocks
            Collection<Entity> fwolf = player.getNearbyEntities(100.00, 100.00, 100.00);
            for (Entity e : fwolf) {
                if (e.getType().equals(EntityType.WOLF)) {
                    Wolf w = (Wolf) e;
                    if (w.getOwner().equals(player)) {
                        e.remove();
                    }
                }
            } //end for each entity in wolf collection
            int wolfnum = 1;
            double randroll = Math.random();
            if (randroll < chance2x) {
                wolfnum = 2;
            } else if (randroll < chance3x) {
                wolfnum = 3;
            }
            for (int i = 0; i < wolfnum; i++) {
                Wolf swolf = (Wolf) player.getWorld().spawnEntity(tloc.getLocation(), EntityType.WOLF);
                swolf.setCollarColor(DyeColor.GREEN);
                swolf.setAdult();
                swolf.setCustomName(player.getName() + "'s Wolf");
                swolf.setCustomNameVisible(true);
                plugin.getCharacterManager().getMonster(swolf).setExperience(0);
                swolf.setOwner(player);
            } // spawn wolves for each number
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;


        } //end if can spawn mob
        else {
            player.sendMessage(ChatColor.RED + "Sorry you can't use this skill at this location!");
            return SkillResult.CANCELLED;
        }
    }
}
