package com.atherys.skills.SkillSuperheat;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class SkillSuperheat extends ActiveSkill {
    private String applyText;
    private String expireText;

    public SkillSuperheat(Heroes plugin) {
        super(plugin, "Superheat");
        setDescription("Your pickaxe smelts ores as you mine them for $1 seconds.");
        setUsage("/skill superheat");
        setArgumentRange(0, 0);
        setIdentifiers("skill superheat");
        setTypes(SkillType.FIRE, SkillType.EARTH, SkillType.BUFF, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillPlayerListener(), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection section = super.getDefaultConfig();
        section.set(SkillSetting.DURATION.node(), Integer.valueOf(20000));
        section.set(SkillSetting.APPLY_TEXT.node(), "%hero%'s pick has become superheated!");
        section.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s pick has cooled down!");
        return section;
    }

    public void init() {
        super.init();
        this.applyText = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero%'s pick has become superheated!").replace("%hero%", "$1");
        this.expireText = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero%'s pick has cooled down!").replace("%hero%", "$1");
    }

    public SkillResult use(Hero hero, String[] args) {
        broadcastExecuteText(hero);
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        hero.addEffect(new SuperheatEffect(this, duration));
        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 20000, false);
        return getDescription().replace("$1", duration / 1000 + "");
    }

    public class SuperheatEffect extends ExpirableEffect {
        public SuperheatEffect(Skill skill, long duration) {
            super(skill, "Superheat", duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.FIRE);
            this.types.add(EffectType.MAGIC);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillSuperheat.this.applyText, player.getDisplayName());
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), SkillSuperheat.this.expireText, player.getDisplayName());
        }
    }

    public class SkillPlayerListener
            implements Listener {
        public SkillPlayerListener() {
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onBlockBreak(BlockBreakEvent event) {
            if (event.isCancelled()) {
                return;
            }
            Hero hero = SkillSuperheat.this.plugin.getCharacterManager().getHero(event.getPlayer());
            if (hero.hasEffect("Superheat")) {
                Block block = event.getBlock();
                switch (block.getType().ordinal()) {
                    case 15:
                        event.setCancelled(true);
                        block.setType(Material.AIR);
                        block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.IRON_INGOT, 1));
                        break;
                    case 14:
                        event.setCancelled(true);
                        block.setType(Material.AIR);
                        block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.GOLD_INGOT, 1));
                        break;
                    case 20:
                        event.setCancelled(true);
                        block.setType(Material.AIR);
                        block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.GLASS, 1));
                        break;
                    case 4:
                        event.setCancelled(true);
                        block.setType(Material.AIR);
                        block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STONE, 1));
                        break;
                }
            }
        }
    }
}