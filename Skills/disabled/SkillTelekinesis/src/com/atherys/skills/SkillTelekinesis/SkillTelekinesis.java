package com.atherys.skills.SkillTelekinesis;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class SkillTelekinesis extends ActiveSkill {
    public SkillTelekinesis(Heroes plugin) {
        super(plugin, "Telekinesis");
        setDescription("You can activate levers, buttons and other interactable objects from afar.");
        setUsage("/skill telekinesis");
        setArgumentRange(0, 0);
        setIdentifiers("skill telekinesis");
        setTypes(SkillType.FORCE, SkillType.SILENCABLE);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(15));
        return node;
    }

    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        HashSet<Byte> transparent = new HashSet<Byte>();
        transparent.add(Byte.valueOf((byte) Material.AIR.getId()));
        transparent.add(Byte.valueOf((byte) Material.WATER.getId()));
        transparent.add(Byte.valueOf((byte) Material.REDSTONE_TORCH_ON.getId()));
        transparent.add(Byte.valueOf((byte) Material.REDSTONE_TORCH_OFF.getId()));
        transparent.add(Byte.valueOf((byte) Material.REDSTONE_WIRE.getId()));
        transparent.add(Byte.valueOf((byte) Material.TORCH.getId()));
        transparent.add(Byte.valueOf((byte) Material.SNOW.getId()));
        org.bukkit.block.Block block = player.getTargetBlock(transparent, SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 15, false));
        if ((block.getType() == Material.LEVER) || (block.getType() == Material.STONE_BUTTON) || (block.getType() == Material.WOOD_BUTTON)) {
            byte state = block.getData();
            block.setData((byte) (state ^ 0x8), true);
            broadcastExecuteText(hero);
            return SkillResult.NORMAL;
        }
        Messaging.send(player, "You must target a lever or button!");
        return SkillResult.INVALID_TARGET_NO_MSG;
    }

    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }
}
