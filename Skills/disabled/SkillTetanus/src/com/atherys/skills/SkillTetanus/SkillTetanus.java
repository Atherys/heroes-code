package com.atherys.skills.SkillTetanus;

import com.atherys.effects.Disaster;
import com.atherys.effects.MasochismEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.common.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SkillTetanus extends TargettedSkill {
    private String applytext;
    private String expiretext;

    public SkillTetanus(Heroes plugin) {
        super(plugin, "Tetanus");
        setDescription("Causes periodic stuns every few seconds accompanied by a DoT.");
        setUsage("/skill Tetanus");
        setArgumentRange(0, 0);
        setIdentifiers("skill Tetanus");
        setTypes(SkillType.SILENCABLE, SkillType.HARMFUL);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity le, String[] strings) {
        if (!(le instanceof Player)) {
            return SkillResult.INVALID_TARGET;
        }
        Player target = (Player) le;
        if (target.equals(hero.getPlayer())) {
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Hero thero = plugin.getCharacterManager().getHero(target);
        Player player = hero.getPlayer();
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
        long duration = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000L, false);
        long period = (long) SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10000L, false);
        long stunduration = (long) SkillConfigManager.getUseSetting(hero, this, "stun-duration", 500L, false);
        TetanusEffect te = new TetanusEffect(this, period, duration, damage, player, stunduration);
        if (hero.hasEffect("MasochismEffect")) {
            hero.addEffect(new TetanusEffect(this, period, duration, damage / 2, player, 0));
            MasochismEffect me = (MasochismEffect) hero.getEffect("MasochismEffect");
            me.addToList(te, duration);
            thero.addEffect(te);
            broadcastExecuteText(hero, target);
            return SkillResult.NORMAL;
        }
        thero.addEffect(te);
        broadcastExecuteText(hero, target);
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 10);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set("stun-duration", 500);
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%skill% expired from %target%");
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%target% was infected with %skill%").replace("%target%", "$1").replace("%skill%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%skill% expired from %target%").replace("%target%", "$1").replace("%skill%", "$2");
    }

    public class TetanusEffect extends Disaster {
        private final long stunduration;
        private final Player caster;

        public TetanusEffect(Skill skill, long period, long duration, int damage, Player caster, long stunduration) {
            super(skill, "TetanusEffect", period, duration, damage, caster);
            this.stunduration = stunduration;
            this.caster = caster;
            types.add(EffectType.DISPELLABLE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, caster.getName(), "Tetanus");
        }

        @Override
        public void tickHero(Hero hero) {
            super.tickHero(hero);
            hero.addEffect(new StunEffect(skill, stunduration));
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, hero.getName(), "Tetanus");
        }

        @Override
        public void tickMonster(Monster mnstr) {
        }
    }
}
