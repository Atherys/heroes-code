package com.atherys.skills.SkillTide;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

public class SkillTide extends ActiveSkill {
    public SkillTide(Heroes plugin) {
        super(plugin, "Tide");
        setDescription("Heals party over time. Phase: knocksback enemies and deals damage.");
        setUsage("/skill Tide");
        setArgumentRange(0, 0);
        setIdentifiers("skill Tide");
        setTypes(SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        if (hero.hasEffect("PhaseEffect")) {
            int v1 = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 1, false);
            int v2 = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 1, false);
            double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 10, false);
            for (Entity e : hero.getPlayer().getNearbyEntities(r, r, r)) {
                if (e instanceof LivingEntity) {
                    LivingEntity l = (LivingEntity) e;
                    if (damageCheck(hero.getPlayer(), l)) {
                        Vector v = l.getLocation().toVector().subtract(hero.getPlayer().getLocation().toVector()).normalize();
                        v = v.multiply(v1);
                        v = v.setY(v2);
                        l.setVelocity(v);
                        damageEntity(l, hero.getPlayer(), damage, DamageCause.MAGIC);
                    }
                }
            }
        } else {
            if (hero.hasParty()) {
                long hd = SkillConfigManager.getUseSetting(hero, this, "heal-duration", 10000, false);
                long hp = SkillConfigManager.getUseSetting(hero, this, "heal-period", 1000, false);
                double ah = SkillConfigManager.getUseSetting(hero, this, "heal-amount", 10, false);
                for (Hero phero : hero.getParty().getMembers()) {
                    if (phero.getPlayer().getLocation().distanceSquared(hero.getPlayer().getLocation()) <= r * r) {
                        phero.addEffect(new PeriodicHealEffect(this, "TideHealEffect", hp, hd, ah, hero.getPlayer()));
                    }
                }
            }
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(10));
        node.set("vertical-vector", 2);
        node.set("horizontal-vector", 1);
        node.set(SkillSetting.RADIUS.node(), 10);
        node.set("heal-amount", 10);
        node.set("heal-period", 1000);
        node.set("heal-duration", 10000);
        return node;
    }
}
