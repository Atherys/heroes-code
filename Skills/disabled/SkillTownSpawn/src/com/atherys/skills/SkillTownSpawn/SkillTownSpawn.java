package com.atherys.skills.SkillTownSpawn;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.palmergames.bukkit.towny.exceptions.TownyException;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Created by NeumimTo on 14.5.14.
 */
public class SkillTownSpawn extends ActiveSkill implements Listener {

    public SkillTownSpawn(Heroes plugin) {
        super(plugin, "TownSpawn");
        setDescription("Use this skill to Teleport back to your Town's Spawn Block");
        setUsage("/skill townspawn");
        setArgumentRange(0, 1);
        setIdentifiers("skill townspawn", "t spawn");
        setTypes(SkillType.TELEPORT, SkillType.SILENCABLE);
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        String name = hero.getName();
        Location loc = null;
        Town t = null;

        if (hero.isInCombat()) {
            hero.getPlayer().sendMessage(ChatColor.RED + "You cannot use this skill while in combat.");
            return SkillResult.CANCELLED;
        } else {
            try {
                t = TownyUniverse.getDataSource().getResident(name).getTown();
                loc = t.getSpawn();
            } catch (TownyException | NullPointerException e) {
                e.printStackTrace();
                return SkillResult.FAIL;
            }
            if (t == null) {
                hero.getPlayer().sendMessage(ChatColor.GRAY + "You are not a resident of any town.");
                return SkillResult.CANCELLED;
            }
            if (loc.getY() > 0) {
                hero.getPlayer().teleport(loc, PlayerTeleportEvent.TeleportCause.PLUGIN);
                broadcastExecuteText(hero);
                return SkillResult.NORMAL;
            } else {
                hero.getPlayer().sendMessage(ChatColor.RED + "Your current location is not valid.");
                return SkillResult.FAIL;
            }
        } //end else not in combat
    }

    @Override
    public String getDescription(Hero hero) {
        return super.getDescription();
    }

}
