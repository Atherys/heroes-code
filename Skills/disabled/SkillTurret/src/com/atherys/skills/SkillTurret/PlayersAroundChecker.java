 package com.atherys.skills.SkillTurret;

 import org.bukkit.Bukkit;
 import org.bukkit.Location;
 import org.bukkit.entity.Player;
 import org.bukkit.plugin.Plugin;

 import java.util.*;

 public class PlayersAroundChecker
 {
   private final List<Location> playersLoc = new ArrayList<Location>();
   private final Map<Location, Integer> minDistanceCache = new HashMap<Location, Integer>();
 
   public PlayersAroundChecker(Plugin plugin, int period) {
     PlayersLocPoller poller = new PlayersLocPoller();
 
     poller.run();
     Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, poller, period, period);
   }
 
   public boolean isAnyPlayerAround(Location loc, int radius)
   {
     if (this.playersLoc.isEmpty()) return false;
 
     Integer cachedMinDist = this.minDistanceCache.get(loc);
     if (cachedMinDist == null) {
       int minDist = 2147483647;
       for (Location playerLoc : this.playersLoc) {
         if (playerLoc.getWorld().equals(loc.getWorld())) {
           int curDist = (int)playerLoc.distance(loc);
           if (curDist < minDist) {
             minDist = curDist;
           }
         }
       }
       cachedMinDist = Integer.valueOf(minDist);
       this.minDistanceCache.put(loc, cachedMinDist);
     }
 
     return cachedMinDist.intValue() <= radius;
   }
   private class PlayersLocPoller implements Runnable {
     private PlayersLocPoller() {
     }
 
     public void run() {
       PlayersAroundChecker.this.playersLoc.clear();
       PlayersAroundChecker.this.minDistanceCache.clear();
 
       Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
       for (Player player : players)
         PlayersAroundChecker.this.playersLoc.add(player.getLocation());
     }
   }
 }




