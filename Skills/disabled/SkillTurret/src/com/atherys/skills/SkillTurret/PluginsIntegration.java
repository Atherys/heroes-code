 package com.atherys.skills.SkillTurret;

 import com.palmergames.bukkit.towny.Towny;
 import com.palmergames.bukkit.towny.object.Nation;
 import com.palmergames.bukkit.towny.object.Resident;
 import com.palmergames.bukkit.towny.object.Town;
 import com.palmergames.bukkit.towny.object.TownyUniverse;
 import org.bukkit.Bukkit;
 import org.bukkit.entity.Player;
 import org.bukkit.plugin.Plugin;

 import java.util.logging.Level;

 public class PluginsIntegration
   implements Runnable
 {
   private boolean checkTowny;
   private boolean checkFactions;
   private boolean usingTowny = false;
   private TownyUniverse townyUniv;
 
   public static final PluginsIntegration instance = new PluginsIntegration();
 
   public void init(boolean checkTowny, boolean checkFactions)
   {
     this.checkTowny = checkTowny;
     this.checkFactions = checkFactions;
   }
 
   public boolean isEnabled() {
     return (this.checkTowny) || (this.checkFactions);
   }
 
   public boolean isFriendly(String playerName, Player targetPlayer) {
     if (!isEnabled()) return false;
 
     return (isTownyFriendly(playerName, targetPlayer));
   }
 
   private boolean isTownyFriendly(String playerName, Player targetPlayer) {
     if (!this.usingTowny) return false;
 
     Resident playerResident = this.townyUniv.getResidentMap().get(playerName.toLowerCase());
     Resident targetResident = this.townyUniv.getResidentMap().get(targetPlayer.getName().toLowerCase());
     if ((playerResident == null) || (targetResident == null)) return false;
 
     if (playerResident.hasFriend(targetResident)) return true;
     try
     {
       if ((playerResident.hasTown()) && (targetResident.hasTown())) {
         Town playerTown = playerResident.getTown();
         Town targetTown = targetResident.getTown();
         if (playerTown == targetTown)
           return true;
         if ((playerTown.hasNation()) && (targetTown.hasNation())) {
           Nation playerNation = playerTown.getNation();
           Nation targetNation = targetTown.getNation();
           if ((playerNation == targetNation) || (playerNation.hasAlly(targetNation))) {
             return true;
           }
         }
       }
     }
     catch (Exception ex)
     {
     }
     return false;
   }
 
   public void run()
   {
     if (this.checkTowny) {
       Plugin townyPlugin = Bukkit.getPluginManager().getPlugin("Towny");
       if ((townyPlugin != null) && ((townyPlugin instanceof Towny))) {
         this.townyUniv = ((Towny)townyPlugin).getTownyUniverse();
         this.usingTowny = true;
         Utils.log("Using Towny", Level.INFO);
       } else {
         Utils.log("Towny integration enabled in config, but there is no Towny", Level.INFO);
       }
     }
   }
 }




