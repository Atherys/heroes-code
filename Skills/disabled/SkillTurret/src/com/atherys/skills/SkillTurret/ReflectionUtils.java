 package com.atherys.skills.SkillTurret;

 import org.bukkit.entity.Arrow;

 import java.lang.reflect.Field;
 import java.lang.reflect.Method;

 public class ReflectionUtils
 {
   private static Method getArrowHandleMethod = null;
 
   private static Field arrowDamageField = null;
 
   private static Field arrowFromPlayerField = null;
 
   public static Object getNMSArrow(Arrow arrow)
     throws Exception
   {
     if (getArrowHandleMethod == null) {
       getArrowHandleMethod = arrow.getClass().getMethod("getHandle");
     }
     return getArrowHandleMethod.invoke(arrow);
   }
 
   public static void setArrowDamage(Arrow arrow, float damage)
   {
     try
     {
       Object nmsArrow = getNMSArrow(arrow);
       if (arrowDamageField == null) {
         arrowDamageField = nmsArrow.getClass().getDeclaredField("damage");
         arrowDamageField.setAccessible(true);
       }
       arrowDamageField.setDouble(nmsArrow, damage);
     } catch (Exception ex) {
       ex.printStackTrace();
     }
   }
 
   public static void setNotPickupable(Arrow arrow)
   {
     try
     {
       Object nmsArrow = getNMSArrow(arrow);
       if (arrowFromPlayerField == null) {
         arrowFromPlayerField = nmsArrow.getClass().getDeclaredField("fromPlayer");
         arrowFromPlayerField.setAccessible(true);
       }
       arrowFromPlayerField.setInt(nmsArrow, 0);
     } catch (Exception ex) {
       ex.printStackTrace();
     }
   }
 }




