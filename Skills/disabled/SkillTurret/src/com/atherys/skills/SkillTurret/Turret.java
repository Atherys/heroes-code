 package com.atherys.skills.SkillTurret;

 import com.herocraftonline.heroes.characters.Hero;
 import com.herocraftonline.heroes.characters.effects.EffectType;
 import com.herocraftonline.heroes.characters.party.HeroParty;
 import com.herocraftonline.heroes.characters.skill.Skill;
 import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
 import com.palmergames.bukkit.towny.utils.CombatUtil;
 import org.bukkit.*;
 import org.bukkit.block.Block;
 import org.bukkit.block.BlockState;
 import org.bukkit.block.Dispenser;
 import org.bukkit.entity.*;
 import org.bukkit.util.Vector;

 import java.util.*;

 public class Turret
 {
   //private static final float Y_OFFSET_OVER_BASE_BLOCK = 1.6F;
   //private static final float RADIUS = 25.0F;
   //private static final int ATTACK_TARGET_FOR_TICKS = 80;
   //private static final int FIND_TARGET_EVERY_TICKS = 30;
   private final String ownerName;
   private Hero owner;
   private final Block baseBlock;
   private final Location loc;
   private final World world;
   private LivingEntity target = null;
   private long startAttackCurrentTargetTick = 0L;
   private long lastFindTick = 0L;
   private final float damage;
   private final boolean fireArrow;
   private final int attackPeriodTicks;
   private long lastAttackTick = 0L;
   private final boolean unlimitedAmmo;
   private int ammo;
   private int ammoPerArrow;
   private final boolean persistent;
   private long endLifeTick;
   private boolean destroyOnOwnerLogout;
   private boolean destroyOnChunkUnload;
   private final boolean lifetimeProtection;
   private boolean targetingNpc = true;
   private boolean targetingPlayer = true;
   private boolean targetingMonster = true;
   private boolean targetingAnimal = true;
 
   public Turret(Hero owner, Block base, SkillTurret skill) {
     this.ownerName = owner.getPlayer().getName();
     this.owner = owner;
     this.baseBlock = base;
     this.loc = base.getLocation().add(0.5D, 1.600000023841858D, 0.5D);
     this.world = this.loc.getWorld();
 
     this.attackPeriodTicks = SkillConfigManager.getUseSetting(owner, skill, "power.attack-period-ticks", 20, true);
     this.damage = SkillConfigManager.getUseSetting(owner, skill, "power.damage", 3, true);
     this.fireArrow = SkillConfigManager.getUseSetting(owner, skill, "power.fire-arrow", false);
     this.unlimitedAmmo = skill.isUnlimitedAmmoFor(owner);
     if (!this.unlimitedAmmo) {
       this.ammo = skill.getInitialAmmoFor(owner);
       this.ammoPerArrow = skill.getAmmoPerArrowFor(owner);
     }
 
     this.persistent = skill.isPersistentFor(owner);
     if (!this.persistent) {
       this.endLifeTick = (this.world.getFullTime() + skill.getTurretLifetimeFor(owner) / 50);
       this.destroyOnOwnerLogout = SkillConfigManager.getUseSetting(owner, skill, "lifetime.destroy-on-owner-logout", true);
       this.destroyOnChunkUnload = SkillConfigManager.getUseSetting(owner, skill, "lifetime.destroy-on-chunk-unload", true);
     }
     this.lifetimeProtection = skill.isLifetimeProtectedFor(owner);
 
     this.targetingNpc = SkillConfigManager.getUseSetting(owner, skill, "targeting.npc", true);
     this.targetingPlayer = SkillConfigManager.getUseSetting(owner, skill, "targeting.player", true);
     this.targetingMonster = SkillConfigManager.getUseSetting(owner, skill, "targeting.monster", true);
     this.targetingAnimal = SkillConfigManager.getUseSetting(owner, skill, "targeting.animal", true);
   }
 
   public Turret(Map<?, ?> turretData) {
     this.ownerName = ((String)turretData.get("owner-name"));
     this.owner = null;
 
     Location baseBlockLoc = Utils.deserializeLocation((Map<?, ?>)turretData.get("base-block-loc"));
     this.loc = baseBlockLoc.clone().add(0.5D, 1.600000023841858D, 0.5D);
     this.world = baseBlockLoc.getWorld();
     this.baseBlock = this.world.getBlockAt(baseBlockLoc);
 
     this.attackPeriodTicks = ((Number)turretData.get("attack-period-ticks")).intValue();
     this.damage = ((Number)turretData.get("damage")).floatValue();
     this.fireArrow = ((Boolean)turretData.get("fire-arrow")).booleanValue();
 
     this.unlimitedAmmo = ((Boolean)turretData.get("unlimited-ammo")).booleanValue();
     if (!this.unlimitedAmmo) {
       this.ammo = ((Number)turretData.get("ammo")).intValue();
       this.ammoPerArrow = ((Number)turretData.get("ammo-per-arrow")).intValue();
     }
 
     this.persistent = true;
     if (turretData.containsKey("lifetime-protection"))
       this.lifetimeProtection = ((Boolean)turretData.get("lifetime-protection")).booleanValue();
     else {
       this.lifetimeProtection = true;
     }
 
     Map<String,Boolean> targeting = (Map<String, Boolean>) turretData.get("targeting");
     if (targeting != null) {
       this.targetingNpc = targeting.get("npc").booleanValue();
       this.targetingPlayer = targeting.get("player").booleanValue();
       this.targetingMonster = targeting.get("monster").booleanValue();
       this.targetingAnimal = targeting.get("animal").booleanValue();
     }
   }
 
   public Map<String, Object> getDataEntry() {
     if (!this.persistent) return null;
 
     Map<String,Object> dataEntry = new LinkedHashMap<String,Object>();
 
     dataEntry.put("owner-name", this.ownerName);
     dataEntry.put("base-block-loc", Utils.serializeLocation(this.baseBlock.getLocation()));
 
     dataEntry.put("attack-period-ticks", Integer.valueOf(this.attackPeriodTicks));
     dataEntry.put("damage", Float.valueOf(this.damage));
     dataEntry.put("fire-arrow", Boolean.valueOf(this.fireArrow));
 
     dataEntry.put("unlimited-ammo", Boolean.valueOf(this.unlimitedAmmo));
     if (!this.unlimitedAmmo) {
       dataEntry.put("ammo", Integer.valueOf(this.ammo));
       dataEntry.put("ammo-per-arrow", Integer.valueOf(this.ammoPerArrow));
     }
 
     dataEntry.put("lifetime-protection", Boolean.valueOf(this.lifetimeProtection));
 
     Map<String,Boolean> targeting = new LinkedHashMap<String,Boolean>();
     targeting.put("npc", Boolean.valueOf(this.targetingNpc));
     targeting.put("player", Boolean.valueOf(this.targetingPlayer));
     targeting.put("monster", Boolean.valueOf(this.targetingMonster));
     targeting.put("animal", Boolean.valueOf(this.targetingAnimal));
     dataEntry.put("targeting", targeting);
 
     return dataEntry;
   }
 
   public String getOwnerName() {
     return this.ownerName;
   }
 
   public void AITick() {
     long curTick = this.world.getFullTime();
 
     if ((!this.unlimitedAmmo) && (this.ammo <= 0) && 
       (!tryLoadAmmo())) {
       this.world.playEffect(this.loc, Effect.SMOKE, 4);
       return;
     }
 
     if ((this.target != null) && ((this.target.isDead()) || (curTick >= this.startAttackCurrentTargetTick + 80L))) {
       this.target = null;
     }
     if ((this.target == null) && (curTick >= this.lastFindTick + 30L)) {
       this.lastFindTick = curTick;
       this.target = findNewTarget();
       if (this.target != null) {
         this.startAttackCurrentTargetTick = curTick;
       }
     }
 
     if ((this.target != null) && ((this.unlimitedAmmo) || (this.ammo > 0)) && (curTick >= this.lastAttackTick + this.attackPeriodTicks)) {
       shootTarget();
       this.lastAttackTick = curTick;
       if (!this.unlimitedAmmo) {
         this.ammo -= 1;
         if ((this.ammo == 0) && 
           (!tryLoadAmmo()))
           Utils.safeSend(this.ownerName, Messages.outOfAmmo, new Object[0]);
       }
     }
   }
 
   private boolean tryLoadAmmo()
   {
     BlockState baseState = this.baseBlock.getState();
     if ((baseState instanceof Dispenser)) {
       Dispenser dispenser = (Dispenser)baseState;
       if (Utils.tryConsumeItem(dispenser.getInventory(), Material.ARROW)) {
         this.ammo += this.ammoPerArrow;
         return true;
       }
     }
     return false;
   }
 
   private LivingEntity findNewTarget() {
     Entity dummy = this.world.spawnEntity(this.loc, EntityType.ENDER_SIGNAL);
     List<Entity> nearEntities = dummy.getNearbyEntities(25.0D, 25.0D, 25.0D);
     dummy.remove();
 
     this.world.playEffect(this.loc, Effect.ENDER_SIGNAL, 0);
 
     List<LivingEntity> possiblyTargets = new ArrayList<LivingEntity>();
     Set<String> friendlyPlayerNames = new HashSet<String>();
     for (Entity nearEntity : nearEntities)
       if ((nearEntity instanceof LivingEntity)) {
         LivingEntity nearLiving = (LivingEntity)nearEntity;
 
         if (nearLiving.getHealth() > 0)
         {
           if ((nearLiving instanceof Player)) {
             if (this.targetingPlayer) {
               Player nearPlayer = (Player)nearLiving;
 
               if (!canBeTarget(nearPlayer))
                 friendlyPlayerNames.add(nearPlayer.getName());
               else possiblyTargets.add(nearLiving); //this line was missing from the code, where it should add players
             }
           } else if (((nearLiving instanceof HumanEntity)) || ((nearLiving instanceof NPC)) ?
             this.targetingNpc : 
             ((nearLiving instanceof Monster)) || ((nearLiving instanceof EnderDragon)) ?
             this.targetingMonster : 
             ((!(nearLiving instanceof Animals)) && (!(nearLiving instanceof Squid))) ||
             (this.targetingAnimal))
           {
             if (Utils.isInLineOfSight(this.loc, nearLiving.getLocation().add(0.0D, nearLiving.getEyeHeight(), 0.0D)))
             {
               possiblyTargets.add(nearLiving);
             }
           }
         }
       }
     return selectTarget(possiblyTargets, friendlyPlayerNames);
   }
 
   private boolean canBeTarget(Player player) {
     if (player.getName().equals(this.ownerName)) return false;
     if (player.getGameMode() == GameMode.CREATIVE) return false;
 
     if (this.owner != null) {
       if (!this.owner.getPlayer().canSee(player)) return false;
 
       HeroParty party = this.owner.getParty();
       if ((party != null) && (party.isPartyMember(player))) return false;
 
       if (!Skill.damageCheck(this.owner.getPlayer(), player)) return false;
     }
     if (Utils.getHero(player).hasEffectType(EffectType.INVIS)) return false;

     if (CombatUtil.preventDamageCall(null, player, target )) return false; //can't be targeted if target is no pvp

     //if (PluginsIntegration.instance.isFriendly(this.ownerName,
     //     player && !Utils.getHero(player).isInCombat())) return false; the old check with an added condition of in combat
 
     return true;
   }
 
   private LivingEntity selectTarget(List<LivingEntity> targets, Set<String> friendlyPlayerNames) {
     if (targets.isEmpty()) return null;
 
     int maxWeightIndex = -1;
     int maxWeight = 0;
 
     for (int curIndex = 0; curIndex < targets.size(); curIndex++) {
       LivingEntity target = targets.get(curIndex);
       int curWeight = 0;
 
       if ((target instanceof Tameable))
       {
         Tameable pet = (Tameable)target;
         if ((pet.getOwner() != null) && (friendlyPlayerNames.contains(pet.getOwner().getName())));
       }
       else
       {
         double distanceMod = this.loc.distance(target.getLocation());
         distanceMod = Math.min(Math.max(distanceMod, 1.0D), 24.0D);
         curWeight += 25 - (int)distanceMod;
 
         if ((target instanceof Player)) {
           Player targetPlayer = (Player)target;
           curWeight += 20;
           if (targetPlayer.isSneaking()) curWeight -= 27;
           if ((this.owner != null) && (this.owner.isInCombatWith(targetPlayer))) curWeight += 30;
         }
 
         if (curWeight > maxWeight) {
           maxWeight = curWeight;
           maxWeightIndex = curIndex;
         }
       }
     }
     return maxWeightIndex == -1 ? null : targets.get(maxWeightIndex);
   }
 
   private void shootTarget() {
     Location tLoc = this.target.getLocation();
     double vecX = tLoc.getX() - this.loc.getX();
     double vecY = tLoc.getY() - this.loc.getY() + this.target.getEyeHeight() - 0.7D;
     double vecZ = tLoc.getZ() - this.loc.getZ();
     double vecXZ = Math.sqrt(vecX * vecX + vecZ * vecZ);
     float yaw = (float)(Math.atan2(vecZ, vecX) * 180.0D / 3.141592653589793D) - 90.0F;
     float pitch = (float)-(Math.atan2(vecY, vecXZ) * 180.0D / 3.141592653589793D);
 
     Location arrowLoc = this.loc.clone();
 
     arrowLoc.setPitch(yaw);
     arrowLoc.setYaw(pitch);
 
     float distanceMod = (float)vecXZ * 0.2F;
     Vector shootVec = new Vector(vecX, vecY + distanceMod, vecZ);
 
     Arrow arrow = this.world.spawnArrow(arrowLoc, shootVec, 1.6F, 12.0F);
     this.world.playEffect(this.loc, Effect.BOW_FIRE, 0);
 
     ReflectionUtils.setArrowDamage(arrow, this.damage);
     ReflectionUtils.setNotPickupable(arrow);
     if (this.fireArrow)
       arrow.setFireTicks(200);
   }
 
   public void ownerJoined(Hero owner)
   {
     this.owner = owner;
   }
 
   public void ownerQuited() {
     this.owner = null;
   }
 
   public boolean isAlive() {
     if (!this.persistent) {
       if (this.world.getFullTime() >= this.endLifeTick) return false;
 
       if ((this.destroyOnChunkUnload) && (!isChunkLoaded())) return false;
 
       if (this.owner == null) {
         if (this.destroyOnOwnerLogout) return false;
       }
       else if (!this.owner.canUseSkill("Turret")) return false;
     }
 
     return true;
   }
 
   public Block getBaseBlock() {
     return this.baseBlock;
   }
 
   public boolean checkBaseBlock()
   {
     return this.baseBlock.getType() == Material.DISPENSER;
   }
 
   public boolean isChunkLoaded() {
     return this.baseBlock.getChunk().isLoaded();
   }
 
   public boolean isProtected() {
     return this.lifetimeProtection;
   }
 }




