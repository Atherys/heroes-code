 package com.atherys.skills.SkillTurret;

 import com.herocraftonline.heroes.characters.Hero;
 import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
 import com.herocraftonline.heroes.util.Messaging;
 import org.bukkit.block.Block;
 import org.bukkit.entity.Player;

 import java.util.ArrayList;
 import java.util.List;
 import java.util.logging.Level;

 public class TurretManager
   implements Runnable
 {
   private final TurretMap turretMap;
   private final SkillTurret skill;
   private final TurretStorage turretStorage;
   private final PlayersAroundChecker playerChecker;
   //private static final String PERM_BYPASS_PROTECTION = "skillturret.bypassprotection";
 
   public TurretManager(SkillTurret skill)
   {
     this.skill = skill;
     this.turretStorage = new TurretStorage(skill.plugin.getDataFolder());
     this.turretMap = new TurretMap();
     this.playerChecker = new PlayersAroundChecker(skill.plugin, 47);
   }
 
   private boolean isReplaceOldestOnLimitFor(Hero hero) {
     return SkillConfigManager.getUseSetting(hero, this.skill, "replace-oldest-on-limit", false);
   }
 
   public boolean canAddFor(Hero hero) {
     List<Turret> playerTurrets = this.turretMap.getByPlayer(hero.getPlayer());
     return (isReplaceOldestOnLimitFor(hero)) || (playerTurrets.size() < this.skill.maxTurretsFor(hero));
   }
 
   public int getTotalTurrets(Hero hero) {
     return this.turretMap.getByPlayer(hero.getPlayer()).size();
   }
 
   public boolean addFor(Hero hero, Block base) {
     List<Turret> playerTurrets = this.turretMap.getByPlayer(hero.getPlayer());
 
     boolean replace = false;
     if (playerTurrets.size() >= this.skill.maxTurretsFor(hero)) {
       if (isReplaceOldestOnLimitFor(hero))
       {
         this.turretMap.removeByBase(playerTurrets.get(0).getBaseBlock());
         replace = true;
       } else {
         return false;
       }
     }
 
     this.turretMap.add(new Turret(hero, base, this.skill));
     if (replace) {
       Messaging.send(hero.getPlayer(), Messages.turretReplaced);
     }
     return true;
   }
 
   public boolean onDispenserDestroyed(Block dispenser, Player destroyer)
   {
     Turret turret = this.turretMap.getAt(dispenser);
     if (turret == null) return false;
 
     boolean isOwner = turret.getOwnerName().equals(destroyer.getName());
 
     if ((isOwner) || (hasAccess(destroyer, turret))) {
       Turret destroyed = this.turretMap.removeByBase(dispenser);
       if (isOwner) {
         Messaging.send(destroyer, Messages.yourTurretDestroyed);
       } else {
         Messaging.send(destroyer, Messages.youDestroyOtherPlayerTurret, destroyed.getOwnerName());
         Utils.safeSend(destroyed.getOwnerName(), Messages.yourTurretDestroyedBy, new Object[] { destroyer.getName() });
       }
       return false;
     }
     Messaging.send(destroyer, Messages.turretProtected);
     return true;
   }
 
   public boolean onDispenserOpen(Block dispenser, Player player)
   {
     Turret turret = this.turretMap.getAt(dispenser);
     if (turret == null) return false;
 
     boolean isOwner = turret.getOwnerName().equals(player.getName());
 
     if ((isOwner) || (hasAccess(player, turret))) {
       return false;
     }
     Messaging.send(player, Messages.turretProtected);
     return true;
   }
 
   private boolean hasAccess(Player player, Turret turret)
   {
     return (!turret.isProtected()) || (player.isOp()) || (player.hasPermission("skillturret.bypassprotection"));
   }
 
   public void removeAll(Hero hero) {
     this.turretMap.removeByPlayer(hero.getPlayer());
   }
 
   public void run()
   {
     if (this.turretMap.size() == 0) return;
     List<Turret> forRemove = new ArrayList<Turret>();
 
     for (Turret curTurret : this.turretMap.getTurrets()) {
       if (curTurret.isAlive()) {
         if ((curTurret.isChunkLoaded()) && (this.playerChecker.isAnyPlayerAround(curTurret.getBaseBlock().getLocation(), 60))) {
           if (curTurret.checkBaseBlock())
             curTurret.AITick();
           else
             forRemove.add(curTurret);
         }
       }
       else {
         forRemove.add(curTurret);
       }
     }
 
     for (Turret rTurret : forRemove) {
       this.turretMap.removeByBase(rTurret.getBaseBlock());
       Utils.safeSend(rTurret.getOwnerName(), Messages.yourTurretLostMagicPower, new Object[0]);
     }
   }
 
   public void loadAll() {
     List<Turret> loaded = this.turretStorage.load();
     Utils.log(loaded.size() + " turrets loaded", Level.INFO);
     this.turretMap.setTurrets(loaded);
   }
 
   public void saveAll() {
     this.turretStorage.save(this.turretMap.getTurrets());
   }
 
   public void onPlayerJoin(Player player) {
     List<Turret> playerTurrets = this.turretMap.getByPlayer(player);
     Hero owner;
     if (!playerTurrets.isEmpty()) {
       owner = Utils.getHero(player);
       for (Turret playerTurret : playerTurrets)
         playerTurret.ownerJoined(owner);
     }
   }
 
   public void onPlayerQuit(Player player)
   {
     List<Turret> playerTurrets = this.turretMap.getByPlayer(player);
     for (Turret playerTurret : playerTurrets)
       playerTurret.ownerQuited();
   }
 }




