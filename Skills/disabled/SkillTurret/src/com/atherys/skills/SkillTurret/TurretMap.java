 package com.atherys.skills.SkillTurret;

 import org.bukkit.block.Block;
 import org.bukkit.entity.Player;

 import java.util.*;

 public class TurretMap
 {
   private final Map<Block, Turret> baseMap = new LinkedHashMap<Block, Turret>();
   private final Map<String, List<Turret>> playerMap = new HashMap<String, List<Turret>>();
 
   public void add(Turret turret) {
     String ownerName = turret.getOwnerName();
 
     this.baseMap.put(turret.getBaseBlock(), turret);
 
     List<Turret> playerTurrets = this.playerMap.get(ownerName);
     if (playerTurrets == null) {
       playerTurrets = new LinkedList<Turret>();
       this.playerMap.put(ownerName, playerTurrets);
     }
     playerTurrets.add(turret);
   }
 
   public List<Turret> getByPlayer(Player player)
   {
     List<Turret> playerTurrets = this.playerMap.get(player.getName());
     return (List<Turret>) (playerTurrets == null ? Collections.emptyList() : Collections.unmodifiableList(playerTurrets));
   }
 
   public Turret getAt(Block baseBlock) {
     return this.baseMap.get(baseBlock);
   }
 
   public void removeByPlayer(Player player) {
     List<Turret> removedTurrets = this.playerMap.remove(player.getName());
     if (removedTurrets != null)
       for (Turret removedTurret : removedTurrets)
         this.baseMap.remove(removedTurret.getBaseBlock());
   }
 
   public Turret removeByBase(Block baseBlock)
   {
     Turret removed = this.baseMap.remove(baseBlock);
     Iterator<Turret> itr;
     if (removed != null) {
       List<Turret> playerTurrets = this.playerMap.get(removed.getOwnerName());
       if (playerTurrets != null) {
         for (itr = playerTurrets.iterator(); itr.hasNext(); ) {
           if (itr.next() == removed) {
             itr.remove();
           }
         }
       }
     }
 
     return removed;
   }
 
   public void setTurrets(List<Turret> newList) {
     this.baseMap.clear();
     this.playerMap.clear();
 
     for (Turret curTurret : newList)
       add(curTurret);
   }
 
   public Collection<Turret> getTurrets()
   {
     return Collections.unmodifiableCollection(this.baseMap.values());
   }
 
   public int size() {
     return this.baseMap.size();
   }
 }




