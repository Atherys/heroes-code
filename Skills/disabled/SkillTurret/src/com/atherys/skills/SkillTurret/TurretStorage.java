 package com.atherys.skills.SkillTurret;

 import org.bukkit.configuration.file.YamlConfiguration;

 import java.io.File;
 import java.io.IOException;
 import java.util.ArrayList;
 import java.util.Collection;
 import java.util.List;
 import java.util.Map;
 import java.util.logging.Level;

 public class TurretStorage
 {
   private final File dataFile;
   //private static final String ROOT_KEY = "turrets";
 
   public TurretStorage(File pluginFolder)
   {
     this.dataFile = new File(pluginFolder, "turrets.yml");
   }
 
   public List<Turret> load() {
     YamlConfiguration config = YamlConfiguration.loadConfiguration(this.dataFile);
     List<Map<?, ?>> turretsData = config.getMapList("turrets");
 
     List<Turret> result = new ArrayList<Turret>();
     if (turretsData != null) {
       for (Map<?, ?> dataEntry : turretsData) {
         try {
           Turret curTurret = new Turret(dataEntry);
           result.add(curTurret);
         } catch (Exception ex) {
           Utils.log("Failed to load turret. Skipping it", Level.SEVERE);
         }
       }
     }
     return result;
   }
 
   public void save(Collection<Turret> turrets) {
     List<Map<?, ?>> turretsData = new ArrayList<Map<?, ?>>();
     for (Turret curTurret : turrets) {
       Map<?, ?> dataEntry = curTurret.getDataEntry();
       if (dataEntry != null) {
         turretsData.add(dataEntry);
       }
     }
     YamlConfiguration config = new YamlConfiguration();
     config.set("turrets", turretsData);
     try
     {
       config.save(this.dataFile);
     } catch (IOException ex) {
       Utils.log("Failed to save turrets data", Level.SEVERE);
       ex.printStackTrace();
     }
   }
 }




