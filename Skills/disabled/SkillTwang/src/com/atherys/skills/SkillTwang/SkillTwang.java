package com.atherys.skills.SkillTwang;

import com.atherys.effects.SilenceEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class SkillTwang extends ActiveSkill {
    public SkillTwang(Heroes plugin) {
        super(plugin, "Twang");
        setDescription("Slaps an enemy in the throat with your bow, dealing damage $1 and silencing them for $2 seconds.");
        setUsage("/skill Twang");
        setArgumentRange(0, 0);
        setIdentifiers("skill twang");
        Bukkit.getServer().getPluginManager().registerEvents(new DamageListener(this), plugin);
        setTypes(SkillType.BUFF);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(4));
        node.set("buff-duration", 10000);
        node.set("silence-duration", 10000L);
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 40, false);
        long s = (long) SkillConfigManager.getUseSetting(hero, this, "silence-duration", 10000L, false);
        long b = (long) SkillConfigManager.getUseSetting(hero, this, "buff-duration", 10000L, false);
        hero.addEffect(new TwangEffect(this, b, s, d, hero.getPlayer()));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        long s = SkillConfigManager.getUseSetting(hero, this, "silence-duration", 10000, false);
        return getDescription().replace("$1", damage + "").replace("$2", s / 1000 + "");
    }

    public class TwangEffect extends ExpirableEffect {
        private final long silenceduration;
        private final int damage;
        private final Player caster;

        public TwangEffect(Skill skill, long duration, long silenceduration, int damage, Player caster) {
            super(skill, "TwangEffect", duration);
            this.silenceduration = silenceduration;
            this.damage = damage;
            this.caster = caster;
            types.add(EffectType.BENEFICIAL);
        }

        public int getDamage() {
            return damage;
        }

        public Player getCaster() {
            return caster;
        }

        public long getSilenceDuration() {
            return silenceduration;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.getPlayer().sendMessage(ChatColor.GRAY + "Twang buff expired");
        }
    }

    public class DamageListener implements Listener {
        private final Skill skill;

        public DamageListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.NORMAL)
        public void onWeaponDamage(EntityDamageEvent event) {
            if (event.isCancelled() || (!(event instanceof EntityDamageByEntityEvent))) {
                return;
            }
            EntityDamageByEntityEvent subevent = (EntityDamageByEntityEvent) event;
            if (!(subevent.getDamager() instanceof Player)) {
                return;
            }
            Player p = (Player) subevent.getDamager();
            if (!(p.getInventory().getItemInHand().getType() == Material.BOW)) {
                return;
            }
            Hero h = plugin.getCharacterManager().getHero(p);
            if (h.hasEffect("TwangEffect")) {
                TwangEffect te = (TwangEffect) h.getEffect("TwangEffect");
                if (event.getEntity() instanceof LivingEntity) {
                    event.setDamage(event.getDamage() + te.getDamage());
                    if (event.getEntity() instanceof Player) {
                        plugin.getCharacterManager().getHero((Player) event.getEntity()).addEffect(new SilenceEffect(skill, te.getSilenceDuration(), true));
                    }
                }
                h.removeEffect(te);
            }
        }
    }
}