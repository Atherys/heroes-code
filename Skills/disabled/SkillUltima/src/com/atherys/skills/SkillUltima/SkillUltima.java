package com.atherys.skills.SkillUltima;

import com.atherys.effects.SlowEffect;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.StunEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SkillUltima extends ActiveSkill {
    private String applyText;
    private String expireText;
    private Map<Long, Set<Block>> blocks = new HashMap<>();

    public SkillUltima(Heroes plugin) {
        super(plugin, "Ultima");
        setDescription("The definitive mix of elemental magic.");
        setUsage("/skill Ultima");
        setArgumentRange(0, 0);
        setIdentifiers("skill ultima");
        setTypes(SkillType.ICE, SkillType.SILENCABLE, SkillType.HARMFUL, SkillType.BUFF);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = (int) (SkillConfigManager.getUseSetting(hero, this, "tick-damage", 1.0D, false) + SkillConfigManager.getUseSetting(hero, this, "tick-damage-increase", 0.0D, false) * hero.getSkillLevel(this));
        damage = damage > 0 ? damage : 0;
        int radius = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS.node(), 10, false) + SkillConfigManager.getUseSetting(hero, this, "radius-increase", 0.0D, false) * hero.getSkillLevel(this));
        radius = radius > 1 ? radius : 1;
        int mana = (int) (SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA.node(), 1, false) - SkillConfigManager.getUseSetting(hero, this, SkillSetting.MANA_REDUCE.node(), 0.0D, false) * hero.getSkillLevel(this));
        mana = mana > 0 ? mana : 0;
        String description = getDescription().replace("$1", damage + "").replace("$2", radius + "");
        if (mana > 0) {
            description = description + " M:" + mana;
        }
        return description;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("on-text", "%hero% chills the air with their %skill%!");
        node.set("off-text", "%hero%'s %skill% expired!");
        node.set("tick-damage", 1);
        node.set("tick-damage-increase", 0);
        node.set(SkillSetting.PERIOD.node(), 1000);
        node.set(SkillSetting.DURATION.node(), 10000);
        node.set("stun-duration", 10000);
        node.set("slow-radius", 10);
        node.set("raidus-increase", 0);
        node.set("bolt-damage", 50);
        node.set("bolt-radius", 50);
        node.set("shield-duration", 10000);
        node.set("fireball-damage", 50);
        node.set("velocity-multiplier", 1.5D);
        node.set("fire-ticks", 100);
        node.set("pause-before-shield", 10);
        node.set("BlockType", "STONE");
        return node;
    }

    @Override
    public void init() {
        super.init();
        applyText = SkillConfigManager.getRaw(this, "on-text", "%hero% used %skill%!").replace("%hero%", "$1").replace("%skill%", "$2");
        expireText = SkillConfigManager.getRaw(this, "off-text", "%hero%'s %skill% expired!").replace("%hero%", "$1").replace("%skill%", "$2");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 1.5D, false);
        double diff = 2 * Math.PI / 24;
        long shieldduration = SkillConfigManager.getUseSetting(hero, this, "shield-duration", 10000, false);
        long pause = SkillConfigManager.getUseSetting(hero, this, "pause-before-shield", 10, false);
        Material setter = Material.valueOf(SkillConfigManager.getUseSetting(hero, this, "BlockType", "STONE"));
        EarthEffect eEffect = new EarthEffect(this, shieldduration, setter);
        hero.addEffect(eEffect);
        for (double a = 0; a < 2 * Math.PI; a += diff) {
            Vector vel = new Vector(Math.cos(a), 0, Math.sin(a));
            Snowball fireball = player.launchProjectile(Snowball.class);
            fireball.setFireTicks(100);
            fireball.setMetadata("UltimaFireVolleyFireball", new FixedMetadataValue(plugin, true));
            fireball.setVelocity(vel.multiply(mult));
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 1000, false);
        long stunduration = SkillConfigManager.getUseSetting(hero, this, "stun-duration", 10000, false);
        if (stunduration > 0L) {
            hero.addEffect(new StunEffect(this, stunduration));
        }
        long period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 1000, false);
        double tickDamage = SkillConfigManager.getUseSetting(hero, this, "tick-damage", 10, false);
        int range = SkillConfigManager.getUseSetting(hero, this, "slow-radius", 10, false);
        double boltdamage = SkillConfigManager.getUseSetting(hero, this, "bolt-damage", 50, false);
        int boltradius = SkillConfigManager.getUseSetting(hero, this, "bolt-radius", 50, false);
        hero.addEffect(new UltimaEffect(this, period, duration, tickDamage, range, mult, boltdamage, boltradius));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class UltimaEffect extends PeriodicExpirableEffect {
        private double tickDamage;
        private int range;
        private long duration;
        //private double multiplier;
        private double boltdamage;
        private int boltradius;

        public UltimaEffect(SkillUltima skill, long period, long duration, double tickDamage, int range, double multiplier, double boltdamage, int boltradius) {
            super(skill, "Ultima", period, duration);
            this.tickDamage = tickDamage;
            this.range = range;
            this.duration = duration;
            //this.multiplier = multiplier;
            this.boltdamage = boltdamage;
            this.boltradius = boltradius;
            types.add(EffectType.DISPELLABLE);
            types.add(EffectType.BENEFICIAL);
            types.add(EffectType.ICE);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            for (Entity entity : player.getNearbyEntities(boltradius, boltradius, boltradius)) {
                if (((entity instanceof LivingEntity))) {
                    LivingEntity target = (LivingEntity) entity;
                    if ((damageCheck(player, target)) && ((target instanceof Player))) {
                        target.getWorld().strikeLightningEffect(target.getLocation());
                        damageEntity(target, player, boltdamage, EntityDamageEvent.DamageCause.MAGIC);
                    }
                }
            }
        }

        @Override
        public void tickHero(Hero hero) {
            Player player = hero.getPlayer();
            for (Entity entity : player.getNearbyEntities(range, range, range)) {
                if (((entity instanceof LivingEntity)) && (damageCheck(player, (LivingEntity) entity))) {
                    CharacterTemplate character = plugin.getCharacterManager().getCharacter((LivingEntity) entity);
                    addSpellTarget(entity, hero);
                    damageEntity(character.getEntity(), player, tickDamage, EntityDamageEvent.DamageCause.MAGIC);
                    SlowEffect effect = new SlowEffect(skill, duration, 2, true, "", "", hero);
                    character.addEffect(effect);
                }
            }
        }

        @Override
        public void tickMonster(Monster mnstr) {
            // throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public class EarthEffect extends ExpirableEffect {
        private Material setter;
        private final Long timeidentifier;

        public EarthEffect(Skill skill, long shieldduration, Material setter) {
            super(skill, "EarthShield", shieldduration);
            this.setter = setter;
            this.timeidentifier = Long.valueOf(System.currentTimeMillis());
            types.add(EffectType.BENEFICIAL);
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), applyText, player.getDisplayName(), "EarthShield");
            Location[] locs = new Location[]{
                    player.getLocation().add(0, 0, 2),
                    player.getLocation().add(1, 0, 2),
                    player.getLocation().add(-1, 0, 2),
                    player.getLocation().add(0, 0, -2),
                    player.getLocation().add(1, 0, -2),
                    player.getLocation().add(-1, 0, -2),
                    player.getLocation().add(2, 0, 0),
                    player.getLocation().add(2, 0, 1),
                    player.getLocation().add(2, 0, -1),
                    player.getLocation().add(-2, 0, 0),
                    player.getLocation().add(-2, 0, 1),
                    player.getLocation().add(-2, 0, -1),
                    player.getLocation().add(-2, 0, -2),
                    player.getLocation().add(2, 0, 2),
                    player.getLocation().add(2, 0, -2),
                    player.getLocation().add(-2, 0, 2),
                    player.getLocation().add(0, 1, 2),
                    player.getLocation().add(1, 1, 2),
                    player.getLocation().add(-1, 1, 2),
                    player.getLocation().add(0, 1, -2),
                    player.getLocation().add(1, 1, -2),
                    player.getLocation().add(-1, 1, -2),
                    player.getLocation().add(2, 1, 0),
                    player.getLocation().add(2, 1, 1),
                    player.getLocation().add(2, 1, -1),
                    player.getLocation().add(-2, 1, 0),
                    player.getLocation().add(-2, 1, 1),
                    player.getLocation().add(-2, 1, -1),
                    player.getLocation().add(-2, 1, -2),
                    player.getLocation().add(2, 1, 2),
                    player.getLocation().add(2, 1, -2),
                    player.getLocation().add(-2, 1, 2),
                    player.getLocation().add(0, 2, 2),
                    player.getLocation().add(1, 2, 2),
                    player.getLocation().add(-1, 2, 2),
                    player.getLocation().add(0, 2, -2),
                    player.getLocation().add(1, 2, -2),
                    player.getLocation().add(-1, 2, -2),
                    player.getLocation().add(2, 2, 0),
                    player.getLocation().add(2, 2, 1),
                    player.getLocation().add(2, 2, -1),
                    player.getLocation().add(-2, 2, 0),
                    player.getLocation().add(-2, 2, 1),
                    player.getLocation().add(-2, 2, -1),
                    player.getLocation().add(-2, 2, -2),
                    player.getLocation().add(2, 2, 2),
                    player.getLocation().add(2, 2, -2),
                    player.getLocation().add(-2, 2, 2),
                    player.getLocation().add(0, 3, 2),
                    player.getLocation().add(1, 3, 2),
                    player.getLocation().add(-1, 3, 2),
                    player.getLocation().add(0, 3, -2),
                    player.getLocation().add(1, 3, -2),
                    player.getLocation().add(-1, 3, -2),
                    player.getLocation().add(2, 3, 0),
                    player.getLocation().add(2, 3, 1),
                    player.getLocation().add(2, 3, -1),
                    player.getLocation().add(-2, 3, 0),
                    player.getLocation().add(-2, 3, 1),
                    player.getLocation().add(-2, 3, -1),
                    player.getLocation().add(-2, 3, -2),
                    player.getLocation().add(2, 3, 2),
                    player.getLocation().add(2, 3, -2),
                    player.getLocation().add(-2, 3, 2),
            };
            Set<Block> blckss = new HashSet<>();
            for (Location loc : locs) {
                if (loc.getBlock().getType() == Material.AIR) {
                    loc.getBlock().setType(setter);
                    blckss.add(loc.getBlock());
                }
            }
            for (int h = -1; h <= 1; h++) {
                for (int w = -1; w <= 1; w++) {
                    if (player.getLocation().add(h, -1, w).getBlock().getType() == Material.AIR) {
                        player.getLocation().add(h, -1, w).getBlock().setType(setter);
                        blckss.add(player.getLocation().add(h, -1, w).getBlock());
                    }
                    if (player.getLocation().add(h, 3, w).getBlock().getType() == Material.AIR) {
                        player.getLocation().add(h, 3, w).getBlock().setType(setter);
                        blckss.add(player.getLocation().add(h, 3, w).getBlock());
                    }
                }
            }
            blocks.put(timeidentifier, blckss);
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            Player player = hero.getPlayer();
            broadcast(player.getLocation(), expireText, player.getDisplayName(), "EarthShield");
            Set<Block> copy = blocks.get(timeidentifier);
            for (Block w : copy) {
                w.setType(Material.AIR);
            }
            blocks.remove(timeidentifier);
        }
    }

    public class SkillEntityListener
            implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof Snowball)) || (!projectile.hasMetadata("UltimaFireVolleyFireball"))) {
                return;
            }
            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            Entity dmger = (Entity) ((Snowball) projectile).getShooter();
            if ((dmger instanceof Player)) {
                Hero hero = plugin.getCharacterManager().getHero((Player) dmger);
                if (!Skill.damageCheck((Player) dmger, entity)) {
                    event.setCancelled(true);
                    return;
                }
                entity.setFireTicks(SkillConfigManager.getUseSetting(hero, skill, "fire-ticks", 100, false));
                addSpellTarget(entity, hero);
                double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 4, false);
                damage += (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE_INCREASE, 0.0D, false) * hero.getSkillLevel(this.skill));
                if (entity instanceof Player) {
                    Hero thero = plugin.getCharacterManager().getHero((Player) entity);
                    if (!thero.hasEffect("dceultima")) {
                        thero.addEffect(new DamageCheckEffect(skill, 10L));
                    } else
                        return;
                }
                Skill.damageEntity(entity, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                event.setCancelled(true);
            }
        }
    }

    public class DamageCheckEffect extends ExpirableEffect {
        public DamageCheckEffect(Skill skill, long duration) {
            super(skill, "dceultima", 20L);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled()) {
            return;
        }
        for (Long x : blocks.keySet()) {
            if (blocks.get(x).contains(event.getBlock())) {
                event.setCancelled(true);
            }
        }
    }
}
