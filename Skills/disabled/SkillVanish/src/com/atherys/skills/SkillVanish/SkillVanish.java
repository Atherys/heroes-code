package com.atherys.skills.SkillVanish;

import com.atherys.effects.VanishEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.SkillUseEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SkillVanish extends ActiveSkill {
    public SkillVanish(Heroes plugin) {
        super(plugin, "Vanish");
        setDescription("Allows the user to completely disappear from sight. No action except Steal can be used while Vanished. Nothing breaks Vanish except for toggling it off, skill steal, or the Locate skill.");
        setUsage("/skill Vanish");
        setArgumentRange(0, 0);
        setIdentifiers("skill vanish");
        setTypes(SkillType.BUFF, SkillType.SILENCABLE, SkillType.ILLUSION);
        Bukkit.getPluginManager().registerEvents(new BlockListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new HeroListener(), plugin);
    }

    public Set<String> getAllowedSkills(Hero hero) {
        List<String> allowedskills = SkillConfigManager.getUseSetting(hero, this, "allowed-skills", Collections.<String>emptyList());
        Set<String> result = new HashSet<>();
        for (String skill : allowedskills) {
            result.add(skill.toLowerCase());
        }
        return result;
    }

    @Override
    public SkillResult use(Hero hero, String[] strings) {
        if (hero.hasEffect("FwEffect")) {
            hero.getPlayer().sendMessage("You cant use this spell now");
            return SkillResult.CANCELLED;
        }
        if (!hero.hasEffect("VanishEff")) {
            broadcastExecuteText(hero);
        }
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        hero.addEffect(new VanishEffect(this, duration, getAllowedSkills(hero)));
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("allowed-skills", Collections.<String>emptyList());
        node.set(SkillSetting.DURATION.node(), Long.valueOf(5000L));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    public class BlockListener implements Listener {
        @SuppressWarnings("deprecation")
        @EventHandler(priority = EventPriority.HIGH)
        public void onPlayerInteract(PlayerInteractEvent event) {
            Player player = event.getPlayer();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("VanishEff")) {
                boolean c = true;
                ItemStack is = event.getPlayer().getItemInHand();
                if (is != null) {
                    if (Lib.foodTypes.contains(is.getType()))
                        c = false;
                }
                if (event.hasBlock()) {
                    event.setUseInteractedBlock(Event.Result.DENY);
                } else if (event.hasItem()) {
                    event.setUseItemInHand(Event.Result.DENY);
                }
                Messaging.send(hero.getPlayer(), "You can't do that while vanished!");
                player.updateInventory();
                event.setCancelled(c);
            }
        }
    }

    public class HeroListener implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST)
        public void onPlayerAttack(EntityDamageEvent event) {
            if (event.isCancelled()) {
                return;
            }
            if (!(event instanceof EntityDamageByEntityEvent)) {
                return;
            }
            EntityDamageByEntityEvent subevent = (EntityDamageByEntityEvent) event;
            if (!(subevent.getDamager() instanceof Player)) {
                return;
            }
            Player player = (Player) subevent.getDamager();
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (hero.hasEffect("VanishEff")) {
                event.setDamage(0);
                event.setCancelled(true);
            }
        }

        @EventHandler(priority = EventPriority.HIGH)
        public void onPlayerDamage(EntityDamageEvent event) {
            if (event.isCancelled()) {
                return;
            }
            if (!(event instanceof EntityDamageByEntityEvent)) {
                return;
            }
            EntityDamageByEntityEvent subevent = (EntityDamageByEntityEvent) event;
            if (!(subevent.getDamager() instanceof Player) || !(subevent.getEntity() instanceof Player)) {
                return;
            }
            Player player = (Player) subevent.getEntity();
            Hero phero = plugin.getCharacterManager().getHero(player);
            if (phero.hasEffect("VanishEff")) {
                event.setCancelled(true);
            }
        }

        @EventHandler(priority = EventPriority.NORMAL)
        public void onSpellUse(SkillUseEvent event) {
            Hero hero = event.getHero();
            if (hero.hasEffect("VanishEff")) {
                VanishEffect vanishEffect = (VanishEffect) hero.getEffect("VanishEff");
                if ((!vanishEffect.getAllowedSkills().contains(event.getSkill().getName().toLowerCase()))) {
                    event.setCancelled(true);
                    Messaging.send(hero.getPlayer(), "You cant use skills in Vanish.");
                }
            }
        }

        @EventHandler(priority = EventPriority.LOW)
        public void onMobTarget(EntityTargetEvent event) {
            if (event.isCancelled()) {
                return;
            }
            if (!(event.getTarget() instanceof Player)) {
                return;
            }
            Hero hero = plugin.getCharacterManager().getHero((Player) event.getTarget());
            if (hero.hasEffect("VanishEff")) {
                event.setTarget(null);
                event.setCancelled(true);
            }
        }

        @EventHandler(priority = EventPriority.LOWEST)
        public void onCommand(PlayerCommandPreprocessEvent event) {
            if (!event.getMessage().toLowerCase().startsWith("/skill vanish"))
                return;
            Hero h = plugin.getCharacterManager().getHero(event.getPlayer());
            if (h.hasEffect("VanishEff")) {
                h.removeEffect(h.getEffect("VanishEff"));
            }
        }
    }
}
