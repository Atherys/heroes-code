package com.atherys.skills.SkillVendetta;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.Map;
import java.util.WeakHashMap;

public class SkillVendetta extends ActiveSkill {
    private final Map<String, Location> LocationMap = new WeakHashMap<String, Location>();

    public SkillVendetta(Heroes plugin) {
        super(plugin, "Vendetta");
        setDescription("Hero blinks $1 blocks forward and has $2 seconds to reactivate to be teleported back to original location.");
        setUsage("/skill Vendetta");
        setArgumentRange(0, 0);
        setIdentifiers("skill Vendetta");
        setTypes(SkillType.SILENCABLE, SkillType.TELEPORT);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), Integer.valueOf(7000));
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(6));
        return node;
    }

    public boolean blink(Hero hero, int distance) {
        final Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return false;
        }
        Block prev = null;
        BlockIterator iter = null;
        try {
            iter = new BlockIterator(player, distance);
        } catch (IllegalStateException e) {
            Messaging.send(player, "There was an error getting your blink location!");
            return false;
        }
        while (iter.hasNext()) {
            Block b = iter.next();
            if ((!Util.transparentBlocks.contains(b.getType())) || ((!Util.transparentBlocks.contains(b.getRelative(BlockFace.UP).getType())) && (!Util.transparentBlocks.contains(b.getRelative(BlockFace.DOWN).getType()))))
                break;
            prev = b;
        }
        if (prev != null) {
            Location teleport = prev.getLocation().clone();
            LocationMap.put(hero.getPlayer().getName(), loc);
            teleport.add(new Vector(0.5D, 0.5D, 0.5D));
            teleport.setPitch(player.getLocation().getPitch());
            teleport.setYaw(player.getLocation().getYaw());
            player.teleport(teleport);
            return true;
        }
        else {
            Messaging.send(player, "There was an error getting your blink location!");
            return false;
        }
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 7000, false);
        long cooldown = SkillConfigManager.getUseSetting(hero, this, SkillSetting.COOLDOWN, 25000, false);
        VendettaEffect effect = null;
        if (!hero.hasEffect("VendettaEffect")) {
            if (blink(hero, distance)) {
                effect = new VendettaEffect(this, duration, cooldown, hero.getPlayer().getName());
                hero.addEffect(effect);
                Messaging.send(hero.getPlayer(), "Reactivate in " + (duration / 1000L) + " seconds to teleport back.");
                return SkillResult.CANCELLED;
            } else {
                return SkillResult.CANCELLED;
            }
        } else {
            effect = (VendettaEffect) hero.getEffect("VendettaEffect");
            LocationMap.get(hero.getPlayer().getName()).setPitch(hero.getPlayer().getLocation().getPitch());
            LocationMap.get(hero.getPlayer().getName()).setYaw(hero.getPlayer().getLocation().getYaw());
            hero.getPlayer().teleport(LocationMap.get(hero.getPlayer().getName()));
            hero.removeEffect(effect);
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int nb = SkillConfigManager.getUseSetting(hero, this, "number-of-blinks", 3, false);
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 6, false);
        return getDescription().replace("$1", distance + "").replace("$2", nb - 1 + "") + Lib.getSkillCostStats(hero, this);
    }

    public class VendettaEffect extends ExpirableEffect {
        private long cooldown;
        private final String name;

        public VendettaEffect(Skill skill, long duration, long cooldown, String name) {
            super(skill, "VendettaEffect", duration);
            types.add(EffectType.BENEFICIAL);
            this.cooldown = cooldown;
            this.name = name;
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            hero.setCooldown("Vendetta", cooldown + System.currentTimeMillis());
            LocationMap.remove(name);
        }
    }
}