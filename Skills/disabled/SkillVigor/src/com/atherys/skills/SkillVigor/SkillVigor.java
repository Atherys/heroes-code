package com.atherys.skills.SkillVigor;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class SkillVigor extends ActiveSkill {
    private String applytext;
    private String savetext;
    private String expiretext;

    public SkillVigor(Heroes plugin) {
        super(plugin, "Vigor");
        setDescription("Nearest player who reaches <10% health is gifted 50% health.");
        setUsage("/skill Vigor");
        setArgumentRange(0, 0);
        setIdentifiers("skill Vigor");
        setTypes(SkillType.SILENCABLE, SkillType.HEAL);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (hero.hasEffect("Vigor")) {
            hero.removeEffect(hero.getEffect("Vigor"));
            return SkillResult.NORMAL;
        }
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        int mana = SkillConfigManager.getUseSetting(hero, this, "manacost/tick", 5, false);
        long cd = SkillConfigManager.getUseSetting(hero, this, "heal-cd", 5, false);
        hero.addEffect(new VigorEffect(this, r, mana, cd));
        return SkillResult.NORMAL;
    }

    @Override
    public void init() {
        super.init();
        applytext = SkillConfigManager.getRaw(this, SkillSetting.APPLY_TEXT, "%hero% starts %skill%.").replace("%skill%", "$1").replace("%hero%", "$2");
        expiretext = SkillConfigManager.getRaw(this, SkillSetting.EXPIRE_TEXT, "%hero% stops %skill%.").replace("%skill%", "$1").replace("%hero%", "$2");
        savetext = SkillConfigManager.getRaw(this, "savetext", "%player% was saved by %hero%").replace("%skill%", "$1").replace("%hero%", "$2").replace("%player%", "$3");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.EXPIRE_TEXT.node(), "%hero% stops %skill%.");
        node.set(SkillSetting.APPLY_TEXT.node(), "%hero% starts %skill%.");
        node.set("savetext", "%player% was saved by %hero%");
        node.set("heal-cd", 90000);
        node.set(SkillSetting.RADIUS.node(), 5);
        node.set("manacost/tick", 10);
        return node;
    }

    public class VigorEffect extends PeriodicEffect {
        private final int radius;
        private final int mana;
        private final long cd;

        public VigorEffect(Skill skill, int radius, int mana, long cd) {
            super(skill, "Vigor", 2000);
            this.cd = cd;
            this.mana = mana;
            this.radius = radius;
        }

        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), applytext, "Vigor", hero.getName());
        }

        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), expiretext, "Vigor", hero.getName());
        }

        @Override
        public void tickHero(Hero hero) {
            if (mana > 0) {
                if (hero.getMana() - mana >= 0) {
                    hero.setMana(hero.getMana() - mana);
                } else {
                    hero.removeEffect(this);
                }
            }
            long time = System.currentTimeMillis();
            if ((hero.getCooldown("VigorHeal") == null || hero.getCooldown("VigorHeal") <= time) && hero.hasParty()) {
                for (Entity e : hero.getPlayer().getNearbyEntities(radius, radius, radius)) {
                    if (e instanceof Player) {
                        Player tplayer = (Player)e;
                        if (hero.getParty().isPartyMember(tplayer) && checkMh(tplayer)) {
                            hero.setCooldown("VigorHeal", time + cd);
                            (tplayer).setHealth(((tplayer).getMaxHealth() * 0.5));
                            broadcast(hero.getPlayer().getLocation(), savetext, "Vigor", hero.getName(), tplayer.getName());
                            break;
                        }
                    }
                }
            }
        }

        private boolean checkMh(Player player) {
            return player.getMaxHealth() * 0.1 >= player.getHealth();
        }
    }
}
