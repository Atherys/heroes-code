package com.atherys.skills.SkillVolatileBlink;

import com.atherys.effects.SlowEffect;
import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicDamageEffect;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.Random;

public class SkillVolatileBlink extends ActiveSkill {

    public SkillVolatileBlink(Heroes plugin) {
        super(plugin, "VolatileBlink");
        setDescription("25% chance to do one of the following effects along with a blink - burn blink, boltblink, a blink followed by a knock up/knockback, a blink followed by an icebolt volley.");
        setUsage("/skill VolatileBlink");
        setArgumentRange(0, 0);
        setIdentifiers("skill VolatileBlink");
        setTypes(SkillType.FORCE, SkillType.DAMAGING, SkillType.HARMFUL, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new IceballHitListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("burnblink-radius", 10);
        node.set("burnblink-damage", 10);
        node.set("burnblink-period", 1000);
        node.set("burnblunk-duration", 10000);
        node.set("boltblink-radius", 10);
        node.set("boltblink-damage", 10);
        node.set("knockback-radius", 10);
        node.set("vertical-vector", 3);
        node.set("horizontal-vector", 3);
        node.set("iceball-velocity", 3);
        node.set("iceball-damage", 3);
        node.set("slow-multiplier", 2);
        node.set("slow-duration", 10000);
        node.set("knockback-damage", 10);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        Location loc = player.getLocation();
        if ((loc.getBlockY() > loc.getWorld().getMaxHeight()) || (loc.getBlockY() < 1)) {
            Messaging.send(player, "The void prevents you from blinking!");
            return SkillResult.FAIL;
        }
        int distance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block prev = null;
        BlockIterator iter = null;
        try {
            iter = new BlockIterator(player, distance);
        } catch (IllegalStateException e) {
            Messaging.send(player, "There was an error getting your blink location!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        while (iter.hasNext()) {
            Block b = iter.next();
            if ((!Util.transparentBlocks.contains(b.getType())) || ((!Util.transparentBlocks.contains(b.getRelative(BlockFace.UP).getType())) && (!Util.transparentBlocks.contains(b.getRelative(BlockFace.DOWN).getType()))))
                break;
            prev = b;
        }
        if (prev == null) {
            Messaging.send(player, "No location to blink to.");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        Random r = new Random();
        // 25% chance for Addition Effect
        double chance = r.nextDouble();
        if (chance < 0.25) {
            // Burnblink
            int ra = SkillConfigManager.getUseSetting(hero, this, "burnblink-radius", 5, false);
            int bd = SkillConfigManager.getUseSetting(hero, this, "burnblink-damage", 3, false);
            long bp = SkillConfigManager.getUseSetting(hero, this, "burnblink-period", 1000, false);
            long bdu = SkillConfigManager.getUseSetting(hero, this, "burnblunk-duration", 5000, false);
            for (Entity e : player.getNearbyEntities(ra, ra, ra)) {
                if (!(e instanceof LivingEntity))
                    continue;
                CharacterTemplate ct = plugin.getCharacterManager().getCharacter((LivingEntity) e);
                ct.addEffect(new VoBurn(this, bdu, bp, bd, player));
            }
        } else if ((chance >= 0.25) && (chance < 0.5)) {
            // BoltBlink
            int bbr = SkillConfigManager.getUseSetting(hero, this, "boltblink-radius", 5, false);
            double bbd = SkillConfigManager.getUseSetting(hero, this, "boltblink-damage", 25, false);
            LivingEntity l = null;
            for (Entity e : player.getNearbyEntities(bbr, bbr, bbr)) {
                if (!(e instanceof LivingEntity))
                    continue;
                if (l == null)
                    l = (LivingEntity) e;
                else if (l.getLocation().distanceSquared(player.getLocation()) > e.getLocation().distanceSquared(player.getLocation()))
                    l = (LivingEntity) e;
            }
            if ((l != null) && damageCheck(player, l)) {
                l.getWorld().strikeLightning(l.getLocation());
                damageEntity(l, player, bbd, DamageCause.MAGIC);
            }
        } else if ((chance >= 0.5) && (chance < 0.75)) {
            // Knockup Blink
            int vv = SkillConfigManager.getUseSetting(hero, this, "vertical-vector", 3, false);
            int vh = SkillConfigManager.getUseSetting(hero, this, "horizontal-vector", 3, false);
            int kb = SkillConfigManager.getUseSetting(hero, this, "knockback-radius", 5, false);
            double damage = SkillConfigManager.getUseSetting(hero, this, "knockback-damage", 10, false);
            for (Entity e : player.getNearbyEntities(kb, kb, kb)) {
                if (!(e instanceof LivingEntity))
                    continue;
                LivingEntity l = (LivingEntity) e;
                if (damageCheck(player, (LivingEntity) e)) {
                    damageEntity(l, player, damage, DamageCause.MAGIC);
                    Vector v = l.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
                    v = v.multiply(vv);
                    v = v.setY(vh);
                    l.setVelocity(v);
                }
            }
        } else if ((chance >= 0.75)) {
            // Iceball volley Blink
            int ivel = SkillConfigManager.getUseSetting(hero, this, "iceball-velocity", 3, false);
            double bq = 2 * Math.PI / 10;
            for (double a = 0; a < 2 * Math.PI; a += bq) {
                Vector v = new Vector(Math.cos(a), 0.0D, Math.sin(a));
                Snowball s = player.launchProjectile(Snowball.class);
                s.setMetadata("VolatileBlinkIcebolt", new FixedMetadataValue(plugin, true));
                s.setVelocity(v.multiply(ivel));
            }
        }
        Location teleport = prev.getLocation().clone();
        teleport.add(new Vector(0.5D, 0.5D, 0.5D));
        teleport.setPitch(player.getLocation().getPitch());
        teleport.setYaw(player.getLocation().getYaw());
        player.teleport(teleport);
        Messaging.send(player, "You have blinked");
        return SkillResult.NORMAL;
    }

    public class VoBurn extends PeriodicDamageEffect {
        public VoBurn(Skill skill, long duration, long period, double damage, Player p) {
            super(skill, "VoBurn", period, duration, damage, p);
            types.add(EffectType.FIRE);
        }
    }

    public class IceballHitListener implements Listener {
        private final Skill skill;

        public IceballHitListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof Snowball)) || (!projectile.hasMetadata("VolatileBlinkIcebolt"))) {
                return;
            }
            Entity dmger = (Entity) ((Snowball) subEvent.getDamager()).getShooter();
            if ((dmger instanceof Player)) {
                Hero hero = SkillVolatileBlink.this.plugin.getCharacterManager().getHero((Player) dmger);
                if (!Skill.damageCheck((Player) dmger, (LivingEntity) event.getEntity())) {
                    event.setCancelled(true);
                    return;
                }
                event.getEntity().setFireTicks(0);
                double damage = SkillConfigManager.getUseSetting(hero, this.skill, "iceball-damage", 10, false);
                long duration = SkillConfigManager.getUseSetting(hero, this.skill, "slow-duration", 4000, false);
                int amplifier = SkillConfigManager.getUseSetting(hero, this.skill, "slow-multiplier", 2, false);
                SlowEffect iceSlowEffect = new SlowEffect(this.skill, duration, amplifier, false, "", "", hero);
                LivingEntity target = (LivingEntity) event.getEntity();
                SkillVolatileBlink.this.plugin.getCharacterManager().getCharacter(target).addEffect(iceSlowEffect);
                SkillVolatileBlink.this.addSpellTarget(event.getEntity(), hero);
                if (event.getEntity() instanceof Player) {
                    Hero thero = plugin.getCharacterManager().getHero((Player) event.getEntity());
                    if (!thero.hasEffect("dcevolblink")) {
                        thero.addEffect(new DamageCheckEffect(skill, 10L));
                    } else
                        return;
                }
                Skill.damageEntity(target, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                event.setCancelled(true);
            }
        }
    }

    public class DamageCheckEffect extends ExpirableEffect {
        public DamageCheckEffect(Skill skill, long duration) {
            super(skill, "dcevolblink", 20L);
        }
    }
}