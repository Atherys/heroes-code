package com.atherys.skills.SkillVolley;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

public class SkillVolley extends ActiveSkill {

    public SkillVolley(Heroes plugin) {
        super(plugin, "Volley");
        setDescription("Creates ring of arrows.");
        setUsage("/skill Volley");
        setArgumentRange(0, 0);
        setIdentifiers("skill Volley");
        setTypes(SkillType.PHYSICAL, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("arrow-damage", Integer.valueOf(50));
        node.set("velocity-multiplier", Double.valueOf(1.5D));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 1.5D, false);
        double diff = 2 * Math.PI / 24;
        for (double a = 0; a < 2 * Math.PI; a += diff) {
            Vector vel = new Vector(Math.cos(a), 0, Math.sin(a));
            Arrow arrow = player.launchProjectile(Arrow.class);
            arrow.setMetadata("VolleyArrow", new FixedMetadataValue(plugin, true));
            arrow.setVelocity(vel.multiply(mult));
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    public class SkillEntityListener implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof Arrow)) || (!projectile.hasMetadata("VolleyArrow"))) {
                return;
            }
            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            ProjectileSource dmger = ((Arrow) projectile).getShooter();
            if (!(dmger instanceof Player)) {
                return;
            }
            Player player = (Player) dmger;
            Hero hero = plugin.getCharacterManager().getHero(player);
            if (!Skill.damageCheck(player, entity)) {
                event.setCancelled(true);
                return;
            }
            addSpellTarget(entity, hero);
            double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 4, false);
            if (entity instanceof Player) {
                Hero t = plugin.getCharacterManager().getHero((Player) entity);
                if (t.hasEffect("dcv")) {
                    return;
                } else {
                    t.addEffect(new DamageCheckV(skill));
                }
            }
            damageEntity(entity, hero.getPlayer(), damage, DamageCause.PROJECTILE);
            event.setCancelled(true);
        }
    }

    public class DamageCheckV extends ExpirableEffect {
        public DamageCheckV(Skill skill) {
            super(skill, "dcv", 20L);
        }
    }
}
