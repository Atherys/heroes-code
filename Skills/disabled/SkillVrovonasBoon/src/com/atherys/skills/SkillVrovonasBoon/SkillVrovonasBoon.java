package com.atherys.skills.SkillVrovonasBoon;

import com.atherys.effects.*;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class SkillVrovonasBoon extends ActiveSkill {
    private Set<Block> k = new HashSet<>();

    public SkillVrovonasBoon(Heroes plugin) {
        super(plugin, "VrovonasBoon");
        setDescription("Sunlight breaks through to target area for $1 seconds. Light would kiss the ground in a $2 block radius and all party members inside would gain Might.");
        setUsage("/skill VrovonasBoon");
        setArgumentRange(0, 0);
        setIdentifiers("skill VrovonasBoon");
        setTypes(SkillType.FORCE, SkillType.LIGHT, SkillType.SILENCABLE);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 20000);
        node.set(SkillSetting.PERIOD.node(), 10000);
        node.set("might-duration", 30000);
        node.set("damage-mult", 0.5D);
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        node.set(SkillSetting.RADIUS.node(), 10);
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        return getDescription().replace("$1", duration / 1000 + "").replace("$2", r + "");
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player p = hero.getPlayer();
        HashSet<Byte> transparent = new HashSet<>();
        transparent.add((byte) Material.AIR.getId());
        transparent.add((byte) Material.SNOW.getId());

        int max = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Block b = p.getTargetBlock(transparent, max);
        if (b.getType() == Material.AIR) {
            p.sendMessage(ChatColor.GRAY + "You must target a block.");
            return SkillResult.CANCELLED;
        }

        Block b1 = b.getRelative(BlockFace.UP);
        if (!(b1.getType() == Material.AIR || b1.getType() == Material.SNOW)) {
            p.sendMessage(ChatColor.GRAY + "Cannot be placed here");
            return SkillResult.CANCELLED;
        }

        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        int period = SkillConfigManager.getUseSetting(hero, this, SkillSetting.PERIOD, 10, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, "damage-mult", 0.5D, false);
        int r = SkillConfigManager.getUseSetting(hero, this, SkillSetting.RADIUS, 10, false);
        long md = SkillConfigManager.getUseSetting(hero, this, "might-duration", 30000, false);
        b1.setType(Material.GLOWSTONE);
        k.add(b1);

        hero.addEffect(new VBEffect(this, period, duration, damage, r, b1, md));
        return SkillResult.NORMAL;
    }
}
