package com.atherys.skills.SkillWitherHead;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

public class SkillWitherHead extends ActiveSkill {

    public SkillWitherHead(Heroes plugin) {
        super(plugin, "WitherHead");
        setDescription("Shoots a wither head and deals $1 damage");
        setUsage("/skill WitherHead");
        setArgumentRange(0, 0);
        setIdentifiers("skill witherhead");
        setTypes(SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.HARMFUL);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillEntityListener(this), plugin);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Integer.valueOf(20));
        node.set("velocity-multiplier", Double.valueOf(1.5D));
        return node;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        Player player = hero.getPlayer();
        WitherSkull ws = player.launchProjectile(WitherSkull.class);
        ws.setMetadata("WitherHeadWitherSkull", new FixedMetadataValue(plugin, true));
        double mult = SkillConfigManager.getUseSetting(hero, this, "velocity-multiplier", 1.5D, false);
        ws.setVelocity(ws.getVelocity().multiply(mult));
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        int damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 20, false);
        return getDescription().replace("$1", damage + "");
    }

    public class SkillEntityListener
            implements Listener {
        private final Skill skill;

        public SkillEntityListener(Skill skill) {
            this.skill = skill;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent event) {
            if ((event.isCancelled()) || (!(event instanceof EntityDamageByEntityEvent)) || (!(event.getEntity() instanceof LivingEntity))) {
                return;
            }
            EntityDamageByEntityEvent subEvent = (EntityDamageByEntityEvent) event;
            Entity projectile = subEvent.getDamager();
            if ((!(projectile instanceof WitherSkull)) || (!projectile.hasMetadata("WitherHeadWitherSkull"))) {
                return;
            }
            LivingEntity entity = (LivingEntity) subEvent.getEntity();
            ProjectileSource dmger = ((WitherSkull) projectile).getShooter();
            if ((dmger instanceof Player)) {
                Hero hero = plugin.getCharacterManager().getHero((Player) dmger);
                if (!Skill.damageCheck((Player) dmger, entity)) {
                    entity.removePotionEffect(PotionEffectType.WITHER);
                    event.setCancelled(true);
                    return;
                }
                addSpellTarget(entity, hero);
                double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 25, false);
                Skill.damageEntity(entity, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC);
                entity.removePotionEffect(PotionEffectType.WITHER);
                event.setCancelled(true);
            }
        }
    }
}