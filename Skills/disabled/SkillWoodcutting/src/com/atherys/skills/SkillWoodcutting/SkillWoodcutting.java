package com.atherys.skills.SkillWoodcutting;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.listeners.HBlockListener;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class SkillWoodcutting extends PassiveSkill {
    public SkillWoodcutting(Heroes plugin) {
        super(plugin, "Woodcutting");
        setDescription("You have a $1% chance to get extra materials when logging.");
        setEffectTypes(EffectType.BENEFICIAL);
        setTypes(SkillType.KNOWLEDGE, SkillType.EARTH, SkillType.BUFF);
        Bukkit.getServer().getPluginManager().registerEvents(new SkillBlockListener(this), plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.CHANCE_LEVEL.node(), Double.valueOf(0.001D));
        return node;
    }

    public String getDescription(Hero hero) {
        double chance = SkillConfigManager.getUseSetting(hero, this, SkillSetting.CHANCE_LEVEL, 0.001D, false);
        int level = hero.getSkillLevel(this);
        if (level < 1) {
            level = 1;
        }
        return getDescription().replace("$1", Util.stringDouble(chance * level * 100.0D));
    }

    public class SkillBlockListener
            implements Listener {
        private final Skill skill;

        SkillBlockListener(Skill skill) {
            this.skill = skill;
        }

        @SuppressWarnings("deprecation")
        @EventHandler(priority = EventPriority.MONITOR)
        public void onBlockBreak(BlockBreakEvent event) {
            if (event.isCancelled()) {
                return;
            }
            Block block = event.getBlock();
            if (HBlockListener.placedBlocks.containsKey(block.getLocation())) {
                return;
            }
            int extraDrops = 0;
            switch (block.getType().ordinal()) {
                case 1:
                    break;
                default:
                    return;
            }
            Hero hero = SkillWoodcutting.this.plugin.getCharacterManager().getHero(event.getPlayer());
            if ((!hero.hasEffect("Woodcutting")) || (Util.nextRand() > SkillConfigManager.getUseSetting(hero, this.skill, SkillSetting.CHANCE_LEVEL, 0.001D, false) * hero.getSkillLevel(this.skill))) {
                return;
            }
            if (extraDrops != 0)
                extraDrops = Util.nextInt(extraDrops) + 1;
            else {
                extraDrops = 1;
            }
            block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(block.getType(), extraDrops, (short) 0, Byte.valueOf(block.getData())));
        }
    }
}
