package com.atherys.skills.SkillZombiePigmen;

import com.atherys.heroesaddon.util.Lib;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class SkillZombiePigmen extends ActiveSkill {
    public SkillZombiePigmen(Heroes plugin) {
        super(plugin, "ZombiePigmen");
        setDescription("Summons 1 pigzombie.");
        setUsage("/skill ZombiePigmen");
        setArgumentRange(0, 0);
        setIdentifiers("skill ZombiePigmen");
        setTypes(SkillType.SUMMON, SkillType.SILENCABLE, SkillType.TELEPORT);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription() + Lib.getSkillCostStats(hero, this);
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), 10);
        return node;
    }

    public HashSet<Byte> TransparentBlock() {
        HashSet<Byte> set = new HashSet<Byte>();
        set.add(Byte.valueOf((byte) Material.AIR.getId()));
        set.add(Byte.valueOf((byte) Material.WATER.getId()));
        set.add(Byte.valueOf((byte) Material.REDSTONE_TORCH_ON.getId()));
        set.add(Byte.valueOf((byte) Material.REDSTONE_TORCH_OFF.getId()));
        set.add(Byte.valueOf((byte) Material.REDSTONE_WIRE.getId()));
        set.add(Byte.valueOf((byte) Material.TORCH.getId()));
        set.add(Byte.valueOf((byte) Material.SNOW.getId()));
        set.add(Byte.valueOf((byte) Material.STRING.getId()));
        return set;
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        int d = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 10, false);
        Player player = hero.getPlayer();
        player.getWorld().spawn(player.getTargetBlock(TransparentBlock(), d).getLocation(), PigZombie.class);
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
}
