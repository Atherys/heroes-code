#threads=$(grep -c ^processor /proc/cpuinfo)
threads=$(nproc --all)

if [ ! -d work/ ]; then echo "Setting Up Submodules..."; ./setup.sh; fi

echo "Using $threads Threads for Maven Compile..."
sleep 1
mvn -T $threads
