#!/usr/bin/env bash

##============================##
##  Heroes Code Setup Script  ##
## (C) 2016 A`therys Ascended ##
##  Created by willies952002  ##
##============================##

(
	echo "Cleaing up Work Directory"
	rm work -Rf

	echo "Initializing Submodules"
	git submodule update --init -f

	echo "Generating SonarPet NMS Classes"
	cd work/SonarPet/
	bash cleanNms.sh
	bash generateNms.sh
	cd ../

	cd Paper
	branch=$(git branch | grep \* | sed -e 's/\* (//' | sed -e 's/)//')
	threads=$(nproc --all)
	echo "Building Paper Server ($branch)"
	./paper p && mvn clean install -T $threads

	echo "Done"
)
